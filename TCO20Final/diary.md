2020-11-15,16 TCO20 Final


11:50

making width and height of destruct area differ gives another 0.05%

submit12 : 99.54527 -> 99.43706

oops, TL is reverted to 6s

resubmit...

submit13 : 99.54527 -> 99.6165


11:20

submit11 : 98.2728 -> 99.53079


10:50

add pruning to `create_initial_solution` and try multiple times

+0.03%?


08:00

extend destruct-refill area if neighbor is vacant -> 0.1% gain


07:30

change `sub_weight` exponentially rather than linear -> +0.05%


07:00

is `select_use_c_rnd` not needed anymore?

- re-evaluate selection criteria for c/r

done. 0.2% gain


06:10

- calibrate target position

when X > 0.3, scatter circles outside and gather rectangled inside in initial solution
-> 0.09% gain


05:50

setting TL 6s -> 20s still gives 0.5% gain


05:20

reduce call count of `select_use_c_rnd`, during this, I found a nasty bug

```
Seed = 1, Score = 5736.730735411367, RunTime = 8 ms
improve turn:122944
Seed = 2, Score = 26048.194402954567, RunTime = 5994 ms
improve turn:144704
Seed = 3, Score = 26761.574328174866, RunTime = 5996 ms
improve turn:182592
Seed = 4, Score = 26843.309294086248, RunTime = 5993 ms
improve turn:135936
Seed = 5, Score = 26286.196965041177, RunTime = 5994 ms
improve turn:207232
Seed = 6, Score = 28433.774402864274, RunTime = 5994 ms
improve turn:213760
Seed = 7, Score = 27088.76136082246, RunTime = 5995 ms
improve turn:726400
Seed = 8, Score = 15236.69047506032, RunTime = 5993 ms
improve turn:268160
Seed = 9, Score = 29289.51342596213, RunTime = 5994 ms
improve turn:172608
Seed = 10, Score = 25962.672486639844, RunTime = 5995 ms
```

+0.1%


04:50

Psyho has come!


04:40

how about changing `max_rm_size` according to time?

-> 0.01% gain...???


03:30

how about changing the weight of the subtract part in the score?
initially lower the weight so as to maximize area, finally take balance

-> 0.5% gain!


03:20

run 5000 cases test to verify that there is no bug


02:30

setting TL 6s -> 20s gives 0.4% gain, so large


01:40

SA doesn't help :(


01:20

fixing small edge coordinage issue gives 0.3% gain

submit 10 : 99.55659 -> 99.71871
wleite's score : 99.57738 -> 99.30972 (this includes score change by Psyho's submission?)


00:50

changing HC to SA

```
Seed = 1, Score = 5736.730735411367, RunTime = 8 ms
Seed = 2, Score = 25961.367581660397, RunTime = 5993 ms
Seed = 3, Score = 26537.619370514476, RunTime = 5995 ms
Seed = 4, Score = 26309.001481117095, RunTime = 5993 ms
Seed = 5, Score = 26120.511506606388, RunTime = 5993 ms
Seed = 6, Score = 27941.925415971346, RunTime = 5994 ms
Seed = 7, Score = 26880.298338753768, RunTime = 5994 ms
Seed = 8, Score = 15090.81155328496, RunTime = 5994 ms
Seed = 9, Score = 29023.127776502784, RunTime = 5995 ms
Seed = 10, Score = 25782.05715413982, RunTime = 5994 ms
```


00:15

thanks to the other optimizations, placed[][] is no longer needed during HC.

10% speedup

```
Seed = 1, Score = 5736.730735411367, RunTime = 8 ms
improve turn:24527
Seed = 2, Score = 25886.112839381232, RunTime = 1994 ms
improve turn:32076
Seed = 3, Score = 26479.44915523058, RunTime = 1994 ms
improve turn:49188
Seed = 4, Score = 26225.823727919065, RunTime = 1994 ms
improve turn:31081
Seed = 5, Score = 26097.40301994129, RunTime = 1994 ms
improve turn:44023
Seed = 6, Score = 27728.103227198742, RunTime = 1994 ms
improve turn:52187
Seed = 7, Score = 26678.690223262587, RunTime = 1994 ms
improve turn:198086
Seed = 8, Score = 15086.155843015535, RunTime = 1994 ms
improve turn:74079
Seed = 9, Score = 28774.488493769524, RunTime = 1994 ms
improve turn:46463
Seed = 10, Score = 25734.38736587055, RunTime = 1994 ms
```


00:00

half of the competition finished!

randimize size of destruct area gives 0.1%

submit9 99.57365 -> 99.69818
current leader nika also submitted. it's difficult to deduce how was the actual improvement

relative evaluation looks broken, as well the previous MMs

made a cup of coffee (2)


23:40

multistart didn't help


23:10

with 100K iteration, Is it better to apply SA than HC?


22:50

implemented another optimization (pruning)

```
Seed = 1, Score = 5736.730735411367, RunTime = 8 ms
improve turn:24400
Seed = 2, Score = 25819.340805644882, RunTime = 2001 ms
improve turn:35416
Seed = 3, Score = 26398.277237234535, RunTime = 2001 ms
improve turn:61454
Seed = 4, Score = 25929.527343935937, RunTime = 2001 ms
improve turn:32669
Seed = 5, Score = 26067.60879659495, RunTime = 2001 ms
improve turn:29295
Seed = 6, Score = 27589.411623437853, RunTime = 2001 ms
improve turn:52600
Seed = 7, Score = 26387.127431397792, RunTime = 2001 ms
improve turn:183877
Seed = 8, Score = 15114.090104632094, RunTime = 2001 ms
improve turn:51252
Seed = 9, Score = 28210.43138989059, RunTime = 2000 ms
improve turn:54703
Seed = 10, Score = 25705.045043661412, RunTime = 2001 ms
```

another x8 speedup

submit8 : 98.23525 -> 99.57388


22:30

implement several optimization for vacant space search

```
Seed = 1, Score = 5736.730735411367, RunTime = 8 ms
improve turn:221
Seed = 2, Score = 25709.954942514836, RunTime = 2009 ms
improve turn:324
Seed = 3, Score = 26441.20828501624, RunTime = 2002 ms
improve turn:815
Seed = 4, Score = 26026.370000787094, RunTime = 2002 ms
improve turn:342
Seed = 5, Score = 25969.336162651896, RunTime = 2003 ms
improve turn:800
Seed = 6, Score = 27009.601101488126, RunTime = 2006 ms
improve turn:760
Seed = 7, Score = 26599.77441447877, RunTime = 2001 ms
improve turn:961
Seed = 8, Score = 14991.489734203866, RunTime = 2005 ms
improve turn:2202
Seed = 9, Score = 27671.690078042495, RunTime = 2001 ms
improve turn:611
Seed = 10, Score = 25660.571823049206, RunTime = 2001 ms

Seed = 1, Score = 5736.730735411367, RunTime = 7 ms
improve turn:2711
Seed = 2, Score = 25764.468579193253, RunTime = 2002 ms
improve turn:3824
Seed = 3, Score = 26441.20828501624, RunTime = 2002 ms
improve turn:7496
Seed = 4, Score = 26093.845995945292, RunTime = 2001 ms
improve turn:3964
Seed = 5, Score = 26022.833216316187, RunTime = 2001 ms
improve turn:5878
Seed = 6, Score = 27151.460181143335, RunTime = 2001 ms
improve turn:8865
Seed = 7, Score = 26557.84192888134, RunTime = 2001 ms
improve turn:17230
Seed = 8, Score = 15020.975899243565, RunTime = 2001 ms
improve turn:12590
Seed = 9, Score = 27733.257992110277, RunTime = 2001 ms
improve turn:6956
Seed = 10, Score = 25741.305478691185, RunTime = 2001 ms
```

x10 speedup!

testcases with large X seem to have large space for improvement


21:20

implemented destruct small area and reconstruct

about 1000 itertions of reconstruction on EC2?

```
RM_SIZE=8

Seed = 1, Score = 5736.730735411367, RunTime = 8 ms
Seed = 2, Score = 25719.49305477027, RunTime = 6008 ms
Seed = 3, Score = 26477.1022682814, RunTime = 6028 ms
Seed = 4, Score = 26056.165791248448, RunTime = 6026 ms
Seed = 5, Score = 26019.301294653345, RunTime = 6014 ms
Seed = 6, Score = 26978.50865852346, RunTime = 6004 ms
Seed = 7, Score = 26583.99335268593, RunTime = 6001 ms
Seed = 8, Score = 15039.598740321273, RunTime = 6011 ms
Seed = 9, Score = 27756.193319701066, RunTime = 6002 ms
Seed = 10, Score = 25717.46285236864, RunTime = 6008 ms

improve turn:312
improve turn:425
improve turn:860
improve turn:415
improve turn:848
improve turn:807
improve turn:965
improve turn:2083
improve turn:640
```

RM_SIZE=3 is the best : 0.7% gain
making TL to 10s from 2s gives another 0.7% gain

submit7 : 97.75298 -> 98.23525


20:20

another severe bug found : `overlap_cr` was wrong

fix and submit(6) : 97.13103 -> 98.09228


18:20

1% gain by extending TL 1s -> 20s
0.3% gain by extending TL 6s -> 20s
0.3% gain by extending TL 20s -> 100s

simple speedup doesn't help much

```
Seed = 1, Score = 5736.730735411367, RunTime = 815 ms
Seed = 2, Score = 24760.723295408337, RunTime = 1747 ms
Seed = 3, Score = 25787.438501357163, RunTime = 2597 ms
Seed = 4, Score = 25389.661145985137, RunTime = 1422 ms
Seed = 5, Score = 25155.488456438947, RunTime = 1515 ms
Seed = 6, Score = 26874.304735989383, RunTime = 3539 ms
Seed = 7, Score = 26204.30192532242, RunTime = 1984 ms
Seed = 8, Score = 14442.11592241155, RunTime = 1260 ms
Seed = 9, Score = 28085.58551382681, RunTime = 1879 ms
Seed = 10, Score = 24638.437885330935, RunTime = 1258 ms
```

17:45

severe bug fixed (order of rectangles not sorted by their area size) -> 2% gain

submit5 : 97.39273


17:00

fix selection criteria of circle or rectangle

generally, rectangles are more efficient to fill the area

update selection criteria of c/r so that increase the effect of X -> 1% gain

```
Seed = 1, Score = 5736.730735411367, RunTime = 987 ms
Seed = 2, Score = 24419.641356524306, RunTime = 1830 ms
Seed = 3, Score = 25150.94659196787, RunTime = 1561 ms
Seed = 4, Score = 24645.822739948388, RunTime = 908 ms
Seed = 5, Score = 25022.329180493627, RunTime = 2376 ms
Seed = 6, Score = 25457.733859703494, RunTime = 4348 ms
Seed = 7, Score = 25764.79220671106, RunTime = 2829 ms
Seed = 8, Score = 14605.065781841473, RunTime = 1921 ms
Seed = 9, Score = 25255.377439763226, RunTime = 1211 ms
Seed = 10, Score = 24711.96289497379, RunTime = 1223 ms
```

local execution is too slow due to high CPU usage of Hopin...

submitted : 94.76414

made a cup of coffee


16:10

implemented to target each ingredients top/bottom of the base

```
Seed = 1, Score = 5736.730735411367, RunTime = 1001 ms
Seed = 2, Score = 23789.362162770514, RunTime = 2074 ms
Seed = 3, Score = 24538.184549919664, RunTime = 2771 ms
Seed = 4, Score = 23902.486990835125, RunTime = 2097 ms
Seed = 5, Score = 24122.181390154277, RunTime = 3806 ms
Seed = 6, Score = 24343.592695487125, RunTime = 7843 ms
Seed = 7, Score = 23456.869270463543, RunTime = 4176 ms
Seed = 8, Score = 13787.21267784557, RunTime = 2841 ms
Seed = 9, Score = 25704.200633032062, RunTime = 1568 ms
Seed = 10, Score = 24237.115319230823, RunTime = 1857 ms
```

14:50

rectangle ingredients are better to put nearby together.

circle ingredients -> top of base
rectangle ingredients -> bottom of base

it's better to use large circle first

14:40

session frequently lost...

13:41

created basic randomize solution putting ingredients one-by-one

submitted.

12:48

customized test case generation, setup score sheet

12:22

wrote skelton solution

12:07 
read the statement
not animated -> no need to custmoize visualizer

seems to require heavy optimization
