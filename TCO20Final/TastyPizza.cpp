#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
#ifndef NDEBUG
// #define MEASURE_TIME
#define DEBUG
#endif
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 3.126448e9;
constexpr ll TL = 6000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 9600;
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
constexpr inline T sq(T v) { return v * v; }

void debug_vec(const vi& vec) {
  for (int i = 0; i < vec.size(); ++i) {
    debug("%d ", vec[i]);
  }
  debugln();
}

XorShift rnd;
Timer timer;
Counter counter;

constexpr double PI = 3.141592653589793238;

//////// end of template ////////

struct Pos {
  int x, y;
};

bool operator==(const Pos& p1, const Pos& p2) {
  return p1.x == p2.x && p1.y == p2.y;
}

struct Size {
  int w, h;
};

struct Result {
  vector<Pos> pos_c, pos_r;
  double score, area_c, area_r;
};

constexpr int INF = 1 << 28;
constexpr int BR = 100;
constexpr int INVALID_COORD = 255;
constexpr int8_t EMPTY = 0;
constexpr int8_t CIRCLE = 1;
constexpr int8_t RECT = 2;
constexpr int MIN_SZ = 5;
constexpr int MAX_CS = 15;
constexpr int MAX_RS = 25;
const Pos INVALID_POS = {INVALID_COORD, INVALID_COORD};
int C, R;
double X;
array<int, 500> CS;
array<Size, 500> RS;
array<vector<Pos>, MAX_CS + 1> avail_c;
array<vector<Pos>, MAX_RS + 1> avail_r;
array<array<int8_t, 2 * BR + 1>, 2 * BR + 1> placed;
constexpr double INITIAL_SUB_W = 0.1;
constexpr double FINAL_SUB_W = 1.0;
double sub_weight = 1.0;
constexpr int INITIAL_MAX_RM_SIZE = 11;
constexpr int FINAL_MAX_RM_SIZE = 2;
// double INITIAL_COOLER = 0.4;
// double FINAL_COOLER = 5.0;
// double sa_cooler = INITIAL_COOLER;

// inline bool accept(int diff) {
//   if (diff >= 0) return true;
//   double v = diff * sa_cooler;
//   if (v < -7) return false;
//   return rnd.nextDouble() < exp(v);
// }

double score(double area_c, double area_r) {
  return area_c + area_r - abs(area_c * X - area_r * (1 - X)) * sub_weight;
}

int rnd_int(int min, int max) {
  return (int)rnd.nextUInt(max - min + 1) + min;
}

bool inside_c(const Pos& p, int r) {
  return sq(p.x - BR) + sq(p.y - BR) <= sq(BR - r);
}

bool inside_r(const Pos& p, const Size& s) {
  if (sq(p.x - BR) + sq(p.y - BR) > sq(BR)) return false;
  if (sq(p.x - BR + s.w) + sq(p.y - BR) > sq(BR)) return false;
  if (sq(p.x - BR) + sq(p.y - BR + s.h) > sq(BR)) return false;
  if (sq(p.x - BR + s.w) + sq(p.y - BR + s.h) > sq(BR)) return false;
  return true;
}

bool overlap_cc(const Pos& p1, int r1, const Pos& p2, int r2) {
  return sq(p1.x - p2.x) + sq(p1.y - p2.y) < sq(r1 + r2);
}

bool overlap_rr(const Pos& p1, const Size& s1, const Pos& p2, const Size& s2) {
  if (p1.x + s1.w <= p2.x) return false;
  if (p1.x >= p2.x + s2.w) return false;
  if (p1.y + s1.h <= p2.y) return false;
  if (p1.y >= p2.y + s2.h) return false;
  return true;
}

bool touch_rr(const Pos& p1, const Size& s1, const Pos& p2, const Size& s2) {
  if (p1.x + s1.w == p2.x || p1.x == p2.x + s2.w) {
    if (p1.y + s1.h < p2.y) return false;
    if (p1.y > p2.y + s2.h) return false;
    return true;
  }
  if (p1.y + s1.h == p2.y || p1.y == p2.y + s2.h) {
    if (p1.x + s1.w < p2.x) return false;
    if (p1.x > p2.x + s2.w) return false;
    return true;
  }
  return false;
}

bool overlap_cr(const Pos& p1, int r1, const Pos& p2, const Size& s2) {
  const int x = abs(p1.x * 2 - (p2.x * 2 + s2.w));
  if (x >= s2.w + r1 * 2) return false;
  const int y = abs(p1.y * 2 - (p2.y * 2 + s2.h));
  if (y >= s2.h + r1 * 2) return false;
  if (x < s2.w) return true;
  if (y < s2.h) return true;
  return sq(x - s2.w) + sq(y - s2.h) < sq(r1 * 2);
}

template<int v>
void update_placed_c(const Pos& p, int r) {
  const int r2 = sq(r);
  for (int x = p.x - r; x < p.x + r; ++x) {
    for (int y = p.y - r; y < p.y + r; ++y) {
      // if (placed[x][y]) continue;
      const int d2 = sq(x) + sq(y);
      if (d2 < r2 || (d2 == r2 && (x < p.x || y < p.y))) {
        placed[x][y] = v;
      }
    }
  }
}

template<int v>
void update_placed_r(const Pos& p, const Size& sz) {
  for (int x = p.x; x < p.x + sz.w; ++x) {
    for (int y = p.y; y < p.y + sz.h; ++y) {
      placed[x][y] = v;
    }
  }
}

// constexpr double COEF_POW = 1.0;

inline bool select_use_c(double area_c, double area_r) {
  // return area_c * pow(X, COEF_POW) < area_r * pow(1 - X, COEF_POW);
  return area_c * X < area_r * (1 - X);
}

inline bool select_use_c_rnd(double area_c, double area_r) {
  double v_c = area_c * X;
  double v_r = area_r * (1 - X);
  return rnd.nextDouble() * (v_c + v_r) > v_c;
}

void improve(Result& res, ll timelimit) {
  Result bk_res = res;
  // Result best_res = res;
  vi orig_cands_c;
  vi orig_cands_r;
  for (int i = 0; i < C; ++i) {
    if (res.pos_c[i].x == INVALID_COORD) orig_cands_c.push_back(i);
  }
  for (int i = 0; i < R; ++i) {
    if (res.pos_r[i].x == INVALID_COORD) orig_cands_r.push_back(i);
  }
  vi remove_cs;
  vi remove_rs;
  vi compare_cs;
  vi compare_rs;
  vi ext_compare_cs;
  vi ext_compare_rs;
  array<bool, MAX_CS + 1> fail_place_c = {};
  array<array<bool, MAX_RS + 1>, MAX_RS + 1> fail_place_r = {};
  sub_weight = INITIAL_SUB_W;
  res.score = score(res.area_c, res.area_r);
  const ll before_time = get_elapsed_msec();
  int max_rm_size = INITIAL_MAX_RM_SIZE;
  for (int turn = 0; ; ++turn) {
    START_TIMER(1);
    if ((turn & 0xFF) == 0) {
      const ll elapsed = get_elapsed_msec();
      if (elapsed > timelimit) {
        debug("improve turn:%d\n", turn);
        break;
      }
      double ratio = 1.0 * (elapsed - before_time) / (timelimit - before_time);
      sub_weight = exp(log(INITIAL_SUB_W) * (1.0 - ratio));
      // sub_weight = FINAL_SUB_W * ratio + INITIAL_SUB_W * (1.0 - ratio);
      res.score = score(res.area_c, res.area_r);
      max_rm_size = int(FINAL_MAX_RM_SIZE * ratio + INITIAL_MAX_RM_SIZE * (1.0 - ratio) + 0.5);
      // double c0 = log(INITIAL_COOLER);
      // double c1 = log(FINAL_COOLER);
      // sa_cooler = exp(c1 * ratio + c0 * (1.0 - ratio));
    }
    const int RM_SIZE_W = rnd_int(2, max_rm_size);
    const int RM_SIZE_H = rnd_int(2, max_rm_size);
    const Pos& base_pos = avail_c[MIN_SZ][rnd.nextUInt(avail_c[MIN_SZ].size())];
    const Pos rm_area_pos = {base_pos.x - RM_SIZE_W, base_pos.y - RM_SIZE_H};
    const Size rm_area_size = {RM_SIZE_W * 2, RM_SIZE_H * 2};
    remove_cs.clear();
    remove_rs.clear();
    compare_cs.clear();
    compare_rs.clear();
    ext_compare_cs.clear();
    ext_compare_rs.clear();
    double area_c = res.area_c;
    double area_r = res.area_r;
    int min_x = rm_area_pos.x;
    int min_y = rm_area_pos.y;
    int max_x = rm_area_pos.x + rm_area_size.w;
    int max_y = rm_area_pos.y + rm_area_size.h;
    // debug("rm_area:(%d %d)-(%d %d)\n", rm_area_pos.x, rm_area_pos.y, rm_area_size.w, rm_area_size.h);
    for (int j = 0; j < C; ++j) {
      if (res.pos_c[j].x == INVALID_COORD) continue;
      if (overlap_cr(res.pos_c[j], CS[j], rm_area_pos, rm_area_size)) {
        remove_cs.push_back(j);
        update_placed_c<EMPTY>(res.pos_c[j], CS[j]);
        area_c -= sq(CS[j]) * PI;
        min_x = min(min_x, res.pos_c[j].x - CS[j]);
        min_y = min(min_y, res.pos_c[j].y - CS[j]);
        max_x = max(max_x, res.pos_c[j].x + CS[j]);
        max_y = max(max_y, res.pos_c[j].y + CS[j]);
        // debug("rm_c:(%d %d) %d\n", res.pos_c[j].x, res.pos_c[j].y, CS[j]);
        bk_res.pos_c[j] = res.pos_c[j];
        res.pos_c[j] = INVALID_POS;
        // debug("set invalid:%d\n", j);
      }
    }
    for (int j = 0; j < R; ++j) {
      if (res.pos_r[j].x == INVALID_COORD) continue;
      if (overlap_rr(res.pos_r[j], RS[j], rm_area_pos, rm_area_size)) {
        remove_rs.push_back(j);
        update_placed_r<EMPTY>(res.pos_r[j], RS[j]);
        area_r -= RS[j].w * RS[j].h;
        min_x = min(min_x, res.pos_r[j].x);
        min_y = min(min_y, res.pos_r[j].y);
        max_x = max(max_x, res.pos_r[j].x + RS[j].w);
        max_y = max(max_y, res.pos_r[j].y + RS[j].h);
        // debug("rm_r:(%d %d) (%d %d)\n",
         // res.pos_r[j].x, res.pos_r[j].y, res.pos_r[j].x + RS[j].w, res.pos_r[j].y + RS[j].h);
        bk_res.pos_r[j] = res.pos_r[j];
        res.pos_r[j] = INVALID_POS;
      }
    }
    while (inside_c({min_x - 1, min_y}, 0) && inside_c({min_x - 1, max_y}, 0)) {
      bool ok = true;
      for (int i = min_y; i < max_y; ++i) {
        if (placed[min_x - 1][i]) {
          ok = false;
          break;
        }
      }
      if (!ok) break;
      min_x--;
    }
    while (inside_c({max_x + 1, min_y}, 0) && inside_c({max_x + 1, max_y}, 0)) {
      bool ok = true;
      for (int i = min_y; i < max_y; ++i) {
        if (placed[max_x + 1][i]) {
          ok = false;
          break;
        }
      }
      if (!ok) break;
      max_x++;
    }
    while (inside_c({min_x, min_y - 1}, 0) && inside_c({max_x, min_y - 1}, 0)) {
      bool ok = true;
      for (int i = min_x; i < max_x; ++i) {
        if (placed[i][min_y - 1]) {
          ok = false;
          break;
        }
      }
      if (!ok) break;
      min_y--;
    }
    while (inside_c({min_x, max_y + 1}, 0) && inside_c({max_x, max_y + 1}, 0)) {
      bool ok = true;
      for (int i = min_x; i < max_x; ++i) {
        if (placed[i][max_y + 1]) {
          ok = false;
          break;
        }
      }
      if (!ok) break;
      max_y++;
    }
    const Pos compare_pos = {min_x, min_y};
    const Size compare_sz = {max_x - min_x, max_y - min_y};
    const Pos ext_compare_pos = {min_x - 20, min_y};
    const Size ext_compare_sz = {20, max_y - min_y};
    for (int j = 0; j < C; ++j) {
      if (res.pos_c[j].x == INVALID_COORD) continue;
      if (overlap_cr(res.pos_c[j], CS[j], compare_pos, compare_sz)) {
        compare_cs.push_back(j);
        // debug("compare_c:(%d %d) %d\n", res.pos_c[j].x, res.pos_c[j].y, CS[j]);
      } else if (overlap_cr(res.pos_c[j], CS[j], ext_compare_pos, ext_compare_sz)) {
        ext_compare_cs.push_back(j);
      }
    }
    for (int j = 0; j < R; ++j) {
      if (res.pos_r[j].x == INVALID_COORD) continue;
      if (overlap_rr(res.pos_r[j], RS[j], compare_pos, compare_sz)) {
        compare_rs.push_back(j);
        // debug("compare_r:(%d %d) (%d %d)\n",
         // res.pos_r[j].x, res.pos_r[j].y, res.pos_r[j].x + RS[j].w, res.pos_r[j].y + RS[j].h);
      } else if (overlap_rr(res.pos_r[j], RS[j], ext_compare_pos, ext_compare_sz)) {
        ext_compare_rs.push_back(j);
      }
    }
    // debug("compare_cs:%lu compare_rs:%lu (%d %d)-(%d %d)\n",
    //   compare_cs.size(), compare_rs.size(), compare_pos.x, compare_pos.y, compare_pos.x + compare_sz.w, compare_pos.y + compare_sz.h);
    vi cands_c = orig_cands_c;
    vi cands_r = orig_cands_r;
    cands_c.insert(cands_c.end(), remove_cs.begin(), remove_cs.end());
    cands_r.insert(cands_r.end(), remove_rs.begin(), remove_rs.end());
    int ci = 0;
    int ri = 0;
    fill(fail_place_c.begin() + MIN_SZ, fail_place_c.end(), false);
    for (int i = MIN_SZ; i <= MAX_RS; ++i) {
      fill(fail_place_r[i].begin() + MIN_SZ, fail_place_r[i].end(), false);
    }
    for (int i = 0; i < (int)cands_c.size() - 1; ++i) {
      int swp = rnd.nextUInt(cands_c.size() - i) + i;
      swap(cands_c[i], cands_c[swp]);
    }
    for (int i = 0; i < (int)cands_r.size() - 1; ++i) {
      int swp = rnd.nextUInt(cands_r.size() - i) + i;
      swap(cands_r[i], cands_r[swp]);
    }
    STOP_TIMER(1);
    START_TIMER(2);
    while (ci < cands_c.size() || ri < cands_r.size()) {
      bool use_c;
      if (ci == cands_c.size()) {
        use_c = false;
      } else if (ri == cands_r.size()) {
        use_c = true;
      } else {
        use_c = select_use_c(area_c, area_r);
      }
      if (use_c) {
        const int cidx = cands_c[ci];
        int rad = CS[cidx];
        bool found = false;
        for (int cy = max_y - rad; cy >= min_y + rad && !found; --cy) {
          int hit_x = INVALID_COORD;
          for (int cx = min_x + rad; cx <= max_x - rad; ++cx) {
            if (!inside_c({cx, cy}, rad)) continue;
            bool ok = true;
            for (int k = (int)compare_cs.size() - 1; k >= 0; --k) {
              int cii = compare_cs[k];
              assert (res.pos_c[cii].x != INVALID_COORD);
              if (overlap_cc({cx, cy}, rad, res.pos_c[cii], CS[cii])) {
                ok = false;
                cx = (int)(res.pos_c[cii].x + sqrt(sq(CS[cii] + rad) - sq(cy - res.pos_c[cii].y)) - 1e-8);
                break;
              }
            }
            for (int k = (int)compare_rs.size() - 1; k >= 0 && ok; --k) {
              int rii = compare_rs[k];
              assert (res.pos_r[rii].x != INVALID_COORD);
              if (overlap_cr({cx, cy}, rad, res.pos_r[rii], RS[rii])) {
                ok = false;
                if (cy < res.pos_r[rii].y) {
                  cx = res.pos_r[rii].x + RS[rii].w + (int)(sqrt(sq(rad) - sq(res.pos_r[rii].y - cy)) - 1e-8);
                } else if (cy <= res.pos_r[rii].y + RS[rii].h) {
                  cx = res.pos_r[rii].x + RS[rii].w + rad - 1;
                } else {
                  cx = res.pos_r[rii].x + RS[rii].w + (int)(sqrt(sq(rad) - sq(res.pos_r[rii].y + RS[rii].h - cy)) - 1e-8);
                }
                break;
              }
            }
            if (ok) {
              hit_x = cx;
              break;
            }
          }
          if (hit_x != INVALID_COORD) {
            if (hit_x == min_x + rad) {
              for (int cx = min_x + rad - 1; cx > 0 && cx >= min_x + rad - 20; --cx) {
                if (!inside_c({cx, cy}, rad)) break;
                bool ok = true;
                for (int k = (int)compare_cs.size() - 1; k >= 0; --k) {
                  int cii = compare_cs[k];
                  assert (res.pos_c[cii].x != INVALID_COORD);
                  if (overlap_cc({cx, cy}, rad, res.pos_c[cii], CS[cii])) {
                    ok = false;
                    break;
                  }
                }
                for (int k = (int)compare_rs.size() - 1; k >= 0 && ok; --k) {
                  int rii = compare_rs[k];
                  assert (res.pos_r[rii].x != INVALID_COORD);
                  if (overlap_cr({cx, cy}, rad, res.pos_r[rii], RS[rii])) {
                    ok = false;
                    break;
                  }
                }
                for (int k = (int)ext_compare_cs.size() - 1; k >= 0 && ok; --k) {
                  int cii = ext_compare_cs[k];
                  assert (res.pos_c[cii].x != INVALID_COORD);
                  if (overlap_cc({cx, cy}, rad, res.pos_c[cii], CS[cii])) {
                    ok = false;
                    break;
                  }
                }
                for (int k = (int)ext_compare_rs.size() - 1; k >= 0 && ok; --k) {
                  int rii = ext_compare_rs[k];
                  assert (res.pos_r[rii].x != INVALID_COORD);
                  if (overlap_cr({cx, cy}, rad, res.pos_r[rii], RS[rii])) {
                    ok = false;
                    break;
                  }
                }
                if (ok) {
                  hit_x = cx;
                } else {
                  break;
                }
              }
            }
            res.pos_c[cidx] = {hit_x, cy};
            // debug("set %d (%d %d)\n", cidx, cx, cy);
            area_c += rad * rad * PI;
            compare_cs.push_back(cidx);
            found = true;
            break;
          }
        }
        if (!found) {
          for (int j = rad; j <= MAX_CS && !fail_place_c[j]; ++j) {
            fail_place_c[j] = true;
          }
        }
        ci++;
        while (ci < cands_c.size() && fail_place_c[CS[cands_c[ci]]]) {
          ci++;
        }
      } else {
        const int ridx = cands_r[ri];
        const Size& sz = RS[ridx];
        bool found = false;
        for (int cy = min_y; cy <= max_y - sz.h && !found; ++cy) {
          for (int cx = min_x; cx <= max_x - sz.w; ++cx) {
            if (!inside_r({cx, cy}, sz)) continue;
            bool ok = true;
            for (int k = (int)compare_rs.size() - 1; k >= 0; --k) {
              int rii = compare_rs[k];
              assert (res.pos_r[rii].x != INVALID_COORD);
              if (overlap_rr(res.pos_r[rii], RS[rii], {cx, cy}, sz)) {
                ok = false;
                cx = res.pos_r[rii].x + RS[rii].w - 1;
                break;
              }
            }
            for (int k = (int)compare_cs.size() - 1; k >= 0 && ok; --k) {
              int cii = compare_cs[k];
              assert (res.pos_c[cii].x != INVALID_COORD);
              if (overlap_cr(res.pos_c[cii], CS[cii], {cx, cy}, sz)) {
                ok = false;
                if (cy + sz.h < res.pos_c[cii].y) {
                  cx = res.pos_c[cii].x + (int)(sqrt(sq(CS[cii]) - sq(res.pos_c[cii].y - (cy + sz.h))) - 1e-5);
                } else if (cy <= res.pos_c[cii].y) {
                  cx = res.pos_c[cii].x + CS[cii] - 1;
                } else {
                  cx = res.pos_c[cii].x + (int)(sqrt(sq(CS[cii]) - sq(res.pos_c[cii].y - cy)) - 1e-5);
                }
                break;
              }
            }
            if (ok) {
              res.pos_r[ridx] = {cx, cy};
              area_r += sz.w * sz.h;
              compare_rs.push_back(ridx);
              found = true;
              break;
            }
          }
        }
        if (!found) {
          for (int j = sz.w; j <= MAX_RS; ++j) {
            for (int k = sz.h; k <= MAX_RS && !fail_place_r[j][k]; ++k) {
              fail_place_r[j][k] = true;
            }
          }
        }
        ri++;
        while (ri < cands_r.size() && fail_place_r[RS[cands_r[ri]].w][RS[cands_r[ri]].h]) {
          ri++;
        }
      }
    }
    STOP_TIMER(2);
    START_TIMER(3);
    double new_score = score(area_c, area_r);
    // if (accept(new_score - res.score)) {
    if (new_score >= res.score - 1e-5) {
      res.area_c = area_c;
      res.area_r = area_r;
      if (new_score > res.score) {
        // debug("%.1f %.1f %.1f -> %.1f\n", area_c, area_r, res.score, new_score);
        // if (new_score > best_res.score) {
        //   best_res = res;
        //   best_res.score = new_score;
        // }
      }
      res.score = new_score;
      orig_cands_c.clear();
      orig_cands_r.clear();
      for (int cidx : cands_c) {
        if (res.pos_c[cidx].x == INVALID_COORD) {
          orig_cands_c.push_back(cidx);
        } else {
          update_placed_c<CIRCLE>(res.pos_c[cidx], CS[cidx]);
        }
      }
      for (int ridx : cands_r) {
        if (res.pos_r[ridx].x == INVALID_COORD) {
          orig_cands_r.push_back(ridx);
        } else {
          update_placed_r<RECT>(res.pos_r[ridx], RS[ridx]);
        }
      }
    } else {
      // revert
      for (int cidx : cands_c) {
        res.pos_c[cidx] = INVALID_POS;
      }
      for (int ridx : cands_r) {
        res.pos_r[ridx] = INVALID_POS;
      }
      for (int cidx : remove_cs) {
        res.pos_c[cidx] = bk_res.pos_c[cidx];
        update_placed_c<CIRCLE>(res.pos_c[cidx], CS[cidx]);
        // debug("reverted: %d (%d %d)\n", cidx, res.pos_c[cidx].x, res.pos_c[cidx].y);
        assert(res.pos_c[cidx].x != INVALID_COORD);
      }
      for (int ridx : remove_rs) {
        res.pos_r[ridx] = bk_res.pos_r[ridx];
        update_placed_r<RECT>(res.pos_r[ridx], RS[ridx]);
        // debug("reverted:(%d %d)\n", res.pos_r[ridx].x, res.pos_r[ridx].y);
        assert(res.pos_r[ridx].x != INVALID_COORD);
      }
    }
    STOP_TIMER(3);
  }
  // res = best_res;
}

template<bool small_X>
Result create_initial_solution() {
  Result res;
  double area_c = 0.0;
  double area_r = 0.0;
  for (auto& row : placed) {
    row.fill(0);
  }
  res.pos_c.resize(C, INVALID_POS);
  res.pos_r.resize(R, INVALID_POS);
  vi cands_c(C);
  iota(cands_c.begin(), cands_c.end(), 0);
  sort(cands_c.begin(), cands_c.end(), [](int i1, int i2){
    return CS[i1] > CS[i2];
  });
  vi cands_r(R);
  iota(cands_r.begin(), cands_r.end(), 0);
  sort(cands_r.begin(), cands_r.end(), [](int i1, int i2){
    return RS[i1].w * RS[i1].h > RS[i2].w * RS[i2].h;
  });
  for (int i = 0; i < (int)cands_c.size() - 1; ++i) {
    int swp = rnd.nextUInt(min(5, (int)cands_c.size() - i)) + i;
    swap(cands_c[i], cands_c[swp]);
  }
  for (int i = 0; i < (int)cands_r.size() - 1; ++i) {
    int swp = rnd.nextUInt(min(5, (int)cands_r.size() - i)) + i;
    swap(cands_r[i], cands_r[swp]);
  }
  int ci = 0;
  int ri = 0;
  array<bool, MAX_CS + 1> fail_place_c = {};
  array<array<bool, MAX_RS + 1>, MAX_RS + 1> fail_place_r = {};
  while (ci < cands_c.size() || ri < cands_r.size()) {
    bool use_c;
    if (ci == C) {
      use_c = false;
    } else if (ri == R) {
      use_c = true;
    } else {
      use_c = select_use_c(area_c, area_r);
    }
    if (use_c) {
      const int cidx = cands_c[ci];
      int rad = CS[cidx];
      int best_d = INF;
      // int best_d = small_X ? INF : -1;
      for (int j = 0; j < avail_c[rad].size(); ++j) {
        const Pos pos = avail_c[rad][j];
        int cx = pos.x;
        int cy = pos.y;
        assert(inside_c(pos, rad));
        if (placed[cx][cy]) continue;
        int cur_d = abs(abs(cx - BR) - abs(cy - BR));
        if (cur_d >= best_d) continue;
        // int cur_d;
        // if (small_X) {
        //   // target to (BR, 2 * BR)
        //   if (sq(cy + rad - 2 * BR) >= best_d) break;
        //   cur_d = sq(cx - BR) + sq(cy + rad - 2 * BR);
        //   if (cur_d >= best_d) continue;
        // } else {
        //   // target to outer
        //   cur_d = sq(cx - BR) + sq(cy - BR);
        //   if (cur_d <= best_d) continue;
        // }
        bool ok = true;
        for (int k = 0; k < ci; ++k) {
          int cii = cands_c[k];
          if (res.pos_c[cii].x == INVALID_COORD) continue;
          if (overlap_cc({cx, cy}, rad, res.pos_c[cii], CS[cii])) {
            ok = false;
            break;
          }
        }
        for (int k = 0; k < ri && ok; ++k) {
          int rii = cands_r[k];
          if (res.pos_r[rii].x == INVALID_COORD) continue;
          if (overlap_cr({cx, cy}, rad, res.pos_r[rii], RS[rii])) {
            ok = false;
            break;
          }
        }
        if (ok) {
          res.pos_c[cidx] = pos;
          best_d = cur_d;
        }
      }
      // if (best_d != (small_X ? INF : -1)) {
      if (best_d != INF) {
        area_c += rad * rad * PI;
        update_placed_c<CIRCLE>(res.pos_c[cidx], rad);
      } else {
        for (int j = rad; j <= MAX_CS && !fail_place_c[j]; ++j) {
          fail_place_c[j] = true;
        }
      }
      ci++;
      while (ci < cands_c.size() && fail_place_c[CS[cands_c[ci]]]) {
        ci++;
      }
    } else {
      const int ridx = cands_r[ri];
      const Size& sz = RS[ridx];
      int min_d = INF;
      int ms = min(sz.w, sz.h);
      for (int j = 0; j < avail_r[ms].size(); ++j) {
        const Pos pos = avail_r[ms][j];
        int cx = pos.x;
        int cy = pos.y;
        if (placed[cx][cy]) continue;
        if (!inside_r({cx, cy}, sz)) continue;
        int cur_d;
        if (small_X) {
          // target to (BR, 0)
          if (sq(cy + sz.h) >= min_d) break;
          cur_d = sq(cx + (sz.w >> 1) - BR) + sq(cy + sz.h);
        } else {
          // target to (BR, BR)
          cur_d = sq(cx + (sz.w >> 1) - BR) + sq(cy + (sz.h >> 1) - BR);
          if (cur_d >= min_d) continue;
        }
        bool ok = true;
        for (int k = 0; k < ci; ++k) {
          int cii = cands_c[k];
          if (res.pos_c[cii].x == INVALID_COORD) continue;
          if (overlap_cr(res.pos_c[cii], CS[cii], {cx, cy}, sz)) {
            ok = false;
            break;
          }
        }
        for (int k = 0; k < ri && ok; ++k) {
          int rii = cands_r[k];
          if (res.pos_r[rii].x == INVALID_COORD) continue;
          if (overlap_rr(res.pos_r[rii], RS[rii], {cx, cy}, sz)) {
            ok = false;
            break;
          }
        }
        if (ok) {
          res.pos_r[ridx] = pos;
          min_d = cur_d;
        }
      }
      if (min_d != INF) {
        area_r += sz.w * sz.h;
        update_placed_r<RECT>(res.pos_r[ridx], sz);
      } else {
        for (int j = sz.w; j <= MAX_RS; ++j) {
          for (int k = sz.h; k <= MAX_RS && !fail_place_r[j][k]; ++k) {
            fail_place_r[j][k] = true;
          }
        }
      }
      ri++;
      while (ri < cands_r.size() && fail_place_r[RS[cands_r[ri]].w][RS[cands_r[ri]].h]) {
        ri++;
      }
    }
    // debug("%d %d %d\n", cand_pos[i].x, cand_pos[i].y, cands[i]);
  }
  res.score = score(area_c, area_r);
  res.area_c = area_c;
  res.area_r = area_r;
  return res;
}

class TastyPizza {
public:
  Result findSolution() {
    double full_area_c = 0.0;
    double full_area_r = 0.0;
    for (int i = 0; i < C; ++i) {
      full_area_c += sq(CS[i]) * PI;
    }
    for (int i = 0; i < R; ++i) {
      full_area_r += RS[i].w * RS[i].h;
    }
    const double full_score = score(full_area_c, full_area_r);
    Result best_res;
    best_res.score = 0.0;
    const int MULTISTART = 5;
    for (int turn = 0; turn < MULTISTART; ++turn) {
      START_TIMER(0);
      Result res = X < 0.3 ? create_initial_solution<true>() : create_initial_solution<false>();
      STOP_TIMER(0);
      debug("initial_sol time:%lld score:%f\n", get_elapsed_msec(), res.score);
      if (res.score > best_res.score) {
        best_res = res;
        if (best_res.score >= full_score - 1e-5) {
          break;
        }
      }
    }
    if (best_res.score < full_score - 1e-5) {
      for (auto& row : placed) {
        row.fill(0);
      }
      for (int i = 0; i < C; ++i) {
        if (best_res.pos_c[i].x != INVALID_COORD) {
          update_placed_c<CIRCLE>(best_res.pos_c[i], CS[i]);
        }
      }
      for (int i = 0; i < R; ++i) {
        if (best_res.pos_r[i].x != INVALID_COORD) {
          update_placed_r<RECT>(best_res.pos_r[i], RS[i]);
        }
      }
      improve(best_res, TL);
    }
    return best_res;
  }
};

int main(/*int argc, char** argv*/) {
  scanf("%d %d %lf", &C, &R, &X);
  start_time = get_time();
  debug("C:%d R:%d X:%f\n", C, R, X);
  for (int i = 0; i < C; ++i) {
    scanf("%d", &CS[i]);
  }
  for (int i = 0; i < R; ++i) {
    scanf("%d %d", &RS[i].w, &RS[i].h);
  }
  for (int i = MIN_SZ; i <= MAX_CS; ++i) {
    for (int y = 2 * BR - 5; y >= 5; --y) {
      for (int x = 5; x <= 2 * BR - 5; ++x) {
        if (inside_c({x, y}, i)) {
          avail_c[i].push_back({x, y});
        }
      }
    }
  }
  for (int i = MIN_SZ; i <= MAX_RS; ++i) {
    for (int y = 1; y <= 2 * BR - 1; ++y) {
      for (int x = 1; x <= 2 * BR - 1; ++x) {
        if (inside_r({x, y}, {i, i})) {
          avail_r[i].push_back({x, y});
        }
      }
    }
  }
  TastyPizza prog;
  const Result res = prog.findSolution();
  printf("%d\n", C + R);
  for (const auto& p : res.pos_c) {
    if (p.x == INVALID_COORD) {
      printf("NA\n");
    } else {
      printf("%d %d\n", p.x - BR, p.y - BR);
    }
  }
  debug(
    "score:%f area_c:%.1f area_r:%.1f (%.1f-%.1f)\n",
    res.score, res.area_c, res.area_r, res.area_c * X, res.area_r * (1 - X));
  debug("elapsed:%lld\n", get_elapsed_msec());
  PRINT_TIMER();
  PRINT_COUNTER();
  for (const auto& p : res.pos_r) {
    if (p.x == INVALID_COORD) {
      printf("NA\n");
    } else {
      printf("%d %d\n", p.x - BR, p.y - BR);
    }
  }
  fflush(stdout);
}