#include <vector>
#include <string>
#include <cstdio>
#include <iostream>
#include <fstream>
#include "FragileMirrors.cpp"

struct Result {
	int seed;
	int N;
	double score;
	ll elapsed;
};

void print(const Result& res) {
	printf("seed:%4d  N:%d\n", res.seed, res.N);
	printf("elapsed:%.5f\n", res.elapsed / 1000.0);
	printf("score:  %.5f\n", res.score);
}

double calcScore(vector<string>& board, vector<int> ray) {
	for (int i = 0; i < ray.size() / 2; ++i) {
		int r = ray[i * 2];
		int c = ray[i * 2 + 1];
		int d;
		if (r == -1) {
			d = 0;
		} else if (c == -1) {
			d = 1;
		} else if (r == N) {
			d = 2;
		} else {
			d = 3;
		}
		while (true) {
			r += DR[d];
			c += DC[d];
			// cerr << r << "," << c << "," << d << endl;
			if (r == -1 || r == N || c == -1 || c == N) break;
			if (board[r][c] == 'L') {
				d ^= 3;
			} else if (board[r][c] == 'R') {
				d ^= 1;
			}
			board[r][c] = ' ';
		}
	}
	for (int i = 0; i < board.size(); ++i) {
		for (int j = 0; j < board.size(); ++j) {
			if (board[i][j] != ' ') {
				return -1;
			}
		}
	}
	return 1.0 * board.size() / (ray.size() / 2);
}

Result runTest(int seed) {
	Result res;
	res.seed = seed;
	char fileName[30];
	sprintf(fileName, "testdata/%04d.txt", seed);
	fstream stm(fileName);
	int N;
	stm >> N;
	res.N = N;
	vector<string> board;
	for (int i = 0; i < N; ++i) {
		string line;
		stm >> line;
		board.push_back(line);
	}
	ll before = getTime();
	FragileMirrors obj;
	vector<int> ret = obj.destroy(board);
	res.elapsed = getTime() - before;
	res.score = calcScore(board, ret);
	return res;
}

int main(int argc, char** argv) {
	int seed = 1, begin = -1, end = -1;
	for (int i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "-seed") == 0) {
			seed = atoi(argv[++i]);
		} else if (strcmp(argv[i], "-begin") == 0) {
			begin = atoi(argv[++i]);
		} else if (strcmp(argv[i], "-end") == 0) {
			end = atoi(argv[++i]);
		}
	}
	if (begin != -1 && end != -1) {
		double sum = 0;
		for (int i = begin; i <= end; ++i) {
			Result res = runTest(i);
			print(res);
			printf("\n");
			sum += res.score;
		}
		printf("ave:%f\n", sum / (end - begin + 1));
	} else {
		Result res = runTest(seed);
		print(res);
	}

}