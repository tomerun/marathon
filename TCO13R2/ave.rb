#!/usr/bin/ruby

sum = 0
count = 0
seed = 0;
seed_begin = ARGV[1] ? ARGV[1].to_i : nil
seed_end = ARGV[2] ? ARGV[2].to_i : nil
sumOdd = 0
sumEven = 0
countOdd = 0
countEven = 0
n = 0

IO.foreach(ARGV[0]){ |line|
  index = line.index("seed")
  if index == 0
    seed = line[index+5..8].to_i
  end
  index = line.index("N:")
  if index 
    n = line[index+2..-1].to_i
  end
  next if seed_begin && seed < seed_begin 
  break if seed_end && seed > seed_end 
  index = line.index('score')
  if index == 0
    score = line[6..-1].to_f
    sum += score
    count += 1
    if (n % 2 == 0) 
      sumEven += score
      countEven += 1
    else
      sumOdd += score
      countOdd += 1
    end
  end
}

puts "ave: #{sum.to_f / count} / #{count}"
puts "aveE: #{sumEven.to_f / countEven} / #{countEven}"
puts "aveO: #{sumOdd.to_f / countOdd} / #{countOdd}"
