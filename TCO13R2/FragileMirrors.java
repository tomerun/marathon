import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class FragileMirrors {

	private static final long TIMELIMIT = 3000;
	private static final boolean DEBUG = 1 == 0;
	static final boolean MEASURE_TIME = 1 == 0;
	private static final int INVALID = -2;
	private static final int WALL = 2;
	private static final int[] DR = { 1, 0, -1, 0 };
	private static final int[] DC = { 0, 1, 0, -1 };
	private int RETAIN = 200;

	private long starttime = System.currentTimeMillis();
	private XorShift rnd = new XorShift();
	private int N;
	private int[][] origBoard;
	private int[] ans;
	private int bestLen;
	private int[] rcBuf, ccBuf, rcIdx, ccIdx;
	private int[][][][] fieldPool;
	private int[][] visited;
	private long[][] hash;
	private int[][] usedIdx;
	private HashSet<Long> used = new HashSet<Long>();
	private Timer timer = new Timer();
	private int[] hist = new int[4];
	private final int maxRetain = 1000;

	public int[] destroy(String[] input) {
		N = input.length;
		origBoard = new int[N + 2][N + 2];
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				origBoard[i + 1][j + 1] = input[i].charAt(j) == 'L' ? 3 : 1;
			}
		}
		for (int i = 0; i < N + 2; ++i) {
			origBoard[0][i] = origBoard[N + 1][i] = origBoard[i][0] = origBoard[i][N + 1] = WALL;
		}
		if (N <= 54) {
			RETAIN = 800;
		} else if (N <= 60) {
			RETAIN = 650;
		} else if (N <= 70) {
			RETAIN = 500;
		} else if (N <= 80) {
			RETAIN = 400;
		} else if (N <= 90) {
			RETAIN = 350;
		} else {
			RETAIN = 280;
		}
		RETAIN = RETAIN * 2 / 3;
		bestLen = 5 * N;
		ans = new int[bestLen * 2];
		rcBuf = new int[N + 2];
		ccBuf = new int[N + 2];
		rcIdx = new int[N + 2];
		ccIdx = new int[N + 2];
		hash = new long[N + 2][N + 2];
		usedIdx = new int[N + 2][N + 2];
		fieldPool = new int[2][maxRetain][N + 2][N + 2];
		visited = new int[N + 2][N + 2];
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				hash[i][j] = ((long) rnd.nextInt() << 32) | rnd.nextInt();
			}
		}
		long before = System.currentTimeMillis() - starttime;
		while (true) {
			log("retain", RETAIN);
			int prevRetain = RETAIN;
			used.clear();
			solveBeam();
			RETAIN = prevRetain;
			long after = System.currentTimeMillis() - starttime;
			log("time", after);
			if (after > TIMELIMIT) break;
			if (after - before > TIMELIMIT - after) {
				RETAIN = (int) (0.98 * RETAIN * (TIMELIMIT - after) / (after - before));
				if (RETAIN < 10) RETAIN = 10;
			}
			before = after;
		}
		int[] ret = new int[bestLen * 2];
		System.arraycopy(ans, 0, ret, 0, bestLen * 2);
		if (MEASURE_TIME) log("timer", timer.sum);
		log("hist", hist);
		//				for (int i = 0; i < bestLen; ++i) {
		//					log(ret[2 * i], ret[2 * i + 1]);
		//				}
		return ret;
	}

	int tryIdx = 1;
	int boardIdx = 1;

	void solveBeam() {
		timer.start(0);
		ArrayList<Board> boards = new ArrayList<Board>();
		boards.add(new Board(origBoard));
		int turn = 0;
		while (turn + 1 < bestLen) {
			if (bestLen < 5 * N && System.currentTimeMillis() - starttime > TIMELIMIT) {
				break;
			}
			RETAIN = Math.min(maxRetain, (int) (1.015 * RETAIN));
			//			log("turn", turn, System.currentTimeMillis() - starttime);
			ArrayList<Path> paths = new ArrayList<Path>(boards.size() * N);
			timer.start(1);
			for (int bi = 0; bi < boards.size(); ++bi, ++boardIdx) {
				Board board = boards.get(bi);
				// TODO when board.rest is small, brute force?

				// try casting all position 
				for (int i = 1; i <= N; ++i) {
					if (board.cc[i] != N) {
						if (visited[0][i] != boardIdx) {
							Path p = board.trycast(0, i, 0);
							tryIdx++;
							paths.add(p);
						}
						if (visited[N + 1][i] != boardIdx) {
							Path p = board.trycast(N + 1, i, 2);
							tryIdx++;
							paths.add(p);
						}
					}
					if (board.rc[i] != N) {
						if (visited[i][0] != boardIdx) {
							Path p = board.trycast(i, 0, 1);
							tryIdx++;
							paths.add(p);
						}
						if (visited[i][N + 1] != boardIdx) {
							Path p = board.trycast(i, N + 1, 3);
							tryIdx++;
							paths.add(p);
						}
					}
				}
			}
			timer.end(1);

			timer.start(2);
			if (paths.size() > RETAIN * 3) {
				partition(paths, RETAIN * 3, 0, paths.size());
				partition(paths, RETAIN, 0, RETAIN * 3);
			} else if (paths.size() > RETAIN) {
				partition(paths, RETAIN, 0, paths.size());
			}
			if (paths.size() > RETAIN / 8) {
				partition(paths, RETAIN / 8, 0, Math.min(paths.size(), RETAIN));
			}
			timer.end(2);

			// randomize
			timer.start(3);
			if (paths.size() > RETAIN * 2) {
				int range = Math.min(paths.size() - RETAIN, RETAIN * 3);
				for (int i = 0, size = Math.max(RETAIN / 8, 2); i < size; ++i) {
					int from = rnd.nextInt(RETAIN - RETAIN / 8) + RETAIN / 8;
					int to = rnd.nextInt(range) + RETAIN;
					swap(paths, from, to);
				}
			}
			timer.end(3);

			// create next Boards
			int[] bc = new int[boards.size()];
			final int maxBC = Math.max(RETAIN / 10, 3);
			ArrayList<Board> next = new ArrayList<Board>(RETAIN);
			for (int i = 0; i < paths.size() && next.size() < RETAIN; ++i) {
				Path p = paths.get(i);
				if (turn != 0 && bc[p.board.idx] == maxBC) continue;
				if (turn + 1 + (p.board.odd + p.odd) / 2 >= bestLen) continue;
				bc[p.board.idx]++;
				timer.start(4);
				Board nb = new Board(p.board, turn & 1, next.size());
				timer.end(4);
				timer.start(5);
				if (!nb.apply(p)) {
					timer.end(5);
					continue;
				}
				timer.end(5);
				if (nb.rest == 0) {
					finish(nb, turn);
					timer.end(0);
					return;
				}
				next.add(nb);
			}
			for (Board board : boards) {
				board.destruct();
			}
			boards = next;
			turn += 1;
			//			log(boards.size(), paths.size());
		}
		timer.end(0);
	}

	void finish(Board board, int turn) {
		int ind = turn;
		while (board.moveR != INVALID) {
			ans[ind * 2] = board.moveR - 1;
			ans[ind * 2 + 1] = board.moveC - 1;
			board = board.parent;
			--ind;
		}
		bestLen = turn + 1;
		log(bestLen);
	}

	final class Board {
		int[][] field;
		int[] rc, cc;
		int moveR, moveC;
		int rest, emptyLine, odd;
		long h;
		Board parent;
		int idx;

		Board(int[][] orig) {
			field = fieldPool[1][0];
			for (int i = 0; i <= N + 1; ++i) {
				for (int j = 0; j <= N + 1; ++j) {
					field[i][j] = orig[i][j] + (1 << 2) + (1 << 9) + (1 << 16) + (1 << 23);
				}
			}
			rc = new int[N + 2];
			cc = new int[N + 2];
			moveR = moveC = INVALID;
			rest = N * N;
			if (N % 2 == 1) odd = 2 * N;
		}

		Board(Board o, int t, int idx) {
			field = fieldPool[t][idx];
			for (int i = 0; i <= N + 1; ++i) {
				if (o.rc[i] != N) System.arraycopy(o.field[i], 0, field[i], 0, N + 2);
			}
			rc = o.rc.clone();
			cc = o.cc.clone();
			rest = o.rest;
			emptyLine = o.emptyLine;
			odd = o.odd;
			h = o.h;
			parent = o;
			this.idx = idx;
		}

		void destruct() {
			field = null;
			rc = cc = null;
		}

		Path trycast(int r, int c, int d) {
			Path path = new Path(this);
			path.r = r;
			path.c = c;
			int lineC = N - (d % 2 == 0 ? cc[c] : rc[r]);
			path.odd -= (lineC & 1) * 2 - 1;
			while (true) {
				int move = (field[r][c] >> (d * 7 + 2)) & 0x7F;
				r += DR[d] * move;
				c += DC[d] * move;
				int f = field[r][c] & 3;
				if (f == WALL) break;
				if (usedIdx[r][c] != tryIdx) {
					usedIdx[r][c] = tryIdx;
					d ^= f;
					if (rcIdx[r] != tryIdx) {
						rcIdx[r] = tryIdx;
						rcBuf[r] = N - rc[r];
					}
					rcBuf[r]--;
					if (rcBuf[r] == 0) path.emptyLine++;
					if (ccIdx[c] != tryIdx) {
						ccIdx[c] = tryIdx;
						ccBuf[c] = N - cc[c];
					}
					ccBuf[c]--;
					if (ccBuf[c] == 0) path.emptyLine++;
					++path.count;
				}
			}
			lineC = (d % 2 == 0 ? ccBuf[c] : rcBuf[r]);
			path.odd += (lineC & 1) * 2 - 1;
			path.setScore();
			if (path.count <= 4) {
				visited[r][c] = boardIdx;
			}
			return path;
		}

		boolean apply(Path path) {
			int r = path.r;
			int c = path.c;
			moveR = r;
			moveC = c;
			int d;
			if (r == 0) {
				d = 0;
			} else if (c == 0) {
				d = 1;
			} else if (r == N + 1) {
				d = 2;
			} else {
				d = 3;
			}
			while (true) {
				int move = (field[r][c] >> (d * 7 + 2)) & 0x7F;
				r += DR[d] * move;
				c += DC[d] * move;
				int f = field[r][c] & 3;
				if (f == WALL) break;
				rc[r]++;
				cc[c]++;
				h ^= hash[r][c];
				d ^= f;
				int down = (field[r][c] >> 2) & 0x7F;
				int right = (field[r][c] >> 9) & 0x7F;
				int up = (field[r][c] >> 16) & 0x7F;
				int left = (field[r][c] >> 23) & 0x7F;
				field[r + down][c] = (field[r + down][c] & 0x3F80FFFF) + ((down + up) << 16);
				field[r][c + right] = (field[r][c + right] & 0x007FFFFF) + ((right + left) << 23);
				field[r - up][c] = (field[r - up][c] & 0x3FFFFE03) + ((down + up) << 2);
				field[r][c - left] = (field[r][c - left] & 0x3FFF01FF) + ((right + left) << 9);
				field[r][c] &= 0x3FFFFFFC;
			}
			if (used.contains(h)) {
				return false;
			}
			used.add(h);
			rest -= path.count;
			odd += path.odd;
			emptyLine += path.emptyLine;
			return true;
		}

	}

	void partition(ArrayList<Path> l, int c, int left, int right) {
		if (c <= 0 || right - left <= c || right - left <= 5) return;
		int pos = rnd.nextInt(right - left) + left;
		swap(l, left, pos);
		int begin = left + 1;
		int end = right - 1;
		final int pivot = l.get(left).score;
		while (begin <= end) {
			if (pivot < l.get(begin).score) {
				++begin;
			} else if (pivot == l.get(begin).score && rnd.nextBoolean()) {
				++begin;
			} else {
				swap(l, begin, end);
				--end;
			}
		}
		if (begin - left == c) return;
		if (begin - left < c) {
			partition(l, c - (begin - left), begin, right);
		} else {
			partition(l, c, left, begin);
		}
	}

	static <T> void swap(ArrayList<T> l, int i, int j) {
		T tmp = l.get(i);
		l.set(i, l.get(j));
		l.set(j, tmp);
	}

	static final class Path implements Comparable<Path> {
		int r, c;
		int count, emptyLine, odd, score;
		Board board;

		public Path(Board board) {
			this.board = board;
		}

		private void setScore() {
			score = (this.count - this.board.rest) + (this.board.emptyLine + this.emptyLine) * 50
					- (this.board.odd + this.odd) * 20;
		}

		public int compareTo(Path that) {
			return that.score - this.score;
		}
	}

	static void log(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void log(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		String[] board = new String[N];
		for (int i = 0; i < N; ++i) {
			board[i] = sc.next();
		}
		int[] ret = new FragileMirrors().destroy(board);
		System.out.println(ret.length);
		for (int i = 0; i < ret.length; ++i) {
			System.out.println(ret[i]);
		}
		System.out.flush();
	}

	static final class Timer {
		long[] sum = new long[9];
		long[] start = new long[sum.length];

		void start(int i) {
			if (FragileMirrors.MEASURE_TIME) {
				start[i] = System.currentTimeMillis();
			}
		}

		void end(int i) {
			if (FragileMirrors.MEASURE_TIME) {
				sum[i] += System.currentTimeMillis() - start[i];
			}
		}
	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		boolean nextBoolean() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return (w & 8) == 0;
		}

		double nextDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return Math.abs(1.0 * w / (1L << 31));
		}

	}
}
