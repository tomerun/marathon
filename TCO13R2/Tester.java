import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Cell {
	int r, c;

	public Cell(int r, int c) {
		this.r = r;
		this.c = c;
	}
}

class Board {
	final int[] DR = new int[] { -1, 0, 1, 0 };
	final int[] DC = new int[] { 0, 1, 0, -1 };
	final int[] LL = new int[] { 1, 0, 3, 2 };
	final int[] RR = new int[] { 3, 2, 1, 0 };

	int N;
	char[][] what;

	public Board(String[] board) {
		this.N = board.length;
		this.what = new char[N][];
		for (int i = 0; i < N; i++) {
			this.what[i] = board[i].toCharArray();
		}
	}

	ArrayList<Cell> shootRay(int r, int c, int d) {
		int curR = r;
		int curC = c;
		int curD = d;

		ArrayList<Cell> res = new ArrayList<Cell>();
		res.add(new Cell(curR, curC));
		curR += DR[curD];
		curC += DC[curD];

		while (curR >= 0 && curR < N && curC >= 0 && curC < N) {
			if (what[curR][curC] == 'L' || what[curR][curC] == 'R') {
				res.add(new Cell(curR, curC));
				curD = (what[curR][curC] == 'L' ? LL[curD] : RR[curD]);
				what[curR][curC] = (what[curR][curC] == 'L' ? 'l' : 'r');
			}
			curR += DR[curD];
			curC += DC[curD];
		}

		res.add(new Cell(curR, curC));
		return res;
	}

	void cleanCells(ArrayList<Cell> ray) {
		for (Cell cell : ray) {
			if (cell.r >= 0 && cell.r < N && cell.c >= 0 && cell.c < N) {
				what[cell.r][cell.c] = '~';
			}
		}
	}
}

class Drawer extends JFrame {
	public static final int EXTRA_WIDTH = 100;
	public static final int EXTRA_HEIGHT = 100;
	public int rayCnt;
	Object paintMutex = new Object();

	class DrawerPanel extends JPanel {
		public void paint(Graphics g) {
			synchronized (paintMutex) {
				g.setColor(Color.GRAY);
				for (int i = 0; i <= N; i++) {
					g.drawLine(30 + (i + 1) * cellSize, 30 + cellSize, 30 + (i + 1) * cellSize, 30 + cellSize * (N + 1));
					g.drawLine(30 + cellSize, 30 + (i + 1) * cellSize, 30 + cellSize * (N + 1), 30 + (i + 1) * cellSize);
				}

				g.setColor(Color.BLACK);
				for (int i = 0; i < N; i++) {
					for (int j = 0; j < N; j++) {
						if (field.what[i][j] == 'L' || field.what[i][j] == 'l') {
							g.drawLine(30 + (j + 2) * cellSize, 30 + (i + 1) * cellSize, 30 + (j + 1) * cellSize, 30 + (i + 2)
									* cellSize);
						}
						if (field.what[i][j] == 'R' || field.what[i][j] == 'r') {
							g.drawLine(30 + (j + 1) * cellSize, 30 + (i + 1) * cellSize, 30 + (j + 2) * cellSize, 30 + (i + 2)
									* cellSize);
						}
					}
				}

				g.setFont(new Font("Arial", Font.BOLD, 12));
				Graphics2D g2 = (Graphics2D) g;
				g2.drawString("Rays: " + rayCnt, 30 + cellSize, 20);

				if (ray != null) {
					g.setColor(Color.RED);
					for (int i = 0; i + 1 < ray.size(); i++) {
						int ySt = 30 + ray.get(i).r * cellSize + cellSize / 2 + cellSize;
						int xSt = 30 + ray.get(i).c * cellSize + cellSize / 2 + cellSize;
						int yFn = 30 + ray.get(i + 1).r * cellSize + cellSize / 2 + cellSize;
						int xFn = 30 + ray.get(i + 1).c * cellSize + cellSize / 2 + cellSize;
						g.drawLine(xSt, ySt, xFn, yFn);
						if (ray.get(i).c < ray.get(i + 1).c) {
							for (int d = 1; d <= arrowSize; d++) {
								g.drawLine(xFn - d, yFn - d, xFn - d, yFn + d);
							}
						} else if (ray.get(i).c > ray.get(i + 1).c) {
							for (int d = 1; d <= arrowSize; d++) {
								g.drawLine(xFn + d, yFn - d, xFn + d, yFn + d);
							}
						} else if (ray.get(i).r < ray.get(i + 1).r) {
							for (int d = 1; d <= arrowSize; d++) {
								g.drawLine(xFn - d, yFn - d, xFn + d, yFn - d);
							}
						} else if (ray.get(i).r > ray.get(i + 1).r) {
							for (int d = 1; d <= arrowSize; d++) {
								g.drawLine(xFn - d, yFn + d, xFn + d, yFn + d);
							}
						}
					}
				}
			}
		}
	}

	ArrayList<Cell> ray = null;
	final Object keyMutex = new Object();
	boolean keyPressed;
	public boolean pauseMode = false;

	class DrawerKeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			synchronized (keyMutex) {
				if (e.getKeyChar() == ' ') {
					pauseMode = !pauseMode;
				}
				keyPressed = true;
				keyMutex.notifyAll();
			}
		}
	}

	public void processPause() {
		synchronized (keyMutex) {
			if (!pauseMode) {
				return;
			}
			keyPressed = false;
			while (!keyPressed) {
				try {
					keyMutex.wait();
				} catch (InterruptedException e) {
					// do nothing
				}
			}
		}
	}

	DrawerPanel panel;
	int width, height;
	int N;
	Board field;
	int cellSize, arrowSize;

	public Drawer(int N, Board field, int cellSize, int arrowSize) {
		this.N = N;
		this.field = field;
		this.cellSize = cellSize;
		this.arrowSize = arrowSize;
		panel = new DrawerPanel();
		getContentPane().add(panel);
		width = cellSize * (N + 2) + EXTRA_WIDTH;
		height = cellSize * (N + 2) + EXTRA_HEIGHT;
		setSize(width, height);
		setTitle("Visualizer tool for problem FragileMirrors");
		addKeyListener(new DrawerKeyListener());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}
}

public class Tester {
	public static boolean vis = false;
	public static int cellSize = 8;
	public static int arrowSize = 2;
	public static int delay = 10;
	public static boolean startPaused = false;
	public static final int MIN_N = 50;
	public static final int MAX_N = 100;
	int N = -1;

	private ArrayList<Cell> shootRay(Board field, int r, int c) {
		if (r == -1 && c >= 0 && c < N) {
			return field.shootRay(r, c, 2);
		} else if (r == N && c >= 0 && c < N) {
			return field.shootRay(r, c, 0);
		} else if (c == -1 && r >= 0 && r < N) {
			return field.shootRay(r, c, 1);
		} else if (c == N && r >= 0 && r < N) {
			return field.shootRay(r, c, 3);
		} else {
			return null;
		}
	}

	public Result runTest(long seed) throws Exception {
		Random rnd = SecureRandom.getInstance("SHA1PRNG");
		rnd.setSeed(seed);
		if (N == -1) N = rnd.nextInt(MAX_N - MIN_N + 1) + MIN_N;
		if (1001 <= seed && seed <= 2000) {
			N = ((int) seed - 1) / 20 + 1;
		}
		String[] board = new String[N];
		for (int i = 0; i < N; i++) {
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < N; j++)
				sb.append(rnd.nextBoolean() ? 'L' : 'R');
			board[i] = sb.toString();
		}

		Board field = new Board(board);
		Result res = new Result();
		res.seed = seed;
		res.N = N;
		long starttime = System.currentTimeMillis();
		int[] ans = new FragileMirrors().destroy(board);
		res.elapsed = System.currentTimeMillis() - starttime;
		int ansLen = ans.length;
		if (ansLen % 2 == 1) {
			System.err.println("ERROR: the length of return value must be even. Your length = " + ansLen + ".");
			return res;
		}
		if (ansLen > 2 * N * N) {
			System.err.println("ERROR: the length of return value must be at most N*N. Your length = " + ansLen + ", N = "
					+ N + ".");
			return res;
		}

		Drawer drawer = null;
		if (vis) {
			drawer = new Drawer(N, field, cellSize, arrowSize);
			if (startPaused) {
				drawer.pauseMode = true;
			}
		}

		for (int i = 0; i < ans.length; i += 2) {
			int r = ans[i], c = ans[i + 1];
			ArrayList<Cell> ray;
			if (vis) {
				synchronized (drawer.paintMutex) {
					ray = shootRay(field, r, c);
					if (ray == null) {
						System.err.println("ERROR: ray " + i + " starts at an invalid point (row " + r + ", column " + c + ").");
						return res;
					}
				}
				drawer.rayCnt++;
				drawer.processPause();
				drawer.ray = ray;
				drawer.repaint();
				try {
					if (!drawer.pauseMode) {
						Thread.sleep(delay);
					}
				} catch (Exception e) {
					// do nothing
				}

				drawer.processPause();
				synchronized (drawer.paintMutex) {
					field.cleanCells(ray);
				}
				drawer.ray = null;
				drawer.repaint();
				try {
					if (!drawer.pauseMode) {
						Thread.sleep(delay);
					}
				} catch (Exception e) {
					// do nothing
				}
			} else {
				ray = shootRay(field, r, c);
				if (ray == null) {
					System.err.println("ERROR: ray " + i + " starts at an invalid point (row " + r + ", column " + c + ").");
					return res;
				}
				field.cleanCells(ray);
			}
		}

		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				if (field.what[r][c] != '~') {
					System.err.println("ERROR: all rays are shot, but cell at (row " + r + ", column " + c
							+ ") still contains a mirror.");
					return res;
				}
			}
		}

		res.score = 1. * N / (ans.length / 2);
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) {
		long seed = 1;
		long begin = -1;
		long end = -1;
		int N = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-begin")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-end")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-vis")) {
				vis = true;
			} else if (args[i].equals("-sz")) {
				cellSize = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-arrow")) {
				arrowSize = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-delay")) {
				delay = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-pause")) {
				startPaused = true;
			} else if (args[i].equals("-N")) {
				N = Integer.parseInt(args[++i]);
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}

		try {
			if (begin != -1 && end != -1) {
				ArrayList<Long> seeds = new ArrayList<Long>();
				for (long i = begin; i <= end; ++i) {
					seeds.add(i);
				}
				int len = seeds.size();
				Result[] results = new Result[len];
				TestThread[] threads = new TestThread[THREAD_COUNT];
				int prev = 0;
				for (int i = 0; i < THREAD_COUNT; ++i) {
					int next = len * (i + 1) / THREAD_COUNT;
					threads[i] = new TestThread(prev, next - 1, seeds, results);
					prev = next;
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].start();
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].join();
				}
				double sum = 0;
				for (int i = 0; i < results.length; ++i) {
					System.out.println(results[i]);
					System.out.println();
					sum += results[i].score;
				}
				System.out.println("ave:" + (sum / results.length));
			} else {
				Tester tester = new Tester();
				tester.N = N;
				System.out.println(tester.runTest(seed));
			}
		} catch (Exception e) {
			System.err.println("ERROR: Unexpected error while running your test case.");
			e.printStackTrace();
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int N;
		double score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d  N:%d\n", seed, N);
			return ret + "elapsed:" + this.elapsed / 1000.0 + "\nscore:  " + this.score;
		}
	}

}
