#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <set>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <sys/time.h>

 // #define MEASURE_TIME 1
 // #define DEBUG 1

//#define SUBMIT 1

#ifdef SUBMIT
#define CLOCK_PER_SEC 3.6e9
#else
#define CLOCK_PER_SEC 2.0e9
#endif

using namespace std;
typedef unsigned int uint;
typedef long long ll;
typedef unsigned long long ull;

#ifdef SUBMIT
const ll TIMELIMIT = 9800;
#else
const ll TIMELIMIT = 4000;
#endif

const int WALL = 2;
const int INVALID = -2;
const int DR[4] = {1, 0, -1, 0};
const int DC[4] = {0, 1, 0, -1};
const int MAX_RETAIN = 1000;

// msec
inline ll getTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ull get_tsc() {
#ifdef SUBMIT
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  return getTime();
#endif
}

struct XorShift {
	uint x,y,z,w;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint nextUInt(uint n) {
		uint t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint nextUInt() {
		uint t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}
};

struct Timer {
	ull at[10];
	double sum[10]; // msec

	Timer() { memset(sum, 0, sizeof(sum)); }

	void start(int i) {
#ifdef MEASURE_TIME
		at[i] = get_tsc();
#endif
	}

	void stop(int i) {
#ifdef MEASURE_TIME
#ifdef SUBMIT
		sum[i] += (get_tsc() - at[i]) / CLOCK_PER_SEC * 1000;
#else
		sum[i] += (get_tsc() - at[i]);
#endif
#endif
	}
};

ll starttime;
int N;
XorShift rnd;
Timer timer;
int bestLen;
int origBoard[102][102];
int ans[1024];
int rcBuf[102],ccBuf[102],rcIdx[102],ccIdx[102];
int visited[102][102];
ull hash[102][102];
int usedIdx[102][102];
vector<set<ull> > used;
int RETAIN;
int tryIdx, visitIdx, gmoveIdx;
int moveR[MAX_RETAIN * 200],moveC[MAX_RETAIN * 200],moveP[MAX_RETAIN * 200];

int fieldMem[2*MAX_RETAIN*102*102];
int* fieldRows[2*MAX_RETAIN*102];
int** fieldPool[2][MAX_RETAIN];

int oddScoreEf,emptyScoreEf;

struct Path {
	int r, c;
	int count, emptyLine, odd;
	int boardIdx;
};

Path pathAry[4 * 100 * MAX_RETAIN];
pair<int, int> pathScore[4 * 100 * MAX_RETAIN];
int pathIdx;

struct Board {
	int** field;
	vector<unsigned char> rc, cc;
	int rest, emptyLine, odd;
	ull h;
	int memIdx,moveIdx;

	Board();
	Board(Board& p, int t, int mIdx, bool move);

	void tryCast(int r, int c, int d);
	ull preApply(const Path& path);
	void apply(const Path& path);
};

Board::Board() : field(fieldPool[1][0]) {
	for (int i = 0; i <= N + 1; ++i) {
		for (int j = 0; j <= N + 1; ++j) {
			field[i][j] = origBoard[i][j] + (1 << 2) + (1 << 9) + (1 << 16) + (1 << 23);
		}
	}
	rc.resize(N + 2);
	cc.resize(N + 2);
	rest = N * N;
	emptyLine = odd = h = 0;
	memIdx = moveIdx = 0;
	++gmoveIdx;
	if (N % 2 == 1) odd = 2 * N;
}

Board::Board(Board& p, int t, int mIdx, bool move) :field(fieldPool[t][mIdx]) {
	if (move) {
		swap(this->field, p.field);
		swap(fieldPool[t][mIdx], fieldPool[1-t][p.memIdx]);
		swap(this->rc, p.rc);
		swap(this->cc, p.cc);
	} else {
		int prev = 0;
		for (int i = 1; i <= N; ++i) {
			if (p.rc[i] == N) {
				if (prev != i) {
					memcpy(field[prev], p.field[prev], (i - prev) * (N + 2) * sizeof(int));
				}
				prev = i+1;
			}
		}
		memcpy(field[prev], p.field[prev], (N + 2 - prev) * (N + 2) * sizeof(int));
		rc = p.rc;
		cc = p.cc;
	}
	rest = p.rest;
	emptyLine = p.emptyLine;
	odd = p.odd;
	h = p.h;
	memIdx = mIdx;
	moveIdx = gmoveIdx;
	moveP[moveIdx] = p.moveIdx;
}

void Board::tryCast(int r, int c, int d) {
	Path& path = pathAry[pathIdx];
	path.boardIdx = this->memIdx;
	path.r = r;
	path.c = c;
	path.count = 1;
	path.emptyLine = 0;
	int lineC = N - (d % 2 == 0 ? cc[c] : rc[r]);
	path.odd = -((lineC & 1) * 2 - 1);

	{
		int move = (field[r][c] >> (d * 7 + 2)) & 0x7F;
		r += DR[d] * move;
		c += DC[d] * move;
		int f = field[r][c] & 3;
		usedIdx[r][c] = tryIdx;
		if (d & 1) {
			rcIdx[r] = tryIdx;
			rcBuf[r] = N - rc[r] - 1;
			if (rcBuf[r] == 0) path.emptyLine++;
		} else {
			ccIdx[c] = tryIdx;
			ccBuf[c] = N - cc[c] - 1;
			if (ccBuf[c] == 0) path.emptyLine++;
		}
		d ^= f;
	}

	while (true) {
		int move = (field[r][c] >> (d * 7 + 2)) & 0x7F;
		r += DR[d] * move;
		c += DC[d] * move;
		int f = field[r][c] & 3;
		if (f == WALL) break;
		if (usedIdx[r][c] != tryIdx) {
			usedIdx[r][c] = tryIdx;
			if (d & 1) {
				if (rcIdx[r] != tryIdx) {
					rcIdx[r] = tryIdx;
					rcBuf[r] = N - rc[r];
				}
				rcBuf[r] -= 2;
				if (rcBuf[r] == 0) path.emptyLine++;
			} else {
				if (ccIdx[c] != tryIdx) {
					ccIdx[c] = tryIdx;
					ccBuf[c] = N - cc[c];
				}
				ccBuf[c] -= 2;
				if (ccBuf[c] == 0) path.emptyLine++;
			}
			d ^= f;
			++path.count;
		}
	}

	if (d & 1) {
		if (rcIdx[r] != tryIdx) {
			rcBuf[r] = N - rc[r];
		}
		rcBuf[r] -= 1;
		if (rcBuf[r] == 0) path.emptyLine++;
	} else {
		if (ccIdx[c] != tryIdx) {
			ccBuf[c] = N - cc[c];
		}
		ccBuf[c] -= 1;
		if (ccBuf[c] == 0) path.emptyLine++;
	}

	lineC = (d % 2 == 0 ? ccBuf[c] : rcBuf[r]);
	path.odd += (lineC & 1) * 2 - 1;
	pathScore[pathIdx].first = 
		(rest - path.count) - (path.emptyLine + emptyLine) * emptyScoreEf + (path.odd + odd) * oddScoreEf;
	pathScore[pathIdx].second = pathIdx;
	if (path.count <= 4) {
		visited[r][c] = visitIdx;
	}
	++tryIdx;
	++pathIdx;
}

ull Board::preApply(const Path& path) {
	int r = path.r;
	int c = path.c;
	int d;
	if (r == 0) {
		d = 0;
	} else if (c == 0) {
		d = 1;
	} else if (r == N + 1) {
		d = 2;
	} else {
		d = 3;
	}
	ull hv = h;
	while (true) {
		int move = (field[r][c] >> (d * 7 + 2)) & 0x7F;
		r += DR[d] * move;
		c += DC[d] * move;
		if (usedIdx[r][c] != tryIdx) {
			usedIdx[r][c] = tryIdx;
			int f = field[r][c] & 3;
			if (f == WALL) break;
			hv ^= hash[r][c];
			d ^= f;
		}
	}
	++tryIdx;
	return hv;
}
 
void Board::apply(const Path& path) {
	int r = path.r;
	int c = path.c;
	moveR[moveIdx] = r;
	moveC[moveIdx] = c;
	int d;
	if (r == 0) {
		d = 0;
	} else if (c == 0) {
		d = 1;
	} else if (r == N + 1) {
		d = 2;
	} else {
		d = 3;
	}
	while (true) {
		int move = (field[r][c] >> (d * 7 + 2)) & 0x7F;
		r += DR[d] * move;
		c += DC[d] * move;
		int f = field[r][c] & 3;
		if (f == WALL) break;
		rc[r]++;
		cc[c]++;
		d ^= f;
		int down = (field[r][c] >> 2) & 0x7F;
		int right = (field[r][c] >> 9) & 0x7F;
		int up = (field[r][c] >> 16) & 0x7F;
		int left = (field[r][c] >> 23) & 0x7F;
		field[r + down][c] = (field[r + down][c] & 0x3F80FFFF) + ((down + up) << 16);
		field[r][c + right] = (field[r][c + right] & 0x007FFFFF) + ((right + left) << 23);
		field[r - up][c] = (field[r - up][c] & 0x3FFFFE03) + ((down + up) << 2);
		field[r][c - left] = (field[r][c - left] & 0x3FFF01FF) + ((right + left) << 9);
		field[r][c] &= 0x3FFFFFFC;
	}
	rest -= path.count;
	odd += path.odd;
	emptyLine += path.emptyLine;
}

void finish(const Board& board, int turn) {
	int mi = board.moveIdx;
	int ind = turn;
	while (moveR[mi] != INVALID) {
		ans[ind * 2] = moveR[mi] - 1;
		ans[ind * 2 + 1] = moveC[mi] - 1;
		mi = moveP[mi];
		--ind;
	}
	bestLen = turn + 1;
#ifdef DEBUG
	cerr << "[" << bestLen << "]" << endl;
#endif DEBUG
}

void solve() {
	timer.start(0);
	used.clear();
	used.resize(N*N);
	gmoveIdx = 0;
	vector<Board*> boards;
	boards.push_back(new Board());
	int turn = 0;
	while (turn + 1 < bestLen && !boards.empty()) {
		if (bestLen < 5 * N && getTime() - starttime > TIMELIMIT) {
			break;
		}
		timer.start(1);
		RETAIN = min(MAX_RETAIN, (int)(1.015 * RETAIN));
		pathIdx = 0;
		for (size_t bi = 0; bi < boards.size(); ++bi, ++visitIdx) {
			Board& board = *(boards[bi]);
			bool alone = false;
			for (int i = 1; i <= N && !alone; ++i) {
				if (board.rc[i] != N - 1) continue;
				for (int j = 1; j <= N; ++j) {
					if ((board.field[i][j] & 3) != 0 && board.cc[j] == N - 1) {
						alone = true;
						board.tryCast(i, 0, 1);
						pathScore[pathIdx - 1].first += 100000000;
						break;
					}
				}
			}
			if (alone) continue;
			for (int i = 1; i <= N; ++i) {
				if (board.cc[i] != N) {
					if (visited[0][i] != visitIdx) {
						board.tryCast(0, i, 0);
					}
					if (visited[N + 1][i] != visitIdx) {
						board.tryCast(N + 1, i, 2);
					}
				}
				if (board.rc[i] != N) {
					if (visited[i][0] != visitIdx) {
						board.tryCast(i, 0, 1);
					}
					if (visited[i][N + 1] != visitIdx) {
						board.tryCast(i, N + 1, 3);
					}
				}
			}
		}
		timer.stop(1);

		timer.start(2);
		const int guard = RETAIN / 8;
		if (pathIdx > RETAIN * 3) {
			nth_element(pathScore, pathScore + RETAIN * 3, pathScore + pathIdx);
			nth_element(pathScore, pathScore + RETAIN, pathScore + RETAIN * 3);
		} else if(pathIdx > RETAIN) {
			nth_element(pathScore, pathScore + RETAIN, pathScore + pathIdx);
		}
		if (pathIdx > RETAIN * 2) {
			nth_element(pathScore, pathScore + guard, pathScore + RETAIN);
		}
		timer.stop(2);
		if (pathIdx > RETAIN * 2) {
			timer.start(3);
			int range = min(pathIdx - RETAIN, RETAIN * 3);
			for (int i = 0, size = max(RETAIN / 8, 2); i < size; ++i) {
				int from = rnd.nextUInt(RETAIN - guard) + guard;
				int to = rnd.nextUInt(range) + RETAIN;
				swap(pathScore[from], pathScore[to]);
			}
			timer.stop(3);
		}

		vector<int> bc(boards.size());
		const int maxBC = max(turn < N / 5 ? RETAIN / 20 : turn < N / 3 ? RETAIN / 12 : turn < N * 2 / 3 ? RETAIN / 6 : RETAIN / 2, 3);
		vector<Board*> next;
		next.reserve(RETAIN);
		vector<pair<int, ull> > delayPathIdx;
		for (int i = 0; i < pathIdx && next.size() + delayPathIdx.size() < RETAIN; ++i) {
			Path& p = pathAry[pathScore[i].second];
			Board* pb = boards[p.boardIdx];
			if (turn != 0 && bc[pb->memIdx] == maxBC) continue;
			if (turn + 1 + (pb->odd + p.odd) / 2 >= bestLen) continue;
			timer.start(6);
			ull newHash = pb->preApply(p);
			timer.stop(6);
			timer.start(7);
			set<ull>& usedSet = used[pb->rest - p.count];
			if (usedSet.find(newHash) != usedSet.end()) {
				timer.stop(7);
				continue;
			}
			usedSet.insert(newHash);
			timer.stop(7);
			bc[pb->memIdx]++;
			if (bc[pb->memIdx] == 1) {
				delayPathIdx.push_back(make_pair(pathScore[i].second, newHash));
			} else {
				timer.start(4);
				Board* nb = new Board(*pb, turn & 1, next.size(), false);
				timer.stop(4);
				timer.start(5);
				nb->h = newHash;
				nb->apply(p);
				timer.stop(5);
				if (nb->rest == 0) {
					finish(*nb, turn);
					timer.stop(0);
					delete nb;
					for (int j = 0; j < boards.size(); ++j) {
						delete boards[j];
					}
					return;
				}
				gmoveIdx++;
				next.push_back(nb);
			}
		}
		for (int i = 0; i < delayPathIdx.size(); ++i) {
			Path& p = pathAry[delayPathIdx[i].first];
			Board* pb = boards[p.boardIdx];
			timer.start(4);
			Board* nb = new Board(*pb, turn & 1, next.size(), true);
			timer.stop(4);
			timer.start(5);
			nb->h = delayPathIdx[i].second;
			nb->apply(p);
			timer.stop(5);
			if (nb->rest == 0) {
				finish(*nb, turn);
				timer.stop(0);
				delete nb;
				for (int j = 0; j < boards.size(); ++j) {
					delete boards[j];
				}
				return;
			}
			gmoveIdx++;
			next.push_back(nb);
		}
		for (int i = 0; i < boards.size(); ++i) {
			delete boards[i];
		}
		boards = next;
		++turn;
#ifdef DEBUG
		// cerr << turn << " " << bestLen << " " << boards.size() << " " << pathIdx << endl;
#endif
	}
	timer.stop(0);
}

class FragileMirrors {
public:
	FragileMirrors();
	vector<int> destroy(const vector<string>& board);
};

FragileMirrors::FragileMirrors() {
	starttime = getTime();
}

vector<int> FragileMirrors::destroy(const vector<string>& b) {
	timer.start(8);
	N = b.size();
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			origBoard[i+1][j+1] = b[i][j] == 'L' ? 3 : 1;
		}
	}
	for (int i = 0; i < N + 2; ++i) {
		origBoard[0][i] = origBoard[N+1][i] = origBoard[i][0] = origBoard[i][N+1] = WALL;
	}
	int* assignFrom = fieldMem;
	int** assignTo = fieldRows;
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < MAX_RETAIN; ++j) {
			fieldPool[i][j] =assignTo;
			for (int k = 0; k < N+2; ++k, assignFrom += N+2, ++assignTo) {
				*assignTo = assignFrom;
			}
		}
	}
	bestLen = 5 * N;
	tryIdx = visitIdx = 1;
	if (N <= 54) {
		RETAIN = 950;
		// RETAIN = 800;
	} else if (N <= 60) {
		RETAIN = 900;
		// RETAIN = 650;
	} else if (N <= 70) {
		RETAIN = 700;
		// RETAIN = 500;
	} else if (N <= 80) {
		RETAIN = 500;
		// RETAIN = 350;
	} else if (N <= 90) {
		RETAIN = 450;
		// RETAIN = 300;
	} else {
		RETAIN = 350;
		// RETAIN = 250;
	}
	for (int i = 1; i <= N; ++i) {
		for (int j = 1; j <= N; ++j) {
			hash[i][j] = ((ull) rnd.nextUInt() << 32) | rnd.nextUInt();
		}
	}
	moveR[0] = moveC[0] = INVALID;
	emptyScoreEf = N * 2 / 3;;
	oddScoreEf = N % 2 == 0 ? N * 2 / 15 : N * 8 / 15;
	timer.stop(8);

	ll before = getTime() - starttime;
	while(true) {
#ifdef DEBUG
		cerr << "retain:" << RETAIN << endl;
#endif
		int prevRetain = RETAIN;
		solve();
		RETAIN = prevRetain;
		ll after = getTime() - starttime;
#ifdef DEBUG
		cerr << "time:" << after << endl;
#endif
		if (after > TIMELIMIT) break;
		if (after - before > TIMELIMIT - after) {
			RETAIN = (int) (0.98 * RETAIN * (TIMELIMIT - after) / (after - before));
			if (RETAIN < 10) RETAIN = 10;
		}
		before = after;
	}

	vector<int> ret(ans, ans + bestLen * 2);
#ifdef DEBUG
	// for (int i = 0; i < bestLen; ++i) {
	// 	cerr << "[" << ret[i*2] << "," << ret[i*2+1] << "]" << endl;
	// }
#ifdef MEASURE_TIME
	cerr << "timer: ";
	for (int i = 0; i < 10; ++i) {
		cerr << timer.sum[i] << ", ";
	}
	cerr << endl;
#endif	
#endif
	return ret;
}


// int main() {
// 	int n;
// 	cin >> n;
// 	vector<string> board;
// 	for (int i = 0; i < n; ++i) {
// 		string line;
// 		cin >> line;
// 		board.push_back(line);
// 	}
// 	FragileMirrors obj;
// 	vector<int> ret = obj.destroy(board);
// 	cout << ret.size() << endl;
// 	for (int i = 0; i < ret.size(); ++i) {
// 		cout << ret[i] << endl;
// 	}
// 	cout.flush();
// }


