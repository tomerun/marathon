#!/bin/sh

for (( i = 1; i <= 10; i++ )); do
	seed=$i
	rm tmp/*.png
	java Tester -seed $seed 2> log.txt
	java CreateImage < log.txt
	rm palette.png
	ffmpeg -i tmp/%d.png -vf palettegen palette.png
	rm seed"$seed".gif
	ffmpeg -f image2 -r 5 -i tmp/%d.png -i palette.png -filter_complex paletteuse seed"$seed".gif
done
