import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class CreateImage {
	static int N, H, W;

	public static void main(String[] args) throws Exception {
		try (Scanner sc = new Scanner(System.in)) {
			for (int i = 0; sc.hasNextInt(); i++) {
				draw(sc, i);
			}
		}
	}

	static void draw(Scanner sc, int idx) throws IOException {
		H = sc.nextInt();
		W = sc.nextInt();
		N = sc.nextInt();
		int[] colors = new int[N];
		int[] r = new int[N];
		int[] c = new int[N];
		for (int i = 0; i < N; i++) {
			r[i] = sc.nextInt();
			c[i] = sc.nextInt();
			colors[i] = sc.nextInt();
		}
		BufferedImage image = new BufferedImage(W, H, BufferedImage.TYPE_INT_RGB);
		for (int i = 0; i < H; i++) {
			for (int j = 0; j < W; j++) {
				int group = sc.nextInt();
				image.setRGB(j, i, colors[group]);
			}
		}
		for (int i = 0; i < N; i++) {
			if (0 <= r[i] && r[i] < H && 0 <= c[i] && c[i] < W) {
				image.setRGB(c[i], r[i], Color.BLACK.getRGB());
			}
		}
		ImageIO.write(image, "png", new File("tmp/" + idx + ".png"));
	}
}
