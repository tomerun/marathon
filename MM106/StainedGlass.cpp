#include <algorithm>
#include <utility>
#include <vector>
#include <bitset>
#include <string>
#include <queue>
#include <unordered_map>
#include <set>
#include <iostream>
#include <array>
#include <tuple>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifndef LOCAL
#define NDEBUG
#define DEBUG
#define MEASURE_TIME
#else
// #define MEASURE_TIME
// #define DEBUG
#endif
#include <cassert>

using namespace std;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t i64;
typedef int64_t ll;
typedef uint64_t ull;
typedef vector<int> vi;
typedef vector<vi> vvi;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 18000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 19500;
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

struct Point {
	int r, c;
};

struct Seed {
	int r, c, color, size;
};

struct Result {
	vector<Seed> seeds;
	int raw;
	double score;
};

struct MergeState {
	int from, to, size_from, size_to, diff, new_color;

	bool operator<(const MergeState& o) const {
		return diff > o.diff;
	}
};

struct IndexSet {
	vi elements;
	vi used;

	IndexSet(int size) : used(size, -1) {}

	void add(int e) {
		if (used[e] == -1) {
			used[e] = elements.size();
			elements.push_back(e);
		}
	}

	void clear() {
		for (int e : elements) {
			used[e] = -1;
		}
		elements.clear();
	}

	bool contains(int e) const {
		return used[e] != -1;
	}
};

struct Frontier {
	vvi edges;
	vvi pos;

	void set_size(int n, int h, int w) {
		edges.resize(n);
		pos.assign(h, vi(w, -1));
	}

	void add(int r, int c, int id) {
		if (pos[r][c] != -1) return;
		pos[r][c] = (id << 20) | edges[id].size();
		edges[id].push_back((r << 10) | c);
	}

	void remove(int r, int c) {
		if (pos[r][c] == -1) return;
		int id = pos[r][c] >> 20;
		int p = pos[r][c] & 0xFFFFF;
		int other = edges[id].back();
		swap(edges[id][p], edges[id].back());
		edges[id].pop_back();
		pos[other >> 10][other & 0x3FF] = pos[r][c];
		pos[r][c] = -1;
	}

	void clear() {
		for (vi& edge : edges) {
			for (int p : edge) {
				pos[p >> 10][p & 0x3FF] = -1;
			}
			edge.clear();
		}
	}
} frontier;

const int DR[] = {-1, 0, 1, 0, -1, 0, 1, 0};
const int DC[] = {0, 1, 0, -1, 0, 1, 0, -1};
const int MARGIN = 100;
const double INITIAL_COOLER = 0.001;
const double FINAL_COOLER = 0.01;
int N, H, W;
array<array<int, 840>, 840> target;
array<array<int, 840>, 840> precalc_median;
array<array<int, 840>, 840> area, best_area;
array<array<int, 840>, 840> evaluate_history;
int evaluate_history_idx;
vector<vector<bool>> used;
vector<vector<Point>> adj_pos;
vector<double> adj_pos_dist;
double sa_cooler = INITIAL_COOLER;

struct World {
	vector<Seed> seeds;
	vector<array<int, 256>> histoR;
	vector<array<int, 256>> histoG;
	vector<array<int, 256>> histoB;
	int raw;

	World() : seeds(N), histoR(N), histoG(N), histoB(N), raw(0) {}
};

inline bool accept(int diff) {
	if (diff <= 0) return true;
	return rnd.nextDouble() < exp(-diff * sa_cooler);
}

inline int diff_color(int color1, int color2) {
	__m64 m1 = _m_from_int(color1);
	__m64 m2 = _m_from_int(color2);
	__m64 sum = _mm_sad_pu8(m1, m2);
	return _m_to_int(sum);
}

pair<int, int> median(const array<int, 256>& histo, int size) {
	int med = 255;
	int count = 0;
	int sum = 0;
	int diff = 0;
	int i = 0;
	for (; i <= 255; ++i) {
		count += histo[i];
		sum += histo[i] * i;
		if (count >= (size + 1) / 2) {
			med = i;
			diff += i * count - sum - i * (size - count);
			break;
		}
	}
	++i;
	for (; i <= 255; ++i) {
		diff += histo[i] * i;
	}
	return make_pair(med, diff);
}

void change_area(World& world, int r, int c, int new_area) {
	frontier.remove(r, c);
	area[r][c] = new_area;
	bool any = false;
	if (r > 0 && area[r - 1][c] != new_area) {
		frontier.add(r - 1, c, area[r - 1][c]);
		any = true;
	}
	if (r < H - 1 && area[r + 1][c] != new_area) {
		frontier.add(r + 1, c, area[r + 1][c]);
		any = true;
	}
	if (c > 0 && area[r][c - 1] != new_area) {
		frontier.add(r, c - 1, area[r][c - 1]);
		any = true;
	}
	if (c < W - 1 && area[r][c + 1] != new_area) {
		frontier.add(r, c + 1, area[r][c + 1]);
		any = true;
	}
	if (any) {
		frontier.add(r, c, new_area);
	}
}

void output_area(World& world) {
	fprintf(stderr, "%d %d %d\n", H, W, N);
	for (int i = 0 ; i < N; ++i) {
		fprintf(stderr, "%d %d %d\n", world.seeds[i].r, world.seeds[i].c, world.seeds[i].color);
	}
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			fprintf(stderr, "%d ", area[i][j]);
		}
		fprintf(stderr, "\n");
	}
}

const int BUCKET_SIZE_BITS = 6;
const int BUCKET_SIZE = 1 << BUCKET_SIZE_BITS;
array<array<vi, 1024 / BUCKET_SIZE>, 1024 / BUCKET_SIZE> bucket;

template<bool with_color>
void voronoi_strict(World& world) {
	START_TIMER(10);
	frontier.clear();
	for (int i = 0; i < N; ++i) {
		world.histoR[i].fill(0);
		world.histoG[i].fill(0);
		world.histoB[i].fill(0);
		world.seeds[i].size = 0;
	}
	world.raw = 0;
	for (int i = 0; i < H; ++i) {
		fill(area[i].begin(), area[i].begin() + W, -1);
	}
	STOP_TIMER(10);
	START_TIMER(11);
	int count = 0;
	for (int i = 0; i < adj_pos.size() && count < H * W; ++i) {
		for (int j = 0; j < N; ++j) {
			Seed& s = world.seeds[j];
			for (Point& diff : adj_pos[i]) {
				int r = s.r + diff.r;
				int c = s.c + diff.c;
				if (0 <= r && r < H && 0 <= c && c < W && area[r][c] == -1) {
					area[r][c] = j;
					s.size++;
					++count;
				}
			}
		}
	}
	// debug("count:%d/%d\n", count, H * W);
	STOP_TIMER(11);
	START_TIMER(12);
	if (count < H * W) {
		for (auto& bucket_row : bucket) {
			for (auto& bucket_cell : bucket_row) {
				bucket_cell.clear();
			}
		}
		for (int i = 0; i < N; ++i) {
			int bucket_r = (world.seeds[i].r + MARGIN) >> BUCKET_SIZE_BITS;
			int bucket_c = (world.seeds[i].c + MARGIN) >> BUCKET_SIZE_BITS;
			bucket[bucket_r][bucket_c].push_back(i);
		}
	}
	for (int r = 0; r < H; ++r) {
		const int bucket_r = (r + MARGIN) >> BUCKET_SIZE_BITS;
		const int from_r = bucket_r == 0 ? 0 : bucket_r - 1;
		const int to_r = bucket_r == (H + MARGIN * 2 - 1) >> BUCKET_SIZE_BITS ? bucket_r : bucket_r + 1;
		const int min_dist_r = min(r + MARGIN - (bucket_r << BUCKET_SIZE_BITS), ((bucket_r + 1) << BUCKET_SIZE_BITS) - 1 - r - MARGIN);
		for (int c = 0; c < W; ++c) {
			ADD_COUNTER(3);
			if (area[r][c] == -1) {
				ADD_COUNTER(4);
				int min_d = 1 << 20;
				int a = -1;
				const int bucket_c = (c + MARGIN) >> BUCKET_SIZE_BITS;
				const int from_c = bucket_c == 0 ? 0 : bucket_c - 1;
				const int to_c = bucket_c == (W + MARGIN * 2 - 1) >> BUCKET_SIZE_BITS ? bucket_c : bucket_c + 1;
				const int min_dist_c = min(c + MARGIN - (bucket_c << BUCKET_SIZE_BITS), ((bucket_c + 1) << BUCKET_SIZE_BITS) - 1 - c - MARGIN);
				int min_dist = min(min_dist_r, min_dist_c) + BUCKET_SIZE;
				for (int br = from_r; br <= to_r; ++br) {
					for (int bc = from_c; bc <= to_c; ++bc) {
						for (int aa : bucket[br][bc]) {
							int d = sq(world.seeds[aa].r - r) + sq(world.seeds[aa].c - c);
							if (d < min_d || (d == min_d && aa < a)) {
								min_d = d;
								a = aa;
							}
						}
					}
				}
				// if (min_d > min_dist * min_dist) {
				// 	ADD_COUNTER(5);
				// 	for (int br = from_r - 1; br <= to_r + 1; ++br) {
				// 		if (br < 0 || br > ((H + MARGIN * 2 - 1) >> BUCKET_SIZE_BITS)) continue;
				// 		for (int bc = from_c - 1; bc <= to_c + 1; ++bc) {
				// 			if (bc < 0 || bc > ((W + MARGIN * 2 - 1) >> BUCKET_SIZE_BITS)) continue;
				// 			if (max(abs(br - bucket_r), abs(bc - bucket_c)) != 2) continue; 
				// 			for (int aa : bucket[br][bc]) {
				// 				int d = sq(world.seeds[aa].r - r) + sq(world.seeds[aa].c - c);
				// 				if (d < min_d || (d == min_d && aa < a)) {
				// 					min_d = d;
				// 					a = aa;
				// 				}
				// 			}
				// 		}
				// 	}
				// 	min_dist += BUCKET_SIZE;
				// }
				if (min_d > min_dist * min_dist) {
					ADD_COUNTER(6);
					for (int i = 0; i < world.seeds.size(); ++i) {
						int d = sq(world.seeds[i].r - r) + sq(world.seeds[i].c - c);
						if (d < min_d) {
							min_d = d;
							a = i;
						}
					}
				}
				area[r][c] = a;
				world.seeds[area[r][c]].size++;
			}
			world.histoR[area[r][c]][target[r][c] >> 16]++;
			world.histoG[area[r][c]][(target[r][c] >> 8) & 0xFF]++;
			world.histoB[area[r][c]][target[r][c] & 0xFF]++;
			if (r > 0 && area[r][c] != area[r - 1][c]) {
				frontier.add(r, c, area[r][c]);
				frontier.add(r - 1, c, area[r - 1][c]);
			}
			if (c > 0 && area[r][c] != area[r][c - 1]) {
				frontier.add(r, c, area[r][c]);
				frontier.add(r, c - 1, area[r][c - 1]);
			}
		}
	}
	STOP_TIMER(12);
	START_TIMER(13);
	if (with_color) {
		for (int i = 0; i < N; ++i) {
			if (world.seeds[i].size > 0) {
				pair<int, int> r = median(world.histoR[i], world.seeds[i].size);
				pair<int, int> g = median(world.histoG[i], world.seeds[i].size);
				pair<int, int> b = median(world.histoB[i], world.seeds[i].size);
				world.seeds[i].color = (r.first << 16) | (g.first << 8) | b.first;
				world.raw += r.second + g.second + b.second;
			} else {
				int r = world.seeds[i].r;
				int c = world.seeds[i].c;
				if (r < 0) r = 0;
				if (r >= H) r = H - 1;
				if (c < 0) c = 0;
				if (c >= W) c = W - 1;
				world.seeds[i].color = target[r][c];
			}
		}
	} else {
		for (int i = 0; i < N; ++i) {
			int r = world.seeds[i].color >> 16;
			int g = (world.seeds[i].color >> 8) & 0xFF;
			int b = world.seeds[i].color & 0xFF;
			for (int j = 0; j < 256; ++j) {
				world.raw += abs(r - j) * world.histoR[i][j];
				world.raw += abs(g - j) * world.histoG[i][j];
				world.raw += abs(b - j) * world.histoB[i][j];
			}
		}
	}
	STOP_TIMER(13);
}

void fix_color(World& world) {
	START_TIMER(16);
	for (int i = 0; i < N; ++i) {
		world.histoR[i].fill(0);
		world.histoG[i].fill(0);
		world.histoB[i].fill(0);
		world.seeds[i].size = 0;
	}
	world.raw = 0;
	for (int r = 0; r < H; ++r) {
		for (int c = 0; c < W; ++c) {
			world.histoR[area[r][c]][target[r][c] >> 16]++;
			world.histoG[area[r][c]][(target[r][c] >> 8) & 0xFF]++;
			world.histoB[area[r][c]][target[r][c] & 0xFF]++;
			world.seeds[area[r][c]].size++;
		}
	}
	for (int i = 0; i < N; ++i) {
		if (world.seeds[i].size > 0) {
			pair<int, int> r = median(world.histoR[i], world.seeds[i].size);
			pair<int, int> g = median(world.histoG[i], world.seeds[i].size);
			pair<int, int> b = median(world.histoB[i], world.seeds[i].size);
			world.seeds[i].color = (r.first << 16) | (g.first << 8) | b.first;
			world.raw += r.second + g.second + b.second;
		} else {
			int r = world.seeds[i].r;
			int c = world.seeds[i].c;
			if (r < 0) r = 0;
			if (r >= H) r = H - 1;
			if (c < 0) c = 0;
			if (c >= W) c = W - 1;
			world.seeds[i].color = target[r][c];
		}
	}
	STOP_TIMER(16);
}

void validate(World& world) {
	for (int i = 0; i < N; ++i) {
		world.histoR[i].fill(0);
		world.histoG[i].fill(0);
		world.histoB[i].fill(0);
	}
	int sum = 0;
	for (int r = 0; r < H; ++r) {
		for (int c = 0; c < W; ++c) {
			world.histoR[area[r][c]][target[r][c] >> 16]++;
			world.histoG[area[r][c]][(target[r][c] >> 8) & 0xFF]++;
			world.histoB[area[r][c]][target[r][c] & 0xFF]++;
		}
	}
	for (int i = 0; i < N; ++i) {
		int r = world.seeds[i].color >> 16;
		int g = (world.seeds[i].color >> 8) & 0xFF;
		int b = world.seeds[i].color & 0xFF;
		for (int j = 0; j < 256; ++j) {
			sum += abs(r - j) * world.histoR[i][j];
			sum += abs(g - j) * world.histoG[i][j];
			sum += abs(b - j) * world.histoB[i][j];
		}
	}
	debug("expect:%d actual:%d\n", world.raw, sum);
}

array<int, 1000> area_count, area_count_diff;
array<int, 1000> area_sumR, area_sumG, area_sumB;
array<int, 1000> area_sum2R, area_sum2G, area_sum2B;
array<int, 1000> area_sumR_diff, area_sumG_diff, area_sumB_diff;
array<int, 1000> area_sum2R_diff, area_sum2G_diff, area_sum2B_diff;

double recalc_var_score() {
	fill(area_count.begin(), area_count.begin() + N, 0);
	fill(area_sumR.begin(), area_sumR.begin() + N, 0);
	fill(area_sumG.begin(), area_sumG.begin() + N, 0);
	fill(area_sumB.begin(), area_sumB.begin() + N, 0);
	fill(area_sum2R.begin(), area_sum2R.begin() + N, 0);
	fill(area_sum2G.begin(), area_sum2G.begin() + N, 0);
	fill(area_sum2B.begin(), area_sum2B.begin() + N, 0);
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			int ai = area[i][j];
			int r = target[i][j] >> 16;
			int g = (target[i][j] >> 8) & 0xFF;
			int b = target[i][j] & 0xFF;
			area_count[ai]++;
			area_sumR[ai] += r;
			area_sumG[ai] += g;
			area_sumB[ai] += b;
			area_sum2R[ai] += r * r;
			area_sum2G[ai] += g * g;
			area_sum2B[ai] += b * b;
		}
	}
	double ret = 0;
	for (int i = 0; i < N; ++i) {
		double inv_count = 1.0 / area_count[i];
		double varR = sqrt(area_sum2R[i] * inv_count - sq(area_sumR[i] * inv_count));
		double varG = sqrt(area_sum2G[i] * inv_count - sq(area_sumG[i] * inv_count));
		double varB = sqrt(area_sum2B[i] * inv_count - sq(area_sumB[i] * inv_count));
		ret += (varR + varG + varB) * area_count[i];
	}
	return ret;
}

void improve_large(World& world, long time_limit) {
	debug("raw:%d\n", world.raw);
	vi changed_pos, got_pos;
	IndexSet adj_area(N);
	double score = recalc_var_score();
	// output_area(world);
	for (int turn = 1; ; ++turn) {
		if ((turn & 0x3FF) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed > time_limit) {
				debug("turn:%d raw:%d\n", turn, world.raw);
				return;
			}
			if ((turn & 0x3FF) == 0) {
				// for (int i = 0; i < N; ++i) {
				// 	world.seeds[i].color = precalc_median[world.seeds[i].r][world.seeds[i].c];
				// }
				// output_area(world);
				// int old_raw = world.raw;
				// voronoi_strict<true>(world);
				// score = recalc_var_score();
				// debug("turn:%d raw:%d->%d score:%f\n", turn, old_raw, world.raw, score);
				// debug("turn:%d score:%f\n", turn, score);
			}
		}
		const int ai = rnd.nextUInt(N);
		int nr = -1;
		int nc = -1;
		int max_diff = -1;
		for (int i = 0; i < 20 || nr == -1; ++i) {
			int cr = rnd.nextUInt(H);
			int cc = rnd.nextUInt(W);
			while (used[cr + MARGIN][cc + MARGIN]) {
				cr = rnd.nextUInt(H);
				cc = rnd.nextUInt(W);
			}
			int area_r = world.seeds[area[cr][cc]].r;
			int area_c = world.seeds[area[cr][cc]].c;
			int area_color = precalc_median[area_r][area_c];
			int here_color = target[cr][cc];
			int diff = diff_color(area_color, here_color);
			if (diff > max_diff) {
				max_diff = diff;
				nr = cr;
				nc = cc;
			}
		}
		const int old_r = world.seeds[ai].r;
		const int old_c = world.seeds[ai].c;
		changed_pos.clear();
		changed_pos.push_back((old_r << 10) | old_c);
		adj_area.clear();
		++evaluate_history_idx;
		evaluate_history[old_r][old_c] = evaluate_history_idx;
		START_TIMER(20);
		for (int i = 0; i < changed_pos.size(); ++i) {
			int cr = changed_pos[i] >> 10;
			int cc = changed_pos[i] & 0x3FF;
			if (cr != 0 && evaluate_history[cr - 1][cc] != evaluate_history_idx) {
				evaluate_history[cr - 1][cc] = evaluate_history_idx;
				if (area[cr - 1][cc] != ai) {
					adj_area.add(area[cr - 1][cc]);
				} else {
					changed_pos.push_back(changed_pos[i] - (1 << 10));
				}
			}
			if (cc != 0 && evaluate_history[cr][cc - 1] != evaluate_history_idx) {
				evaluate_history[cr][cc - 1] = evaluate_history_idx;
				if (area[cr][cc - 1] != ai) {
					adj_area.add(area[cr][cc - 1]);
				} else {
					changed_pos.push_back(changed_pos[i] - 1);
				}
			}
			if (cc != W - 1 && evaluate_history[cr][cc + 1] != evaluate_history_idx) {
				evaluate_history[cr][cc + 1] = evaluate_history_idx;
				if (area[cr][cc + 1] != ai) {
					adj_area.add(area[cr][cc + 1]);
				} else {
					changed_pos.push_back(changed_pos[i] + 1);
				}
			}
			if (cr != H - 1 && evaluate_history[cr + 1][cc] != evaluate_history_idx) {
				evaluate_history[cr + 1][cc] = evaluate_history_idx;
				if (area[cr + 1][cc] != ai) {
					adj_area.add(area[cr + 1][cc]);
				} else {
					changed_pos.push_back(changed_pos[i] + (1 << 10));
				}
			}
		}
		area_count_diff[ai] = -area_count[ai];
		area_sumR_diff[ai] = -area_sumR[ai];
		area_sumG_diff[ai] = -area_sumG[ai];
		area_sumB_diff[ai] = -area_sumB[ai];
		area_sum2R_diff[ai] = -area_sum2R[ai];
		area_sum2G_diff[ai] = -area_sum2G[ai];
		area_sum2B_diff[ai] = -area_sum2B[ai];
		for (int aa : adj_area.elements) {
			area_count_diff[aa] = 0;
			area_sumR_diff[aa] = area_sumG_diff[aa] = area_sumB_diff[aa] = 0;
			area_sum2R_diff[aa] = area_sum2G_diff[aa] = area_sum2B_diff[aa] = 0;
		}
		STOP_TIMER(20);
		START_TIMER(21);
		for (int i = 0; i < changed_pos.size(); ++i) {
			int cr = changed_pos[i] >> 10;
			int cc = changed_pos[i] & 0x3FF;
			int min_d = 1 << 20;
			int new_area = ai;
			for (int ca : adj_area.elements) {
				int d = sq(cr - world.seeds[ca].r) + sq(cc - world.seeds[ca].c);
				if (d < min_d || (d == min_d && ca < area[cr][cc])) {
					new_area = ca;
					min_d = d;
				}
			}
			int red = target[cr][cc] >> 16;
			int green = (target[cr][cc] >> 8) & 0xFF;
			int blue = target[cr][cc] & 0xFF;
			area_count_diff[new_area]++;
			area_sumR_diff[new_area] += red;
			area_sum2R_diff[new_area] += red * red;
			area_sumG_diff[new_area] += green;
			area_sum2G_diff[new_area] += green * green;
			area_sumB_diff[new_area] += blue;
			area_sum2B_diff[new_area] += blue * blue;
			changed_pos[i] |= new_area << 20;
		}
		STOP_TIMER(21);
		START_TIMER(22);
		got_pos.clear();
		got_pos.push_back((nr << 10) | nc);
		++evaluate_history_idx;
		evaluate_history[nr][nc] = evaluate_history_idx;
		adj_area.add(ai);
		{
			int old_area = area[nr][nc];
			if (!adj_area.contains(old_area)) {
				adj_area.add(old_area);
				area_count_diff[old_area] = 0;
				area_sumR_diff[old_area] = area_sumG_diff[old_area] = area_sumB_diff[old_area] = 0;
				area_sum2R_diff[old_area] = area_sum2G_diff[old_area] = area_sum2B_diff[old_area] = 0;
			}
		}
		for (int i = 0; i < got_pos.size(); ++i) {
			int cr = got_pos[i] >> 10;
			int cc = got_pos[i] & 0x3FF;
			int old_area = area[cr][cc];
			int red = target[cr][cc] >> 16;
			int green = (target[cr][cc] >> 8) & 0xFF;
			int blue = target[cr][cc] & 0xFF;
			area_count_diff[ai]++;
			area_sumR_diff[ai] += red;
			area_sum2R_diff[ai] += red * red;
			area_sumG_diff[ai] += green;
			area_sum2G_diff[ai] += green * green;
			area_sumB_diff[ai] += blue;
			area_sum2B_diff[ai] += blue * blue;
			area_count_diff[old_area]--;
			area_sumR_diff[old_area] -= red;
			area_sum2R_diff[old_area] -= red * red;
			area_sumG_diff[old_area] -= green;
			area_sum2G_diff[old_area] -= green * green;
			area_sumB_diff[old_area] -= blue;
			area_sum2B_diff[old_area] -= blue * blue;
			for (int j = 0; j < 4; ++j) {
				int ar = cr + DR[j];
				int ac = cc + DC[j];
				if (ar == -1 || ar == H || ac == -1 || ac == W || evaluate_history[ar][ac] == evaluate_history_idx) continue;
				evaluate_history[ar][ac] = evaluate_history_idx;
				int ad = sq(ar - nr) + sq(ac - nc);
				int old_area = area[ar][ac];
				int od = sq(ar - world.seeds[old_area].r) + sq(ac - world.seeds[old_area].c);
				if (ad < od || (ad == od && ai < old_area)) {
					if (!adj_area.contains(old_area)) {
						adj_area.add(old_area);
						area_count_diff[old_area] = 0;
						area_sumR_diff[old_area] = area_sumG_diff[old_area] = area_sumB_diff[old_area] = 0;
						area_sum2R_diff[old_area] = area_sum2G_diff[old_area] = area_sum2B_diff[old_area] = 0;
					}
					got_pos.push_back((ar << 10) | ac);
				}
			}
		}
		STOP_TIMER(22);
		double diff = 0;
		for (int aa : adj_area.elements) {
			double inv_count = 1.0 / area_count[aa];
			double varR = sqrt(area_sum2R[aa] * inv_count - sq(area_sumR[aa] * inv_count));
			double varG = sqrt(area_sum2G[aa] * inv_count - sq(area_sumG[aa] * inv_count));
			double varB = sqrt(area_sum2B[aa] * inv_count - sq(area_sumB[aa] * inv_count));
			diff -= (varR + varG + varB) * area_count[aa];
			double new_inv_count = 1.0 / (area_count[aa] + area_count_diff[aa]);
			double new_varR = sqrt((area_sum2R[aa] + area_sum2R_diff[aa]) * new_inv_count - sq((area_sumR[aa] + area_sumR_diff[aa]) * new_inv_count));
			double new_varG = sqrt((area_sum2G[aa] + area_sum2G_diff[aa]) * new_inv_count - sq((area_sumG[aa] + area_sumG_diff[aa]) * new_inv_count));
			double new_varB = sqrt((area_sum2B[aa] + area_sum2B_diff[aa]) * new_inv_count - sq((area_sumB[aa] + area_sumB_diff[aa]) * new_inv_count));
			diff += (new_varR + new_varG + new_varB) * (area_count[aa] + area_count_diff[aa]);
		}

		ADD_COUNTER(0);
		if (diff <= 0) {
			ADD_COUNTER(1);
			START_TIMER(23);
			score += diff;
			world.seeds[ai].r = nr;
			world.seeds[ai].c = nc;
			used[old_r + MARGIN][old_c + MARGIN] = false;
			used[nr + MARGIN][nc + MARGIN] = true;
			for (int cp : changed_pos) {
				int cha = cp >> 20;
				int r = (cp >> 10) & 0x3FF;
				int c = cp & 0x3FF;
				area[r][c] = cha;
			}
			for (int cp : got_pos) {
				int r = cp >> 10;
				int c = cp & 0x3FF;
				area[r][c] = ai;
			}
			for (int aa : adj_area.elements) {
				area_count[aa] += area_count_diff[aa];
				area_sumR[aa] += area_sumR_diff[aa];
				area_sumG[aa] += area_sumG_diff[aa];
				area_sumB[aa] += area_sumB_diff[aa];
				area_sum2R[aa] += area_sum2R_diff[aa];
				area_sum2G[aa] += area_sum2G_diff[aa];
				area_sum2B[aa] += area_sum2B_diff[aa];
			}
			STOP_TIMER(23);
		}
	}
}

template<bool with_color>
void improve_small(World& world, long time_limit) {
	debug("raw:%d\n", world.raw);
	vi changed_pos;
	vi inside_pos;
	vi outside_pos;
	vi old_edge;
	IndexSet adj_area(N);
	int best_raw = world.raw;
	if (!with_color) {
		for (int i = 0; i < H; ++i) {
			copy(area[i].begin(), area[i].begin() + W, best_area[i].begin());
		}
	}
	sa_cooler = INITIAL_COOLER;
	ll begin_time = get_elapsed_msec();
	vector<Point> best_pos(N);
	const int MAX_MOVE = 4;
	const int MIN_MOVE = 1;
	int move = MAX_MOVE;
	for (int i = 0; i < N; ++i) {
		best_pos[i].r = world.seeds[i].r;
		best_pos[i].c = world.seeds[i].c;
	}
	// output_area(world);
	for (int turn = 1; ; ++turn) {
		if ((turn & 0x3FF) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed > time_limit) {
				debug("turn:%d raw:%d\n", turn, best_raw);
				for (int i = 0; i < N; ++i) {
					used[world.seeds[i].r + MARGIN][world.seeds[i].c + MARGIN] = false;
				}
				world.raw = best_raw;
				for (int i = 0; i < N; ++i) {
					world.seeds[i].r = best_pos[i].r;
					world.seeds[i].c = best_pos[i].c;
					used[world.seeds[i].r + MARGIN][world.seeds[i].c + MARGIN] = true;
				}
				return;
			}
			if ((turn & (with_color ? 0x3FFF : 0xFFF)) == 0) {
				// int old_raw = world.raw;
				voronoi_strict<with_color>(world);
				// output_area(world);
				// debug("turn:%d raw:%d->%d(%d)\n", turn, old_raw, world.raw, old_raw - world.raw);
				if (world.raw < best_raw) {
					best_raw = world.raw;
					for (int i = 0; i < N; ++i) {
						best_pos[i].r = world.seeds[i].r;
						best_pos[i].c = world.seeds[i].c;
					}
					if (!with_color) {
						for (int i = 0; i < H; ++i) {
							copy(area[i].begin(), area[i].begin() + W, best_area[i].begin());
						}
					}
				}
			} else {
				if (with_color && (turn & 0x3FF) == 0) {
					// int old_raw = world.raw;
					fix_color(world);
					// debug("turn:%d raw:%d->%d(%d)\n", turn, old_raw, world.raw, old_raw - world.raw);
				}
			}
			const double ratio = 1.0 * (elapsed - begin_time) / (time_limit - begin_time);
			double c0 = log(INITIAL_COOLER);
			double c1 = log(FINAL_COOLER);
			sa_cooler = exp(c0 * (1 - ratio) + c1 * ratio);
			c0 = log(MAX_MOVE);
			c1 = log(MIN_MOVE);
			move = (int)exp(c0 * (1 - ratio) + c1 * ratio);
		}
		const int ai = rnd.nextUInt(N);
		const int old_r = world.seeds[ai].r;
		const int old_c = world.seeds[ai].c;
		int nr = old_r + rnd.nextUInt(2 * move + 1) - move;
		int nc = old_c + rnd.nextUInt(2 * move + 1) - move;
		while (nr < -MARGIN || nr >= H + MARGIN || nc < -MARGIN || nc >= W + MARGIN || used[nr + MARGIN][nc + MARGIN]) {
			nr = old_r + rnd.nextUInt(2 * move + 1) - move;
			nc = old_c + rnd.nextUInt(2 * move + 1) - move;
		}
		world.seeds[ai].r = nr;
		world.seeds[ai].c = nc;
		changed_pos.clear();
		inside_pos.clear();
		outside_pos.clear();
		adj_area.clear();
		int diff_sum = 0;
		++evaluate_history_idx;
		if (0 <= nr && nr < H && 0 <= nc && nc < W && area[nr][nc] != ai) {
			outside_pos.push_back((nr << 10) | nc);
			evaluate_history[nr][nc] = evaluate_history_idx;
		}
		START_TIMER(6);
		for (const int fp : frontier.edges[ai]) {
			const int r = fp >> 10;
			const int c = fp & 0x3FF;
			int old_d = sq(r - old_r) + sq(c - old_c);
			int new_d = sq(r - nr) + sq(c - nc);
			if (new_d < old_d) {
				for (int i = 0; i < 4; ++i) {
					int ar = r + DR[i];
					int ac = c + DC[i];
					if (0 <= ar && ar < H && 0 <= ac && ac < W && area[ar][ac] != ai && evaluate_history[ar][ac] != evaluate_history_idx) {
						outside_pos.push_back((ar << 10) | ac);
						evaluate_history[ar][ac] = evaluate_history_idx;
					}
				}
			}	else {
				inside_pos.push_back(fp);
				evaluate_history[r][c] = evaluate_history_idx;
				for (int i = 0; i < 4; ++i) {
					int ar = r + DR[i];
					int ac = c + DC[i];
					if (0 <= ar && ar < H && 0 <= ac && ac < W && area[ar][ac] != ai) {
						adj_area.add(area[ar][ac]);
					}
				}
			}
		}
		STOP_TIMER(6);
		START_TIMER(7);
		for (int i = 0; i < outside_pos.size(); ++i) {
			const int r = outside_pos[i] >> 10;
			const int c = outside_pos[i] & 0x3FF;
			const int old_area = area[r][c];
			const int old_d = sq(r - world.seeds[old_area].r) + sq(c - world.seeds[old_area].c);
			const int new_d = sq(r - nr) + sq(c - nc);
			if (new_d < old_d || (new_d == old_d && ai < old_area)) {
				changed_pos.push_back((ai << 20) | outside_pos[i]);
				diff_sum += diff_color(world.seeds[ai].color, target[r][c]);
				diff_sum -= diff_color(world.seeds[old_area].color, target[r][c]);
				if (r > 0 && area[r - 1][c] != ai && evaluate_history[r - 1][c] != evaluate_history_idx) {
					outside_pos.push_back(outside_pos[i] - 1024);
					evaluate_history[r - 1][c] = evaluate_history_idx;
				}
				if (c > 0 && area[r][c - 1] != ai && evaluate_history[r][c - 1] != evaluate_history_idx) {
					outside_pos.push_back(outside_pos[i] - 1);
					evaluate_history[r][c - 1] = evaluate_history_idx;
				}
				if (c < W - 1 && area[r][c + 1] != ai && evaluate_history[r][c + 1] != evaluate_history_idx) {
					outside_pos.push_back(outside_pos[i] + 1);
					evaluate_history[r][c + 1] = evaluate_history_idx;
				}
				if (r < H - 1 && area[r + 1][c] != ai && evaluate_history[r + 1][c] != evaluate_history_idx) {
					outside_pos.push_back(outside_pos[i] + 1024);
					evaluate_history[r + 1][c] = evaluate_history_idx;
				}
			}
		}
		STOP_TIMER(7);
		START_TIMER(8);
		for (int i = 0; i < inside_pos.size(); ++i) {
			const int r = inside_pos[i] >> 10;
			const int c = inside_pos[i] & 0x3FF;
			int min_d = sq(r - nr) + sq(c - nc);
			int new_area = ai;
			for (int ca : adj_area.elements) {
				int d = sq(r - world.seeds[ca].r) + sq(c - world.seeds[ca].c);
				if (d < min_d || (d == min_d && ca < new_area)) {
					new_area = ca;
					min_d = d;
				}
			}
			if (new_area == ai) continue;
			diff_sum += diff_color(world.seeds[new_area].color, target[r][c]);
			diff_sum -= diff_color(world.seeds[ai].color, target[r][c]);
			changed_pos.push_back((new_area << 20) | inside_pos[i]);
			if (r > 0 && area[r - 1][c] == ai && evaluate_history[r - 1][c] != evaluate_history_idx) {
				inside_pos.push_back(inside_pos[i] - 1024);
				evaluate_history[r - 1][c] = evaluate_history_idx;
			}
			if (c > 0 && area[r][c - 1] == ai && evaluate_history[r][c - 1] != evaluate_history_idx) {
				inside_pos.push_back(inside_pos[i] - 1);
				evaluate_history[r][c - 1] = evaluate_history_idx;
			}
			if (c < W - 1 && area[r][c + 1] == ai && evaluate_history[r][c + 1] != evaluate_history_idx) {
				inside_pos.push_back(inside_pos[i] + 1);
				evaluate_history[r][c + 1] = evaluate_history_idx;
			}
			if (r < H - 1 && area[r + 1][c] == ai && evaluate_history[r + 1][c] != evaluate_history_idx) {
				inside_pos.push_back(inside_pos[i] + 1024);
				evaluate_history[r + 1][c] = evaluate_history_idx;
			}
		}
		STOP_TIMER(8);
		if (with_color ? accept(diff_sum) : diff_sum <= 0) {
			START_TIMER(9);
			world.raw += diff_sum;
			used[old_r + MARGIN][old_c + MARGIN] = false;
			used[nr + MARGIN][nc + MARGIN] = true;
			old_edge = frontier.edges[ai];
			for (int cp : changed_pos) {
				int cha = cp >> 20;
				int r = (cp >> 10) & 0x3FF;
				int c = cp & 0x3FF;
				change_area(world, r, c, cha);
			}
			for (int cp : old_edge) {
				int r = (cp >> 10) & 0x3FF;
				int c = cp & 0x3FF;
				if (area[r][c] != ai) continue;
				if ((r > 0 && area[r - 1][c] != ai) || (r < H - 1 && area[r + 1][c] != ai) ||
					  (c > 0 && area[r][c - 1] != ai) || (c < W - 1 && area[r][c + 1] != ai)) {
					// nothing here
				} else {
					frontier.remove(r, c);
				}
			}
			STOP_TIMER(9);
		} else {
			world.seeds[ai].r = old_r;
			world.seeds[ai].c = old_c;
		}
	}
}

pair<int, int> find_new_median(const array<int, 256>& count1, const array<int, 256>& sum1,
                               const array<int, 256>& count2, const array<int, 256>& sum2,
                               int c1 ,int c2) {
	if (c1 == c2) return {c1, 0};
	int all = count1[255] + count2[255];
	int c = min(c1, c2);
	for (; c < max(c1, c2); c++) {
		int count = count1[c] + count2[c];
		if (count >= (all + 1) / 2) {
			break;
		}
	}
	int cost1 = (count1[c1] * c1 - sum1[c1]) + ((sum1[255] - sum1[c1]) - (count1[255] - count1[c1]) * c1);
	int cost2 = (count2[c2] * c2 - sum2[c2]) + ((sum2[255] - sum2[c2]) - (count2[255] - count2[c2]) * c2);
	int cost_after1 = (count1[c] * c - sum1[c]) + ((sum1[255] - sum1[c]) - (count1[255] - count1[c]) * c);
	int cost_after2 = (count2[c] * c - sum2[c]) + ((sum2[255] - sum2[c]) - (count2[255] - count2[c]) * c);
	return {c, cost_after1 + cost_after2 - cost1 - cost2};
}

int merge_colors(World& world) {
	static array<array<int, 256>, 1000> histo_sum_r;
	static array<array<int, 256>, 1000> histo_sum_g;
	static array<array<int, 256>, 1000> histo_sum_b;
	voronoi_strict<true>(world);
	unordered_map<int, int> colors_set;
	vi initial_colors(N);
	for (int i = 0; i < N; ++i) {
		initial_colors[i] = world.seeds[i].color;
		colors_set[world.seeds[i].color] = colors_set[world.seeds[i].color] + 1;
		for (int j = 1; j < 256; ++j) {
			histo_sum_r[i][j] = histo_sum_r[i][j - 1] + j * world.histoR[i][j];
			histo_sum_g[i][j] = histo_sum_g[i][j - 1] + j * world.histoG[i][j];
			histo_sum_b[i][j] = histo_sum_b[i][j - 1] + j * world.histoB[i][j];
			world.histoR[i][j] += world.histoR[i][j - 1];
			world.histoG[i][j] += world.histoG[i][j - 1];
			world.histoB[i][j] += world.histoB[i][j - 1];
		}
	}
	vector<pair<int, int>> merge_history;
	vector<bool> merged(N);
	priority_queue<MergeState> q;
	for (int i = 0; i < N; ++i) {
		const int c1 = world.seeds[i].color;
		const int red_1 = c1 >> 16;
		const int green_1 = (c1 >> 8) & 0xFF;
		const int blue_1 = c1 & 0xFF;
		for (int j = i + 1; j < N; ++j) {
			const int c2 = world.seeds[j].color;
			const int d = diff_color(c1, c2);
			if (d >= 140) continue;
			const int red_2 = c2 >> 16;
			const int green_2 = (c2 >> 8) & 0xFF;
			const int blue_2 = c2 & 0xFF;
			const pair<int, int> new_red   = find_new_median(world.histoR[i], histo_sum_r[i], world.histoR[j], histo_sum_r[j], red_1, red_2);
			const pair<int, int> new_green = find_new_median(world.histoG[i], histo_sum_g[i], world.histoG[j], histo_sum_g[j], green_1, green_2);
			const pair<int, int> new_blue  = find_new_median(world.histoB[i], histo_sum_b[i], world.histoB[j], histo_sum_b[j], blue_1, blue_2);
			const int diff = new_red.second + new_green.second + new_blue.second;
			const int new_color = (new_red.first << 16) | (new_green.first << 8) | new_blue.first;
			q.push({i, j, world.seeds[i].size, world.seeds[j].size, diff, new_color});
		}
	}
	double best_score = world.raw * sq(1.0 + 1.0 * colors_set.size() / N);
	int best_time = 0;
	int ret = colors_set.size();
	vi best_colors(N);
	for (int i = 0; i < N; ++i) {
		best_colors[i] = world.seeds[i].color;
	}
	int best_raw = world.raw;
	for (int i = 1; i < N && !q.empty(); ) {
		const MergeState st = q.top();
		q.pop();
		if (merged[st.from] || merged[st.to] ||
			world.seeds[st.from].size != st.size_from || world.seeds[st.to].size != st.size_to) {
			continue;
		}
		merge_history.emplace_back(st.from, st.to);
		merged[st.from] = true;
		for (int j = 0; j < 256; ++j) {
			world.histoR[st.to][j] += world.histoR[st.from][j];
			world.histoG[st.to][j] += world.histoG[st.from][j];
			world.histoB[st.to][j] += world.histoB[st.from][j];
			histo_sum_r[st.to][j] += histo_sum_r[st.from][j];
			histo_sum_g[st.to][j] += histo_sum_g[st.from][j];
			histo_sum_b[st.to][j] += histo_sum_b[st.from][j];
		}
		world.seeds[st.to].size += world.seeds[st.from].size;
		world.raw += st.diff;
		if (colors_set[world.seeds[st.from].color] == 1) {
			colors_set.erase(world.seeds[st.from].color);
		} else {
			colors_set[world.seeds[st.from].color] -= 1;
		}
		if (colors_set[world.seeds[st.to].color] == 1) {
			colors_set.erase(world.seeds[st.to].color);
		} else {
			colors_set[world.seeds[st.to].color] -= 1;
		}
		colors_set[st.new_color] = colors_set[st.new_color] + 1;
		world.seeds[st.to].color = st.new_color;
		double score = world.raw * sq(1 + 1.0 * colors_set.size() / N);
		// debug("time:%d score:%f raw:%d color_count:%d\n", i, score, world.raw, (int)colors_set.size());
		if (score < best_score) {
			best_score = score;
			best_time = i;
			ret = colors_set.size();
			for (int j = 0; j < N; ++j) {
				best_colors[j] = world.seeds[j].color;
			}
			best_raw = world.raw;
		}

		const int c1 = st.new_color;
		const int red_1 = c1 >> 16;
		const int green_1 = (c1 >> 8) & 0xFF;
		const int blue_1 = c1 & 0xFF;
		for (int j = 0; j < N; ++j) {
			if (merged[j] || j == st.to) continue;
			const int c2 = world.seeds[j].color;
			const int d = diff_color(c1, c2);
			if (d >= 140) continue;
			const int red_2 = c2 >> 16;
			const int green_2 = (c2 >> 8) & 0xFF;
			const int blue_2 = c2 & 0xFF;
			const pair<int, int> new_red   = find_new_median(world.histoR[st.to], histo_sum_r[st.to], world.histoR[j], histo_sum_r[j], red_1, red_2);
			const pair<int, int> new_green = find_new_median(world.histoG[st.to], histo_sum_g[st.to], world.histoG[j], histo_sum_g[j], green_1, green_2);
			const pair<int, int> new_blue  = find_new_median(world.histoB[st.to], histo_sum_b[st.to], world.histoB[j], histo_sum_b[j], blue_1, blue_2);
			const int diff = new_red.second + new_green.second + new_blue.second;
			const int new_color = (new_red.first << 16) | (new_green.first << 8) | new_blue.first;
			q.push({st.to, j, world.seeds[st.to].size, world.seeds[j].size, diff, new_color});
		}
		++i;
	}
	debug("time:%d score:%f raw:%d\n", best_time, best_score, best_raw);
	for (int t = best_time; t > 0; --t) {
		int from = merge_history[t - 1].first;
		int to = merge_history[t - 1].second;
		best_colors[from] = best_colors[to];
	}
	world.raw = best_raw;
	for (int i = 0; i < N; ++i) {
		world.seeds[i].color = best_colors[i];
	}
	return ret;
}

Result solve_single(double whole_best_score, ll time_limit) {
	World world;
	while (true) {
		double l = sqrt(H * W / (sqrt(3) * N * (rnd.nextDouble() * 0.3 + 1.01)));
		double y = rnd.nextDouble() * sqrt(3) * 0.5 * l;
		double x = rnd.nextDouble() * l;
		vector<Point> cand_pos;
		while (y < H) {
			for (double cx = x; cx < W; cx += l) {
				cand_pos.push_back({(int)y, (int)cx});
			}
			y += l * sqrt(3);
			x -= l * 0.5;
			if (x < 0) x += l;
		}
		if (cand_pos.size() < N) continue;
		for (int i = 0; i < N; ++i) {
			int p = rnd.nextUInt(cand_pos.size() - i) + i;
			swap(cand_pos[i], cand_pos[p]);
			int r = cand_pos[i].r;
			int c = cand_pos[i].c;
			used[r + MARGIN][c + MARGIN] = true;
			world.seeds[i].r = r;
			world.seeds[i].c = c;
		}
		break;
	}
	ll begin_time = get_elapsed_msec();
	voronoi_strict<true>(world);
	START_TIMER(1);
	improve_large(world, (time_limit - begin_time) * 3 / 10 + begin_time);
	STOP_TIMER(1);
	voronoi_strict<true>(world);
	START_TIMER(2);
	improve_small<true>(world, (time_limit - begin_time) * 8 / 10 + begin_time);
	STOP_TIMER(2);
	START_TIMER(3);
	Result best_res;
	best_res.score = 1e99;
	if (world.raw >= whole_best_score) return best_res;
	int colors_count = merge_colors(world);
	STOP_TIMER(3);
	START_TIMER(4);
	voronoi_strict<false>(world);
	improve_small<false>(world, time_limit);
	STOP_TIMER(4);
	unordered_map<int, int> color_idx;
	vi area_color_idx(N);
	for (int i = 0; i < N; ++i) {
		if (color_idx.count(world.seeds[i].color) == 0) {
			int v = color_idx.size();
			color_idx[world.seeds[i].color] = v;
		}
		area_color_idx[i] = color_idx[world.seeds[i].color];
	}
	for (int i = 0; i < color_idx.size(); ++i) {
		world.histoR[i].fill(0);
		world.histoG[i].fill(0);
		world.histoB[i].fill(0);
	}
	vi color_size(color_idx.size());
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			int ci = area_color_idx[best_area[i][j]];
			color_size[ci]++;
			world.histoR[ci][target[i][j] >> 16]++;
			world.histoG[ci][(target[i][j] >> 8) & 0xFF]++;
			world.histoB[ci][target[i][j] & 0xFF]++;
		}
	}
	int new_raw = 0;
	vi new_color(color_idx.size());
	for (int i = 0; i < color_idx.size(); ++i) {
		pair<int, int> r = median(world.histoR[i], color_size[i]);
		pair<int, int> g = median(world.histoG[i], color_size[i]);
		pair<int, int> b = median(world.histoB[i], color_size[i]);
		new_color[i] = (r.first << 16) | (g.first << 8) | b.first;
		new_raw += r.second + g.second + b.second;
	}
	debug("old_raw:%d new_raw:%d\n", world.raw, new_raw);
	world.raw = new_raw;

	best_res.seeds.resize(N);
	for (int i = 0; i < N; ++i) {
		world.seeds[i].color = new_color[area_color_idx[i]];
		best_res.seeds[i] = world.seeds[i];
		used[best_res.seeds[i].r + MARGIN][best_res.seeds[i].c + MARGIN] = false;
	}
	best_res.score = world.raw * sq(1.0 + 1.0 * colors_count / N);
	return best_res;
}

Result solve() {
	Result best_result;
	best_result.score = 1e99;
	int solve_count;
	if (N < 50) {
		solve_count = 5;
	} else if (N < 70) {
		solve_count = 4;
	} else if (N < 100) {
		solve_count = 3;
	} else if (N < 250) {
		solve_count = 2;
	} else {
		solve_count = 1;
	}
	ll begin_time = get_elapsed_msec();
	for (int turn = 0; turn < solve_count; ++turn) {
		Result res = solve_single(best_result.score, (TL - begin_time) * (turn + 1) / solve_count + begin_time);
		debug("score:%f\n\n", res.score);
		if (res.score < best_result.score) {
			best_result = res;
		}
	}
	return best_result;
}

struct StainedGlass {
  vi create(int h, vi& pixels, int n);
};

vi StainedGlass::create(int h, vi& pixels, int n) {
	start_time = get_time();
	N = n;
	H = h;
	W = pixels.size() / H;
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			target[i][j] = pixels[i * W + j];
		}
	}
	START_TIMER(25);
	array<int, 9> median_r;
	array<int, 9> median_g;
	array<int, 9> median_b;
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			int pos = 0;
			for (int dr = -2; dr <= 2; ++dr) {
				int r = i + dr;
				if (r < 0 || H <= r) continue;
				for (int dc = -2; dc <= 2; ++dc) {
					int c = j + dc;
					if (c < 0 || W <= c) continue;
					median_r[pos] = target[r][c] & 0xFF0000;
					median_g[pos] = target[r][c] & 0x00FF00;
					median_b[pos] = target[r][c] & 0x0000FF;
					++pos;
				}
			}
			nth_element(median_r.begin(), median_r.begin() + pos / 2, median_r.begin() + pos);
			nth_element(median_g.begin(), median_g.begin() + pos / 2, median_g.begin() + pos);
			nth_element(median_b.begin(), median_b.begin() + pos / 2, median_b.begin() + pos);
			int m_r = median_r[pos / 2];
			int m_g = median_g[pos / 2];
			int m_b = median_b[pos / 2];
			precalc_median[i][j] = m_r | m_g | m_b;
		}
	}
	STOP_TIMER(25);
	frontier.set_size(N, H, W);
	vector<tuple<int, int, int>> pos;
	const int len = sqrt(5.5 * H * W / (3.14 * N));
	debug("len:%d\n", len);
	for (int i = -len; i <= len; ++i) {
		for (int j = -len; j <= len; ++j) {
			int d = i * i + j * j;
			if (d <= len * len) {
				pos.push_back(make_tuple(d, i, j));
			}
		}
	}
	sort(pos.begin(), pos.end());
	for (int i = 0, p = 0; p < pos.size(); ++i) {
		vector<Point> cur_adj;
		while (p < pos.size() && (cur_adj.empty() || get<0>(pos[p]) == get<0>(pos[p - 1]))) {
			cur_adj.push_back({get<1>(pos[p]), get<2>(pos[p])});
			++p;
		}
		adj_pos.push_back(cur_adj);
		adj_pos_dist.push_back(sqrt(sq(cur_adj[0].r) + sq(cur_adj[0].c)));
	}
	used.assign(H + MARGIN * 2, vector<bool>(W + MARGIN * 2));
	Result res = solve();
	vi ret(res.seeds.size() * 3);
	for (int i = 0; i < res.seeds.size(); ++i) {
		ret[i * 3 + 0] = res.seeds[i].r;
		ret[i * 3 + 1] = res.seeds[i].c;
		ret[i * 3 + 2] = res.seeds[i].color;
	}
	PRINT_COUNTER();
	PRINT_TIMER();
	return ret;
}

#if defined(LOCAL)
int main() {
	StainedGlass obj;
	int H, S, N;
	scanf("%d %d", &H, &S);
	vi pixels(S);
	for (int i = 0; i < S; ++i) {
		scanf("%d", &pixels[i]);
	}
	scanf("%d", &N);

	const vi ret = obj.create(H, pixels, N);
	printf("%d\n", (int)ret.size());
	for (int i = 0; i < (int)ret.size(); ++i) {
		printf("%d\n", ret[i]);
	}
	fflush(stdout);
}
#endif