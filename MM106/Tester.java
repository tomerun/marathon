import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Tester {
	static final int maxN = 1000, minN = 20;
	static final int border = 1000;
	static int myN;
	int N, H, W, Np;
	int[][] givenImage;
	int[] imgArg;
	int[][] drawnImage;

	void generate(long seed) throws Exception {
		SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
		rnd.setSeed(seed);
		N = rnd.nextInt(maxN - minN + 1) + minN;
		if (seed == 1) N = minN;
		else if (seed == 2) N = maxN;
		if (1000 <= seed && seed < 1200) {
			N = (int) (minN + (maxN - minN) * (seed - 1000) / 199);
		}
		if (myN != 0) N = myN;

		BufferedImage img = ImageIO.read(new File("testdata/" + seed + (seed == 1 ? ".png" : ".jpg")));
		H = img.getHeight();
		W = img.getWidth();
		givenImage = new int[H][W];
		for (int r = 0; r < H; ++r)
			for (int c = 0; c < W; ++c)
				givenImage[r][c] = img.getRGB(c, r) & 0xFFFFFF;
		imgArg = new int[H * W];
		for (int r = 0; r < H; ++r)
			System.arraycopy(givenImage[r], 0, imgArg, r * W, W);
	}

	int getComponent(int color, int index) {
		return (color >> (8 * index)) & 0xFF;
	}

	int getDiff(int c1, int c2) {
		int d = 0;
		for (int i = 0; i < 3; ++i)
			d += Math.abs(getComponent(c1, i) - getComponent(c2, i));
		return d;
	}

	int p2(int t) {
		return t * t;
	}

	Result runTest(long seed) throws Exception {
		generate(seed);
		Result res = new Result();
		res.seed = seed;
		res.N = N;
		res.H = H;
		res.W = W;
		Runtime rt = Runtime.getRuntime();
		proc = rt.exec("./tester");
		Thread thread = new ErrorReader(proc.getErrorStream());
		thread.start();
		long beforeTime = System.currentTimeMillis();
		try {
			int[] raw_points = create(H, imgArg, N);
			res.elapsed = System.currentTimeMillis() - beforeTime;
			thread.join(1000);
			if (raw_points.length % 3 != 0) {
				throw new RuntimeException("The number of elements in your return must be divisible by 3, and your return contained " + raw_points.length + ".");
			}
			if (raw_points.length == 0) {
				throw new RuntimeException("Your return must have at least 3 elements, and it contained " + raw_points.length + ".");
			}
			if (raw_points.length > 3 * N) {
				throw new RuntimeException("Your return can have at most " + (3 * N) + " elements, and it contained " + raw_points.length + ".");
			}

			res.NP = Np = raw_points.length / 3;
			int[] pointRow = new int[Np];
			int[] pointCol = new int[Np];
			int[] color = new int[Np];
			HashSet<Integer> colors = new HashSet<>();
			for (int i = 0; i < Np; ++i) {
				pointRow[i] = raw_points[3 * i];
				pointCol[i] = raw_points[3 * i + 1];
				color[i] = raw_points[3 * i + 2];
				if (pointRow[i] < -border || pointRow[i] > H - 1 + border) {
					throw new RuntimeException("Seed point " + i + " must be at row between " + (-border) + " and " + (H - 1 + border) + ", inclusive.");
				}
				if (pointCol[i] < -border || pointCol[i] > W - 1 + border) {
					throw new RuntimeException("Seed point " + i + " must be at column between " + (-border) + " and " + (W - 1 + border) + ", inclusive.");
				}
				if (color[i] < 0 || color[i] > 0xFFFFFF) {
					throw new RuntimeException("Piece " + i + " can only have color between 0 and 0xFFFFFF.");
				}
				colors.add(color[i]);
			}

			for (int i = 0; i < Np; ++i)
				for (int j = i + 1; j < Np; ++j)
					if (pointRow[i] == pointRow[j] && pointCol[i] == pointCol[j]) {
						throw new RuntimeException("Seed points " + i + " and " + j + " have the same coordinates.");
					}

			drawnImage = new int[H][W];
			for (int r = 0; r < H; ++r)
				for (int c = 0; c < W; ++c) {
					int minDist2 = -1;
					for (int ind = 0; ind < Np; ++ind) {
						int dist2 = p2(pointRow[ind] - r) + p2(pointCol[ind] - c);
						if (minDist2 == -1 || dist2 < minDist2) {
							minDist2 = dist2;
							drawnImage[r][c] = color[ind];
						}
					}
				}

			if (vis) {
				BufferedImage bi = new BufferedImage(W, H, BufferedImage.TYPE_INT_RGB);
				for (int r = 0; r < H; ++r)
					for (int c = 0; c < W; ++c)
						bi.setRGB(c, r, drawnImage[r][c]);
				if (mark)
					for (int ind = 0; ind < Np; ++ind) {
						for (int dr = -3; dr <= 3; dr++)
							if (pointRow[ind] + dr >= 0 && pointRow[ind] + dr < H)
								for (int dc = -3; dc <= 3; dc++)
									if (pointCol[ind] + dc >= 0 && pointCol[ind] + dc < W)
										bi.setRGB(pointCol[ind] + dc, pointRow[ind] + dr, 0);
					}
				ImageIO.write(bi, "png", new File("testdata/" + seed + "_out.png"));
			}

			int diff = 0;
			for (int r = 0; r < H; ++r)
				for (int c = 0; c < W; ++c)
					diff += getDiff(givenImage[r][c], drawnImage[r][c]);

			res.NC = colors.size();
			res.raw = diff;
			res.score = diff * Math.pow(1 + 1.0 * colors.size() / Np, 2);
		} catch (Exception e) {
			if (res.elapsed == 0) {
				res.elapsed = System.currentTimeMillis() - beforeTime;
			}
			e.printStackTrace();
			res.score = 999999999;
		} finally {
			if (proc != null) proc.destroy();
		}
		return res;
	}

	static boolean vis;
	static boolean mark = true;
	Process proc;

	int[] create(int H, int[] pixels, int N) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(H).append('\n');
		sb.append(pixels.length).append('\n');
		for (int i = 0; i < pixels.length; ++i)
			sb.append(pixels[i]).append('\n');
		sb.append(N).append('\n');
		try (OutputStream os = proc.getOutputStream()) {
			os.write(sb.toString().getBytes());
			os.flush();
		}

		int[] ret;
		try (InputStream is = proc.getInputStream();
				 BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
			int nRet = Integer.parseInt(br.readLine());
			ret = new int[nRet];
			for (int i = 0; i < nRet; ++i)
				ret[i] = Integer.parseInt(br.readLine());
		}
		return ret;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 0, begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-n")) myN = Integer.parseInt(args[++i]);
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-nomark")) mark = false;
		}
		if (begin != -1 && end != -1) {
			BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
			BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
			for (long i = begin; i <= end; ++i) {
				q.add(i);
			}
			Result[] results = new Result[(int) (end - begin + 1)];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i] = new TestThread(q, receiver);
				threads[i].start();
			}
			int printed = 0;
			try {
				for (int i = 0; i < (int) (end - begin + 1); i++) {
					Result res = receiver.poll(160, TimeUnit.SECONDS);
					results[(int) (res.seed - begin)] = res;
					for (; printed < results.length && results[printed] != null; printed++) {
						System.out.println(results[printed]);
						System.out.println();
					}
				}
			} catch (InterruptedException e) {
				for (int i = printed; i < results.length; i++) {
					System.out.println(results[i]);
					System.out.println();
				}
				e.printStackTrace();
			}

			double sum = 0;
			for (Result result : results) {
				sum += result.score;
			}
			System.out.println("ave:" + (sum / (end - begin + 1)));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	private static class TestThread extends Thread {
		BlockingQueue<Result> results;
		BlockingQueue<Long> q;

		TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
			this.q = q;
			this.results = results;
		}

		public void run() {
			while (true) {
				Long seed = q.poll();
				if (seed == null) break;
				try {
					Tester f = new Tester();
					Result res = f.runTest(seed);
					results.add(res);
				} catch (Exception e) {
					e.printStackTrace();
					Result res = new Result();
					res.seed = seed;
					results.add(res);
				}
			}
		}
	}
}

class Result {
	long seed;
	int N, H, W;
	int raw, NC, NP;
	double score;
	long elapsed;

	public String toString() {
		String ret = String.format("seed:%4d\n", seed);
		ret += String.format("N:%4d H:%3d W:%3d\n", N, H, W);
		ret += String.format("NP:%4d NC:%4d raw:%.4f\n", NP, NC, raw / 1000.0);
		ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
		ret += String.format("score:%.5f", score / 1000.0);
		return ret;
	}
}

class ErrorReader extends Thread {
	InputStream error;

	public ErrorReader(InputStream is) {
		error = is;
	}

	public void run() {
		try {
			byte[] ch = new byte[50000];
			int read;
			while ((read = error.read(ch)) > 0) {
				String s = new String(ch, 0, read);
				System.err.print(s);
				System.err.flush();
			}
		} catch (Exception e) {
		}
	}
}
