import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Constants {
	public static final int SIMULATION_TIME = 2000;
	public static final int MAX_WORKERS = 100;
	public static final int[] DR = new int[] { 1, 0, -1, 0 };
	public static final int[] DC = new int[] { 0, -1, 0, 1 };
}

class CloudType {
	static final int MIN_CLOUD_SIZE = 1;
	static final int MAX_CLOUD_SIZE = 3;
	static final int MIN_CLOUD_TIME = 10;
	static final int MAX_CLOUD_TIME = 25;

	int size;
	int liveTime;
	double snowProbGlobal;
	double[][] snowProbLocal;
	int[] moveProb;
	int sumMoveProb;

	public int getDirection(Random rnd) {
		int val = rnd.nextInt(sumMoveProb);
		int res = 0;
		while (val >= moveProb[res]) {
			val -= moveProb[res];
			res++;
		}
		return res;
	}

	public CloudType(Random rnd) {
		size = rnd.nextInt(MAX_CLOUD_SIZE - MIN_CLOUD_SIZE + 1) + MIN_CLOUD_SIZE;
		liveTime = rnd.nextInt(MAX_CLOUD_TIME - MIN_CLOUD_TIME + 1) + MIN_CLOUD_TIME;
		snowProbGlobal = rnd.nextDouble();
		snowProbLocal = new double[2 * size + 1][2 * size + 1];
		for (int i = 0; i <= 2 * size; i++) {
			for (int j = 0; j <= 2 * size; j++) {
				snowProbLocal[i][j] = rnd.nextDouble();
			}
		}

		moveProb = new int[Constants.DR.length];
		for (int i = 0; i < moveProb.length; i++) {
			double x = rnd.nextDouble();
			moveProb[i] = (int) Math.ceil(100 * x * x);
			sumMoveProb += moveProb[i];
		}
	}
}

class Cell implements Comparable<Cell> {
	public int r, c;

	public Cell(int r, int c) {
		this.r = r;
		this.c = c;
	}

	public boolean equals(Object other) {
		if (!(other instanceof Cell)) {
			return false;
		}

		Cell otherCell = (Cell) other;
		return this.r == otherCell.r && this.c == otherCell.c;
	}

	public int hashCode() {
		return (r << 8) + c;
	}

	public int compareTo(Cell other) {
		return (r == other.r ? c - other.c : r - other.r);
	}
}

class TestCase {
	private static final int MIN_CLOUD_TYPES = 1;
	private static final int MAX_CLOUD_TYPES = 10;
	private static final int MIN_CLOUD_COUNT = 50;
	private static final int MAX_CLOUD_COUNT = 200;
	private static final int MIN_BOARD_SIZE = 20;
	private static final int MAX_BOARD_SIZE = 50;
	private static final int MIN_SALARY = 10;
	private static final int MAX_SALARY = 100;
	private static final int MIN_SNOW_FINE = 10;
	private static final int MAX_SNOW_FINE = 100;

	public int boardSize;
	public int salary;
	public int snowFine;
	public int cloudTypeCnt;
	public CloudType[] cloudTypes;
	public int cloudCnt;
	Set<Cell>[] snowFalls = new Set[Constants.SIMULATION_TIME];

	static class Params implements Comparable<Params> {
		int salary, snowFine;
		double ratio;

		Params(int salary, int snowFine) {
			this.salary = salary;
			this.snowFine = snowFine;
			this.ratio = 1.0 * salary / snowFine;
		}

		public int compareTo(Params o) {
			return Double.compare(this.ratio, o.ratio);
		}
	}

	public TestCase(long seed) throws Exception {
		SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
		rnd.setSeed(seed);

		boardSize = rnd.nextInt(MAX_BOARD_SIZE - MIN_BOARD_SIZE + 1) + MIN_BOARD_SIZE;
		salary = rnd.nextInt(MAX_SALARY - MIN_SALARY + 1) + MIN_SALARY;
		snowFine = rnd.nextInt(MAX_SNOW_FINE - MIN_SNOW_FINE + 1) + MIN_SNOW_FINE;
		if (1001 <= seed && seed <= 1400) {
			Params[] ps = new Params[400];
			int pos = 0;
			for (int i = 10; i <= 100; i += 10) {
				for (int j = 10; j <= 100; j += 10) {
					ps[pos++] = new Params(i, j);
					ps[pos++] = new Params(i, j);
					ps[pos++] = new Params(i, j);
					ps[pos++] = new Params(i, j);
				}
			}
			Arrays.sort(ps);
			salary = ps[(int) (seed - 1001)].salary;
			snowFine = ps[(int) (seed - 1001)].snowFine;
		}

		cloudTypeCnt = rnd.nextInt(MAX_CLOUD_TYPES - MIN_CLOUD_TYPES + 1) + MIN_CLOUD_TYPES;
		cloudTypes = new CloudType[cloudTypeCnt];
		for (int i = 0; i < cloudTypeCnt; i++) {
			cloudTypes[i] = new CloudType(rnd);
		}

		cloudCnt = rnd.nextInt(MAX_CLOUD_COUNT - MIN_CLOUD_COUNT + 1) + MIN_CLOUD_COUNT;

		for (int t = 0; t < Constants.SIMULATION_TIME; t++) {
			snowFalls[t] = new HashSet<Cell>();
		}

		for (int i = 0; i < cloudCnt; i++) {
			int type = rnd.nextInt(cloudTypeCnt);

			int curRow = rnd.nextInt(boardSize);
			int curCol = rnd.nextInt(boardSize);
			int startTime = rnd.nextInt(Constants.SIMULATION_TIME);

			for (int t = startTime; t < startTime + cloudTypes[type].liveTime && t < Constants.SIMULATION_TIME; t++) {
				if (rnd.nextDouble() < cloudTypes[type].snowProbGlobal) {
					for (int r = 0; r <= 2 * cloudTypes[type].size; r++) {
						for (int c = 0; c <= 2 * cloudTypes[type].size; c++) {
							if (rnd.nextDouble() < cloudTypes[type].snowProbLocal[r][c]) {
								int snowR = curRow + r - cloudTypes[type].size;
								int snowC = curCol + c - cloudTypes[type].size;
								if (snowR >= 0 && snowR < boardSize && snowC >= 0 && snowC < boardSize) {
									snowFalls[t].add(new Cell(snowR, snowC));
								}
							}
						}
					}
				}
				int dir = cloudTypes[type].getDirection(rnd);
				curRow += Constants.DR[dir];
				curCol += Constants.DC[dir];
			}
		}
	}
}

class Drawer extends JFrame {
	public static final int EXTRA_WIDTH = 300;
	public static final int EXTRA_HEIGHT = 100;

	public World world;
	public DrawerPanel panel;
	public int cellSize, boardSize;
	public int width, height;

	public boolean pauseMode = false;

	class DrawerKeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			synchronized (keyMutex) {
				if (e.getKeyChar() == ' ') {
					pauseMode = !pauseMode;
				}
				keyPressed = true;
				keyMutex.notifyAll();
			}
		}
	}

	class DrawerPanel extends JPanel {
		public void paint(Graphics g) {
			g.setColor(Color.DARK_GRAY);
			g.fillRect(15, 15, cellSize * boardSize + 1, cellSize * boardSize + 1);
			g.setColor(Color.RED);
			for (int i = 0; i <= boardSize; i++) {
				g.drawLine(15 + i * cellSize, 15, 15 + i * cellSize, 15 + cellSize * boardSize);
				g.drawLine(15, 15 + i * cellSize, 15 + cellSize * boardSize, 15 + i * cellSize);
			}

			g.setColor(Color.WHITE);
			for (int i = 0; i < boardSize; i++) {
				for (int j = 0; j < boardSize; j++) {
					if (world.haveSnow[i][j]) {
						g.fillRect(15 + j * cellSize + 1, 15 + i * cellSize + 1, cellSize - 2, cellSize - 2);
					}
				}
			}

			g.setColor(Color.BLUE);
			synchronized (world.workersLock) {
				for (Cell worker : world.workers) {
					g.fillRect(15 + worker.c * cellSize + 1, 15 + worker.r * cellSize + 1, cellSize - 2, cellSize - 2);
				}
			}

			g.setColor(Color.BLACK);
			g.setFont(new Font("Arial", Font.BOLD, 12));
			Graphics2D g2 = (Graphics2D) g;

			int horPos = 40 + boardSize * cellSize;

			g2.drawString("Board size = " + world.haveSnow.length, horPos, 30);
			g2.drawString("Snow fine = " + world.fine, horPos, 50);
			g2.drawString("Salary = " + world.salary, horPos, 70);

			g2.drawString("Day = " + world.curDay, horPos, 105);
			g2.drawString("Uncleared snow cells = " + world.snowCnt, horPos, 125);
			synchronized (world.workersLock) {
				g2.drawString("Workers = " + world.workers.size(), horPos, 145);
			}
			g2.drawString("Total snow fine = ", horPos, 180);
			g2.drawString("" + world.totFine, horPos + 100, 180);
			g2.drawString("Total salary = ", horPos, 200);
			g2.drawString("" + world.totSalary, horPos + 100, 200);
			g2.drawString("Current score = ", horPos, 220);
			g2.drawString("" + (world.totFine + world.totSalary), horPos + 100, 220);
		}
	}

	class DrawerWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			System.exit(0);
		}
	}

	final Object keyMutex = new Object();
	boolean keyPressed;

	public void processPause() {
		synchronized (keyMutex) {
			if (!pauseMode) {
				return;
			}
			keyPressed = false;
			while (!keyPressed) {
				try {
					keyMutex.wait();
				} catch (InterruptedException e) {
					// do nothing
				}
			}
		}
	}

	public Drawer(World world, int cellSize) {
		panel = new DrawerPanel();
		getContentPane().add(panel);
		addWindowListener(new DrawerWindowListener());

		this.world = world;
		boardSize = world.haveSnow.length;
		this.cellSize = cellSize;
		width = cellSize * boardSize + EXTRA_WIDTH;
		height = cellSize * boardSize + EXTRA_HEIGHT;

		addKeyListener(new DrawerKeyListener());

		setSize(width, height);
		setTitle("Visualizer tool for problem SnowCleaning");
		setResizable(false);
		setVisible(true);
	}
}

class World {
	final Object workersLock = new Object();
	int snowCnt;
	boolean[][] haveSnow;
	List<Cell> workers = new ArrayList<Cell>();
	Set<Integer> usedWorkers = new HashSet<Integer>();
	int salary, fine;
	int totSalary, totFine;
	int curDay = -1;

	public World(int boardSize, int salary, int fine) {
		this.salary = salary;
		this.fine = fine;
		haveSnow = new boolean[boardSize][boardSize];
	}

	public void updateTotalSalary() {
		synchronized (workersLock) {
			totSalary += salary * workers.size();
		}
	}

	public void updateTotalFine() {
		totFine += snowCnt * fine;
	}

	public void addSnow(int r, int c) {
		if (!haveSnow[r][c]) {
			snowCnt++;
			haveSnow[r][c] = true;
		}
	}

	public void removeSnow(int r, int c) {
		if (haveSnow[r][c]) {
			snowCnt--;
			haveSnow[r][c] = false;
		}
	}

	public void startNewDay() {
		curDay++;
		usedWorkers.clear();
	}

	public String addWorker(int r, int c) {
		synchronized (workersLock) {
			if (workers.size() == Constants.MAX_WORKERS) {
				return "You are allowed to have at most " + Constants.MAX_WORKERS + " workers.";
			} else if (r < 0 || r >= haveSnow.length || c < 0 || c >= haveSnow.length) {
				return "You are trying to hire a worker at a cell outside the board.";
			} else {
				workers.add(new Cell(r, c));
				usedWorkers.add(workers.size() - 1);
				removeSnow(r, c);
				return "";
			}
		}
	}

	public String moveWorker(int id, int dir) {
		synchronized (workersLock) {
			if (id < 0 || id >= workers.size()) {
				return "You are trying to move worker which does not exist.";
			} else if (usedWorkers.contains(id)) {
				return "You are trying to execute a command for some worker more than once during the same turn.";
			} else {
				Cell worker = workers.get(id);
				worker.r += Constants.DR[dir];
				worker.c += Constants.DC[dir];
				if (worker.r < 0 || worker.c < 0 || worker.r >= haveSnow.length || worker.c >= haveSnow.length) {
					return "You are trying to move a worker outside the board.";
				}
				removeSnow(worker.r, worker.c);
				usedWorkers.add(id);
				return "";
			}
		}
	}

	public void cleanAllSnow() {
		synchronized (workersLock) {
			for (Cell worker : workers) {
				removeSnow(worker.r, worker.c);
			}
		}
	}
}

public class Tester {
	static String WRONG_COMMAND_ERROR = "ERROR: Each worker command must be formatted either \"M <ID> <DIR>\""
			+ " or \"H <ROW> <COL>\". Here <ID>, <ROW> and <COL> are integers from 0 to 99 without leading zeros"
			+ " and <DIR> is one of 'U', 'L', 'D', 'R'.";

	public static boolean vis = false;
	public static int cellSize = 12;
	public static int delay = 10;
	public static boolean startPaused = false;

	public Result runTest(long seed) throws Exception {
		Result result = new Result();
		result.seed = seed;
		TestCase tc = new TestCase(seed);
		result.salary = tc.salary;
		result.fine = tc.snowFine;
		long startTime = System.currentTimeMillis();
		SnowCleaning obj = new SnowCleaning();
		obj.init(tc.boardSize, tc.salary, tc.snowFine);
		result.elapsed += System.currentTimeMillis() - startTime;

		World world = new World(tc.boardSize, tc.salary, tc.snowFine);
		Drawer drawer = null;
		if (vis) {
			drawer = new Drawer(world, cellSize);
			if (startPaused) {
				drawer.pauseMode = true;
			}
		}

		for (int t = 0; t < Constants.SIMULATION_TIME; t++) {
			world.startNewDay();

			int snowFallCnt = tc.snowFalls[t].size();
			Cell[] snowFalls = new Cell[snowFallCnt];
			int pos = 0;
			for (Cell cell : tc.snowFalls[t]) {
				snowFalls[pos++] = cell;
			}
			Arrays.sort(snowFalls);

			int[] snowFallsPos = new int[snowFallCnt * 2];
			for (int i = 0; i < snowFallCnt; i++) {
				snowFallsPos[i * 2] = snowFalls[i].r;
				snowFallsPos[i * 2 + 1] = snowFalls[i].c;
				world.addSnow(snowFalls[i].r, snowFalls[i].c);
			}
			startTime = System.currentTimeMillis();
			String[] commands = obj.nextDay(snowFallsPos);
			result.elapsed += System.currentTimeMillis() - startTime;

			int commandCnt = commands.length;
			for (int i = 0; i < commandCnt; i++) {
				String command = commands[i];
				if (command.charAt(0) == 'H') {
					String[] items = command.split(" ");
					int row = Integer.parseInt(items[1]);
					int col = Integer.parseInt(items[2]);
					String msg = world.addWorker(row, col);
					if (msg.length() > 0) {
						System.err.println("ERROR: time step = " + t + ", worker command = " + i + " (0-based indices). " + msg);
						return result;
					}
				} else if (command.charAt(0) == 'M') {
					String[] items = command.split(" ");
					int id = Integer.parseInt(items[1]);
					int dir = "DLUR".indexOf(items[2]);
					String msg = world.moveWorker(id, dir);
					if (msg.length() > 0) {
						System.err.println("ERROR: time step = " + t + ", worker command = " + i + " (0-based indices). " + msg);
						return result;
					}
				} else {
					System.err.println("ERROR: time step = " + t + ", worker command = " + i + " (0-based indices). "
							+ WRONG_COMMAND_ERROR);
					return result;
				}
			}

			world.cleanAllSnow();
			world.updateTotalFine();
			world.updateTotalSalary();

			if (vis) {
				drawer.processPause();
				drawer.repaint();
				try {
					Thread.sleep(delay);
				} catch (Exception e) {
					// do nothing
				}
			}
		}
		result.workersCount = world.workers.size();
		result.totalFine = world.totFine;
		result.totalSalary = world.totSalary;
		return result;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 1;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-b")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-e")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-vis")) {
				vis = true;
			} else if (args[i].equals("-sz")) {
				cellSize = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-delay")) {
				delay = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-pause")) {
				startPaused = true;
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}

		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].totalFine + results[i].totalSalary;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int fine, salary;
		int workersCount;
		int totalFine, totalSalary;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("fine:%3d salary:%3d\n", fine, salary);
			ret += String.format("workers:%8d\n", workersCount);
			ret += String.format("tfine  :%8d\n", totalFine);
			ret += String.format("tsalary:%8d\n", totalSalary);
			ret += String.format("score  :%8d", totalFine + totalSalary);
			return ret;
		}
	}
}
