import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class SnowCleaning {
	private static final boolean DEBUG = false;
	static final String DIR = "ULDR";
	static final int UP = 0;
	static final int LEFT = 1;
	static final int DOWN = 2;
	static final int RIGHT = 3;
	static final int[] DR = { -1, 0, 1, 0 };
	static final int[] DC = { 0, -1, 0, 1 };
	Random rnd = new Random(42);
	int N;
	int salary, fine;
	int totalSalary, totalFine;
	double hireRatio;
	int snowCount;
	int prevWorkersCount;
	int workersCountUpper = 100;
	int day;
	int[][] snow;
	int[][] targeted;
	ArrayList<Worker> workers = new ArrayList<>();
	ArrayList<String> commands = new ArrayList<>();
	Stats stats = new Stats();

	int init(int boardSize, int salary, int snowFine) {
		this.N = boardSize;
		this.salary = salary;
		this.fine = snowFine;
		this.snow = new int[N][N];
		this.targeted = new int[N][N];
		this.hireRatio = 6.0 * Math.pow(1.0 * salary / snowFine, 0.6);
		for (int[] a : snow) {
			Arrays.fill(a, -1);
		}
		for (int[] a : targeted) {
			Arrays.fill(a, -1);
		}
		return 0;
	}

	String[] nextDay(int[] snowFalls) {
		++day;
		if (day % 512 == 0) {
			if (stats.cleanedSnow != 0) {
				int extraSnowCost = 0;
				for (int i = 0; i < N; ++i) {
					for (int j = 0; j < N; ++j) {
						if (snow[i][j] != -1) {
							extraSnowCost += fine * (day - snow[i][j]);
						}
					}
				}
				double aveCleanDays = 1.0 * stats.cleanedSnowDays / stats.cleanedSnow;
				int fallingSnow = (int) Math.ceil(1.0 * stats.realFallenSnow * (2000 - day) / day);
				int bestWorkersCount = workers.size();
				int bestCost = Integer.MAX_VALUE;
				for (int i = workers.size(); i <= 100; ++i) {
					int cost = (int) ((2000 - day) * i * salary + aveCleanDays * fallingSnow * workers.size() / i * fine);
					debug(i + " " + (cost + totalFine + totalSalary - extraSnowCost));
					if (cost < bestCost) {
						bestCost = cost;
						bestWorkersCount = i;
					} else if (cost > bestCost * 1.1) {
						break;
					}
				}
				debug("bestWorkersCount:" + bestWorkersCount);
				workersCountUpper = bestWorkersCount;
				stats.cleanedSnow = 0;
				stats.cleanedSnowDays = 0;
				hireRatio = 1;
			}
//			hireRatio *= 0.9;
		}
		commands.clear();
		addSnow(snowFalls);
		prevWorkersCount = workers.size();
		hire();
		ArrayList<Worker> resetWorkers = new ArrayList<>();
		for (int i = 0; i < prevWorkersCount; ++i) {
			Worker w = workers.get(i);
			if (w.targetR == -1 || snow[w.targetR][w.targetC] == -1) {
				resetWorkers.add(w);
			}
		}
		setTarget(resetWorkers);
		exchange();
		move();
		removeSnow();
		totalSalary += workers.size() * salary;
		totalFine += snowCount * fine;
		return commands.toArray(new String[0]);
	}

	void hire() {
		if (snowCount == 0) return;
		while (workers.size() < workersCountUpper && snowCount > workers.size() * hireRatio) {
			hireWorker();
		}
	}

	void hireWorker() {
		int candR = -1;
		int candC = -1;
		int maxDist = 0;
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				if (snow[i][j] == -1 || targeted[i][j] != -1) continue;
				int nearest = 999;
				for (Worker w : workers) {
					int d = Math.abs(i - w.targetR) + Math.abs(j - w.targetC);
					if (d < nearest) nearest = d;
				}
				if (nearest > maxDist) {
					maxDist = nearest;
					candR = i;
					candC = j;
				}
			}
		}
		if (candR == -1) throw new RuntimeException("cannot hire worker");
		Worker newWorker = new Worker(candR, candC, workers.size());
		newWorker.targetR = candR;
		newWorker.targetC = candC;
		targeted[candR][candC] = newWorker.id;
		workers.add(newWorker);
		commands.add("H " + candR + " " + candC);
	}

	void setTarget(ArrayList<Worker> ws) {
		for (Worker w : ws) {
			if (w.targetR != -1) {
				targeted[w.targetR][w.targetC] = -1;
				w.targetR = w.targetC = -1;
			}
		}

		ArrayList<Integer> untargetedSnow = new ArrayList<Integer>();
		ArrayList<Integer> targetedSnow = new ArrayList<Integer>();
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				if (snow[i][j] == -1) continue;
				if (targeted[i][j] == -1) {
					untargetedSnow.add((i << 8) + j);
				} else {
					targetedSnow.add((i << 8) + j);
				}
			}
		}
		for (int i = 0, size = Math.min(ws.size(), untargetedSnow.size()); i < size; ++i) {
			int maxI = -1;
			int maxD = 0;
			for (int j = 0; j < untargetedSnow.size(); ++j) {
				int pos = untargetedSnow.get(j);
				int cr = pos >> 8;
				int cc = pos & 0xFF;
				int minD = 999;
				for (int snow : targetedSnow) {
					int or = snow >> 8;
					int oc = snow & 0xFF;
					minD = Math.min(minD, Math.abs(cr - or) + Math.abs(cc - oc));
				}
				if (minD > maxD) {
					maxD = minD;
					maxI = j;
				}
			}
			int snowR = untargetedSnow.get(maxI) >> 8;
			int snowC = untargetedSnow.get(maxI) & 0xFF;
			int nearest = 999;
			Worker cand = null;
			for (Worker w : ws) {
				int d = Math.abs(w.r - snowR) + Math.abs(w.c - snowC);
				if (d < nearest) {
					nearest = d;
					cand = w;
				}
			}
			cand.targetR = snowR;
			cand.targetC = snowC;
			targeted[snowR][snowC] = cand.id;
			ws.remove(cand);
			targetedSnow.add(untargetedSnow.get(maxI));
			untargetedSnow.remove(maxI);
		}
	}

	void exchange() {
		ArrayList<Worker> idles = new ArrayList<>();
		ArrayList<Worker> workings = new ArrayList<>();
		for (int i = 0; i < prevWorkersCount; ++i) {
			Worker w = workers.get(i);
			if (w.targetR == -1) {
				idles.add(w);
			} else {
				workings.add(w);
			}
		}
		if (idles.isEmpty()) return;
		for (int i = 0; i < workings.size(); ++i) {
			Worker w = workings.get(i);
			int dist = Math.abs(w.targetR - w.r) + Math.abs(w.targetC - w.c);
			Worker cand = null;
			for (Worker idle : idles) {
				int d = Math.abs(w.targetR - idle.r) + Math.abs(w.targetC - idle.c);
				if (d < dist) {
					dist = d;
					cand = idle;
				}
			}
			if (cand != null) {
				cand.targetR = w.targetR;
				cand.targetC = w.targetC;
				w.targetR = w.targetC = -1;
				targeted[cand.targetR][cand.targetC] = cand.id;
				idles.remove(cand);
				idles.add(w);
			}
		}
	}

	void move() {
		for (int i = 0; i < prevWorkersCount; ++i) {
			Worker worker = workers.get(i);
			if (worker.targetR == -1) {
				int bestDir = -1;
				int bestLen = distApart(worker.r, worker.c, worker);
				for (int dir = 0; dir < 4; ++dir) {
					int len = distApart(worker.r + DR[dir], worker.c + DC[dir], worker);
					if (len > bestLen) {
						bestLen = len;
						bestDir = dir;
					}
				}
				if (bestDir != -1) {
					move(worker, bestDir);
				} else if (0 < worker.r && worker.r < N - 1 && 0 < worker.c && worker.c < N - 1 && rnd.nextInt(8) == 0) {
					move(worker, rnd.nextInt(4));
				}
				continue;
			}
			if (worker.targetR == worker.r && worker.targetC == worker.c) continue;
			if (worker.targetC == worker.c) {
				if (worker.r < worker.targetR) {
					move(worker, DOWN);
				} else {
					move(worker, UP);
				}
			} else if (worker.targetR == worker.r) {
				if (worker.c < worker.targetC) {
					move(worker, RIGHT);
				} else {
					move(worker, LEFT);
				}
			} else {
				if (rnd.nextBoolean()) {
					if (worker.r < worker.targetR) {
						move(worker, DOWN);
					} else {
						move(worker, UP);
					}
				} else {
					if (worker.c < worker.targetC) {
						move(worker, RIGHT);
					} else {
						move(worker, LEFT);
					}
				}
			}
		}
	}

	int distApart(int r, int c, Worker self) {
		if (r < 0 || N <= r || c < 0 || N <= c) return -1;
		int len = updateApartLen(Integer.MAX_VALUE, 2 * Math.min(r, c));
		len = updateApartLen(len, 2 * (N - 1 - r));
		len = updateApartLen(len, 2 * (N - 1 - c));
		for (Worker other : workers) {
			if (other == self) continue;
			if (other.targetR == -1) {
				len = updateApartLen(len, Math.abs(r - other.r) + Math.abs(c - other.c));
			} else {
				len = updateApartLen(len, Math.abs(r - other.targetR) + Math.abs(c - other.targetC));
			}
		}
		return len;
	}

	int updateApartLen(int len, int cur) {
		if ((len >> 8) > cur) {
			return (cur << 8) + 0xFF;
		} else if ((len >> 8) == cur) {
			return len - 1;
		} else {
			return len;
		}
	}

	void move(Worker worker, int dir) {
		worker.r += DR[dir];
		worker.c += DC[dir];
		commands.add("M " + worker.id + " " + DIR.charAt(dir));
	}

	void addSnow(int[] snowFalls) {
		stats.fallenSnow += snowFalls.length / 2;
		for (int i = 0; i < snowFalls.length; i += 2) {
			if (snow[snowFalls[i]][snowFalls[i + 1]] == -1) {
				snow[snowFalls[i]][snowFalls[i + 1]] = day;
				stats.realFallenSnow++;
				++snowCount;
			}
		}
	}

	void removeSnow() {
		for (Worker worker : workers) {
			if (snow[worker.r][worker.c] != -1) {
				stats.cleanedSnow++;
				stats.cleanedSnowDays += day - snow[worker.r][worker.c];
				--snowCount;
				snow[worker.r][worker.c] = -1;
			}
			if (worker.r == worker.targetR && worker.c == worker.targetC) {
				worker.targetR = -1;
				worker.targetC = -1;
				targeted[worker.r][worker.c] = -1;
			}
		}
	}

	static class Worker {
		int r, c, id;
		int targetR = -1, targetC = -1;

		public Worker(int r, int c, int id) {
			this.r = r;
			this.c = c;
			this.id = id;
		}
	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			SnowCleaning obj = new SnowCleaning();
			int boardSize = sc.nextInt();
			int salary = sc.nextInt();
			int snowFine = sc.nextInt();
			obj.init(boardSize, salary, snowFine);
			for (int i = 0; i < 2000; ++i) {
				int snowCnt = sc.nextInt();
				int[] snowFalls = new int[snowCnt * 2];
				for (int j = 0; j < 2 * snowCnt; ++j) {
					snowFalls[j] = sc.nextInt();
				}
				String[] ret = obj.nextDay(snowFalls);
				System.out.println(ret.length);
				for (String elem : ret) {
					System.out.println(elem);
				}
				System.out.flush();
			}
		}
	}

	static class Stats {
		int fallenSnow;
		int realFallenSnow;
		int cleanedSnow;
		int cleanedSnowDays;
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}
}
