#include <algorithm>
#include <utility>
#include <vector>
#include <unordered_set>
#include <bitset>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
//#define DEBUG
#define NDEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 8000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9500;
#endif

ll start_time; // msec

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
	ull ret;
	__asm__ volatile ("rdtsc" : "=A" (ret));
	return ret;
#else
	ull high,low;
	__asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
	return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

struct Point {
	uint8_t r, c;
};

struct Cut {
	Point s, e;
};

struct SimpleShape {
	uint8_t r, c, h, w;
	uint8_t rad[4];
};

struct ComplexShape {
	Point p[16];
	uint8_t size;
};

struct PartialResult {
	int area, rose, count;
	Cut cut;
	union {
		SimpleShape ss;
		ComplexShape cs;
	} shape;
};

struct DpResult {
	int area_d, rose_d;
	uint8_t cut_pos, count;
};

vector<PartialResult> results, best_results;
vector<vector<bool>> orig_field;
uint32_t rose_count[81][81];
uint32_t rose_count_vert[81][81], rose_count_horz[81][81];
uint32_t rose_count_tri[4][81][81][81];
uint32_t rose_count_below[81][81][81][81];
// DpResult dp[80][80][80][80];
int edge_idx[81][81];
int H, W, N, R;
double target_rose;
double ave_area;
PartialResult pr_buf[2];
const Cut invalid_cut = {{0xFF, 0}, {}};
int COMPLEX_TH = 6;
const double INITIAL_COOLER = 100.0;
const double FINAL_COOLER = 1000.0;
double cooler = INITIAL_COOLER;
Point pos_buf[320];

inline bool accept(double diff) {
	if (diff <= 0) return true;
	return rnd.nextDouble() < exp(-diff * cooler);
}

void debug_simple_shape(const SimpleShape& shape) {
	debug("(%d %d)-(%d %d) [%d %d %d %d]\n", shape.r, shape.c, shape.r + shape.h, shape.c + shape.w,
		    shape.rad[0], shape.rad[1], shape.rad[2], shape.rad[3]);
}

void debug_complex_shape(const ComplexShape& shape) {
	for (int i = 0; i < shape.size; ++i) {
		debug("(%d %d) ", shape.p[i].r, shape.p[i].c);
	}
	debugln();
}

void convert_simple_complex(PartialResult& res) {
	SimpleShape ss = res.shape.ss;
	const int left = ss.h - ss.rad[0] - ss.rad[1];
	const int bottom = ss.w - ss.rad[1] - ss.rad[2];
	const int right = ss.h - ss.rad[2] - ss.rad[3];
	const int top = ss.w - ss.rad[3] - ss.rad[0];
	ComplexShape& cs = res.shape.cs;
	int idx = 0;
	if (ss.rad[0] > 0) {
		cs.p[idx].r = ss.r + ss.rad[0];
		cs.p[idx].c = ss.c;
		++idx;
	}
	if (left > 0) {
		cs.p[idx].r = ss.r + ss.rad[0] + left;
		cs.p[idx].c = ss.c;
		++idx;
	}
	if (ss.rad[1] > 0) {
		cs.p[idx].r = ss.r + ss.h;
		cs.p[idx].c = ss.c + ss.rad[1];
		++idx;
	}
	if (bottom > 0) {
		cs.p[idx].r = ss.r + ss.h;
		cs.p[idx].c = ss.c + ss.rad[1] + bottom;
		++idx;
	}
	if (ss.rad[2] > 0) {
		cs.p[idx].r = ss.r + ss.h - ss.rad[2];
		cs.p[idx].c = ss.c + ss.w;
		++idx;
	}
	if (right > 0) {
		cs.p[idx].r = ss.r + ss.rad[3];
		cs.p[idx].c = ss.c + ss.w;
		++idx;
	}
	if (ss.rad[3] > 0) {
		cs.p[idx].r = ss.r;
		cs.p[idx].c = ss.c + ss.w - ss.rad[3];
		++idx;
	}
	if (top > 0) {
		cs.p[idx].r = ss.r;
		cs.p[idx].c = ss.c + ss.rad[0];
		++idx;
	}
	cs.size = idx;
}

int set_pos_buf(const ComplexShape& shape) {
	int bi = 0;
	for (int i = 0; i < shape.size - 1; ++i) {
		int dr = shape.p[i + 1].r - shape.p[i].r;
		int dc = shape.p[i + 1].c - shape.p[i].c;
		int gcd = __gcd(abs(dr), abs(dc));
		dr /= gcd;
		dc /= gcd;
		edge_idx[shape.p[i].r][shape.p[i].c] |= (1 << i);
		for (int j = 0; j < gcd; ++j, ++bi) {
			pos_buf[bi].r = shape.p[i].r + dr * j;
			pos_buf[bi].c = shape.p[i].c + dc * j;
			edge_idx[pos_buf[bi].r + dr][pos_buf[bi].c + dc] = (1 << i);
		}
	}
	int dr = shape.p[0].r - shape.p[shape.size - 1].r;
	int dc = shape.p[0].c - shape.p[shape.size - 1].c;
	int gcd = __gcd(abs(dr), abs(dc));
	dr /= gcd;
	dc /= gcd;
	edge_idx[shape.p[shape.size - 1].r][shape.p[shape.size - 1].c] |= (1 << (shape.size - 1));
	for (int j = 0; j < gcd; ++j, ++bi) {
		pos_buf[bi].r = shape.p[shape.size - 1].r + dr * j;
		pos_buf[bi].c = shape.p[shape.size - 1].c + dc * j;
		edge_idx[pos_buf[bi].r + dr][pos_buf[bi].c + dc] = (1 << (shape.size - 1));
	}
	edge_idx[pos_buf[0].r][pos_buf[0].c] |= 1;
	return bi;
}

int rose_count_on_line(int r1, int c1, int r2, int c2) {
	int dr = r2 - r1;
	int dc = c2 - c1;
	int ret = 0;
	if (dr != 0 && dc != 0 && __builtin_ctz(dr) == __builtin_ctz(dc)) {
		int gcd = __gcd(abs(dr), abs(dc));
		dr /= gcd;
		dc /= gcd;
		int cr = r1 * 2;
		int cc = c1 * 2;
		for (int i = 0; i < gcd; ++i) {
			int rr = (cr + dr) >> 1;
			int rc = (cc + dc) >> 1;
			if (orig_field[rr][rc]) {
				++ret;
			}
			cr += 2 * dr;
			cc += 2 * dc;
		}
	}
	return ret;
}

inline int rose_count_rect(int r, int c, int h, int w) {
	return rose_count[r + h][c + w] - rose_count[r + h][c] - rose_count[r][c + w] + rose_count[r][c];
}

int rose_count_shape(const SimpleShape& shape) {
	int ret = rose_count_rect(shape.r, shape.c, shape.h, shape.w);
	ret -= rose_count_tri[0][shape.r          ][shape.c          ][shape.rad[0]];
	ret -= rose_count_tri[1][shape.r + shape.h][shape.c          ][shape.rad[1]];
	ret -= rose_count_tri[2][shape.r + shape.h][shape.c + shape.w][shape.rad[2]];
	ret -= rose_count_tri[3][shape.r          ][shape.c + shape.w][shape.rad[3]];
	return ret;
}

int rose_count_edge_below(int r1, int c1, int r2, int c2) {
	if (rose_count_below[r1][c1][r2][c2] != -1) {
		return rose_count_below[r1][c1][r2][c2];
	}
	int ret = 0;
	int dr = (r2 - r1) * 2;
	int dc = (c2 - c1) * 2;
	int row = r1 * 2 * dc;
	for (uint32_t i = c1 * 2; i < c2 * 2; i += 2) {
		int mr = (row + dr + dc - 1) / (dc * 2);
		ret += rose_count_vert[i / 2][mr];
		row += dr * 2;
	}
	return rose_count_below[r1][c1][r2][c2] = ret;
}

int rose_count_shape(const ComplexShape& shape) {
	int ret = 0;
	for (int i = 0; i < shape.size; ++i) {
		const Point& p1 = shape.p[i];
		const Point& p2 = shape.p[i == shape.size - 1 ? 0 : i + 1];
		if (p1.c == p2.c) continue;
		if (p1.c < p2.c) {
			ret += rose_count_edge_below(p1.r, p1.c, p2.r, p2.c);
		} else {
			ret -= rose_count_edge_below(p2.r, p2.c, p1.r, p1.c);
			ret -= rose_count_on_line(p2.r, p2.c, p1.r, p1.c);
		}
	}
	return ret;
}

// int rose_count_shape(const ComplexShape& shape) {
// 	int left = shape.p[0].c;
// 	int right = shape.p[0].c;
// 	int top = shape.p[0].r;
// 	int bottom = shape.p[0].r;
// 	for (int i = 1; i < shape.size; ++i) {
// 		left = min(left, (int)shape.p[i].c);
// 		right = max(right, (int)shape.p[i].c);
// 		top = min(top, (int)shape.p[i].r);
// 		bottom = max(bottom, (int)shape.p[i].r);
// 	}
// 	int start = 0;
// 	int ret = 0;
// 	if (right - left < bottom - top) {
// 		for (int i = 1; i < shape.size; ++i) {
// 			if (shape.p[i].c < shape.p[start].c || (shape.p[i].c == shape.p[start].c && shape.p[i].r < shape.p[start].r)) {
// 				start = i;
// 			}
// 		}
// 		left *= 2;
// 		right *= 2;
// 		top = shape.p[start].r * 2;
// 		bottom = top;
// 		int pi = start == 0 ? shape.size - 1 : start - 1;
// 		int ni = start == shape.size - 1 ? 0 : start + 1;
// 		if (shape.p[ni].c * 2 == left) {
// 			bottom = shape.p[ni].r * 2;
// 			ni++;
// 			if (ni == shape.size) ni = 0;
// 		}
// 		int drt = shape.p[pi].r * 2 - top;
// 		int dct = shape.p[pi].c * 2 - left;
// 		int drb = shape.p[ni].r * 2 - bottom;
// 		int dcb = shape.p[ni].c * 2 - left;
// 		top *= dct;
// 		bottom *= dcb;
// 		for (uint32_t i = left; i < right; i += 2) {
// 			if (i == shape.p[pi].c * 2) {
// 				top = shape.p[pi].r * 2;
// 				pi = pi == 0 ? shape.size - 1 : pi - 1;
// 				drt = shape.p[pi].r * 2 - top;
// 				dct = shape.p[pi].c * 2 - i;
// 				top *= dct;
// 			}
// 			if (i == shape.p[ni].c * 2) {
// 				bottom = shape.p[ni].r * 2;
// 				ni = ni == shape.size - 1 ? 0 : ni + 1;
// 				drb = shape.p[ni].r * 2 - bottom;
// 				dcb = shape.p[ni].c * 2 - i;
// 				bottom *= dcb;
// 			}
// 			int mt = (top + drt + dct) / (dct * 2);
// 			int mb = (bottom + drb + dcb - 1) / (dcb * 2);
// 			// debug("col:%d top:%d bottom:%d mt:%d mb:%d rc:%d %d\n", i, top, bottom, mt, mb, rose_count_vert[i / 2][mt], rose_count_vert[i / 2][mb]);
// 			ret += rose_count_vert[i / 2][mb] - rose_count_vert[i / 2][mt];
// 			top += drt * 2;
// 			bottom += drb * 2;
// 		}
// 	} else {
// 		for (int i = 1; i < shape.size; ++i) {
// 			if (shape.p[i].r < shape.p[start].r || (shape.p[i].r == shape.p[start].r && shape.p[i].c < shape.p[start].c)) {
// 				start = i;
// 			}
// 		}
// 		bottom *= 2;
// 		top *= 2;
// 		left = shape.p[start].c * 2;
// 		right = left;
// 		int pi = start == 0 ? shape.size - 1 : start - 1;
// 		int ni = start == shape.size - 1 ? 0 : start + 1;
// 		if (shape.p[pi].r * 2 == top) {
// 			right = shape.p[pi].c * 2;
// 			pi = pi == 0 ? shape.size - 1 : pi - 1;
// 		}
// 		int drr = shape.p[pi].r * 2 - top;
// 		int dcr = shape.p[pi].c * 2 - right;
// 		int drl = shape.p[ni].r * 2 - top;
// 		int dcl = shape.p[ni].c * 2 - left;
// 		left *= drl;
// 		right *= drr;
// 		for (uint32_t i = top; i < bottom; i += 2) {
// 			if (i == shape.p[ni].r * 2) {
// 				left = shape.p[ni].c * 2;
// 				ni = ni == shape.size - 1 ? 0 : ni + 1;
// 				drl = shape.p[ni].r * 2 - i;
// 				dcl = shape.p[ni].c * 2 - left;
// 				left *= drl;
// 			}
// 			if (i == shape.p[pi].r * 2) {
// 				right = shape.p[pi].c * 2;
// 				pi = pi == 0 ? shape.size - 1 : pi - 1;
// 				drr = shape.p[pi].r * 2 - i;
// 				dcr = shape.p[pi].c * 2 - right;
// 				right *= drr;
// 			}
// 			int ml = (left + dcl + drl) / (drl * 2);
// 			int mr = (right + dcr + drr - 1) / (drr * 2);
// 			// debug("row:%d left:%d right:%d ml:%d mr:%d rc:%d %d\n", i, left, right, ml, mr, rose_count_horz[i / 2][mr], rose_count_horz[i / 2][ml]);
// 			ret += rose_count_horz[i / 2][mr] - rose_count_horz[i / 2][ml];
// 			left += dcl * 2;
// 			right += dcr * 2;
// 		}
// 	}
// 	return ret;
// }

int area_shape(const SimpleShape& shape) {
	return 2 * shape.h * shape.w - sq<int>(shape.rad[0]) - sq<int>(shape.rad[1]) - sq<int>(shape.rad[2]) - sq<int>(shape.rad[3]);
}

int area_shape(const ComplexShape& shape) {
	int area = 0;
	int r0 = shape.p[0].r;
	int c0 = shape.p[0].c;
	for (int i = 1; i < shape.size - 1; ++i) {
		int r1 = shape.p[i].r;
		int c1 = shape.p[i].c;
		int r2 = shape.p[i + 1].r;
		int c2 = shape.p[i + 1].c;
		area += (r1 - r0) * (c2 - c0) - (r2 - r0) * (c1 - c0);
	}
	return area;
}

Cut cut_simple_from(int idx, int cr, int cc, int dir) {
	// debug("cut_simple_from:(%d %d) %d\n", cr, cc, dir);
	const SimpleShape& shape = results[idx].shape.ss;
	Cut cur_cut = {{(uint8_t)cr, (uint8_t)cc}, {}};
	int er, ec;
	if (dir == 0) {
		if (shape.r + shape.h - (shape.c + shape.w - shape.rad[2]) <= cr - cc) { // hit bottom
			er = shape.r + shape.h;
			ec = er - cr + cc;
		} else if (shape.r + shape.h - shape.rad[2] - (shape.c + shape.w) < cr - cc) { // hit bottom-right
			int move = (cr - cc) - (shape.r + shape.h - shape.rad[2] - (shape.c + shape.w));
			if (move % 2 != 0) {
				return invalid_cut;
			}
			er = shape.r + shape.h - shape.rad[2] + move / 2;
			ec = shape.c + shape.w - move / 2;
		} else { // hit right
			ec = shape.c + shape.w;
			er = cr - cc + ec;
		}
		pr_buf[0].shape.ss.r = cr;
		pr_buf[0].shape.ss.c = shape.c;
		pr_buf[0].shape.ss.h = shape.r + shape.h - cr;
		pr_buf[0].shape.ss.w = ec - shape.c;
		pr_buf[0].shape.ss.rad[0] = max(0, shape.rad[0] - (cr - shape.r));
		pr_buf[0].shape.ss.rad[1] = shape.rad[1];
		pr_buf[0].shape.ss.rad[2] = min((int)shape.rad[2], shape.r + shape.h - er);
		pr_buf[0].shape.ss.rad[3] = er - cr;
		pr_buf[1].shape.ss.r = shape.r;
		pr_buf[1].shape.ss.c = cc;
		pr_buf[1].shape.ss.h = er - shape.r;
		pr_buf[1].shape.ss.w = shape.c + shape.w - cc;
		pr_buf[1].shape.ss.rad[0] = shape.rad[0] - pr_buf[0].shape.ss.rad[0];
		pr_buf[1].shape.ss.rad[1] = pr_buf[0].shape.ss.rad[3];
		pr_buf[1].shape.ss.rad[2] = shape.rad[2] - pr_buf[0].shape.ss.rad[2];
		pr_buf[1].shape.ss.rad[3] = shape.rad[3];
	} else if (dir == 1) {
		er = cr;
		if (shape.r + shape.h - shape.rad[2] < cr) { // hit bottom-right
			ec = shape.c + shape.w - (cr - (shape.r + shape.h - shape.rad[2]));
		} else if (shape.r + shape.rad[3] <= cr) { // hit right
			ec = shape.c + shape.w;
		} else { // hit top-right
			ec = shape.c + shape.w - shape.rad[3] + (cr - shape.r);
		}
		pr_buf[0].shape.ss.r = cr;
		pr_buf[0].shape.ss.c = cr > shape.r + shape.h - shape.rad[1] ? cc : shape.c;
		pr_buf[0].shape.ss.h = shape.r + shape.h - cr;
		pr_buf[0].shape.ss.w = (cr > shape.r + shape.h - shape.rad[2] ? ec : shape.c + shape.w) - pr_buf[0].shape.ss.c;
		pr_buf[0].shape.ss.rad[0] = max(0, shape.rad[0] - (cr - shape.r));
		pr_buf[0].shape.ss.rad[1] = min((int)shape.rad[1], shape.r + shape.h - cr);
		pr_buf[0].shape.ss.rad[2] = min((int)shape.rad[2], shape.r + shape.h - cr);
		pr_buf[0].shape.ss.rad[3] = max(0, shape.rad[3] - (cr - shape.r));
		pr_buf[1].shape.ss.r = shape.r;
		pr_buf[1].shape.ss.c = cr < shape.r + shape.rad[0] ? cc : shape.c;
		pr_buf[1].shape.ss.h = cr - shape.r;
		pr_buf[1].shape.ss.w = (cr < shape.r + shape.rad[3] ? ec : shape.c + shape.w) - pr_buf[1].shape.ss.c;
		pr_buf[1].shape.ss.rad[0] = shape.rad[0] - pr_buf[0].shape.ss.rad[0];
		pr_buf[1].shape.ss.rad[1] = shape.rad[1] - pr_buf[0].shape.ss.rad[1];
		pr_buf[1].shape.ss.rad[2] = shape.rad[2] - pr_buf[0].shape.ss.rad[2];
		pr_buf[1].shape.ss.rad[3] = shape.rad[3] - pr_buf[0].shape.ss.rad[3];
	} else if (dir == 2) {
		if (shape.r + shape.rad[3] + shape.c + shape.w <= cr + cc) { // hit right
			er = cr + cc - (shape.c + shape.w);
			ec = shape.c + shape.w;
		} else if (shape.r + (shape.c + shape.w - shape.rad[3]) < cr + cc) { // hit top-right
			int move = (cr + cc) - (shape.r + (shape.c + shape.w - shape.rad[3]));
			if (move % 2 != 0) {
				return invalid_cut;
			}
			er = shape.r + move / 2;
			ec = shape.c + shape.w - shape.rad[3] + move / 2;
		} else { // hit top
			er = shape.r;
			ec = cr + cc - er;
		}
		pr_buf[0].shape.ss.r = er;
		pr_buf[0].shape.ss.c = cc;
		pr_buf[0].shape.ss.h = shape.r + shape.h - er;
		pr_buf[0].shape.ss.w = shape.c + shape.w - cc;
		pr_buf[0].shape.ss.rad[0] = cr - er;
		pr_buf[0].shape.ss.rad[1] = max(0, shape.rad[1] - (cc - shape.c));
		pr_buf[0].shape.ss.rad[2] = shape.rad[2];
		pr_buf[0].shape.ss.rad[3] = max(0, shape.rad[3] - (er - shape.r));
		pr_buf[1].shape.ss.r = shape.r;
		pr_buf[1].shape.ss.c = shape.c;
		pr_buf[1].shape.ss.h = cr - shape.r;
		pr_buf[1].shape.ss.w = ec - shape.c;
		pr_buf[1].shape.ss.rad[0] = shape.rad[0];
		pr_buf[1].shape.ss.rad[1] = shape.rad[1] - pr_buf[0].shape.ss.rad[1];
		pr_buf[1].shape.ss.rad[2] = pr_buf[0].shape.ss.rad[0];
		pr_buf[1].shape.ss.rad[3] = shape.rad[3] - pr_buf[0].shape.ss.rad[3];
	} else if (dir == 3) {
		ec = cc;
		if (shape.c + shape.w - shape.rad[3] < cc) { // hit top-right
			er = shape.r + (cc - (shape.c + shape.w - shape.rad[3]));
		} else if (shape.c + shape.rad[0] <= cc) { // hit top
			er = shape.r;
		} else { // hit top-left
			er = shape.r + (shape.c + shape.rad[0] - cc);
		}
		pr_buf[0].shape.ss.r = cc > shape.c + shape.w - shape.rad[3] ? er : shape.r;
		pr_buf[0].shape.ss.c = cc;
		pr_buf[0].shape.ss.h = (cc > shape.c + shape.w - shape.rad[2] ? cr : shape.r + shape.h) - pr_buf[0].shape.ss.r;
		pr_buf[0].shape.ss.w = shape.c + shape.w - cc;
		pr_buf[0].shape.ss.rad[0] = max(0, shape.rad[0] - (cc - shape.c));
		pr_buf[0].shape.ss.rad[1] = max(0, shape.rad[1] - (cc - shape.c));
		pr_buf[0].shape.ss.rad[2] = min((int)shape.rad[2], shape.c + shape.w - cc);
		pr_buf[0].shape.ss.rad[3] = min((int)shape.rad[3], shape.c + shape.w - cc);
		pr_buf[1].shape.ss.r = cc < shape.c + shape.rad[0] ? er : shape.r;
		pr_buf[1].shape.ss.c = shape.c;
		pr_buf[1].shape.ss.h = (cc < shape.c + shape.rad[1] ? cr : shape.r + shape.h) - pr_buf[1].shape.ss.r;
		pr_buf[1].shape.ss.w = cc - shape.c;
		pr_buf[1].shape.ss.rad[0] = shape.rad[0] - pr_buf[0].shape.ss.rad[0];
		pr_buf[1].shape.ss.rad[1] = shape.rad[1] - pr_buf[0].shape.ss.rad[1];
		pr_buf[1].shape.ss.rad[2] = shape.rad[2] - pr_buf[0].shape.ss.rad[2];
		pr_buf[1].shape.ss.rad[3] = shape.rad[3] - pr_buf[0].shape.ss.rad[3];
	} else if (dir == 4) {
		if (shape.r - (shape.c + shape.rad[0]) >= cr - cc) { // hit top
			er = shape.r;
			ec = er - cr + cc;
		} else if (shape.r + shape.rad[0] - shape.c > cr - cc) { // hit top-left
			int move = (shape.r + shape.rad[0] - shape.c) - (cr - cc);
			if (move % 2 != 0) {
				return invalid_cut;
			}
			er = shape.r + shape.rad[0] - move / 2;
			ec = shape.c + move / 2;
		} else { // hit left
			ec = shape.c;
			er = ec + cr - cc;
		}
		pr_buf[0].shape.ss.r = shape.r;
		pr_buf[0].shape.ss.c = ec;
		pr_buf[0].shape.ss.h = cr - shape.r;
		pr_buf[0].shape.ss.w = shape.c + shape.w - ec;
		pr_buf[0].shape.ss.rad[0] = min((int)shape.rad[0], er - shape.r);
		pr_buf[0].shape.ss.rad[1] = cr - er;
		pr_buf[0].shape.ss.rad[2] = max(0, shape.rad[2] - (shape.r + shape.h - cr));
		pr_buf[0].shape.ss.rad[3] = shape.rad[3];
		pr_buf[1].shape.ss.r = er;
		pr_buf[1].shape.ss.c = shape.c;
		pr_buf[1].shape.ss.h = shape.r + shape.h - er;
		pr_buf[1].shape.ss.w = cc - shape.c;
		pr_buf[1].shape.ss.rad[0] = shape.rad[0] - pr_buf[0].shape.ss.rad[0];
		pr_buf[1].shape.ss.rad[1] = shape.rad[1];
		pr_buf[1].shape.ss.rad[2] = shape.rad[2] - pr_buf[0].shape.ss.rad[2];
		pr_buf[1].shape.ss.rad[3] = pr_buf[0].shape.ss.rad[1];
	} else if (dir == 5) {
		er = cr;
		if (shape.r + shape.rad[0] > cr) { // hit top-left
			ec = shape.c + (shape.r + shape.rad[0] - cr);
		} else if (shape.r + shape.h - shape.rad[1] >= cr) { // hit left
			ec = shape.c;
		} else { // hit bottom-left
			ec = shape.c + cr - (shape.r + shape.h - shape.rad[1]);
		}
		pr_buf[0].shape.ss.r = shape.r;
		pr_buf[0].shape.ss.c = cr < shape.r + shape.rad[0] ? ec : shape.c;
		pr_buf[0].shape.ss.h = cr - shape.r;
		pr_buf[0].shape.ss.w = (cr < shape.r + shape.rad[3] ? cc : shape.c + shape.w) - pr_buf[0].shape.ss.c;
		pr_buf[0].shape.ss.rad[0] = min((int)shape.rad[0], cr - shape.r);
		pr_buf[0].shape.ss.rad[1] = max(0, shape.rad[1] - (shape.r + shape.h - cr));
		pr_buf[0].shape.ss.rad[2] = max(0, shape.rad[2] - (shape.r + shape.h - cr));
		pr_buf[0].shape.ss.rad[3] = min((int)shape.rad[3], cr - shape.r);
		pr_buf[1].shape.ss.r = cr;
		pr_buf[1].shape.ss.c = cr > shape.r + shape.h - shape.rad[1] ? ec : shape.c;
		pr_buf[1].shape.ss.h = shape.r + shape.h - cr;
		pr_buf[1].shape.ss.w = (cr > shape.r + shape.h - shape.rad[2] ? cc : shape.c + shape.w) - pr_buf[1].shape.ss.c;
		pr_buf[1].shape.ss.rad[0] = shape.rad[0] - pr_buf[0].shape.ss.rad[0];
		pr_buf[1].shape.ss.rad[1] = shape.rad[1] - pr_buf[0].shape.ss.rad[1];
		pr_buf[1].shape.ss.rad[2] = shape.rad[2] - pr_buf[0].shape.ss.rad[2];
		pr_buf[1].shape.ss.rad[3] = shape.rad[3] - pr_buf[0].shape.ss.rad[3];
	} else if (dir == 6) {
		if (shape.r + shape.h - shape.rad[1] + shape.c >= cr + cc) { // hit left
			ec = shape.c;
			er = cr + cc - ec;
		} else if (shape.r + shape.h + (shape.c + shape.rad[1]) > cr + cc) { // hit bottom-left
			int move = (shape.r + shape.h + (shape.c + shape.rad[1])) - (cr + cc);
			if (move % 2 != 0) {
				return invalid_cut;
			}
			er = shape.r + shape.h - move / 2;
			ec = shape.c + shape.rad[1] - move / 2;
		} else { // hit bottom
			er = shape.r + shape.h;
			ec = cr + cc - er;
		}
		pr_buf[0].shape.ss.r = shape.r;
		pr_buf[0].shape.ss.c = shape.c;
		pr_buf[0].shape.ss.h = er - shape.r;
		pr_buf[0].shape.ss.w = cc - shape.c;
		pr_buf[0].shape.ss.rad[0] = shape.rad[0];
		pr_buf[0].shape.ss.rad[1] = min((int)shape.rad[1], ec - shape.c);
		pr_buf[0].shape.ss.rad[2] = er - cr;
		pr_buf[0].shape.ss.rad[3] = min((int)shape.rad[3], cr - shape.r);
		pr_buf[1].shape.ss.r = cr;
		pr_buf[1].shape.ss.c = ec;
		pr_buf[1].shape.ss.h = shape.r + shape.h - cr;
		pr_buf[1].shape.ss.w = shape.c + shape.w - ec;
		pr_buf[1].shape.ss.rad[0] = pr_buf[0].shape.ss.rad[2];
		pr_buf[1].shape.ss.rad[1] = shape.rad[1] - pr_buf[0].shape.ss.rad[1];
		pr_buf[1].shape.ss.rad[2] = shape.rad[2];
		pr_buf[1].shape.ss.rad[3] = shape.rad[3] - pr_buf[0].shape.ss.rad[3];
	} else {
		assert(dir == 7);
		ec = cc;
		if (shape.c + shape.rad[1] > cc) { // hit bottom-left
			er = shape.r + shape.h - shape.rad[1] + (cc - shape.c);
		} else if (shape.c + shape.w - shape.rad[2] >= cc) { // hit bottom
			er = shape.r + shape.h;
		} else { // hit bottom-right
			er = shape.r + shape.h - shape.rad[2] + (shape.c + shape.w - cc);
		}
		pr_buf[0].shape.ss.r = cc < shape.c + shape.rad[0] ? cr : shape.r;
		pr_buf[0].shape.ss.c = shape.c;
		pr_buf[0].shape.ss.h = (cc < shape.c + shape.rad[1] ? er : shape.r + shape.h) - pr_buf[0].shape.ss.r;
		pr_buf[0].shape.ss.w = cc - shape.c;
		pr_buf[0].shape.ss.rad[0] = min((int)shape.rad[0], cc - shape.c);
		pr_buf[0].shape.ss.rad[1] = min((int)shape.rad[1], cc - shape.c);
		pr_buf[0].shape.ss.rad[2] = max(0, shape.rad[2] - (shape.c + shape.w - cc));
		pr_buf[0].shape.ss.rad[3] = max(0, shape.rad[3] - (shape.c + shape.w - cc));
		pr_buf[1].shape.ss.r = cc > shape.c + shape.w - shape.rad[3] ? cr : shape.r;
		pr_buf[1].shape.ss.c = cc;
		pr_buf[1].shape.ss.h = (cc > shape.c + shape.w - shape.rad[2] ? er : shape.r + shape.h) - pr_buf[1].shape.ss.r;
		pr_buf[1].shape.ss.w = shape.c + shape.w - cc;
		pr_buf[1].shape.ss.rad[0] = shape.rad[0] - pr_buf[0].shape.ss.rad[0];
		pr_buf[1].shape.ss.rad[1] = shape.rad[1] - pr_buf[0].shape.ss.rad[1];
		pr_buf[1].shape.ss.rad[2] = shape.rad[2] - pr_buf[0].shape.ss.rad[2];
		pr_buf[1].shape.ss.rad[3] = shape.rad[3] - pr_buf[0].shape.ss.rad[3];
	}
	cur_cut.e.r = er;
	cur_cut.e.c = ec;
	pr_buf[0].area = area_shape(pr_buf[0].shape.ss);
	pr_buf[1].area = results[idx].area - pr_buf[0].area;
	pr_buf[0].rose = rose_count_shape(pr_buf[0].shape.ss);
	pr_buf[1].rose = rose_count_shape(pr_buf[1].shape.ss);
	assert (pr_buf[0].area + area_shape(pr_buf[1].shape.ss) == results[idx].area);
	// {
	// 	debug("(%d %d)-(%d %d) %d %d %d\n", cur_cut.s.r, cur_cut.s.c, cur_cut.e.r, cur_cut.e.c, area_shape(pr_buf[0].shape.ss), area_shape(pr_buf[1].shape.ss), results[idx].area);
	// 	debug_simple_shape(pr_buf[0].shape.ss);
	// 	debug_simple_shape(pr_buf[1].shape.ss);
	// 	exit(1);
	// }
	return cur_cut;
}

Cut cut_simple(int idx, int size) {
	const SimpleShape& shape = results[idx].shape.ss;
	int cp = rnd.nextUInt(size);
	const int left = shape.h - shape.rad[0] - shape.rad[1];
	const int bottom = shape.w - shape.rad[1] - shape.rad[2];
	const int right = shape.h - shape.rad[2] - shape.rad[3];
	const int top = shape.w - shape.rad[3] - shape.rad[0];
	int dir_cand[4];
	const int dir_idx = rnd.nextUInt() & 3;
	if (cp < shape.rad[0]) { // top-left
		if (cp == 0 && top == 0 && shape.rad[3] == 0) return invalid_cut;
		dir_cand[0] = dir_cand[1] = (cp == 0 && top == 0 ? 7 : 0);
		dir_cand[2] = 7;
		dir_cand[3] = cp == 0 ? 7 : 1;
		return cut_simple_from(idx, shape.r + cp, shape.c + shape.rad[0] - cp, dir_cand[dir_idx]);
	}
	cp -= shape.rad[0];
	if (cp < left) { // left
		if (cp == 0 && shape.rad[0] == 0 && top == 0) return invalid_cut;
		dir_cand[0] = dir_cand[1] = (cp == 0 && shape.rad[0] == 0 ? 0 : 1);
		dir_cand[2] = 0;
		dir_cand[3] = cp == 0 ? 0 : 2;
		return cut_simple_from(idx, shape.r + shape.rad[0] + cp, shape.c, dir_cand[dir_idx]);
	}
	cp -= left;
	if (cp < shape.rad[1]) { // bottom-left
		if (cp == 0 && left == 0 && shape.rad[0] == 0) return invalid_cut;
		dir_cand[0] = dir_cand[1] = (cp == 0 && left == 0 ? 1 : 2);
		dir_cand[2] = 1;
		dir_cand[3] = cp == 0 ? 1 : 3;
		return cut_simple_from(idx, shape.r + shape.rad[0] + left + cp, shape.c + cp, dir_cand[dir_idx]);
	}
	cp -= shape.rad[1];
	if (cp < bottom) { // bottom
		if (cp == 0 && shape.rad[1] == 0 && left == 0) return invalid_cut;
		dir_cand[0] = dir_cand[1] = (cp == 0 && shape.rad[1] == 0 ? 2 : 3);
		dir_cand[2] = 2;
		dir_cand[3] = cp == 0 ? 2 : 4;
		return cut_simple_from(idx, shape.r + shape.h, shape.c + shape.rad[1] + cp, dir_cand[dir_idx]);
	}
	cp -= bottom;
	if (cp < shape.rad[2]) { // bottom-right
		if (cp == 0 && bottom == 0 && shape.rad[1] == 0) return invalid_cut;
		dir_cand[0] = dir_cand[1] = (cp == 0 && bottom == 0 ? 3 : 4);
		dir_cand[2] = 3;
		dir_cand[3] = cp == 0 ? 3 : 5;
		return cut_simple_from(idx, shape.r + shape.h - cp, shape.c + shape.rad[1] + bottom + cp, dir_cand[dir_idx]);
	}
	cp -= shape.rad[2];
	if (cp < right) { // right
		if (cp == 0 && shape.rad[2] == 0 && bottom == 0) return invalid_cut;
		dir_cand[0] = dir_cand[1] = (cp == 0 && shape.rad[2] == 0 ? 4 : 5);
		dir_cand[2] = 4;
		dir_cand[3] = cp == 0 ? 4 : 6;
		return cut_simple_from(idx, shape.r + shape.h - shape.rad[2] - cp, shape.c + shape.w, dir_cand[dir_idx]);
	}
	cp -= right;
	if (cp < shape.rad[3]) { // top-right
		if (cp == 0 && right == 0 && shape.rad[2] == 0) return invalid_cut;
		dir_cand[0] = dir_cand[1] = (cp == 0 && right == 0 ? 5 : 6);
		dir_cand[2] = 5;
		dir_cand[3] = cp == 0 ? 5 : 7;
		return cut_simple_from(idx, shape.r + shape.rad[3] - cp, shape.c + shape.w - cp, dir_cand[dir_idx]);
	}
	cp -= shape.rad[3];
	// top
	assert(cp < top);
	if (cp == 0 && shape.rad[3] == 0 && right == 0) return invalid_cut;
	dir_cand[0] = dir_cand[1] = (cp == 0 && shape.rad[3] == 0 ? 6 : 7);
	dir_cand[2] = 6;
	dir_cand[3] = cp == 0 ? 6 : 0;
	return cut_simple_from(idx, shape.r, shape.c + shape.w - shape.rad[3] - cp, dir_cand[dir_idx]);
}

void cut_complex(int idx, int cp1, int cp2) {
	const ComplexShape& shape = results[idx].shape.cs;
	START_TIMER(11);
	if (cp1 > cp2) swap(cp1, cp2);
	int e1 = __builtin_ctz(edge_idx[pos_buf[cp1].r][pos_buf[cp1].c]);
	int e2 = __builtin_ctz(edge_idx[pos_buf[cp2].r][pos_buf[cp2].c]);
	int pi = 0;
	for (int i = 0; i <= e1; ++i) {
		pr_buf[0].shape.cs.p[pi++] = shape.p[i];
	}
	if (shape.p[e1].r != pos_buf[cp1].r || shape.p[e1].c != pos_buf[cp1].c) {
		pr_buf[0].shape.cs.p[pi++] = pos_buf[cp1];
	}
	pr_buf[0].shape.cs.p[pi++] = pos_buf[cp2];
	if (e2 < shape.size - 1 && (shape.p[e2 + 1].r != pos_buf[cp2].r || shape.p[e2 + 1].c != pos_buf[cp2].c)) {
		pr_buf[0].shape.cs.p[pi++] = shape.p[e2 + 1];
	}
	for (int i = e2 + 2; i < shape.size; ++i) {
		pr_buf[0].shape.cs.p[pi++] = shape.p[i];
	}
	pr_buf[0].shape.cs.size = pi;

	pi = 0;
	if (shape.p[e1 + 1].r != pos_buf[cp1].r || shape.p[e1 + 1].c != pos_buf[cp1].c) {
		pr_buf[1].shape.cs.p[pi++] = pos_buf[cp1];
	}
	for (int i = e1 + 1; i <= e2; ++i) {
		pr_buf[1].shape.cs.p[pi++] = shape.p[i];
	}
	pr_buf[1].shape.cs.p[pi++] = pos_buf[cp2];
	pr_buf[1].shape.cs.size = pi;
	STOP_TIMER(11);

	START_TIMER(12);
	pr_buf[0].area = area_shape(pr_buf[0].shape.cs);
	pr_buf[1].area = results[idx].area - pr_buf[0].area;
	STOP_TIMER(12);
	START_TIMER(13);
	pr_buf[0].rose = rose_count_shape(pr_buf[0].shape.cs);
	STOP_TIMER(13);
	START_TIMER(14);
	pr_buf[1].rose = results[idx].rose - pr_buf[0].rose;
	pr_buf[1].rose -= rose_count_on_line(pos_buf[cp1].r, pos_buf[cp1].c, pos_buf[cp2].r, pos_buf[cp2].c);
	STOP_TIMER(14);
	// debug("cut:(%d %d)-(%d %d) e1:%d e2:%d\n", pos_buf[cp1].r, pos_buf[cp1].c, pos_buf[cp2].r, pos_buf[cp2].c, e1, e2);
	// debug_complex_shape(pr_buf[0].shape.cs);
	// debug_complex_shape(pr_buf[1].shape.cs);
	// debug("area:%d %d rose:%d %d\n", pr_buf[0].area, pr_buf[1].area, pr_buf[0].rose, pr_buf[1].rose);
	assert(pr_buf[0].area + area_shape(pr_buf[1].shape.cs) == results[idx].area);
	assert(rose_count_shape(pr_buf[1].shape.cs) == pr_buf[1].rose);
}

void eval_area(int area, int area1, int count, int& count1) {
	int area2 = area - area1;
	count1 = (count * area1 + area / 2) / area;
	if (count1 == 0) count1 = 1;
	int count2 = count - count1;
	if (count2 == 0) {
		count2 = 1;
		count1--;
	}
	double best_area_diff = fabs(1.0 * area1 / count1 - 1.0 * area2 / count2);
	if (count1 > 1) {
		double area_diff = fabs(1.0 * area1 / (count1 - 1) - 1.0 * area2 / (count2 + 1));
		if (area_diff < best_area_diff) {
			count1--;
			count2++;
		}
	}
	if (count2 > 1) {
		double area_diff = fabs(1.0 * area1 / (count1 + 1) - 1.0 * area2 / (count2 - 1));
		if (area_diff < best_area_diff) {
			count1++;
			count2--;
		}
	}
}

int points_on_edge(const Point& p1, const Point& p2) {
	const int dr = abs(p2.r - p1.r);
	const int dc = abs(p2.c - p1.c);
	return __gcd(dr, dc) - 1;
}

bool dfs(int start_pos, double best_score) {
	int loop = results[start_pos].count * 2 - 1;
	double sum_area = 0;
	double sum2_area = 0;
	double sum_rose = 0;
	double sum2_rose = 0;
	int seen = 0;
	for (int idx = 1; idx < start_pos; ++idx) {
		if (best_results[idx].count == 1) {
			++seen;
			sum_area += best_results[idx].area;
			sum2_area += sq(best_results[idx].area);
			sum_rose += best_results[idx].rose;
			sum2_rose += sq(best_results[idx].rose);
		}
	}
	for (int idx = start_pos + loop; idx < best_results.size(); ++idx) {
		if (best_results[idx].count == 1) {
			++seen;
			sum_area += best_results[idx].area;
			sum2_area += sq(best_results[idx].area);
			sum_rose += best_results[idx].rose;
			sum2_rose += sq(best_results[idx].rose);
		}
	}
	for (int idx = start_pos; idx < start_pos + loop; ++idx) {
		// debug("dfs idx:%d count:%d area:%d rose:%d (%d,%d)-(%d,%d) [%d %d %d %d]\n", idx, results[idx].count, results[idx].area, results[idx].rose,
		// 	   shape.r, shape.c, shape.r + shape.h, shape.c + shape.w, shape.rad[0], shape.rad[1], shape.rad[2], shape.rad[3]);
		if (results[idx].count == 1) {
			++seen;
			sum_area += results[idx].area;
			sum2_area += sq(results[idx].area);
			sum_rose += results[idx].rose;
			sum2_rose += sq(results[idx].rose);
			double rest_area = seen == N ? 0 : (2.0 * H * W - sum_area) / (N - seen);
			double optimal_stdev_area = sqrt((sum2_area + (N - seen) * sq(rest_area)) / N - sq(ave_area)) / 2;
			double ave_rose = sum_rose / seen;
			double optimal_stdev_rose = sqrt((sum2_rose + (N - seen) * sq(ave_rose)) / N - sq(ave_rose));
			if ((1 + optimal_stdev_area) * (1 + optimal_stdev_rose) >= best_score) {
				return false;
			}
			continue;
		}
		const int REP = idx == start_pos ? 10 : 90;
		Cut best_cut;
		double best_metrics = 1e100;
		int best_count1 = 0;
		const double rev_c = 1.0 / (results[idx].count + seen);
		if (results[idx].count > COMPLEX_TH) {
			const SimpleShape& shape = results[idx].shape.ss;
			int size = 2 * (shape.h + shape.w) - shape.rad[0] - shape.rad[1] - shape.rad[2] - shape.rad[3];
			for (int i = 0; i < REP; ++i) {
				START_TIMER(0);
				Cut cur_cut = cut_simple(idx, size);
				STOP_TIMER(0);
				if (cur_cut.s.r == 0xFF) continue;
				START_TIMER(1);
				int count1;
				eval_area(results[idx].area, pr_buf[0].area, results[idx].count, count1);
				int count2 = results[idx].count - count1;

				double op_area_1 = 1.0 * pr_buf[0].area / count1;
				double op_area_2 = 1.0 * pr_buf[1].area / count2;
				double op_rose_1 = 1.0 * pr_buf[0].rose / count1;
				double op_rose_2 = 1.0 * pr_buf[1].rose / count2;
				double stdev_a = (sum2_area + sq(op_area_1) * count1 + sq(op_area_2) * count2) * rev_c - sq((sum_area + results[idx].area) * rev_c);
				double stdev_r = (sum2_rose + sq(op_rose_1) * count1 + sq(op_rose_2) * count2) * rev_c - sq((sum_rose + pr_buf[0].rose + pr_buf[1].rose) * rev_c);
				double metrics = (sqrt(stdev_a) * 0.8 + 1) * (sqrt(stdev_r) + 1);
				if (metrics < best_metrics) {
					best_metrics = metrics;
					best_cut = cur_cut;
					best_count1 = count1;
					results[idx + 1] = pr_buf[0];
					results[idx + best_count1 * 2] = pr_buf[1];
				}
				STOP_TIMER(1);
			}
		} else {
			// debug("cut complex:%d %d\n", idx, results[idx].count);
			// debug_complex_shape(results[idx].shape.cs);
			int size = set_pos_buf(results[idx].shape.cs);
			if (size <= 3) {
				ADD_COUNTER(2);
				return false;
			}
			if (results[idx].count >= 3 && (rnd.nextUInt() & 1)) {
				for (int i = 0; i < REP; ++i) {
					START_TIMER(10);
					int cp1 = rnd.nextUInt(size);
					int cp2 = rnd.nextUInt(size - 3) + cp1 + 2;
					if (cp2 >= size) cp2 -= size;
					while (true) {
						if ((edge_idx[pos_buf[cp1].r][pos_buf[cp1].c] & edge_idx[pos_buf[cp2].r][pos_buf[cp2].c]) == 0) break;
						cp1 = rnd.nextUInt(size);
						cp2 = rnd.nextUInt(size - 3) + cp1 + 2;
						if (cp2 >= size) cp2 -= size;
					}
					STOP_TIMER(10);
					START_TIMER(5);
					cut_complex(idx, cp1, cp2);
					STOP_TIMER(5);
					START_TIMER(6);
					int count1;
					eval_area(results[idx].area, pr_buf[0].area, results[idx].count, count1);
					int count2 = results[idx].count - count1;

					double op_area_1 = 1.0 * pr_buf[0].area / count1;
					double op_area_2 = 1.0 * pr_buf[1].area / count2;
					double op_rose_1 = 1.0 * pr_buf[0].rose / count1;
					double op_rose_2 = 1.0 * pr_buf[1].rose / count2;
					double stdev_a = (sum2_area + sq(op_area_1) * count1 + sq(op_area_2) * count2) * rev_c - sq((sum_area + results[idx].area) * rev_c);
					double stdev_r = (sum2_rose + sq(op_rose_1) * count1 + sq(op_rose_2) * count2) * rev_c - sq((sum_rose + pr_buf[0].rose + pr_buf[1].rose) * rev_c);
					double metrics = (sqrt(stdev_a) * 0.8 + 1) * (sqrt(stdev_r) + 1) - points_on_edge(pos_buf[cp1], pos_buf[cp2]) * (results[idx].count - 2) * 0.1 / (H + W);
					if (metrics < best_metrics) {
						best_metrics = metrics;
						best_cut = {pos_buf[cp1], pos_buf[cp2]};
						best_count1 = count1;
						results[idx + 1] = pr_buf[0];
						results[idx + best_count1 * 2] = pr_buf[1];
					}
					STOP_TIMER(6);
				}
			} else {
				for (int count1 = 1; count1 <= results[idx].count / 2; ++count1) {
					const int target_area = (int)(ave_area * count1);
					if (results[idx].area <= target_area) break;
					int cp1 = 0;
					int cp2 = 1;
					int area = 0;
					while (true) {
						const int dr1 = pos_buf[cp2].r - pos_buf[cp1].r;
						const int dc1 = pos_buf[cp2].c - pos_buf[cp1].c;
						bool do_cut = false;
						if (area <= target_area) {
							int npi = cp2 == size - 1 ? 0 : cp2 + 1;
							const int dr2 = pos_buf[npi].r - pos_buf[cp1].r;
							const int dc2 = pos_buf[npi].c - pos_buf[cp1].c;
							area += dr1 * dc2 - dr2 * dc1;
							cp2 = npi;
							do_cut = target_area < area && area < results[idx].area;
						} else {
							if (cp1 == size - 1) break;
							int npi = cp1 + 1;
							const int dr2 = pos_buf[npi].r - pos_buf[cp1].r;
							const int dc2 = pos_buf[npi].c - pos_buf[cp1].c;
							area += dr1 * dc2 - dr2 * dc1;
							cp1 = npi;
							do_cut = 0 < area && area <= target_area;
						}
						if (do_cut) {
							ADD_COUNTER(6);
							START_TIMER(2);
							cut_complex(idx, cp1, cp2);
							STOP_TIMER(2);
							START_TIMER(3);
							int count2 = results[idx].count - count1;
							double op_area_1 = 1.0 * pr_buf[0].area / count1;
							double op_area_2 = 1.0 * pr_buf[1].area / count2;
							double op_rose_1 = 1.0 * pr_buf[0].rose / count1;
							double op_rose_2 = 1.0 * pr_buf[1].rose / count2;
							double stdev_a = (sum2_area + sq(op_area_1) * count1 + sq(op_area_2) * count2) * rev_c - sq((sum_area + results[idx].area) * rev_c);
							double stdev_r = (sum2_rose + sq(op_rose_1) * count1 + sq(op_rose_2) * count2) * rev_c - sq((sum_rose + pr_buf[0].rose + pr_buf[1].rose) * rev_c);
							double metrics = (sqrt(stdev_a) * 0.8 + 1) * (sqrt(stdev_r) + 1) - points_on_edge(pos_buf[cp1], pos_buf[cp2]) * (results[idx].count - 2) * 0.1 / (H + W);
							if (metrics < best_metrics) {
								best_metrics = metrics;
								best_cut = {pos_buf[cp1], pos_buf[cp2]};
								best_count1 = count1;
								results[idx + 1] = pr_buf[0];
								results[idx + best_count1 * 2] = pr_buf[1];
							}
							STOP_TIMER(3);
						}
					}
				}
			}
		}
		if (best_metrics >= 1e100) {
			ADD_COUNTER(3);
			return false;
		}
		// debug("best_cut:%d (%d %d)-(%d %d) %d\n", idx, best_cut.s.r, best_cut.s.c, best_cut.e.r, best_cut.e.c, best_count1);
		START_TIMER(4);
		results[idx].cut = best_cut;
		const int idx1 = idx + 1;
		const int idx2 = idx + best_count1 * 2;
		results[idx1].count = best_count1;
		results[idx2].count = results[idx].count - best_count1;
		if (results[idx].count > COMPLEX_TH) {
			if (results[idx1].count <= COMPLEX_TH) convert_simple_complex(results[idx1]);
			if (results[idx2].count <= COMPLEX_TH) convert_simple_complex(results[idx2]);
		}
		STOP_TIMER(4);
	}
	return true;
}

double solve_dfs(double prev_best_score, ll time_limit) {
	double best_score = 1e9;
	for (int turn = 1; ; ++turn) {
		if ((turn & 0x1F) == 0) {
			auto elapsed = get_elapsed_msec();
			if (elapsed > time_limit) {
				debug("turn:%d score:%.5f\n", turn, best_score);
				break;
			}
		}
		target_rose = (rnd.nextDouble() * 0.2 + 0.8) * R / N;
		bool success = dfs(0, best_score);
		if (!success) continue;
		double sd_a = 0;
		double sd_r = 0;
		int sum_r = 0;
		for (int i = 0; i < results.size(); ++i) {
			if (results[i].count == 1) {
				sd_a += sq(results[i].area);
				sd_r += sq(results[i].rose);
				sum_r += results[i].rose;
			}
		}
		sd_a = sqrt(sd_a / N - sq(ave_area)) / 2;
		sd_r = sqrt(sd_r / N - sq(1.0 * sum_r / N));
		double score = (1 + sd_a) * (1 + sd_r);
		if (score < best_score) {
			best_score = score;
			if (score < prev_best_score) {
				best_results = results;
			}
			debug("score:%.5f sd_a:%.5f sd_r:%.5f at turn:%d\n", score, sd_a, sd_r, turn);
			if (best_score == 1.0) return best_score;
		}
	}
	return best_score;
}

void update_cand_pos(vi& v, int min_count, int max_count) {
	v.clear();
	for (int i = 0; i < best_results.size(); ++i) {
		if (min_count <= best_results[i].count && best_results[i].count <= max_count) {
			v.push_back(i);
		}
	}
	if (v.empty()) {
		for (int i = 0; i < best_results.size(); ++i) {
			if (3 <= best_results[i].count) {
				v.push_back(i);
			}
		}
	}
}

double update_dfs(double best_score, ll time_limit) {
	if (best_score == 1.0) return 1.0;
	results = best_results;
	target_rose = 0;
	for (int i = 0; i < results.size(); ++i) {
		if (results[i].count == 1) {
			target_rose += results[i].rose;
		}
	}
	target_rose /= N;
	const ll begin_time = get_elapsed_msec();
	vector<PartialResult> ans_results = best_results;
	const int initial_max_count = min(40, N + 1);
	const int initial_min_count = min(N / 2, 20);
	const int final_max_count = min(N, 15);
	const int final_min_count = 3;
	int max_count = initial_max_count;
	int min_count = initial_min_count;
	double cur_score = best_score;
	vi cand_pos;
	update_cand_pos(cand_pos, min_count, max_count);
	for (int turn = 1; ; ++turn) {
		if ((turn & 0xFF) == 0) {
			auto elapsed = get_elapsed_msec();
			if (elapsed > time_limit) {
				debug("turn:%d score:%.5f\n", turn, best_score);
				break;
			}
			double ratio = 1.0 * (elapsed - begin_time) / (TL - begin_time);
			double c1 = log(INITIAL_COOLER);
			double c2 = log(FINAL_COOLER);
			cooler = elapsed > TL - 50 ? 100000 : exp(c1 + (c2 - c1) * ratio);
			max_count = min(N, final_max_count + (int)((initial_max_count - final_max_count) * (1-ratio)));
			min_count = final_min_count + (int)((initial_min_count - final_min_count) * (1-ratio));
			update_cand_pos(cand_pos, min_count, max_count);
			debug("cooler:%.4f score:%.5f count:%d-%d\n", cooler, cur_score, min_count, max_count);
		}
		int pos = cand_pos[rnd.nextUInt(cand_pos.size())];
		const int loop_count = best_results[pos].count * 2 - 1;
		results[pos] = best_results[pos];
		bool success = dfs(pos, best_score);
		if (!success) continue;
		double sd_a = 0;
		double sd_r = 0;
		int sum_r = 0;
		for (int i = 1; i < pos; ++i) {
			if (best_results[i].count == 1) {
				sd_a += sq(best_results[i].area);
				sd_r += sq(best_results[i].rose);
				sum_r += best_results[i].rose;
			}
		}
		for (int i = pos + 1; i < pos + loop_count; ++i) {
			if (results[i].count == 1) {
				sd_a += sq(results[i].area);
				sd_r += sq(results[i].rose);
				sum_r += results[i].rose;
			}
		}
		for (int i = pos + loop_count; i < best_results.size(); ++i) {
			if (best_results[i].count == 1) {
				sd_a += sq(best_results[i].area);
				sd_r += sq(best_results[i].rose);
				sum_r += best_results[i].rose;
			}
		}
		sd_a = sqrt(sd_a / N - sq(ave_area)) / 2;
		sd_r = sqrt(sd_r / N - sq(1.0 * sum_r / N));
		double score = (1 + sd_a) * (1 + sd_r);
		if (accept(score - cur_score)) {
			debug("%.4f -> %.4f %d\n", cur_score, score, results[pos].count);
			cur_score = score;
			target_rose = 1.0 * sum_r / N;
			copy(results.begin() + pos, results.begin() + pos + loop_count, best_results.begin() + pos);
			update_cand_pos(cand_pos, min_count, max_count);
			if (score < best_score) {
				best_score = score;
				ans_results = best_results;
				debug("score:%.5f sd_a:%.5f sd_r:%.5f at turn:%d\n", score, sd_a, sd_r, turn);
				if (best_score == 1.0) break;
			}
		}
	}
	best_results = ans_results;
	return best_score;
}

// double dp_simple() {
// 	for (int h = 1; h <= H; ++h) {
// 		for (int w = 1; w <= W; ++w) {
// 			for (int r = 0; r + h <= H; ++r) {
// 				for (int c = 0; c + w <= W; ++c) {
// 					int count = (h * w * N + H * W / 2) / (H * W);
// 					if (count <= 1) {
// 						dp[r][c][h-1][w-1].count = 1;
// 						dp[r][c][h-1][w-1].area_d = sq(h * w);
// 						dp[r][c][h-1][w-1].rose_d = sq(rose_count_rect(r, c, h, w));
// 						continue;
// 					}
// 					if (count > N) count = N;
// 					dp[r][c][h-1][w-1].count = count;
// 					dp[r][c][h-1][w-1].area_d = 1 << 30;
// 					uint64_t best = 1ull << 60;
// 					for (int p = 0; p < h - 1; ++p) {
// 						const DpResult& res1 = dp[r][c][p][w-1];
// 						const DpResult& res2 = dp[r+p+1][c][h-p-2][w-1];
// 						if (res1.count + res2.count != count) continue;
// 						int ad = res1.area_d + res2.area_d;
// 						int rd = res1.rose_d + res2.rose_d;
// 						uint64_t metrics = (uint64_t)(1 + ad) * (1 + rd);
// 						if (metrics < best) {
// 							best = metrics;
// 							dp[r][c][h-1][w-1].cut_pos = (uint8_t)p;
// 							dp[r][c][h-1][w-1].area_d = ad;
// 							dp[r][c][h-1][w-1].rose_d = rd;
// 						}
// 					}
// 					for (int p = 0; p < w - 1; ++p) {
// 						const DpResult& res1 = dp[r][c][h-1][p];
// 						const DpResult& res2 = dp[r][c+p+1][h-1][w-p-2];
// 						if (res1.count + res2.count != count) continue;
// 						int ad = res1.area_d + res2.area_d;
// 						int rd = res1.rose_d + res2.rose_d;
// 						uint64_t metrics = (uint64_t)(1 + ad) * (1 + rd);
// 						if (metrics < best) {
// 							best = metrics;
// 							dp[r][c][h-1][w-1].cut_pos = (uint8_t)(p + h - 1);
// 							dp[r][c][h-1][w-1].area_d = ad;
// 							dp[r][c][h-1][w-1].rose_d = rd;
// 						}
// 					}
// 				}
// 			}
// 		}
// 	}
// 	const DpResult& whole_result = dp[0][0][H-1][W-1];
// 	const double stdev_area = sqrt(1.0 * whole_result.area_d / N - sq(1.0 * H * W / N));
// 	const double stdev_rose = sqrt(1.0 * whole_result.rose_d / N - sq(1.0 * R / N));
// 	const double score = (1 + stdev_area) * (1 + stdev_rose);
// 	debug("dp result: %.6f\n", score);
// 	debug("dp elapsed:%lld\n", get_elapsed_msec());
// 	return score;
// }

// void get_result_dp(int idx, uint8_t r, uint8_t c, uint8_t h, uint8_t w) {
// 	const DpResult& res = dp[r][c][h-1][w-1];
// 	best_results[idx].shape.ss = {r, c, h, w, {}};
// 	best_results[idx].area = h * w * 2;
// 	best_results[idx].rose = rose_count_rect(r, c, h, w);
// 	best_results[idx].count = res.count;
// 	if (res.count == 1) return;
// 	if (res.count <= COMPLEX_TH) {
// 		convert_simple_complex(best_results[idx]);
// 	}
// 	if (res.cut_pos < h - 1) {
// 		uint8_t nh = res.cut_pos + 1;
// 		best_results[idx].cut = {{(uint8_t)(r + nh), c}, {(uint8_t)(r + nh), (uint8_t)(c + w)}};
// 		get_result_dp(idx + 1, r, c, nh, w);
// 		get_result_dp(idx + dp[r][c][nh-1][w-1].count * 2, (uint8_t)(r + nh), c, (uint8_t)(h - nh), w);
// 	} else {
// 		uint8_t nw = res.cut_pos - (h - 1) + 1;
// 		best_results[idx].cut = {{r, (uint8_t)(c + nw)}, {(uint8_t)(r + h), (uint8_t)(c + nw)}};
// 		get_result_dp(idx + 1, r, c, h, nw);
// 		get_result_dp(idx + dp[r][c][h-1][nw-1].count * 2, r, c + nw, h, w - nw);
// 	}
// }

vector<Cut> solve() {
	best_results.resize(2 * N - 1);
	results.resize(N * 2 - 1);
	results[0].area = H * W * 2;
	results[0].rose = R;
	results[0].count = N;
	double dfs_score = 1e9;
	int orig_complex_th = COMPLEX_TH;
	for (COMPLEX_TH = orig_complex_th; COMPLEX_TH >= 2 && dfs_score == 1e9; --COMPLEX_TH) {
		results[0].shape.ss = {0, 0, (uint8_t)H, (uint8_t)W, {}};
		if (N <= COMPLEX_TH) convert_simple_complex(results[0]);
		dfs_score = solve_dfs(dfs_score, TL * (orig_complex_th - COMPLEX_TH + 1) / 8);
	}
	if (dfs_score == 1e9) {
		return vector<Cut>();
	}
	++COMPLEX_TH;

	for (int i = 0; i < 2 * N - 1; ++i) {
		if (COMPLEX_TH < best_results[i].count && best_results[i].count <= orig_complex_th) {
			convert_simple_complex(best_results[i]);
		}
	}
	COMPLEX_TH = orig_complex_th;
	dfs_score = update_dfs(dfs_score, TL);

#ifdef DEBUG
	vi hist_area(H * W * 5 / N);
	vi hist_rose(R * 5 / N);
	int min_area = H * W * 2;
	int max_area = 0;
	int min_rose = R;
	int max_rose = 0;
	int sum_rose = 0;
	for (int i = 0; i < best_results.size(); ++i) {
		if (best_results[i].count == 1) {
			hist_area[best_results[i].area]++;
			min_area = min(min_area, best_results[i].area);
			max_area = max(max_area, best_results[i].area);
			hist_rose[best_results[i].rose]++;
			min_rose = min(min_rose, best_results[i].rose);
			max_rose = max(max_rose, best_results[i].rose);
			sum_rose += best_results[i].rose;
		}
	}
	debug("area:%.4f\n", ave_area);
	for (int i = min_area; i <= max_area; ++i) {
		if (hist_area[i] > 0) debug("%3d:%d\n", i, hist_area[i]);
	}
	debug("rose:%.4f\n", 1.0 * sum_rose / N);
	for (int i = min_rose; i <= max_rose; ++i) {
		if (hist_rose[i] > 0) debug("%3d:%d\n", i, hist_rose[i]);
	}
#endif

	vector<Cut> ans;
	for (int i = 0; i < best_results.size(); ++i) {
		if (best_results[i].count > 1) {
			ans.push_back(best_results[i].cut);
		}
	}
	return ans;
}

class CakeSharing {
public:
	vector<string> cut(const vector<string>& roses, int NP);
};

vector<string> CakeSharing::cut(const vector<string>& roses, int NP) {
	start_time = get_elapsed_msec();
	H = roses.size();
	W = roses[0].size();
	N = NP;
	ave_area = 2.0 * H * W / N;
	orig_field.assign(H, vector<bool>(W));
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			orig_field[i][j] = roses[i][j] == 'R';
			if (orig_field[i][j]) ++R;
			rose_count[i+1][j+1] = rose_count[i][j+1] + rose_count[i+1][j] - rose_count[i][j] + (int)orig_field[i][j];
			rose_count_vert[j][i+1] = rose_count_vert[j][i] + (int)orig_field[i][j];
			rose_count_horz[i][j+1] = rose_count_horz[i][j] + (int)orig_field[i][j];
		}
	}
	for (int i = 1; i <= W; ++i) {
		for (int j = 0; j <= H; ++j) {
			for (int l = 1; l <= H - j; ++l) {
				rose_count_tri[3][j][i][l] = rose_count_tri[3][j][i-1][l-1] + (rose_count_vert[i-1][j+l] - rose_count_vert[i-1][j]);
			}
			for (int l = 1; l <= j; ++l) {
				rose_count_tri[2][j][i][l] = rose_count_tri[2][j][i-1][l-1] + (rose_count_vert[i-1][j] - rose_count_vert[i-1][j-l]);
			}
		}
	}
	for (int i = W - 1; i >= 0; --i) {
		for (int j = 0; j <= H; ++j) {
			for (int l = 1; l <= H - j; ++l) {
				rose_count_tri[0][j][i][l] = rose_count_tri[0][j][i+1][l-1] + (rose_count_vert[i][j+l] - rose_count_vert[i][j]);
			}
			for (int l = 1; l <= j; ++l) {
				rose_count_tri[1][j][i][l] = rose_count_tri[1][j][i+1][l-1] + (rose_count_vert[i][j] - rose_count_vert[i][j-l]);
			}
		}
	}

	memset(rose_count_below, -1, sizeof(rose_count_below));
	for (int r1 = 0; r1 <= H; ++r1) {
		for (int c1 = 0; c1 <= W; ++c1) {
			for (int c2 = c1 + 1; c2 <= W; ++c2) {
				rose_count_below[r1][c1][r1][c2] = rose_count_rect(0, c1, r1, c2 - c1);
				int r2 = r1 + c2 - c1;
				if (r2 <= H) {
					rose_count_below[r1][c1][r2][c2] = rose_count_below[r1][c1][r1][c2] + rose_count_tri[3][r1][c2][c2 - c1] - rose_count_on_line(r1, c1, r2, c2);
				}
				r2 = r1 - (c2 - c1);
				if (r2 >= 0) {
					rose_count_below[r1][c1][r2][c2] = rose_count_below[r2][c1][r2][c2] + rose_count_tri[0][r2][c1][c2 - c1] - rose_count_on_line(r1, c1, r2, c2);
				}
			}
		}
	}
	auto ans = solve();
	vector<string> ret;
	for (auto a : ans) {
		stringstream ss;
		ss << (int)a.s.c << " " << (int)a.s.r << " " << (int)a.e.c << " " << (int)a.e.r;
		ret.push_back(ss.str());
	}
	return ret;
}


#ifdef LOCAL
int main() {
	CakeSharing obj;
	int h, NP;
	cin >> h;
	vector<string> board(h);
	for (int i = 0; i < h; ++i) {
		cin >> board[i];
	}
	cin >> NP;
	vector<string> ret = obj.cut(board, NP);
	cout << ret.size() << endl;
	for (int i = 0; i < (int)ret.size(); ++i) {
		cout << ret[i] << endl;
	}
	PRINT_COUNTER();
	PRINT_TIMER();
}
#endif
