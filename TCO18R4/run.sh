#!/bin/sh -x

for i in 6 9 12 15 20; do
		g++48 -DFIN_MAX="$i" -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL -s -pipe -mmmx -msse -msse2 -msse3 CakeSharing.cpp -o tester
		java Tester -b 1000 -e 1999 > log10_FINMAX_"$i".txt
done
