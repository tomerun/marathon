import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;

class P {
	public int r, c;

	public P() {
		this.r = this.c = -1;
	}

	public P(int r, int c) {
		this.r = r;
		this.c = c;
	}

	public P(P p) {
		this.r = p.r;
		this.c = p.c;
	}
}

public class Tester {
	static final int maxSize = 60, minSize = 10;
	static final int maxWallsP = 30, minWallsP = 10;
	static final int maxBallsP = 20, minBallsP = 5;
	static final int[] dr = { 0, 1, 0, -1 };
	static final int[] dc = { -1, 0, 1, 0 };
	static char EMPTY = '.';
	static char WALL = '#';
	static int myW, myH, myWp, myBp, myColor;

	int maxRolls;
	int W, H, nBalls;
	int wp, bp, color;
	char[][] targetMaze;
	char[][] startMaze;
	char[][] curMaze;
	P[][] ballMoves; // [prev, next]
	SecureRandom r1;

	boolean isInside(P p) {
		return (p.r >= 0 && p.r < H && p.c >= 0 && p.c < W);
	}

	P roll(P p, int dir) {
		P newP = new P(p);
		do {
			newP.r += dr[dir];
			newP.c += dc[dir];
		} while (isInside(newP) && curMaze[newP.r][newP.c] == EMPTY);
		newP.r -= dr[dir];
		newP.c -= dc[dir];
		if (newP.r != p.r || newP.c != p.c) {
			curMaze[newP.r][newP.c] = curMaze[p.r][p.c];
			curMaze[p.r][p.c] = EMPTY;
		}
		return newP;
	}

	int chooseRoll(P ballP) {
		// find all allowed roll directions for this ball, and return a random one or -1 if none exist
		int[] allowedDir = new int[4];
		int nAllow = 0;
		for (int dir = 0; dir < 4; ++dir) {
			P adjP = new P(ballP);
			adjP.r += dr[dir];
			adjP.c += dc[dir];
			if (isInside(adjP) && curMaze[adjP.r][adjP.c] == EMPTY) {
				allowedDir[nAllow] = dir;
				++nAllow;
			}
		}
		if (nAllow == 0) return -1;
		return allowedDir[r1.nextInt(nAllow)];
	}

	void generate(long seed) {
		try {
			r1 = SecureRandom.getInstance("SHA1PRNG");
			r1.setSeed(seed);
			H = r1.nextInt(maxSize - minSize + 1) + minSize;
			W = r1.nextInt(maxSize - minSize + 1) + minSize;
			color = r1.nextInt(10) + 1;
			if (seed <= 3) {
				W = H = minSize * (int) seed;
				color = (int) seed;
			}
			wp = r1.nextInt(maxWallsP - minWallsP + 1) + minWallsP;
			bp = r1.nextInt(maxBallsP - minBallsP + 1) + minBallsP;
			if (1000 <= seed && seed < 2000) {
				color = (int) (seed - 900) / 100;
				wp = minWallsP + (maxWallsP - minWallsP + 1) * ((int) seed % 100) / 100;
			}
			if (myH != 0) H = myH;
			if (myW != 0) W = myW;
			if (myColor != 0) color = myColor;
			if (myWp != 0) wp = myWp;
			if (myBp != 0) bp = myBp;
			nBalls = 0;
			do {
				targetMaze = new char[H][W];
				for (int i = 0; i < H; i++)
					for (int j = 0; j < W; j++) {
						if (r1.nextInt(100) < wp) {
							targetMaze[i][j] = WALL;
						} else {
							if (r1.nextInt(100) < bp) {
								targetMaze[i][j] = (char) ('0' + r1.nextInt(color));
								++nBalls;
							} else {
								targetMaze[i][j] = EMPTY;
							}
						}
					}
			} while (nBalls == 0);

			curMaze = new char[H][W];
			for (int i = 0; i < H; ++i)
				curMaze[i] = Arrays.copyOf(targetMaze[i], W);
			P[] ball = new P[nBalls];
			int ballInd = 0;
			for (int i = 0; i < H; i++)
				for (int j = 0; j < W; j++)
					if (curMaze[i][j] != WALL && curMaze[i][j] != EMPTY) {
						ball[ballInd] = new P(i, j);
						++ballInd;
					}

			// do random rolls
			int nRolls = r1.nextInt(nBalls * 10) + nBalls * 3;
			for (int roll = 0; roll < nRolls; ++roll) {
				int i = r1.nextInt(nBalls);
				int choice = chooseRoll(ball[i]);
				if (choice > -1) {
					ball[i] = roll(ball[i], choice);
				}
			}
			// try to get balls out of their target positions (best effort)
			for (int i = 0; i < nBalls; ++i) {
				int t = targetMaze[ball[i].r][ball[i].c];
				if (t == curMaze[ball[i].r][ball[i].c] && t != EMPTY && t != WALL) {
					int choice = chooseRoll(ball[i]);
					if (choice > -1) {
						ball[i] = roll(ball[i], choice);
					}
				}
			}

			startMaze = new char[H][W];
			for (int i = 0; i < H; ++i)
				startMaze[i] = Arrays.copyOf(curMaze[i], W);
			maxRolls = nBalls * 20;
		} catch (Exception e) {
			System.err.println("An exception occurred while generating test case.");
			e.printStackTrace();
		}
	}

	static int getScore(char[][] target, char[][] cur) {
		int H = target.length;
		int W = target[0].length;
		int score = 0;
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (target[i][j] != WALL && target[i][j] != EMPTY) {
					if (cur[i][j] == target[i][j]) {
						score += 2;
					} else if (cur[i][j] != WALL && cur[i][j] != EMPTY) {
						score += 1;
					}
				}
			}
		}
		return score;
	}

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		try {
			generate(seed);
			res.H = H;
			res.W = W;
			res.wp = wp;
			res.bp = bp;
			res.color = color;
			res.nBall = nBalls;

			String[] startMazeStr, targetMazeStr;
			startMazeStr = new String[H];
			if (debug) System.out.println("Start maze:");
			for (int i = 0; i < H; ++i) {
				startMazeStr[i] = new String(startMaze[i]);
				if (debug) System.out.println(startMazeStr[i]);
			}
			targetMazeStr = new String[H];
			if (debug) System.out.println("Target maze:");
			for (int i = 0; i < H; ++i) {
				targetMazeStr[i] = new String(targetMaze[i]);
				if (debug) System.out.println(targetMazeStr[i]);
			}

			long beforeTime = System.currentTimeMillis();
			String[] rolls = restorePattern(startMazeStr, targetMazeStr);
			res.elapsed = System.currentTimeMillis() - beforeTime;

			if (rolls == null) {
				addFatalError("Your return contained invalid number of elements.");
				return res;
			}
			if (rolls.length > maxRolls) {
				addFatalError("Your return contained more than " + maxRolls + " elements.");
				return res;
			}

			// parse and simulate rolls starting with startMaze
			// manual mode just returns 0 rolls to process
			ballMoves = new P[rolls.length][2];
			for (int i = 0; i < rolls.length; ++i) {
				String[] s = rolls[i].split(" ");
				int R, C, D;
				if (s.length != 3) {
					addFatalError("Element " + i + " of your return must be formatted as \"R C D\"");
					return res;
				}
				try {
					R = Integer.parseInt(s[0]);
					C = Integer.parseInt(s[1]);
					D = Integer.parseInt(s[2]);
				} catch (Exception e) {
					addFatalError("R, C and D in element " + i + " of your return must be integers.");
					return res;
				}
				if (!isInside(new P(R, C))) {
					addFatalError("R and C in element " + i + " of your return must specify a cell within the maze.");
					return res;
				}
				if (curMaze[R][C] == EMPTY || curMaze[R][C] == WALL) {
					res.nRoll = i;
					addFatalError("Each roll of your return must start in a cell with a ball.");
					return res;
				}
				if (D < 0 || D > 3) {
					addFatalError("D must be an integer between 0 and 3, inclusive.");
					return res;
				}
				ballMoves[i][0] = new P(R, C);
				ballMoves[i][1] = roll(ballMoves[i][0], D);
				//				System.err.println("(" + R + "," + C + ") -> (" + ballMoves[i][1].r + "," + ballMoves[i][1].c + ")");
			}
			res.nRoll = rolls.length;
			res.score = getScore(targetMaze, curMaze) / (nBalls * 2.0);
			return res;
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results.");
			e.printStackTrace();
			return res;
		} finally {
			if (proc != null) proc.destroy();
		}
	}

	// ------------- visualization part ------------
	JFrame jf;
	Vis v;
	static String exec = "./tester";
	static boolean vis, debug;
	Process proc;
	InputStream is;
	OutputStream os;
	BufferedReader br;
	static int SZ = -1;
	final static Color[] colors = { new Color(0x4000FF), new Color(0xFF00BF), new Color(0x00BFFF), new Color(0xFFD761), new Color(0xFF4000),
			new Color(0x40FF00), new Color(0xB3002D), new Color(0x009973), new Color(0x999999), new Color(0x404040) };

	String[] restorePattern(String[] start, String[] target) throws IOException {
		if (proc != null) {
			StringBuffer sb = new StringBuffer();
			sb.append(H).append("\n");
			for (int i = 0; i < H; ++i) {
				sb.append(start[i]).append("\n");
			}
			sb.append(H).append("\n");
			for (int i = 0; i < H; ++i) {
				sb.append(target[i]).append("\n");
			}
			os.write(sb.toString().getBytes());
			os.flush();
		}

		String[] ret = new String[0];
		int N = Integer.parseInt(br.readLine());
		ret = new String[N];
		for (int i = 0; i < N; i++)
			ret[i] = br.readLine();
		return ret;
	}

	static BufferedImage deepCopy(BufferedImage source) {
		BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
		Graphics g = b.getGraphics();
		g.drawImage(source, 0, 0, null);
		g.dispose();
		return b;
	}

	public static class Vis extends JPanel {

		int W, H, B;
		char[][] target, start, cur;
		P[][] moves;
		int turn;
		BufferedImage cache;
		static Font font = new Font("Ricty", Font.BOLD, 13);

		void drawTargetMaze() {
			cache = new BufferedImage((W + 3) * SZ + 100, H * SZ + 40, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) cache.getGraphics();
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			// background
			g2.setColor(new Color(0xDDDDDD));
			g2.fillRect(0, 0, (W + 3) * SZ + 100, H * SZ + 40);
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, W * SZ, H * SZ);

			// target ball positions (square frames of required color)
			for (int i = 0; i < H; ++i)
				for (int j = 0; j < W; ++j)
					if (target[i][j] == WALL) {
						g2.setColor(Color.BLACK);
						g2.fillRect(j * SZ, i * SZ, SZ, SZ);
					} else if (target[i][j] != EMPTY) {
						g2.setColor(colors[(int) (target[i][j] - '0')]);
						g2.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
						g2.setColor(Color.WHITE);
						g2.fillRect(j * SZ + 3, i * SZ + 3, SZ - 5, SZ - 5);
					}

			// lines between maze cells
			g2.setColor(Color.BLACK);
			Stroke origStroke = g2.getStroke();
			Stroke boldStroke = new BasicStroke(2.0f);
			g2.setStroke(boldStroke);
			for (int i = 0; i <= H; i++) {
				if (i % 5 == 0) g2.setStroke(boldStroke);
				g2.drawLine(0, i * SZ, W * SZ, i * SZ);
				if (i % 5 == 0) {
					g2.setStroke(origStroke);
					g2.drawString("" + i, W * SZ + 3, i * SZ);
				}
			}
			for (int i = 0; i <= W; i++) {
				if (i % 5 == 0) g2.setStroke(boldStroke);
				g2.drawLine(i * SZ, 0, i * SZ, H * SZ);
				if (i % 5 == 0) {
					g2.setStroke(origStroke);
					g2.drawString("" + i, i * SZ - 10, H * SZ + 12);
				}
			}
		}

		class DrawerKeyListener extends KeyAdapter {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					if (e.isShiftDown()) {
						while (turn < moves.length) {
							next();
						}
					} else if (e.isAltDown()) {
						for (int i = 0; i < 10; ++i) {
							next();
						}
					} else {
						next();
					}
					repaint();
				} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					if (e.isShiftDown()) {
						while (turn > 0) {
							prev();
						}
					} else if (e.isAltDown()) {
						for (int i = 0; i < 10; ++i) {
							prev();
						}
					} else {
						prev();
					}
					repaint();
				}
			}
		}

		void next() {
			if (turn == moves.length) return;
			P prev = moves[turn][0];
			P next = moves[turn][1];
			if (prev == null || next == null) return;
			cur[next.r][next.c] = cur[prev.r][prev.c];
			cur[prev.r][prev.c] = EMPTY;
			++turn;
		}

		void prev() {
			if (turn == 0) return;
			--turn;
			P prev = moves[turn][0];
			P next = moves[turn][1];
			cur[prev.r][prev.c] = cur[next.r][next.c];
			cur[next.r][next.c] = EMPTY;
		}

		public void paint(Graphics g) {
			if (cache == null) {
				drawTargetMaze();
			}
			BufferedImage bi = deepCopy(cache);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			for (int i = 0; i < H; ++i)
				for (int j = 0; j < W; ++j)
					if (cur[i][j] != EMPTY && cur[i][j] != WALL) {
						g2.setColor(colors[(int) (cur[i][j] - '0')]);
						g2.fillOval(j * SZ + 3, i * SZ + 3, SZ - 6, SZ - 6);
					}

			if (moves != null) {
				// draw line for previous roll
				if (turn > 0) {
					P prev = moves[turn - 1][0];
					P next = moves[turn - 1][1];
					g2.setColor(Color.BLACK);
					Stroke origStroke = g2.getStroke();
					Stroke stroke = new BasicStroke(3.0f);
					g2.setStroke(stroke);
					g2.drawLine(prev.c * SZ + SZ / 2, prev.r * SZ + SZ / 2, next.c * SZ + SZ / 2, next.r * SZ + SZ / 2);
					int[] dx = { 1, -1 };
					int[] dy = { 1, -1 };
					if (prev.r < next.r) {
						dy[0] = -1;
					} else if (prev.r > next.r) {
						dy[1] = 1;
					} else if (prev.c < next.c) {
						dx[0] = -1;
					} else {
						dx[1] = 1;
					}
					for (int i = 0; i < 2; ++i) {
						final int ar = SZ / 3;
						g2.drawLine(next.c * SZ + SZ / 2, next.r * SZ + SZ / 2, next.c * SZ + SZ / 2 + ar * dx[i], next.r * SZ + SZ / 2 + ar * dy[i]);
					}
					g2.setStroke(origStroke);
				}

				// information
				g2.setColor(Color.BLACK);
				g2.setFont(font);
				FontMetrics fm = g2.getFontMetrics();
				final int drawX = W * SZ + 10;
				int drayY = 20 + fm.getHeight();
				g2.drawString(String.format("H:%2d W:%2d B:%d", H, W, B), drawX, drayY);
				drayY += fm.getHeight() * 3 / 2;
				int rollsLen = (moves.length + "").length();
				String fmtstr = String.format("Rolls : %%%dd/%%%dd", rollsLen, rollsLen);
				g2.drawString(String.format(fmtstr, turn, moves.length), drawX, drayY);
				drayY += fm.getHeight() * 3 / 2;
				final int score = getScore(target, cur);
				g2.drawString(String.format("Score : %d/%d", score, B * 2), drawX, drayY);
				drayY += fm.getHeight();
				g2.drawString(String.format("        (%.5f)", score / (2.0 * B)), drawX, drayY);
				drayY += fm.getHeight() * 3 / 2;
			}

			g.drawImage(bi, 0, 0, (W + 3) * SZ + 100, H * SZ + 40, null);
		}

		public Vis(char[][] target, char[][] start, P[][] moves) {
			this.target = target;
			this.start = start;
			this.H = target.length;
			this.W = target[0].length;
			this.moves = moves;
			this.cur = new char[H][W];
			for (int i = 0; i < H; ++i) {
				for (int j = 0; j < W; ++j) {
					this.cur[i][j] = this.start[i][j];
					if (this.cur[i][j] != EMPTY && this.cur[i][j] != WALL) ++B;
				}
			}
			if (SZ < 0) SZ = Math.min(840 / H, 1300 / W);
			addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					// for manual play
				}
			});
		}
	}

	public Tester() {
		try {
			if (exec != null) {
				try {
					Runtime rt = Runtime.getRuntime();
					proc = rt.exec(exec);
					os = proc.getOutputStream();
					is = proc.getInputStream();
					br = new BufferedReader(new InputStreamReader(is));
					new ErrorReader(proc.getErrorStream()).start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 1, begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-exec")) exec = args[++i];
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
			if (args[i].equals("-debug")) debug = true;
			if (args[i].equals("-H")) myH = Integer.parseInt(args[++i]);
			if (args[i].equals("-W")) myW = Integer.parseInt(args[++i]);
			if (args[i].equals("-C")) myColor = Integer.parseInt(args[++i]);
			if (args[i].equals("-wp")) myWp = Integer.parseInt(args[++i]);
			if (args[i].equals("-bp")) myBp = Integer.parseInt(args[++i]);
		}

		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
			if (vis) {
				JFrame jf = new JFrame();
				Vis v = new Vis(tester.targetMaze, tester.startMaze, tester.ballMoves);
				jf.addKeyListener(v.new DrawerKeyListener());
				jf.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				jf.getContentPane().add(v);
				jf.setSize((tester.W + 3) * SZ + 100, tester.H * SZ + 40);
				jf.setVisible(true);
			}
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int H, W, wp, bp, color, nBall, nRoll;
		double score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("H:%2d W:%2d wp:%2d bp:%2d color:%2d\n", H, W, wp, bp, color);
			ret += String.format("nBall:%4d nRoll:%4d\n", nBall, nRoll);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("score:%.4f", score);
			return ret;
		}
	}
}

class ErrorReader extends Thread {
	InputStream error;

	public ErrorReader(InputStream is) {
		error = is;
	}

	public void run() {
		try {
			byte[] ch = new byte[50000];
			int read;
			while ((read = error.read(ch)) > 0) {
				String s = new String(ch, 0, read);
				System.err.print(s);
				System.err.flush();
			}
		} catch (Exception e) {}
	}
}
