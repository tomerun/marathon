#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <unordered_set>
#include <iostream>
#include <sstream>
#include <functional>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <tuple>
#include <sys/time.h>

#ifndef LOCAL
#define NDEBUG
#endif
#include <cassert>

using namespace std;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t ll;
typedef uint64_t ull;

#ifndef TEST
#define MEASURE_TIME
#define DEBUG
#endif

namespace {
const int DR[] = {0, 1, 0, -1, 0, 1, 0, -1};
const int DC[] = {-1, 0, 1, 0, -1, 0, 1, 0};
const int DIR_LEFT   = 0;
const int DIR_BOTTOM = 1;
const int DIR_RIGHT  = 2;
const int DIR_TOP    = 3;

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 5000;
#else
#ifdef TEST
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 5000;
#else
const double CLOCK_PER_SEC = 3.6e9;
const ll TL = 9500;
#endif
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

XorShift rnd;
Timer timer;
Counter counter;
const u8 WALL = 0x80;

int H,W,C,B;
vector<vector<vector<u64>>> hashCell;
vector<vector<int>> target; // with sentinel

inline bool is_ball_cell(int v) { return (v & 0xF) != 0; };
inline bool is_empty_cell(int v) { return v == 0; };

using Move = uint16_t;
inline Move create_move(int r, int c, int d) {
	return (Move)((r << 8) + (c << 2) + d);
}
inline int get_move_r(Move m) {	return m >> 8; }
inline int get_move_c(Move m) {	return (m >> 2) & 0x3F; }
inline int get_move_d(Move m) {	return m & 0x3; }

struct Board {
	vector<vector<u8>> field;
	u64 hash;
	int score;

	Board(const vector<vector<int>>& start) : field(H + 2, vector<u8>(W + 2)), hash(0), score(0) {
		field[0].assign(W+2, WALL);
		for (int i = 1; i <= H; ++i) {
			field[i][0] = field[i][W+1] = WALL;
			for (int j = 1; j <= W; ++j) {
				field[i][j] = start[i][j];
				if (0 < field[i][j] && field[i][j] != WALL) {
					if (target[i][j] == field[i][j]) {
						score += 2;
					} else if (target[i][j] != 0) {
						score += 1;
					}
					hash ^= hashCell[field[i][j]-1][i-1][j-1];
				}
			}
		}
		field[H+1].assign(W+2, WALL);
	}

	Board(const Board& b) : field(b.field), hash(b.hash), score(b.score) {}
	Board(const Board&& b) : field(b.field), hash(b.hash), score(b.score) {}
	Board& operator=(const Board& b) {
		if (&b == this) return *this;
		field = b.field;
		hash = b.hash;
		score = b.score;
	}
	Board& operator=(const Board&& b) {
		if (&b == this) return *this;
		field = std::move(b.field);
		hash = std::move(b.hash);
		score = std::move(b.score);
	}

	int get_point_score(int r, int c) const {
		if (field[r][c] == 0 || target[r][c] == 0) return 0;
		return 1 + (target[r][c] == field[r][c]);
	}

	pair<int, int> get_terminal(int sr, int sc, int d) const {
		assert(field[sr][sc] != 0);
		int cr = sr + DR[d];
		int cc = sc + DC[d];
		assert(field[cr][cc] == 0);
		while (field[cr][cc] == 0) {
			cr += DR[d];
			cc += DC[d];
		}
		cr -= DR[d];
		cc -= DC[d];
		return make_pair(cr, cc);
	}

	int calc_score(int r, int c, int d) const {
		const int color = field[r][c];
		auto pos = get_terminal(r, c, d);
		int ret = score - get_point_score(r, c);
		const int tc = target[pos.first][pos.second];
		if (tc != 0 && tc != WALL) ++ret;
		if (tc == color) ++ret;
		return ret;
	}

	int calc_hash(Move m) const {
		const int r = get_move_r(m);
		const int c = get_move_c(m);
		const int d = get_move_d(m);
		const int color = field[r][c];
		const auto pos = get_terminal(r, c, d);
		return hash ^ hashCell[color-1][r-1][c-1] ^ hashCell[color-1][pos.first-1][pos.second-1];
	}

	void move(Move m) {
		const int sr = get_move_r(m);
		const int sc = get_move_c(m);
		const int d = get_move_d(m);
		const int color = field[sr][sc];
		const auto pos = get_terminal(sr, sc, d);
		hash ^= hashCell[color-1][sr-1][sc-1];
		hash ^= hashCell[color-1][pos.first-1][pos.second-1];
		swap(field[sr][sc], field[pos.first][pos.second]);
	}

	void print() const {
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (field[i+1][j+1] == 0) {
					cerr << '.';
				} else if (field[i+1][j+1] == WALL) {
					cerr << '#';
				} else {
					cerr << (char)(field[i+1][j+1]+'0');
				}
			}
			cerr << endl;
		}
		cerr << endl;
	}
};

namespace std {
	template<>
	void swap(Board& b1, Board& b2) throw() {
		swap(b1.field, b2.field);
		swap(b1.hash, b2.hash);
		swap(b1.score, b2.score);
	}
}


vector<Move> solve_beam(const vector<vector<int>> start_, int rest_turn) {
	Board initial_board(start_);
	vector<vector<Move>> moves(rest_turn);
	vector<vector<int>> prev_move(rest_turn);
	using Hand = tuple<int, int, int>; // (score, board_idx, Move)

	const int BEAM_WIDTH = (int)(400000 / sqrt(B) / (H+W));
	const int MAX_CHILD_FROM_SAME_PARENT = BEAM_WIDTH / 3;
	debug("BEAM_WIDTH:%d\n", BEAM_WIDTH);
	debug("initialScore:%d\n", initial_board.score);
	int bestScore = initial_board.score;
	int bestTurn = -1;
	int bestIdx = 0;
	vector<Board> cur_boards;
	cur_boards.push_back(move(initial_board));
	unordered_set<u64> used_hash;
	for (int turn = 0; turn < rest_turn; ++turn) {
		if (get_elapsed_msec() > TL) break;
		vector<Hand> hands;
		for (int i = 0; i < cur_boards.size(); ++i) {
			vector<Hand> cur_hands;
			const Board& cur_board = cur_boards[i];
			for (int r = 1; r <= H; ++r) {
				for (int c = 1; c <= W; ++c) {
					if (cur_board.field[r][c] == 0 || cur_board.field[r][c] == WALL) continue;
					for (int d = 0; d < 4; ++d) {
						int nr = r + DR[d];
						int nc = c + DC[d];
						if (cur_board.field[nr][nc] != 0) continue;
						cur_hands.emplace_back(cur_board.calc_score(r, c, d), i, create_move(r, c, d));
					}
				}
			}
			if (turn == 0 || cur_hands.size() <= MAX_CHILD_FROM_SAME_PARENT) {
				hands.insert(hands.end(), cur_hands.begin(), cur_hands.end());
			} else {
				nth_element(cur_hands.begin(), cur_hands.end() - MAX_CHILD_FROM_SAME_PARENT, cur_hands.end());
				hands.insert(hands.end(), cur_hands.end() - MAX_CHILD_FROM_SAME_PARENT, cur_hands.end());
			}
		}
		vector<Board> next_boards;
		sort(hands.begin(), hands.end());
		for (auto itr = hands.crbegin(); next_boards.size() < BEAM_WIDTH && itr != hands.crend(); ++itr) {
			const Hand& hand = *itr;
			const int bi = get<1>(hand);
			const Board& b = cur_boards[bi];
			auto hash = b.calc_hash(get<2>(hand));
			if (used_hash.find(hash) == used_hash.end()) {
				used_hash.insert(hash);
				next_boards.emplace_back(b);
				Board& nb = next_boards.back();
				nb.move(get<2>(hand));
				nb.score = get<0>(hand);
				moves[turn].push_back(get<2>(hand));
				prev_move[turn].push_back(get<1>(hand));
				if (nb.score > bestScore) {
					bestScore = nb.score;
					bestTurn = turn + 1;
					bestIdx = moves[turn].size() - 1;
					debug("bestScore:%d at turn:%d\n", nb.score, turn);
				}
			}
		}

		swap(cur_boards, next_boards);
	}

	debug("bestScore:%.4f %d %d\n", bestScore / 2.0 / B, bestTurn, bestIdx);

	int idx = bestIdx;
	vector<Move> ans;
	for (int turn = bestTurn; turn > 0; --turn) {
		ans.push_back(moves[turn-1][idx]);
		idx = prev_move[turn-1][idx];
	}
	reverse(ans.begin(), ans.end());
	return ans;
}

struct Trans {
	int pr,pc,nr,nc;
};

void print_trans(const vector<Trans>& ts) {
	for (auto& t : ts) {
		debug("(%d %d) -> (%d %d)\n", t.pr-1, t.pc-1, t.nr-1, t.nc-1);
	}
}

struct World {
	static const int ANY_COLOR = 0;
	int maxDepth = 10;
	vector<vector<int>> field;
	vector<vector<bool>> fix;
	vector<vector<bool>> temporary;

	struct TemporarySetter {
		vector<int> pos;
		World& w;
		TemporarySetter(int r, int c, World& w_) : w(w_) {
			w.temporary[r][c] = true;
			pos.emplace_back((r << 8) + c);
		}
		TemporarySetter(const vector<int>& pos_, World& w_) : pos(pos_), w(w_) {
			for (auto& p : pos) {
				w.temporary[p >> 8][p & 0xFF] = true;
			}
		}
		TemporarySetter(World& w_) : w(w_) { }

		~TemporarySetter() {
			for (auto& p : pos) {
				w.temporary[p >> 8][p & 0xFF] = false;
			}
		}

		void set(int r, int c) {
			w.temporary[r][c] = true;
			pos.emplace_back((r << 8) + c);
		}
	};

	struct FixSetter {
		int r, c;
		bool old;
		World& w;
		FixSetter(int r_, int c_, World& w_) : r(r_), c(c_), w(w_) { old = w.fix[r][c]; w.fix[r][c] = true; }
		~FixSetter() { w.fix[r][c] = old; }
	};

	World(const vector<string>& start) : field(H + 2, vector<int>(W + 2)) {
		field[0].assign(W+2, WALL);
		for (int i = 0; i < H; ++i) {
			field[i+1][0] = field[i+1][W+1] = WALL;
			for (int j = 0; j < W; ++j) {
				if (start[i][j] == '.') {
					field[i+1][j+1] = 0;
				} else if (start[i][j] == '#') {
					field[i+1][j+1] = WALL;
				} else {
					field[i+1][j+1] = (start[i][j] - '0') + 1;
				}
			}
		}
		field[H+1].assign(W+2, WALL);
		fix.assign(H+2, vector<bool>(W+2));
		temporary.assign(H+2, vector<bool>(W+2));
	}

	void apply(const vector<Trans>& trans) {
		for (auto& t : trans) {
			// print();
			// debug("(%d %d) -> (%d %d)\n", t.pr-1, t.pc-1, t.nr-1, t.nc-1);
			assert(is_ball_cell(field[t.pr][t.pc]));
			assert(is_empty_cell(field[t.nr][t.nc]));
			swap(field[t.pr][t.pc], field[t.nr][t.nc]);
		}
	}

	void undo(const vector<Trans>& trans) {
		for (auto itr = trans.crbegin(); itr != trans.crend(); ++itr) {
			auto& t = *itr;
#ifdef DEBUG
			if (!is_empty_cell(field[t.pr][t.pc])) {
				print();
				print_trans(trans);
			}
#endif
 			assert(is_ball_cell(field[t.nr][t.nc]));
			assert(is_empty_cell(field[t.pr][t.pc]));
			swap(field[t.pr][t.pc], field[t.nr][t.nc]);
		}
	}

	int terminal(int r, int c, int d) {
		int cr = r + DR[d];
		int cc = c + DC[d];
		while (is_empty_cell(field[cr][cc])) {
			cr += DR[d];
			cc += DC[d];
		}
		cr -= DR[d];
		cc -= DC[d];
		return (cr << 8) + cc;
	}

	void print() const {
		for (int i = 1; i <= H; ++i) {
			for (int j = 1; j <= W; ++j) {
				cerr << (field[i][j] == WALL ? '#' : (char)(field[i][j] + '0'));
			}
			cerr << endl;
		}
		cerr << endl;
	}

	int calc_score() const {
		int score = 0;
		for (int i = 1; i <= H; ++i) {
			for (int j = 1; j <= W; ++j) {
				if (is_ball_cell(target[i][j])) {
					if (is_ball_cell(field[i][j])) ++score;
					if (field[i][j] == target[i][j]) ++score;
				}
			}
		}
		return score;
	}

	template<int dir>
	vector<Trans> route_sub(int cr, int cc, int tr, int tc, int depth, int color) {
		assert(is_empty_cell(field[cr - DR[dir]][cc - DC[dir]]));
		// debug("route_sub:(%d %d) -> (%d %d) %d %d\n", cr, cc, tr, tc, depth, dir);
		// print();
		vector<Trans> ret;
		{
			FixSetter fix_setter(cr, cc, *this);
			ret = want_dir<dir>(cr - DR[dir], cc - DC[dir], depth, cr - DR[dir] == tr && cc - DC[dir] == tc ? color : ANY_COLOR);
		}
		if (ret.empty()) return ret;
		if (cr - DR[dir] == tr && cc - DC[dir] == tc) return ret;

		auto sub = route(cr - DR[dir], cc - DC[dir], tr, tc, depth + 1, color);
		if (sub.empty()) {
			undo(ret);
			return vector<Trans>();
		} else {
			ret.insert(ret.end(), sub.begin(), sub.end());
			return ret;
		}
	}

	vector<Trans> route(int cr, int cc, int tr, int tc, int depth, int color) {
		// debug("route:(%d %d) -> (%d %d) %d\n", cr, cc, tr, tc, depth);
		assert(!is_empty_cell(field[cr][cc]));
		assert(is_empty_cell(field[tr][tc]));
		if (depth > maxDepth) return vector<Trans>();
		if (cr > tr && is_empty_cell(field[cr-1][cc])) {
			const auto ret = route_sub<DIR_BOTTOM>(cr, cc, tr, tc, depth, color);
			if (!ret.empty()) return ret;
		}
		if (cr < tr && is_empty_cell(field[cr+1][cc])) {
			const auto ret = route_sub<DIR_TOP>(cr, cc, tr, tc, depth, color);
			if (!ret.empty()) return ret;
		}
		if (cc > tc && is_empty_cell(field[cr][cc-1])) {
			const auto ret = route_sub<DIR_RIGHT>(cr, cc, tr, tc, depth, color);
			if (!ret.empty()) return ret;
		}
		if (cc < tc && is_empty_cell(field[cr][cc+1])) {
			const auto ret = route_sub<DIR_LEFT>(cr, cc, tr, tc, depth, color);
			if (!ret.empty()) return ret;
		}
		return vector<Trans>();
	}

	vector<Trans> want_here(int r, int c, int depth, int color) {
		// debug("want_here:(%d %d) %d\n", r, c, depth);
		assert(is_empty_cell(field[r][c]));
		assert(!temporary[r][c]);
		if (depth > maxDepth) return vector<Trans>();

		vector<Trans> ret;
		vector<vector<bool>> visited(H + 2, vector<bool>(W + 2));
		vector<int> q = {(r << 8) + c};
		visited[r][c] = true;
		for (int i = 0; i < q.size(); ++i) {
			int cr = q[i] >> 8;
			int cc = q[i] & 0xFF;
			if (abs(cr - r) + abs(cc - c) >= maxDepth - depth) break;
			if (!is_empty_cell(field[cr][cc])) {
				ret = route(cr, cc, r, c, depth, color);
				if (!ret.empty()) break;
			} else {
				if (r <= cr && !visited[cr + 1][cc]) {
					q.push_back(((cr + 1) << 8) + cc);
					visited[cr + 1][cc] = true;
				}
				if (r >= cr && !visited[cr - 1][cc]) {
					q.push_back(((cr - 1) << 8) + cc);
					visited[cr - 1][cc] = true;
				}
				if (c <= cc && !visited[cr][cc + 1]) {
					q.push_back((cr << 8) + cc + 1);
					visited[cr][cc + 1] = true;
				}
				if (cc <= c && !visited[cr][cc - 1]) {
					q.push_back((cr << 8) + cc - 1);
					visited[cr][cc - 1] = true;
				}
			}
		}
		assert(ret.empty() || is_ball_cell(field[r][c]));
		return ret;
	}

	template<int dir>
	vector<Trans> want_dir(int r, int c, int depth, int color) {
		assert(!temporary[r][c]);
		TemporarySetter temp_setter(r, c, *this);
		const auto ret = want_dir_sub<dir>(r, c, r, c, depth, color);
		return ret;
	}

	template<int dir>
	vector<Trans> want_dir_sub(const int r, const int c, int sr, int sc, int depth, int color) {
		// debug("want_dir_sub:(%d %d) (%d %d) %d %d\n", r, c, sr, sc, dir, depth);
		assert(is_empty_cell(field[r][c]));
		assert(!is_empty_cell(field[r + DR[dir]][c + DC[dir]]));
		assert(temporary[r][c]);
		assert(fix[r + DR[dir]][c + DC[dir]]);
		if (depth > maxDepth) return vector<Trans>();
		vector<Trans> ret;
		vector<int> search_pos;
		int cr = sr - DR[dir];
		int cc = sc - DC[dir];
		while (is_empty_cell(field[cr][cc])) {
			if (!temporary[cr][cc]) {
				search_pos.push_back((cr << 8) + cc);
			}
			cr -= DR[dir];
			cc -= DC[dir];
		}
		if (is_ball_cell(field[cr][cc]) && !fix[cr][cc] && (color == ANY_COLOR || field[cr][cc] == color)) {
			ret.push_back({cr, cc, r, c});
			swap(field[cr][cc], field[r][c]);
			assert(is_ball_cell(field[r][c]));
			return ret;
		}

		TemporarySetter temp_setter(*this);
		for (int pos : search_pos) {
			int pr = pos >> 8;
			int pc = pos & 0xFF;
			if (!is_empty_cell(field[pr + DR[dir + 1]][pc + DC[dir + 1]])) {
				FixSetter fix_setter(pr + DR[dir + 1], pc + DC[dir + 1], *this);
				int next_depth = depth + 1;
				if (!ret.empty()) next_depth = max(next_depth, (int)(maxDepth - ret.size() + 2));
				const auto ret_sub = want_dir<(dir + 1) & 3>(pr, pc, next_depth, color);
				if (!ret_sub.empty()) {
					if (ret.empty() || ret_sub.size() + 1 < ret.size()) {
						ret = ret_sub;
						ret.push_back({pr, pc, r, c});
					}
					undo(ret_sub);
				}
			}
			if (!is_empty_cell(field[pr + DR[dir + 3]][pc + DC[dir + 3]])) {
				FixSetter fix_setter(pr + DR[dir + 3], pc + DC[dir + 3], *this);
				int next_depth = depth + 1;
				if (!ret.empty()) next_depth = max(next_depth, (int)(maxDepth - ret.size() + 2));
				const auto ret_sub = want_dir<(dir + 3) & 3>(pr, pc, next_depth, color);
				if (!ret_sub.empty()) {
					if (ret.empty() || ret_sub.size() + 1 < ret.size()) {
						ret = ret_sub;
						ret.push_back({pr, pc, r, c});
					}
					undo(ret_sub);
				}
			}
			temp_setter.set(pr, pc);
		}
		if (!ret.empty()) {
			apply(ret);
			assert(is_ball_cell(field[r][c]));
			return ret;
		}

		if (ret.empty() && is_ball_cell(field[cr][cc]) && depth < maxDepth - 1) {
			if (!fix[cr][cc]) {
				for (int d = 1; d <= 3; d += 2) {
					int pos = terminal(cr, cc, (dir + d) & 3);
					if (pos == (cr << 8) + cc) continue;
					if (temporary[pos >> 8][pos & 0xFF]) continue;
					ret.push_back({cr, cc, pos >> 8, pos & 0xFF});
					swap(field[cr][cc], field[pos >> 8][pos & 0xFF]);
					TemporarySetter temp_setter_sub(cr, cc, *this);
					auto sub = want_dir_sub<dir>(r, c, cr, cc, depth + 2, color);
					if (sub.empty()) {
						undo(ret);
						ret.clear();
					} else {
						assert(is_ball_cell(field[r][c]));
						ret.insert(ret.end(), sub.begin(), sub.end());
						break;
					}
				}
			} else {
				for (int d = 1; d <= 3; d += 2) {
					if (is_empty_cell(field[cr - DR[dir + d]][cc - DC[dir + d]])) continue;
					int pos = terminal(cr, cc, (dir + d) & 3);
					if (pos == (cr << 8) + cc) continue;
					const int nr = pos >> 8;
					const int nc = pos & 0xFF;
					FixSetter fix_setter1(cr - DR[dir + d], cc - DC[dir + d], *this);
					FixSetter fix_setter2(nr, nc, *this);
					TemporarySetter temp_setter_sub(*this);
					int tr = cr;
					int tc = cc;
					while (tr != nr || tc != nc) {
						if (!temporary[tr][tc]) temp_setter_sub.set(tr, tc);
						tr += DR[dir + d];
						tc += DC[dir + d];
					}
					ret.push_back({cr, cc, nr, nc});
					swap(field[cr][cc], field[nr][nc]);
					auto sub = want_dir_sub<dir>(r, c, cr, cc, depth + 3, color);
					if (sub.empty()) {
						undo(ret);
						ret.clear();
					} else {
						assert(is_ball_cell(field[r][c]));
						assert(is_ball_cell(field[nr][nc]));
						assert(terminal(nr, nc, (dir + d + 2) & 3) == (cr << 8) + cc);
						ret.insert(ret.end(), sub.begin(), sub.end());
						swap(field[cr][cc], field[nr][nc]);
						ret.push_back({nr, nc, cr, cc});
						break;
					}
				}
			}
		}

		assert(ret.empty() || is_ball_cell(field[r][c]));
		return ret;
	}

	vector<Trans> move_near(int br, int bc, const vector<vector<int>>& dist, const int max_len) {
		assert(is_ball_cell(field[br][bc]));
		vector<Trans> ret;
		vector<Trans> cur;
		int min_dist = dist[br][bc];

		function<void(int,int,int)> dfs = [&](int r, int c, int prev_dir){
			assert(is_ball_cell(field[r][c]));
			if (dist[r][c] < min_dist) {
				min_dist = dist[r][c];
				ret = cur;
			}
			if (cur.size() >= max_len) return;
			for (int dir = 0; dir < 4; ++dir) {
				if (dir == prev_dir) continue;
				int cr = r + DR[dir];
				int cc = c + DC[dir];
				while (is_empty_cell(field[cr][cc])) {
					cr += DR[dir];
					cc += DC[dir];
				}
				cr -= DR[dir];
				cc -= DC[dir];
				if (cr == r && cc == c) continue;
				cur.push_back({r, c, cr, cc});
				assert(is_ball_cell(field[r][c]));
				assert(!is_ball_cell(field[cr][cc]));
				swap(field[r][c], field[cr][cc]);
				dfs(cr, cc, dir);
				assert(!is_ball_cell(field[r][c]));
				assert(is_ball_cell(field[cr][cc]));
				swap(field[r][c], field[cr][cc]);
				cur.pop_back();

				cr += DR[dir];
				cc += DC[dir];
				if (!is_ball_cell(field[cr][cc])) continue;

				int nr = cr + DR[dir];
				int nc = cc + DC[dir];
				while (is_empty_cell(field[nr][nc])) {
					nr += DR[dir];
					nc += DC[dir];
				}
				nr -= DR[dir];
				nc -= DC[dir];
				if (nr == cr && nc == cc) continue;
				if (dist[nr][nc] >= min_dist) continue;

				for (int nd = 1; nd <= 3; nd += 2) {
					if (!is_empty_cell(field[cr - DR[dir + nd]][cc - DC[dir + nd]])
						  && is_empty_cell(field[cr + DR[dir + nd]][cc + DC[dir + nd]])) {
						int mp = terminal(cr, cc, (dir + nd) & 3);
						int mpr = mp >> 8;
						int mpc = mp & 0xFF;
						cur.push_back({cr, cc, mpr, mpc});
						cur.push_back({r, c, nr, nc});
						cur.push_back({mpr, mpc, cr, cc});
						assert(is_ball_cell(field[r][c]));
						assert(is_empty_cell(field[nr][nc]));
						assert(is_empty_cell(field[mpr][mpc]));
						swap(field[r][c], field[nr][nc]);
						dfs(nr, nc, dir);
						assert(is_empty_cell(field[r][c]));
						assert(is_ball_cell(field[nr][nc]));
						assert(is_empty_cell(field[mpr][mpc]));
						swap(field[r][c], field[nr][nc]);
						cur.pop_back();
						cur.pop_back();
						cur.pop_back();
					}
				}
			}
		};

		dfs(br, bc, -1);
		apply(ret);
		// debug("move_near:(%d %d) %d->%d %ld\n", br, bc, dist[br][bc], min_dist, ret.size());

		return ret;
	}

	vector<Trans> gather(int r, int c, int rest_rolls) {
		const int INF_DIST = 0xFFFF;
		vector<vector<int>> dist(H + 2, vector<int>(W + 2, INF_DIST));
		dist[r][c] = 0;
		vector<int> q = {(r << 8) + c};
		for (int i = 0; i < q.size(); ++i) {
			int cr = q[i] >> 8;
			int cc = q[i] & 0xFF;
			for (int d = 0; d < 4; ++d) {
				int nr = cr + DR[d];
				int nc = cc + DC[d];
				if (is_empty_cell(field[nr][nc])) {
					if (dist[cr][cc] + 1 < dist[nr][nc]) {
						dist[nr][nc] = dist[cr][cc] + 1;
						q.push_back((nr << 8) + nc);
					}
				} else {
					int tr = cr - DR[d];
					int tc = cc - DC[d];
					int next_dist = dist[cr][cc] + 1;
					while (field[tr][tc] != WALL) {
						if (is_ball_cell(field[tr][tc])) {
							next_dist += fix[tr][tc] ? 2 : 1;
						}
						if (next_dist < dist[tr][tc]) {
							dist[tr][tc] = next_dist;
							q.push_back((tr << 8) + tc);
						}
						tr -= DR[d];
						tc -= DC[d];
					}
				}
			}
		}

		vector<pair<int, int>> free_balls;
		for (int i = 1; i <= H; ++i) {
			for (int j = 1; j <= W; ++j) {
				if (!fix[i][j] && is_ball_cell(field[i][j]) && dist[i][j] != INF_DIST) {
					free_balls.push_back(make_pair(dist[i][j], (i << 8) + j));
				}
			}
		}
		if (free_balls.empty()) return vector<Trans>();
		const int max_len = min(9, (int)((rest_rolls - 10) / free_balls.size()));
		if (max_len <= 0) return vector<Trans>();
		sort(free_balls.begin(), free_balls.end());
		// debug("gather to (%d %d) %d %d %lu\n", r, c, max_len, rest_rolls, free_balls.size());
		vector<Trans> ret;
		int count_move_balls = 0;
		for (auto& fb : free_balls) {
			if (fb.first <= 3) continue;
			auto moves = move_near(fb.second >> 8, fb.second & 0xFF, dist, max_len);
			if (!moves.empty()) {
				ret.insert(ret.end(), moves.begin(), moves.end());
				++count_move_balls;
				if (count_move_balls == 5) break;
			}
		}

		return ret;
	}

};

vector<vector<int>> group;
vector<int> group_size;
const int INVALID_GROUP = -1;

int count_reachable_cell(int pos) {
	vector<int> q = {pos};
	vector<vector<bool>> visited(H + 2, vector<bool>(W + 2));
	for (int i = 0; i < q.size(); ++i) {
		const int cr = q[i] >> 8;
		const int cc = q[i] & 0xFF;
		for (int d = 0; d < 4; ++d) {
			const int nr = cr + DR[d];
			const int nc = cc + DC[d];
			if (is_empty_cell(target[nr][nc])) continue;
			int pr = cr - DR[d];
			int pc = cc - DC[d];
			while (is_empty_cell(target[pr][pc])) {
				if (!visited[pr][pc]) {
					visited[pr][pc] = true;
					q.push_back((pr << 8) + pc);
				}
				pr -= DR[d];
				pc -= DC[d];
			}
		}
	}
	// debug("reachable size:(%d, %d) -> %ld\n", pos >> 8, pos & 0xFF, q.size());
	return q.size();
}

struct TargetGroup {
	vector<int> target_cells;

	void sort_cells() {
		vector<pair<int, int>> tcs;
		for (int pos : target_cells) {
			tcs.push_back(make_pair(count_reachable_cell(pos), pos));
		}
		sort(tcs.begin(), tcs.end());
		for (int i = 0; i < target_cells.size(); ++i) {
			target_cells[i] = tcs[i].second;
		}
	}

	void shuffle() {
		const int WIDTH = 4;
		for (int i = 0; i < target_cells.size(); ++i) {
			int j = i + rnd.nextUInt(WIDTH * 2 + 1) - WIDTH;
			if (j < 0) j = 0;
			if (j >= target_cells.size()) j = target_cells.size() - 1;
			swap(target_cells[i], target_cells[j]);
		}
	}
};

void fill_group() {
	group.assign(H + 2, vector<int>(W + 2, INVALID_GROUP));
	auto fill_group_sub = [](int r, int c) {
		const int gi = group_size.size();
		vector<int> q;
		q.push_back((r << 8) + c);
		group[r][c] = gi;
		for (int i = 0; i < q.size(); ++i) {
			int cr = q[i] >> 8;
			int cc = q[i] & 0xFF;
			for (int j = 0; j < 4; ++j) {
				int nr = cr + DR[j];
				int nc = cc + DC[j];
				if (group[nr][nc] == INVALID_GROUP && is_empty_cell(target[nr][nc])) {
					q.push_back((nr << 8) + nc);
					group[nr][nc] = gi;
				}
			}
		}
		group_size.push_back(q.size());
	};
	for (int r = 1; r <= H; ++r) {
		for (int c = 1; c <= W; ++c) {
			if (is_empty_cell(target[r][c]) && group[r][c] == INVALID_GROUP) {
				fill_group_sub(r, c);
			}
		}
	}
	for (int r = 1; r <= H; ++r) {
		for (int c = 1; c <= W; ++c) {
			debug("%2X", group[r][c] + 1);
		}
		debugln();
	}
}

vector<TargetGroup> determine_target_order() {
	fill_group();
	vector<vector<int>> visited(H + 2, vector<int>(W + 2));
	vector<bool> used_group(group_size.size());
	auto get_frontier = [&](int pos){
		vector<int> ret;
		vector<int> q = {pos};
		visited[pos >> 8][pos & 0xFF] = true;
		assert(target[pos >> 8][pos & 0xFF] != WALL);
		if (is_ball_cell(target[pos >> 8][pos & 0xFF])) return q;
		for (int i = 0; i < q.size(); ++i) {
			const int cr = q[i] >> 8;
			const int cc = q[i] & 0xFF;
			used_group[group[cr][cc]] = true;
			for (int j = 0; j < 4; ++j) {
				int nr = cr + DR[j];
				int nc = cc + DC[j];
				if (visited[nr][nc]) continue;
				if (is_ball_cell(target[nr][nc])) {
					ret.push_back((nr << 8) + nc);
				} else if (is_empty_cell(target[nr][nc])) {
					q.push_back((nr << 8) + nc);
				}
				visited[nr][nc] = true;
			}
		}
		return ret;
	};
	vector<TargetGroup> ret;
	while (true) {
		int largest_group = -1;
		int largest_size = 0;
		for (int i = 0; i < group_size.size(); ++i) {
			if (!used_group[i] && group_size[i] > largest_size) {
				largest_size = group_size[i];
				largest_group = i;
			}
		}
		if (largest_group == -1) break;
		int pos = -1;
		for (int i = 1; i <= H && pos == -1; ++i) {
			for (int j = 1; j <= W; ++j) {
				if (group[i][j] == largest_group) {
					pos = (i << 8) + j;
					break;
				}
			}
		}
		vector<int> tc;
		{
			TargetGroup tg;
			tg.target_cells = get_frontier(pos);
			if (tg.target_cells.empty()) continue;
			ret.push_back(tg);
			tc = tg.target_cells;
		}
		for (int i = 0; i < tc.size(); ++i) {
			for (int j = 0; j < 4; ++j) {
				int cr = (tc[i] >> 8) + DR[j];
				int cc = (tc[i] & 0xFF) + DC[j];
				if (visited[cr][cc] || target[cr][cc] == WALL) continue;
				TargetGroup tg;
				tg.target_cells = get_frontier((cr << 8) + cc);
				if (tg.target_cells.empty()) continue;
				ret.push_back(tg);
				tc.insert(tc.end(), tg.target_cells.begin(), tg.target_cells.end());
			}
		}
	}
	TargetGroup last_tg;
	for (int r = 1; r <= H; ++r) {
		for (int c = 1; c <= W; ++c) {
			if (!visited[r][c] && is_ball_cell(target[r][c])){
				last_tg.target_cells.push_back((r << 8) + c);
			}
		}
	}
	if (!last_tg.target_cells.empty()) {
		ret.push_back(last_tg);
	}
	reverse(ret.begin(), ret.end());
	for (auto& target_group: ret) {
		target_group.sort_cells();
		// debugStr("target_cell:");
		// for (auto pos : target_group.target_cells) {
		// 	debug("(%d, %d) ", pos >> 8, pos & 0xFF);
		// }
		// debugln();
	}

	return ret;
}

vector<Trans> solve_dfs(World& world, vector<TargetGroup>& target_groups, int best_score) {
	vector<Trans> ret;
	vector<int> target_cell;
	for (auto& tg : target_groups) {
		target_cell.insert(target_cell.end(), tg.target_cells.begin(), tg.target_cells.end());
	}
	int phase = 0;
	while (phase < 2) {
		int last_update = 0;
		bool retry = false;
		for (int turn = 0; turn - last_update <= target_cell.size(); ++turn) {
			const int r = target_cell[turn % target_cell.size()] >> 8;
			const int c = target_cell[turn % target_cell.size()] & 0xFF;
			if (world.field[r][c] == target[r][c]) {
				world.fix[r][c] = true;
				continue;
			}
			vector<Trans> cur_trans;
			bool gathered = false;
			if (turn >= target_cell.size() || retry) {
				gathered = true;
				retry = false;
				cur_trans = world.gather(r, c, B * 20 - ret.size());
				if (ret.size() + cur_trans.size() > B * 20) {
					debugStr("Roll Limit Exceeded in gather().\n");
					world.undo(cur_trans);
					cur_trans.clear();
				}
			}
			if (phase == 0 && is_ball_cell(world.field[r][c]) && world.field[r][c] != target[r][c]) {
				// move arbitrary direction
				int dir = rnd.nextUInt() & 3;
				for (int i = 0; i < 4; ++i, ++dir) {
					if (is_empty_cell(world.field[r + DR[dir]][c + DC[dir]])) {
						int cr = r + DR[dir];
						int cc = c + DC[dir];
						while (is_empty_cell(world.field[cr][cc])) {
							cr += DR[dir];
							cc += DC[dir];
						}
						cr -= DR[dir];
						cc -= DC[dir];
						swap(world.field[r][c], world.field[cr][cc]);
						cur_trans.push_back({r, c, cr, cc});
						break;
					}
				}
			}
			if (is_empty_cell(world.field[r][c])) {
				const auto ts = world.want_here(r, c, 0, phase == 0 ? target[r][c] : World::ANY_COLOR);
				if (ts.empty()) {
					// debug("%d %d:empty\n", r, c);
					world.undo(cur_trans);
					if (!gathered && turn < target_cell.size()) {
						retry = true;
						turn--;
					}
				} else {
					if (ts.size() + cur_trans.size() + ret.size() > B * 20) {
						debugStr("Roll Limit Exceeded.\n");
						world.undo(ts);
						world.undo(cur_trans);
						goto END;
					}
					last_update = turn;
					world.fix[r][c] = true;
					ret.insert(ret.end(), cur_trans.begin(), cur_trans.end());
					ret.insert(ret.end(), ts.begin(), ts.end());
					// debug("%d %d %ld %ld:\n", r, c, ts.size(), ret.size());
					// print_trans(ts);
				}
				if (get_elapsed_msec() > TL - 1000) {
					debugStr("TLE\n");
					goto END;
				}
			} else if (world.field[r][c] == target[r][c]) {
				ret.insert(ret.end(), cur_trans.begin(), cur_trans.end());
				world.fix[r][c] = true;
			} else {
				world.undo(cur_trans);
			}
		}
		++phase;
		if (phase == 1) {
			if (C == 1) break;
			int fix_count = 0;
			for (int i = 1; i <= H; ++i) {
				for (int j = 1; j <= W; ++j) {
					if (world.fix[i][j]) ++fix_count;
				}
			}
			debug("fix_count:%d optimal_score:%d best_score:%d\n", fix_count, fix_count * 2 + (B - fix_count), best_score);
			if (fix_count * 2 + (B - fix_count) <= best_score) break;
		}
	}
END:
	return ret;
}

struct RollingBalls {
	vector<string> restorePattern(const vector<string>& start, const vector<string>& target);
};

vector<string> RollingBalls::restorePattern(const vector<string>& start_, const vector<string>& target_) {
	start_time = get_time();
	H = start_.size();
	W = start_[0].size();
	B = C = 0;
	target.emplace_back(W + 2, WALL);
	for (int i = 0; i < H; ++i) {
		target.emplace_back(W + 2);
		target[i+1][0] = target[i+1][W+1] = WALL;
		for (int j = 0; j < W; ++j) {
			if (target_[i][j] == '.') {
				target[i+1][j+1] = 0;
			} else if (target_[i][j] == '#') {
				target[i+1][j+1] = WALL;
			} else {
				target[i+1][j+1] = (target_[i][j] - '0') + 1;
				C = max(C, target[i+1][j+1]);
				++B;
			}
		}
	}
	target.emplace_back(W + 2, WALL);
	hashCell.resize(C + 1);
	for (int i = 0; i <= C; ++i) {
		hashCell.emplace_back(H);
		for (int j = 0; j < H; ++j) {
			hashCell[i].emplace_back(W);
			for (int k = 0; k < W; ++k) {
				hashCell[i][j][k] = ((ull)rnd.nextUInt() << 32) | rnd.nextUInt();
			}
		}
	}

	vector<string> ans;

	auto add_dfs_result_to_ans = [&](const vector<Trans>& result) {
		for (auto& t : result) {
			int dir;
			if (t.pr < t.nr) {
				dir = 1;
			} else if (t.pr > t.nr) {
				dir = 3;
			} else if (t.pc < t.nc) {
				dir = 2;
			} else {
				dir = 0;
			}
			stringstream ss;
			ss << (t.pr - 1) << " " << (t.pc - 1) << " " << dir;
			ans.push_back(ss.str());
		}
	};

	// DFS
	const int BEAM_TIME = 2000;
	int best_score = 0;
	World best_world(start_);
	int max_depth = 11;
	auto target_groups = determine_target_order();
	while (true) {
		World world(start_);
		world.maxDepth = max_depth;
		const auto before_time = get_time();
		auto ret = solve_dfs(world, target_groups, best_score);
		int cur_score = world.calc_score();
		debug("maxDepth:%d score:%d elapsed:%lld\n", world.maxDepth, cur_score, get_elapsed_msec());
		if (cur_score > best_score) {
			best_score = cur_score;
			best_world = world;
			ans.clear();
			add_dfs_result_to_ans(ret);
			if (best_score == B * 2) break;
		}
		const auto elapsed_dfs = get_time() - before_time;
		const auto rest_time = TL - BEAM_TIME - get_elapsed_msec();
		debug("elapsed:%lld rest:%lld\n", elapsed_dfs, rest_time);
		if (rest_time <= 0) break;
		if (elapsed_dfs * 3 < rest_time && max_depth < 20) {
			++max_depth;
		} else if (max_depth > 8 && elapsed_dfs > rest_time) {
			--max_depth;
		}
		if (rest_time < elapsed_dfs / 2) break;
		for (auto& tg : target_groups) {
			tg.shuffle();
		}
	}

	// beam search
	if (best_score < B * 2) {
		auto ret_beam = solve_beam(best_world.field, B * 20 - ans.size());
		for (auto& beam_move : ret_beam) {
			stringstream ss;
			ss << (get_move_r(beam_move) - 1) << " " << (get_move_c(beam_move) - 1) << " " << get_move_d(beam_move);
			ans.push_back(ss.str());
		}
	}

	return ans;
}


#if defined(LOCAL) || defined(TEST)
int main() {
	int h;
	cin >> h;
	vector<string> start(h);
	for (int i = 0; i < h; ++i) {
		cin >> start[i];
	}
	cin >> h;
	vector<string> target(h);
	for (int i = 0; i < h; ++i) {
		cin >> target[i];
	}
	RollingBalls obj;
	const auto ret = obj.restorePattern(start, target);
	cout << ret.size() << endl;
	for (int i = 0; i < ret.size(); ++i) {
		cout << ret[i] << endl;
	}
	cout.flush();
}
#endif