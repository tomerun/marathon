#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:H, :W, :wp, :bp, :color, :nBall, :nRoll, :elapsed, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed:(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /H: *(\d+) W: *(\d+) wp: *(\d+) bp: *(\d+) color: *(\d+)/
      testcase.H = $1.to_i
      testcase.W = $2.to_i
      testcase.wp = $3.to_i
      testcase.bp = $4.to_i
      testcase.color = $5.to_i
    elsif line =~ /nBall: *(\d+) nRoll: *(\d+)/
      testcase.nBall = $1.to_i
      testcase.nRoll = $2.to_i
    elsif line =~ /elapsed:(.+)/
      testcase.elapsed = $1.to_f
    elsif line =~ /score:(.+)/
      testcase.score = $1.to_f
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, filter)
  sum_score_diff = 0
  sum_score = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    s1 = pair[0].score
    s2 = pair[1].score
    sum_score += s2
    sum_score_diff += s2 - s1
    if s1 < s2
      win += 1
    elsif s1 > s2
      lose += 1
    else
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, list.size - win - lose)
  puts sprintf("  diff:%.4f", sum_score_diff / list.size)
  puts sprintf("  ave :%.4f", sum_score / list.size)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  1.upto(10) do |c|
    puts "color:#{c}"
    calc(from, to, proc { |t| t[0].color == c })
  end
  puts "---------------------"
  step_wp = 3;
  10.step(30, step_wp) do |wp|
    puts "wp:[#{wp}-#{wp + step_wp - 1}]"
    calc(from, to, proc { |t| wp <= t[0].wp && t[0].wp < wp + step_wp })
  end
  puts "---------------------"
  step_bp = 4;
  5.step(20, step_bp) do |bp|
    puts "bp:[#{bp}-#{bp + step_bp - 1}]"
    calc(from, to, proc { |t| bp <= t[0].bp && t[0].bp < bp + step_bp })
  end
  puts "---------------------"
  puts "total"
  calc(from, to, proc { |t| true })
end

main
