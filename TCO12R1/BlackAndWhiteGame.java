import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public final class BlackAndWhiteGame {

	private static final long TIMELIMIT = 3000;
	private static final boolean DEBUG = 0 == 1;
	private static final boolean MEASURE_TIME = 0 == 1;
	private static final int[] DR = { 0, 1, 0, -1 };
	private static final int[] DC = { 1, 0, -1, 0 };
	private static final int[] ordPenaSrc = { 10, 3, 2, 1, 0 };
	private static final int[] ordPena = new int[16];
	private static final int factorDist = 6;
	private static final int currentPena = 50;

	final long startTime = System.currentTimeMillis();
	XorShift rand = new XorShift();
	Timer timer = new Timer();

	Board brd;
	int N;
	int wc;
	boolean stripe;
	Rectangle origRect;
	int[][] distPena;
	int[] rsBuf, csBuf;
	ArrayList<Pair<Integer, Integer>> ans = null;
	boolean[][] visited;
	int leftNext;
	int moveCount;
	byte[][][][] surPool;
	boolean[][][][] boardPool;
	int insertL, insertR, insertT, insertB;
	int[] insertPos;
	int turn;

	String[] makeConnected(String[] input) {
		timer.start(0);
		N = input.length;
		init(input);
		for (turn = 0; System.currentTimeMillis() - startTime < TIMELIMIT; ++turn) {
			logln("try:" + turn);
			if (turn != 0) {
				this.brd = new Board(input);
			}
			this.brd.calcInitialState();
			ArrayList<Pair<Integer, Integer>> moves = solve();
			if (moves != null && (ans == null || moves.size() < ans.size())) {
				ans = moves;
			}
		}
		String[] ret;
		if (ans != null) {
			ret = new String[ans.size()];
			for (int i = 0; i < ans.size(); ++i) {
				ret[i] = ans.get(i).f + " " + ans.get(i).s;
			}
		} else {
			ret = new String[0];
		}
		if (MEASURE_TIME) {
			logln("time:" + Arrays.toString(timer.sum));
		}
		return ret;
	}

	void init(String[] board) {
		leftNext = 20;
		if (N <= 85) leftNext = 35;
		if (N <= 75) leftNext = 50;
		if (N <= 60) leftNext = 80;
		if (N <= 40) leftNext = 120;
		if (N <= 25) leftNext = 200;
		boardPool = new boolean[2][leftNext * 3 + 10][N + 2][N + 2];
		surPool = new byte[2][leftNext * 3 + 10][N + 2][N + 2];
		for (int i = 0; i < 16; ++i) {
			ordPena[i] = ordPenaSrc[Integer.bitCount(i)];
		}

		this.brd = new Board(board);
		int maxCount = 0;
		int[][] vline = new int[N + 2][N + 2];
		for (int h = 1; h <= N; ++h) {
			if (wc % h == 0 && wc / h <= N) {
				int w = wc / h;
				for (int c = 1; c <= N; ++c) {
					int count = 0;
					int r = 1;
					for (; r < h - 1; ++r) {
						if (brd.b[r][c]) ++count;
					}
					for (; r <= N; ++r) {
						if (brd.b[r][c]) ++count;
						vline[r - h + 1][c] = count;
						if (brd.b[r - h + 1][c]) --count;
					}
				}
				for (int r = 1; r <= N - h + 1; ++r) {
					int count = 0;
					int c = 1;
					for (; c < w; ++c) {
						count += vline[r][c];
					}
					for (; c <= N; ++c) {
						count += vline[r][c];
						if (count > maxCount) {
							maxCount = count;
							origRect = new Rectangle(c - w + 1, r, w, h);
						}
						count -= vline[r][c - w + 1];
					}
				}
			}
		}
		stripe = origRect.height == N || origRect.width == N;
		int[] distX = new int[N + 1];
		int[] distY = new int[N + 1];
		distPena = new int[N + 1][N + 1];
		for (int i = 1; i <= N; ++i) {
			if (i < origRect.y) {
				distY[i] = origRect.y - i;
			} else if (i >= origRect.y + origRect.height) {
				distY[i] = i - origRect.y - origRect.height + 1;
			}
			if (i < origRect.x) {
				distX[i] = origRect.x - i;
			} else if (i >= origRect.x + origRect.width) {
				distX[i] = i - origRect.x - origRect.width + 1;
			}
		}
		int rectFromEdge = Math.min(Math.min(origRect.y - 1, N + 1 - origRect.y - origRect.height),
				Math.min(origRect.x - 1, N + 1 - origRect.x - origRect.width));
		insertL = Math.max(1, origRect.x - 5);
		insertR = Math.min(N, origRect.x + origRect.width + 4);
		insertT = Math.max(1, origRect.y - 5);
		insertB = Math.min(N, origRect.y + origRect.height + 4);
		if (!stripe) {
			ArrayList<Integer> pos = new ArrayList<Integer>();
			int margin = rectFromEdge + 2;
			if (origRect.y - 1 <= rectFromEdge + margin) {
				for (int i = insertL; i <= insertR; ++i) {
					pos.add(i + 2 * 256);
				}
			}
			if (N + 1 - origRect.y - origRect.height <= rectFromEdge + margin) {
				for (int i = insertL; i <= insertR; ++i) {
					pos.add(i + 3 * 256);
				}
			}
			if (origRect.x - 1 <= rectFromEdge + margin) {
				for (int i = insertT; i <= insertB; ++i) {
					pos.add(i + 256);
				}
			}
			if (N + 1 - origRect.x - origRect.width <= rectFromEdge + margin) {
				for (int i = insertT; i <= insertB; ++i) {
					pos.add(i);
				}
			}
			insertPos = new int[pos.size()];
			for (int i = 0; i < pos.size(); ++i) {
				insertPos[i] = pos.get(i);
			}
		}

		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int far = distY[i] + distX[j];
				int edge = Math.min(Math.min(i, N + 1 - i), Math.min(j, N + 1 - j));
				if (far > (rectFromEdge + 1) * 2 && edge < far) {
					distPena[i][j] = sq(edge + rectFromEdge + 5) * 1 * factorDist;
				} else {
					distPena[i][j] = (sq(distY[i]) + sq(distX[j])) * factorDist;
				}
			}
		}
		rsBuf = new int[wc];
		csBuf = new int[wc];
		visited = new boolean[N + 1][N + 1];

		logln("origRect:" + origRect);
		timer.end(0);
	}

	ArrayList<Pair<Integer, Integer>> solve() {
		final int tryShift = 20;
		final int leftNextCountIncrease = 10;

		int[] bufLine = new int[N * 4 + 10];
		ArrayList<Board> boards = new ArrayList<Board>();
		this.brd.score = this.brd.score();
		boards.add(this.brd);
		int leftNextCount = leftNext;
		if (ans != null) {
			leftNextCount *= turn > 30 ? 3 : 2;
		}
		TestResult[] worse = new TestResult[tryShift * (leftNextCount + leftNextCountIncrease)];
		int[] prevUsed = new int[leftNextCount + leftNextCountIncrease];
		for (moveCount = 0; ans == null || moveCount < ans.size() - 1; ++moveCount) {
			if (moveCount == N * N * 30 / 100) {
				logln("stop at turn " + moveCount);
				break;
			}
			if (System.currentTimeMillis() - startTime > TIMELIMIT) {
				logln("giveup at turn " + moveCount);
				break;
			}
			if ((ans == null && moveCount == N * N * 15 / 100) || ans != null && moveCount == ans.size() * 3 / 4) {
				leftNextCount += leftNextCountIncrease;
			}
			ArrayList<TestResult> next = new ArrayList<TestResult>(tryShift * boards.size());
			int worsec = 0;
			int target = boards.get(0).score + 50;
			for (Board b : boards) {
				timer.start(4);
				int lineCount = 0;
				int[] insertAt;
				if (b.current) {
					if (!stripe) {
						insertAt = insertPos;
						lineCount = insertPos.length;
					} else {
						insertAt = bufLine;
						for (int k = insertT; k <= insertB; ++k) {
							if (b.rc[k] != N) {
								if (!(b.moveR == k && b.moveC == 0)) {
									bufLine[lineCount++] = k;
								}
								if (!(b.moveR == k && b.moveC == N + 1)) {
									bufLine[lineCount++] = k + 256;
								}
							}
						}
						for (int k = insertL; k <= insertR; ++k) {
							if (b.cc[k] != N) {
								if (!(b.moveC == k && b.moveR == N + 1)) {
									bufLine[lineCount++] = k + 2 * 256;
								}
								if (!(b.moveC == k && b.moveR == 0)) {
									bufLine[lineCount++] = k + 3 * 256;
								}
							}
						}
					}
				} else {
					insertAt = bufLine;
					for (int k = Math.min(b.lb, b.tb), end = Math.max(b.rb, b.bb); k <= end; ++k) {
						if (b.rc[k] != 0) {
							if (!(b.moveR == k && b.moveC == 0)) {
								bufLine[lineCount++] = k;
							}
							if (!(b.moveR == k && b.moveC == N + 1)) {
								bufLine[lineCount++] = k + 256;
							}
						}
						if (b.cc[k] != 0) {
							if (!(b.moveC == k && b.moveR == N + 1)) {
								bufLine[lineCount++] = k + 2 * 256;
							}
							if (!(b.moveC == k && b.moveR == 0)) {
								bufLine[lineCount++] = k + 3 * 256;
							}
						}
					}
				}
				if (lineCount > tryShift) {
					shuffle(insertAt, lineCount, tryShift);
				}
				timer.end(4);
				timer.start(2);
				for (int j = 0, last = Math.min(tryShift, lineCount); j < last; ++j) {
					TestResult res = b.test(insertAt[j] / 256, insertAt[j] % 256);
					if (res.score <= target) {
						next.add(res);
					} else {
						worse[worsec++] = res;
					}
				}
				timer.end(2);
			}
			timer.start(5);
			boards.clear();
			final int randStart;
			if ((N < 40 || origRect.width * 2 < origRect.height || origRect.height * 2 < origRect.width) && moveCount > N * N * 5 / 100) {
				randStart = leftNextCount * 3 / 4;
			} else {
				randStart = leftNextCount;
			}
			int worseI = 0;
			if (next.size() < randStart) {
				if (randStart - next.size() < 25) {
					while (next.size() < randStart && worseI < worsec) {
						int minScore = Integer.MAX_VALUE;
						int minI = 0;
						for (int i = worseI; i < worsec; ++i) {
							if (worse[i].score < minScore) {
								minScore = worse[i].score;
								minI = i;
							}
						}
						next.add(worse[minI]);
						worse[minI] = worse[worseI];
						++worseI;
					}
				} else {
					Arrays.sort(worse, 0, worsec);
					while (next.size() < randStart && worseI < worsec) {
						next.add(worse[worseI++]);
					}
				}
			} else {
				Collections.sort(next);
				while (next.size() >= randStart) {
					worse[worsec++] = next.get(next.size() - 1);
					next.remove(next.size() - 1);
				}
			}
			while (next.size() < leftNextCount && worseI < worsec) {
				int from = rand.nextInt(worsec - worseI) + worseI;
				next.add(worse[from]);
				worse[from] = worse[worseI];
				++worseI;
			}
			timer.end(5);
			for (int j = 0, last = Math.min(next.size(), leftNextCount); j < last; ++j) {
				++prevUsed[next.get(j).parent.index];
			}
			for (int j = 0, last = Math.min(next.size(), leftNextCount); j < last; ++j) {
				timer.start(7);
				--prevUsed[next.get(j).parent.index];
				boolean move = prevUsed[next.get(j).parent.index] == 0;
				timer.end(7);
				Board board = next.get(j).create(move, j);
				if (board.finish()) {
					logln("finished at turn " + (moveCount + 1));
					logln("eval:" + board.score);
					return createAnswer(board);
				}
				boards.add(board);
			}
		}

		//		if (ans == null) {
		//			return createAnswer(boards.get(0));
		//		}
		return null;
	}

	ArrayList<Pair<Integer, Integer>> createAnswer(Board board) {
		ArrayList<Pair<Integer, Integer>> ret = new ArrayList<Pair<Integer, Integer>>(moveCount + 1);
		while (board.parent != null) {
			ret.add(makePair(board.moveR - 1, board.moveC - 1));
			board = board.parent;
		}
		Collections.reverse(ret);
		return ret;
	}

	final class Board {

		boolean[][] b;
		byte[][] sur;
		int index;
		boolean current = false;
		Board parent;
		int moveR, moveC;
		int[] rc, cc;
		//		int[] scRow, scCol;
		int lb, rb, tb, bb;
		int score;
		int isolate = 0;

		Board(String[] source) {
			wc = 0;
			b = boardPool[0][0];
			sur = surPool[0][0];
			index = 0;
			rc = new int[N + 2];
			cc = new int[N + 2];
			//			scRow = new int[N + 1];
			//			scCol = new int[N + 1];
			for (int i = 0; i <= N + 1; ++i) {
				for (int j = 0; j <= N + 1; ++j) {
					sur[i][j] = 0;
				}
			}
			for (int i = 1; i <= N; ++i) {
				for (int j = 1; j <= N; ++j) {
					b[i][j] = source[i - 1].charAt(j - 1) == 'X';
					if (b[i][j]) {
						++wc;
						++rc[i];
						++cc[j];
						sur[i + 1][j] += 8;
						sur[i - 1][j] += 2;
						sur[i][j + 1] += 4;
						sur[i][j - 1] += 1;
					}
				}
			}
			for (int i = 1; i <= N; ++i) {
				if (rc[i] != 0) bb = i;
				if (rc[N - i + 1] != 0) tb = N - i + 1;
				if (cc[i] != 0) rb = i;
				if (cc[N - i + 1] != 0) lb = N - i + 1;
			}
		}

		Board(Board other, boolean move, int index) {
			parent = other;
			current = other.current;
			isolate = other.isolate;
			lb = other.lb;
			rb = other.rb;
			tb = other.tb;
			bb = other.bb;
			this.index = index;
			int from = moveCount & 1;
			int to = 1 - from;
			if (move) {
				boolean[][] tmp = boardPool[to][index];
				boardPool[to][index] = boardPool[from][other.index];
				boardPool[from][other.index] = tmp;
				this.b = boardPool[to][index];
				byte[][] tmp2 = surPool[to][index];
				surPool[to][index] = surPool[from][other.index];
				surPool[from][other.index] = tmp2;
				this.sur = surPool[to][index];
				rc = other.rc;
				cc = other.cc;
				//				scRow = other.scRow;
				//				scCol = other.scCol;
			} else {
				b = boardPool[to][index];
				sur = surPool[to][index];
				System.arraycopy(other.sur[0], 0, sur[0], 0, N + 2);
				for (int i = 1; i <= N; ++i) {
					System.arraycopy(other.b[i], 1, b[i], 1, N);
					System.arraycopy(other.sur[i], 0, sur[i], 0, N + 2);
				}
				System.arraycopy(other.sur[N + 1], 0, sur[N + 1], 0, N + 2);
				rc = other.rc.clone();
				cc = other.cc.clone();
				//				scRow = other.scRow.clone();
				//				scCol = other.scCol.clone();
			}
		}

		void calcInitialState() {
			for (int i = 1; i <= N; ++i) {
				for (int j = 1; j <= N; ++j) {
					if (b[i][j]) {
						if (sur[i][j] == 0) ++isolate;
						//						scRow[i] += distPena[i][j];
						//						scCol[j] += distPena[i][j];
						//						scRow[i] += ordPena[sur[i][j]];
						//						scCol[j] += ordPena[sur[i][j]];
					}
				}
			}
		}

		int score() {
			int ret = current ? currentPena : 0;
			int[] order = new int[16];
			for (int i = 1; i <= N; ++i) {
				for (int j = 1; j <= N; ++j) {
					if (!b[i][j]) continue;
					ret += distPena[i][j];
					++order[sur[i][j]];
				}
			}
			for (int i = 0; i < 16; ++i) {
				ret += order[i] * ordPena[i];
			}
			return ret;
		}

		boolean finish() {
			if (this.current) return false;
			if (this.isolate != 0) return false;
			timer.start(1);
			for (int i = 1; i <= N; ++i) {
				if (b[tb][i]) {
					int ns = 1;
					rsBuf[0] = tb;
					csBuf[0] = i;
					visited[tb][i] = true;
					for (int k = 0; k < ns; ++k) {
						for (int l = 0; l < 4; ++l) {
							int nr = rsBuf[k] + DR[l];
							int nc = csBuf[k] + DC[l];
							if (b[nr][nc] && !visited[nr][nc]) {
								rsBuf[ns] = nr;
								csBuf[ns] = nc;
								visited[nr][nc] = true;
								++ns;
							}
						}
					}
					for (int j = 0; j < ns; ++j) {
						visited[rsBuf[j]][csBuf[j]] = false;
					}
					timer.end(1);
					return ns == wc;
				}
			}
			return false;
		}

		void testHorz(TestResult ret, int line, int diff) {
			//			ret.score -= scRow[line];
			final int start, end;
			if (current) {
				ret.score -= currentPena;
				if (diff == 1) {
					start = lb == 1 ? 1 : lb - 1;
					end = N;
				} else {
					start = 1;
					end = rb == N ? N : rb + 1;
				}
			} else {
				start = lb == 1 ? 1 : lb - 1;
				end = rb == N ? N : rb + 1;
			}
			if (diff == 1 ? b[line][1] : b[line][N]) {
				ret.score += currentPena;
			}

			for (int i = start; i <= end; ++i) {
				final boolean next = b[line][i + diff];
				int curOrd = sur[line][i];
				if (current) {
					if (i == N && diff == 1) {
						curOrd &= 0xE;
					} else if (i == 1 && diff == -1) {
						curOrd &= 0xB;
					}
				}
				if (i == N - 1 && diff == -1 && b[line][N]) {
					curOrd += 1;
				} else if (i == 2 && diff == 1 && b[line][1]) {
					curOrd += 4;
				}
				if (b[line][i] != next) {
					if (next) {
						ret.score += distPena[line][i];
						int newOrd = (curOrd & 10) + (sur[line][i + diff] & 5);
						ret.score += ordPena[newOrd];
					} else {
						ret.score -= distPena[line][i];
						ret.score -= ordPena[curOrd];
					}
					if ((sur[line][i] & 8) != 0) {
						int oc = sur[line - 1][i];
						ret.score += ordPena[oc ^ 2] - ordPena[oc];
					}
					if ((sur[line][i] & 2) != 0) {
						int oc = sur[line + 1][i];
						ret.score += ordPena[oc ^ 8] - ordPena[oc];
					}
				} else if (next && (curOrd & 5) != (sur[line][i + diff] & 5)) {
					ret.score -= ordPena[curOrd];
					int newOrd = (curOrd & 10) + (sur[line][i + diff] & 5);
					ret.score += ordPena[newOrd];
				}

				//				if (next) {
				//					int newOrd = (sur[line][i] & 10) + (sur[line][i + diff] & 5);
				//					ret.score += ordPena[newOrd];
				//					ret.score += distPena[line][i];
				//				}
				//				if (next == b[line][i]) continue;
				//				if ((sur[line][i] & 8) != 0) {
				//					int oc = sur[line - 1][i];
				//					ret.score += ordPena[oc ^ 2] - ordPena[oc];
				//				}
				//				if ((sur[line][i] & 2) != 0) {
				//					int oc = sur[line + 1][i];
				//					ret.score += ordPena[oc ^ 8] - ordPena[oc];
				//				}
			}
		}

		void testVert(TestResult ret, int line, int diff) {
			//			ret.score -= scCol[line];
			final int start, end;
			if (current) {
				ret.score -= currentPena;
				if (diff == 1) {
					start = tb == 1 ? 1 : tb - 1;
					end = N;
				} else {
					start = 1;
					end = bb == N ? N : bb + 1;
				}
			} else {
				start = tb == 1 ? 1 : tb - 1;
				end = bb == N ? N : bb + 1;
			}
			if (diff == 1 ? b[1][line] : b[N][line]) {
				ret.score += currentPena;
			}
			for (int i = start; i <= end; ++i) {
				final boolean next = b[i + diff][line];
				int curOrd = sur[i][line];
				if (current) {
					if (i == N && diff == 1) {
						curOrd &= 0xD;
					} else if (i == 1 && diff == -1) {
						curOrd &= 0x7;
					}
				}
				if (i == N - 1 && diff == -1 && b[N][line]) {
					curOrd += 2;
				} else if (i == 2 && diff == 1 && b[1][line]) {
					curOrd += 8;
				}
				if (b[i][line] != next) {
					if (next) {
						ret.score += distPena[i][line];
						int newOrd = (curOrd & 5) + (sur[i + diff][line] & 10);
						ret.score += ordPena[newOrd];
					} else {
						ret.score -= distPena[i][line];
						ret.score -= ordPena[curOrd];
					}
					if ((sur[i][line] & 4) != 0) {
						int oc = sur[i][line - 1];
						ret.score += ordPena[oc ^ 1] - ordPena[oc];
					}
					if ((sur[i][line] & 1) != 0) {
						int oc = sur[i][line + 1];
						ret.score += ordPena[oc ^ 4] - ordPena[oc];
					}
				} else if (next && (curOrd & 10) != (sur[i + diff][line] & 10)) {
					ret.score -= ordPena[curOrd];
					int newOrd = (curOrd & 5) + (sur[i + diff][line] & 10);
					ret.score += ordPena[newOrd];
				}

				//				boolean next = b[i + diff][line];
				//				if (next) {
				//					int newOrd = (sur[i][line] & 5) + (sur[i + diff][line] & 10);
				//					ret.score += ordPena[newOrd];
				//					ret.score += distPena[i][line];
				//				}
				//				if (next == b[i][line]) continue;
				//				if ((sur[i][line] & 4) != 0) {
				//					int oc = sur[i][line - 1];
				//					ret.score += ordPena[oc ^ 1] - ordPena[oc];
				//				}
				//				if ((sur[i][line] & 1) != 0) {
				//					int oc = sur[i][line + 1];
				//					ret.score += ordPena[oc ^ 4] - ordPena[oc];
				//				}
			}
		}

		TestResult test(int dir, int line) {
			TestResult ret = new TestResult();
			ret.parent = this;
			ret.score = this.score;
			ret.dir = dir;
			ret.line = line;
			if (dir == 0) { // left
				b[line][N + 1] = current;
				if (current) sur[line][N] += 1;
				if (b[line][1]) sur[line][2] -= 4;
				testHorz(ret, line, 1);
				if (b[line][1]) sur[line][2] += 4;
				if (current) sur[line][N] -= 1;
				b[line][N + 1] = false;
			} else if (dir == 1) { // right
				b[line][0] = current;
				if (current) sur[line][1] += 4;
				if (b[line][N]) sur[line][N - 1] -= 1;
				testHorz(ret, line, -1);
				if (b[line][N]) sur[line][N - 1] += 1;
				if (current) sur[line][1] -= 4;
				b[line][0] = false;
			} else if (dir == 2) { // bottom
				b[0][line] = current;
				if (current) sur[1][line] += 8;
				if (b[N][line]) sur[N - 1][line] -= 2;
				testVert(ret, line, -1);
				if (b[N][line]) sur[N - 1][line] += 2;
				if (current) sur[1][line] -= 8;
				b[0][line] = false;
			} else if (dir == 3) { // top
				b[N + 1][line] = current;
				if (current) sur[N][line] += 2;
				if (b[1][line]) sur[2][line] -= 8;
				testVert(ret, line, 1);
				if (b[1][line]) sur[2][line] += 8;
				if (current) sur[N][line] -= 2;
				b[N + 1][line] = false;
			}
			return ret;
		}

		void shiftHorz(int row, int start, int end, int diff) {
			boolean newCurrent = b[row][start];
			b[row][end + diff] = current;
			if (current) ++rc[row];

			if (b[row][start]) {
				int ord = sur[row][start];
				//				int change = ordPena[ord];
				//				scRow[row] -= change;
				//				scCol[start] -= change;
				if (ord == 0) --isolate;
			}
			for (int i = start; i != end + diff; i += diff) {
				if (i != end && b[row][i + diff]) {
					int ord = sur[row][i + diff];
					//					int change = ordPena[ord];
					//					scRow[row] -= change;
					//					scCol[i + diff] -= change;
					if (ord == 0) --isolate;
				}
				if (b[row][i] != b[row][i + diff]) {
					int dif = b[row][i] ? -1 : 1;
					//					scRow[row] += distPena[row][i] * dif;
					//					scCol[i] += distPena[row][i] * dif;
					cc[i] += dif;
					sur[row - 1][i] ^= 2;
					sur[row + 1][i] ^= 8;
					sur[row][i + 1] ^= 4;
					sur[row][i - 1] ^= 1;
					b[row][i] = b[row][i + diff];
					if (b[row - 1][i]) {
						int ord = sur[row - 1][i];
						//						int change = ordPena[ord] - ordPena[ord ^ 2];
						//						scRow[row - 1] += change;
						//						scCol[i] += change;
						if (ord == 0) ++isolate;
						if (ord == 2) --isolate;
					}
					if (b[row + 1][i]) {
						int ord = sur[row + 1][i];
						//						int change = ordPena[ord] - ordPena[ord ^ 8];
						//						scRow[row + 1] += change;
						//						scCol[i] += change;
						if (ord == 0) ++isolate;
						if (ord == 8) --isolate;
					}
				}
				if (b[row][i - diff]) {
					int ord = sur[row][i - diff];
					//					int change = ordPena[ord];
					//					scRow[row] += change;
					//					scCol[i - diff] += change;
					if (ord == 0) ++isolate;
				}
			}
			if (b[row][end]) {
				int ord = sur[row][end];
				//				int change = ordPena[ord];
				//				scRow[row] += change;
				//				scCol[end] += change;
				if (ord == 0) ++isolate;
			}

			current = newCurrent;
			if (current) --rc[row];
			b[row][end + diff] = false;

			if (rc[row] == 0) {
				if (tb == row) {
					for (int i = tb + 1;; ++i) {
						if (rc[i] != 0) {
							tb = i;
							break;
						}
					}
				} else if (bb == row) {
					for (int i = bb - 1;; --i) {
						if (rc[i] != 0) {
							bb = i;
							break;
						}
					}
				}
			} else {
				tb = Math.min(tb, row);
				bb = Math.max(bb, row);
			}
		}

		void shiftVert(int col, int start, int end, int diff) {
			boolean newCurrent = b[start][col];
			b[end + diff][col] = current;
			if (current) ++cc[col];

			if (b[start][col]) {
				int ord = sur[start][col];
				//				int change = ordPena[ord];
				//				scCol[col] -= change;
				//				scRow[start] -= change;
				if (ord == 0) --isolate;
			}
			for (int i = start; i != end + diff; i += diff) {
				if (i != end && b[i + diff][col]) {
					int ord = sur[i + diff][col];
					//					int change = ordPena[ord];
					//					scCol[col] -= change;
					//					scRow[i + diff] -= change;
					if (ord == 0) --isolate;
				}
				if (b[i][col] != b[i + diff][col]) {
					int dif = b[i][col] ? -1 : 1;
					//					scCol[col] += distPena[i][col] * dif;
					//					scRow[i] += distPena[i][col] * dif;
					rc[i] += dif;
					sur[i][col - 1] ^= 1;
					sur[i][col + 1] ^= 4;
					sur[i + 1][col] ^= 8;
					sur[i - 1][col] ^= 2;
					b[i][col] = b[i + diff][col];
					if (b[i][col - 1]) {
						int ord = sur[i][col - 1];
						//						int change = ordPena[ord] - ordPena[ord ^ 1];
						//						scCol[col - 1] += change;
						//						scRow[i] += change;
						if (ord == 0) ++isolate;
						if (ord == 1) --isolate;
					}
					if (b[i][col + 1]) {
						int ord = sur[i][col + 1];
						//						int change = ordPena[ord] - ordPena[ord ^ 4];
						//						scCol[col + 1] += change;
						//						scRow[i] += change;
						if (ord == 0) ++isolate;
						if (ord == 4) --isolate;
					}
				}
				if (b[i - diff][col]) {
					int ord = sur[i - diff][col];
					//					int change = ordPena[ord];
					//					scCol[col] += change;
					//					scRow[i - diff] += change;
					if (ord == 0) ++isolate;
				}
			}
			if (b[end][col]) {
				int ord = sur[end][col];
				//				int change = ordPena[ord];
				//				scCol[col] += change;
				//				scRow[end] += change;
				if (ord == 0) ++isolate;
			}

			current = newCurrent;
			if (current) --cc[col];
			b[end + diff][col] = false;

			if (cc[col] == 0) {
				if (lb == col) {
					for (int i = lb + 1;; ++i) {
						if (cc[i] != 0) {
							lb = i;
							break;
						}
					}
				} else if (rb == col) {
					for (int i = rb - 1;; --i) {
						if (cc[i] != 0) {
							rb = i;
							break;
						}
					}
				}
			} else {
				lb = Math.min(lb, col);
				rb = Math.max(rb, col);
			}
		}

		void shiftRight(int row) {
			if (current) lb = 1;
			shiftHorz(row, N, 1, -1);
			if (cc[rb + 1] != 0) {
				++rb;
			} else {
				while (cc[rb] == 0) {
					--rb;
				}
			}
			if (cc[lb] == 0) ++lb;
		}

		void shiftLeft(int row) {
			if (current) rb = N;
			shiftHorz(row, 1, N, 1);
			if (cc[lb - 1] != 0) {
				--lb;
			} else {
				while (cc[lb] == 0) {
					++lb;
				}
			}
			if (cc[rb] == 0) --rb;
		}

		void shiftBottom(int col) {
			if (current) tb = 1;
			shiftVert(col, N, 1, -1);
			if (rc[bb + 1] != 0) {
				++bb;
			} else {
				while (rc[bb] == 0) {
					--bb;
				}
			}
			if (rc[tb] == 0) ++tb;
		}

		void shiftTop(int col) {
			if (current) bb = N;
			shiftVert(col, 1, N, 1);
			if (rc[tb - 1] != 0) {
				--tb;
			} else {
				while (rc[tb] == 0) {
					++tb;
				}
			}
			if (rc[bb] == 0) --bb;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i <= N; ++i) {
				for (int j = 1; j <= N; ++j) {
					sb.append(b[i][j] ? 'X' : '.');
				}
				sb.append("\n");
			}
			sb.append(lb + " " + rb + " " + tb + " " + bb + "\n");
			sb.append("\n");
			for (int i = 1; i <= N; ++i) {
				sb.append(Arrays.toString(sur[i]) + "\n");
			}
			sb.append("\n");
			sb.append("rc:" + Arrays.toString(rc) + "\n");
			sb.append("cc:" + Arrays.toString(cc) + "\n");
			sb.append("isolate:" + isolate + "\n");
			sb.append("current:" + current + "\n");
			sb.append("move:" + moveR + " " + moveC + "\n");
			return sb.toString();
		}

		void validate() {
			//			int[] rsc = new int[N + 1];
			//			int[] csc = new int[N + 1];
			//			for (int i = 1; i <= N; ++i) {
			//				for (int j = 1; j <= N; ++j) {
			//					if (b[i][j]) {
			//						rsc[i] += distPena[i][j] + ordPena[sur[i][j]];
			//						csc[j] += distPena[i][j] + ordPena[sur[i][j]];
			//					}
			//				}
			//			}
			//			if (!Arrays.equals(rsc, scRow)) {
			//				logln(this);
			//				logln("scRow");
			//				logln(Arrays.toString(scRow));
			//				logln(Arrays.toString(rsc));
			//				System.exit(0);
			//			}
			//			if (!Arrays.equals(csc, scCol)) {
			//				logln(this);
			//				logln("scCol");
			//				logln(Arrays.toString(scCol));
			//				logln(Arrays.toString(csc));
			//				System.exit(0);
			//			}

			int realScore = score();
			if (realScore != score) {
				logln(this);
				logln(score + " " + realScore);
				System.exit(0);
			}
			//			int scr = current ? currentPena : 0;
			//			int scc = scr;
			//			for (int i = 1; i <= N; ++i) {
			//				scr += scRow[i];
			//				scc += scCol[i];
			//			}
			//			if (scr != realScore || scc != realScore) {
			//				logln(this);
			//				logln(realScore + " " + scr + " " + scc);
			//				System.exit(0);
			//			}
			int[][] surc = new int[N + 2][N + 2];
			int[] rcc = new int[N + 2];
			int[] ccc = new int[N + 2];
			for (int i = 1; i <= N; ++i) {
				for (int j = 1; j <= N; ++j) {
					if (b[i][j]) {
						++rcc[i];
						++ccc[j];
						surc[i + 1][j] += 8;
						surc[i - 1][j] += 2;
						surc[i][j + 1] += 4;
						surc[i][j - 1] += 1;
					}
				}
			}
			if (!Arrays.equals(rcc, rc) || !Arrays.equals(ccc, cc)) {
				logln(this);
				logln(Arrays.toString(rcc));
				logln(Arrays.toString(ccc));
				System.exit(0);
			}
			for (int i = 0; i <= N + 1; ++i) {
				for (int j = 0; j <= N + 1; ++j) {
					if (surc[i][j] != sur[i][j]) {
						logln(this);
						for (int k = 0; k <= N + 1; ++k) {
							logln(Arrays.toString(surc[k]));
						}
						logln(i + " " + j + " " + surc[i][j] + " " + sur[i][j]);
						System.exit(0);
					}
				}
			}
		}
	}

	final class TestResult implements Comparable<TestResult> {
		Board parent;
		int score;
		int dir;
		int line;

		Board create(boolean move, int index) {
			timer.start(3);
			Board ret = new Board(parent, move, index);
			timer.end(3);
			timer.start(6);
			ret.score = score;
			if (dir == 0) {
				ret.shiftLeft(line);
				ret.moveR = line;
				ret.moveC = N + 1;
			} else if (dir == 1) {
				ret.shiftRight(line);
				ret.moveR = line;
				ret.moveC = 0;
			} else if (dir == 2) {
				ret.shiftBottom(line);
				ret.moveR = 0;
				ret.moveC = line;
			} else if (dir == 3) {
				ret.shiftTop(line);
				ret.moveR = N + 1;
				ret.moveC = line;
			}
			timer.end(6);
//			ret.validate();
			return ret;
		}

		public int compareTo(TestResult o) {
			return this.score - o.score;
		}

		public String toString() {
			return dir + " " + line;
		}

	}

	void shuffle(int[] a, int len, int count) {
		for (int i = 0; i < count; ++i) {
			int to = rand.nextInt(len - i) + i;
			swap(a, i, to);
		}
	}

	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}

	static int sq(int v) {
		return v * v;
	}

	static void log(Object obj) {
		if (DEBUG) {
			System.err.print(obj);
		}
	}

	static void logln(Object obj) {
		if (DEBUG) {
			System.err.println(obj);
		}
	}

	static void logln() {
		if (DEBUG) {
			System.err.println();
		}
	}

	static <T1, T2> Pair<T1, T2> makePair(T1 v1, T2 v2) {
		return new Pair<T1, T2>(v1, v2);
	}

	static final class Pair<T1, T2> {
		T1 f;
		T2 s;

		Pair(T1 v1, T2 v2) {
			this.f = v1;
			this.s = v2;
		}

		public String toString() {
			return "<" + this.f + "," + this.s + ">";
		}
	}

	static final class Timer {
		long[] sum = new long[9];
		long[] start = new long[sum.length];

		void start(int i) {
			if (MEASURE_TIME) {
				start[i] = System.currentTimeMillis();
			}
		}

		void end(int i) {
			if (MEASURE_TIME) {
				sum[i] += System.currentTimeMillis() - start[i];
			}
		}
	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

	}

}
