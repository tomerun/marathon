import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Visualizer {
	static final int MIN_SZ = 20, MAX_SZ = 100;
	static final double MIN_WHITE_SHARE = 0.05, MAX_WHITE_SHARE = 0.4;
	static final double MIN_POWER = 1.25, MAX_POWER = 1.75;
	static final int[] dr = { 0, 1, 0, -1 }, dc = { 1, 0, -1, 0 };
	int SZ; // size of the grid
	double white_share;
	int white_count;
	volatile char[][] grid; // the grid itself
	volatile char current_tile; // the tile in hand used during shifts
	volatile int row, col; // the cell where the shift applies
	volatile int shift_index; // the index of the current shift
	volatile int maxConnect;

	boolean isConnected() {
		char[][] g = new char[SZ][SZ];
		for (int i = 0; i < SZ; ++i) {
			g[i] = Arrays.copyOf(grid[i], SZ);
		}
		boolean marked_connected = false;
		for (int r0 = 0; r0 < SZ; ++r0)
			for (int c0 = 0; c0 < SZ; ++c0) {
				if (g[r0][c0] == 'X' || g[r0][c0] == 'N') {
					if (marked_connected) {
						return false; // already marked the connected component => something is not connected to it
					}
					int[] rs = new int[SZ * SZ], cs = new int[SZ * SZ];
					int ns = 1;
					rs[0] = r0;
					cs[0] = c0;
					g[r0][c0] = '+';
					for (int i = 0; i < ns; ++i) {
						for (int k = 0; k < 4; ++k) {
							int nr = rs[i] + dr[k];
							int nc = cs[i] + dc[k];
							if (nr >= 0 && nr < SZ && nc >= 0 && nc < SZ && g[nr][nc] != '.') {
								g[nr][nc] = '.';
								rs[ns] = nr;
								cs[ns] = nc;
								++ns;
							}
						}
					}
					marked_connected = true;
				}
			}
		return true;
	}

	void apply_shift(int dir, int pos) {
		char tmp;
		if (dir == 0) { // shift pos-th row to the right
			tmp = grid[pos][SZ - 1];
			for (int i = SZ - 1; i > 0; --i) {
				grid[pos][i] = grid[pos][i - 1];
			}
			grid[pos][0] = current_tile;
			current_tile = tmp;
		}
		if (dir == 1) { // shift pos-th row to the left
			tmp = grid[pos][0];
			for (int i = 0; i < SZ - 1; ++i) {
				grid[pos][i] = grid[pos][i + 1];
			}
			grid[pos][SZ - 1] = current_tile;
			current_tile = tmp;
		}
		if (dir == 2) { // shift pos-th column down
			tmp = grid[SZ - 1][pos];
			for (int i = SZ - 1; i > 0; --i) {
				grid[i][pos] = grid[i - 1][pos];
			}
			grid[0][pos] = current_tile;
			current_tile = tmp;
		}
		if (dir == 3) { // shift pos-th column up
			tmp = grid[0][pos];
			for (int i = 0; i < SZ - 1; ++i) {
				grid[i][pos] = grid[i + 1][pos];
			}
			grid[SZ - 1][pos] = current_tile;
			current_tile = tmp;
		}
	}

	boolean apply_shift_returned() {
		if (col == -1 && row > -1 && row < SZ) {
			apply_shift(0, row);
			col = SZ;
			return true;
		}
		if (col == SZ && row > -1 && row < SZ) {
			apply_shift(1, row);
			col = -1;
			return true;
		}
		if (row == -1 && col > -1 && col < SZ) {
			apply_shift(2, col);
			row = SZ;
			return true;
		}
		if (row == SZ && col > -1 && col < SZ) {
			apply_shift(3, col);
			row = -1;
			return true;
		}
		return false;
	}

	String fileName(long seed) {
		DecimalFormat format = new DecimalFormat("0000");
		return "testdata/" + format.format(seed) + ".dat";
	}

	void read(long seed) throws Exception {
		Scanner sc = new Scanner(new File(fileName(seed)));
		SZ = sc.nextInt();
		white_share = sc.nextDouble();
		white_count = (int) (SZ * SZ * white_share + 0.5);
		grid = new char[SZ][SZ];
		current_tile = '.';
		for (int i = 0; i < SZ; ++i) {
			grid[i] = sc.next().toCharArray();
		}
	}

	void write(long seed) throws Exception {
		PrintWriter writer = new PrintWriter(new FileWriter(fileName(seed)));
		writer.println(SZ + " " + white_share);
		for (int i = 0; i < SZ; ++i) {
			writer.println(String.valueOf(grid[i]));
		}
		writer.flush();
	}

	void generate_real(long seed) throws Exception {
		SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
		rnd.setSeed(seed);
		if (1001 <= seed && seed <= 2000) {
			SZ = (int) (MIN_SZ + (seed - 1001) / (1000.0 / (MAX_SZ - MIN_SZ + 1)));
		} else {
			if (seed == 1) {
				SZ = MIN_SZ;
			} else {
				SZ = rnd.nextInt(MAX_SZ - MIN_SZ + 1) + MIN_SZ;
			}
		}
		if (debug) {
			System.out.println("Grid size = " + SZ);
		}
		current_tile = '.';

		int shifts_count;
		do {
			grid = new char[SZ][SZ];
			for (int i = 0; i < SZ; ++i) {
				Arrays.fill(grid[i], '.');
			}
			boolean stripe_mode = (rnd.nextInt(3) == 2);
			while (true) {
				int min_row = rnd.nextInt(SZ), max_row = rnd.nextInt(SZ - min_row) + min_row;
				int min_col = rnd.nextInt(SZ), max_col = rnd.nextInt(SZ - min_col) + min_col;
				if (stripe_mode) {
					if (rnd.nextInt(2) == 1) {
						min_row = 0;
						max_row = SZ - 1;
					} else {
						min_col = 0;
						max_col = SZ - 1;
					}
				}
				white_share = (max_row - min_row + 1) * (max_col - min_col + 1) * 1.0 / SZ / SZ;
				if (white_share < MIN_WHITE_SHARE || white_share > MAX_WHITE_SHARE) {
					continue;
				}
				for (int i = min_row; i <= max_row; ++i) {
					for (int j = min_col; j <= max_col; ++j) {
						grid[i][j] = 'X';
					}
				}
				break;
			}

			shifts_count = (int) Math.pow(SZ, rnd.nextDouble() * (MAX_POWER - MIN_POWER) + MIN_POWER);
			int direction, position, white_shifted, black_shifted;
			for (int i = 0; i < shifts_count || current_tile == 'X'; ++i) {
				do {
					direction = rnd.nextInt(4);
					position = rnd.nextInt(SZ);
					white_shifted = 0;
					black_shifted = 0;
					for (int j = 0; j < SZ; ++j) {
						if (direction < 2 && grid[position][j] == 'X' || direction > 1 && grid[j][position] == 'X') {
							++white_shifted;
						} else {
							++black_shifted;
						}
					}
				} while (white_shifted == 0 || black_shifted == 0);
				apply_shift(direction, position);
			}
		} while (isConnected()); // if the result is connected, restart generation

		if (debug) {
			System.out.println("% of white = " + white_share);
			System.out.println("Shifts done = " + shifts_count);
			for (int i = 0; i < SZ; ++i) {
				System.out.println(new String(grid[i]));
			}
		}
	}

	void generate(long seed) throws Exception {
		//		read(seed);
		generate_real(seed);
		//		write(seed);
	}

	public Result runTest(long seed) throws Exception {
		generate(seed);
		if (manual) {
			findMaxConnect();
		}
		if (vis) {
			jf.setSize((SZ + 2) * side + 150, (SZ + 2) * side + 38);
			jf.setVisible(true);
			draw(1);
		}
		Result res = new Result();
		res.seed = seed;
		res.SZ = this.SZ;
		if (!manual) {
			String gridin[] = new String[SZ];
			for (int i = 0; i < SZ; ++i)
				gridin[i] = new String(grid[i]);
			long startTime = System.currentTimeMillis();
			BlackAndWhiteGame obj = new BlackAndWhiteGame();
			String[] shifts = obj.makeConnected(gridin);
			res.elapsed = System.currentTimeMillis() - startTime;
			res.whiteCount = (int) (SZ * SZ * white_share + 0.5);
			res.isStripe = obj.origRect.width == SZ || obj.origRect.height == SZ;

			if (shifts == null || shifts.length == 0) {
				System.err.println("Your return must contain at least one element.");
				return res;
			}
			for (shift_index = 0; shift_index < shifts.length; ++shift_index) {
				String[] s = shifts[shift_index].split(" ");
				if (s.length != 2) {
					System.err.println("Each element of your return must be formatted as \"row col\".");
					return res;
				}
				try {
					row = Integer.parseInt(s[0]);
					col = Integer.parseInt(s[1]);
				} catch (Exception e) {
					System.err.println("Each element of your return must be formatted as \"row col\".");
					return res;
				}
				draw(2);
				if (!apply_shift_returned()) {
					System.err.println("Element " + shift_index + " of your return specifies an invalid shift.");
					return res;
				}
				findMaxConnect();
				draw(2);
				draw(1);
			}
		} else {
			shift_index = 0;
			ready = false;
			while (!ready)
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
				}
			manual = false;
		}

		if (current_tile != '.') {
			System.err.println("After all shifts all white tiles must stay on the board.");
			return res;
		}
		if (!isConnected()) {
			System.err.println("After all shifts all white tiles must be connected.");
			return res;
		}
		res.score = Math.max(0, 1 - shift_index * 1.0 / SZ / SZ);
		return res;
	}

	void findMaxConnect() {
		maxConnect = 0;
		int bestR = 0;
		int bestC = 0;
		boolean[][] visited = new boolean[SZ][SZ];
		int[] rs = new int[SZ * SZ], cs = new int[SZ * SZ];
		for (int i = 0; i < SZ; ++i) {
			for (int j = 0; j < SZ; ++j) {
				if (grid[i][j] != '.' && !visited[i][j]) {
					int ns = 1;
					rs[0] = i;
					cs[0] = j;
					visited[i][j] = true;
					for (int k = 0; k < ns; ++k) {
						grid[rs[k]][cs[k]] = 'X';
						for (int l = 0; l < 4; ++l) {
							int nr = rs[k] + dr[l];
							int nc = cs[k] + dc[l];
							if (0 <= nr && nr < SZ && 0 <= nc && nc < SZ && grid[nr][nc] != '.' && !visited[nr][nc]) {
								rs[ns] = nr;
								cs[ns] = nc;
								visited[nr][nc] = true;
								++ns;
							}
						}
					}
					if (ns > maxConnect) {
						maxConnect = ns;
						bestR = i;
						bestC = j;
					}
				}
			}
		}
		rs[0] = bestR;
		cs[0] = bestC;
		grid[bestR][bestC] = 'N';
		int ns = 1;
		for (int k = 0; k < ns; ++k) {
			for (int l = 0; l < 4; ++l) {
				int nr = rs[k] + dr[l];
				int nc = cs[k] + dc[l];
				if (0 <= nr && nr < SZ && 0 <= nc && nc < SZ && grid[nr][nc] == 'X') {
					rs[ns] = nr;
					cs[ns] = nc;
					grid[nr][nc] = 'N';
					++ns;
				}
			}
		}
	}

	static int side = 10;
	static boolean debug, vis, manual, ready;
	static int del = 10;
	JFrame jf;
	Vis v;
	volatile int phase;

	void draw(int ph) {
		if (!vis) return;
		phase = ph;
		v.repaint();
		try {
			Thread.sleep(del);
		} catch (Exception e) {
		}
	}

	public class Vis extends JPanel {
		int hoverR = -1;
		int hoverC = -1;
		ArrayList<Integer> moveR = new ArrayList<Integer>();
		ArrayList<Integer> moveC = new ArrayList<Integer>();

		public void paint(Graphics gr) {
			BufferedImage bi = new BufferedImage((SZ + 2) * side + 150, (SZ + 2) * side + 1, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();
			g2.setColor(new Color(0xEEEEEE));
			g2.fillRect(0, 0, (SZ + 2) * side + 150, (SZ + 2) * side + 1);
			g2.setColor(new Color(0xAAAAAA));
			g2.fillRect(0, 0, (SZ + 2) * side, (SZ + 2) * side);
			g2.setColor(Color.BLACK);
			for (int i = 0; i <= SZ + 2; ++i)
				g2.drawLine(0, i * side, (SZ + 2) * side, i * side);
			for (int i = 0; i <= SZ + 2; ++i)
				g2.drawLine(i * side, 0, i * side, (SZ + 2) * side);
			for (int i = 0; i < SZ; ++i)
				for (int j = 0; j < SZ; ++j) {
					boolean hovered = (i == hoverR || j == hoverC);
					if (grid[i][j] == '.') {
						g2.setColor(new Color(hovered ? 0x222244 : 0x444444));
					} else if (grid[i][j] == 'N') {
						g2.setColor(new Color(hovered ? 0x4444FF : 0x8888FF));
					} else {
						g2.setColor(new Color(hovered ? 0xCCCCFF : 0xFFFFFF));
					}
					g2.fillRect((j + 1) * side + 1, (i + 1) * side + 1, side - 1, side - 1);
				}

			int i, j;
			if (phase == 1) { // draw the tile "in hand"
				j = SZ + 3;
				i = 1;
				g2.setColor(Color.BLACK);
				g2.drawLine(j * side, i * side, (j + 1) * side, i * side);
				g2.drawLine(j * side, (i + 1) * side, (j + 1) * side, (i + 1) * side);
				g2.drawLine(j * side, i * side, j * side, (i + 1) * side);
				g2.drawLine((j + 1) * side, i * side, (j + 1) * side, (i + 1) * side);
			} else { // draw the tile on the border of the board, cell (row, col)
				j = col + 1;
				i = row + 1;
			}
			if (current_tile == '.') {
				g2.setColor(new Color(0x444444));
			} else {
				g2.setColor(new Color(0xFFFFFF));
			}
			g2.fillRect(j * side + 1, i * side + 1, side - 1, side - 1);

			g2.setColor(Color.BLACK);
			char[] c = ("" + shift_index).toCharArray();
			g2.setFont(new Font("Arial", Font.BOLD, 14));
			g2.drawChars(c, 0, c.length, (SZ + 3) * side, 5 * side);
			c = (maxConnect + " / " + white_count).toCharArray();
			g2.drawChars(c, 0, c.length, (SZ + 3) * side, 7 * side);
			if (hoverR != -1 || hoverC != -1) {
				c = ("(" + hoverR + "," + hoverC + ")").toCharArray();
				g2.drawChars(c, 0, c.length, (SZ + 3) * side, 9 * side);
			}

			gr.drawImage(bi, 0, 0, (SZ + 2) * side + 150, (SZ + 2) * side + 1, null);
		}

		void undo() {
			if (shift_index == 0) return;
			--shift_index;
			row = moveR.get(shift_index);
			col = moveC.get(shift_index);
			if (row == -1) {
				row = SZ;
			} else if (row == SZ) {
				row = -1;
			}
			if (col == -1) {
				col = SZ;
			} else if (col == SZ) {
				col = -1;
			}
			moveR.remove(shift_index);
			moveC.remove(shift_index);
			apply_shift_returned();
			findMaxConnect();
			draw(1);
		}

		public Vis() {
			addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (!manual) return;
					row = e.getY() / side - 1;
					col = e.getX() / side - 1;
					if (row != -1 && row != SZ && col != -1 && col != SZ) return;
					if ((row == -1 || row == SZ) && (col == -1 && col == SZ)) return;
					++shift_index;
					moveR.add(row);
					moveC.add(col);
					if (!apply_shift_returned()) return;
					findMaxConnect();
					draw(1);
					if (current_tile == '.' && isConnected()) ready = true;
				}
			});
			addMouseMotionListener(new MouseAdapter() {
				public void mouseMoved(MouseEvent e) {
					if (!manual) return;
					int r = e.getY() / side - 1;
					int c = e.getX() / side - 1;
					hoverR = -1;
					hoverC = -1;
					if (((r == -1 || r == SZ) && 0 <= c && c < SZ) || ((c == -1 || c == SZ) && 0 <= r && r < SZ)) {
						hoverR = r;
						hoverC = c;
					}
					draw(1);
				}
			});
			jf.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (!manual) return;
					if (e.getKeyCode() == KeyEvent.VK_Z && e.isMetaDown()) {
						undo();
					}
				}
			});
			jf.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
		}
	}

	public Visualizer() {
		if (vis) {
			jf = new JFrame();
			v = new Vis();
			jf.getContentPane().add(v);
		}
	}

	private static final int THREAD_COUNT = 4;

	public static void main(String[] args) throws Exception {
		long seed = 1;
		long begin = -1;
		long end = -1;
		vis = false;
		manual = false;
		debug = false;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-begin")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-end")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-manual")) manual = true;
			if (args[i].equals("-info")) debug = true;
			if (args[i].equals("-delay")) del = Integer.parseInt(args[++i]);
			if (args[i].equals("-side")) side = Integer.parseInt(args[++i]);
		}
		if (manual) vis = true;
		if (begin != -1 && end != -1) {
			vis = false;
			manual = false;
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Visualizer tester = new Visualizer();
			System.out.println(tester.runTest(seed));
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Visualizer f = new Visualizer();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		static final Result NULL_RESULT = new Result();
		long seed;
		int SZ;
		int whiteCount;
		double score;
		long elapsed;
		boolean isStripe;

		public String toString() {
			return "seed " + this.seed + "\nSZ " + this.SZ + "  WhiteCount " + this.whiteCount + "  WhiteRatio "
					+ (1.0 * this.whiteCount / SZ / SZ) + "\nstripe " + this.isStripe + "\nelapsed " + this.elapsed / 1000.0 + "\nscore "
					+ this.score;
		}

	}

}
