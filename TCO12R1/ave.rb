#!/usr/bin/ruby

sum = 0
sum_stripe = 0
count = 0
count_stripe = 0
stripe = false
seed = 0;
seed_begin = ARGV[1] ? ARGV[1].to_i : nil
seed_end = ARGV[2] ? ARGV[2].to_i : nil

IO.foreach(ARGV[0]){ |line|
  index = line.index("seed")
  if index == 0
    seed = line[index+5..-1].to_i
  end
  next if seed_begin && seed < seed_begin 
  break if seed_end && seed > seed_end 
  index = line.index('score')
  if index == 0
    score = line[6..-1].to_f
    sum += score
    count += 1
    if (stripe) 
      count_stripe += 1
      sum_stripe += score
    end
  end
  index = line.index('stripe') 
  if index == 0
    stripe = line[('stripe'.length + 1)..-1].chomp == 'true'
  end
}

count_nonstripe = count - count_stripe
puts "all:         #{sum.to_f / count} / #{count}"
puts "stripe:      #{sum_stripe.to_f / count_stripe} / #{count_stripe}"
puts "nonstripe:   #{(sum - sum_stripe).to_f / count_nonstripe} / #{count_nonstripe}"

