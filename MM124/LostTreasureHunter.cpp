#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
#ifndef NDEBUG
// #define MEASURE_TIME
// #define DEBUG
#endif
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 3.126448e9;
constexpr ll TL = 4000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 9000;
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
constexpr inline T sq(T v) { return v * v; }

void debug_vec(const vi& vec) {
  debugStr("[");
  for (int i = 0; i < vec.size(); ++i) {
    debug("%d ", vec[i]);
  }
  debugStr("]");
}

template<class T>
void erase_vec(vector<T>& vec, const T& value) {
  vec.erase(remove(vec.begin(), vec.end(), value), vec.end());
}

XorShift rnd;
Timer timer;
Counter counter;

constexpr double PI = 3.141592653589793238;
constexpr int INF = 1 << 28;

//////// end of template ////////

struct History {
  int order, next_path, treasure, got;
  vi match;
};

struct Node {
  int id, order, treasure;
  vi rev;
  vector<Node*> adj;
};

int N, C, V, P;
int gain, step;
int elapsed;
array<History, 1000> history;
array<Node, 1000> nodes_pool;
constexpr auto RES_FINISH = make_pair(-1, -1);
constexpr int GET_BACK = -1;
double threshold_stop;

#ifdef INFO
vvi truth_graph;
vi truth_tc;
vi truth_tc_orig;
vi truth_visit_count;
int truth_pos;
int truth_prev_pos;
#endif

struct StandardSolver {

  int seen_total, seen_count;
  vi adj_count, adj_order;
  vi plan;
  bool contradiction;
  vector<Node*> nodes;

  StandardSolver() : seen_total(0), seen_count(0), contradiction(false) {
    threshold_stop = min(15.0, 1.5 / (1.0 * C / V));
    for (int i = 0; i < 1000; ++i) {
      nodes_pool[i].id = i;
      nodes.push_back(&nodes_pool[i]);
    }
  }

  void collect_prev(int start, vi& prev) {
    if (history[start].match.size() != 1) return;
    for (int i : history[start].match) {
      if (find(prev.begin(), prev.end(), i) == prev.end()) {
        prev.push_back(i);
        collect_prev(i, prev);
      }
    }
  }

  vi find_match(int nt, int np, int max_step) {
    // debug("find_match %d %d\n", nt, np);
    vi already;
    Node* nn = nodes[max_step + 1];
    for (int i = 0; i < nn->order; ++i) {
      if (nn->adj[i]) {
        already.push_back(nn->adj[i]->id);
      }
    }
    vi ret;
    for (int i = 0; i <= max_step; ++i) {
      if (find(ret.begin(), ret.end(), nodes[i]->id) != ret.end()) continue;
      if (find(already.begin(), already.end(), nodes[i]->id) != already.end()) continue;
      // debug("%d %d %d\n", nodes[i]->id, nodes[i]->treasure, nodes[i]->order);
      if (nodes[i]->treasure == nt && nodes[i]->order == np) {
        ret.push_back(nodes[i]->id);
        assert(nodes[i] == nodes[nodes[i]->id]);
      }
    }
    return ret;
  }

  vi deduce(int s) {
    vi match_cand;
    const History& cur_hist = history[s];
    const History& prev_hist = history[s - 1];
    const int nt = cur_hist.treasure + cur_hist.got;
    int should_be;
    if (prev_hist.next_path == GET_BACK) {
      should_be = nodes[s - 2]->id;
    } else {
      should_be = nodes[s - 1]->adj[prev_hist.next_path] ? nodes[s - 1]->adj[prev_hist.next_path]->id : -1;      
    }
    debug("should_be:%d\n", should_be);
    if (should_be != -1 && nt == nodes[should_be]->treasure && cur_hist.order == nodes[should_be]->order) {
      nodes[s] = nodes[should_be];
    } else {
      if (should_be != -1) {
        if (prev_hist.next_path == GET_BACK || history[s - 2].next_path == GET_BACK) {
          contradiction = true;
          return match_cand;
        }
        debug("try fix: should be %d\n", should_be);
        vi prev_match = find_match(prev_hist.treasure + prev_hist.got, prev_hist.order, s - 3);
        debugStr("prev_match:");
        debug_vec(prev_match);
        debugln();
        for (int id : prev_match) {
          if (id == nodes[s - 1]->id) {
            continue;
          }
          if (id == 0 && history[s - 2].next_path != 0) {
            continue;
          }
          const Node* adj = nodes[id]->adj[prev_hist.next_path];
          if (adj && (adj->order != cur_hist.order || adj->treasure != nt)) {
            continue;
          }
          int adj_cnt = nodes[id]->rev.size();
          for (Node* an : nodes[id]->adj) {
            if (an) {
              adj_cnt++;
            }
          }
          if (adj_cnt == cur_hist.order) {
            continue;
          }
          if (adj_cnt > cur_hist.order) {
            debugStr("contradiction\n");
            contradiction = true;
            return match_cand;
          }
          match_cand.push_back(id);
        }
        debugStr("match_cand:");
        debug_vec(match_cand);
        nodes[s - 1]->treasure += prev_hist.got;
        erase_vec(nodes[s - 1]->rev, nodes[s - 2]->id);
        if (match_cand.size() >= 1) {
          vi match_cand_rev;
          for (int m : match_cand) {
            if (find(nodes[m]->rev.begin(), nodes[m]->rev.end(), nodes[s - 2]->id) != nodes[m]->rev.end()) {
              match_cand_rev.push_back(m);
            } else {
              for (Node* nn : nodes[m]->adj) {
                if (nn && nn->id == nodes[s - 2]->id) {
                  match_cand_rev.push_back(m);
                  break;
                }
              }
            }
          }
          int nid;
          debugStr(" match_cand_rev:");
          debug_vec(match_cand_rev);
          if (match_cand_rev.empty()) {
            nid = match_cand[0];
          } else {
            nid = match_cand_rev[0];
          }
          assert(nodes[nid]->treasure >= prev_hist.got);
          nodes[s - 1] = nodes[nid];
        } else {
          nodes[s - 1] = &nodes_pool[s - 1];
          seen_total += nt;
          seen_count++;
        }
        debugln();
        nodes[s - 1]->treasure -= prev_hist.got;
        nodes[s - 2]->adj[history[s - 2].next_path] = nodes[s - 1];
        erase_vec(nodes[s - 2]->rev, nodes[s - 1]->id);
        match_cand.clear();
      }

      vi match = find_match(nt, cur_hist.order, s - 2);
      for (int id : match) {
        if (id == nodes[s - 1]->id) {
          continue;
        }
        if (id == 0 && history[s - 1].next_path != 0) {
          continue;
        }
        int adj_cnt = nodes[id]->rev.size();
        debug_vec(nodes[id]->rev);
        debugln();
        debugStr("[");
        for (Node* an : nodes[id]->adj) {
          if (an) {
            adj_cnt++;
            debug("%d ", an->id);
          }
        }
        debugStr("]\n");
        if (adj_cnt == cur_hist.order) {
          continue;
        }
        if (adj_cnt > cur_hist.order) {
          debugStr("contradiction\n");
          contradiction = true;
          return match_cand;
        }
        match_cand.push_back(id);
      }
      debugStr("match_cand:");
      debug_vec(match_cand);
      if (match_cand.size() >= 1) {
        vi match_cand_rev;
        for (int m : match_cand) {
          if (find(nodes[m]->rev.begin(), nodes[m]->rev.end(), nodes[s - 1]->id) != nodes[m]->rev.end()) {
            match_cand_rev.push_back(m);
          } else {
            for (Node* nn : nodes[m]->adj) {
              if (nn && nn->id == nodes[s - 1]->id) {
                match_cand_rev.push_back(m);
                break;
              }
            }
          }
        }
        int nid;
        debugStr(" match_cand_rev:");
        debug_vec(match_cand_rev);
        if (match_cand_rev.empty()) {
          nid = match_cand[0];
        } else {
          nid = match_cand_rev[0];
        }
        assert(nodes[nid]->treasure >= nt);
        nodes[s] = nodes[nid];
      } else {
        seen_total += nt;
        seen_count++;
      }
    }
    debugln();
    nodes[s]->treasure -= cur_hist.got;
    if (prev_hist.next_path != GET_BACK) {
      nodes[s - 1]->adj[prev_hist.next_path] = nodes[s];
      erase_vec(nodes[s - 1]->rev, nodes[s]->id);
    }

    // rev
    if (nodes[s - 1]->id == 0) {
      if (nodes[s]->adj[0] && nodes[s]->adj[0]->id != 0) {
        debugStr("suspicious path to 0\n");
      }
      nodes[s]->adj[0] = nodes[0];
    } else {
      int vacant = -1;
      for (int i = 0; i < cur_hist.order; ++i) {
        debug("%d ", nodes[s]->adj[i] ? nodes[s]->adj[i]->id : -1);
        if (!nodes[s]->adj[i]) {
          vacant = vacant == -1 ? i : -2;
        } else if (nodes[s]->adj[i]->id == nodes[s - 1]->id) {
          vacant = -3;
          break;
        }
      }
      debugln();
      debug("%d\n", vacant);
      if (vacant == -2) { // multiple 
        if (find(nodes[s]->rev.begin(), nodes[s]->rev.end(), nodes[s - 1]->id) == nodes[s]->rev.end()) {
          nodes[s]->rev.push_back(nodes[s - 1]->id);
        }
      } else if (vacant == -3) { // found
        // do nothing
      } else { // single
        assert (vacant != -1);
        nodes[s]->adj[vacant] = nodes[s - 1];
      }
    }
    return match_cand;
  }

  pair<int, int> solve(int nt, int np) {
    if (step >= 1000) {
      return RES_FINISH;
    }
    static int zero_streak = 0;
    nodes_pool[step].order = np;
    nodes_pool[step].treasure = nt;
    nodes_pool[step].adj.assign(np, nullptr);
    int get = min(P, nt);
    History& cur_hist = history[step];
    cur_hist.order = np;
    cur_hist.got = get;
    cur_hist.treasure = nt - get;
    if (step == 0) {
      nodes[step]->treasure -= get;
      cur_hist.next_path = 0;
      return make_pair(get, 0);
    }
    int to = -2;
    if (P - 1 + rnd.nextUInt(11 - P) < history[step - 1].treasure) {
      zero_streak = 0;
      to = GET_BACK;
    }

    if (nt == 0) {
      zero_streak++;
    } else {
      zero_streak = 0;
    }

    vi match_cand;
    if (!contradiction) {
      match_cand = deduce(step);
      // if (to != GET_BACK) {
        debug("id:%d\n", nodes[step]->id);
        for (int i = 0; i <= step; ++i) {
          const Node* n = nodes[i];
          if (n->id == i) {
            debug("id=%3d t=%2d o=%2d adj:[", i, n->treasure, n->order);
            for (int j = 0; j < n->order; ++j) {
              if (n->adj[j] == nullptr) {
                debugStr(" -1 ");
              } else {
                debug("%3d ", n->adj[j]->id);
              }
            }
            for (int j = n->order; j < 12; ++j) {
              debugStr("    ");
            }
            debugStr("] rev:");
            debug_vec(n->rev);
            debugStr("\n");
          }
        }
      // }
    }
    if (step > 0 && history[step - 1].next_path == GET_BACK) {
      cur_hist.match.push_back(step - 2);
    } else {
      for (int i = step - 2; i >= 0; --i) {
        const History& ph = history[i];
        if (ph.order != np || ph.treasure != nt) continue;
        bool found = false;
        for (int j = i + 1; j < step; ++j) {
          const vi& am = history[j].match;
          if (am.size() == 1 && am[0] == i) {
            found = true;
            break;
          }
        }
        if (!found) {
          cur_hist.match.push_back(i);
        }
      }
    }
    if (contradiction) {
      if (cur_hist.match.empty()) {
        seen_total += nt;
        seen_count++;
      }
    }

    double exp_total = seen_total + 25 * (N - seen_count) * 1.4;
    if (zero_streak > 1 && gain > exp_total * 0.8 && zero_streak * pow(1.0 * gain / exp_total, 10.0) > threshold_stop) {
      return RES_FINISH;
    }

    if (to == -2) {
      if (!contradiction) {
        vi next_cand;
        for (int i = 0; i < np; ++i) {
          if (!nodes[step]->adj[i] || nodes[step]->adj[i]->treasure > P * 5) {
            next_cand.push_back(i);
          }
        }
        if (!next_cand.empty()) {
          to = next_cand[rnd.nextUInt(next_cand.size())];
        } else {
          to = 0;
          for (int i = 1; i < np; ++i) {
            if (nodes[step]->adj[to]->treasure < nodes[step]->adj[i]->treasure) {
              to = i;
            }
          }
        }
      } else {
        vi prev = cur_hist.match;
        if (cur_hist.match.size() == 1) {
          prev.clear();
          collect_prev(step, prev);
        }
        static vi used_count;
        used_count.assign(np, 0);
        for (int m : prev) {
          if (history[m].next_path != GET_BACK) {
            used_count[history[m].next_path]++;
          }
        }
        vi cand;
        for (int i = 0; i < np; ++i) {
          if (used_count[i] == 0) {
            cand.push_back(i);
          }
        }
        if (!cand.empty()) {
          to = cand[rnd.nextUInt(cand.size())];
          zero_streak = 0;
        }
        if (to == -2) {
          static vector<double> sum_prev;
          static vector<int> weight_prev;
          static vector<bool> avoid;
          sum_prev.assign(np, 0.0);
          weight_prev.assign(np, 0);
          avoid.assign(np, false);
          for (int m : cur_hist.match) {
            const History& before_hist = history[m];
            if (before_hist.next_path != GET_BACK) {
              sum_prev[before_hist.next_path] += history[m + 1].treasure * (m + 1);
              weight_prev[before_hist.next_path] += m + 1;
              if (history[m + 1].treasure == 0) {
                avoid[before_hist.next_path] = true;
              }
            }
          }
          for (int i = 0; i < np; ++i) {
            if (avoid[i] || weight_prev[i] == 0) {
              sum_prev[i] = 0;
            } else {
              sum_prev[i] /= weight_prev[i];
            }
          }
          double max = *max_element(sum_prev.begin(), sum_prev.end());
          for (int i = 0; i < np; ++i) {
            if (sum_prev[i] == max) {
              cand.push_back(i);
            }
          }
          to = cand[rnd.nextUInt(cand.size())];
        }
      }
    }
    cur_hist.next_path = to;
    debugStr("match:");
    debug_vec(cur_hist.match);
    debugln();
    return make_pair(get, to);
  }
};

void debug_step(pair<int, int> res) {
#ifdef INFO
#ifdef DEBUG
  int old_count = truth_tc[truth_pos];
#endif
  truth_tc[truth_pos] -= res.first;
  truth_visit_count[truth_pos]++;
  if (res.second == -1) {
    swap(truth_pos, truth_prev_pos);
  } else {
    truth_prev_pos = truth_pos;
    truth_pos = truth_graph[truth_pos][res.second];
  }
  debug(
    "    %2d->%2d %s collecting %2d (%2d -> %2d) %d %d\n",
    truth_prev_pos, truth_pos, res.second == -1 ? "(back)" : "",
    res.first, old_count, truth_tc[truth_prev_pos], res.second, gain * V - (step + 1) * C
  );
#endif
}

template<class Solver>
void solve() {
  Solver solver;
  for(step = 0; ; step++) {
    int nt, np;
    if (scanf("%d %d %d", &nt, &np, &elapsed) == EOF) {
      int exp_total = solver.seen_total + 25 * (N - solver.seen_count);
      debug("exp_total:%d seen_count:%d/%d\n", exp_total, solver.seen_count, N);
      break;
    }
    debug("\n=== step:%d === nt:%d np:%d\n", step, nt, np);
    start_time = get_time();
    auto res = solver.solve(nt, np);
    printf("%d\n", res.first);
    if (res.first != -1) {
      printf("%d\n", res.second);
      gain += res.first;
      debug_step(res);
    } else {
      debug("gain:%d step:%d score:%d-%d=%d\n", gain, step, gain * V, step * C, gain * V - step * C);
      vi got_hist(P + 1, 0);
      for (const History& h : history) {
        got_hist[h.got]++;
      }
      debugStr("got hist:[");
      for (int i = 0; i <= P; ++i) {
        debug("%3d ", got_hist[i]);
      }
      debugStr("]\n");
#ifdef INFO
      debugStr("visit count:\n");
      for (int i = 0; i < (N + 9) / 10; ++i) {
        for (int j = i * 10; j < min(N, i * 10 + 10); ++j) {
          debug("%2d(%2d->%2d) ", truth_visit_count[j], truth_tc_orig[j], truth_tc[j]);
        }
        debugln();
      }
      debugln();
#endif
    }
    fflush(stdout);
  }
}

int main() {
  scanf("%d %d %d %d", &V, &C, &N, &P);
  debug("N=%d V=%d C=%d P=%d\n", N, V, C, P);
#ifdef INFO
  truth_graph.resize(N);
  truth_tc.resize(N);
  truth_visit_count.resize(N);
  debugStr("ground_truth:\n");
  for (int i = 0; i < N; ++i) {
    scanf("%2d", &truth_tc[i]);
    debug("%2d %2d [", i, truth_tc[i]);
    int s;
    scanf("%d", &s);
    truth_graph[i].resize(s);
    for (int j = 0; j < s; ++j) {
      scanf("%d", &truth_graph[i][j]);
      debug("%2d ", truth_graph[i][j]);
    }
    debugStr("]\n");
  }
  truth_tc_orig = truth_tc;
  debug("total_tc:%d\n", accumulate(truth_tc.begin(), truth_tc.end(), 0));
  int ec = 0;
  for (int i = 0; i < N; ++i) {
    ec += truth_graph[i].size();
  }
  // debug("%d %d\n", N, ec / 2);
  // for (int i = 0; i < N; ++i) {
  //   for (int j : truth_graph[i]) {
  //     if (j < i) {
  //       debug("%d %d\n", i, j);
  //     }
  //   }
  // }
#endif
  if (P == 1 && V == C) {
    printf("-1\n");
    fflush(stdout);
    return 0;
  }
  solve<StandardSolver>();
}
