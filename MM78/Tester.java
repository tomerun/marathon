import java.security.SecureRandom;
import java.util.ArrayList;

public class Tester {
	//Constraints
	static final int MIN_DIAGRAM_SIZE = 15;
	static final int MAX_DIAGRAM_SIZE = 100;
	static final double MIN_DIAGRAM_FILL = 0.1;
	static final double MAX_DIAGRAM_FILL = 1.0;
	static final double[] MAX_WEIGHTS = { 1.0, 1.0, 1.0, 0.5 };

	static final String directions = "UDLR";
	static final int[] dirRow = { -1, 1, 0, 0 };
	static final int[] dirCol = { 0, 0, -1, 1 };

	int size;
	double fill;
	double[] weights;
	int[][] diagram = null;
	boolean[][] grid = null;

	public void generate(long seed) {
		while (true) {
			SecureRandom rnd = null;
			try {
				rnd = SecureRandom.getInstance("SHA1PRNG");
				rnd.setSeed(seed);
			} catch (Exception e) {
				e.printStackTrace();
			}
			int minSize = MIN_DIAGRAM_SIZE;
			int maxSize = MAX_DIAGRAM_SIZE;
			double minFill = MIN_DIAGRAM_FILL;
			double maxFill = MAX_DIAGRAM_FILL;
			if (1001 <= seed && seed <= 1860) {
				minSize = maxSize = MIN_DIAGRAM_SIZE + (int) (seed - 1001) / 10;
				minFill = 0.1 + (seed - 1) % 10 * 0.09;
				maxFill = minFill + 0.09;
			}
			fill = rnd.nextDouble() * (maxFill - minFill) + minFill;
			weights = new double[4];
			double totalWeight = 0;
			for (int i = 0; i < 4; i++) {
				weights[i] = rnd.nextDouble() * MAX_WEIGHTS[i];
				totalWeight += weights[i];
			}
			for (int i = 0; i < 4; i++)
				weights[i] /= totalWeight;
			size = rnd.nextInt(maxSize - minSize + 1) + minSize;
			diagram = new int[size][size];
			boolean emptyDiagram = true;
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					diagram[i][j] = -1;
					if (rnd.nextDouble() < fill) {
						emptyDiagram = false;
						double r = rnd.nextDouble();
						diagram[i][j] = 3;
						for (int k = 0; k < 4; k++) {
							r -= weights[k];
							if (r <= 0) {
								diagram[i][j] = k;
								break;
							}
						}
					}
				}
			}
			if (!emptyDiagram) break;
		}
	}

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		try {
			generate(seed);
			res.size = size;
			res.fill = fill;
			res.prob = weights;
			String[] diag = new String[size];
			for (int i = 0; i < size; ++i) {
				char[] buf = new char[size];
				for (int j = 0; j < size; ++j) {
					buf[j] = diagram[i][j] == -1 ? '-' : (char) ('0' + diagram[i][j]);
				}
				diag[i] = String.valueOf(buf);
			}
			long startTime = System.currentTimeMillis();
			String solution = new FixTheFence().findLoop(diag);
			res.elapsed = System.currentTimeMillis() - startTime;
			if (solution == null) {
				System.err.println("Unable to get any return value from your solution.");
				return res;
			}

			String[] sa = solution.split(" ");
			int row, col;
			try {
				if (sa.length != 3) throw new Exception();
				row = Integer.parseInt(sa[0]);
				col = Integer.parseInt(sa[1]);
			} catch (Exception e) {
				System.err
						.println("Your return value should be in \"Row Column LoopDescription\" format, where Row and Column are valid integers.");
				return res;
			}
			String loop = sa[2];

			if (loop.length() == 0) {
				System.err.println("Your loop description can not by empty.");
				return res;
			}
			row *= 2;
			col *= 2;
			int startRow = row;
			int startCol = col;
			if (row < 0 || row > size * 2 || col < 0 || col > size * 2) {
				System.err.println("Row and Column should be inside [0, DiagramSize] range.");
				return res;
			}

			grid = new boolean[2 * size + 1][2 * size + 1];
			for (int i = 0; i < loop.length(); i++) {
				int d = directions.indexOf(loop.charAt(i));
				if (d == -1) {
					System.err.println("Loop description can contain only \'U\', \'D\', \'L\' and \'R\' characters.");
					return res;
				}
				row += dirRow[d];
				col += dirCol[d];
				if (row < 0 || row > 2 * size || col < 0 || col > 2 * size) {
					System.err.println("Your loop goes outside of the diagram boundary.");
					return res;
				}
				if (grid[row][col]) {
					System.err.println("Your loop visits the same edge twice.");
					return res;
				}

				grid[row][col] = true;
				row += dirRow[d];
				col += dirCol[d];
				if (grid[row][col]) {
					System.err.println("Your loop visits the same lattice point twice.");
					return res;
				}

				grid[row][col] = true;
			}

			if (row != startRow || col != startCol) {
				System.err.println("Your loop should start and finish at the same lattice point.");
				return res;
			}

			int totalNumbers = 0;
			int correctNumbers = 0;
			for (int r = 0; r < size; r++)
				for (int c = 0; c < size; c++)
					if (diagram[r][c] != -1) {
						totalNumbers++;
						int totalEdges = 0;
						for (int d = 0; d < 4; d++)
							totalEdges += grid[r * 2 + 1 + dirRow[d]][c * 2 + 1 + dirCol[d]] ? 1 : 0;
						correctNumbers += (totalEdges == diagram[r][c] ? 1 : 0);
					}

			res.score = (double) correctNumbers / totalNumbers;
			return res;
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results: ");
			e.printStackTrace();
			return res;
		}

	}

	private static final int THREAD_COUNT = 4;

	public static void main(String[] args) throws Exception {
		long seed = 1;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-begin")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-end")) {
				end = Long.parseLong(args[++i]);
			}
		}
		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			System.out.println(tester.runTest(seed));
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int size;
		double fill;
		double[] prob;
		double score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d  size:%d\nfill:   %.4f  p:[%.4f %.4f %.4f %.4f]", seed, size, fill, prob[0], prob[1], prob[2],
					prob[3]);
			return ret + "\nelapsed:" + this.elapsed / 1000.0 + "\nscore:  " + this.score;
		}
	}
}
