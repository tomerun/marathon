import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.SecureRandom;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Vis {
	//Constraints
	static final int MIN_DIAGRAM_SIZE = 15;
	static final int MAX_DIAGRAM_SIZE = 100;
	static final double MIN_DIAGRAM_FILL = 0.1;
	static final double MAX_DIAGRAM_FILL = 1.0;
	static final double[] MAX_WEIGHTS = { 1.0, 1.0, 1.0, 0.5 };

	static final String directions = "UDLR";
	static final int[] dirRow = { -1, 1, 0, 0 };
	static final int[] dirCol = { 0, 0, -1, 1 };

	//testcase data
	int size;
	double fill;
	double[] weights;
	int[][] diagram = null;
	boolean[][] grid = null;
	boolean[][] correct = null;

	static int side = 10;
	static String exec = "java FixTheFence";
	static boolean vis = true;
	static Process proc = null;

	InputStream is;
	OutputStream os;
	BufferedReader br;
	JFrame jf;
	VisInternal v;

	public Vis(String seed) {
		if (vis) {
			jf = new JFrame();
			v = new VisInternal();
			jf.getContentPane().add(v);
		}

		if (exec != null) {
			try {
				Runtime rt = Runtime.getRuntime();
				proc = rt.exec(exec);
				os = proc.getOutputStream();
				is = proc.getInputStream();
				br = new BufferedReader(new InputStreamReader(is));
				new ErrorReader(proc.getErrorStream()).start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("Score = " + runTest(seed));
		if (proc != null) {
			try {
				proc.destroy();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void generate(String seedStr) {
		long seed = Long.parseLong(seedStr);
		while (true) {
			SecureRandom rnd = null;
			try {
				rnd = SecureRandom.getInstance("SHA1PRNG");
				rnd.setSeed(seed);
			} catch (Exception e) {
				e.printStackTrace();
			}
			int minSize = MIN_DIAGRAM_SIZE;
			int maxSize = MAX_DIAGRAM_SIZE;
			double minFill = MIN_DIAGRAM_FILL;
			double maxFill = MAX_DIAGRAM_FILL;
			if (1001 <= seed && seed <= 1860) {
				minSize = maxSize = MIN_DIAGRAM_SIZE + (int) (seed - 1001) / 10;
				minFill = 0.1 + (seed - 1) % 10 * 0.09;
				maxFill = minFill + 0.09;
			}
			fill = rnd.nextDouble() * (maxFill - minFill) + minFill;
			weights = new double[4];
			double totalWeight = 0;
			for (int i = 0; i < 4; i++) {
				weights[i] = rnd.nextDouble() * MAX_WEIGHTS[i];
				totalWeight += weights[i];
			}
			for (int i = 0; i < 4; i++)
				weights[i] /= totalWeight;
			size = rnd.nextInt(maxSize - minSize + 1) + minSize;
			diagram = new int[size][size];
			boolean emptyDiagram = true;
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					diagram[i][j] = -1;
					if (rnd.nextDouble() < fill) {
						emptyDiagram = false;
						double r = rnd.nextDouble();
						diagram[i][j] = 3;
						for (int k = 0; k < 4; k++) {
							r -= weights[k];
							if (r <= 0) {
								diagram[i][j] = k;
								break;
							}
						}
					}
				}
			}

			if (!emptyDiagram) break;
		}
	}

	private String callSolution() throws IOException {
		if (exec == null) return null;

		StringBuilder sb = new StringBuilder();
		sb.append(size).append('\n');
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++)
				sb.append(diagram[i][j] == -1 ? '-' : (char) ('0' + diagram[i][j]));
			sb.append("\n");
		}
		os.write(sb.toString().getBytes());
		os.flush();

		return br.readLine();
	}

	public double runTest(String seed) {
		try {
			generate(seed);

			if (exec == null) {
				System.out.println("Nothing to run. Please specify your solution with -exec option.");
				draw();
				return 0.0;
			}

			String solution = callSolution();

			if (vis) {
				jf.setSize(size * side + VisInternal.HMARGINS, size * side + VisInternal.VMARGINS);
				jf.setVisible(true);
			}
			if (solution == null) {
				addFatalError("Unable to get any return value from your solution.");
				return 0.0;
			}

			String[] sa = solution.split(" ");
			int row, col;
			try {
				if (sa.length != 3) throw new Exception();
				row = Integer.parseInt(sa[0]);
				col = Integer.parseInt(sa[1]);
			} catch (Exception e) {
				addFatalError("Your return value should be in \"Row Column LoopDescription\" format, where Row and Column are valid integers.");
				return 0.0;
			}
			String loop = sa[2];

			if (loop.length() == 0) {
				addFatalError("Your loop description can not by empty.");
				return 0;
			}

			row *= 2;
			col *= 2;

			int startRow = row;
			int startCol = col;
			if (row < 0 || row > size * 2 || col < 0 || col > size * 2) {
				addFatalError("Row and Column should be inside [0, DiagramSize] range.");
				return 0.0;
			}

			grid = new boolean[2 * size + 1][2 * size + 1];
			for (int i = 0; i < loop.length(); i++) {
				int d = directions.indexOf(loop.charAt(i));
				if (d == -1) {
					addFatalError("Loop description can contain only \'U\', \'D\', \'L\' and \'R\' characters.");
					return 0.0;
				}

				row += dirRow[d];
				col += dirCol[d];

				if (row < 0 || row > 2 * size || col < 0 || col > 2 * size) {
					addFatalError("Your loop goes outside of the diagram boundary.");
					return 0.0;
				}

				if (grid[row][col]) {
					addFatalError("Your loop visits the same edge twice.");
					return 0.0;
				}

				grid[row][col] = true;

				row += dirRow[d];
				col += dirCol[d];

				if (grid[row][col]) {
					addFatalError("Your loop visits the same lattice point twice.");
					return 0.0;
				}

				grid[row][col] = true;
			}

			if (row != startRow || col != startCol) {
				addFatalError("Your loop should start and finish at the same lattice point.");
				return 0.0;
			}

			int totalNumbers = 0;
			int correctNumbers = 0;

			correct = new boolean[size][size];
			for (int r = 0; r < size; r++)
				for (int c = 0; c < size; c++)
					if (diagram[r][c] != -1) {
						totalNumbers++;
						int totalEdges = 0;
						for (int d = 0; d < 4; d++)
							totalEdges += grid[r * 2 + 1 + dirRow[d]][c * 2 + 1 + dirCol[d]] ? 1 : 0;
						correct[r][c] = totalEdges == diagram[r][c];
						correctNumbers += correct[r][c] ? 1 : 0;
					}

			draw();
			return (double) correctNumbers / totalNumbers;

		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results: ");
			e.printStackTrace();
			return 0.0;
		}

	}

	public static void main(String[] args) {
		String seed = "1";
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = args[++i];
			} else if (args[i].equals("-exec")) {
				exec = args[++i];
			} else if (args[i].equals("-novis")) {
				vis = false;
			} else if (args[i].equals("-side")) {
				side = Integer.parseInt(args[++i]);
			}
		}

		new Vis(seed);
	}

	void addFatalError(String message) {
		System.out.println(message);
	}

	// Visualization stuff
	void draw() {
		if (v == null) return;
		v.repaint();
		try {
			//Thread.sleep(del);
		} catch (Exception e) {}
	}

	class VisInternal extends JPanel implements WindowListener {
		static final int MARGIN = 20;
		static final int HMARGINS = 2 * MARGIN + 15;
		static final int VMARGINS = 2 * MARGIN + 35;
		static final int LATTICE_POINT_SIZE = 4;
		final Color CORRECT_COLOR = new Color(0xC0C0FF);
		final Color INCORRECT_COLOR = new Color(0xFFC0C0);

		static final int MINIMUM_FONT_SIZE = 10;
		static final int MINIMUM_POINT_SIZE = 10;

		public void paint(Graphics gr) {
			BufferedImage bi = new BufferedImage(size * side + HMARGINS, size * side + VMARGINS, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();

			// background
			g2.setColor(new Color(0xFFFFFF));
			g2.fillRect(0, 0, size * side + HMARGINS, size * side + VMARGINS);

			if (grid != null) {
				// draw satisfied numbers
				for (int r = 0; r < size; r++)
					for (int c = 0; c < size; c++) {
						if (diagram[r][c] == -1) continue;
						g2.setColor(correct[r][c] ? CORRECT_COLOR : INCORRECT_COLOR);
						g2.fillRect(MARGIN + c * side, MARGIN + r * side, side, side);
					}

				// draw loop
				g2.setColor(new Color(0x000000));
				g2.setStroke(new BasicStroke(2));
				for (int r = 0; r <= size; r++)
					for (int c = 0; c <= size; c++) {
						if (c < size && grid[2 * r][2 * c + 1])
							g2.drawLine(MARGIN + c * side, MARGIN + r * side, MARGIN + c * side + side, MARGIN + r * side);
						if (r < size && grid[2 * r + 1][2 * c])
							g2.drawLine(MARGIN + c * side, MARGIN + r * side, MARGIN + c * side, MARGIN + r * side + side);
					}

			}

			// generate lattice points
			if (side >= MINIMUM_POINT_SIZE) {
				g2.setColor(new Color(0x000000));
				g2.setStroke(new BasicStroke(1));
				for (int i = 0; i <= size; i++)
					for (int j = 0; j <= size; j++)
						g2.fillRect(MARGIN + i * side - LATTICE_POINT_SIZE / 2, MARGIN + j * side - LATTICE_POINT_SIZE / 2,
								LATTICE_POINT_SIZE, LATTICE_POINT_SIZE);
			}

			// generate diagram hints
			if (side >= MINIMUM_FONT_SIZE) {
				g2.setColor(new Color(0x000000));
				g2.setFont(new Font("Monospaced", Font.BOLD, side));
				FontMetrics fm = g2.getFontMetrics(g2.getFont());
				int fontWidth = fm.charWidth('0');
				int fontHeight = fm.getHeight();
				for (int r = 0; r < size; r++)
					for (int c = 0; c < size; c++)
						if (diagram[r][c] != -1) {
							String digit = "" + (char) ('0' + diagram[r][c]);
							g2.drawString(digit, MARGIN + c * side + side / 2 - fontWidth / 2, MARGIN + r * side + side / 2 + side
									- fontHeight / 2);
						}
			}

			gr.drawImage(bi, 0, 0, size * side + HMARGINS, size * side + VMARGINS, null);
		}

		public VisInternal() {
			jf.addWindowListener(this);
		}

		public void windowClosing(WindowEvent e) {
			if (proc != null) try {
				proc.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.exit(0);
		}

		public void windowActivated(WindowEvent e) {}

		public void windowDeactivated(WindowEvent e) {}

		public void windowOpened(WindowEvent e) {}

		public void windowClosed(WindowEvent e) {}

		public void windowIconified(WindowEvent e) {}

		public void windowDeiconified(WindowEvent e) {}
	}

}

class ErrorReader extends Thread {
	InputStream error;

	public ErrorReader(InputStream is) {
		error = is;
	}

	public void run() {
		try {
			byte[] ch = new byte[50000];
			int read;
			while ((read = error.read(ch)) > 0) {
				String s = new String(ch, 0, read);
				System.out.print(s);
				System.out.flush();
			}
		} catch (Exception e) {}
	}
}
