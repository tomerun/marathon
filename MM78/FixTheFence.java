import java.util.Arrays;
import java.util.Scanner;

public class FixTheFence {

    private static final boolean DEBUG = 1 == 0;
    static final boolean MEASURE_TIME = false;
    private static final long TIMELIMIT = 3000;
    private static final int[] DR = { 1, 0, -1, 0 };
    private static final int[] DC = { 0, 1, 0, -1 };
    private static final int[] DCR = { 0, 0, -1, -1, 0 };
    private static final int[] DCC = { -1, 0, 0, -1, -1 };
    private static final char[] DIR_CHARS = { 'D', 'R', 'U', 'L' };
    private static final int EMPTY = 7;
    private static final int DIR_NOTUSED = -1;
    private static final int AF_BASE = 15;
    private static final int[] AF_0 = { 8, 4, 0, -12, 0, 0, 0, 0 };
    private static final int[] AF_1 = { -12, 0, 0, 0, 0, 0, 0, 0 };
    private static final int[] AF_2 = { 0, -12, 4, 8, 0, 0, 0, 0 };

    long startTime = System.currentTimeMillis();
    XorShift rand = new XorShift();
    int N, digitsCount;
    int[][] count, dir, bestDir, satisfied;
    int[][][][] affinity;
    int bestScore = 0;
    double heat;
    Timer timer = new Timer();
    int[] hist = new int[31];

    String findLoop(String[] diagram) {
        N = diagram.length;
        //        if (N < 19) {
        //            heat = 2;
        //        } else if (N < 25) {
        //            heat = 2;
        //        } else if (N < 60) {
        //            heat = 2;
        //        } else if (N < 80) {
        //            heat = 2;
        //        } else {
        //            heat = 2;
        //        }
        heat = 2;
        count = new int[N + 2][N + 2];
        satisfied = new int[N + 2][N + 2];
        dir = new int[N + 3][N + 3];
        bestDir = new int[N + 3][N + 3];
        affinity = new int[N + 3][N + 3][4][4];
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                char c = diagram[i].charAt(j);
                count[i + 1][j + 1] = c == '-' ? EMPTY : -(c - '0');
                if (c != '-') digitsCount++;
                for (int k = 0; k < 4; ++k) {
                    for (int l = 0; l < 4; ++l) {
                        affinity[i + 1][j + 1][k][l] = AF_BASE;
                    }
                }
            }
        }
        for (int i = 0; i < N + 2; ++i) {
            count[0][i] = count[N + 1][i] = count[i][0] = count[i][N + 1] = EMPTY;
            for (int k = 0; k < 4; ++k) {
                for (int l = 0; l < 4; ++l) {
                    affinity[i + 1][N + 1][k][l] = AF_BASE;
                    affinity[N + 1][i + 1][k][l] = AF_BASE;
                }
            }
        }
        for (int i = 1; i <= N + 1; ++i) {
            for (int j = 1; j <= N + 1; ++j) {
                for (int k = 0; k < 4; ++k) {
                    int flr, flc, frr, frc, blr, blc, brr, brc;
                    if (k == 0) {
                        flr = frr = i;
                        blr = brr = i - 1;
                        flc = blc = j;
                        frc = brc = j - 1;
                    } else if (k == 1) {
                        flr = blr = i - 1;
                        frr = brr = i;
                        flc = frc = j;
                        blc = brc = j - 1;
                    } else if (k == 2) {
                        flr = frr = i - 1;
                        blr = brr = i;
                        flc = blc = j - 1;
                        frc = brc = j;
                    } else {
                        flr = blr = i;
                        frr = brr = i - 1;
                        flc = frc = j - 1;
                        blc = brc = j;
                    }
                    affinity[i][j][k][0] += AF_1[Math.abs(count[flr][flc])];
                    affinity[i][j][k][0] += AF_1[Math.abs(count[frr][frc])];
                    if (affinity[i][j][k][0] <= 0) affinity[i][j][k][0] = 1;
                    affinity[i][j][k][1] += AF_0[Math.abs(count[frr][frc])];
                    affinity[i][j][k][1] += AF_1[Math.abs(count[flr][flc])];
                    affinity[i][j][k][1] += AF_2[Math.abs(count[blr][blc])];
                    if (affinity[i][j][k][1] <= 0) affinity[i][j][k][1] = 1;
                    affinity[i][j][k][3] += AF_0[Math.abs(count[flr][flc])];
                    affinity[i][j][k][3] += AF_1[Math.abs(count[frr][frc])];
                    affinity[i][j][k][3] += AF_2[Math.abs(count[brr][brc])];
                    if (affinity[i][j][k][3] <= 0) affinity[i][j][k][3] = 1;
                }
            }
        }
        for (int[] a : dir) {
            Arrays.fill(a, DIR_NOTUSED);
        }
        for (int[] a : bestDir) {
            Arrays.fill(a, DIR_NOTUSED);
        }
        for (int i = 1; i <= N + 1; ++i) {
            dir[0][i] = 1;
            dir[N + 2][i] = 3;
            dir[i][0] = 2;
            dir[i][N + 2] = 0;
        }
        log("count:" + digitsCount);
        solveDivideAndConquer();
        log("before improve:" + (System.currentTimeMillis() - startTime));
        improve();
        log(1.0 * bestScore / digitsCount);
        int ansStartR = 0;
        int ansStartC = 0;
        OUT: for (int i = 1; i <= N + 1; ++i) {
            for (int j = 1; j <= N + 1; ++j) {
                if (bestDir[i][j] != DIR_NOTUSED) {
                    ansStartR = i;
                    ansStartC = j;
                    break OUT;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        int r = ansStartR;
        int c = ansStartC;
        do {
            int d = bestDir[r][c];
            sb.append(DIR_CHARS[d]);
            r += DR[d];
            c += DC[d];
        } while (r != ansStartR || c != ansStartC);
        String ansPath = sb.toString();
        log(hist);
        log(timer.sum);
        return String.format("%d %d %s", ansStartR - 1, ansStartC - 1, ansPath);
    }

    void improve() {
        int len = 0;
        for (int i = 1; i <= N; ++i) {
            for (int j = 1; j <= N; ++j) {
                bestDir[i][j] = dir[i][j];
                if (dir[i][j] != DIR_NOTUSED) ++len;
                if (count[i][j] == 0) ++bestScore;
            }
        }
        for (int i = 1; i <= N + 1; ++i) {
            bestDir[N + 1][i] = dir[N + 1][i];
            if (dir[N + 1][i] != DIR_NOTUSED) ++len;
            bestDir[i][N + 1] = dir[i][N + 1];
            if (dir[i][N + 1] != DIR_NOTUSED) ++len;
        }
        if (dir[N + 1][N + 1] != DIR_NOTUSED) --len;
        int r = 0;
        int c = 0;
        OUT: for (int i = 1; i <= N + 1; ++i) {
            for (int j = 1; j <= N + 1; ++j) {
                if (dir[i][j] != DIR_NOTUSED) {
                    r = i;
                    c = j;
                    break OUT;
                }
            }
        }

        int score = bestScore;
        int[] dBuf = new int[128];
        //        int moveMax = (int) Math.pow(N, 0.7);
        int moveMax = 10;
        long time = System.currentTimeMillis() - startTime;
        long HEATCYCLE = (TIMELIMIT - time) / 10;
        long HEATTIME = time + HEATCYCLE;

        for (int turn = 0;; ++turn) {
            if ((turn & 4095) == 4095) {
                long elapsed = System.currentTimeMillis() - startTime;
                if (elapsed > TIMELIMIT) {
                    log("improve turn:" + turn);
                    break;
                }
                if (elapsed > HEATTIME) {
                    heat *= 1.1;
                    //                    log("heat:" + heat);
                    HEATTIME += HEATCYCLE;
                }
            }
            int pd = dir[r][c];
            r += DR[pd];
            c += DC[pd];
            int sr = r;
            int sc = c;
            int masR = sr;
            int misR = sr;
            int masC = sc;
            int misC = sc;
            timer.start(0);
            int move = rand.nextInt(moveMax) + 2;
            if (move >= len) move = len - 3;
            for (int i = 0; i < move; ++i) {
                int d = dir[r][c];
                dir[r][c] = DIR_NOTUSED;
                dBuf[i] = d;
                count[r + DCR[d]][c + DCC[d]]--;
                count[r + DCR[d + 1]][c + DCC[d + 1]]--;
                r += DR[d];
                c += DC[d];
                if (r > masR) {
                    masR = r;
                } else if (r < misR) {
                    misR = r;
                } else if (c > masC) {
                    masC = c;
                } else if (c < misC) {
                    misC = c;
                }
            }
            int er = r;
            int ec = c;
            int ed = dir[er][ec];
            dir[er][ec] = DIR_NOTUSED;
            timer.end(0);
            // make random walk path
            boolean success = false;
            timer.start(1);
            OUT: for (int challenge = 0; challenge < 10; ++challenge) {
                r = sr;
                c = sc;
                int d = pd;
                while (r != er || c != ec) {
                    final int goDir = dir[r + DR[d]][c + DC[d]];
                    final int turnL = (d + 1) & 3;
                    final int turnR = (d + 3) & 3;
                    if (goDir == DIR_NOTUSED) {
                        int rot = selectRot(r, c, d, dir[r + DR[turnL]][c + DC[turnL]] == DIR_NOTUSED, dir[r + DR[turnR]][c
                                + DC[turnR]] == DIR_NOTUSED);
                        d = (d + rot) & 3;
                    } else {
                        if (goDir == turnL) { // inner, turn right
                            d = turnR;
                        } else if (goDir == turnR) { // outer, turn left
                            d = turnL;
                        } else if (goDir == d) {
                            if (dir[r + DR[d] + DR[turnL]][c + DC[d] + DC[turnL]] == turnR) {
                                d = turnL;
                            } else {
                                d = turnR;
                            }
                        }
                        if (dir[r + DR[d]][c + DC[d]] != DIR_NOTUSED) {
                            resetRange2(sr, sc, r, c);
                            continue OUT;
                        }
                    }
                    dir[r][c] = d;
                    r += DR[d];
                    c += DC[d];
                }
                success = true;
                break;
            }
            timer.end(1);
            dir[er][ec] = ed;
            if (!success) {
                revertRange(sr, sc, move, dBuf);
            } else {
                timer.start(2);
                int maR = masR;
                int miR = misR;
                int maC = masC;
                int miC = misC;
                r = sr;
                c = sc;
                int newLen = 0;
                while (r != er || c != ec) {
                    int d = dir[r][c];
                    count[r + DCR[d]][c + DCC[d]]++;
                    count[r + DCR[d + 1]][c + DCC[d + 1]]++;
                    r += DR[d];
                    c += DC[d];
                    if (r > maR) {
                        maR = r;
                    } else if (r < miR) {
                        miR = r;
                    } else if (c > maC) {
                        maC = c;
                    } else if (c < miC) {
                        miC = c;
                    }
                    ++newLen;
                }

                int satisfyCount = 0;
                int currentCount = 0;
                miR = Math.max(1, miR - 1);
                miC = Math.max(1, miC - 1);
                maR = Math.min(N, maR);
                maC = Math.min(N, maC);
                for (int i = miR; i <= maR; ++i) {
                    for (int j = miC; j <= maC; ++j) {
                        currentCount += satisfied[i][j];
                        if (count[i][j] == 0) {
                            ++satisfyCount;
                        }
                    }
                }
                timer.end(2);
                if (transit(satisfyCount, currentCount)) {
                    score += satisfyCount - currentCount;
                    if (score > bestScore) {
                        timer.start(3);
//                        log("improve:" + score);
                        bestScore = score;
                        for (int i = 1; i <= N + 1; ++i) {
                            for (int j = 1; j <= N + 1; ++j) {
                                bestDir[i][j] = dir[i][j];
                            }
                        }
                        timer.end(3);
                    }
                    timer.start(4);
                    len += newLen - move;
                    for (int i = miR; i <= maR; ++i) {
                        for (int j = miC; j <= maC; ++j) {
                            satisfied[i][j] = count[i][j] == 0 ? 1 : 0;
                        }
                    }
                    timer.end(4);
                } else {
                    timer.start(5);
                    resetRange(sr, sc, er, ec);
                    revertRange(sr, sc, move, dBuf);
                    timer.end(5);
                }
            }
            r = sr;
            c = sc;
        }
    }

    int[][] rotDir = { { 0 }, { 0, 1 }, { 0, 3 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, }, };

    int selectRot(int v) {
        if (v == 0) return 0;
        return rotDir[v][rand.nextInt(rotDir[v].length)];
    }

    int selectRot(int i, int j, int pd, boolean turnL, boolean turnR) {
        if (!turnL && !turnR) return 0;
        int af0 = affinity[i][j][pd][0];
        int af1 = turnL ? affinity[i][j][pd][1] : 0;
        int af3 = turnR ? affinity[i][j][pd][3] : 0;
        int v = rand.nextInt(af0 + af1 + af3);
        if (v < af0) return 0;
        if (v < af0 + af1) return 1;
        return 3;
    }

    boolean transit(int newScore, int oldScore) {
        double diff = newScore - oldScore;// + (oldLen - newLen) / 3.0 / N;
        if (diff >= 0) return true;
        return rand.nextDouble() < Math.exp(diff * heat);
    }

    void resetRange(int sr, int sc, int er, int ec) {
        int r = sr;
        int c = sc;
        while (r != er || c != ec) {
            int d = dir[r][c];
            dir[r][c] = DIR_NOTUSED;
            count[r + DCR[d]][c + DCC[d]]--;
            count[r + DCR[d + 1]][c + DCC[d + 1]]--;
            r += DR[d];
            c += DC[d];
        }
    }

    void resetRange2(int sr, int sc, int er, int ec) {
        int r = sr;
        int c = sc;
        while (r != er || c != ec) {
            int d = dir[r][c];
            dir[r][c] = DIR_NOTUSED;
            r += DR[d];
            c += DC[d];
        }
    }

    void revertRange(int sr, int sc, int len, int[] dirBuf) {
        int r = sr;
        int c = sc;
        for (int i = 0; i < len; ++i) {
            int d = dirBuf[i];
            dir[r][c] = d;
            count[r + DCR[d]][c + DCC[d]]++;
            count[r + DCR[d + 1]][c + DCC[d + 1]]++;
            r += DR[d];
            c += DC[d];
        }
    }

    void solveDivideAndConquer() {
        final int deg = N < 31 ? 2 : N < 63 ? 3 : 4;
        final int bCount = 1 << deg;
        int[] rp = new int[bCount + 1];
        int[] cp = new int[bCount + 1];
        for (int i = 0; i <= bCount; ++i) {
            rp[i] = cp[i] = (N + 1) * i / bCount + 1;
        }
        HilbertPath hPath = new HilbertPath(deg);
        int rb = 0;
        int cb = 0;
        int r = (rp[0] + rp[1]) / 2;
        int c = (cp[0] + cp[1]) / 2;
        for (int i = 0; i < bCount * bCount; ++i) {
            int bd = hPath.dir[rb][cb];
            if (bd == 0) {
                int er = (rp[rb + 1] + rp[rb + 2]) / 2;
                while (r < er) {
                    dir[r][c] = 0;
                    count[r][c]++;
                    count[r][c - 1]++;
                    ++r;
                }
                ++rb;
            } else if (bd == 1) {
                int ec = (cp[cb + 1] + cp[cb + 2]) / 2;
                while (c < ec) {
                    dir[r][c] = 1;
                    count[r][c]++;
                    count[r - 1][c]++;
                    ++c;
                }
                ++cb;
            } else if (bd == 2) {
                int er = (rp[rb - 1] + rp[rb]) / 2;
                while (r > er) {
                    dir[r][c] = 2;
                    count[r - 1][c]++;
                    count[r - 1][c - 1]++;
                    --r;
                }
                --rb;
            } else {
                int ec = (cp[cb - 1] + cp[cb]) / 2;
                while (c > ec) {
                    dir[r][c] = 3;
                    count[r - 1][c - 1]++;
                    count[r][c - 1]++;
                    --c;
                }
                --cb;
            }
        }
        for (int ord = 0; ord < deg; ++ord) {
            int sr = (rp[0] + rp[1]) / 2;
            int sc = cp[1 << ord] - 1;
            rb = 0;
            cb = (1 << ord) - 1;
            for (int i = 0; i < bCount * bCount / (1 << (ord * 2)); ++i) {
                int nrb = rb;
                int ncb = cb;
                int maxRb = rb;
                int minRb = rb;
                int maxCb = cb;
                int minCb = cb;
                for (int j = 1; j < (1 << (ord * 2)); ++j) {
                    int nbd = hPath.dir[nrb][ncb];
                    nrb += DR[nbd];
                    ncb += DC[nbd];
                    maxRb = Math.max(maxRb, nrb);
                    minRb = Math.min(minRb, nrb);
                    maxCb = Math.max(maxCb, ncb);
                    minCb = Math.min(minCb, ncb);
                }
                int bd = hPath.dir[nrb][ncb];
                int er, ec;
                if (bd == 0) {
                    er = rp[nrb + 1] - 1;
                    ec = (cp[ncb] + cp[ncb + 1]) / 2;
                } else if (bd == 1) {
                    er = (rp[nrb] + rp[nrb + 1]) / 2;
                    ec = rp[ncb + 1] - 1;
                } else if (bd == 2) {
                    er = rp[nrb];
                    ec = (cp[ncb] + cp[ncb + 1]) / 2;
                } else {
                    er = (rp[nrb] + rp[nrb + 1]) / 2;
                    ec = rp[ncb];
                }
                improveBlock(sr, sc, er, ec, rp[minRb], cp[minCb], rp[maxRb + 1] - 1, cp[maxCb + 1] - 1, 100 * (1 << (ord * 2)));
                sr = er + DR[bd];
                sc = ec + DC[bd];
                rb = nrb + DR[bd];
                cb = ncb + DC[bd];
            }
        }
    }

    int[] surBuf = new int[500];
    int[][] bestCount = new int[102][102];

    void improveBlock(int startR, int startC, int endR, int endC, int top, int left, int bottom, int right, int turnC) {
        //        log(startR, startC, endR, endC, top, left, bottom, right);
        for (int i = top; i <= bottom; ++i) {
            for (int j = left; j <= right; ++j) {
                bestDir[i][j] = dir[i][j];
            }
        }
        for (int i = top - 1; i <= bottom; ++i) {
            for (int j = left - 1; j <= right; ++j) {
                bestCount[i][j] = count[i][j];
            }
        }

        int outd = 0;
        if (endR == bottom) {
            outd = 0;
        } else if (endR == top) {
            outd = 2;
        } else if (endC == right) {
            outd = 1;
        } else {
            outd = 3;
        }
        {
            int bufI = 0;
            int surr = endR + DR[outd];
            int surc = endC + DC[outd];
            int surd = (outd + 1) & 3;
            while (true) {
                surr += DR[surd];
                surc += DC[surd];
                if (Math.abs(surr - startR) + Math.abs(surc - startC) == 1) break;
                if ((surr < top || bottom < surr) && (surc < left || right < surc)) {
                    surd = (surd + 1) & 3;
                } else {
                    surBuf[bufI++] = dir[surr][surc];
                    dir[surr][surc] = surd;
                }
            }
            surr = endR + DR[outd];
            surc = endC + DC[outd];
            surd = (outd + 3) & 3;
            while (true) {
                surr += DR[surd];
                surc += DC[surd];
                if (Math.abs(surr - startR) + Math.abs(surc - startC) == 1) break;
                if ((surr < top || bottom < surr) && (surc < left || right < surc)) {
                    surd = (surd + 3) & 3;
                } else {
                    surBuf[bufI++] = dir[surr][surc];
                    dir[surr][surc] = surd;
                }
            }
        }

        int score = 0;
        int best = score;
        int[] dBuf = new int[128];
        //        int moveMax = (int) Math.pow(Math.max(bottom - top, right - left) + 1, 0.8);
        int moveMax = 8;
        int r = startR;
        int c = startC;
        for (int turn = 0; turn < turnC; ++turn) {
            int pd = dir[r][c];
            if (r == endR && c == endC) {
                r = startR;
                c = startC;
            }
            int sr = r;
            int sc = c;
            int move = rand.nextInt(moveMax) + 2;
            for (int i = 0; i < move; ++i) {
                int d = dir[r][c];
                dir[r][c] = DIR_NOTUSED;
                dBuf[i] = d;
                count[r + DCR[d]][c + DCC[d]]--;
                count[r + DCR[d + 1]][c + DCC[d + 1]]--;
                r += DR[d];
                c += DC[d];
                if (r == endR && c == endC) {
                    move = i + 1;
                    break;
                }
            }
            int er = r;
            int ec = c;
            int ed = dir[er][ec];
            dir[er][ec] = DIR_NOTUSED;

            // make random walk path
            boolean success = false;
            OUT: for (int challenge = 0; challenge < 10; ++challenge) {
                r = sr;
                c = sc;
                int d = pd;
                while (r != er || c != ec) {
                    final int goDir = dir[r + DR[d]][c + DC[d]];
                    final int turnL = (d + 1) & 3;
                    final int turnR = (d + 3) & 3;
                    if (goDir == DIR_NOTUSED) {
                        int rot = selectRot((dir[r + DR[turnL]][c + DC[turnL]] >>> 31)
                                | ((dir[r + DR[turnR]][c + DC[turnR]] >>> 30) & 2));
                        d = (d + rot) & 3;
                    } else {
                        if (goDir == turnL) { // inner, turn right
                            d = turnR;
                        } else if (goDir == turnR) { // outer, turn left
                            d = turnL;
                        } else if (goDir == d) {
                            if (dir[r + DR[d] + DR[turnL]][c + DC[d] + DC[turnL]] == turnR) {
                                d = turnL;
                            } else {
                                d = turnR;
                            }
                        }
                        if (dir[r + DR[d]][c + DC[d]] != DIR_NOTUSED) {
                            resetRange(sr, sc, r, c);
                            continue OUT;
                        }
                    }
                    dir[r][c] = d;
                    count[r + DCR[d]][c + DCC[d]]++;
                    count[r + DCR[d + 1]][c + DCC[d + 1]]++;
                    r += DR[d];
                    c += DC[d];
                }
                success = true;
                break;
            }
            dir[er][ec] = ed;
            if (!success) {
                revertRange(sr, sc, move, dBuf);
            } else {
                int satisfyCount = 0;
                int currentCount = 0;
                for (int i = top - 1; i <= bottom; ++i) {
                    for (int j = left - 1; j <= right; ++j) {
                        currentCount += satisfied[i][j];
                        if (count[i][j] == 0) {
                            ++satisfyCount;
                        }
                    }
                }
                //                if (satisfyCount > currentCount) {
                if (transit(satisfyCount, currentCount)) {
                    score += satisfyCount - currentCount;
                    for (int i = top - 1; i <= bottom; ++i) {
                        for (int j = left - 1; j <= right; ++j) {
                            satisfied[i][j] = count[i][j] == 0 ? 1 : 0;
                        }
                    }
                    if (score > best) {
                        best = score;
                        for (int i = top; i <= bottom; ++i) {
                            for (int j = left; j <= right; ++j) {
                                bestDir[i][j] = dir[i][j];
                                bestCount[i][j] = count[i][j];
                            }
                        }
                        for (int i = top - 1; i <= bottom; ++i) {
                            bestCount[i][left - 1] = count[i][left - 1];
                        }
                        for (int i = left - 1; i <= right; ++i) {
                            bestCount[top - 1][i] = count[top - 1][i];
                        }
                    }
                } else {
                    resetRange(sr, sc, er, ec);
                    revertRange(sr, sc, move, dBuf);
                }
            }
            r = er;
            c = ec;
        }
        for (int i = top; i <= bottom; ++i) {
            for (int j = left; j <= right; ++j) {
                dir[i][j] = bestDir[i][j];
            }
        }
        for (int i = top - 1; i <= bottom; ++i) {
            for (int j = left - 1; j <= right; ++j) {
                count[i][j] = bestCount[i][j];
                satisfied[i][j] = count[i][j] == 0 ? 1 : 0;
            }
        }

        {
            int bufI = 0;
            int surr = endR + DR[outd];
            int surc = endC + DC[outd];
            int surd = (outd + 1) & 3;
            while (true) {
                surr += DR[surd];
                surc += DC[surd];
                if (Math.abs(surr - startR) + Math.abs(surc - startC) == 1) break;
                if ((surr < top || bottom < surr) && (surc < left || right < surc)) {
                    surd = (surd + 1) & 3;
                } else {
                    dir[surr][surc] = surBuf[bufI++];
                }
            }
            surr = endR + DR[outd];
            surc = endC + DC[outd];
            surd = (outd + 3) & 3;
            while (true) {
                surr += DR[surd];
                surc += DC[surd];
                if (Math.abs(surr - startR) + Math.abs(surc - startC) == 1) break;
                if ((surr < top || bottom < surr) && (surc < left || right < surc)) {
                    surd = (surd + 3) & 3;
                } else {
                    dir[surr][surc] = surBuf[bufI++];
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            int N = sc.nextInt();
            String[] diagram = new String[N];
            for (int i = 0; i < N; ++i) {
                diagram[i] = sc.next();
            }
            String ret = new FixTheFence().findLoop(diagram);
            System.out.println(ret);
            System.out.flush();
        } finally {
            sc.close();
        }
    }

    static void log(String str) {
        if (DEBUG) System.err.println(str);
    }

    static void log(Object... obj) {
        if (DEBUG) System.err.println(Arrays.deepToString(obj));
    }
}

class HilbertPath {
    int r, c;
    int[][] dir;

    HilbertPath(int deg) {
        int size = 1 << deg;
        dir = new int[size][size];
        for (int[] a : dir) {
            Arrays.fill(a, 9);
        }
        r = 0;
        c = size / 2 - 1;
        ldr(deg - 2);
        dir[r][c] = 0;
        for (int i = 0; i < size / 2; ++i) {
            for (int j = 0; j < size / 2; ++j) {
                dir[i + size / 2][j] = dir[i][j];
            }
        }
        dir[size - 1][size / 2 - 1] = 1;
        for (int i = 0; i < size; ++i) {
            for (int j = size / 2; j < size; ++j) {
                dir[i][j] = (dir[size - 1 - i][size - 1 - j] + 2) & 3;
            }
        }
    }

    void ldr(int deg) {
        if (deg < 0) return;
        dlu(deg - 1);
        dir[r][c--] = 3;
        ldr(deg - 1);
        dir[r++][c] = 0;
        ldr(deg - 1);
        dir[r][c++] = 1;
        urd(deg - 1);
    }

    void dlu(int deg) {
        if (deg < 0) return;
        ldr(deg - 1);
        dir[r++][c] = 0;
        dlu(deg - 1);
        dir[r][c--] = 3;
        dlu(deg - 1);
        dir[r--][c] = 2;
        rul(deg - 1);
    }

    void rul(int deg) {
        if (deg < 0) return;
        urd(deg - 1);
        dir[r][c++] = 1;
        rul(deg - 1);
        dir[r--][c] = 2;
        rul(deg - 1);
        dir[r][c--] = 3;
        dlu(deg - 1);
    }

    void urd(int deg) {
        if (deg < 0) return;
        rul(deg - 1);
        dir[r--][c] = 2;
        urd(deg - 1);
        dir[r][c++] = 1;
        urd(deg - 1);
        dir[r++][c] = 0;
        ldr(deg - 1);
    }
}

final class Timer {
    long[] sum = new long[9];
    long[] start = new long[sum.length];

    void start(int i) {
        if (FixTheFence.MEASURE_TIME) {
            start[i] = System.currentTimeMillis();
        }
    }

    void end(int i) {
        if (FixTheFence.MEASURE_TIME) {
            sum[i] += System.currentTimeMillis() - start[i];
        }
    }
}

final class XorShift {
    int x = 123456789;
    int y = 362436069;
    int z = 521288629;
    int w = 88675123;

    int nextInt(int n) {
        final int t = x ^ (x << 11);
        x = y;
        y = z;
        z = w;
        w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
        final int r = w % n;
        return r < 0 ? r + n : r;
    }

    double nextDouble() {
        final int t = x ^ (x << 11);
        x = y;
        y = z;
        z = w;
        w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
        return Math.abs(1.0 * w / (1L << 31));
    }

}