import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class GatheringResources {

	static final int[] DR = { -1, 0, 1, 0 };
	static final int[] DC = { 0, 1, 0, -1 };
	static final boolean DEBUG = false;
	static final boolean MEASURE_TIME = false;
	static final long TL = 3000;
	Timer timer = new Timer();
	double searchP;
	long starttime = System.currentTimeMillis();
	long maxtime;
	Random rnd = new Random(42);
	int[] baseCost, workerCost;
	int N;
	int[] bestAns;
	State origSt, st;
	int[][] searchStage, searchDir;
	int searchIdx;
	int maxWorker, maxBase;
	int maxResource;
	int bestScore, bestWorkers, bestBases, bestProb;
	int[] searchList;
	int searchPos;
	boolean extraAdjust = false;
	int createBaseProb = 1;

	int[] bestStrategy(int N, int[] board, int[] baseCost, int[] workerCost, int T) {
		this.N = N;
		this.baseCost = baseCost;
		this.workerCost = workerCost;
		this.origSt = new State();
		this.searchStage = new int[N][N];
		this.searchDir = new int[N][N];
		this.searchList = new int[N * N];
		origSt.turn = origSt.prevCreateTurn = T;
		origSt.board = new int[N][N];
		origSt.reserve = new boolean[N][N];
		double countRes = 0;
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				int r = board[i * N + j];
				if (r == -1) {
					origSt.bases.add(new Base(i, j));
					origSt.board[i][j] = -1;
				} else if (r == -2) {
					origSt.workers.add(new Worker(i, j));
				} else if (r > 0) {
					origSt.totalRes += r;
					origSt.board[i][j] = r;
					++countRes;
					maxResource = Math.max(maxResource, r);
				}
			}
		}
		countRes /= N * N;
		searchP = (countRes - 0.25) * 4 + 1.2;
		origSt.remainRes = origSt.totalRes;

		int baseCostSum = 0;
		for (maxBase = 0; maxBase < baseCost.length; ++maxBase) {
			baseCostSum += baseCost[maxBase];
			if (baseCostSum * (7 + 0.5 / (countRes - 0.2)) > origSt.totalRes) {
				break;
			}
		}
		maxBase++;
		int[] scores = new int[workerCost.length + 2];
		int initialWorkersUpper = 12;
		int left = 1;
		{
			maxWorker = left;
			st = new State(origSt);
			solve();
			scores[maxWorker] = st.pool;
		}
		int right = Math.min(initialWorkersUpper, workerCost.length + 1);
		{
			maxWorker = right;
			st = new State(origSt);
			solve();
			scores[maxWorker] = st.pool;
		}

		while (right - left > 2) {
			if (System.currentTimeMillis() - starttime > TL - maxtime * 4 / 3) {
				break;
			}
			int ml = left + (right - left) / 3;
			int mr = left + (right - left) * 2 / 3;
			if (scores[ml] == 0) {
				maxWorker = ml;
				st = new State(origSt);
				solve();
				scores[maxWorker] = st.pool;
			}
			if (mr != ml && scores[mr] == 0) {
				maxWorker = mr;
				st = new State(origSt);
				solve();
				scores[maxWorker] = st.pool;
			}
			if (scores[ml] > scores[mr] || scores[ml] == scores[mr] && scores[left] > scores[right]) {
				right = mr;
			} else {
				left = ml;
			}
		}

		if (bestWorkers == initialWorkersUpper) {
			for (maxWorker = initialWorkersUpper + 1; maxWorker <= workerCost.length + 1; ++maxWorker) {
				if (System.currentTimeMillis() - starttime > TL - maxtime * 4 / 3) {
					break;
				}
				st = new State(origSt);
				solve();
				if (st.pool < bestScore) break;
			}
		}

		maxWorker = bestWorkers;
		int maxBaseOrig = maxBase;
		for (--maxBase; maxBase >= 1; --maxBase) {
			if (System.currentTimeMillis() - starttime > TL - maxtime * 4 / 3) {
				break;
			}
			st = new State(origSt);
			solve();
			if (st.pool < bestScore) break;
		}
		for (maxBase = maxBaseOrig + 1; maxBase <= baseCost.length + 1; ++maxBase) {
			if (System.currentTimeMillis() - starttime > TL - maxtime * 4 / 3) {
				break;
			}
			st = new State(origSt);
			solve();
			if (st.pool < bestScore) break;
		}

		extraAdjust = true;
		while (true) {
			if (System.currentTimeMillis() - starttime > TL - maxtime * 4 / 3) {
				break;
			}
			maxBase = bestBases + (int) (rnd.nextGaussian() * 2);
			if (maxBase <= 0) maxBase = 1;
			if (maxBase > baseCost.length + 1) maxBase = baseCost.length + 1;
			maxWorker = bestWorkers + (int) (rnd.nextGaussian() * 2);
			if (maxWorker <= 0) maxWorker = 1;
			if (maxWorker > workerCost.length + 1) maxWorker = workerCost.length + 1;
			createBaseProb = rnd.nextInt(100) + 1;
			st = new State(origSt);
			solve();
		}
		debug(bestProb);

		timer.print();
		return bestAns;
	}

	void solve() {
		long solveStartTime = System.currentTimeMillis();
		while (st.turn > 0) {
			st.judgeWantBase();
			int prevIdx = st.moves.size();
			int prevWorkerCount = st.workers.size();
			int prevBaseCount = st.bases.size();
			int baseCreator = -1;
			for (int i = 0; i < st.workers.size(); ++i) {
				Worker worker = st.workers.get(i);
				int d = worker.step();
				if (d < 0) {
					baseCreator = i;
				}
				st.moves.add(d);
			}
			int createWorkerIndex = st.createWorkerIndex();
			if (createWorkerIndex != -1) {
				st.bases.get(createWorkerIndex).willCreateWorker = true;
			}
			for (Base base : st.bases) {
				st.moves.add(base.step());
			}
			if (baseCreator != -1) {
				Worker creator = st.workers.get(baseCreator);
				int bd = st.moves.get(prevIdx + baseCreator);
				bd = -bd - 1;
				int baseR = creator.r + DR[bd];
				int baseC = creator.c + DC[bd];
				boolean ok = true;
				for (int i = 0; i < st.workers.size(); ++i) {
					if (i == baseCreator) continue;
					Worker worker = st.workers.get(i);
					int pr = worker.r;
					int pc = worker.c;
					int nr, nc;
					int d = st.moves.get(prevIdx + i);
					if (d == 0) {
						nr = pr;
						nc = pc;
					} else {
						--d;
						nr = pr + DR[d];
						nc = pc + DC[d];
					}
					if (baseR == nr && baseC == nc || baseR == pr && baseC == pc) {
						ok = false;
						break;
					}
				}
				if (!ok) {
					creator.pi--;
					st.moves.set(prevIdx + baseCreator, 0);
				}
			}
			for (int i = 0; i < prevWorkerCount; ++i) {
				if (!st.workers.get(i).move(st.moves.get(prevIdx + i))) {
					int newd = st.workers.get(i).step();
					st.moves.set(prevIdx + i, newd);
					st.workers.get(i).move(newd);
				}
			}
			prevIdx += prevWorkerCount;
			for (int i = 0; i < prevBaseCount; ++i) {
				st.bases.get(i).move(st.moves.get(prevIdx + i));
			}
			--st.turn;
		}
		int[] ans = new int[st.moves.size()];
		for (int i = 0; i < ans.length; ++i) {
			ans[i] = st.moves.get(i);
		}
		debug("maxBase:" + maxBase + " maxWorker:" + maxWorker + " " + st.pool);
		if (st.pool > bestScore) {
			bestScore = st.pool;
			bestAns = ans.clone();
			bestWorkers = maxWorker;
			bestBases = maxBase;
			bestProb = createBaseProb;
		}
		maxtime = Math.max(maxtime, System.currentTimeMillis() - solveStartTime);
	}

	void addSearchList(int v) {
		searchList[searchPos++] = v;
	}

	class State {
		int turn;
		int[][] board;
		boolean[][] reserve;
		int totalRes, remainRes, pool;
		ArrayList<Base> bases;
		ArrayList<Worker> workers;
		ArrayList<Integer> moves;
		int prevCreateTurn = 0;
		int creation;
		static final int CREATE_NO = 0;
		static final int CREATE_BASE = 1;
		static final int CREATE_BASE_PENDING = 2;
		static final int CREATE_WORKER = 3;

		State() {
			bases = new ArrayList<>();
			workers = new ArrayList<>();
			moves = new ArrayList<>();
		}

		State(State o) {
			this.turn = o.turn;
			int N = o.board.length;
			this.board = new int[N][];
			this.reserve = new boolean[N][];
			for (int i = 0; i < N; ++i) {
				this.board[i] = o.board[i].clone();
				this.reserve[i] = o.reserve[i].clone();
			}
			this.totalRes = o.totalRes;
			this.remainRes = o.remainRes;
			this.pool = o.pool;
			this.bases = new ArrayList<>(o.bases.size());
			for (int i = 0; i < o.bases.size(); ++i) {
				this.bases.add(o.bases.get(i).clone());
			}
			this.workers = new ArrayList<>(o.workers.size());
			for (int i = 0; i < o.workers.size(); ++i) {
				this.workers.add(o.workers.get(i).clone());
			}
			this.moves = new ArrayList<>(o.moves);
			this.prevCreateTurn = o.prevCreateTurn;
			this.creation = o.creation;
		}

		void take(int r, int c) {
			remainRes -= board[r][c];
			board[r][c] = 0;
		}

		void judgeWantBase() {
			if (creation != CREATE_NO) return;
			if (bases.size() >= maxBase) return;
			if (pool < baseCost[bases.size() - 1]) return;
			if (turn < 100) return;
			if (extraAdjust) {
				if (rnd.nextInt(createBaseProb) == 0) {
					creation = CREATE_BASE;
					prevCreateTurn = turn;
				}
			} else {
				if (turn < prevCreateTurn - 80) {
					creation = CREATE_BASE;
					prevCreateTurn = turn;
				}
			}
		}

		int createWorkerIndex() {
			if (st.creation == State.CREATE_NO && st.workers.size() < maxWorker
					&& st.pool >= workerCost[st.workers.size() - 1]) {
				return st.bases.size() - 1;
			} else {
				return -1;
			}
		}

	}

	class Base {
		int r, c;
		boolean willCreateWorker;

		public Base(int r, int c) {
			this.r = r;
			this.c = c;
		}

		public Base clone() {
			Base base = new Base(r, c);
			base.willCreateWorker = this.willCreateWorker;
			return base;
		}

		int step() {
			if (willCreateWorker) {
				for (int i = 0; i < 4; ++i) {
					int nr = r + DR[i];
					int nc = c + DC[i];
					if (0 <= nr && nr < N && 0 <= nc && nc < N && st.board[nr][nc] == 0) {
						st.pool -= workerCost[st.workers.size() - 1];
						willCreateWorker = false;
						return i + 1;
					}
				}
				throw new RuntimeException("cannot create worker");
			}
			return 0;
		}

		void move(int d) {
			if (d == 0) return;
			--d;
			int nr = r + DR[d];
			int nc = c + DC[d];
			st.workers.add(new Worker(nr, nc));
		}

	}

	class Worker {
		int r, c;
		int[] plan = new int[150];
		int pi = 0, pl = 0;
		int targetR, targetC;
		int load;

		public Worker(int r, int c) {
			this.r = r;
			this.c = c;
		}

		public Worker clone() {
			Worker ret = new Worker(r, c);
			ret.plan = this.plan.clone();
			ret.pi = this.pi;
			ret.pl = this.pl;
			ret.load = this.load;
			return ret;
		}

		int step() {
			if (pi == pl) {
				if (load == 0) {
					if (st.creation == State.CREATE_BASE) {
						createBasePlan();
					} else {
						createGetPlan(st.turn / 2);
					}
				} else {
					createPutPlan();
				}
			}
			return plan[pi++];
		}

		boolean move(int d) {
			if (d == 0) return true;
			if (d > 0) {
				--d;
				int nr = r + DR[d];
				int nc = c + DC[d];
				if (st.board[nr][nc] > 0) {
					if (load > 0) {
						// why comes here?  
						createPutPlan();
						return false;
					}
					load = st.board[nr][nc];
					st.take(nr, nc);
				} else if (st.board[nr][nc] == -1) {
					if (load == 0) {
						// another base is created on the path
						st.reserve[targetR][targetC] = false;
						createGetPlan(st.turn / 2);
						return false;
					}
					st.pool += load;
					load = 0;
				} else {
					this.r += DR[d];
					this.c += DC[d];
				}
			} else {
				d = -d - 1;
				int nr = r + DR[d];
				int nc = c + DC[d];
				st.pool -= baseCost[st.bases.size() - 1];
				st.bases.add(new Base(nr, nc));
				st.creation = State.CREATE_NO;
				st.board[nr][nc] = -1;
			}
			return true;
		}

		void createBasePlan() {
			timer.start(2);
			++searchIdx;
			int WINDOW = 8;
			int[][] sum = new int[N + 2 * WINDOW + 1][N + 2 * WINDOW + 1];
			for (int i = 0; i < N; ++i) {
				for (int j = 0; j < N; ++j) {
					sum[i + WINDOW + 1][j + WINDOW + 1] = st.board[i][j] + sum[i + WINDOW][j + WINDOW + 1]
							+ sum[i + WINDOW + 1][j + WINDOW] - sum[i + WINDOW][j + WINDOW];
				}
				for (int j = N; j < N + WINDOW; ++j) {
					sum[i + WINDOW + 1][j + WINDOW + 1] = sum[i + WINDOW][j + WINDOW + 1] + sum[i + WINDOW + 1][j + WINDOW]
							- sum[i + WINDOW][j + WINDOW];
				}
			}
			for (int i = N; i < N + WINDOW; ++i) {
				for (int j = 0; j < N + WINDOW; ++j) {
					sum[i + WINDOW + 1][j + WINDOW + 1] = sum[i + WINDOW][j + WINDOW + 1] + sum[i + WINDOW + 1][j + WINDOW]
							- sum[i + WINDOW][j + WINDOW];
				}
			}
			int candR = -1;
			int candC = -1;
			double candV = -1e10;
			int qi = 0;
			searchPos = 0;
			addSearchList((this.r << 8) + this.c);
			for (int i = 0;; ++i) {
				int end = searchPos;
				for (int j = qi; j < end; ++j) {
					int cp = searchList[j];
					int cr = cp >> 8;
					int cc = cp & 0xFF;
					for (int k = 0; k < 4; ++k) {
						int nr = cr + DR[k];
						int nc = cc + DC[k];
						if (nr < 0 || N <= nr || nc < 0 || N <= nc) continue;
						if (searchStage[nr][nc] == searchIdx) continue;
						if (st.board[nr][nc] != 0) continue;
						searchStage[nr][nc] = searchIdx;
						searchDir[nr][nc] = k;
						double v = sum[nr + WINDOW * 2 + 1][nc + WINDOW * 2 + 1] - sum[nr][nc + WINDOW * 2 + 1]
								- sum[nr + WINDOW * 2 + 1][nc] + sum[nr][nc];
						v /= (origSt.totalRes / (N * N));
						int len = 2 * N;
						for (Base base : st.bases) {
							len = Math.min(len, Math.abs(base.r - nr) + Math.abs(base.c - nc));
						}
						v -= 3000.0 / Math.sqrt(len);
						int horz = Math.min(N - nr, nr + 1);
						int vert = Math.min(N - nc, nc + 1);
						v -= 400.0 / horz;
						v -= 400.0 / vert;
						v -= i * 10;
						if (v > candV) {
							candR = nr;
							candC = nc;
							candV = v;
						}
						addSearchList((nr << 8) + nc);
					}
				}
				if (searchPos == end) break;
			}
			timer.stop(2);
			if (candR == -1) {
				createGetPlan(st.turn / 2);
			} else {
				trail(candR, candC);
				plan[pl - 1] *= -1;
				st.creation = State.CREATE_BASE_PENDING;
			}
		}

		void createGetPlan(int limit) {
			timer.start(0);
			++searchIdx;
			int candR = -1;
			int candC = -1;
			double candV = -1;
			final boolean easy = st.remainRes * 20 < st.totalRes;
			int qi = 0;
			searchPos = 0;
			addSearchList((this.r << 8) + this.c);
			OUT: for (int i = 0; i < limit; ++i) {
				double div = 1.0 / Math.pow(i + 1, st.turn < 100 ? 1 : searchP);
				if (maxResource * div < candV) break;
				int end = searchPos;
				for (int j = qi; j < end; ++j) {
					int cp = searchList[j];
					int cr = cp >> 8;
					int cc = cp & 0xFF;
					for (int k = 0; k < 4; ++k) {
						int nr = cr + DR[k];
						int nc = cc + DC[k];
						if (nr < 0 || N <= nr || nc < 0 || N <= nc) continue;
						if (searchStage[nr][nc] == searchIdx) continue;
						searchStage[nr][nc] = searchIdx;
						searchDir[nr][nc] = k;
						if (st.board[nr][nc] > 0 && !st.reserve[nr][nc]) {
							double v = st.board[nr][nc] * div;
							if (v > candV) {
								candR = nr;
								candC = nc;
								candV = v;
								if (easy) {
									break OUT;
								}
							}
						} else if (st.board[nr][nc] == 0) {
							addSearchList((nr << 8) + nc);
						}
					}
				}
			}
			if (candV == -1) {
				Arrays.fill(plan, 0);
				pi = 0;
				pl = plan.length;
				return;
			}
			targetR = candR;
			targetC = candC;
			st.reserve[candR][candC] = true;
			timer.stop(0);
			trail(candR, candC);
		}

		void createPutPlan() {
			timer.start(1);
			++searchIdx;
			int qi = 0;
			searchPos = 0;
			addSearchList((this.r << 8) + this.c);
			while (true) {
				int end = searchPos;
				for (int j = qi; j < end; ++j) {
					int cp = searchList[j];
					int cr = cp >> 8;
					int cc = cp & 0xFF;
					for (int k = 0; k < 4; ++k) {
						int nr = cr + DR[k];
						int nc = cc + DC[k];
						if (nr < 0 || N <= nr || nc < 0 || N <= nc) continue;
						if (searchStage[nr][nc] == searchIdx) continue;
						searchStage[nr][nc] = searchIdx;
						searchDir[nr][nc] = k;
						if (st.board[nr][nc] == -1) {
							timer.stop(1);
							trail(nr, nc);
							return;
						} else if (st.board[nr][nc] == 0) {
							addSearchList((nr << 8) + nc);
						}
					}
				}
			}
		}

		void trail(int cr, int cc) {
			pi = pl = 0;
			while (cr != this.r || cc != this.c) {
				int d = searchDir[cr][cc];
				plan[pl++] = d + 1;
				d = (d + 2) & 3;
				cr += DR[d];
				cc += DC[d];
			}
			for (int i = 0; i < pl / 2; ++i) {
				int tmp = plan[i];
				plan[i] = plan[pl - 1 - i];
				plan[pl - 1 - i] = tmp;
			}
		}
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}

	}
}
