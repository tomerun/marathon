import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

class TestCase {
	public static final int MIN_N = 40, MAX_N = 60;
	public static final double MIN_RP = 0.25, MAX_RP = 0.75;
	public static final double MIN_PW = 1.0, MAX_PW = 5.0;
	public static final double RESOURCE_MUL = 10000;
	public static final int MIN_NB = 10, MAX_NB = 20;
	public static final double MIN_BMUL = 0.25, MAX_BMUL = 0.5;
	public static final double MAX_BPW = 100.0;
	public static final double MIN_TMUL = 0.5, MAX_TMUL = 1.0;
	public static final double TDIV = 100.0;

	int N;
	int[] board;
	int[] baseCost;
	int[] workerCost;
	int T;
	double rp, pw;
	double bpw, bmul, wpw, wmul;
	int totalResources = 0;

	int[] generateCostsArray(Random rnd, int tot) {
		int NB = MIN_NB + rnd.nextInt(MAX_NB - MIN_NB + 1);
		double bmul = MIN_BMUL + (MAX_BMUL - MIN_BMUL) * rnd.nextDouble();
		double bpw = Math.pow(MAX_BPW, rnd.nextDouble() / (NB - 1));
		if (this.bpw == 0) {
			this.bpw = bpw;
			this.bmul = bmul;
		} else {
			this.wpw = bpw;
			this.wmul = bmul;
		}
		double BS = 0, add = 1.0;
		for (int i = 0; i < NB; i++) {
			BS += add;
			add *= bpw;
		}
		int[] ret = new int[NB];
		double mul = 1.0;
		for (int i = 0; i < NB; i++) {
			ret[i] = (int) Math.ceil(mul / BS * tot * bmul);
			mul *= bpw;
		}
		return ret;
	}

	public TestCase(long seed) throws Exception {
		SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
		rnd.setSeed(seed);

		this.N = MIN_N + rnd.nextInt(MAX_N - MIN_N + 1);
		this.board = new int[N * N];
		rp = MIN_RP + (MAX_RP - MIN_RP) * rnd.nextDouble();
		pw = MIN_PW + (MAX_PW - MIN_PW) * rnd.nextDouble();
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (rnd.nextDouble() < rp) {
					board[N * i + j] = (int) Math.ceil(RESOURCE_MUL * Math.pow(rnd.nextDouble(), pw));
					totalResources += board[N * i + j];
				}
			}
		}

		while (true) {
			int r = rnd.nextInt(N);
			int c = rnd.nextInt(N);
			int dir = rnd.nextInt(4);
			int r2 = r + World.DR[dir];
			int c2 = c + World.DC[dir];
			if (r2 >= 0 && r2 < N && c2 >= 0 && c2 < N) {
				board[N * r + c] = -1;
				board[N * r2 + c2] = -2;
				break;
			}
		}
		baseCost = generateCostsArray(rnd, totalResources);
		workerCost = generateCostsArray(rnd, totalResources);
		double tmul = MIN_TMUL + (MAX_TMUL - MIN_TMUL) * rnd.nextDouble();
		T = (int) Math.ceil(N * N * N / TDIV * tmul);
	}
}

class Drawer extends JFrame {
	public static final int EXTRA_WIDTH = 500;
	public static final int EXTRA_HEIGHT = 100;

	public World world;
	public DrawerPanel panel;
	public int cellSize, boardSize;
	public int width, height;
	public boolean pauseMode = false;
	public boolean outcomeKnown = false;
	public int outcome = 0;

	class DrawerKeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			synchronized (keyMutex) {
				if (e.getKeyChar() == ' ') {
					pauseMode = !pauseMode;
				}
				keyPressed = true;
				keyMutex.notifyAll();
			}
		}
	}

	class DrawerPanel extends JPanel {
		private Color getColor(int resource) {
			return new Color((10000 - resource) * 191 / 10000, (10000 - resource) * 255 / 10000, 255);
		}

		public void drawGrid(Graphics g, World world, int topX, int topY) {
			g.setColor(Color.WHITE);
			g.fillRect(topX, topY, cellSize * boardSize + 1, cellSize * boardSize + 1);

			g.setColor(Color.BLACK);
			for (int i = 0; i <= boardSize; i++) {
				g.drawLine(topX + i * cellSize, topY, topX + i * cellSize, topY + cellSize * boardSize);
				g.drawLine(topX, topY + i * cellSize, topX + cellSize * boardSize, topY + i * cellSize);
			}

			g.setColor(new Color(63, 63, 63));
			for (Cell base : world.bases) {
				g.fillRect(topX + base.c * cellSize + 1, topY + base.r * cellSize + 1, cellSize - 1, cellSize - 1);
			}

			for (Cell worker : world.workers) {
				g.setColor(new Color(185, 122, 87));
				g.fillRect(topX + worker.c * cellSize + 1, topY + worker.r * cellSize + 1, cellSize - 1, cellSize - 1);
				if (worker.hasResource > 0) {
					g.setColor(getColor(worker.hasResource));
					g.fillRect(topX + worker.c * cellSize + cellSize / 4 + 1, topY + worker.r * cellSize + cellSize / 4 + 1,
							cellSize - 1 - 2 * (cellSize / 4), cellSize - 1 - 2 * (cellSize / 4));
				}
			}

			for (int i = 0; i < boardSize; i++) {
				for (int j = 0; j < boardSize; j++) {
					if (world.resource[i][j] > 0) {
						g.setColor(getColor(world.resource[i][j]));
						g.fillRect(topX + j * cellSize + 1, topY + i * cellSize + 1, cellSize - 1, cellSize - 1);
					}
				}
			}
		}

		public void drawMeter(Graphics2D g2, World world, int topX, int topY, int width, int height) {
			double inPool = world.resourcePool / (double) world.tc.totalResources;
			double gone = world.gatheredResources / (double) world.tc.totalResources;
			int greenLen = (int) Math.floor(width * inPool);
			int redLen = (int) Math.floor(width * gone);

			g2.setColor(new Color(236, 203, 15));
			g2.fillRect(topX, topY, width, height);
			g2.setColor(new Color(255, 63, 63));
			g2.fillRect(topX, topY, redLen, height);
			g2.setColor(new Color(17, 130, 37));
			g2.fillRect(topX, topY, greenLen, height);
			g2.setColor(Color.BLACK);
			g2.drawRect(topX, topY, width, height);
		}

		public void oneSolutionPaint(Graphics g) {
			drawGrid(g, world, 15, 15);
			g.setColor(Color.BLACK);
			g.setFont(new Font("Arial", Font.BOLD, 12));
			Graphics2D g2 = (Graphics2D) g;

			final int horPos = 40 + boardSize * cellSize;
			final int meterLen = 420;

			g2.drawString("Board size = " + world.tc.N, horPos, 30);
			g2.drawString("Turn = " + (world.curTurn < world.tc.T ? world.curTurn + 1 : "---") + " (out of " + world.tc.T
					+ ")", horPos, 50);

			drawMeter(g2, world, horPos, 65, meterLen, 15);

			g2.drawString("Resources:", horPos, 95);
			g2.setColor(new Color(17, 130, 37));
			g2.drawString("In Pool", horPos + 72, 95);
			g2.setColor(new Color(255, 63, 63));
			g2.drawString("Spent", horPos + 122, 95);
			g2.setColor(new Color(236, 203, 15));
			g2.drawString("Remaining", horPos + 170, 95);

			g2.setColor(Color.BLACK);
			final int extra = 45;

			g2.drawString(
					"Resources in pool = " + world.resourcePool + " ("
							+ String.format("%.2f", world.resourcePool / (double) world.tc.totalResources * 100)
							+ "% of board's total)", horPos, 85 + extra);

			int travel = 0;
			for (Cell worker : world.workers) {
				travel += worker.hasResource;
			}
			g2.drawString("Resources traveling to base = " + travel, horPos, 105 + extra);

			int WC = world.workers.size(), BC = world.bases.size();

			g2.drawString("Workers = " + WC + " (out of " + (world.tc.workerCost.length + 1) + ")", horPos, 140 + extra);
			g2.drawString("Next worker cost = "
					+ (WC == world.tc.workerCost.length + 1 ? "---" : world.tc.workerCost[WC - 1]), horPos, 160 + extra);

			g2.drawString("Bases = " + BC + " (out of " + (world.tc.baseCost.length + 1) + ")", horPos, 195 + extra);
			g2.drawString("Next base cost = " + (BC == world.tc.baseCost.length + 1 ? "---" : world.tc.baseCost[BC - 1]),
					horPos, 215 + extra);

			g2.drawString(
					"Gathered resources (pool + purchases) = " + world.gatheredResources + " ("
							+ String.format("%.2f", world.gatheredResources / (double) world.tc.totalResources * 100)
							+ "% of board's total)", horPos, 250 + extra);
			g2.drawString(
					"Gathered resources per resource cell = "
							+ (world.gatheredResourceCells == 0 ? "---" : String.format("%.2f", world.gatheredResources
									/ (double) world.gatheredResourceCells)), horPos, 270 + extra);
			g2.drawString(
					"Gathered resources per turn = "
							+ (world.curTurn == 0 ? "---" : String.format("%.2f", world.gatheredResources / (double) world.curTurn)),
					horPos, 290 + extra);

			g2.drawString(
					"Remaining resources = " + world.remainingResources + " ("
							+ String.format("%.2f", world.remainingResources / (double) world.tc.totalResources * 100)
							+ "% of board's total)", horPos, 325 + extra);
			g2.drawString(
					"Remaining resources per resource cell = "
							+ (world.remaininResourceCells == 0 ? "---" : String.format("%.2f", world.remainingResources
									/ (double) world.remaininResourceCells)), horPos, 345 + extra);
		}

		public void paint(Graphics g) {
			synchronized (world.worldLock) {
				oneSolutionPaint(g);
			}
		}
	}

	class DrawerWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			System.exit(0);
		}
	}

	final Object keyMutex = new Object();
	boolean keyPressed;

	public void processPause() {
		synchronized (keyMutex) {
			if (!pauseMode) {
				return;
			}
			keyPressed = false;
			while (!keyPressed) {
				try {
					keyMutex.wait();
				} catch (InterruptedException e) {
					// do nothing
				}
			}
		}
	}

	public Drawer(World world, int cellSize) {
		panel = new DrawerPanel();
		getContentPane().add(panel);
		addWindowListener(new DrawerWindowListener());
		this.world = world;
		boardSize = world.tc.N;
		this.cellSize = cellSize;
		width = cellSize * boardSize + EXTRA_WIDTH;
		height = cellSize * boardSize + EXTRA_HEIGHT;
		addKeyListener(new DrawerKeyListener());
		setSize(width, height);
		setTitle("TCO'13 Marathon Championship Round");
		setResizable(false);
		setVisible(true);
	}
}

class Cell {
	int r, c;
	int hasResource;

	public Cell(int r, int c) {
		this.r = r;
		this.c = c;
	}

	public boolean equals(Object other) {
		return other instanceof Cell && r == ((Cell) other).r && c == ((Cell) other).c;
	}
}

class World {
	public static final int[] DR = new int[] { -1, 0, 1, 0 };
	public static final int[] DC = new int[] { 0, 1, 0, -1 };
	public final Object worldLock;

	TestCase tc;
	List<Cell> workers = new ArrayList<Cell>();
	List<Cell> bases = new ArrayList<Cell>();
	int resourcePool = 0;
	int gatheredResources = 0;
	int gatheredResourceCells = 0;
	int remainingResources = 0;
	int remaininResourceCells = 0;
	int[][] resource;
	int curTurn;

	public World(TestCase tc, Object worldLock) {
		this.tc = tc;
		this.worldLock = worldLock;
		this.resource = new int[tc.N][tc.N];

		for (int i = 0; i < tc.N; i++) {
			for (int j = 0; j < tc.N; j++) {
				int what = tc.board[i * tc.N + j];
				if (what > 0) {
					resource[i][j] = what;
					remainingResources += what;
					remaininResourceCells++;
				}
				if (what == -1) {
					bases.add(new Cell(i, j));
				}
				if (what == -2) {
					workers.add(new Cell(i, j));
				}
			}
		}
	}

	public static final int MOVE = 0, GATHER = 1, PUT = 2, BUILD_BASE = 3, SKIP = 4, BUILD_WORKER = 5;

	boolean cellExists(List<Cell> bases, Cell cell) {
		for (Cell b : bases) {
			if (cell.equals(b)) {
				return true;
			}
		}
		return false;
	}

	public String processTurn(int[] workerMoves, int[] baseMoves) {
		synchronized (worldLock) {
			curTurn++;
			int W = workerMoves.length, B = baseMoves.length;

			int[] workerMoveType = new int[W], baseMoveType = new int[B];
			Cell[] workerInteracts = new Cell[W], baseInteracts = new Cell[B];

			int extraBases = 0, extraWorkers = 0;

			for (int i = 0; i < W; i++) {
				if (workerMoves[i] > 4 || workerMoves[i] < -4) {
					return "Invalid move for worker " + i + " (0-based): " + workerMoves[i] + ".";
				}
				Cell curWorker = workers.get(i);
				if (workerMoves[i] == 0) {
					workerMoveType[i] = SKIP;
					workerInteracts[i] = new Cell(curWorker.r, curWorker.c);
					continue;
				}
				int dir = Math.abs(workerMoves[i]) - 1;
				workerInteracts[i] = new Cell(curWorker.r + DR[dir], curWorker.c + DC[dir]);
				if (workerInteracts[i].r < 0 || workerInteracts[i].r >= tc.N || workerInteracts[i].c < 0
						|| workerInteracts[i].c >= tc.N) {
					return "Worker " + i
							+ " (0-based) is trying to interact (move/gather/deliver/build) with a cell outside the board.";
				}
				if (workerMoves[i] > 0) {
					if (resource[workerInteracts[i].r][workerInteracts[i].c] > 0) {
						if (curWorker.hasResource > 0) {
							return "Worker "
									+ i
									+ " (0-based) is trying to gather resources before returning the previously gathered resources to base.";
						}
						workerMoveType[i] = GATHER;
					} else if (cellExists(bases, workerInteracts[i])) {
						if (curWorker.hasResource == 0) {
							return "Worker " + i
									+ " (0-based) is trying to deliver resources to base while not carrying any resources.";
						}
						workerMoveType[i] = PUT;
					} else {
						workerMoveType[i] = MOVE;
					}
				} else {
					if (curWorker.hasResource > 0) {
						return "Worker " + i + " (0-based) is trying to build base while carrying resources.";
					}
					if (resource[workerInteracts[i].r][workerInteracts[i].c] > 0) {
						return "Worker " + i + " (0-based) is trying to build base at a cell with resources.";
					}
					if (cellExists(bases, workerInteracts[i])) {
						return "Worker " + i + " (0-based) is trying to build base at a cell where base already exists.";
					}
					if (cellExists(workers, workerInteracts[i])) {
						return "Worker " + i + " (0-based) is trying to build base at a cell where a worker is currently standing.";
					}
					workerMoveType[i] = BUILD_BASE;
					extraBases++;
				}
			}

			for (int i = 0; i < B; i++) {
				if (baseMoves[i] < 0 || baseMoves[i] > 4) {
					return "Invalid move for base " + i + " (0-based): " + baseMoves[i] + ".";
				}
				Cell curBase = bases.get(i);
				if (baseMoves[i] == 0) {
					baseMoveType[i] = SKIP;
					baseInteracts[i] = new Cell(curBase.r, curBase.c);
					continue;
				}
				int dir = baseMoves[i] - 1;
				baseMoveType[i] = BUILD_WORKER;
				extraWorkers++;
				baseInteracts[i] = new Cell(curBase.r + DR[dir], curBase.c + DC[dir]);
				if (baseInteracts[i].r < 0 || baseInteracts[i].r >= tc.N || baseInteracts[i].c < 0
						|| baseInteracts[i].c >= tc.N) {
					return "Base " + i + " (0-based) is trying to build a worker at a cell outside the board.";
				}
				if (resource[baseInteracts[i].r][baseInteracts[i].c] > 0) {
					return "Base " + i + " (0-based) is trying to build a worker at a cell with resources.";
				}
				if (cellExists(bases, baseInteracts[i])) {
					return "Base " + i + " (0-based) is trying to build a worker at a cell containing a base.";
				}
			}

			for (int i = 0; i < W; i++) {
				for (int j = 0; j < W; j++) {
					if (i == j || !workerInteracts[i].equals(workerInteracts[j])) {
						continue;
					}
					if (workerMoveType[i] == BUILD_BASE && workerMoveType[j] == MOVE) {
						return "Worker " + i + " (0-based) is trying to build a base at the same cell into which worker " + j
								+ " (0-based) is trying to move.";
					}
					if (workerMoveType[i] == GATHER && workerMoveType[j] == GATHER) {
						return "Workers " + i + " and " + j + " are trying to gather resources from the same cell simultaneously.";
					}
					if (workerMoveType[i] == BUILD_BASE && workerMoveType[j] == BUILD_BASE) {
						return "Workers " + i + " and " + j + " are trying to build base at the same cell simultaneously.";
					}
				}

				for (int j = 0; j < B; j++) {
					if (workerInteracts[i].equals(baseInteracts[j]) && workerMoveType[i] == BUILD_BASE
							&& baseMoveType[j] == BUILD_WORKER) {
						return "Base " + j + " (0-based) is trying to build a worker at the same cell at which worker " + i
								+ " (0-based) is trying to build a base.";
					}
				}
			}

			int remainingResource = resourcePool;

			if (W + extraWorkers > tc.workerCost.length + 1) {
				return "Workers build limit exceeded.";
			}

			for (int i = W - 1; i < W + extraWorkers - 1; i++) {
				if (remainingResource < tc.workerCost[i]) {
					return "Not enough resources to build a worker.";
				}
				remainingResource -= tc.workerCost[i];
			}

			if (B + extraBases > tc.baseCost.length + 1) {
				return "Bases build limit exceeded.";
			}

			for (int i = B - 1; i < B + extraBases - 1; i++) {
				if (remainingResource < tc.baseCost[i]) {
					return "Not enough resources to build a base.";
				}
				remainingResource -= tc.baseCost[i];
			}

			for (int i = 0; i < W; i++) {
				if (workerMoveType[i] == SKIP) {
					continue;
				}
				int dir = Math.abs(workerMoves[i]) - 1;
				Cell curWorker = workers.get(i);
				int r = curWorker.r + DR[dir], c = curWorker.c + DC[dir];
				if (workerMoveType[i] == MOVE) {
					curWorker.r += DR[dir];
					curWorker.c += DC[dir];
				}
				if (workerMoveType[i] == GATHER) {
					curWorker.hasResource = resource[r][c];
					resource[r][c] = 0;
				}
				if (workerMoveType[i] == PUT) {
					resourcePool += curWorker.hasResource;
					gatheredResources += curWorker.hasResource;
					gatheredResourceCells++;
					remainingResources -= curWorker.hasResource;
					remaininResourceCells--;
					curWorker.hasResource = 0;
				}
				if (workerMoveType[i] == BUILD_BASE) {
					resourcePool -= tc.baseCost[bases.size() - 1];
					bases.add(new Cell(r, c));
				}
			}

			for (int i = 0; i < B; i++) {
				if (baseMoveType[i] == BUILD_WORKER) {
					int dir = baseMoves[i] - 1;
					int r = bases.get(i).r + DR[dir], c = bases.get(i).c + DC[dir];
					resourcePool -= tc.workerCost[workers.size() - 1];
					workers.add(new Cell(r, c));
				}
			}

			return "";
		}
	}
}

class Solution {
	int pos;
	int[] data;

	public Solution(int[] data) {
		this.data = data;
	}

	boolean hasNext() {
		return pos < data.length;
	}

	int next() {
		return data[pos++];
	}
}

public class Tester {
	public static boolean vis = false;
	public static int cellSize = 14;
	public static int delay = 10;
	public static boolean startPaused = false;

	public void worldProcessTurn(Solution solution, World world, int turnID) throws Exception {
		int W = world.workers.size();
		int[] workerMoves = new int[W];
		for (int i = 0; i < W; i++) {
			if (!solution.hasNext()) {
				throw new Exception("Not enough elements in your return value. Can't get move for worker " + i
						+ " (0-based), turn " + turnID + " (0-based).");
			}
			workerMoves[i] = solution.next();
		}
		int B = world.bases.size();
		int[] baseMoves = new int[B];
		for (int i = 0; i < B; i++) {
			if (!solution.hasNext()) {
				throw new Exception("Not enough elements in your return value. Can't get move for base " + i
						+ " (0-based), turn " + turnID + " (0-based).");
			}
			baseMoves[i] = solution.next();
		}

		String msg = world.processTurn(workerMoves, baseMoves);
		if (msg.length() > 0) {
			throw new Exception("during turn " + turnID + " (0-based). " + msg);
		}
	}

	public Result runTest(long seed) throws Exception {
		TestCase tc = new TestCase(seed);
		Result res = new Result();
		res.seed = seed;
		res.N = tc.N;
		res.T = tc.T;
		res.pw = tc.pw;
		res.rp = tc.rp;
		res.bpw = tc.bpw;
		res.bmul = tc.bmul;
		res.wpw = tc.wpw;
		res.wmul = tc.wmul;
		res.bc = tc.baseCost;
		res.wc = tc.workerCost;
		long before = System.currentTimeMillis();
		int[] ans = new GatheringResources().bestStrategy(tc.N, tc.board, tc.baseCost, tc.workerCost, tc.T);
		res.elapsed = System.currentTimeMillis() - before;
		Object worldLock = new Object();
		World world = new World(tc, worldLock);
		res.total = world.remainingResources;
		Drawer drawer = null;
		if (vis) {
			drawer = new Drawer(world, cellSize);
			if (startPaused) {
				drawer.pauseMode = true;
			}
		}

		Solution sol = new Solution(ans);
		for (int t = 0; t <= tc.T; t++) {
			if (vis) {
				drawer.processPause();
				drawer.repaint();
				try {
					Thread.sleep(delay);
				} catch (Exception e) {
					// do nothing
				}
			}
			if (t == tc.T) {
				break;
			}
			try {
				worldProcessTurn(sol, world, t);
			} catch (Exception e) {
				System.out.println("ERROR: " + e.getMessage());
				return res;
			}
		}
		res.workerCount = world.workers.size();
		res.baseCount = world.bases.size();
		res.score = world.resourcePool;
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) {
		long seed = 1;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-begin")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-end")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-vis")) {
				vis = true;
			} else if (args[i].equals("-sz")) {
				cellSize = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-delay")) {
				delay = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-pause")) {
				startPaused = true;
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}

		try {
			if (begin != -1 && end != -1) {
				vis = false;
				ArrayList<Long> seeds = new ArrayList<Long>();
				for (long i = begin; i <= end; ++i) {
					seeds.add(i);
				}
				int len = seeds.size();
				Result[] results = new Result[len];
				TestThread[] threads = new TestThread[THREAD_COUNT];
				int prev = 0;
				for (int i = 0; i < THREAD_COUNT; ++i) {
					int next = len * (i + 1) / THREAD_COUNT;
					threads[i] = new TestThread(prev, next - 1, seeds, results);
					prev = next;
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].start();
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].join();
				}
				double sum = 0;
				for (int i = 0; i < results.length; ++i) {
					System.out.println(results[i]);
					System.out.println();
					sum += results[i].score;
				}
				System.out.println("ave:" + (sum / results.length));
			} else {
				Tester tester = new Tester();
				Result res = tester.runTest(seed);
				System.out.println(res);
			}
		} catch (Exception e) {
			System.err.println("ERROR: Unexpected error while running your test case.");
			e.printStackTrace();
		}

	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int N, T;
		double rp, pw;
		int[] wc, bc;
		int workerCount, baseCount;
		int total;
		double bpw, bmul, wpw, wmul;
		int score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d  N:%4d  T:%4d\n", seed, N, T);
			ret += String.format(" rp:%.4f    pw:%.4f\n", rp, pw);
			ret += String.format("bpw:%.4f  bmul:%.4f\n", bpw, bmul);
			ret += String.format("wpw:%.4f  wmul:%.4f\n", wpw, wmul);
			ret += "bc:" + Arrays.toString(bc) + "\n";
			ret += "wc:" + Arrays.toString(wc) + "\n";
			ret += String.format("bases:%2d  workers:%2d\n", baseCount, workerCount);
			ret += String.format("total resource:%d\n", total);
			ret += String.format(" mean resource:%.4f\n", 1. * total / N / N);
			ret += String.format("elapsed:%.3f  score:%d", elapsed / 1000.0, score);
			return ret;
		}
	}

}
