#include <algorithm>
#include <numeric>
#include <utility>
#include <vector>
#include <string>
#include <set>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <sys/time.h>

using namespace std;
typedef int64_t ll;
typedef uint64_t ull;
typedef uint32_t uint;
typedef vector<uint> vi;
typedef vector<vi> vvi;

const uint INF = 1 << 30;
#define MEASURE_TIME 1
#define DEBUG 1
//#define SUBMIT 1

#ifdef SUBMIT
const double CLOCK_PER_SEC = 3.6e9;
const ll TL = 9600;
#else
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 4000;
#endif

// msec
inline ll getTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		uint i = nextUInt();
		return i * (1.0 / 0xFFFFFFFFu);
	}
};

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum; // clocks

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
		}
		cerr << "]" << endl;
	}
};

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#else
#define debug(format, ...)
#endif

XorShift rnd;
ll startTime;
Timer timer;
Counter counter;
uint R, C, N, K;
vvi gridBuf[20];
vvi gridCur[20];
vi scoreRow;
uint rule[1<<9];

bool transit(int next, int prev, double cold) {
	int diff = next - prev;
	if (diff >= 0) return true;
	return rnd.nextDouble() < exp(diff * cold);
}

struct Board {
	vvi f;
	Board(const vector<string>& grid) : f(R, vi(C)) {
		for (int i = 0; i < R; ++i) {
			for (int j = 0; j < C; ++j) {
				f[i][j] = grid[i][j] - '0';
			}
		} 
	}

	Board(const Board& o) : f(o.f) {}
};

void copyBackPartial(uint cr1, uint cr2) {
	vector<bool> useR(R);
	uint tr1 = cr1;
	uint br1 = cr1;
	useR[cr1] = true;
	uint tr2 = INF;
	uint br2 = INF;
	if (cr1 == cr2) cr2 = INF;
	if (cr2 != INF) {
		tr2 = cr2;
		br2 = cr2;
		useR[cr2] = true;
	}
	for (int i = 0; i < K; ++i) {
		tr1 = tr1 == 0 ? R-1 : tr1-1;
		br1 = br1 == R-1 ? 0 : br1+1;
		useR[tr1] = useR[br1] = true;
		if (cr2 != INF) {
			tr2 = tr2 == 0 ? R-1 : tr2-1;
			br2 = br2 == R-1 ? 0 : br2+1;
			useR[tr2] = useR[br2] = true;
		}
		const vvi& from = gridBuf[i];
		vvi& to = gridCur[i];
		for (int r = 0; r < R; ++r) {
			if (!useR[r]) continue;
			to[r] = from[r];
			if (i == K-1) {
				scoreRow[r] = accumulate(to[r].begin(), to[r].end(), 0);
			}
		}
	}
} 

uint calcPartial(Board& b, uint cr1, uint cr2) {
	vector<bool> useR(2*R);
	uint tr1 = cr1;
	uint br1 = cr1;
	useR[cr1] = true;
	uint tr2 = INF;
	uint br2 = INF;
	if (cr1 == cr2) cr2 = INF;
	if (cr2 != INF) {
		tr2 = cr2;
		br2 = cr2;
		useR[cr2] = true;
	}
	for (int i = 0; i < K-1; ++i) {
		vvi& from = (i == 0 ? b.f : gridBuf[i-1]);
		vvi& to = gridBuf[i];
		START_TIMER(3);
		tr1 = tr1 == 0 ? R-1 : tr1-1;
		br1 = br1 == R-1 ? 0 : br1+1;
		if (cr2 != INF) {
			tr2 = tr2 == 0 ? R-1 : tr2-1;
			br2 = br2 == R-1 ? 0 : br2+1;
		}
		if (i != 0) {
			if (!useR[tr1]) from[tr1] = gridCur[i-1][tr1];
			int tmpr = tr1 == 0 ? R-1 : tr1-1;
			if (!useR[tmpr]) from[tmpr] = gridCur[i-1][tmpr];
			if (!useR[br1]) from[br1] = gridCur[i-1][br1];
			tmpr = br1 == R-1 ? 0 : br1+1;
			if (!useR[tmpr]) from[tmpr] = gridCur[i-1][tmpr];
			if (cr2 != INF) {
				if (!useR[tr2]) from[tr2] = gridCur[i-1][tr2];
				tmpr = tr2 == 0 ? R-1 : tr2-1;
				if (!useR[tmpr]) from[tmpr] = gridCur[i-1][tmpr];
				if (!useR[br2]) from[br2] = gridCur[i-1][br2];
				tmpr = br2 == R-1 ? 0 : br2+1;
				if (!useR[tmpr]) from[tmpr] = gridCur[i-1][tmpr];
			}
		}
		useR[tr1] = useR[br1] = true;
		if (cr2 != INF) {
			useR[tr2] = useR[br2] = true;
		}
		STOP_TIMER(3);
		START_TIMER(4);
		for (int r = 0; r < R; ++r) {
			if (!useR[r]) continue;
			int ur = r == 0 ? R-1 : r-1;
			int br = r == R-1 ? 0 : r+1;
			uint sur  = from[ur][C-1] << 0;
			     sur |= from[r ][C-1] << 1;
			     sur |= from[br][C-1] << 2;
			     sur |= from[ur][0  ] << 3;
			     sur |= from[r ][0  ] << 4;
			     sur |= from[br][0  ] << 5;
			for (int c = 1; c < C; ++c) {
				sur |= from[ur][c] << 6;
				sur |= from[r ][c] << 7;
				sur |= from[br][c] << 8;
				to[r][c-1] = rule[sur];
				sur >>= 3;
			}
			sur |= from[ur][0] << 6;
			sur |= from[r ][0] << 7;
			sur |= from[br][0] << 8;
			to[r][C-1] = rule[sur];
		}
		STOP_TIMER(4);
	}
	uint ret = 0;
	{
		vvi& from = gridBuf[K-2];
		vvi& to = gridBuf[K-1];
		START_TIMER(5);
		tr1 = tr1 == 0 ? R-1 : tr1-1;
		br1 = br1 == R-1 ? 0 : br1+1;
		if (cr2 != INF) {
			tr2 = tr2 == 0 ? R-1 : tr2-1;
			br2 = br2 == R-1 ? 0 : br2+1;
		}
		{
			if (!useR[tr1]) from[tr1] = gridCur[K-2][tr1];
			int tmpr = tr1 == 0 ? R-1 : tr1-1;
			if (!useR[tmpr]) from[tmpr] = gridCur[K-2][tmpr];
			if (!useR[br1]) from[br1] = gridCur[K-2][br1];
			tmpr = br1 == R-1 ? 0 : br1+1;
			if (!useR[tmpr]) from[tmpr] = gridCur[K-2][tmpr];
			if (cr2 != INF) {
				if (!useR[tr2]) from[tr2] = gridCur[K-2][tr2];
				tmpr = tr2 == 0 ? R-1 : tr2-1;
				if (!useR[tmpr]) from[tmpr] = gridCur[K-2][tmpr];
				if (!useR[br2]) from[br2] = gridCur[K-2][br2];
				tmpr = br2 == R-1 ? 0 : br2+1;
				if (!useR[tmpr]) from[tmpr] = gridCur[K-2][tmpr];
			}
		}
		useR[tr1] = useR[br1] = true;
		if (cr2 != INF) {
			useR[tr2] = useR[br2] = true;
		}
		STOP_TIMER(5);
		START_TIMER(6);
		for (int r = 0; r < R; ++r) {
			if (!useR[r]) continue;
			int ur = r == 0 ? R-1 : r-1;
			int br = r == R-1 ? 0 : r+1;
			uint sur  = from[ur][C-1] << 0;
			     sur |= from[r ][C-1] << 1;
			     sur |= from[br][C-1] << 2;
			     sur |= from[ur][0  ] << 3;
			     sur |= from[r ][0  ] << 4;
			     sur |= from[br][0  ] << 5;
			for (int c = 1; c < C; ++c) {
				sur |= from[ur][c] << 6;
				sur |= from[r ][c] << 7;
				sur |= from[br][c] << 8;
				to[r][c-1] = rule[sur];
				ret += rule[sur];
				sur >>= 3;
			}
			sur |= from[ur][0] << 6;
			sur |= from[r ][0] << 7;
			sur |= from[br][0] << 8;
			to[r][C-1] = rule[sur];
			ret += rule[sur];
			ret -= scoreRow[r];
		}
		STOP_TIMER(6);
	}
	return ret;
}

uint calcAll(const Board& b) {
	for (int i = 0; i < K-1; ++i) {
		const vvi& from = (i == 0 ? b.f : gridBuf[i % 2]);
		vvi& to = gridBuf[1 - (i % 2)];
		for (int r = 0; r < R; ++r) {
			int ur = r == 0 ? R-1 : r-1;
			int br = r == R-1 ? 0 : r+1;
			uint sur  = from[ur][C-1] << 0;
			     sur |= from[r ][C-1] << 1;
			     sur |= from[br][C-1] << 2;
			     sur |= from[ur][0  ] << 3;
			     sur |= from[r ][0  ] << 4;
			     sur |= from[br][0  ] << 5;
			for (int c = 1; c < C; ++c) {
				sur |= from[ur][c] << 6;
				sur |= from[r ][c] << 7;
				sur |= from[br][c] << 8;
				to[r][c-1] = rule[sur];
				sur >>= 3;
			}
			sur |= from[ur][0] << 6;
			sur |= from[r ][0] << 7;
			sur |= from[br][0] << 8;
			to[r][C-1] = rule[sur];
		}
	}
	uint ret = 0;
	{
		const vvi& from = gridBuf[1 - K % 2];
		for (int r = 0; r < R; ++r) {
			int ur = r == 0 ? R-1 : r-1;
			int br = r == R-1 ? 0 : r+1;
			uint sur  = from[ur][C-1] << 0;
			     sur |= from[r ][C-1] << 1;
			     sur |= from[br][C-1] << 2;
			     sur |= from[ur][0  ] << 3;
			     sur |= from[r ][0  ] << 4;
			     sur |= from[br][0  ] << 5;
			for (int c = 1; c < C; ++c) {
				sur |= from[ur][c] << 6;
				sur |= from[r ][c] << 7;
				sur |= from[br][c] << 8;
				ret += rule[sur];
				sur >>= 3;
			}
			sur |= from[ur][0] << 6;
			sur |= from[r ][0] << 7;
			sur |= from[br][0] << 8;
			ret += rule[sur];
		}
	}
	return ret;
}

uint calcInit(const Board& b) {
	for (int i = 0; i < K; ++i) {
		const vvi& from = (i == 0 ? b.f : gridCur[i-1]);
		vvi& to = gridCur[i];
		for (int r = 0; r < R; ++r) {
			int ur = r == 0 ? R-1 : r-1;
			int br = r == R-1 ? 0 : r+1;
			uint sur  = from[ur][C-1] << 0;
			     sur |= from[r ][C-1] << 1;
			     sur |= from[br][C-1] << 2;
			     sur |= from[ur][0  ] << 3;
			     sur |= from[r ][0  ] << 4;
			     sur |= from[br][0  ] << 5;
			for (int c = 1; c < C; ++c) {
				sur |= from[ur][c] << 6;
				sur |= from[r ][c] << 7;
				sur |= from[br][c] << 8;
				to[r][c-1] = rule[sur];
				sur >>= 3;
			}
			sur |= from[ur][0] << 6;
			sur |= from[r ][0] << 7;
			sur |= from[br][0] << 8;
			to[r][C-1] = rule[sur];
		}
	}
	scoreRow.resize(R);
	uint ret = 0;
	{
		const vvi& from = gridCur[K-1];
		for (int r = 0; r < R; ++r) {
			for (int c = 0; c < C; ++c) {
				ret += from[r][c];
				scoreRow[r] += from[r][c];
			}
		}
	}
	return ret;
}

uint getMask() {
	const uint guessedAllTurn = 2500000000u / (R * C * K);
	debug("guessedAllTurn:%d\n", guessedAllTurn);
	uint mask = 0x10;
	while (mask * 200 < guessedAllTurn) mask <<= 1;
	mask--;
	debug("mask:%x\n", mask);
	return mask;
}

vvi optimizeAll(const Board& initial) {
	Board board(initial);
	const uint mask = getMask();
	vi pos(N, INF);
	for (int i = 0; i < N; ++i) {
		pos[i] = rnd.nextUInt(R*C-1);
		uint cr = pos[i] / C;
		uint cc = pos[i] - cr * C;
		board.f[cr][cc] ^= 1;
	}
	Board bestBoard(board);
	uint bestScore = calcInit(board);
	uint score = bestScore;
	double cold = .5;
	const int DIV = 8;
	const ll beforeTime = getTime();
	const ll remTime = TL - (getTime() - startTime);
	ll updateTime = beforeTime + remTime / DIV;
	int updateCount = 0;
	START_TIMER(0);
	uint pi = 0;
	for (int turn = 1; ; ++turn, ++pi) {
		if ((turn & mask) == 0) {
			const ll curTime = getTime();
			if (curTime - startTime > TL) {
				debug("turn:%d score:%d cold:%f\n", turn, bestScore, cold);
				break;
			}
			if (curTime > updateTime) {
				updateTime += remTime / DIV;
				debug("turn:%d score:%d cold:%f\n", turn, bestScore, cold);
				cold *= 1.3;
				++updateCount;
				if(updateCount == DIV - 1) cold = 9999;
			}
		}
		if (pi == N) pi = 0;
		uint prev = pos[pi];
		uint prevR = prev / C;
		uint prevC = prev - prevR * C;
		board.f[prevR][prevC] ^= 1;
		uint next = rnd.nextUInt(R*C-1);
		if (next >= prev) ++next;
		uint nextR = next / C;
		uint nextC = next - nextR * C;
		board.f[nextR][nextC] ^= 1;
		START_TIMER(1);
		uint nextScore = calcAll(board);
		STOP_TIMER(1);
		if (transit(nextScore, score, cold)) {
			ADD_COUNTER(0);
			score = nextScore;
			pos[pi] = next;
			if (nextScore > bestScore) {
				ADD_COUNTER(1);
				bestScore = nextScore;
				bestBoard = board;
				// debug("bestScore:%d %d\n", bestScore, turn);
			}
		} else {
			board.f[nextR][nextC] ^= 1;
			board.f[prevR][prevC] ^= 1;
		}
	}
	debug("bestScore:%d\n", bestScore);
	STOP_TIMER(0);
	return bestBoard.f;
}

vvi optimizePartial(const Board& initial) {
	Board board(initial);
	const uint mask = getMask();
	vi pos(N, INF);
	for (int i = 0; i < N; ++i) {
		pos[i] = rnd.nextUInt(R*C-1);
		uint cr = pos[i] / C;
		uint cc = pos[i] - cr * C;
		board.f[cr][cc] ^= 1;
	}
	Board bestBoard(board);
	uint bestScore = calcInit(board);
	uint score = bestScore;
	double cold = .5;
	const int DIV = 8;
	const ll beforeTime = getTime();
	const ll remTime = TL - (getTime() - startTime);
	ll updateTime = beforeTime + remTime / DIV;
	int updateCount = 0;
	START_TIMER(0);
	uint pi = 0;
	for (int turn = 1; ; ++turn, ++pi) {
		if ((turn & mask) == 0) {
			const ll curTime = getTime();
			if (curTime - startTime > TL) {
				debug("turn:%d score:%d cold:%f\n", turn, bestScore, cold);
				break;
			}
			if (curTime > updateTime) {
				updateTime += remTime / DIV;
				debug("turn:%d score:%d cold:%f\n", turn, bestScore, cold);
				cold *= 1.3;
				++updateCount;
				if(updateCount == DIV - 1) cold = 9999;
			}
		}
		if (pi == N) pi = 0;
		uint prev = pos[pi];
		uint prevR = prev / C;
		uint prevC = prev - prevR * C;
		board.f[prevR][prevC] ^= 1;
		uint nextR, nextC;
		{
			uint move = rnd.nextUInt(16);
			if (move < 8) {
				nextR = prevR + R - move;
			} else {
				nextR = prevR + (move - 8);
			}
			if (nextR >= R) nextR -= R;
			if (nextR == prevR) {
				nextC = rnd.nextUInt(C-1);
				if (nextC >= prevC) ++nextC;
			} else {
				nextC = rnd.nextUInt(C);
			}
		}
		board.f[nextR][nextC] ^= 1;
		START_TIMER(1);
		uint nextScore = calcPartial(board, nextR, prevR) + score;
		STOP_TIMER(1);
		if (transit(nextScore, score, cold)) {
			ADD_COUNTER(0);
			score = nextScore;
			pos[pi] = nextR * C + nextC;
			START_TIMER(2);
			copyBackPartial(nextR, prevR);
			STOP_TIMER(2);
			if (nextScore > bestScore) {
				ADD_COUNTER(1);
				bestScore = nextScore;
				bestBoard = board;
				// debug("bestScore:%d %d\n", bestScore, turn);
			}
		} else {
			board.f[nextR][nextC] ^= 1;
			board.f[prevR][prevC] ^= 1;
		}
	}
	debug("bestScore:%d\n", bestScore);
	STOP_TIMER(0);
	return bestBoard.f;
}

class CellularAutomaton {
public:
	CellularAutomaton();
	vector<string> configure(vector<string>& grid, const string& rules, int N_, int K_);
};

CellularAutomaton::CellularAutomaton() {
	startTime = getTime();
}

vector<string> CellularAutomaton::configure(vector<string>& grid, const string& rules, int N_, int K_) {
	R = grid.size();
	C = grid[0].size();
	N = N_;
	K = K_;
	const bool partial = (max(R, C) > 2 * K + 1 + 12);
	debug("partial:%d\n", partial);
	bool rot = partial && R < C;
	if (rot) {
		vector<string> gridTmp(C);
		for (int i = 0; i < R; ++i) {
			for (int j = 0; j < C; ++j) {
				gridTmp[j].push_back(grid[i][j]);
			}
		}
		grid = gridTmp;
		swap(R, C);
	}
	for (int i = 0; i < 20; ++i) {
		gridBuf[i].assign(R, vi(C));
		gridCur[i].assign(R, vi(C));
	}
	for (int i = 0; i < (1 << 9); ++i) {
		int center = (i >> 4) & 1;
		int count = __builtin_popcount(i) - center;
		switch (rules[count]) {
			case '=':
			rule[i] = center;
			break;
			case '-':
			rule[i] = 0;
			break;
			case '+':
			rule[i] = 1;
			break;
			case 'X':
			rule[i] = 1 - center;
			break;
		}
	}
	Board board(grid);
	vvi result = partial ? optimizePartial(board) : optimizeAll(board);
	vector<string> ret(rot ? C : R);
	for (int i = 0; i < R; ++i) {
		for (int j = 0; j < C; ++j) {
			(rot ? ret[j] : ret[i]).push_back((char)(result[i][j] + '0'));
		}
	}
	PRINT_TIMER();
	PRINT_COUNTER();
	return ret;
}

#ifndef SUBMIT
int main() {
	int R, N, K;
	cin >> R;
	vector<string> grid(R);
	for (int i = 0; i < R; ++i) {
		cin >> grid[i];
	}
	string rules;
	cin >> rules >> N >> K;
	CellularAutomaton obj;
	vector<string> ret = obj.configure(grid, rules, N, K);
	for (auto row : ret) {
		cout << row << endl;
	}
}
#endif
