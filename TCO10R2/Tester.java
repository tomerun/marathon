import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;

public class Tester {
	static final int[] dr = { 0, 1, 0, -1, 1, 1, -1, -1 }, dc = { 1, 0, -1, 0, 1, -1, -1, 1 };
	char[] rules;
	int R, C, K, N; //dimensions of the grid, number of iterations, number of cells to be changed
	char[][] grid; //actual cells of the grid - 0-1-?
	char[][] g;//cells of the grid during manual play and iterating (the ones shown on the image)

	void generate(long seed) {
		try {
			SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
			r1.setSeed(seed);
			R = r1.nextInt(91) + 10;
			C = r1.nextInt(91) + 10;
			if (seed == 1) R = C = 12;

			//override random values with manual settings 
			if (Rm > -1) R = Rm;
			if (Cm > -1) C = Cm;
			grid = new char[R][C];
			int i, j, r, c;
			int pal = r1.nextInt(5) + 3;
			for (r = 0; r < R; r++)
				for (c = 0; c < C; c++)
					grid[r][c] = (r1.nextInt(10) < pal ? '1' : '0');

			//generate the rules - CHECK FOR INTERESTING ONES
			rules = new char[9];
			rules[0] = rules[1] = rules[7] = rules[8] = '-';
			for (i = 2; i < 7; i++) {
				j = r1.nextInt(4);
				if (j == 0) rules[i] = '+'; //cell turns alive
				if (j == 1) rules[i] = '-'; //cell dies
				if (j == 2) rules[i] = '='; //cell keeps its state
				if (j == 3) rules[i] = 'X'; //cell switches its state
			}
			//manual setting
			if (seed == 1) { //rules string for life game
				for (i = 0; i < 9; i++)
					rules[i] = '-';
				rules[2] = '=';
				rules[3] = '+';
			}

			//generate number of cells to be changed and number of interations to be done
			N = r1.nextInt((R * C) / 16 - 4) + 5;
			//manual setting
			if (seed == 1) N = R * C / 16;
			if (Nm > -1) N = Nm;

			K = r1.nextInt(19) + 2;
			//manual setting
			if (seed == 1) K = 2;
			if (Km > -1) K = Km;
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results.");
			e.printStackTrace();
		}
	}

	public Result runTest(long seed) {
		Result res = new Result();
		try {
			generate(seed);
			res.seed = seed;
			res.R = R;
			res.C = C;
			res.K = K;
			res.N = N;
			res.rule = String.valueOf(rules);

			int i, j, k, l, n = 0;
			//copy grid to g
			g = new char[R][C];
			for (i = 0; i < R; i++)
				for (j = 0; j < C; j++)
					g[i][j] = grid[i][j];

			String gpar[] = new String[R];
			for (i = 0; i < R; i++)
				gpar[i] = String.valueOf(grid[i]);
			String[] ret = configure(gpar, res);

			//check the return for validity
			if (ret.length != R) {
				addFatalError("Your return must contain exactly R elements.");
				return res;
			}
			for (i = 0; i < R; i++) {
				if (ret[i].length() != C) {
					addFatalError("Each element of your return must contain exactly C characters.");
					return res;
				}
				for (j = 0; j < C; j++) {
					if (ret[i].charAt(j) != '0' && ret[i].charAt(j) != '1') {
						addFatalError("Each character in your return must be '0' or '1'.");
						return res;
					}
					if (ret[i].charAt(j) != grid[i][j]) n++; //number of changed cells
				}
			}
			if (n > N) {
				addFatalError("Your return must contain at most " + N + " changed cells, while it contained " + n + ".");
				return res;
			}
			res.changed = n;

			//copy the return to grid and g
			for (i = 0; i < R; i++)
				for (j = 0; j < C; j++)
					g[i][j] = grid[i][j] = ret[i].charAt(j);

			//iterate K times: grid is OLD values, and g is NEW values
			for (k = 0; k < K; k++) { //for each cell
				for (i = 0; i < R; i++)
					for (j = 0; j < C; j++) { //calculate number of live cells around it
						n = 0;
						for (l = 0; l < 8; l++)
							//note that the sides of the grid are connected
							if (grid[(i + dr[l] + R) % R][(j + dc[l] + C) % C] == '1') n++;
						//and decide its next value depending on rules[n]
						if (rules[n] == '=') {}
						if (rules[n] == '+') {
							g[i][j] = '1';
						}
						if (rules[n] == '-') {
							g[i][j] = '0';
						}
						if (rules[n] == 'X') {
							g[i][j] = (g[i][j] == '0' ? '1' : '0');
						}
					}

				//copy new values back to grid
				for (i = 0; i < R; i++)
					for (j = 0; j < C; j++)
						grid[i][j] = g[i][j];
			}

			//score is the percentage of live cells after K iterations
			n = 0;
			for (i = 0; i < R; i++)
				for (j = 0; j < C; j++)
					if (g[i][j] == '1') n++;
			res.score = n * 1. / (R * C);
			return res;
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results.");
			e.printStackTrace();
			return res;
		}
	}

	Process proc;
	InputStream is;
	OutputStream os;
	BufferedReader br;
	static int Rm, Cm, Nm, Km;

	String[] configure(String[] gridin, Result res) throws IOException {
		String[] ret = new String[R];
		StringBuffer sb = new StringBuffer();
		sb.append(R).append('\n');
		for (int i = 0; i < R; i++)
			sb.append(gridin[i]).append('\n');
		sb.append(String.valueOf(rules)).append('\n');
		sb.append(N).append('\n');
		sb.append(K).append('\n');
		long before = System.currentTimeMillis();
		os.write(sb.toString().getBytes());
		os.flush();
		for (int i = 0; i < R; i++)
			ret[i] = br.readLine();
		res.elapsed = System.currentTimeMillis() - before;
		return ret;
	}

	public Tester() {
		try {
			Runtime rt = Runtime.getRuntime();
			proc = rt.exec("./CellularAutomaton.o");
			os = proc.getOutputStream();
			is = proc.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));
			new ErrorReader(proc.getErrorStream()).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 1;
		long begin = -1;
		long end = -1;
		Rm = Cm = Nm = Km = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-R")) Rm = Integer.parseInt(args[++i]);
			if (args[i].equals("-C")) Cm = Integer.parseInt(args[++i]);
			if (args[i].equals("-K")) Km = Integer.parseInt(args[++i]);
			if (args[i].equals("-N")) Nm = Integer.parseInt(args[++i]);
		}
		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester f = new Tester();
			System.out.println(f.runTest(seed));
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}
}

class ErrorReader extends Thread {
	InputStream error;

	public ErrorReader(InputStream is) {
		error = is;
	}

	public void run() {
		try {
			byte[] ch = new byte[50000];
			int read;
			while ((read = error.read(ch)) > 0) {
				String s = new String(ch, 0, read);
				System.out.print(s);
				System.out.flush();
			}
		} catch (Exception e) {}
	}
}

class TestThread extends Thread {
	int begin, end;
	ArrayList<Long> seeds;
	Result[] results;

	TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
		this.begin = begin;
		this.end = end;
		this.seeds = seeds;
		this.results = results;
	}

	public void run() {
		for (int i = begin; i <= end; ++i) {
			Tester f = new Tester();
			try {
				Result res = f.runTest(seeds.get(i));
				results[i] = res;
			} catch (Exception e) {
				e.printStackTrace();
				results[i] = new Result();
				results[i].seed = seeds.get(i);
			}
		}
	}
}

class Result {
	int R, C, K, N, changed;
	long seed, elapsed;
	String rule;
	double score;

	public String toString() {
		String ret = String.format("seed:%4d\n", seed);
		ret += String.format("R:%3d  C:%3d  K:%2d  N:%3d\n", R, C, K, N);
		ret += String.format("rule:%s\n", rule);
		ret += String.format("changed:%d\n", changed);
		ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
		ret += String.format("score:%.4f", score);
		return ret;
	}
}