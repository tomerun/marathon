#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:changeProb, :S, :W, :L, :G, :patH, :patW, :elapsed, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /S: *(\d+) W:(\d+) L:(\d+) G:(\d+)/
      testcase.S = $1.to_i
      testcase.W = $2.to_i
      testcase.L = $3.to_i
      testcase.G = $4.to_i
    elsif line =~ /changeProb:(.+)$/
      testcase.changeProb = $1.to_f
    elsif line =~ /patternSize:(\d+) (\d+)/
      testcase.patH = $1.to_i
      testcase.patW = $2.to_i
    elsif line =~ /elapsed:(.+)/
      testcase.elapsed = $1.to_f
    elsif line =~ /score:(.+)/
      testcase.score = $1.to_f
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, filter)
  sum_score_diff = 0
  sum_score_diff_relative = 0
  sum_score = 0
  total = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    sum_score += s2
    sum_score_diff += s2 - s1
    total += 1
    if s1 > s2
      win += 1
      sum_score_diff_relative += s2 == 0 ? 1.0 : 1.0 - s2 / s1
    elsif s1 < s2
      lose += 1
      sum_score_diff_relative += s1 == 0 ? -1.0 : s1 / s2 - 1.0
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, total - win - lose)
  # puts sprintf("  diff_abs:%.6f", sum_score_diff / total)
  puts sprintf("  diff_rel:%.6f", sum_score_diff_relative / total)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  1.upto(10) do |w|
    puts "W:#{w}"
    calc(from, to, proc { |tc| tc[0].W == w })
  end
  puts "---------------------"
  0.05.step(0.20, 0.03) do |p|
    puts "Prob:#{p}-#{p+0.03}"
    calc(from, to, proc { |tc| p <= tc[0].changeProb && tc[0].changeProb < p + 0.03 })
  end
  puts "---------------------"
  50.step(450, 50) do |s|
    puts "S:#{s}-#{s+49}"
    calc(from, to, proc { |tc| s <= tc[0].S && tc[0].S < s + 50 })
  end
  puts "---------------------"
  patCnt = [1, 2, 4, 16, 64, 256, 1024, 500*500+1]
  0.upto(patCnt.size - 2) do |pi|
    puts "pattern count:#{patCnt[pi]}-#{patCnt[pi+1]-1}"
    calc(from, to, proc { |tc| 
      count = tc[0].S / tc[0].patH * tc[0].S / tc[0].patW
      patCnt[pi] <= count && count < patCnt[pi+1]
    })
  end
  puts "---------------------"
  puts "total"
  calc(from, to, proc { |tc| true })
end

main
