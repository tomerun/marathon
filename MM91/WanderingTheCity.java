import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

public class WanderingTheCity {

	private static final boolean DEBUG = false;
	private static final boolean MEASURE_TIME = false;
	Tester Actions; // for local test
	Timer timer = new Timer();

	long startTime = System.currentTimeMillis();
	int S, W, L, G;
	int countW, countL, countG, lookedCell;
	long[][] orig, looked, mask; // bitmap
	int[][] origMapV, origMapW;
	int[][] pattern;
	boolean[][] guessed;
	int cr, cc;
	int patH, patW, patR = -1, patC = -1;

	int whereAmI(String[] map, int W, int L, int G) {
		this.S = map.length;
		this.W = W;
		this.L = L;
		this.G = G;
		this.cr = this.cc = 1; // assume initial position as (1, 1) for simplicity
		orig = new long[S][(S + 63) / 64];
		origMapV = new int[S][S];
		for (int i = 0; i < S; ++i) {
			for (int j = 0; j < S; ++j) {
				if (map[i].charAt(j) == 'X') {
					orig[i][j / 64] |= 1L << (j % 64);
					origMapV[i][j] = 1;
				}
			}
		}
		looked = new long[S][(S + 63) / 64];
		mask = new long[S][(S + 63) / 64];
		guessed = new boolean[S][S];

		detectPattern();
		boolean result = solve();
		if (!result) {
			bruteforce();
		}
		debugf("looked:%d cell:%d guessed:%d\n", countL, lookedCell, countG);
		timer.print();
		return 0;
	}

	void detectPattern() {
		int[][] patSum = new int[S][S];
		double best = 1;
		for (int h = 1; h <= S; ++h) {
			if (S % h != 0) continue;
			int[][] rowSum = new int[h][S];
			for (int i = 0; i < S; ++i) {
				for (int j = 0; j < S; ++j) {
					rowSum[i % h][j] += origMapV[i][j];
				}
			}
			for (int w = 1; w <= S; ++w) {
				if (h == 1 && w == 1) continue;
				if (S % w != 0) continue;
				if (h == S && w == S) break;
				for (int i = 0; i < h; ++i) {
					for (int j = 0; j < w; ++j) {
						patSum[i][j] = 0;
					}
				}
				for (int i = 0; i < h; ++i) {
					for (int j = 0; j < S; ++j) {
						patSum[i][j % w] += rowSum[i][j];
					}
				}
				int repeat = S / h * S / w;
				double ave = 0;
				for (int i = 0; i < h; ++i) {
					for (int j = 0; j < w; ++j) {
						double blackRatio = 1.0 * patSum[i][j] / repeat;
						ave += Math.min(blackRatio, 1 - blackRatio);
					}
				}
				ave /= h * w;
				if (ave + 1e-3 < best) {
					best = ave;
					patH = h;
					patW = w;
				}
			}
		}
		if (best > 0.02) {
			best = 0;
			patH = patW = S;
		}
		int[][] patternCount = new int[patH][patW];
		pattern = new int[patH][patW];
		int repCount = S / patH * S / patW;
		for (int i = 0; i < patH; ++i) {
			for (int j = 0; j < patW; ++j) {
				for (int r = i; r < S; r += patH) {
					for (int c = j; c < S; c += patW) {
						patternCount[i][j] += origMapV[r][c];
					}
				}
				pattern[i][j] = patternCount[i][j] * 2 >= repCount ? 1 : 0;
			}
		}
		origMapW = new int[S][S];
		for (int i = 0; i < S; ++i) {
			for (int j = 0; j < S; ++j) {
				if (origMapV[i][j] != pattern[i % patH][j % patW]) {
					origMapW[i][j] = 20;
				} else if (origMapV[i][j] == 1) {
					origMapW[i][j] = 2;
				} else {
					origMapW[i][j] = 1;
				}
			}
		}
	}

	boolean solve() {
		int[][] penalty = new int[S][S];
		PenaStats stats = null;
		int bestR = -1;
		int bestC = -1;
		int bestCount = 0;
		final int guessThreshold = S * S / patH / patW > 1000 ? 4 : 3;
		for (int turn = 0;; ++turn) {
			timer.start(3);
			Point lookPoint;
			if (turn < 1) {
				lookPoint = new Point(cc + turn * 2, cr);
			} else {
				lookPoint = determineLookPosition(penalty, stats);
			}
			timer.stop(3);
			if (lookPoint == null) return false; // no effective point
			timer.start(4);
			walkAndLook(lookPoint, penalty);
			timer.stop(4);

			timer.start(5);
			stats = calcPenaStats(penalty);
			timer.stop(5);
			if (stats.minR == bestR && stats.minC == bestC) {
				++bestCount;
			} else {
				bestR = stats.minR;
				bestC = stats.minC;
				bestCount = 1;
			}
			if (stats.min + guessThreshold <= stats.min2 || bestCount >= 20 && stats.min + 3 <= stats.min2) {
				boolean result = guess(stats.minR, stats.minC);
				if (result) return true;
			}
			if (elapsed() > 5500) return false;
			if (countW > 16 * S * S - 2 * S) return false; // walk distance limit exceeded
			timer.start(6);
			if ((patH != S || patW != S) && patR == -1) {
				if (determinePositionInPattern(penalty, stats)) {
					//
				}
			}
			timer.stop(6);
		}
	}

	Point determineLookPosition(int[][] pena, PenaStats stats) {
		final int MAX_CAND_SIZE = 20;
		ArrayList<Integer> candPos = new ArrayList<>();
		candPos.add((stats.minR << 10) | stats.minC);
		if (stats.min != stats.min2) {
			candPos.add((stats.minR << 10) | stats.minC);
			OUT: for (int i = 0; i < S; ++i) {
				for (int j = 0; j < S; ++j) {
					if (pena[i][j] == stats.min2) {
						candPos.add((i << 10) | j);
						if (candPos.size() >= MAX_CAND_SIZE) {
							break OUT;
						}
					}
				}
			}
		} else {
			OUT: for (int i = 0; i < S; ++i) {
				for (int j = 0; j < S; ++j) {
					if (pena[i][j] == stats.min) {
						candPos.add((i << 10) | j);
						if (candPos.size() >= 20) {
							break OUT;
						}
					}
				}
			}
		}
		int maxInfo = 0;
		int minDist = 0;
		int lookR = -1;
		int lookC = -1;
		final int RANGE = (patH == S && patW == S ? 5 : (W == 1 ? 30 : 20));
		int width = 0;
		if (candPos.size() <= 5) {
			for (int di = -RANGE; di <= RANGE; ++di) {
				int i = (cr + di + S) % S;
				int pi = i == 0 ? S - 1 : i - 1;
				for (int dj = -width; dj <= width; ++dj) {
					int j = (cc + dj + S) % S;
					int pj = j == 0 ? S - 1 : j - 1;
					int info = 0;
					for (int k = 0; k < candPos.size(); ++k) {
						int r = ((candPos.get(k) >> 10) + i) % S;
						int pr = r == 0 ? S - 1 : r - 1;
						int c = ((candPos.get(k) & 0x3FF) + j) % S;
						int pc = c == 0 ? S - 1 : c - 1;
						if (!isLooked(i, j)) info += origMapW[r][c];
						if (!isLooked(i, pj)) info += origMapW[r][pc];
						if (!isLooked(pi, j)) info += origMapW[pr][c];
						if (!isLooked(pi, pj)) info += origMapW[pr][pc];
					}
					int dist = distance(cr, cc, i, j);
					if (info > maxInfo || info == maxInfo && dist < minDist) {
						maxInfo = info;
						minDist = dist;
						lookR = i;
						lookC = j;
					}
				}
				if (di < 0) {
					++width;
				} else {
					--width;
				}
			}
		} else {
			for (int di = -RANGE; di <= RANGE; ++di) {
				int i = (cr + di + S) % S;
				int pi = i == 0 ? S - 1 : i - 1;
				for (int dj = -width; dj <= width; ++dj) {
					int j = (cc + dj + S) % S;
					int pj = j == 0 ? S - 1 : j - 1;
					int info = 0;
					double[] blackCount = new double[4];
					for (int k = 0; k < candPos.size(); ++k) {
						int r = ((candPos.get(k) >> 10) + i) % S;
						int pr = r == 0 ? S - 1 : r - 1;
						int c = ((candPos.get(k) & 0x3FF) + j) % S;
						int pc = c == 0 ? S - 1 : c - 1;
						if (!isLooked(i, j)) {
							info += origMapW[r][c];
							blackCount[0] += origMapV[r][c];
						}
						if (!isLooked(i, pj)) {
							info += origMapW[r][pc];
							blackCount[1] += origMapV[r][pc];
						}
						if (!isLooked(pi, j)) {
							info += origMapW[pr][c];
							blackCount[2] += origMapV[pr][c];
						}
						if (!isLooked(pi, pj)) {
							info += origMapW[pr][pc];
							blackCount[3] += origMapV[pr][pc];
						}
					}
					for (int k = 0; k < 4; ++k) {
						blackCount[k] /= candPos.size();
						info += (int) (Math.ceil(Math.min(blackCount[k], 1 - blackCount[k]) * 5 * candPos.size()));
					}
					int dist = distance(cr, cc, i, j);
					if (info > maxInfo || info == maxInfo && dist < minDist) {
						maxInfo = info;
						minDist = dist;
						lookR = i;
						lookC = j;
					}
				}
				if (di < 0) {
					++width;
				} else {
					--width;
				}
			}
		}
		if (!(patH == S && patW == S) && maxInfo <= 10) {
			maxInfo = 0;
			minDist = 0;
			for (int i = 0; i < S; ++i) {
				int r = (stats.minR + i) % S;
				int pr = r == 0 ? S - 1 : r - 1;
				int pi = i == 0 ? S - 1 : i - 1;
				for (int j = 0; j < S; ++j) {
					int c = (stats.minC + j) % S;
					int pc = c == 0 ? S - 1 : c - 1;
					int pj = j == 0 ? S - 1 : j - 1;
					int info = 0;
					if (!isLooked(i, j)) info += origMapW[r][c];
					if (!isLooked(i, pj)) info += origMapW[r][pc];
					if (!isLooked(pi, j)) info += origMapW[pr][c];
					if (!isLooked(pi, pj)) info += origMapW[pr][pc];
					int dist = distance(cr, cc, i, j);
					if (info > maxInfo || info == maxInfo && dist < minDist) {
						maxInfo = info;
						minDist = dist;
						lookR = i;
						lookC = j;
					}
				}
			}
		}
		debug("candPos size:" + candPos.size() + " maxInfo:" + maxInfo + " look:(" + lookR + "," + lookC + ")");
		if (maxInfo == 0) return null;
		return new Point(lookC, lookR);
	}

	boolean[] alreadyLooked = new boolean[4];
	int[] lookedLatest = new int[4];

	void walkAndLook(Point destination, int[][] pena) {
		final int lookR = destination.y;
		final int lookC = destination.x;
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 2; ++j) {
				alreadyLooked[i * 2 + j] = isLooked((lookR + S - 1 + i) % S, (lookC + S - 1 + j) % S);
			}
		}
		walkTo(lookR, lookC);
		look();
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 2; ++j) {
				lookedLatest[i * 2 + j] = getLookedValue((lookR + S - 1 + i) % S, (lookC + S - 1 + j) % S);
			}
		}
		int r = lookR;
		for (int i = 0; i < S; ++i) {
			int pr = r == 0 ? S - 1 : r - 1;
			int c = lookC;
			for (int j = 0; j < S; ++j) {
				int pc = c == 0 ? S - 1 : c - 1;
				if (!alreadyLooked[0]) pena[i][j] += lookedLatest[0] ^ origMapV[pr][pc];
				if (!alreadyLooked[1]) pena[i][j] += lookedLatest[1] ^ origMapV[pr][c];
				if (!alreadyLooked[2]) pena[i][j] += lookedLatest[2] ^ origMapV[r][pc];
				if (!alreadyLooked[3]) pena[i][j] += lookedLatest[3] ^ origMapV[r][c];
				c = c == S - 1 ? 0 : c + 1;
			}
			r = r == S - 1 ? 0 : r + 1;
		}
	}

	boolean determinePositionInPattern(int[][] pena, PenaStats stats) {
		final int candR = stats.minR % patH;
		final int candC = stats.minC % patW;
		final int threshold = 3;
		int min = 1 << 30;
		int max = 0;
		int im = 0;
		OUT: for (int i = 0; i < S; ++i) {
			int jm = 0;
			for (int j = 0; j < S; ++j) {
				if (im == candR && jm == candC) {
					max = Math.max(max, pena[i][j]);
				} else {
					min = Math.min(min, pena[i][j]);
				}
				if (max + threshold >= min) break OUT;
				jm = jm == patW - 1 ? 0 : jm + 1;
			}
			im = im == patH - 1 ? 0 : im + 1;
		}
		debug("max:" + max + " min:" + min + " (" + candR + "," + candC + ")");
		if (max + threshold < min) {
			patR = candR;
			patC = candC;
			return true;
		}
		return false;
	}

	void bruteforce() {
		long[] candidates = new long[S * S];
		final int width = orig[0].length;
		long[][] origFront = new long[2 * S][width];
		for (int i = 0; i < S; ++i) {
			origFront[i] = origFront[S + i] = orig[i];
		}
		for (int sc = 0; sc < S; ++sc) {
			timer.start(0);
			for (int sr = 0; sr < S; ++sr) {
				int sum = 0;
				for (int i = 0; i < S; ++i) {
					for (int j = 0; j < width; ++j) {
						sum += Long.bitCount((looked[i][j] ^ origFront[sr + i][j]) & mask[i][j]);
					}
				}
				candidates[sc * S + sr] = ((long) sum << 32) | (sr << 16) | sc;
			}
			timer.stop(0);
			for (int i = 0; i < S; ++i) {
				long carry = orig[i][0] & 1;
				for (int j = 0; j < width - 1; ++j) {
					orig[i][j] = (orig[i][j] >>> 1) | ((orig[i][j + 1] & 1) << 63);
				}
				orig[i][width - 1] = (orig[i][width - 1] >>> 1) | (carry << ((S - 1) % 64));
			}
		}
		Arrays.sort(candidates);
		for (int i = 0;; ++i) {
			int qr = (int) ((candidates[i] >> 16) & 0xFFFF);
			int qc = (int) ((candidates[i] >> 0) & 0xFFFF);
			if (guess(qr, qc)) {
				break;
			}
		}
	}

	PenaStats calcPenaStats(int[][] pena) {
		final int h = pena.length;
		final int w = pena[0].length;
		PenaStats stats = new PenaStats();
		if (DEBUG) stats.minPos = new long[h * w];
		for (int r = 0; r < h; ++r) {
			for (int c = 0; c < w; ++c) {
				if (DEBUG) stats.minPos[r * w + c] = ((long) pena[r][c] << 32) | (r << 16) | c;
				if (pena[r][c] < stats.min) {
					stats.min2 = stats.min;
					stats.min2R = stats.minR;
					stats.min2C = stats.minC;
					stats.min = pena[r][c];
					stats.minR = r;
					stats.minC = c;
				} else if (pena[r][c] < stats.min2) {
					stats.min2 = pena[r][c];
					stats.min2R = r;
					stats.min2C = c;
				}
			}
		}
		if (DEBUG) {
			Arrays.sort(stats.minPos);
			for (int i = 0; i < 10; ++i) {
				debugf("(%3d,%3d) %d ", (stats.minPos[i] >> 16) & 0xFFFF, stats.minPos[i] & 0xFFFF, stats.minPos[i] >> 32);
			}
			debug();
		}
		//		debugf("(%3d,%3d) (%3d,%3d) %d %d\n", stats.minR, stats.minC, stats.min2R, stats.min2C, stats.min, stats.min2);
		return stats;
	}

	static class PenaStats {
		int min = Integer.MAX_VALUE;
		int min2 = Integer.MAX_VALUE;
		int minR, minC, min2R, min2C;
		long[] minPos;
	}

	boolean isLooked(int r, int c) {
		return ((mask[r][c / 64] >> (c % 64)) & 1) != 0;
	}

	int getLookedValue(int r, int c) {
		return (int) (looked[r][c / 64] >> (c % 64)) & 1;
	}

	int distance(int r1, int c1, int r2, int c2) {
		return distance(r1, r2) + distance(c1, c2);
	}

	int distance(int p1, int p2) {
		int d = Math.abs(p1 - p2);
		return Math.min(d, S - d);
	}

	void walk(int dr, int dc) {
		countW += Math.abs(dr) + Math.abs(dc);
		Actions.walk(new int[] { dr, dc });
		cr = (cr + S + dr) % S;
		cc = (cc + S + dc) % S;
	}

	void walkTo(int r, int c) {
		walk(getMove(cr, r), getMove(cc, c));
		assert (cr == r && cc == c);
	}

	int getMove(int from, int to) {
		int forward = from <= to ? to - from : to + S - from;
		int backward = from <= to ? to - S - from : to - from;
		return forward < -backward ? forward : backward;
	}

	void look() {
		++countL;
		String[] res = Actions.look();
		for (int i = 0; i < 2; ++i) {
			final int br = (cr + S - 1 + i) % S;
			for (int j = 0; j < 2; ++j) {
				final int bc = (cc + S - 1 + j) % S;
				if (isLooked(br, bc)) continue;
				++lookedCell;
				mask[br][bc / 64] |= 1L << (bc % 64);
				if (res[i].charAt(j) == 'X') looked[br][bc / 64] |= 1L << (bc % 64);
			}
		}
	}

	boolean guess(int r, int c) {
		r = (r + 1) % S; // adjust initial shift
		c = (c + 1) % S;
		if (guessed[r][c]) return false;
		guessed[r][c] = true;
		++countG;
		int result = Actions.guess(new int[] { r, c });
		if (result == 0) {
			debug("guess failed:(" + r + "," + c + ")");
		} else {
			debug("guess succeeded:(" + r + "," + c + ")");
		}
		return result == 1;
	}

	long elapsed() {
		return System.currentTimeMillis() - startTime;
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debugf(String str, Object... obj) {
		if (DEBUG) System.err.printf(str, obj);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(obj.length == 0 ? "" : Arrays.deepToString(obj));
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}

	}
}
