import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.security.SecureRandom;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Tester {
	static final int minSz = 50, maxSz = 500;
	static final double minProb = 0.05, maxProb = 0.5;
	static final double minChangeProb = 0.05, maxChangeProb = 0.2;

	int S, W, L, G;
	static int myS = -1, myW = -1, myL = -1, myG = -1;
	int patternH, patternW;
	char[][] cityMapOld;
	char[][] cityMap;
	int[] startPos;
	double changeProb;
	int maxLook, maxGuess, maxTotalWalk;
	volatile int nLook, nGuess, nCorGuess, totalWalked;
	volatile boolean correct;
	volatile int[] curPos;
	int[][] pattern;

	void generate(long seed) throws Exception {
		SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
		r1.setSeed(seed);
		S = r1.nextInt(maxSz - minSz + 1) + minSz;
		double blackProb = r1.nextDouble() * (maxProb - minProb) + minProb;
		changeProb = r1.nextDouble() * (maxChangeProb - minChangeProb) + minChangeProb;
		final double mixProb = 0.01;

		if (seed <= 3) {
			S = (int) seed * 20;
			blackProb = maxProb - minProb * 2 * seed;
		} else if (seed == 4) {
			S = maxSz;
			blackProb = minProb;
		} else if (1000 <= seed && seed < 2000) {
			changeProb = minChangeProb + (maxChangeProb - minChangeProb) * (seed - 1000) / 999;
		}
		if (myS > 0) S = myS;

		cityMapOld = new char[S][S];
		cityMap = new char[S][S];
		int[] reps = new int[S];
		int repCnt = 0;
		for (int i = 2; i <= S; i++) {
			if ((S % i) == 0) {
				reps[repCnt++] = i;
			}
		}

		int nB = 0;
		do {
			patternH = reps[r1.nextInt(repCnt)];
			patternW = reps[r1.nextInt(repCnt)];
			pattern = new int[patternH][patternW];
			for (int i = 0; i < patternH; i++)
				for (int j = 0; j < patternW; j++) {
					cityMap[i][j] = r1.nextDouble() < blackProb ? 'X' : '.';
					pattern[i][j] = cityMap[i][j] == 'X' ? 1 : 0;
				}
			for (int i = 0; i < S; i++)
				for (int j = 0; j < S; j++) {
					cityMap[i][j] = cityMap[i % patternH][j % patternW];
				}
			for (int i = 0; i < S; i++)
				for (int j = 0; j < S; j++) {
					if (r1.nextDouble() < mixProb) {
						cityMap[i][j] = r1.nextDouble() < blackProb ? 'X' : '.';
					}
					if (cityMap[i][j] == 'X') nB++;
				}
		} while (nB == 0 && nB != S * S);

		for (int i = 0; i < S; i++)
			for (int j = 0; j < S; j++) {
				cityMapOld[i][j] = cityMap[i][j];
			}

		int nChange = 0;
		for (int i = 0; i < S; i++)
			for (int j = 0; j < S; j++) {
				if (r1.nextDouble() < changeProb) {
					cityMap[i][j] = (cityMap[i][j] == '.' ? 'X' : '.');
					++nChange;
				}
			}

		startPos = new int[2];
		for (int i = 0; i < 2; ++i)
			startPos[i] = r1.nextInt(S);
		W = r1.nextInt(10) + 1;
		L = r1.nextInt(S / 2) + S / 2;
		G = r1.nextInt(S * S / 2) + S * S / 2;
		if (myW > 0) W = myW;
		if (myL > 0) L = myL;
		if (myG > 0) G = myG;

		maxGuess = maxLook = S * S;
		maxTotalWalk = 16 * S * S;
		if (debug) {
			System.out.println("S=" + S + " S^2=" + (S * S));
			System.out.println("Probability of black building = " + blackProb);
			System.out.println("Starting position: (" + startPos[0] + "," + startPos[1] + ")");
			//			System.out.println("Old map:");
			//			for (int i = 0; i < S; i++)
			//				System.out.println(new String(cityMapOld[i]));
			System.out.println("Changed cells = " + nChange);
			System.out.println("start pos in pattern = (" + (startPos[0] + S - 1) % patternH + "," + (startPos[1] + S - 1) % patternW + ")");
			System.out.println();
		}
	}

	boolean isMonotonous() {
		for (int i = 0; i < patternH; ++i) {
			for (int j = 0; j < patternW; ++j) {
				if (pattern[i][j] != pattern[0][0]) return false;
			}
		}
		return true;
	}

	boolean isChecker() {
		for (int i = 0; i < patternH; ++i) {
			for (int j = 0; j < patternW; ++j) {
				if ((pattern[i][j] ^ ((i + j) & 1)) != pattern[0][0]) return false;
			}
		}
		return true;
	}

	public String[] look() {
		nLook++;
		if (nLook > maxLook) {
			throw new RuntimeException("You can do at most " + maxLook + " look() calls.");
		}
		char[][] seen = new char[2][2];
		for (int i = 0; i < 2; ++i)
			for (int j = 0; j < 2; ++j) {
				seen[i][j] = cityMap[(curPos[0] + S + i - 1) % S][(curPos[1] + S + j - 1) % S];
				if (vis) seenVis[(curPos[0] + S + i - 1) % S][(curPos[1] + S + j - 1) % S] = true;
			}
		draw();
		return new String[] { String.valueOf(seen[0]), String.valueOf(seen[1]) };
	}

	boolean validShift(int shift) {
		if (shift <= -S || shift >= S) {
			addFatalError("Value of shift (" + shift + ") must be between " + (-S + 1) + " and " + (S - 1) + ", inclusive.");
			return false;
		}
		return true;
	}

	int applyShift(int cur, int shift) {
		return (cur + S + shift) % S;
	}

	public int walk(int[] shift) {
		if (shift == null || shift.length != 2) {
			throw new RuntimeException("Shift must have exactly two elements");
		}
		for (int i = 0; i < 2; ++i)
			if (!validShift(shift[i])) return -1;
		for (int i = 0; i < 2; ++i)
			curPos[i] = applyShift(curPos[i], shift[i]);
		totalWalked += Math.abs(shift[0]) + Math.abs(shift[1]);
		if (totalWalked > maxTotalWalk) {
			throw new RuntimeException("You can walk at most " + maxTotalWalk + " distance.");
		}
		return 0;
	}

	boolean validCoord(int coord) {
		if (coord < 0 || coord >= S) {
			addFatalError("Value of coordinate (" + coord + ") must be between 0 and " + (S - 1) + ", inclusive.");
			return false;
		}
		return true;
	}

	public int guess(int[] coord) {
		if (coord == null || coord.length != 2) {
			throw new RuntimeException("Coord must have exactly two elements");
		}
		for (int i = 0; i < 2; ++i)
			if (!validCoord(coord[i])) return -1;
		nGuess++;
		if (nGuess > maxGuess) {
			throw new RuntimeException("You can do at most " + maxGuess + " guess() calls.");
		}
		boolean res = (startPos[0] == coord[0] && startPos[1] == coord[1]);
		if (res) nCorGuess++;
		correct |= res;
		if (vis) guessedVis[coord[0]][coord[1]] = true;
		return res ? 1 : 0;
	}

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		try {
			generate(seed);
			res.S = S;
			res.W = W;
			res.L = L;
			res.G = G;
			res.changeProb = changeProb;
			res.patternH = patternH;
			res.patternW = patternW;
			res.monotone = isMonotonous();
			res.checker = isChecker();
			curPos = new int[] { startPos[0], startPos[1] };
			totalWalked = 0;
			nLook = 0;
			nGuess = 0;
			nCorGuess = 0;
			correct = false;

			if (vis) {
				int wv = (twomaps ? S * 2 + 2 : S) * SZ + 20;
				int hv = S * SZ + 40;
				seenVis = new boolean[S][S];
				guessedVis = new boolean[S][S];
				jf.setSize(wv, hv);
				jf.setVisible(true);
				draw();
			}

			String[] cityMapStr = new String[S];
			for (int i = 0; i < S; ++i)
				cityMapStr[i] = new String(cityMapOld[i]);
			long startTime = System.currentTimeMillis();
			{
				WanderingTheCity obj = new WanderingTheCity();
				obj.Actions = this;
				obj.whereAmI(cityMapStr, W, L, G);
//				if (obj.patR != -1 && (obj.patR != (startPos[0] + S - 1) % patternH || obj.patC != (startPos[1] + S - 1) % patternW)) {
//					System.err.println("seed:" + seed + " expected:(" + (startPos[0] + S - 1) % patternH + "," + (startPos[1] + S - 1) % patternW
//							+ ") actual:(" + obj.patR + "," + obj.patC + ")");
//				}
			}
			res.elapsed = System.currentTimeMillis() - startTime;
			res.scoreW = W * totalWalked;
			res.scoreL = L * nLook;
			res.scoreG = (long) G * (nGuess - nCorGuess);
			res.score = res.scoreW + res.scoreL + res.scoreG;
			if (!correct) {
				res.score = -1;
				return res;
			}
			if (debug) {
				addFatalError("Total distance walked = " + totalWalked);
				addFatalError("Number of look() calls = " + nLook);
				addFatalError("Number of incorrect guess() calls = " + (nGuess - nCorGuess));
			}
			return res;
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results.");
			e.printStackTrace();
			return res;
		}
	}

	JFrame jf;
	Vis v;
	static boolean debug;
	static boolean vis = false;
	static boolean twomaps = true;
	static int del = 20;
	static int SZ = 5;
	volatile boolean ready;
	volatile boolean[][] seenVis;
	volatile boolean[][] guessedVis;

	void draw() {
		if (!vis) return;
		v.repaint();
		try {
			Thread.sleep(del);
		} catch (Exception e) {}
	}

	BufferedImage DrawOldMap() {
		BufferedImage oldMap = new BufferedImage(S * SZ + 1, S * SZ + 1, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = (Graphics2D) oldMap.getGraphics();
		// buildings on the map
		for (int i = 0; i < S; ++i)
			for (int j = 0; j < S; ++j) {
				g2.setColor(new Color(cityMapOld[i][j] == 'X' ? 0x444444 : 0xDDDDDD));
				g2.fillRect(j * SZ, i * SZ, SZ, SZ);
			}
		// lines between buildings for streets
		g2.setColor(new Color(0xAAAAAA));
		for (int i = 0; i <= S; i++)
			g2.drawLine(0, i * SZ, S * SZ, i * SZ);
		for (int i = 0; i <= S; i++)
			g2.drawLine(i * SZ, 0, i * SZ, S * SZ);
		g2.setColor(Color.RED);
		g2.drawRect(0, 0, patternW * SZ, patternH * SZ);
		return oldMap;
	}

	static BufferedImage deepCopy(BufferedImage source) {
		BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
		Graphics g = b.getGraphics();
		g.drawImage(source, 0, 0, null);
		g.dispose();
		return b;
	}

	public class Vis extends JPanel {
		BufferedImage oldMap;

		public void paint(Graphics g) {
			// draw the given (old) map once (and cache it)
			if (oldMap == null) oldMap = DrawOldMap();
			BufferedImage bi = deepCopy(oldMap);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();
			if (!twomaps) {
				// overlay seen parts of the map, using real black and white and with a border:
				// red for cells which differ from map and green for cells which match
				for (int i = 0; i < S; ++i)
					for (int j = 0; j < S; ++j)
						if (seenVis[i][j]) {
							// border
							g2.setColor(cityMap[i][j] == cityMapOld[i][j] ? Color.GREEN : Color.RED);
							g2.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
							// actual color of the building
							g2.setColor(cityMap[i][j] == 'X' ? Color.BLACK : Color.WHITE);
							g2.fillRect(j * SZ + 2, i * SZ + 2, SZ - 3, SZ - 3);
						}
			}

			// mark positions which have been guessed incorrectly
			g2.setStroke(new BasicStroke(2.0f));
			for (int i = 0; i < S; ++i)
				for (int j = 0; j < S; ++j)
					if (guessedVis[i][j]) {
						g2.setColor(i == startPos[0] && j == startPos[1] ? Color.GREEN : Color.RED);
						g2.drawLine(j * SZ - 3, i * SZ - 3, j * SZ + 3, i * SZ + 3);
						g2.drawLine(j * SZ - 3, i * SZ + 3, j * SZ + 3, i * SZ - 3);
					}
			g.drawImage(bi, 0, 0, S * SZ + 1, S * SZ + 1, null);

			if (twomaps) {
				// draw currently seen parts of the map using fog of war - only show the cells observed
				BufferedImage bi2 = new BufferedImage(S * SZ + 1, S * SZ + 1, BufferedImage.TYPE_INT_RGB);
				Graphics2D g22 = (Graphics2D) bi2.getGraphics();
				g22.setColor(new Color(0xAAAAAA));
				g22.fillRect(0, 0, S * SZ, S * SZ);
				// buildings on the map
				for (int i = 0; i < S; ++i)
					for (int j = 0; j < S; ++j)
						if (seenVis[i][j]) {
							g22.setColor(cityMap[i][j] == 'X' ? Color.BLACK : Color.WHITE);
							g22.fillRect(j * SZ, i * SZ, SZ, SZ);
						}
				// lines between buildings for streets
				g22.setColor(new Color(0xAAAAAA));
				for (int i = 0; i <= S; i++)
					g22.drawLine(0, i * SZ, S * SZ, i * SZ);
				for (int i = 0; i <= S; i++)
					g22.drawLine(i * SZ, 0, i * SZ, S * SZ);

				g.drawImage(bi2, (S + 2) * SZ, 0, S * SZ + 1, S * SZ + 1, null);
			}
		}

		public Vis() {
			jf.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
		}
	}

	public Tester() {
		try {
			if (vis) {
				jf = new JFrame();
				v = new Vis();
				jf.getContentPane().add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final int THREAD_COUNT = 4;

	public static void main(String[] args) throws Exception {
		long seed = 1, begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-delay")) del = Integer.parseInt(args[++i]);
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
			if (args[i].equals("-debug")) debug = true;
			if (args[i].equals("-onemaps")) twomaps = false;
			if (args[i].equals("-S")) myS = Integer.parseInt(args[++i]);
			if (args[i].equals("-W")) myW = Integer.parseInt(args[++i]);
			if (args[i].equals("-L")) myL = Integer.parseInt(args[++i]);
			if (args[i].equals("-G")) myG = Integer.parseInt(args[++i]);
		}
		if (begin != -1 && end != -1) {
			vis = false;
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int S, W, L, G;
		int patternH, patternW;
		boolean monotone, checker;
		double changeProb;
		long score, scoreW, scoreL, scoreG;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("S:%3d W:%d L:%d G:%d\n", S, W, L, G);
			ret += String.format("changeProb:%.4f\n", changeProb);
			ret += String.format("patternSize:%d %d %s\n", patternH, patternW, monotone ? "mono" : (checker ? "checker" : ""));
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("score:%d\n", score);
			ret += String.format("scoreW:%d scoreL:%d scoreG:%d\n", scoreW, scoreL, scoreG);
			ret += String.format("ratioW:%.4f ratioL:%.4f ratioG:%.4f", 1.0 * scoreW / score, 1.0 * scoreL / score, 1.0 * scoreG / score);
			return ret;
		}
	}
}
