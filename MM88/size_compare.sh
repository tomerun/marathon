#!/bin/sh

for i in 0 1 2 3 4 5 6 7 8
do
	echo -----1"$i"00-1"$i"99-----
	./compare.rb $1 $2 1"$i"00 1"$i"99 | tail -n 5
	echo
done
echo -----total-----
./compare.rb $1 $2 | tail -n 5
