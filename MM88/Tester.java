import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Tester {

	private final int MAX_TIME = 10000;
	Random r;
	int[][] virus;
	double[][] med;
	static int userH, userW, userK, userS;
	static double userP;
	int medStrength;
	int killTime;
	double spreadProb;
	int medCount = 0;
	int timeCount = 0;
	int endTime = -1;
	int width, height;
	ArrayList<TimeFrame> frames = new ArrayList<TimeFrame>();

	private String[] getStatus() {
		String[] ret = new String[virus.length];
		for (int i = 0; i < virus.length; i++) {
			ret[i] = "";
			for (int j = 0; j < virus[i].length; j++)
				ret[i] += virus[i][j] > 0 ? 'V' : (virus[i][j] < 0 ? 'X' : 'C');
		}
		return ret;
	}

	private void generateTestCase(long seed) {
		r = new Random(seed);
		medStrength = r.nextInt(91) + 10;
		killTime = r.nextInt(10) + 1;
		spreadProb = r.nextDouble() * 0.75 + 0.25;
		height = r.nextInt(86) + 15;
		width = r.nextInt(86) + 15;
		if (seed < 5) {
			width = height = 10 + 5 * (int) seed;
		}
		if (userH != 0) height = userH;
		if (userW != 0) width = userW;
		if (userK != 0) killTime = userK;
		if (userS != 0) medStrength = userS;
		if (userP != 0) spreadProb = userP;
		if (1000 <= seed && seed < 1900) {
			medStrength = 10 + (int) ((seed - 1000) / 10);
			spreadProb = 0.25 + 0.75 * (seed % 10 + 0.5) / 10;
		}
		virus = new int[height][width];
		med = new double[height][width];
		int numVirus = killTime + r.nextInt(height * width / 10);
		int numDead = r.nextInt(height * width / 10);
		while (numDead > 0) {
			int y = r.nextInt(height);
			int x = r.nextInt(width);
			if (virus[y][x] != 0) continue;
			virus[y][x] = -1;
			numDead--;
		}
		while (numVirus > 0) {
			int y = r.nextInt(height);
			int x = r.nextInt(width);
			if (virus[y][x] != 0) continue;
			virus[y][x] = killTime;
			numVirus--;
		}
		r = new Random(seed ^ 987654321987654321L);
		if (debug) {
			String[] t = getStatus();
			String ret = "Med Strength = " + medStrength;
			ret += "\nKill Time = " + killTime + "\nSpread Prob = " + spreadProb + "\n\n";
			for (int i = 0; i < t.length; i++)
				ret += t[i] + "\n";
			System.out.println(ret);
		}
	}

	public int addMed(int x, int y) {
		if (x < 0 || y < 0 || x >= med[0].length || y >= med.length) {
			throw new RuntimeException("addMed() called with invalid parameters");
		}
		if (timeCount >= MAX_TIME) {
			throw new RuntimeException("addMed() called after max time exceeded.");
		}
		med[y][x] += medStrength;
		medCount++;
		incrementTime();
		return 0;
	}

	public String[] observe() {
		String[] ret = getStatus();
		incrementTime();
		return ret;
	}

	public int waitTime(int units) {
		if (units < 1 || timeCount + units > MAX_TIME) {
			throw new RuntimeException("waitTime() called with invalid parameters");
		}
		for (int i = 0; i < units && timeCount < MAX_TIME; i++)
			incrementTime();
		return 0;
	}

	private void addTimeFrame() {
		if (vis) {
			TimeFrame frame = new TimeFrame();
			frame.virus = new int[height][width];
			frame.med = new double[height][width];
			for (int i = 0; i < height; ++i) {
				frame.virus[i] = virus[i].clone();
				frame.med[i] = med[i].clone();
			}
			for (int i = 0; i < height; ++i) {
				for (int j = 0; j < width; ++j) {
					if (virus[i][j] < 0) ++frame.dc;
					if (virus[i][j] > 0) ++frame.vc;
				}
			}
			frames.add(frame);
		}
	}

	private void incrementTime() {
		processMeds();
		addTimeFrame();
		processViruses();
		addTimeFrame();
		diffuse();
		addTimeFrame();
		timeCount++;
		if (doneSim() && endTime < 0) {
			endTime = timeCount;
		}
		if (debug) {
			System.out.println("Elapsed time: " + timeCount);
		}
	}

	private void processMeds() {
		for (int y = 0; y < virus.length; y++)
			for (int x = 0; x < virus[y].length; x++)
				if (virus[y][x] > 0 && med[y][x] >= 1.0) virus[y][x] = 0;
	}

	private void processViruses() {
		for (int y = 0; y < virus.length; y++)
			for (int x = 0; x < virus[y].length; x++) {
				if (virus[y][x] <= 0) continue;
				virus[y][x]--;
				if (virus[y][x] == 0) {
					virus[y][x] = -2;
				}
			}
		for (int y = 0; y < virus.length; y++)
			for (int x = 0; x < virus[y].length; x++) {
				if (virus[y][x] == -2) {
					virus[y][x] = -1;
					spreadVirus(x, y);
				}
			}
	}

	private void spreadVirus(int x, int y) {
		if (x > 0 && virus[y][x - 1] == 0 && r.nextDouble() < spreadProb) virus[y][x - 1] = killTime;
		if (x < virus[y].length - 1 && virus[y][x + 1] == 0 && r.nextDouble() < spreadProb) virus[y][x + 1] = killTime;
		if (y > 0 && virus[y - 1][x] == 0 && r.nextDouble() < spreadProb) virus[y - 1][x] = killTime;
		if (y < virus.length - 1 && virus[y + 1][x] == 0 && r.nextDouble() < spreadProb) virus[y + 1][x] = killTime;
	}

	private void diffuse() {
		double[][] diff = new double[med.length][med[0].length];
		for (int y = 0; y < med.length; y++)
			for (int x = 0; x < med[y].length; x++) {
				if (x > 0) {
					diff[y][x - 1] += (med[y][x] - med[y][x - 1]) * 0.2;
					diff[y][x] += (med[y][x - 1] - med[y][x]) * 0.2;
				}
				if (y > 0) {
					diff[y - 1][x] += (med[y][x] - med[y - 1][x]) * 0.2;
					diff[y][x] += (med[y - 1][x] - med[y][x]) * 0.2;
				}
			}
		for (int y = 0; y < med.length; y++)
			for (int x = 0; x < med[y].length; x++)
				med[y][x] += diff[y][x];
	}

	private boolean doneSim() {
		if (timeCount >= MAX_TIME) return true;
		for (int y = 0; y < med.length; y++)
			for (int x = 0; x < med[y].length; x++)
				if (virus[y][x] > 0) return false;
		return true;
	}

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		try {
			generateTestCase(seed);
			res.H = virus.length;
			res.W = virus[0].length;
			res.medStrength = medStrength;
			res.killTime = killTime;
			res.spreadProb = spreadProb;
			for (int i = 0; i < height; ++i) {
				for (int j = 0; j < width; ++j) {
					if (virus[i][j] > 0) ++res.initialVirusCount;
				}
			}
			addTimeFrame();

			long beforeTime = System.currentTimeMillis();
			ViralInfection obj = new ViralInfection();
			obj.Research = this;
			int ret = obj.runSim(getStatus(), medStrength, killTime, spreadProb);
			res.elapsed = System.currentTimeMillis() - beforeTime;
			if (ret == -1) {
				addFatalError("One or more issued queries contained invalid parameters.");
				return res;
			}

			while (!doneSim())
				incrementTime();
			res.endTime = endTime;
			int healthy = 0;
			for (int y = 0; y < med.length; y++)
				for (int x = 0; x < med[y].length; x++)
					if (virus[y][x] == 0) healthy++;
			if (debug) {
				System.out.println("After " + timeCount + " time:");
				String[] s = getStatus();
				for (int i = 0; i < s.length; i++)
					System.out.println(s[i]);
			}
			res.liveCell = healthy;
			res.medCount = medCount;
			res.simTime = timeCount;
			res.score = Math.max(1.0 * (healthy - medCount * 0.5) / Math.max(timeCount, 1), 0.0);
		} catch (Exception e) {
			addFatalError("RunTest Exception: " + e.getMessage() + "\n");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			addFatalError(sw.toString() + "\n");
		}
		return res;
	}

	// ------------- visualization part -----------------------------------------------------
	static int scale = -1;
	static boolean debug = false, vis = false;

	static class Visualizer extends JFrame {
		int time, H, W;
		boolean medVisible = true;
		int medOpacity = 0x80;
		ArrayList<TimeFrame> frames;
		Result result;
		Font font = new Font("Ricty", Font.BOLD, 13);

		Visualizer(ArrayList<TimeFrame> frames, Result result) {
			this.frames = frames;
			this.result = result;
			this.H = frames.get(0).virus.length;
			this.W = frames.get(0).virus[0].length;
			if (scale < 0) scale = Math.min(850 / H, 1300 / W);
			setSize(W * scale + 150, H * scale + 40);
			addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
			addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
						moveTime(e.isShiftDown() ? frames.size() : 1);
					} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
						moveTime(e.isShiftDown() ? -frames.size() : -1);
					} else if (e.getKeyCode() == KeyEvent.VK_UP) {
						if (medOpacity + 8 <= 0xFF) {
							medOpacity += 8;
							repaint();
						}
					} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
						if (medOpacity - 8 >= 0) {
							medOpacity -= 8;
							repaint();
						}
					} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
						medVisible ^= true;
						repaint();
					}
				}

				void moveTime(int diff) {
					int newTime = time + diff;
					if (newTime < 0) newTime = 0;
					if (newTime >= frames.size()) newTime = frames.size() - 1;
					if (time == newTime) return;
					time = newTime;
					repaint();
				}
			});
			getContentPane().add(new Vis());
		}

		public class Vis extends JPanel {
			public void paint(Graphics gr) {
				BufferedImage bi = new BufferedImage(W * scale + 150, H * scale + 1, BufferedImage.TYPE_INT_RGB);
				Graphics2D g2 = (Graphics2D) bi.getGraphics();
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g2.setFont(font);

				// background
				g2.setColor(new Color(0xEEEEEE));
				g2.fillRect(0, 0, bi.getWidth(), bi.getHeight());

				// each cell
				TimeFrame frame = frames.get(time);
				for (int i = 0; i < H; ++i) {
					for (int j = 0; j < W; ++j) {
						int c;
						if (frame.virus[i][j] < 0) {         // dead
							c = 0x404040;
						} else if (frame.virus[i][j] == 0) { // healthy
							c = 0xFFFFFF;
						} else {                             // infected
							c = 0xFF40FF;
						}
						g2.setColor(new Color(c));
						g2.fillRect(j * scale + 1, i * scale + 1, scale, scale);
						int medColor = medVisible ? Math.max(0, (int) (0xFF * (1 - frame.med[i][j] / 1))) : 0xFF;
						g2.setColor(new Color(medColor, medColor, 0xFF, medOpacity));
						g2.fillRect(j * scale + 1, i * scale + 1, scale, scale);
						g2.setColor(Color.BLUE);
					}
				}

				// cell border
				g2.setColor(new Color(0xCCCCCC));
				Stroke origStroke = g2.getStroke();
				Stroke boldStroke = new BasicStroke(2.0f);
				for (int i = 0; i <= H; ++i) {
					if (i % 5 == 0) g2.setStroke(boldStroke);
					g2.drawLine(0, i * scale, W * scale, i * scale);
					if (i % 5 == 0) g2.setStroke(origStroke);
				}
				for (int i = 0; i <= W; ++i) {
					if (i % 5 == 0) g2.setStroke(boldStroke);
					g2.drawLine(i * scale, 0, i * scale, H * scale);
					if (i % 5 == 0) g2.setStroke(origStroke);
				}

				// info
				g2.setColor(Color.BLACK);
				g2.translate(W * scale + 20, 10);
				int yPos = 10;

				g2.drawString("seed:" + result.seed, 0, yPos);
				yPos += 20;
				g2.drawString("H:" + H + " W:" + W, 0, yPos);
				yPos += 20;
				g2.drawString("S:" + result.medStrength, 0, yPos);
				yPos += 20;
				g2.drawString("K:" + result.killTime, 0, yPos);
				yPos += 20;
				g2.drawString("P:" + String.format("%.4f", result.spreadProb), 0, yPos);
				yPos += 40;

				g2.drawString("time   :" + ((time + 2) / 3) + " (" + ((time + 2) % 3 + 1) + "/3)", 0, yPos);
				yPos += 20;
				g2.drawString("virus  :" + frame.vc, 0, yPos);
				yPos += 20;
				g2.drawString("dead   :" + frame.dc, 0, yPos);
				yPos += 20;
				g2.drawString("healthy:" + (H * W - frame.vc - frame.dc), 0, yPos);

				gr.drawImage(bi, 0, 0, null);
			}
		}

	}

	static class TimeFrame {
		int[][] virus;
		double[][] med;
		int vc, dc;
	}

	private static final int THREAD_COUNT = 35;

	public static void main(String[] args) throws Exception {
		long seed = 1, begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-debug")) debug = true;
			if (args[i].equals("-scale")) scale = Integer.parseInt(args[++i]);
			if (args[i].equals("-H")) userH = Integer.parseInt(args[++i]);
			if (args[i].equals("-W")) userW = Integer.parseInt(args[++i]);
			if (args[i].equals("-K")) userK = Integer.parseInt(args[++i]);
			if (args[i].equals("-S")) userS = Integer.parseInt(args[++i]);
			if (args[i].equals("-P")) userP = Double.parseDouble(args[++i]);
		}
		if (begin != -1 && end != -1) {
			for (int id = 0; id <= 0; ++id) {
				try (PrintWriter writer = new PrintWriter(String.format("log/%02d.txt", id))) {
					ArrayList<Long> seeds = new ArrayList<Long>();
					for (long i = begin; i <= end; ++i) {
						seeds.add(i);
					}
					int len = seeds.size();
					Result[] results = new Result[len];
					TestThread[] threads = new TestThread[THREAD_COUNT];
					int prev = 0;
					for (int i = 0; i < THREAD_COUNT; ++i) {
						int next = len * (i + 1) / THREAD_COUNT;
						threads[i] = new TestThread(prev, next - 1, seeds, results);
						prev = next;
					}
					for (int i = 0; i < THREAD_COUNT; ++i) {
						threads[i].start();
					}
					for (int i = 0; i < THREAD_COUNT; ++i) {
						threads[i].join();
					}
					double sum = 0;
					for (int i = 0; i < results.length; ++i) {
						writer.println(results[i]);
						writer.println();
						sum += results[i].score;
					}
					writer.println("ave:" + (sum / results.length));
				}
			}
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
			if (vis) {
				Visualizer visualizer = new Visualizer(tester.frames, res);
				visualizer.setVisible(true);
			}
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int H, W, medStrength, killTime, initialVirusCount, liveCell, simTime, medCount, endTime;
		double spreadProb, score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("H:%3d W:%3d S:%2d K:%2d\n", H, W, medStrength, killTime);
			ret += String.format("spreadProb:%.4f\n", spreadProb);
			ret += String.format("initialVirus:%d\n", initialVirusCount);
			ret += String.format("live:%d medCount:%d simTime:%d endTime:%d\n", liveCell, medCount, simTime, endTime);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("score:%.4f", score);
			return ret;
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}
}
