#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:S, :K, :P, :initialVirus, :endTime, :elapsed, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = TestCase.new
  IO.foreach(filename) do |line|
    if line =~ /seed:(\d+)/
      seed = $1.to_i
    elsif line =~ /H: *(\d+) W: *(\d+) S: *(\d+) K: *(\d+)/
      testcase.S = $3.to_i
      testcase.K = $4.to_i
    elsif line =~ /spreadProb:(.+)/
      testcase.P = $1.to_f
    elsif line =~ /live:(\d+) medCount:(\d+) simTime:(\d+) endTime:(\d+)/
      testcase.endTime = $4.to_f
    elsif line =~ /elapsed:(.+)/
      testcase.elapsed = $1.to_f
    elsif line =~ /score:(.+)/
      testcase.score = $1.to_f
      scores[seed] = testcase
      testcase = TestCase.new
    end
  end
  return scores.compact
end

def calc(from, to, filter)
  sumScoreDiff = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  list.each do |pair|
    s1 = pair[0].score
    s2 = pair[1].score
    if s1 < s2
      win += 1
      sumScoreDiff += 1 - s1 / s2
    elsif s1 > s2
      lose += 1
      sumScoreDiff += s2 / s1 - 1
    else
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, list.size - win - lose)
  puts sprintf("  diff:%.4f", sumScoreDiff / list.size)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  1.upto(10) do |k|
    puts "K:#{k}"
    calc(from, to, proc { |t| t[0].K == k })
  end
  puts "---------------------"
  ps = from.map { |t| t.P }.sort.uniq
  ps.each do |p|
    puts "P:#{p}"
    calc(from, to, proc { |t| t[0].P == p })
  end
  puts "---------------------"
  10.step(90, 10) do |s|
    puts "S:[#{s}-#{s+9}]"
    calc(from, to, proc { |t| s <= t[0].S && t[0].S < s + 10 })
  end
  puts "---------------------"
  puts "total"
  calc(from, to, proc { |t| true })
end

main
