import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ViralInfection {

	private static final boolean DEBUG = false;
	private static final boolean MEASURE_TIME = false;
	private static final boolean SUBMIT = false;
	private static final long TL = SUBMIT ? 57000 : 40000;
	private static final int VIRUS_CELL = 1;
	private static final int LIVE_CELL = 0;
	private static final int DEAD_CELL = -1;
	private static final int DIFFUSION_MEMO_SIZE = 10;
	private static final int[] STEPS_AFTER = { 0, 3, 8, 9, 6, 5, 9, 7, 8, 9, 10 };

	Tester Research; // for local test
	long startTime = System.currentTimeMillis();
	XorShift rnd = new XorShift();
	Timer timer = new Timer();
	int H, W, S, K;
	double spreadP;
	State curState;
	int medCount;
	int stepsAfter;
	int[] positionCand = new int[1 << 20];
	double[][] medBuf;
	double[][][] diffusionMemo;
	double[][][] noopMed;
	int maxMedCountOnce;
	long timeLimit;

	int runSim(String[] slide, int medStrength, int killTime, double spreadProb) {
		H = slide.length;
		W = slide[0].length();
		S = medStrength;
		K = killTime;
		spreadP = spreadProb;
		curState = new State(slide);
		medBuf = new double[H][W];
		diffusionMemo = new double[DIFFUSION_MEMO_SIZE + 1][2 * DIFFUSION_MEMO_SIZE + 1][2 * DIFFUSION_MEMO_SIZE + 1];
		diffusionMemo[0][DIFFUSION_MEMO_SIZE][DIFFUSION_MEMO_SIZE] = S;
		for (int i = 0; i < DIFFUSION_MEMO_SIZE; ++i) {
			diffuse(diffusionMemo[i], diffusionMemo[i + 1]);
		}
		noopMed = new double[DIFFUSION_MEMO_SIZE + 1][H][W];
		maxMedCountOnce = Math.max((int) Math.ceil(K * spreadP), 1);
		stepsAfter = STEPS_AFTER[K];
		debug("initial Virus:" + curState.virusC);
		solve();
		timer.print();
		return 0;
	}

	void solve() {
		timeLimit = TL / 4;
		EvalStats stats = new EvalStats();
		boolean smallCase = curState.virusC < 2 * K;
		for (int turn = 0;; ++turn) {
			int virusCountWithWait = 0;
			State state = new State(curState);
			for (int i = 0; i < 2; ++i) {
				while (true) {
					state.simulateSingleStep();
					if (state.time % K == 0) break;
				}
			}
			virusCountWithWait = state.virusC;
			debug("estimated virus count after next decay:" + state.virusC);
			boolean finished;
			long elapsed = elapsed();
			if (!smallCase && (curState.virusC * spreadP > 2 * K || virusCountWithWait * spreadP > 4 * K) && elapsed() < TL - 2000) {
				int medCount = K == 1 ? 4 : K <= 7 ? K * 3 - 1 : K * 2 - 1;
				if ((curState.virusC * spreadP < 4 * K && virusCountWithWait * spreadP < 5 * K) || spreadP < 0.4) {
					medCount = K == 1 ? 1 : K - 1;
				}
				if (curState.time == 0) medCount++;
				int virtualTurn = Math.max(1, Math.min(curState.virusC, virusCountWithWait));
				timeLimit = elapsed + (TL - 1000 - elapsed) * medCount / virtualTurn;
				timeLimit = Math.max(timeLimit, elapsed + (TL - 1000 - elapsed) / Math.max(2, (10 - turn)));
				timeLimit = Math.min(timeLimit, TL - 1000);
				finished = solveEarlyPhase(medCount);
			} else {
				timeLimit = Math.max(timeLimit, elapsed + (TL - elapsed) / 2);
				finished = solveLastPhase(stats);
			}
			if (finished) break;
			observe();
			if (curState.virusC == 0) break;
			debug("");
		}
	}

	boolean solveEarlyPhase(int medCount) {
		final long initialTime = elapsed();
		debug("early phase start time:" + initialTime + " timeLimit:" + timeLimit);
		int BEAM_WIDTH = 1;
		debug("medCount:" + medCount);

		boolean result = false;
		ArrayList<BeamNode> beam = new ArrayList<>();
		beam.add(new BeamNode(0, 0, 0));
		beam.get(0).state = new State(curState);
		if (curState.time != 0 && curState.time % K == 0) beam.get(0).state.decay();
		OUT: for (int i = 0; i < medCount; ++i) {
			final long turnStartTime = elapsed();
			final long timeSlot = (timeLimit - turnStartTime) / (medCount - i);
			debug("beam turn:" + i + " start time:" + turnStartTime);
			ArrayList<BeamNode> next = new ArrayList<>();
			for (int j = 0; j < beam.size(); ++j) {
				BeamNode parent = beam.get(j);
				timer.start(3);
				State parentState = parent.state;
				if (i != 0) {
					parentState.addMed(parent.pos >> 8, parent.pos & 0xFF);
					parentState.simulateSingleStep();
				}
				if (K != 1) curePredictive(parentState);
				if (parentState.virusC == 0) {
					debug("early exit");
					beam.clear();
					beam.add(parent);
					timer.stop(3);
					result = true;
					break OUT;
				}
				timer.stop(3);
				timer.start(4);
				ArrayList<Integer> candPos = getEvalCandPos(parentState);
				timer.stop(4);
				timer.start(5);
				ArrayList<BeamNode> children = getChildNodes(parentState, candPos, j);
				timer.stop(5);
				timer.start(6);
				if (i != 0) {
					Collections.sort(children);
					for (int k = 0; k < Math.min(children.size(), Math.max(1, BEAM_WIDTH / 3)); ++k) {
						next.add(children.get(k));
					}
				} else {
					next.addAll(children);
				}
				timer.stop(6);
				if (elapsed() - turnStartTime > timeSlot) {
					debug("timelimit for current turn reached:" + j);
					break;
				}
			}
			Collections.sort(next);
			if (i < medCount - 1 && medCount > 1) {
				long usedTime = elapsed() - turnStartTime + 1; // avoid zero division
				long restTime = timeLimit - elapsed();
				if (restTime <= 100) {
					BEAM_WIDTH = 1;
				} else if (i == 0) {
					// restTime == usedTime * NEW_BEAM_WIDTH * (medCount - 1);
					BEAM_WIDTH = (int) (restTime / (usedTime * (medCount - 1)));
					if (curState.time == 0) BEAM_WIDTH *= 2; // compensate for slow execution speed before JIT complie
				} else {
					// restTime == usedTime * NEW_BEAM_WIDTH / BEAM_WIDTH * (medCount - (i + 1));
					BEAM_WIDTH = Math.min(BEAM_WIDTH == 1 ? 2 : BEAM_WIDTH * 3 / 2, (int) (restTime * BEAM_WIDTH / (usedTime * (medCount - i - 1))));
				}
				BEAM_WIDTH = Math.min(BEAM_WIDTH, (100 << 20) / (12 * W * H)); // avoid MLE
				BEAM_WIDTH = Math.max(1, BEAM_WIDTH);
				debug("set BEAM_WIDTH:" + BEAM_WIDTH + " usedTime:" + usedTime);
			}
			for (int j = 0; j < Math.min(next.size(), BEAM_WIDTH); ++j) {
				BeamNode node = next.get(j);
				node.parent = beam.get(node.idx);
				node.state = new State(beam.get(node.idx).state);
				//				debug("pos:(" + (node.pos >> 8) + "," + (node.pos & 0xFF) + ") v:" + node.v + " idx:" + node.idx);
			}
			beam.clear();
			for (int j = 0; j < Math.min(next.size(), BEAM_WIDTH); ++j) {
				beam.add(next.get(j));
			}
		}
		ArrayList<Integer> medPos = new ArrayList<>();
		BeamNode node = beam.get(0);
		while (true) {
			if (node.parent == null) break;
			medPos.add(node.pos);
			node = node.parent;
		}
		for (int i = medPos.size() - 1; i >= 0; --i) {
			addMed(medPos.get(i) >> 8, medPos.get(i) & 0xFF);
		}
		if (curState.virusC == 0) {
			result = true;
		}

		debug("early phase end time:" + elapsed());
		return result;
	}

	ArrayList<Integer> getEvalCandPos(State state) {
		int steps = S < 42 ? 2 : 3;
		double[][] weight = new double[H][W];
		final int RANGE = 10;
		for (int p : state.virusPos) {
			int vr = p >> 8;
			int vc = p & 0xFF;
			int liveCount = 0;
			int virusCount = 0;
			for (int i = Math.max(0, vr - RANGE); i <= Math.min(H - 1, vr + RANGE); ++i) {
				int width = RANGE - Math.abs(vr - i);
				for (int j = Math.max(0, vc - width); j <= Math.min(W - 1, vc + width); ++j) {
					if (state.virus[i][j] == LIVE_CELL) ++liveCount;
					if (state.virus[i][j] == VIRUS_CELL) ++virusCount;
				}
			}
			for (int i = Math.max(0, vr - steps); i <= Math.min(H - 1, vr + steps); ++i) {
				int width = steps - Math.abs(vr - i);
				for (int j = Math.max(0, vc - width); j <= Math.min(W - 1, vc + width); ++j) {
					weight[i][j] = Math.max(weight[i][j], 1.0 * liveCount / (liveCount + virusCount));
				}
			}
		}
		for (int p : state.virusPos) {
			int vr = p >> 8;
			int vc = p & 0xFF;
			weight[vr][vc]++;
			if (vr > 0) weight[vr - 1][vc]++;
			if (vc > 0) weight[vr][vc - 1]++;
			if (vr < H - 1) weight[vr + 1][vc]++;
			if (vc < W - 1) weight[vr][vc + 1]++;
			if (K > 5) {
				if (vr > 0 & vc > 0) weight[vr - 1][vc - 1]++;
				if (vr > 0 & vc < W - 1) weight[vr - 1][vc + 1]++;
				if (vr < H - 1 & vc > 0) weight[vr + 1][vc - 1]++;
				if (vr < H - 1 & vc < W - 1) weight[vr + 1][vc + 1]++;
			}
		}
		ArrayList<Long> list = new ArrayList<>();
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (weight[i][j] == 0) continue;
				list.add(((long) (weight[i][j] * (1L << 27)) << 32) + (i << 8) + j);
			}
		}
		Collections.sort(list);
		ArrayList<Integer> ret = new ArrayList<>();
		for (int i = 0; i < Math.min(list.size(), Math.max(1000, Math.sqrt(W * H) * 12)); ++i) {
			ret.add(list.get(list.size() - 1 - i).intValue());
		}
		return ret;
	}

	ArrayList<BeamNode> getChildNodes(State state, ArrayList<Integer> candPos, int parentIdx) {
		RATIO_PENA_VIRUS = state.virusC < 2 * K ? 10 : state.virusC < 4 * K ? 3 : 1;
		ArrayList<BeamNode> ret = new ArrayList<>();
		State noopState = new State(state);
		for (int y = 0; y < H; ++y) {
			System.arraycopy(noopState.med[y], 0, noopMed[0][y], 0, W);
		}
		for (int i = 0; i < stepsAfter; ++i) {
			noopState.simulateSingleStep();
			for (int y = 0; y < H; ++y) {
				System.arraycopy(noopState.med[y], 0, noopMed[i + 1][y], 0, W);
			}
		}
		State passState = new State(state);
		for (int p : candPos) {
			// for speedup, avoid creating new State object repeatedly
			for (int y = 0; y < H; ++y) {
				System.arraycopy(state.virus[y], 0, passState.virus[y], 0, W);
			}
			passState.virusPos.clear();
			passState.virusPos.addAll(state.virusPos);
			passState.virusC = state.virusC;
			passState.deadC = state.deadC;
			passState.time = state.time;

			int v = evaluateAfterSteps(passState, p, noopState, noopMed);
			ret.add(new BeamNode(p, v, parentIdx));
		}
		return ret;
	}

	static class BeamNode implements Comparable<BeamNode> {
		State state;
		BeamNode parent;
		int pos;
		int v;
		int idx;

		public BeamNode(int pos, int v, int idx) {
			this.pos = pos;
			this.v = v;
			this.idx = idx;
		}

		public int compareTo(BeamNode o) {
			return -Integer.compare(this.v, o.v);
		}
	}

	int[] posBuf = new int[10000];
	int RATIO_PENA_VIRUS = 1;

	int evaluateAfterSteps(State state, int medPos, State noopState, double[][][] noopMed) {
		timer.start(0);
		final int medY = medPos >> 8;
		final int medX = medPos & 0xFF;
		for (int i = 0; i < stepsAfter; ++i) {
			for (int j = 0; j < state.virusC; ++j) {
				int pos = state.virusPos.get(j);
				int y = pos >> 8;
				int x = pos & 0xFF;
				double medVal = noopMed[i][y][x] + getMedFromMemo(medY, medX, y, x, i);
				if (medVal >= 1) {
					state.virus[y][x] = LIVE_CELL;
					--state.virusC;
					Integer tmp = state.virusPos.get(j);
					state.virusPos.set(j, state.virusPos.get(state.virusC));
					state.virusPos.set(state.virusC, tmp);
					--j;
				}
			}
			state.time++;
			if (state.time % K == 0) state.decay();
		}
		state.virusPos.subList(state.virusC, state.virusPos.size()).clear();
		timer.stop(0);
		if (state.virusC >= noopState.virusC) { // pruning
			return -(1 << 28);
		}
		if (state.virusC == 0) {
			return state.countHealthyCell() * (W + H);
		}
		int ret = 0;
		timer.start(1);
		int posIdx = 0;
		for (int p : state.virusPos) {
			posBuf[posIdx++] = p;
			ret -= RATIO_PENA_VIRUS;
		}
		timer.stop(1);
		timer.start(2);
		final int MAX_DIST = (W + H) / 8;
		int dist = 1;
		int end = posIdx;
		ret += end;
		for (int i = 0; i != posIdx; ++i) {
			int p = posBuf[i];
			int vr = p >> 8;
			int vc = p & 0xFF;
			if (vr > 0 && state.virus[vr - 1][vc] == LIVE_CELL) {
				state.virus[vr - 1][vc] = VIRUS_CELL;
				posBuf[posIdx++] = p - 0x100;
			}
			if (vc > 0 && state.virus[vr][vc - 1] == LIVE_CELL) {
				state.virus[vr][vc - 1] = VIRUS_CELL;
				posBuf[posIdx++] = p - 1;
			}
			if (vc < W - 1 && state.virus[vr][vc + 1] == LIVE_CELL) {
				state.virus[vr][vc + 1] = VIRUS_CELL;
				posBuf[posIdx++] = p + 1;
			}
			if (vr < H - 1 && state.virus[vr + 1][vc] == LIVE_CELL) {
				state.virus[vr + 1][vc] = VIRUS_CELL;
				posBuf[posIdx++] = p + 0x100;
			}
			if (i == end) {
				++dist;
				end = posIdx;
				if (dist == MAX_DIST) {
					ret += (state.countHealthyCell() - i) * MAX_DIST;
					break;
				}
				ret += (end - i) * dist;
			}
		}
		timer.stop(2);

		return ret;
	}

	double getMedFromMemo(int medY, int medX, int posY, int posX, int step) {
		int diffY = posY - medY;
		int diffX = posX - medX;
		if (diffY < -step || step < diffY || diffX < -step || step < diffX) return 0;
		double ret = diffusionMemo[step][DIFFUSION_MEMO_SIZE + diffY][DIFFUSION_MEMO_SIZE + diffX];
		if (0 <= posY && posY < H) {
			ret += getMedFromMemo(medY, medX, -(posY + 1), posX, step);
			ret += getMedFromMemo(medY, medX, H - 1 + (H - posY), posX, step);
		}
		if (0 <= posX && posX < W) {
			ret += getMedFromMemo(medY, medX, posY, -(posX + 1), step);
			ret += getMedFromMemo(medY, medX, posY, W - 1 + (W - posX), step);
		}
		return ret;
	}

	void curePredictive(State state) {
		State os = new State(state);
		while (true) {
			os.cure();
			os.time++;
			if (os.time % K == 0) break;
			os.diffuse();
		}
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (state.virus[i][j] == VIRUS_CELL && os.virus[i][j] == LIVE_CELL) {
					state.virus[i][j] = LIVE_CELL;
					state.virusPos.remove((Integer) ((i << 8) + j));
					state.virusC--;
				}
			}
		}
	}

	boolean solveLastPhase(EvalStats stats) {
		debug("last phase start time:" + elapsed());
		Strategy strategy = getNextStrategy(stats);
		debug("best strategy medCount:" + strategy.pos.size());
		boolean finished = executeStrategy(strategy, stats, timeLimit);
		if (finished) return true;
		int virtualTurn = Math.max(2, (int) Math.ceil((stats.timeAve - curState.time) / (maxMedCountOnce + K / 2.0)));
		timeLimit += (TL - elapsed()) / virtualTurn;
		return false;
	}

	Strategy getNextStrategy(EvalStats stats) {
		long initialTime = elapsed();
		int positionRange = curState.virusC / K + 1;
		if (stats.timeAve != 0) {
			positionRange = (int) Math.min(positionRange, Math.max(1, (stats.timeAve - curState.time + K - 1) / K));
		}
		setupPositionCand(positionRange);
		Strategy initialStrategy = determineInitialStrategy();
		Strategy bestStrategy = simulatedAnneling(initialStrategy, initialTime);
		EvalStats bestScoreStats = calcExpectedScore(bestStrategy);
		stats.scoreAve = bestScoreStats.scoreAve;
		stats.timeAve = bestScoreStats.timeAve;
		return bestStrategy;
	}

	Strategy simulatedAnneling(Strategy initialStrategy, long initialTime) {
		debug("SA start time:" + elapsed());
		Strategy curStrategy = initialStrategy;
		Strategy bestStrategy = initialStrategy;
		EvalStats bestStats = calcExpectedScore(bestStrategy);
		debug(String.format("bestScore:%.4f time:%d", bestStats.scoreAve, (int) bestStats.timeAve));
		double curScore = bestStats.scoreAve;
		COLDNESS = 5;
		int bestUpdateTurn = 0;

		for (int turn = 0;; ++turn) {
			final long curTime = elapsed();
			if (curTime > timeLimit) {
				int virtualLoop = (int) Math.ceil((bestStats.timeAve - curState.time) / Math.min(maxMedCountOnce, bestStrategy.pos.size()));
				long virtualLimit;
				if (virtualLoop <= 1) {
					virtualLimit = TL - 500;
				} else {
					virtualLimit = initialTime + (TL - initialTime) / virtualLoop;
				}
				if (curTime >= virtualLimit) {
					debug("SA turn:" + turn + " elapsed:" + curTime);
					timeLimit = curTime;
					break;
				} else {
					debug("set timelimit to:" + virtualLimit);
					timeLimit = virtualLimit;
				}
			}
			if (turn > bestUpdateTurn + 300) {
				bestUpdateTurn = turn;
				if (COLDNESS < 400) {
					COLDNESS *= 1.2;
				}
			}
			Strategy nextStrategy = curStrategy.spawn();
			EvalStats nextScoreStats = calcExpectedScore(nextStrategy);
			if (transit(curScore, nextScoreStats.scoreAve)) {
				if (nextScoreStats.scoreAve > bestStats.scoreAve) {
					bestStats = nextScoreStats;
					bestStrategy = nextStrategy;
					bestUpdateTurn = turn;
					debug(String.format("bestScore:%.4f time:%d turn:%d", bestStats.scoreAve, (int) bestStats.timeAve, turn));
				}
				curScore = nextScoreStats.scoreAve;
				curStrategy = nextStrategy;
			}
		}
		return bestStrategy;
	}

	// returns true if finished
	boolean executeStrategy(Strategy strategy, EvalStats stats, long nextTL) {
		int stopTime = curState.time + maxMedCountOnce;
		while (stopTime % K != 0) {
			stopTime++;
		}
		boolean reachedTL = TL - elapsed() < 100;
		boolean shrink = !reachedTL && strategy.pos.size() + curState.time > stopTime;
		if (shrink) {
			while (strategy.pos.size() + curState.time > stopTime) {
				strategy.pos.remove(strategy.pos.size() - 1);
			}
			debug("medPos shrinked to size:" + strategy.pos.size());
		}

		for (int pos : strategy.pos) {
			addMed(pos >> 8, pos & 0xFF);
		}
		if (reachedTL) {
			debug("terminated by timelimit");
			return true;
		}
		if (stats.timeAve <= curState.time) {
			debug("terminate to avoid decreasing score by observe after finish");
			return true;
		}
		return false;
	}

	Strategy determineInitialStrategy() {
		double bestScore = -1;
		Strategy ret = new Strategy();
		Strategy strategy = new Strategy();
		strategy.pos.addAll(curState.virusPos);
		for (int i = 0; i < 20; ++i) {
			EvalStats smallStat = calcExpectedScore(strategy);
			if (smallStat.scoreAve > bestScore) {
				ret = new Strategy(strategy);
				bestScore = smallStat.scoreAve;
			}
			Collections.shuffle(strategy.pos);
		}
		debug("initial medCount:" + ret.pos.size() + " elapsed:" + elapsed());
		return ret;
	}

	EvalStats calcExpectedScore(Strategy strategy) {
		EvalStats ret = new EvalStats();
		State st = new State(curState);
		st.simulateUntilEnd(strategy);
		double score = score(st.countHealthyCell(), medCount + strategy.pos.size(), st.time);
		ret.scoreAve += score;
		ret.timeAve += st.time;
		return ret;
	}

	void setupPositionCand(int range) {
		debug("setup position range:" + range);
		int addPos = 0;
		double[][] deadProb = new double[H][W];
		double[][] virusProb = new double[H][W];
		double[][] sum = new double[H][W];
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (curState.virus[i][j] == VIRUS_CELL) virusProb[i][j] = 1;
				if (curState.virus[i][j] == DEAD_CELL) deadProb[i][j] = 1;
			}
		}
		for (int i = 0; i <= range; ++i) {
			double[][] nextVirusProb = new double[H][W];
			for (int r = 0; r < H; ++r) {
				for (int c = 0; c < W; ++c) {
					deadProb[r][c] += virusProb[r][c];
				}
			}
			for (int r = 0; r < H; ++r) {
				for (int c = 0; c < W; ++c) {
					if (virusProb[r][c] == 0) continue;
					sum[r][c] += virusProb[r][c];
					if (r > 0) {
						nextVirusProb[r - 1][c] += virusProb[r][c] * (1 - deadProb[r - 1][c] - nextVirusProb[r - 1][c]) * spreadP;
					}
					if (r < H - 1) {
						nextVirusProb[r + 1][c] += virusProb[r][c] * (1 - deadProb[r + 1][c] - nextVirusProb[r + 1][c]) * spreadP;
					}
					if (c > 0) {
						nextVirusProb[r][c - 1] += virusProb[r][c] * (1 - deadProb[r][c - 1] - nextVirusProb[r][c - 1]) * spreadP;
					}
					if (c < W - 1) {
						nextVirusProb[r][c + 1] += virusProb[r][c] * (1 - deadProb[r][c + 1] - nextVirusProb[r][c + 1]) * spreadP;
					}
				}
			}
			virusProb = nextVirusProb;
		}
		double[][] diffusedSum = sum;
		for (int i = 5; i < S; i *= 5) {
			double[][] newSum = new double[H][W];
			diffuse(sum, newSum);
			sum = newSum;
			for (int j = 0; j < H; ++j) {
				for (int k = 0; k < W; ++k) {
					diffusedSum[j][k] += sum[j][k];
				}
			}
		}

		double total = 0;
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				total += diffusedSum[i][j];
			}
		}
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				diffusedSum[i][j] *= (positionCand.length - addPos) / total;
			}
		}
		long[] resolver = new long[H * W];
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				int integralPart = (int) diffusedSum[i][j];
				Arrays.fill(positionCand, addPos, addPos + integralPart, (i << 8) + j);
				addPos += integralPart;
				resolver[i * W + j] = (((long) (diffusedSum[i][j] - integralPart) * (1L << 32)) << 24) + (i << 8) + j;
			}
		}
		final int space = positionCand.length - addPos;
		Arrays.sort(resolver);
		for (int i = 0; i < space; ++i) {
			positionCand[addPos++] = (int) (resolver[resolver.length - 1 - i] & 0xFFFF);
		}
	}

	int getRandomPosition() {
		return positionCand[rnd.nextInt() & ((1 << 20) - 1)];
	}

	void addMed(int y, int x) {
		debug("add med:(" + y + "," + x + ")");
		Research.addMed(x, y);
		curState.addMed(y, x);
		curState.simulateSingleStep();
		++medCount;
	}

	void observe() {
		String[] real = Research.observe();
		curState.update(real);
		curState.simulateSingleStepExceptDecay();
		curState.afterObserve = true;
		debug("observe at time:" + curState.time + " virusC:" + curState.virusC + " live:" + curState.countHealthyCell());
	}

	void waitTime(int units) {
		Research.waitTime(units);
		for (int i = 0; i < units; ++i) {
			curState.simulateSingleStep();
		}
	}

	final class Strategy {
		ArrayList<Integer> pos;

		Strategy() {
			this.pos = new ArrayList<>();
		}

		Strategy(Strategy o) {
			this.pos = new ArrayList<>(o.pos);
		}

		Strategy spawn() {
			final int MOVE_RANGE = (H + W) / 16;
			Strategy ret = new Strategy(this);
			int select = rnd.nextInt(4);
			if (select == 0 || ret.pos.isEmpty()) { // add
				ret.pos.add(getRandomPosition());
			} else if (!ret.pos.isEmpty() && select == 1) { // remove
				ret.pos.remove(rnd.nextInt(ret.pos.size()));
			} else if (ret.pos.size() > 1 && select == 2) { // swap
				int i1 = rnd.nextInt(ret.pos.size());
				int i2 = rnd.nextInt(ret.pos.size() - 1);
				if (i2 >= i1) ++i2;
				Integer tmp = ret.pos.get(i1);
				ret.pos.set(i1, ret.pos.get(i2));
				ret.pos.set(i2, tmp);
			} else { // move
				for (int i = 0; i < 5; ++i) {
					int idx = rnd.nextInt(ret.pos.size());
					int pos = ret.pos.get(idx);
					int y = (pos >> 8) + rnd.nextInt(MOVE_RANGE * 2 + 1) - MOVE_RANGE;
					int x = (pos & 0xFF) + rnd.nextInt(MOVE_RANGE * 2 + 1) - MOVE_RANGE;
					if (y < 0) y = 0;
					if (y >= H) y = H - 1;
					if (x < 0) x = 0;
					if (x >= W) x = W - 1;
					ret.pos.set(idx, (y << 8) + x);
				}
			}
			return ret;
		}
	}

	static final class EvalStats {
		double scoreAve;
		double timeAve;
	}

	final class State {
		int[][] virus;
		double[][] med;
		ArrayList<Integer> virusPos = new ArrayList<>(); // apply delayed evaluation for cured cell
		int virusC, deadC;
		int time;
		boolean afterObserve = false;

		State(String[] slide) {
			this.virus = new int[H][W];
			this.med = new double[H][W];
			update(slide);
		}

		State(State o) {
			this.virus = new int[H][];
			this.med = new double[H][];
			for (int i = 0; i < H; ++i) {
				this.virus[i] = o.virus[i].clone();
				this.med[i] = o.med[i].clone();
			}
			this.virusPos.addAll(o.virusPos);
			this.virusC = o.virusC;
			this.deadC = o.deadC;
			this.time = o.time;
			this.afterObserve = o.afterObserve;
		}

		void update(String[] slide) {
			virusPos.clear();
			deadC = 0;
			for (int i = 0; i < H; ++i) {
				for (int j = 0; j < W; ++j) {
					if (slide[i].charAt(j) == 'V') {
						virus[i][j] = VIRUS_CELL;
						virusPos.add((i << 8) + j);
					} else if (slide[i].charAt(j) == 'X') {
						virus[i][j] = DEAD_CELL;
						++deadC;
					} else {
						virus[i][j] = LIVE_CELL;
					}
				}
			}
			this.virusC = virusPos.size();
		}

		void cure() {
			for (int i = 0; i < virusC; ++i) {
				int pos = virusPos.get(i);
				int y = pos >> 8;
				int x = pos & 0xFF;
				if (med[y][x] >= 1) {
					virus[y][x] = LIVE_CELL;
					--virusC;
					Integer tmp = virusPos.get(i);
					virusPos.set(i, virusPos.get(virusC));
					virusPos.set(virusC, tmp);
					--i;
				}
			}
		}

		// assume called only when time % K == 0
		void decay() {
			assert (time % K == 0);
			ArrayList<Integer> nextVirusPos = new ArrayList<>();
			for (int i = 0; i < virusC; ++i) {
				int pos = virusPos.get(i);
				int y = pos >> 8;
				int x = pos & 0xFF;
				virus[y][x] = DEAD_CELL;
				++deadC;
				if (y > 0) infect(y - 1, x, nextVirusPos);
				if (y < H - 1) infect(y + 1, x, nextVirusPos);
				if (x > 0) infect(y, x - 1, nextVirusPos);
				if (x < W - 1) infect(y, x + 1, nextVirusPos);
			}
			virusPos = nextVirusPos;
			virusC = virusPos.size();
		}

		private void infect(int y, int x, ArrayList<Integer> nextPos) {
			if (virus[y][x] == LIVE_CELL /*&& rnd.nextDouble() < spreadP*/) {
				virus[y][x] = VIRUS_CELL;
				nextPos.add((y << 8) + x);
			}
		}

		void diffuse() {
			ViralInfection.this.diffuse(this.med, medBuf);
			double[][] tmp = med;
			this.med = medBuf;
			medBuf = tmp;
		}

		void addMed(int y, int x) {
			med[y][x] += S;
		}

		void simulateUntilEnd(Strategy strategy) {
			if (this.afterObserve && time % K == 0) decay();
			int minTime = time + strategy.pos.size();
			for (int i = 0; i < strategy.pos.size() && virusC > 0; ++i) {
				int pos = strategy.pos.get(i);
				addMed(pos >> 8, pos & 0xFF);
				simulateSingleStep();
			}
			while (virusC > 0) {
				simulateSingleStep();
			}
			time = Math.max(minTime, time);
		}

		void simulateSingleStep() {
			cure();
			++time;
			if (time % K == 0) decay();
			diffuse();
		}

		void simulateSingleStepExceptDecay() {
			cure();
			++time;
			diffuse();
		}

		int countHealthyCell() {
			return H * W - virusC - deadC;
		}
	}

	double COLDNESS = 5;

	boolean transit(double prev, double next) {
		if (prev <= next) return true;
		return rnd.nextDouble() < Math.exp((next - prev) / prev * COLDNESS);
	}

	void diffuse(double[][] from, double[][] to) {
		final int h = from.length;
		final int w = from[0].length;
		double[] buf = new double[w];
		for (int i = 0; i < h - 1; ++i) {
			for (int j = 0; j < w - 1; ++j) {
				double diffV = (from[i][j] - from[i + 1][j]) * 0.2;
				double diffH = (from[i][j] - from[i][j + 1]) * 0.2;
				buf[j + 1] += diffH;
				to[i][j] = from[i][j] + buf[j] - diffV - diffH;
				buf[j] = diffV;
			}
			double diffV = (from[i][w - 1] - from[i + 1][w - 1]) * 0.2;
			to[i][w - 1] = from[i][w - 1] + buf[w - 1] - diffV;
			buf[w - 1] = diffV;
		}
		{
			for (int j = 0; j < w - 1; ++j) {
				double diffH = (from[h - 1][j] - from[h - 1][j + 1]) * 0.2;
				buf[j + 1] += diffH;
				to[h - 1][j] = from[h - 1][j] + buf[j] - diffH;
			}
			to[h - 1][w - 1] = from[h - 1][w - 1] + buf[w - 1];
		}
	}

	static double score(int healthy, int medCount, int time) {
		return Math.max(0, (healthy - medCount * 0.5) / time);
	}

	long elapsed() {
		return System.currentTimeMillis() - startTime;
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;
		static final double TO_DOUBLE = 1.0 / (1L << 31);

		int nextInt(int n) {
			final int r = nextInt() % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		double nextDouble() {
			return Math.abs(nextInt() * TO_DOUBLE);
		}
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}
	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int H = sc.nextInt();
			String[] slide = new String[H];
			for (int i = 0; i < H; ++i) {
				slide[i] = sc.next();
			}
			int medStrength = sc.nextInt();
			int killTime = sc.nextInt();
			double spreadProb = sc.nextDouble();
			ViralInfection obj = new ViralInfection();
			obj.runSim(slide, medStrength, killTime, spreadProb);
			System.out.println("END");
			System.out.flush();
		}
	}
}
