#!/bin/sh -exu

IDX=${AWS_BATCH_JOB_ARRAY_INDEX:-0}
SEED_START=$(expr $IDX \* $RANGE + 1000)
SEED_END=$(expr $IDX \* $RANGE + $RANGE + 1000 - 1)

javac Tester.java
g++ BouncingBalls.cpp -Wall -Wno-sign-compare -std=gnu++11 -O3 -DTESTER -o main
java Tester -seed $SEED_START,$SEED_END -printRunTime > log.txt 2> /dev/null
aws s3 cp log.txt s3://marathon-tester/$RESULT_PATH/$SEED_START.txt
