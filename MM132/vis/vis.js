"use strict";
const output = document.getElementById("output")
const startBtn = document.getElementById("start")
const importBtn = document.getElementById("import")
const exportBtn = document.getElementById("export")
const size = document.getElementById("size")
const step = document.getElementById("step")
const canvas = document.getElementById("canvas")
const score = document.getElementById("score")
const EMPTY = 0
const SLASH = 1
const BACKSLASH = 3
const DR = [0, 1, 0, -1]
const DC = [-1, 0, 1, 0]
const DIR_LEFT = 0
const DIR_BOTTOM = 1
const DIR_RIGHT = 2
const DIR_TOP = 3
const IN_CELL_MARGIN = 4
const COLORS = ["#F39800", "#8FC31F", "#009E96", "#0068B7", "#920783", "#E5004F"]

function show_field(ctx, field, cellLen)	{
	const N = field.length
	ctx.fillStyle = 'white'
	ctx.fillRect(cellLen, cellLen, N * cellLen, N * cellLen)
	ctx.strokeStyle = 'black'
	ctx.lineWidth = 1.0
	ctx.beginPath()
	for (let i = 0; i <= N; i++) {
		ctx.moveTo((i + 1) * cellLen, cellLen)
		ctx.lineTo((i + 1) * cellLen, (N + 1) * cellLen)
		ctx.moveTo(cellLen, (i + 1) * cellLen)
		ctx.lineTo((N + 1) * cellLen, (i + 1) * cellLen)
	}
	ctx.stroke()
	ctx.lineWidth = 2.0
	ctx.beginPath()
	for (let i = 0; i < N; i++) {
		for (let j = 0; j < N; j++) {
			if (field[i][j] == SLASH) {
				ctx.moveTo((j + 2) * cellLen - IN_CELL_MARGIN, (i + 1) * cellLen + IN_CELL_MARGIN)
				ctx.lineTo((j + 1) * cellLen + IN_CELL_MARGIN, (i + 2) * cellLen - IN_CELL_MARGIN)
			} else if (field[i][j] == BACKSLASH) {
				ctx.moveTo((j + 1) * cellLen + IN_CELL_MARGIN, (i + 1) * cellLen + IN_CELL_MARGIN)
				ctx.lineTo((j + 2) * cellLen - IN_CELL_MARGIN, (i + 2) * cellLen - IN_CELL_MARGIN)
			}
		}
	}
	ctx.stroke()
	ctx.closePath()
}

class Ball {
	constructor(x, y, dir) {
		this.x = x
		this.y = y
		this.dir = dir
	}
}

class Frame {
	constructor(field, balls, score) {
		const N = field.length
		this.field = new Array(N)
		for (let i = 0; i < N; i++) {
			this.field[i] = [...field[i]]
		}
		this.balls = balls.map((b) => { return new Ball(b.x, b.y, b.dir) })
		this.score = score
	}
}


class Game {
	constructor(N) {
		this.N = N
		this.field = new Array(N)
		for (let i = 0; i < N; i++) {
			this.field[i] = new Array(N)
			this.field[i].fill(EMPTY)
		}
		this.hasGun = new Array(4)
		for (let i = 0; i < 4; i++) {
			this.hasGun[i] = new Array(N)
			this.hasGun[i].fill(false)
		}
		this.score = 0
		this.frames = []
		this.preparing = true
		this.cl = canvas.height / (this.N + 2)
	}

	show() {
		const ctx = canvas.getContext('2d'); 
		ctx.fillStyle = 'white'
		ctx.fillRect(0, 0, canvas.width, canvas.height)
		ctx.strokeStyle = 'black'
		ctx.lineWidth = 1.0
		ctx.beginPath()
		for (let i = 0; i <= this.N; i++) {
			ctx.moveTo((i + 1) * this.cl, this.cl)
			ctx.lineTo((i + 1) * this.cl, (this.N + 1) * this.cl)
			ctx.moveTo(this.cl, (i + 1) * this.cl)
			ctx.lineTo((this.N + 1) * this.cl, (i + 1) * this.cl)
		}
		ctx.stroke()

		const AR_LEN = 8
		for (let i = 0; i < this.N; i++) {
			if (this.hasGun[DIR_LEFT][i]) {
				ctx.beginPath()
				ctx.moveTo(IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.lineTo(this.cl - IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.moveTo(this.cl - IN_CELL_MARGIN - AR_LEN, (i + 1.5) * this.cl - AR_LEN)
				ctx.lineTo(this.cl - IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.moveTo(this.cl - IN_CELL_MARGIN - AR_LEN, (i + 1.5) * this.cl + AR_LEN)
				ctx.lineTo(this.cl - IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.stroke()
			}
			if (this.hasGun[DIR_BOTTOM][i]) {
				ctx.beginPath()
				ctx.moveTo((i + 1.5) * this.cl, (this.N + 2) * this.cl - IN_CELL_MARGIN)
				ctx.lineTo((i + 1.5) * this.cl, (this.N + 1) * this.cl + IN_CELL_MARGIN)
				ctx.moveTo((i + 1.5) * this.cl - AR_LEN, (this.N + 1) * this.cl + IN_CELL_MARGIN + AR_LEN)
				ctx.lineTo((i + 1.5) * this.cl, (this.N + 1) * this.cl + IN_CELL_MARGIN)
				ctx.moveTo((i + 1.5) * this.cl + AR_LEN, (this.N + 1) * this.cl + IN_CELL_MARGIN + AR_LEN)
				ctx.lineTo((i + 1.5) * this.cl, (this.N + 1) * this.cl + IN_CELL_MARGIN)
				ctx.stroke()
			}
			if (this.hasGun[DIR_RIGHT][i]) {
				ctx.beginPath()
				ctx.moveTo((this.N + 2) * this.cl - IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.lineTo((this.N + 1) * this.cl + IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.moveTo((this.N + 1) * this.cl + IN_CELL_MARGIN + AR_LEN, (i + 1.5) * this.cl - AR_LEN)
				ctx.lineTo((this.N + 1) * this.cl + IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.moveTo((this.N + 1) * this.cl + IN_CELL_MARGIN + AR_LEN, (i + 1.5) * this.cl + AR_LEN)
				ctx.lineTo((this.N + 1) * this.cl + IN_CELL_MARGIN, (i + 1.5) * this.cl)
				ctx.stroke()
			}
			if (this.hasGun[DIR_TOP][i]) {
				ctx.beginPath()
				ctx.moveTo((i + 1.5) * this.cl, IN_CELL_MARGIN)
				ctx.lineTo((i + 1.5) * this.cl, this.cl - IN_CELL_MARGIN)
				ctx.moveTo((i + 1.5) * this.cl - AR_LEN, this.cl - IN_CELL_MARGIN - AR_LEN)
				ctx.lineTo((i + 1.5) * this.cl, this.cl - IN_CELL_MARGIN)
				ctx.moveTo((i + 1.5) * this.cl + AR_LEN, this.cl - IN_CELL_MARGIN - AR_LEN)
				ctx.lineTo((i + 1.5) * this.cl, this.cl - IN_CELL_MARGIN)
				ctx.stroke()
			}
		}

		show_field(ctx, this.field, this.cl)
		ctx.closePath()
	}

	show_frame(t) {
		const ctx = canvas.getContext('2d'); 
		const radius = Math.max(8, this.cl * 0.2)
		const frame = this.frames[t]
		show_field(ctx, frame.field, this.cl)
		frame.balls.forEach((b, i) => {
			const x = (b.x + 1.5) * this.cl - DC[b.dir] * 5
			const y = (b.y + 1.5) * this.cl - DR[b.dir] * 5
			ctx.fillStyle = COLORS[i % COLORS.length]
			ctx.beginPath()
			ctx.arc(x, y, radius, 0, 2 * Math.PI)
			ctx.fill()
		})
		score.innerText = frame.score
	}

	click(x, y) {
		const cellX = Math.floor(x / this.cl) - 1
		const cellY = Math.floor(y / this.cl) - 1
		if (cellX == -1 && 0 <= cellY && cellY < this.N) {
			this.hasGun[DIR_LEFT][cellY] ^= true
			this.show()
		} else if (cellX == this.N && 0 <= cellY && cellY < this.N) {
			this.hasGun[DIR_RIGHT][cellY] ^= true
			this.show()
		} else if (cellY == -1 && 0 <= cellX && cellX < this.N) {
			this.hasGun[DIR_TOP][cellX] ^= true
			this.show()
		} else if (cellY == this.N && 0 <= cellX && cellX < this.N) {
			this.hasGun[DIR_BOTTOM][cellX] ^= true
			this.show()
		} else if (0 <= cellX && cellX <= this.N && 0 <= cellY && cellY < this.N) {
			const cur = this.field[cellY][cellX]
			this.field[cellY][cellX] = cur == EMPTY ? SLASH : cur == SLASH ? BACKSLASH : EMPTY
			this.show()
		}
	}

	simulate() {
		this.frames = []
		const curField = new Array(this.N)
		for (let i = 0; i < this.N; i++) {
			curField[i] = [...this.field[i]]
		}
		const balls = []
		for (let i = 0; i < this.N; i++) {
			if (this.hasGun[DIR_LEFT][i]) {
				balls.push(new Ball(0, i, DIR_RIGHT))
			}
			if (this.hasGun[DIR_BOTTOM][i]) {
				balls.push(new Ball(i, this.N - 1, DIR_TOP))
			}
			if (this.hasGun[DIR_RIGHT][i]) {
				balls.push(new Ball(this.N - 1, i, DIR_LEFT))
			}
			if (this.hasGun[DIR_TOP][i]) {
				balls.push(new Ball(i, 0, DIR_BOTTOM))
			}
		}
		let score = 0
		while (true) {
			this.frames.push(new Frame(curField, balls, score))
			if (balls.some((b) => {
				return b.x < 0 || this.N <= b.x || b.y < 0 || this.N <= b.y
			})) {
				break
			}
			const flipPos = []
			balls.forEach((b, i) => {
				if (curField[b.y][b.x] != EMPTY) {
					score++
					if (balls.every((b2, i2) => {
						return i == i2 || b.x != b2.x || b.y != b2.y
					})) {
						flipPos.push([b.x, b.y])
					}
				}
			})
			balls.forEach((b, i) => {
				b.dir ^= curField[b.y][b.x]
				b.y += DR[b.dir]
				b.x += DC[b.dir]
			})
			flipPos.forEach((p) => {
				curField[p[1]][p[0]] ^= 2
			})
		}
	}
}

let game = new Game(6)
game.show()

size.onchange = (event) => {
	game = new Game(parseInt(event.target.value))
	game.show()
}

startBtn.onclick = (event) => {
	if (game.preparing) {
		game.preparing = false
		game.simulate()	
		step.max = game.frames.length - 1
		step.value = 0
		game.show_frame(0)
		startBtn.innerText = "stop"
		importBtn.disabled = true
		exportBtn.disabled = true
		size.disabled = true
		step.disabled = false
	} else {
		game.preparing = true
		game.show()
		startBtn.innerText = "start"
		importBtn.disabled = false
		exportBtn.disabled = false
		size.disabled = false
		step.disabled = true
	}
}

importBtn.onclick = (event) => {
	const str = output.value.split("\n")
	const n = str[0].length
	game = new Game(n)
	for (let i = 0; i < n; i++) {
		for (let j = 0; j < n; j++) {
			game.field[i][j] = str[i][j] == '/' ? SLASH : str[i][j] == '\\' ? BACKSLASH : EMPTY
		}
	}
	size.value = n
	game.show()
}

step.onchange = (event) => {
	if (game.preparing) return
	game.show_frame(parseInt(event.target.value))
}

canvas.onclick = (event) => {
	if (!game.preparing) return
	game.click(event.offsetX, event.offsetY)
}

