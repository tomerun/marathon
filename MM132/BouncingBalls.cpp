#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <set>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
#ifndef NDEBUG
// #define MEASURE_TIME
#define DEBUG
#endif
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 3.126448e9;
constexpr ll TL = 1000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 4000;
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
constexpr inline T sq(T v) { return v * v; }

void debug_vec(const vi& vec) {
  debugStr("[");
  for (int i = 0; i < vec.size(); ++i) {
    debug("%d ", vec[i]);
  }
  debugStr("]");
}

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

constexpr int INF = 1 << 28;
constexpr int EMPTY = 0;
constexpr int SLASH = 1;
constexpr int BACKSLASH = 3;
constexpr int BONUS = 8;  // HACK: avoid branching for updating direction of balls
constexpr array<char, 9> CELL = {'.', '/', '$', '\\', '$', '$', '$', '$', '*'};
constexpr array<int, 4> DR = {1, 0, -1, 0};
constexpr array<int, 4> DC = {0, -1, 0, 1};
constexpr int DOWN = 0;
constexpr int LEFT = 1;
constexpr int UP = 2;
constexpr int RIGHT = 3;

int N, V;
array<array<int, 30>, 30> orig_field;
array<array<int, 30>, 30> field_score;
array<array<int, 30>, 30> ini_field_rot;
array<array<int, 30>, 30> field_rot;

inline bool in_grid(int p) {
  return 0 <= p && p < N;
}

inline int dist_edge(int pos)  {
  return min(pos + 1, N - pos);
}

int dist_edge(int r, int c) {
  if (r == -1 || r == N || c == -1 || c == N) return 0;
  return min(dist_edge(r), dist_edge(c)); // + (orig_field[r][c] == BONUS ? 2 : 0);
}

struct Gun {
  int r, c;
  int dir() const {
    if (r == -1) {
      return DOWN;
    } else if (c == N) {
      return LEFT;
    } else if (r == N) {
      return UP;
    } else {
      return RIGHT;
    }
  }
};

struct Ball {
  int r, c, dir;
};

struct Result {
  vector<vector<int>> field;
  vector<Gun> guns;
  int score;
};

struct Solver {
  int B;
  Solver(int B_) : B(B_) { }

  Result solve() {
    vector<Gun> all_guns;
    for (int i = 0; i < N; ++i) {
      all_guns.push_back({-1, i});
      all_guns.push_back({N, i});
      all_guns.push_back({i, -1});
      all_guns.push_back({i, N});
    }
    vector<Gun> guns;
    vector<vector<int>> field(N);
    Result best_result = {};
    best_result.field = field;
    for (int turn = 0; ; ++turn) {
      if (get_elapsed_msec() > TL) {
        debug("total turn:%d\n", turn);
        break;
      }
      for (int i = 0; i < B; ++i) {
        int pos = rnd.nextUInt(all_guns.size() - i) + i;
        swap(all_guns[i], all_guns[pos]);
      }
      guns.assign(all_guns.begin(), all_guns.begin() + B);
      for (int i = 0; i < N; ++i) {
        field[i].assign(orig_field[i].begin(), orig_field[i].begin() + N);
      }
      // for (int i = 0; i < N; ++i) {
      //   for (int j = 0; j < N; ++j) {
      //     if (orig_field[i][j] == EMPTY) {
      //       field[i][j] = (rnd.nextUInt() & 1) ? SLASH : BACKSLASH;
      //     }
      //   }
      // }
      int score = simulate(field, guns);
      if (score > best_result.score) {
        debug("score:%d at turn:%d\n", score, turn);
        best_result.score = score;
        swap(best_result.field, field);
        swap(best_result.guns, guns);

        // for (int i = 0; i < N; ++i) {
        //   for (int j = 0; j < N; ++j) {
        //     debug("%c", CELL[best_result.field[i][j]]);
        //   }
        //   debugln();
        // }
        // debugln();
        // simulate(best_result.field, best_result.guns, true);
      }
    }
    return best_result;
  }

  int select_mirror(const vector<vector<int>>& field, const Ball& ball) {
    int dir0 = ball.dir ^ 1;
    int nr0 = ball.r + DR[dir0];
    int nc0 = ball.c + DC[dir0];
    int dist0 = dist_edge(nr0, nc0);
    if (dist0 == 0) {
      return BACKSLASH;
    }
    int dir1 = ball.dir ^ 3;
    int nr1 = ball.r + DR[dir1];
    int nc1 = ball.c + DC[dir1];
    int dist1 = dist_edge(nr1, nc1);
    if (dist1 == 0) {
      return SLASH;
    }
    if (dist0 == 1 && dist1 == 1) {
      if (field[nr0][nc0] == SLASH || field[nr0][nc0] == BACKSLASH) {
        int nnr0 = nr0 + DR[dir0 ^ field[nr0][nc0]];
        int nnc0 = nc0 + DC[dir0 ^ field[nr0][nc0]];
        if (dist_edge(nnr0, nnc0) == 0) {
          return BACKSLASH;
        }
      }
      if (field[nr1][nc1] == SLASH || field[nr1][nc1] == BACKSLASH) {
        int nnr1 = nr1 + DR[dir1 ^ field[nr1][nc1]];
        int nnc1 = nc1 + DC[dir1 ^ field[nr1][nc1]];
        if (dist_edge(nnr1, nnc1) == 0) {
          return SLASH;
        }
      }
    }
    dist0 *= dist0;
    dist1 *= dist1;
    if (rnd.nextUInt(dist0 + dist1) < dist0) {
      return SLASH;
    } else {
      return BACKSLASH;
    }
  }

  int simulate(vector<vector<int>>& ini_field, const vector<Gun>& guns, bool print = false) {
    auto field = ini_field;
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        if (field[i][j] == BONUS) {
          field_score[i][j] = V;
        } else if (field[i][j] == EMPTY) {
          field_score[i][j] = 0;
        } else {
          field_score[i][j] = 1;
        }
      }
    }
    vector<Ball> balls;
    for (int i = 0; i < B; ++i) {
      const Gun& gun = guns[i];
      balls.push_back({gun.r, gun.c, gun.dir()});
    }
    int score = 0;
    while (true) {
      bool finish = false;
      for (int i = 0; i < B; ++i) {
        Ball& ball = balls[i];
        ball.r += DR[ball.dir];
        ball.c += DC[ball.dir];
        if (in_grid(ball.r) && in_grid(ball.c)) {
          if (ini_field[ball.r][ball.c] == EMPTY) {
            ini_field[ball.r][ball.c] = field[ball.r][ball.c] = select_mirror(field, ball);
            field_score[ball.r][ball.c] = 1;
          }
          score += field_score[ball.r][ball.c];
          field_rot[ball.r][ball.c] = ini_field_rot[ball.r][ball.c];
          ball.dir = (ball.dir ^ field[ball.r][ball.c]) & 3;
        } else {
          finish = true;
        }
      }
      if (finish) {
        break;
      }
      for (int i = 0; i < B; ++i) {
        const Ball& ball = balls[i];
        field[ball.r][ball.c] ^= field_rot[ball.r][ball.c] & (field[ball.r][ball.c] << 1);
        field_rot[ball.r][ball.c] >>= 2;
      }
      if (print) {
        for (int i = 0; i < N; ++i) {
          for (int j = 0; j < N; ++j) {
            bool has_ball = false;
            for (int k = 0; k < B; ++k) {
              if (balls[k].r == i && balls[k].c == j) {
                has_ball = true;
              }
            }
            if (has_ball) {
              debug("[%c]", CELL[field[i][j]]);
            } else {
              debug(" %c ", CELL[field[i][j]]);
            }
          }
          debugln();
        }
        debugln();
      }
    }

    return score;
  }
};

int main() {
  int B;
  scanf("%d %d %d", &N, &B, &V);
  debug("N=%d B=%d V=%d\n", N, B, V);
  start_time = get_time();

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      char cell[2];
      scanf("%s", cell);
      if (cell[0] == '.') {
        orig_field[i][j] = EMPTY;
        ini_field_rot[i][j] = 0xA; // 0b1010
      } else if (cell[0] == '/') {
        orig_field[i][j] = SLASH;
      } else if (cell[0] == '\\') {
        orig_field[i][j] = BACKSLASH;
      } else {
        orig_field[i][j] = BONUS;
      }
      debug("%c", CELL[orig_field[i][j]]);
    }
    debugln();
  }
  debugln();

  auto solver = unique_ptr<Solver>(new Solver(B));
  Result res = solver->solve();
  debug("final score:%d\n", res.score);
  for (int i = 0; i < B; ++i) {
    printf("%d %d\n", res.guns[i].r, res.guns[i].c);
    debug("%d %d\n", res.guns[i].r, res.guns[i].c);
  }
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      printf("%c\n", CELL[res.field[i][j]]);
      debug("%c", CELL[res.field[i][j]]);
    }
    debugln();
  }
  debugln();
  fflush(stdout);
}