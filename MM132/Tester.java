import com.topcoder.marathon.MarathonAnimatedVis;
import com.topcoder.marathon.MarathonController;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import java.util.List;


public class Tester extends MarathonAnimatedVis {
	//parameter ranges
	private static final int minN = 6, maxN = 30;
	private static final int minBalls = 1, maxBalls = 5;
	private static final int minFixed = 2;
	private static final int minBonuses = 2;
	private static final int minBonusValue = 2;


	//Inputs
	private int N;            //grid size
	private int B;            //number of balls
	private int bonusValue;   //value of visiting a bonus cell
	private int numFixed;     //number of fixed panels
	private int numBonus;     //number of bonus cells


	//Constants
	private static final char Empty = '.';
	private static final char Bonus = '*';
	private static final char panel1 = '/';
	private static final char panel2 = '\\';
	private static final char[] dirs = {'L', 'U', 'R', 'D'};
	private static final int[] dr = {0, -1, 0, 1};
	private static final int[] dc = {-1, 0, 1, 0};
	private static final int[] changeDir1 = new int[]{3, 2, 1, 0};   //D / L, R / U, U / R, L / D
	private static final int[] changeDir2 = new int[]{1, 0, 3, 2};   // D \ R, R \ D, U \ L, L \ U
	private static final int panelValue = 1;     //value of hitting a panel


	//State Control
	private int bonusHits;
	private int panelHits;
	private char[][] inputGrid;
	private char[][] outputGrid;
	private Loc[] Balls;
	private Loc[] Guns;
	private List<Loc>[] History;


	//Graphics
	private Color[] colors;
	private Color[] colorsLight;
	private boolean firstDraw = true;
	private static final double gunWidth1 = 0.3;
	private static final double gunWidth2 = 0.6;
	private static final double gap = 0.1;
	private static final double ballSize = 0.3;


	protected void generate() {
		N = randomInt(minN, maxN);
		B = randomInt(minBalls, maxBalls);

		//Special cases
		if (seed == 1) {
			N = minN;
			B = 2;
		} else if (seed == 2) {
			N = maxN;
			B = maxBalls;
		}

		if (1000 <= seed && seed < 2000) {
			N = ((int)seed - 1000) * (maxN - minN + 1) / 1000 + minN;
			B = ((int)seed - 1000) % (maxBalls - minBalls + 1) + minBalls;
		}

		//User defined N
		if (parameters.isDefined("N")) {
			N = randomInt(parameters.getIntRange("N"), minN, maxN);
		}

		bonusValue = randomInt(minBonusValue, N);
		numFixed = randomInt(minFixed, N);
		numBonus = randomInt(minBonuses, N);

		//Other user defined parameters
		if (parameters.isDefined("B")) {
			B = randomInt(parameters.getIntRange("B"), minBalls, N);
		}
		if (parameters.isDefined("bonusValue")) {
			bonusValue = randomInt(parameters.getIntRange("bonusValue"), 0, N);
		}
		if (parameters.isDefined("numFixed")) {
			numFixed = randomInt(parameters.getIntRange("numFixed"), 0, N);
		}
		if (parameters.isDefined("numBonus")) {
			numBonus = randomInt(parameters.getIntRange("numBonus"), 0, N);
		}


		inputGrid = new char[N][N];
		for (int r = 0; r < N; r++)
			for (int c = 0; c < N; c++)
				inputGrid[r][c] = Empty;

		outputGrid = new char[N][N];

		//add fixed panels
		int fixed = 0;
		while (fixed < numFixed) {
			int r = randomInt(0, N - 1);
			int c = randomInt(0, N - 1);
			if (inputGrid[r][c] != Empty) continue;

			if (randomInt(0, 1) == 0) inputGrid[r][c] = panel1;
			else inputGrid[r][c] = panel2;

			fixed++;
		}

		//add bonus cells
		int bonus = 0;
		while (bonus < numBonus) {
			int r = randomInt(0, N - 1);
			int c = randomInt(0, N - 1);
			if (inputGrid[r][c] != Empty) continue;

			inputGrid[r][c] = Bonus;
			bonus++;
		}


		if (debug) {
			System.out.println("Grid Size, N = " + N);
			System.out.println("B = " + B);
			System.out.println("numFixed = " + numFixed);
			System.out.println("numBonus = " + numBonus);
			System.out.println();
			System.out.println("Input Grid:");
			for (int r = 0; r < N; r++) {
				for (int c = 0; c < N; c++)
					System.out.print(inputGrid[r][c] + " ");
				System.out.println();
			}
		}
	}

	protected boolean isMaximize() {
		return true;
	}

	protected double run() throws Exception {
		init();

		return runAuto();
	}


	protected double runAuto() throws Exception {
		double score = callSolution();
		if (score < 0) {
			if (!isReadActive()) return getErrorScore();
			return fatalError();
		}
		return score;
	}

	protected void updateState() {
		if (hasVis()) {
			synchronized (updateLock) {
				addInfo("Bonus", bonusHits);
				addInfo("Hits", panelHits);
				addInfo("Time", getRunTime());
				addInfo("Score", getScore());
			}
			updateDelay();
		}
	}

	protected void timeout() {
		addInfo("Time", getRunTime());
		update();
	}

	private boolean inGrid(int r, int c) {
		return (r >= 0 && r < N && c >= 0 && c < N);
	}


	private int getScore() {
		return bonusHits * bonusValue + panelHits * panelValue;
	}


	private double callSolution() throws Exception {
		writeLine(N);
		writeLine(B);
		writeLine(bonusValue);
		for (int r = 0; r < N; r++)
			for (int c = 0; c < N; c++)
				writeLine("" + inputGrid[r][c]);

		flush();
		if (!isReadActive()) return -1;

		if (hasVis() && hasDelay()) {
			synchronized (updateLock) {
			}
			updateDelay();
		}


		//read gun locations
		Set<String> used = new HashSet<>();
		Guns = new Loc[B];
		for (int i = 0; i < B; i++) {
			startTime();
			String[] temp = readLine().split(" ");
			stopTime();

			try {
				int r = Integer.parseInt(temp[0]);
				int c = Integer.parseInt(temp[1]);
				if (!(((r == -1 || r == N) && c >= 0 && c < N) ||
						((c == -1 || c == N) && r >= 0 && r < N))) {
					setErrorMessage("Invalid gun location at (" + r + ", " + c + ")");
					return -1;
				}
				if (used.contains(r + " " + c)) {
					setErrorMessage("Gun location at (" + r + ", " + c + ") has already been used");
					return -1;
				}
				used.add(r + " " + c);
				Guns[i] = new Loc(r, c);
			} catch (Exception e) {
				setErrorMessage("Error reading gun locations");
				return -1;
			}
		}

		//read output grid
		for (int r = 0; r < N; r++)
			for (int c = 0; c < N; c++) {
				startTime();
				char cell = readLine().charAt(0);
				stopTime();

				outputGrid[r][c] = cell;

				if (cell != Empty && cell != Bonus && cell != panel1 && cell != panel2) {
					setErrorMessage("Invalid cell " + cell + " at (" + r + ", " + c + ")");
					return -1;
				}
				if (inputGrid[r][c] != Empty && inputGrid[r][c] != outputGrid[r][c]) {
					setErrorMessage("Input and output grids do not match at (" + r + ", " + c + ")");
					return -1;
				}
				if (inputGrid[r][c] == Empty && outputGrid[r][c] == Bonus) {
					setErrorMessage("You cannot place a bonus at (" + r + ", " + c + ")");
					return -1;
				}
			}

		if (debug) {
			System.out.println();
			System.out.println("Gun locations:");
			for (int i = 0; i < B; i++)
				System.out.println((i + 1) + ": " + Guns[i].r + " " + Guns[i].c);
			System.out.println();
			System.out.println("Output Grid:");
			for (int r = 0; r < N; r++) {
				for (int c = 0; c < N; c++)
					System.out.print(outputGrid[r][c] + " ");
				System.out.println();
			}
		}

		History = new ArrayList[B];

		//initialize balls
		Balls = new Loc[B];
		for (int i = 0; i < B; i++) {
			int dir;
			if (Guns[i].r == -1) dir = 3;
			else if (Guns[i].r == N) dir = 1;
			else if (Guns[i].c == -1) dir = 2;
			else dir = 0;

			Balls[i] = new Loc(Guns[i].r, Guns[i].c);
			Balls[i].dir = dir;

			History[i] = new ArrayList<>();
			History[i].add(new Loc(Guns[i].r, Guns[i].c));
		}
		firstDraw = false;
		updateState();    //show output grid


		//simulate and compute score
		loop:
		while (true) {
			for (int i = 0; i < B; i++) {
				Balls[i].r += dr[Balls[i].dir];
				Balls[i].c += dc[Balls[i].dir];
				int r = Balls[i].r;
				int c = Balls[i].c;
				History[i].add(new Loc(r, c));

				//update hits
				if (inGrid(r, c)) {
					if (outputGrid[r][c] == panel1 || outputGrid[r][c] == panel2)
						panelHits++;
					if (outputGrid[r][c] == Bonus)
						bonusHits++;
				}
			}
			updateState();

			//record cells that have multiple balls
			Set<String> multiple = new HashSet<>();
			for (int i = 0; i < B; i++)
				for (int k = i + 1; k < B; k++)
					if (Balls[i].r == Balls[k].r && Balls[i].c == Balls[k].c)
						multiple.add(Balls[i].r + " " + Balls[i].c);


			for (int i = 0; i < B; i++) {
				int r = Balls[i].r;
				int c = Balls[i].c;

				if (!inGrid(r, c)) break loop;        //one of the balls has left the grid

				//perform the bounce
				//NOTE: do not rotate a panel if it is hit by multiple balls at the same time
				if (outputGrid[r][c] == panel1) {
					Balls[i].dir = changeDir1[Balls[i].dir];
					if (inputGrid[r][c] == Empty && !multiple.contains(r + " " + c)) outputGrid[r][c] = panel2;
				} else if (outputGrid[r][c] == panel2) {
					Balls[i].dir = changeDir2[Balls[i].dir];
					if (inputGrid[r][c] == Empty && !multiple.contains(r + " " + c)) outputGrid[r][c] = panel1;
				}
			}
		}

		return getScore();
	}


	protected void paintContent(Graphics2D g) {
		//draw grid
		g.setStroke(new BasicStroke(0.005f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

		g.setColor(Color.white);
		g.fillRect(1, 1, N, N);
		g.setColor(Color.gray);
		for (int i = 1; i <= N + 1; i++) {
			g.drawLine(i, 1, i, N + 1);
			g.drawLine(1, i, N + 1, i);
		}


		if (firstDraw) {
			g.setStroke(new BasicStroke(0.1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			for (int r = 0; r < N; r++)
				for (int c = 0; c < N; c++) {
					g.setColor(Color.magenta);
					if (inputGrid[r][c] == Bonus)
						g.fillRect(c + 1, r + 1, 1, 1);
					if (inputGrid[r][c] == panel1)
						g.draw(new Line2D.Double(c + 1 + 0.1, r + 1 + 0.9, c + 1 + 0.9, r + 1 + 0.1));
					if (inputGrid[r][c] == panel2)
						g.draw(new Line2D.Double(c + 1 + 0.1, r + 1 + 0.1, c + 1 + 0.9, r + 1 + 0.9));
				}

			return;
		}


		Set<String> balls = new HashSet<>();
		for (int i = 0; i < B; i++) balls.add(Balls[i].r + " " + Balls[i].c);

		//draw cells
		g.setStroke(new BasicStroke(0.1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		for (int r = 0; r < N; r++)
			for (int c = 0; c < N; c++) {
				boolean hasBall = balls.contains(r + " " + c);

				if (outputGrid[r][c] == Bonus) {
					g.setColor(Color.magenta);
					if (hasBall) g.setColor(Color.orange);

					g.fillRect(c + 1, r + 1, 1, 1);
				}
				if (outputGrid[r][c] == panel1) {
					if (outputGrid[r][c] == inputGrid[r][c]) g.setColor(Color.magenta);
					else g.setColor(Color.black);

					if (hasBall) g.setColor(Color.orange);

					g.draw(new Line2D.Double(c + 1 + 0.1, r + 1 + 0.9, c + 1 + 0.9, r + 1 + 0.1));
				}
				if (outputGrid[r][c] == panel2) {
					if (outputGrid[r][c] == inputGrid[r][c]) g.setColor(Color.magenta);
					else g.setColor(Color.black);

					if (hasBall) g.setColor(Color.orange);

					g.draw(new Line2D.Double(c + 1 + 0.1, r + 1 + 0.1, c + 1 + 0.9, r + 1 + 0.9));
				}
			}


		//show guns
		for (int i = 0; i < B; i++) {
			int r = Guns[i].r;
			int c = Guns[i].c;

			g.setColor(colors[i]);

			if (r == -1) {
				g.fill(new Rectangle2D.Double(c + 1 + (1 - gunWidth1) / 2.0, r + 1 + gap, gunWidth1, 1 - 2 * gap));
				g.fill(new Ellipse2D.Double(c + 1 + (1 - gunWidth2) / 2.0, r + 1, gunWidth2, gunWidth2));
			}
			if (r == N) {
				g.fill(new Rectangle2D.Double(c + 1 + (1 - gunWidth1) / 2.0, r + 1 + gap, gunWidth1, 1 - 2 * gap));
				g.fill(new Ellipse2D.Double(c + 1 + (1 - gunWidth2) / 2.0, r + 1 + (1 - gunWidth2), gunWidth2, gunWidth2));
			}
			if (c == -1) {
				g.fill(new Rectangle2D.Double(c + 1 + gap, r + 1 + (1 - gunWidth1) / 2.0, 1 - 2 * gap, gunWidth1));
				g.fill(new Ellipse2D.Double(c + 1, r + 1 + (1 - gunWidth2) / 2.0, gunWidth2, gunWidth2));
			}
			if (c == N) {
				g.fill(new Rectangle2D.Double(c + 1 + gap, r + 1 + (1 - gunWidth1) / 2.0, 1 - 2 * gap, gunWidth1));
				g.fill(new Ellipse2D.Double(c + 1 + (1 - gunWidth2), r + 1 + (1 - gunWidth2) / 2.0, gunWidth2, gunWidth2));
			}
		}


		//show balls
		for (int i = 0; i < B; i++) {
			g.setColor(colors[i]);

			double changeR;
			double changeC;
			int r = Balls[i].r;
			int c = Balls[i].c;
			int dir = Balls[i].dir;

			if (!inGrid(r, c)) continue;

			if (outputGrid[r][c] == panel1) {
				if (dirs[dir] == 'L' || dirs[dir] == 'U') {
					changeR = 0.5;
					changeC = 0.5;
				} else {
					changeR = 0.5 - ballSize;
					changeC = 0.5 - ballSize;
				}
			} else if (outputGrid[r][c] == panel2) {
				if (dirs[dir] == 'L' || dirs[dir] == 'D') {
					changeR = 0.5 - ballSize;
					changeC = 0.5;
				} else {
					changeR = 0.5;
					changeC = 0.5 - ballSize;
				}
			} else {
				changeR = (1 - ballSize) / 2.0;
				changeC = (1 - ballSize) / 2.0;
			}

			g.fill(new Ellipse2D.Double(c + 1 + changeC, r + 1 + changeR, ballSize, ballSize));
		}

		//show paths
		if (parameters.isDefined("showPaths")) {
			for (int i = 0; i < B; i++) {
				GeneralPath gp = new GeneralPath();
				Loc L = History[i].get(0);
				gp.moveTo(L.c + 1 + 0.5, L.r + 1 + 0.5);

				for (int k = 1; k < History[i].size(); k++) {
					L = History[i].get(k);
					gp.lineTo(L.c + 1 + 0.5, L.r + 1 + 0.5);
				}

				g.setColor(colorsLight[i]);
				g.draw(gp);
			}
		}
	}


	private void init() {
		bonusHits = 0;
		panelHits = 0;

		if (hasVis()) {
			setDefaultDelay(50);    //this needs to be first, I think

			//setup ball colors
			colors = new Color[]{Color.red, Color.cyan, Color.green, Color.blue, Color.black};
			colorsLight = new Color[colors.length];
			for (int i = 0; i < colors.length; i++)
				colorsLight[i] = new Color(colors[i].getRed(), colors[i].getGreen(), colors[i].getBlue(), 80);

			setContentRect(0, 0, N + 2, N + 2);
			setInfoMaxDimension(15, 12);

			addInfo("Seed", seed);
			addInfo("Size N", N);
			addInfo("Balls B", B);
			addInfo("bonusValue", bonusValue);
			addInfo("numFixed", numFixed);
			addInfo("numBonus", numBonus);
			addInfoBreak();
			addInfo("Bonus", bonusHits);
			addInfo("Hits", panelHits);
			addInfo("Score", getScore());
			addInfoBreak();
			addInfo("Time", "-");
			update();
		}
	}


	public class Loc {
		int r;
		int c;
		int dir;

		public Loc(int r2, int c2) {
			r = r2;
			c = c2;
		}
	}


	public static void main(String[] args) {
		if (Arrays.stream(args).noneMatch("-vis"::equals)) {
			args = Arrays.copyOf(args, args.length + 1);
			args[args.length - 1] = "-novis";
		}
		if (Arrays.stream(args).noneMatch("-exec"::equals)) {
			args = Arrays.copyOf(args, args.length + 2);
			args[args.length - 2] = "-exec";
			args[args.length - 1] = "./main";
		}
		if (Arrays.stream(args).noneMatch("-nopr"::equals)) {
			args = Arrays.copyOf(args, args.length + 1);
			args[args.length - 1] = "-pr";
		}
		new MarathonController().run(args);
	}
}