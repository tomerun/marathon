#!/bin/sh -eux

SOLVER_ID=$1
rm solver.zip || true
zip -r solver.zip *.cpp Tester.java com/ run.sh
aws s3 cp solver.zip s3://marathon-tester/119/$SOLVER_ID/solver.zip 
ruby submit-job.rb $SOLVER_ID