#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <bitset>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 2.2e9;
constexpr ll TL = 5000;
#else
#ifdef TESTER
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 5000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 9600;
#endif
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

constexpr int INF = 1 << 29;
constexpr int START_POS = 1 << 8;
constexpr int TARGET_POS = 2 << 8;
constexpr array<int, 8> DR = {-1, 0, 1, 0, -1, 0, 1, 0};
constexpr array<int, 8> DC = {0, 1, 0, -1, 0, 1, 0, -1};
constexpr array<int, 9> DR8 = {-1, -1, -1, 0, 0, 1, 1, 1};
constexpr array<int, 9> DC8 = {-1, 0, 1, -1, 1, -1, 0, 1};
constexpr int DIR_T = 0;
constexpr int DIR_R = 1;
constexpr int DIR_B = 2;
constexpr int DIR_L = 3;

int N, R, T;

template<class T>
int signum(T v) {
  return (v > 0) - (v < 0);
}

struct Pos {
  int r, c;

  Pos& operator=(const Pos& o) {
    r = o.r;
    c = o.c;
    return *this;
  }
};


bool operator==(const Pos& p1, const Pos& p2) {
  return p1.r == p2.r && p1.c == p2.c;
}

bool operator!=(const Pos& p1, const Pos& p2) {
  return p1.r != p2.r || p1.c != p2.c;
}

struct Result {
  vector<ull> wall;
  int score, rot;

  void flip(int r, int c) {
    wall[r] ^= 1ull << c;
  }

  inline bool is_wall(int r, int c) const {
    return wall[r] & (1ull << c);
  }
};

struct CountBuf {
  array<array<int, 42>, 42> count;
  int turn;

  CountBuf() : turn(0) {
    for (int i = 0; i < N; ++i) {
      fill(count[i].begin(), count[i].begin() + N + 2, 0);
    }
  }

  void clear() {
    turn++;
  }

  bool get(int r, int c) const {
    return count[r][c] == turn;
  }

  void set(int r, int c) {
    count[r][c] = turn;
  }
};

struct UnionFind {
  vvi data;
  vi root;

  UnionFind(int size) {
    data.resize(size);
    root.assign(size, -1);
  }

  void unite(int i, int j) {
    int a = get_root(i);
    int b = get_root(j);
    if (a != b) {
      if (-root[a] < -root[b]) {
        swap(a, b);
      }
      root[a] += root[b];
      root[b] = a;
      data[a].insert(data[a].end(), data[b].begin(), data[b].end());
      data[b].clear();
    }
  }

  bool find(int i, int j) {
    return get_root(i) == get_root(j);
  }

  int get_root(int pos) {
    int r = pos;
    while (root[r] > 0) {
      r = root[r];
    }
    int rr = pos;
    while (root[rr] > 0) {
      int nr = root[rr];
      root[rr] = r;
      rr = nr;
    }
    return r;
  }

  int size(int pos) {
    return -root[get_root(pos)];
  }
};

struct Area {
  Pos entrance;
  int size;
  vector<bool> start;
  vi target;

  Area() : start(N), target(N) {}
};

struct AreaPartition {
  vector<Area> areas;
  vector<vector<Pos>> curtains;
  Pos core;
};

struct AreaPath {
  vector<int> score;
  vector<Pos> wall;
};

struct ShortestPathRobot {
  int score_robot;
  vi target_order;
  vi dist_from_start;
  vvi dist_from_target;
  vector<vvi> path_dir; // path_dir[from_idx][r][c]
};

struct ShortestPath {
  int score_sum;
  vector<ShortestPathRobot> robots;
  vector<vector<uint64_t>> cell_active; // cell_active[r][c] bit i*8+j = robot i segment j
} shortest_path;

constexpr Pos POS_NULL = {0, 0};
constexpr Pos POS_INVALID = {-1, -1};
array<int, 1 << 9> sur_areas;
array<Pos, 6> starts;
array<array<Pos, 6>, 6> targets;
array<array<int, 42>, 42> grid; // (START_POS + robot index) or (TARGET_POS + (target index << 4) + robot index)
array<array<uint8_t, 42>, 42> open_dir;
array<array<int, 42>, 42> dist_map;
CountBuf bfs_counter;
array<int, 2500> general_buf;
int MIN_DIFF_MIN = 9;
int min_diff;

Result empty_result() {
  Result res = {};
  res.wall.assign(N + 2, 0ull);
  res.wall[0] = res.wall[N + 1] = (1ull << (N + 2)) - 1;
  for (int i = 0; i < N; ++i) {
    res.wall[i + 1] = 1ull | (1ull << (N + 1));
  }
  return res;  
}

inline int p2i(int r, int c) {
  return r * (N + 2) + c;
}

inline int get_dir_idx(int dr, int dc) {
  if (dr == -1) return 0;
  if (dr == 1) return 2;
  if (dc == -1) return 3;
  return 1;
}

void print_grid(const Result& res) {
#ifdef DEBUG
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {
      if (res.wall[i] & (1ull << j)) {
        debugStr("#");
      } else if (grid[i][j]) {
        debug("%d", grid[i][j] & 0xF);
      } else {
        debugStr(".");
      }
    }
    debugln();
  }
  debugln();
#endif
}

template<class T>
void rotate_grid(vector<vector<T>>& g) {
  const int n = g.size();
  for (int i = 0; i < n / 2; ++i) {
    for (int j = 0; j < (n + 1) / 2; ++j) {
      T tmp = g[i][j];
      g[i][j] = g[j][n - 1 - i];
      g[j][n - 1 - i] = g[n - 1 - i][n - 1 - j];
      g[n - 1 - i][n - 1 - j] = g[n - 1 - j][i];
      g[n - 1 - j][i] = tmp;
    }
  }
}

void rotate() {
  for (int i = 1; i <= N; ++i) {
    fill(grid[i].begin(), grid[i].begin() + N + 2, 0);
  }
  for (int i = 0; i < R; ++i) {
    starts[i] = {starts[i].c, N - starts[i].r + 1};
    grid[starts[i].r][starts[i].c] = START_POS | i;
    for (int j = 0; j < T; ++j) {
      targets[i][j] = {targets[i][j].c, N - targets[i][j].r + 1};
      grid[targets[i][j].r][targets[i][j].c] = TARGET_POS | (j << 4)  | i;
    }
  }
}

template<bool expect>
void flip_cell_active(int ri, int pti, int nti, int segment_no) {
  // debug("ri:%d pti:%d nti:%d seg:%d expect:%d\n", ri, pti, nti, segment_no, expect);
  Pos sp, ep;
  if (segment_no == 0) {
    sp = starts[ri];
    ep = targets[ri][nti];
  } else {
    if (pti > nti) {
      swap(pti, nti);
    }
    sp = targets[ri][pti];
    ep = targets[ri][nti];
  }
  const ull bit = 1ull << (ri * 8 + segment_no);
  const vvi& path_dir = shortest_path.robots[ri].path_dir[pti + 1];
  while (ep != sp) {
    int dir = path_dir[ep.r][ep.c];
    ep.r -= DR[dir];
    ep.c -= DC[dir];
    // debug("(%d %d)\n", ep.r, ep.c);
    assert((bool)(shortest_path.cell_active[ep.r][ep.c] & bit) == expect);
    shortest_path.cell_active[ep.r][ep.c] ^= bit;
  }
}

template<bool set_info>
void calc_dist(const vector<ull>& wall, Pos& from, int ri, int min_ti, array<int, 6>& ar) {
  bfs_counter.clear();
  bfs_counter.set(from.r, from.c);
  int qs = 1;
  int qi = 0;
  general_buf[0] = (open_dir[from.r][from.c] << 16) | (from.r << 8) | from.c;
  int visited = 0;
  for (int i = 1; qi < qs; ++i) {
    for (int cqs = qs; qi < cqs; ++qi) {
      int cpos = general_buf[qi];
      int cr = (cpos >> 8) & 0xFF;
      int cc = cpos & 0xFF;
      int od = cpos >> 16;
      assert(od);
      do {
        int dir = __builtin_ctz(od);
        od &= od - 1;
        int nr = cr + DR[dir];
        int nc = cc + DC[dir];
        if (bfs_counter.get(nr, nc)) continue;
        bfs_counter.set(nr, nc);
        assert(open_dir[nr][nc] & (1 << (dir ^ 2)));
        int nd = open_dir[nr][nc] - (1 << (dir ^ 2));
        if (nd) {
          general_buf[qs++] = (nd << 16) | (nr << 8) | nc;
        }
        if (set_info) {
          shortest_path.robots[ri].path_dir[min_ti][nr][nc] = dir;
        }
        if ((grid[nr][nc] & TARGET_POS) && (grid[nr][nc] & 0xF) == ri) {
          int ti = (grid[nr][nc] >> 4) & 0xF;
          if (ti >= min_ti) {
            ar[ti] = i;
            ++visited;
            if (visited == T - min_ti) return;
          }
        }
      } while (od);
    }
  }
  assert(false);
}

template<bool set_info>
int calc_score_robot(const vector<ull>& wall, int ri) {
  static array<int, 6> dist_from_start;
  static array<array<int, 6>, 6> dist_target;
  static array<array<int, 1 << 6>, 6> dp;
  static array<array<int, 1 << 6>, 6> dp_prev;
  START_TIMER(11);
  calc_dist<set_info>(wall, starts[ri], ri, 0, dist_from_start);
  for (int i = 0; i < T; ++i) {
    if (i != T - 1) calc_dist<set_info>(wall, targets[ri][i], ri, i + 1, dist_target[i]);
    for (int j = 0; j < i; ++j) {
      dist_target[i][j] = dist_target[j][i];
    }
  }
  if (set_info) {
    shortest_path.robots[ri].dist_from_start.assign(dist_from_start.begin(), dist_from_start.begin() + T);
    for (int i = 0; i < T; ++i) {
      shortest_path.robots[ri].dist_from_target[i].assign(dist_target[i].begin(), dist_target[i].begin() + T);
    }
  }
  STOP_TIMER(11);
  START_TIMER(12);
  for (int i = 0; i < T; ++i) {
    fill(dp[i].begin(), dp[i].begin() + (1 << T), INF);
    dp[i][1 << i] = dist_from_start[i];
    dp_prev[i][1 << i] = -1;
  }
  for (int i = 1; i < (1 << T); ++i) {
    int cur = i;
    while (cur > 0) {
      int prev = __builtin_ctz(cur);
      cur &= cur - 1;
      int rev = (1 << T) - 1 - i;
      while (rev > 0) {
        int next = __builtin_ctz(rev);
        rev &= rev - 1;
        const int new_v = dp[prev][i] + dist_target[prev][next];
        int& old_v = dp[next][i | (1 << next)];
        if (new_v < old_v) {
          old_v = new_v;
          dp_prev[next][i | (1 << next)] = prev;
        }
      }
    }
  }

  int ret = INF;
  int ti = 0;
  for (int i = 0; i < T; ++i) {
    if (dp[i][(1 << T) - 1] < ret) {
      ret = dp[i][(1 << T) - 1];
      ti = i;
    }
  }
  if (set_info) {
    int oi = T - 1;
    int state = (1 << T) - 1;
    while (true) {
      shortest_path.robots[ri].target_order[oi--] = ti;
      int pti = dp_prev[ti][state];
      if (pti == -1) break;
      assert(state & (1 << ti));
      state ^= 1 << ti;
      flip_cell_active<false>(ri, pti, ti, oi + 1);
      ti = pti;
    }
    flip_cell_active<false>(ri, -1, ti, 0);
    assert(oi == -1);
  }
  if (set_info) {
    shortest_path.robots[ri].score_robot = ret;
  }
  STOP_TIMER(12);
  // debug("ri:%d score:%d\n", ri, ret);
  return ret;
}

template<bool set_info = false>
int calc_score(const Result& res) {
  START_TIMER(13);
  if (set_info) {
    for (int i = 1; i <= N; ++i) {
      auto& row = shortest_path.cell_active[i];
      fill(row.begin(), row.end(), 0);
    }
  }
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {
      if (res.is_wall(i, j)) continue;
      open_dir[i][j] = 0xF;
      if (res.is_wall(i + DR[0], j + DC[0])) open_dir[i][j] -= 1;
      if (res.is_wall(i + DR[1], j + DC[1])) open_dir[i][j] -= 1 << 1;
      if (res.is_wall(i + DR[2], j + DC[2])) open_dir[i][j] -= 1 << 2;
      if (res.is_wall(i + DR[3], j + DC[3])) open_dir[i][j] -= 1 << 3;
    }
  }
  if (set_info) {
    for (int i = 1; i <= N; ++i) {
      shortest_path.cell_active[i].assign(N + 2, 0ull);
    }
  }
  STOP_TIMER(13);
  int score = 0;
  for (int i = 0; i < R; ++i) {
    int score_robot = calc_score_robot<set_info>(res.wall, i);
    score += score_robot;
  }
  if (set_info) {
    shortest_path.score_sum = score;
  }
  return score;
}

Pos select_open_pos(const Result& res, const Pos& close_pos) {
  int bits = (int)(((res.wall[close_pos.r - 1] >> (close_pos.c - 1)) & 0x7)
                | (((res.wall[close_pos.r    ] >> (close_pos.c - 1)) & 0x7) << 3)
                | (((res.wall[close_pos.r + 1] >> (close_pos.c - 1)) & 0x7) << 6));
  if (sur_areas[bits] < 2) {
    return POS_NULL;
  }
  if (sur_areas[bits] > 2) {
    return POS_INVALID;
  }
  bfs_counter.clear();
  const int version = bfs_counter.turn;
  int adj = 0;
  for (; adj < 4; ++adj) {
    int sr = close_pos.r + DR[adj];
    int sc = close_pos.c + DC[adj];
    if (res.is_wall(sr, sc)) continue;
    int qs = 1;
    general_buf[0] = (sr << 8) | sc;
    bfs_counter.set(sr, sc);
    for (int qi = 0; qi < qs; ++qi) {
      int cr = general_buf[qi] >> 8;
      int cc = general_buf[qi] & 0xFF;
      for (int i = 0; i < 4; ++i) {
        int nr = cr + DR[i];
        int nc = cc + DC[i];
        if (bfs_counter.get(nr, nc)) continue;
        bfs_counter.set(nr, nc);
        if (res.is_wall(nr, nc)) {
          continue;
        }
        general_buf[qs++] = (nr << 8) | nc;
      }
    }
    break;
  }
  adj++;
  static vector<int> cands;
  cands.clear();
  bfs_counter.clear();
  bfs_counter.set(close_pos.r, close_pos.c); // skip close pos
  for (int i = 1; i <= N; ++i) {
    bfs_counter.set(0, i);
    bfs_counter.set(N + 1, i);
    bfs_counter.set(i, 0);
    bfs_counter.set(i, N + 1);
  }
  for (; adj < 4; ++adj) {
    int sr = close_pos.r + DR[adj];
    int sc = close_pos.c + DC[adj];
    if (res.is_wall(sr, sc)) continue;
    if (bfs_counter.count[sr][sc] == version) continue;
    int qs = 1;
    general_buf[0] = (sr << 8) | sc;
    bfs_counter.set(sr, sc);
    for (int qi = 0; qi < qs; ++qi) {
      int cr = general_buf[qi] >> 8;
      int cc = general_buf[qi] & 0xFF;
      for (int i = 0; i < 4; ++i) {
        int nr = cr + DR[i];
        int nc = cc + DC[i];
        if (bfs_counter.get(nr, nc)) continue;
        if (res.is_wall(nr, nc)) {
          if (bfs_counter.count[nr][nc] == version) {
            cands.push_back((nr << 8) | nc);
          }
          bfs_counter.set(nr, nc);
          continue;
        }
        bfs_counter.set(nr, nc);
        general_buf[qs++] = (nr << 8) | nc;
      }
    }
    break;
  }
  if (cands.empty()) {
    return POS_INVALID;
  }
  int ret = cands[rnd.nextUInt(cands.size())];
  return {ret >> 8, ret & 0xFF};
}

void close_inactive_cell(Result& res) {
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {
      if (!res.is_wall(i, j) && grid[i][j] == 0 && !shortest_path.cell_active[i][j]) {
        // debug("closed (%d %d)\n", i, j);
        res.flip(i, j);
        for (int k = 0; k < 4; ++k) {
          open_dir[i + DR[k]][j + DC[k]] ^= 1 << (k ^ 2);
        }
      }
    }
  }
}

bool find_connectivity(const Result& res, const Pos& close_pos, int dir) {
  // debug("find_connectivity: (%d %d) %d\n", close_pos.r, close_pos.c, dir);
  const int sr = close_pos.r + DR[dir];
  const int sc = close_pos.c + DC[dir];
  bfs_counter.clear();
  bfs_counter.set(sr, sc);
  int qs = 1;
  int qi = 0;
  int sur_start = 0;
  for (int i = 0; i < 4; ++i) {
    if (i != dir && !res.is_wall(close_pos.r + DR[i] , close_pos.c + DC[i])) {
      sur_start++;
    }
  }
  general_buf[0] = (open_dir[sr][sc] << 16) | (sr << 8) | sc;
  for (int i = 1; qi < qs; ++i) {
    for (int cqs = qs; qi < cqs; ++qi) {
      int cpos = general_buf[qi];
      int cr = (cpos >> 8) & 0xFF;
      int cc = cpos & 0xFF;
      int od = cpos >> 16;
      // debug("%d %d %d\n", cr, cc, od);
      while (od) {
        int dir = __builtin_ctz(od);
        od &= od - 1;
        int nr = cr + DR[dir];
        int nc = cc + DC[dir];
        if (bfs_counter.get(nr, nc)) continue;
        bfs_counter.set(nr, nc);
        if (abs(nr - close_pos.r) + abs(nc - close_pos.c) == 1) {
          sur_start--;
          if (sur_start == 0) {
            return true;
          }
        }
        assert(open_dir[nr][nc] & (1 << (dir ^ 2)));
        int nd = open_dir[nr][nc] - (1 << (dir ^ 2));
        if (nd) {
          general_buf[qs++] = (nd << 16) | (nr << 8) | nc;
        }
      }
    }
  }
  return false;
}

void update_connected(Result& res, const Pos& close_pos) {
  // debug("update_connected: (%d %d)\n", close_pos.r, close_pos.c);
  const ull active = shortest_path.cell_active[close_pos.r][close_pos.c];
  int new_score = 0;
  for (int i = 0; i < R; ++i) {
    // debug("update_path:robot %d\n", i);;
    int bits = (active >> (i * 8)) & 0xFF;
    if (bits == 0) {
      new_score += shortest_path.robots[i].score_robot;
      continue;
    }
    flip_cell_active<true>(i, -1, shortest_path.robots[i].target_order[0], 0);
    for (int j = 1; j < T; ++j) {
      flip_cell_active<true>(i, shortest_path.robots[i].target_order[j - 1], shortest_path.robots[i].target_order[j], j);
    }
    new_score += calc_score_robot<true>(res.wall, i); // TODO: can be optimized?
  }
  // debug("update_connected: %d -> %d\n", shortest_path.score_sum, new_score);
  shortest_path.score_sum = new_score;
  close_inactive_cell(res);
}

void update_typeL(vvi& path_dir_seg, const Pos& pc, const Pos& p0, const Pos& p1, const Pos& p2) {
  int dir = path_dir_seg[pc.r][pc.c];
  Pos prev_pos = {pc.r - DR[dir], pc.c - DC[dir]};
  if (prev_pos == p0) {
    path_dir_seg[p1.r][p1.c] = get_dir_idx(p1.r - p0.r, p1.c - p0.c);
    path_dir_seg[p2.r][p2.c] = get_dir_idx(p2.r - p1.r, p2.c - p1.c);
  } else {
    assert(prev_pos == p2);
    path_dir_seg[p1.r][p1.c] = get_dir_idx(p1.r - p2.r, p1.c - p2.c);
    path_dir_seg[p0.r][p0.c] = get_dir_idx(p0.r - p1.r, p0.c - p1.c);
  }
}

inline bool sur_is_simple_path(int sur) {
  return sur == 1 + 2 || sur == 2 + 4 || sur == 4 + 8 || sur == 8 + 1;
}

int estimate_score_ub(const Result& res, const Pos& sp) {
  // debug("estimate_score_ub:(%d %d)\n", sp.r, sp.c);
  // print_grid(res);
  START_TIMER(25);
  static CountBuf visited;
  visited.clear();
  visited.set(sp.r, sp.c);
  int qs = 1;
  int qi = 0;
  dist_map[sp.r][sp.c] = 0;
  general_buf[0] = (open_dir[sp.r][sp.c] << 16) | (sp.r << 8) | sp.c;
  for (int i = 1; qi < qs; ++i) {
    for (int cqs = qs; qi < cqs; ++qi) {
      int cpos = general_buf[qi];
      int cr = (cpos >> 8) & 0xFF;
      int cc = cpos & 0xFF;
      int od = cpos >> 16;
      // debug("%d %d %d\n", cr, cc, od);
      while (od) {
        int dir = __builtin_ctz(od);
        od &= od - 1;
        int nr = cr + DR[dir];
        int nc = cc + DC[dir];
        if (visited.get(nr, nc)) continue;
        visited.set(nr, nc);
        dist_map[nr][nc] = i;
        assert(open_dir[nr][nc] & (1 << (dir ^ 2)));
        int nd = open_dir[nr][nc] - (1 << (dir ^ 2));
        if (nd) {
          general_buf[qs++] = (nd << 16) | (nr << 8) | nc;
        }
      }
    }
  }

  int diff = 0;
  for (int i = 0; i < R; ++i) {
    const ShortestPathRobot& robot = shortest_path.robots[i];
    int ti = robot.target_order[0];
    bool find1 = bfs_counter.get(starts[i].r, starts[i].c);
    bool find2 = bfs_counter.get(targets[i][ti].r, targets[i][ti].c);
    if (find1 != find2) {
      diff += dist_map[starts[i].r][starts[i].c] + dist_map[targets[i][ti].r][targets[i][ti].c];
      diff -= robot.dist_from_start[ti];
    }
    for (int j = 1; j < T; ++j) {
      int pti = robot.target_order[j - 1];
      int nti = robot.target_order[j];
      find1 = bfs_counter.get(targets[i][pti].r, targets[i][pti].c);
      find2 = bfs_counter.get(targets[i][nti].r, targets[i][nti].c);
      if (find1 != find2) {
        diff += dist_map[targets[i][pti].r][targets[i][pti].c] + dist_map[targets[i][nti].r][targets[i][nti].c];
        diff -= robot.dist_from_target[pti][nti];
      }
    }
  }
  // debug("prune:%d %d\n", diff, new_score - old_score);
  // assert(diff >= calc_score<false>(res) - shortest_path.score_sum);
  STOP_TIMER(25);
  return diff;
}

void update_separated(Result& res, const Pos& close_pos, int dir, vector<Pos>& hole) {
  // debug("update_separated: (%d %d) %d\n", close_pos.r, close_pos.c, dir);
  START_TIMER(23);
  static CountBuf visited;
  visited.clear();
  visited.set(close_pos.r + DR[dir], close_pos.c + DC[dir]);
  array<int, 8> dirs = {};
  assert(hole.empty());
  hole.push_back({close_pos.r + DR[dir], close_pos.c + DC[dir]});
  Pos finish_pos;
  while (true) {
    int cr = hole.back().r;
    int cc = hole.back().c;
    int as = 0;
    bool finish = false;
    for (int i = 0; i < 4; ++i) {
      int nr = cr + DR[i];
      int nc = cc + DC[i];
      if (nr == 0 || nr == N + 1 || nc == 0 || nc == N + 1) continue;
      if (nr == close_pos.r && nc == close_pos.c) continue;
      if (!res.is_wall(nr, nc)) {
        if (bfs_counter.get(nr, nc)) {
          finish_pos = {nr, nc};
          finish = true;
          break;
        } else {
          continue;
        }
      }
      if (visited.get(nr, nc)) continue;
      bool con_self = false;
      for (int j = 0; j < 4; ++j) {
        if (j == (i ^ 2)) continue;
        int ar = nr + DR[j];
        int ac = nc + DC[j];
        if ((!res.is_wall(ar, ac) && !bfs_counter.get(ar, ac)) || visited.get(ar, ac)) {
          con_self = true;
          break;
        }
      }
      if (!con_self) {
        dirs[as++] = i;
      }
    }
    if (finish) break;
    if (as == 0) {
      ADD_COUNTER(3);
      ADD_COUNTER(10 + hole.size());
      // debug("fail at (%d %d)\n", hole.back().r, hole.back().c);
      hole.clear();
      return;
    }
    if (as > 1) {
      swap(dirs[0], dirs[rnd.nextUInt(as)]);
    }
    hole.push_back({cr + DR[dirs[0]], cc + DC[dirs[0]]});
    visited.set(cr + DR[dirs[0]], cc + DC[dirs[0]]);
  }
  for (int i = 1; i < hole.size(); ++i) {
    // debug("(%d %d)\n", hole[i].r, hole[i].c);
    assert(res.is_wall(hole[i].r, hole[i].c));
    open_dir[hole[i].r][hole[i].c] = 0;
    res.flip(hole[i].r, hole[i].c);
    for (int j = 0; j < 4; ++j) {
      int nr = hole[i].r + DR[j];
      int nc = hole[i].c + DC[j];
      if (!res.is_wall(nr, nc)) {
        open_dir[nr][nc] ^= 1 << (j ^ 2);
        open_dir[hole[i].r][hole[i].c] ^= 1 << j;
      }
    }
  }
  STOP_TIMER(23);
  // pruning
  START_TIMER(24);
  if (hole.size() == 2 && abs(finish_pos.r - close_pos.r) + abs(finish_pos.c - close_pos.c) == 1) {
    int sur_hole = 0;
    int sur_close = 0;
    for (int i = 0; i < 4; ++i) {
      if (!res.is_wall(hole[1].r + DR[i], hole[1].c + DC[i])) {
        sur_hole |= 1 << i;
      }
      if (!res.is_wall(close_pos.r + DR[i], close_pos.c + DC[i])) {
        sur_close |= 1 << i;
      }
    }
    if (sur_is_simple_path(sur_hole) && sur_is_simple_path(sur_close)) {
      ADD_COUNTER(1);
      // debug("type L: (%d %d) (%d %d)\n", hole[0].r, hole[0].c, hole[1].r, hole[1].c);
      // print_grid(res);
      const ull active = shortest_path.cell_active[close_pos.r][close_pos.c];
      shortest_path.cell_active[hole[1].r][hole[1].c] = active;
      shortest_path.cell_active[close_pos.r][close_pos.c] = 0;
      for (int i = 0; i < R; ++i) {
        if (active & (1ull << (i * 8))) {
          // debug("update_typeL ri:%d from:%d (%d %d) (%d %d) (%d %d) (%d %d)\n",
          //   i, 0, close_pos.r, close_pos.c, hole[0].r, hole[0].c, hole[1].r, hole[1].c, finish_pos.r, finish_pos.c);
          update_typeL(shortest_path.robots[i].path_dir[0], close_pos, hole[0], hole[1], finish_pos);
        }
        for (int j = 1; j < T; ++j) {
          if (active & (1ull << (i * 8 + j))) {
            const int pti = shortest_path.robots[i].target_order[j - 1];
            const int nti = shortest_path.robots[i].target_order[j];
            // debug("update_typeL ri:%d from:%d (%d %d) (%d %d) (%d %d) (%d %d)\n",
            //   i, min(pti, nti) + 1, close_pos.r, close_pos.c, hole[0].r, hole[0].c, hole[1].r, hole[1].c, finish_pos.r, finish_pos.c);
            update_typeL(shortest_path.robots[i].path_dir[min(pti, nti) + 1], close_pos, hole[0], hole[1], finish_pos);
          }
        }
      }
      STOP_TIMER(24);
      return;
    }
  }
  STOP_TIMER(24);

  assert(min_diff > 0);
  int threshold = -rnd.nextUInt(min_diff);
  int est_diff = estimate_score_ub(res, hole[0]);
  if (est_diff >= threshold) {
    START_TIMER(26);
    int new_score = calc_score<false>(res);
    STOP_TIMER(26);
    // debug("score %d->%d\n", shortest_path.score_sum, new_score);
    if (new_score - shortest_path.score_sum >= threshold) {
      START_TIMER(27);
      calc_score<true>(res);
      close_inactive_cell(res);
      STOP_TIMER(27);
      return;
    }
  } else {
    ADD_COUNTER(2);
  }
  for (int i = (int)hole.size() - 1; i > 0; --i) {
    assert(!res.is_wall(hole[i].r, hole[i].c));
    res.flip(hole[i].r, hole[i].c);
    for (int j = 0; j < 4; ++j) {
      int nr = hole[i].r + DR[j];
      int nc = hole[i].c + DC[j];
      if (!res.is_wall(nr, nc)) {
        assert(open_dir[nr][nc] & (1 << (j ^ 2)));
        open_dir[nr][nc] ^= 1 << (j ^ 2);
      }
    }
  }
  hole.clear();
}

void improve(Result& res, ll timelimit, bool final) {
  debug("init_score:%d\n", res.score);
  shortest_path.robots.resize(R);
  shortest_path.cell_active.assign(N + 2, vector<uint64_t>(N + 2));
  for (int i = 0; i < R; ++i) {
    shortest_path.robots[i].target_order.resize(T);
    shortest_path.robots[i].dist_from_start.resize(T);
    shortest_path.robots[i].dist_from_target.assign(T, vi(T));
    shortest_path.robots[i].path_dir.assign(T + 1, vvi(N + 2, vi(N + 2)));
  }
  calc_score<true>(res);
  vector<Pos> active_pos;
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {
      if (grid[i][j]) continue;
      if (res.is_wall(i, j)) continue;
      if (shortest_path.cell_active[i][j]) {
        active_pos.push_back({i, j});
      } else {
        res.flip(i, j);
        for (int k = 0; k < 4; ++k) {
          open_dir[i + DR[k]][j + DC[k]] ^= 1 << (k ^ 2);
        }
      }
    }
  }
  print_grid(res);
  if (active_pos.empty()) return;
  vector<Pos> hole;
  min_diff = final ? 1 : MIN_DIFF_MIN;
  const ll begin_time = get_elapsed_msec();
  for (int turn = 0; ; ++turn) {
    ADD_COUNTER(0);
    if ((turn & 0xFF) == 0) {
      const ll elapsed = get_elapsed_msec();
      if (elapsed >= timelimit) {
        debug("turn:%d\n", turn);
        res.score = shortest_path.score_sum;
        print_grid(res);
#ifdef DEBUG
        // TODO: need to care?
        // for (int i = 0; i < active_pos.size(); ++i) {
        //   for (int j = 0; j < i; ++j) {
        //     if (active_pos[i] == active_pos[j]) {
        //       debugStr("active_pos\n");
        //       for (Pos& p : active_pos) {
        //         debug("%d %d\n", p.r, p.c);
        //       }
        //       assert(false);
        //     }
        //   }
        // }
#endif
        break;
      }
      double ratio = 1.0 * (elapsed - begin_time) / (timelimit - begin_time);
      if (!final) min_diff = MIN_DIFF_MIN * (1.0 - ratio) + 1;
    }
    const int api = rnd.nextUInt(active_pos.size());
    const Pos close_pos = active_pos[api];
    if (res.is_wall(close_pos.r, close_pos.c)) {
      swap(active_pos[api], active_pos.back());
      active_pos.pop_back();
      continue;
    }
    const int sur_bits = (int)(((res.wall[close_pos.r - 1] >> (close_pos.c - 1)) & 0x7)
                            | (((res.wall[close_pos.r    ] >> (close_pos.c - 1)) & 0x7) << 3)
                            | (((res.wall[close_pos.r + 1] >> (close_pos.c - 1)) & 0x7) << 6));
    if (sur_areas[sur_bits] > 2) {
      continue;
    }
    // debug("(%d %d), %d %x\n", close_pos.r, close_pos.c, sur_areas[sur_bits], sur_bits);
    res.flip(close_pos.r, close_pos.c);
    for (int i = 0; i < 4; ++i) {
      if (!res.is_wall(close_pos.r + DR[i], close_pos.c + DC[i])) {
        assert(open_dir[close_pos.r + DR[i]][close_pos.c + DC[i]] & (1 << (i ^ 2)));
        open_dir[close_pos.r + DR[i]][close_pos.c + DC[i]] ^= 1 << (i ^ 2);
      }
    }
    const int dir_base = rnd.nextUInt() & 3;
    int di = 0;
    bool connected = false;
    for (; di < 4; ++di) {
      const int dir = (dir_base + di) & 3;
      if (res.is_wall(close_pos.r + DR[dir], close_pos.c + DC[dir])) continue;
      START_TIMER(20);
      if (find_connectivity(res, close_pos, dir)) {
        update_connected(res, close_pos);
        assert(res.is_wall(active_pos[api].r, active_pos[api].c));
        swap(active_pos[api], active_pos.back());
        active_pos.pop_back();
        connected = true;
      }
      STOP_TIMER(20);
      break;
    }
    if (connected) continue;
    di++;
    bool done = false;
    for (; di < 4; ++di) {
      const int dir = (dir_base + di) & 3;
      if (res.is_wall(close_pos.r + DR[dir], close_pos.c + DC[dir])) continue;
      if (bfs_counter.get(close_pos.r + DR[dir], close_pos.c + DC[dir])) continue;
      int old_score = shortest_path.score_sum;
      START_TIMER(21);
      update_separated(res, close_pos, dir, hole);
      STOP_TIMER(21);
      if (hole.empty()) {
        // revert
        res.flip(close_pos.r, close_pos.c);
        for (int i = 0; i < 4; ++i) {
          if (!res.is_wall(close_pos.r + DR[i], close_pos.c + DC[i])) {
            assert((open_dir[close_pos.r + DR[i]][close_pos.c + DC[i]] & (1 << (i ^ 2))) == 0);
            open_dir[close_pos.r + DR[i]][close_pos.c + DC[i]] ^= 1 << (i ^ 2);
          }
        }
      } else {
        swap(active_pos[api], active_pos.back());
        active_pos.pop_back();
        active_pos.insert(active_pos.end(), hole.begin() + 1, hole.end());
        if (shortest_path.score_sum > old_score) {
          debug("score %d -> %d at turn %d\n", old_score, shortest_path.score_sum, turn);
          // print_grid(res);
        }
        hole.clear();
      }
      done = true;
      break;
    }
// #ifdef DEBUG
//     for (int i = 1; i <= N; ++i) {
//       for (int j = 1; j <= N; ++j) {
//         if (res.is_wall(i, j)) continue;
//         int sur = 0;
//         for (int k = 0; k < 4; ++k) {
//           if (!res.is_wall(i + DR[k], j + DC[k])) {
//             sur |= 1 << k;
//           }
//         }
//         if (sur != open_dir[i][j]) {
//           print_grid(res);
//           debug("fail:(%d %d) %x %x\n", i, j, sur, open_dir[i][j]);
//           assert(false);
//         }
//       }
//     }
// #endif
    assert(done);
  }
}

Result create_initial_solution(int init_len) {
  const int TBD = 0;
  const int WALL = 1;
  const int EMPTY = 2;
  vvi cell(N + 2, vi(N + 2, TBD));
  fill(cell[0].begin(), cell[0].end(), WALL);
  fill(cell[N + 1].begin(), cell[N + 1].end(), WALL);
  for (int i = 1; i <= N; ++i) {
    cell[i][0] = cell[i][N + 1] = WALL;
  }
  Pos cur = {1, 1};
  const array<int, 4> dirs1_1 = {DIR_T, DIR_R, DIR_L, DIR_B};
  const array<int, 4> dirs1_2 = {DIR_L, DIR_T, DIR_R, DIR_B};
  const array<int, 4> dirs2_1 = {DIR_L, DIR_B, DIR_T, DIR_R};
  const array<int, 4> dirs2_2 = {DIR_T, DIR_L, DIR_B, DIR_R};
  vector<Pos> lst;
  vector<Pos> stk;
  for (int i = 0; i < init_len; ++i) {
    lst.push_back({1, 1 + i});
    cell[1][1 + i] = EMPTY;
  }
  bool initial = true;
  while (true) {
    // bottom-left -> top-right
    if (!initial) {
      lst = {cur};
      stk = {cur};
      while (!stk.empty()) {
        cur = stk.back();
        if (cur.r == 1 || cur.c == N) {
          break;
        }
        const auto& dirs = cur.c < cur.r ? dirs1_1 : dirs1_2;
        for (int i = 0; i < 4; ++i) {
          const int dir = dirs[i];
          int nr = cur.r + DR[dir];
          int nc = cur.c + DC[dir];
          if (cell[nr][nc] != TBD) continue;
          cell[nr][nc] = EMPTY;
          lst.push_back({nr, nc});
          stk.push_back({nr, nc});
          break;
        }
        if (stk.back() == cur) {
          stk.pop_back();
        }
      }
    }
    int wall_pos = 0;
    for (int i = 0; i < lst.size(); ++i) {
      for (int j = 0; j < 4; ++j) {
        int nr = lst[i].r + DR[j];
        int nc = lst[i].c + DC[j];
        if (cell[nr][nc] != TBD) continue;
        if (grid[nr][nc]) {
          cell[nr][nc] = EMPTY;
          lst.push_back({nr, nc});
        } else {
          cell[nr][nc] = WALL;
          int wp = (nr << 8) + nc;
          if ((nr == 1 || nc == N) && wall_pos < wp) wall_pos = wp;
        }
      }
    }
    if (!initial && lst.size() == 1) break;
    if (wall_pos == 0) {
      break;
    }
    cur = {wall_pos >> 8, wall_pos & 0xFF};
    cell[cur.r][cur.c] = EMPTY;
    initial = false;

    // top-right -> bottom-left
    lst = {cur};
    stk = {cur};
    while (!stk.empty()) {
      cur = stk.back();
      if (cur.r == N || cur.c == 1) {
        break;
      }
      const auto& dirs = cur.r < cur.c ? dirs2_1 : dirs2_2;
      for (int i = 0; i < 4; ++i) {
        const int dir = dirs[i];
        int nr = cur.r + DR[dir];
        int nc = cur.c + DC[dir];
        if (cell[nr][nc] != TBD) continue;
        cell[nr][nc] = EMPTY;
        lst.push_back({nr, nc});
        stk.push_back({nr, nc});
        break;
      }
      if (stk.back() == cur) {
        stk.pop_back();
      }
    }
    wall_pos = 0;
    for (int i = 0; i < lst.size(); ++i) {
      for (int j = 0; j < 4; ++j) {
        int nr = lst[i].r + DR[j];
        int nc = lst[i].c + DC[j];
        if (cell[nr][nc] != TBD) continue;
        if (grid[nr][nc]) {
          cell[nr][nc] = EMPTY;
          lst.push_back({nr, nc});
        } else {
          cell[nr][nc] = WALL;
          int wp = (nr << 8) + nc;
          if ((nr == N || nc == 1) && wall_pos < wp) wall_pos = wp;
        }
      }
    }
    if (lst.size() == 1) break;
    if (wall_pos == 0) {
      break;
    }
    cur = {wall_pos >> 8, wall_pos & 0xFF};
    cell[cur.r][cur.c] = EMPTY;
  }
  // connect undetermined area
  vector<Pos> tbds;
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {
      if (cell[i][j] == TBD) tbds.push_back({i, j});
    }
  }
  for (int i = 0; i < tbds.size(); ++i) {
    int r = tbds[i].r;
    int c = tbds[i].c;
    if (cell[r][c] == EMPTY) continue;
    for (int j = 0; j < 4; ++j) {
      int nr = r + DR[j];
      int nc = c + DC[j];
      if (nr == 0 || nr == N + 1 || nc == 0 || nc == N + 1 || cell[nr][nc] != WALL) continue;
      bool ok = false;
      for (int k = 0; k < 4; ++k) {
        if (cell[nr + DR[k]][nc + DC[k]] == EMPTY) ok = true;
      }
      assert(ok);
      cell[nr][nc] = EMPTY;
      general_buf[0] = (r << 8) | c;
      int qs = 1;
      for (int qi = 0; qi < qs; ++qi) {
        int cr = general_buf[qi] >> 8;
        int cc = general_buf[qi] & 0xFF;
        for (int k = 0; k < 4; ++k) {
          int ar = cr + DR[k];
          int ac = cc + DC[k];
          if (cell[ar][ac] == TBD) {
            cell[ar][ac] = EMPTY;
            general_buf[qs++] = (ar << 8) | ac;
          }
        }
      }
    }
  }

  Result res = empty_result();
  for (int i = 1; i <= N; ++i) {
    for (int j = 1; j <= N; ++j) {
      if (cell[i][j] == WALL) res.wall[i] |= (1ull << j);
    }
  }
  res.score = calc_score(res);
  return res;
}

Result solve_single_diag(int shift) {
  Result res = empty_result();
  int parity = 0;
  for (int sc = -N + shift; sc <= N; sc += 3, parity++) {
    vector<Pos> put;
    int r, c;
    if (sc < 1) {
      r = 1 + (1 - sc);
      c = 1;
    } else {
      r = 1;
      c = sc;
    }
    bool any = false;
    while (r <= N && c <= N) {
      if (grid[r][c] != 0) {
        any = true;
      } else {
        put.push_back({r, c});
      }
      r += 1;
      c += 1;
    }
    if (put.empty()) continue;
    if (!any) {
      if (parity & 1) {
        put.pop_back();
      } else {
        put.erase(put.begin());
      }
    }
    for (const Pos& p : put) {
      res.wall[p.r] |= 1ull << p.c;
    }
  }
  res.score = calc_score(res);
  return res;
}

int evaluate_partition(const AreaPartition& ap) {
  int eval = 0;
  for (int i = 0; i < R; ++i) {
    int sum = 0;
    int max_size = 0;
    for (int j = 0; j < ap.areas.size(); ++j) {
      const Area& area = ap.areas[j];
      if (area.start[i]) {
        if (area.target[i]) {
          sum += area.size;
        } else {
          sum += area.size / 2;
        }
      } else if (area.target[i]) {
        sum += area.size * 2;
        max_size = max(max_size, area.size);
      }
    }
    eval += sum - max_size;
  }
  return eval;
}

template<int S>
void set_destination(Pos& core, array<Pos, S>& destination) {
  const int cr = core.r;
  const int cc = core.c;
  int dest_pos = rnd.nextUInt(cr + cc);
  if (dest_pos < cr) {
    destination[0].r = dest_pos + 1;
    destination[0].c = 1;
  } else {
    destination[0].r = 1;
    destination[0].c = dest_pos - cr + 1;
  }
  Pos dpos = destination[0];
  int di = 1;
  int da = 0;
  while (dpos.r == 1 && dpos.c > 1 && di < 4) {
    dpos.c--;
    da += cr - 1;
    if (da * 4 >= (N - 1) * (N - 1) * 2) {
      destination[di++] = dpos;
      da = 0;
    }
  }
  while (dpos.r < N && dpos.c == 1 && di < 4) {
    dpos.r++;
    da += cc - 1;
    if (da * 4 >= (N - 1) * (N - 1) * 2) {
      destination[di++] = dpos;
      da = 0;
    }
  }
  while (dpos.r == N && dpos.c < N && di < 4) {
    dpos.c++;
    da += N - cr;
    if (da * 4 >= (N - 1) * (N - 1) * 2) {
      destination[di++] = dpos;
      da = 0;
    }
  }
  while (dpos.r > 1 && dpos.c == N && di < 4) {
    dpos.r--;
    da += N - cc;
    if (da * 4 >= (N - 1) * (N - 1) * 2) {
      destination[di++] = dpos;
      da = 0;
    }
  }
  while (dpos.r == 1 && dpos.c > 1 && di < 4) {
    dpos.c--;
    da += cr - 1;
    if (da * 4 >= (N - 1) * (N - 1) * 2) {
      destination[di++] = dpos;
      da = 0;
    }
  }
  while (dpos.r < N && dpos.c == 1 && di < 4) {
    dpos.r++;
    da += cc - 1;
    if (da * 4 >= (N - 1) * (N - 1) * 2) {
      destination[di++] = dpos;
      da = 0;
    }
  }
  assert(di == 4);
}

bool extend_curtain(vector<Pos>& curtain, int nr, int nc, Result& res) {
  if (grid[nr][nc]) return false;
  for (int j = 0; j < 8; ++j) {
    int ar = nr + DR8[j];
    int ac = nc + DC8[j];
    if (curtain.back().r == ar && curtain.back().c == ac) continue;
    if (curtain.size() >= 2 && curtain[curtain.size() - 2].r == ar && curtain[curtain.size() - 2].c == ac) continue;
    if (ar == 0 || ar == N + 1 || ac == 0 || ac == N + 1) continue;
    if (res.is_wall(ar, ac)) {
      return false;
    }
  }
  if (curtain.size() >= 2 && max(abs(nr - curtain[curtain.size() - 2].r), abs(nc - curtain[curtain.size() - 2].c)) == 1) {
    assert(res.is_wall(curtain.back().r, curtain.back().c));
    res.flip(curtain.back().r, curtain.back().c);
    curtain.pop_back();
  }
  curtain.push_back({nr, nc});
  assert(!res.is_wall(nr, nc));
  res.flip(nr, nc);
  return true;
}

bool create_curtain(vector<Pos>& curtain, Pos target, Result& res) {
  // debug("create_curtain start:(%d %d) target:(%d %d)\n", curtain[0].r, curtain[0].c, target.r, target.c);
  while (true) {
    int cr = curtain.back().r;
    int cc = curtain.back().c;
    if (cr == 1 || cr == N || cc == 1 || cc == N) break;
    int dr = abs(target.r - cr);
    int dc = abs(target.c - cc);
    int mr = signum(target.r - cr);
    int mc = signum(target.c - cc);
    if (rnd.nextUInt(dr + dc) < dr) {
      if (extend_curtain(curtain, cr + mr, cc, res)) {
        continue;
      }
      if (dc > 0 && extend_curtain(curtain, cr, cc + mc, res)) {
        continue;
      }
    } else{
      if (extend_curtain(curtain, cr, cc + mc, res)) {
        continue;
      }
      if (dr > 0 && extend_curtain(curtain, cr + mr, cc, res)) {
        continue;
      }
    }
    if (dr > 0 && dc > 0 && extend_curtain(curtain, cr + mr, cc + mc, res)) {
      continue;
    }
    return false;
  }
  return true;
}

void examine_area(Area& area, const Result& res) {
  general_buf[0] = (area.entrance.r << 8) | area.entrance.c;
  int qs = 1;
  bfs_counter.clear();
  bfs_counter.set(area.entrance.r, area.entrance.c);
  for (int qi = 0; qi < qs; ++qi) {
    int cr = general_buf[qi] >> 8;
    int cc = general_buf[qi] & 0xFF;
    for (int i = 0; i < 4; ++i) {
      int nr = cr + DR[i];
      int nc = cc + DC[i];
      if (res.is_wall(nr, nc)) continue;
      if (bfs_counter.get(nr, nc)) continue;
      bfs_counter.set(nr, nc);
      general_buf[qs++] = (nr << 8) | nc;
      if (grid[nr][nc] & START_POS) {
        area.start[grid[nr][nc] & 0xF] = true;
      } else if (grid[nr][nc] & TARGET_POS) {
        area.target[grid[nr][nc] & 0xF]++;
      }
    }
  }
  for (int i = max(1, area.entrance.r - 2); i <= min(N, area.entrance.r + 2); ++i) {
    for (int j = max(1, area.entrance.c - 2); j <= min(N, area.entrance.c + 2); ++j) {
      if (!bfs_counter.get(i, j)) continue;
      if (grid[i][j] & START_POS) {
        area.start[grid[i][j] & 0xF] = false;
      } else if (grid[i][j] & TARGET_POS) {
        area.target[grid[i][j] & 0xF]--;
      }
    }
  }
  area.size = qs;
  // debug("area.size:%d\n", area.size);
}

template<int S>
bool create_partition(Pos& core, AreaPartition& ap, Result& res, const array<Pos, S>& destination) {
  for (int i = 1; i <= N; ++i) {
    res.wall[i] = 1ull | (1ull << (N + 1));
  }
  ap.core = core;
  for (int i = 0; i < 4; ++i) {
    ap.areas[i].start.assign(R, false);
    ap.areas[i].target.assign(R, 0);
    ap.curtains[i].clear();
  }
  const int cr = core.r;
  const int cc = core.c;
  // protect
  if (grid[cr][cc] == 0) grid[cr][cc] = INF;
  for (int i = 0; i < 4; ++i) {
    if (grid[cr + DR[i]][cc + DC[i]] == 0) grid[cr + DR[i]][cc + DC[i]] = INF;
    if (grid[cr + DR[i] * 2][cc + DC[i] * 2] == 0) grid[cr + DR[i] * 2][cc + DC[i] * 2] = INF;
  }
  res.flip(cr - 1, cc - 1);
  res.flip(cr - 1, cc + 1);
  res.flip(cr + 1, cc - 1);
  res.flip(cr + 1, cc + 1);
  ap.curtains[0].push_back({cr - 1, cc - 1});
  ap.curtains[1].push_back({cr + 1, cc - 1});
  ap.curtains[2].push_back({cr + 1, cc + 1});
  ap.curtains[3].push_back({cr - 1, cc + 1});
  ap.areas[0].entrance = {cr, cc - 1};
  ap.areas[1].entrance = {cr + 1, cc};
  ap.areas[2].entrance = {cr, cc + 1};
  ap.areas[3].entrance = {cr - 1, cc};
  bool ok = true;
  for (int i = 0; i < 4; ++i) {
    if (!create_curtain(ap.curtains[i], destination[i], res)) {
      // debug("fail create_curtain %d\n", i);
      ok = false;
      break;
    }
  }
  if (grid[cr][cc] == INF) grid[cr][cc] = 0;
  for (int i = 0; i < 4; ++i) {
    if (grid[cr + DR[i]][cc + DC[i]] == INF) grid[cr + DR[i]][cc + DC[i]] = 0;
    if (grid[cr + DR[i] * 2][cc + DC[i] * 2] == INF) grid[cr + DR[i] * 2][cc + DC[i] * 2] = 0;
  }
  if (!ok) return false;
  assert(!res.is_wall(cr, cc));
  res.flip(cr, cc); // tmporarily set for counting area size
  for (int i = 0; i < 4; ++i) {
    examine_area(ap.areas[i], res);
  }
  res.flip(cr, cc);
  return true;
}

AreaPartition part_areas(const vector<Pos>& core_cands) {
  static Result tmp_res = empty_result();
  AreaPartition best;
  int best_v = 0;
  AreaPartition cur;
  cur.areas.resize(4);
  cur.curtains.resize(4);
  array<Pos, 4> destination;
  for (int turn = 0; turn < 30; ++turn) {
    Pos core_pos = core_cands[rnd.nextUInt(core_cands.size())];
    set_destination<4>(core_pos, destination);
    if (!create_partition<4>(core_pos, cur, tmp_res, destination)) {
      continue;
    }
    int v = evaluate_partition(cur);
    if (v > best_v) {
      best_v = v;
      best = cur;
    }
    if (v > best_v * 8 / 10) {
      int prev_v = v;
      for (int i = 0; i < 30; ++i) {
        array<Pos, 4> new_dests = destination;
        for (int j = 0; j < 4; ++j) {
          if (new_dests[j].r == 1 || new_dests[j].r == N) {
            new_dests[j].c += rnd.nextUInt(9) - 4;
            if (new_dests[j].c < 1) {
              int over = 1 - new_dests[j].c;
              new_dests[j].r = new_dests[j].r == 1 ? 1 + over : N - over;
              new_dests[j].c = 1;
            } else if (new_dests[j].c > N) {
              int over = new_dests[j].c - N;
              new_dests[j].r = new_dests[j].r == 1 ? 1 + over : N - over;
              new_dests[j].c = N;
            }
          } else {
            new_dests[j].r += rnd.nextUInt(9) - 4;
            if (new_dests[j].r < 1) {
              int over = 1 - new_dests[j].r;
              new_dests[j].c = new_dests[j].c == 1 ? 1 + over : N - over;
              new_dests[j].r = 1;
            } else if (new_dests[j].r > N) {
              int over = new_dests[j].r - N;
              new_dests[j].c = new_dests[j].c == 1 ? 1 + over : N - over;
              new_dests[j].r = N;
            }
          }
        }
        if (!create_partition<4>(core_pos, cur, tmp_res, new_dests)) {
          continue;
        }
        int v = evaluate_partition(cur);
        if (v > prev_v) {
          prev_v = v;
          destination = new_dests;
        }
        if (v > best_v) {
          best_v = v;
          best = cur;
        }
      }
    }
  }
  // debug("value:%d core:(%d %d)\n", best_v, best.core.r, best.core.c);
  return best;
}

AreaPath create_single_path(const Area& area, Result& res) {
  AreaPath path;
  path.score.resize(R);
  Pos base = area.entrance;
  array<int, 4> dirs = {};
  static vi dirs_near;
  static vi dirs_far;
  static vi dirs_close;
  static vi start_dist(R);
  start_dist.assign(R, 0);
  static vector<Pos> stack;
  stack.clear();
  stack.push_back(area.entrance);
  static vvi distance(N + 2, vi(N + 2));
  bfs_counter.clear();
  bfs_counter.set(area.entrance.r, area.entrance.c);
  while (!stack.empty()) {
    int cr = stack.back().r;
    int cc = stack.back().c;
    dirs_near.clear();
    dirs_far.clear();
    dirs_close.clear();
    for (int i = 0; i < 4; ++i) {
      int nr = cr + DR[i];
      int nc = cc + DC[i];
      if (res.is_wall(nr, nc)) {
        if (bfs_counter.get(nr, nc)) { // only internal wall of the area
          dirs_close.push_back(i);
        }
      } else {
        if (bfs_counter.get(nr, nc)) continue;
        if (grid[nr][nc]) {
          dirs_near.push_back(i);
        } else {
          bool connect = true;
          for (int j = 0; j < 4; ++j) {
            int ar = nr + DR[j];
            int ac = nc + DC[j];
            if (grid[ar][ac]) {
              connect = false;  // skip short-connect
              break;
            }
          }
          if (!connect) {
            dirs_close.push_back(i);
          } else if (DR[i] * (base.r - cr) + DC[i] * (base.c - cc) >= 0) {
            dirs_near.push_back(i);
          } else {
            dirs_far.push_back(i);
          }
        }
      }
    }
    if (dirs_near.size() >= 2) {
      swap(dirs_near[0], dirs_near[rnd.nextUInt(dirs_near.size())]);
    }
    if (dirs_far.size() >= 2) {
      swap(dirs_far[0], dirs_far[rnd.nextUInt(dirs_far.size())]);
    }
    if (dirs_close.size() >= 2) {
      swap(dirs_close[0], dirs_close[rnd.nextUInt(dirs_close.size())]);
    }
    int dirs_size = 0;
    for (int d : dirs_near) {
      dirs[dirs_size++] = d;
    }
    for (int d : dirs_far) {
      dirs[dirs_size++] = d;
    }
    for (int d : dirs_close) {
      dirs[dirs_size++] = d;
    }
    for (int i = 0; i < dirs_size; ++i) {
      int dir = dirs[i];
      int nr = cr + DR[dir];
      int nc = cc + DC[dir];
      if (res.is_wall(nr, nc)) {
        bool ok = true;
        bool has_unknown = false;
        for (int j = 0; j < 4; ++j) {
          if ((j ^ 2) == dir) continue;
          int ar = nr + DR[j];
          int ac = nc + DC[j];
          if (res.is_wall(ar, ac)) continue;
          if (!bfs_counter.get(ar, ac)) {
            has_unknown = true;
          } else {
            if (distance[ar][ac] < distance[cr][cc]) {
              ok = false;
              break;
            }
          }
        }
        if (ok && has_unknown) {
          res.flip(nr, nc);
          distance[nr][nc] = stack.size();
          stack.push_back({nr, nc});
          break;
        }
      } else {
        bool ok = true;
        for (int j = 0; j < 4; ++j) {
          if ((j ^ 2) == dir) continue;
          int ar = nr + DR[j];
          int ac = nc + DC[j];
          if (!res.is_wall(ar, ac) && bfs_counter.get(ar, ac)) {
            ok = false;
            break;
          }
        }
        if (!ok) continue;
        bfs_counter.set(nr, nc);
        distance[nr][nc] = stack.size();
        stack.push_back({nr, nc});
        if (grid[nr][nc] & START_POS) {
          start_dist[grid[nr][nc] & 0xF] = stack.size();
        } else if (grid[nr][nc] & TARGET_POS) {
          path.score[grid[nr][nc] & 0xF] = max(path.score[grid[nr][nc] & 0xF], (int)stack.size());
        }
        for (int j = 0; j < 4; ++j) {
          int ar = cr + DR[j];
          int ac = cc + DC[j];
          if (grid[ar][ac] == 0 && !bfs_counter.get(ar, ac) && !res.is_wall(ar, ac)) {
            res.flip(ar, ac);
            bfs_counter.set(ar, ac);
            path.wall.push_back({ar, ac});
          }
        }
        break;
      }
    }
    if (stack.back().r == cr && stack.back().c == cc) {
      stack.pop_back();
    }
    // debug("%d %d\n", cr, cc);
    // print_grid(res);
  }
  for (int i = 0; i < R; ++i) {
    if (area.start[i]) {
      if (path.score[i] < start_dist[i]) {
        path.score[i] = start_dist[i];
      } else {
        path.score[i] += path.score[i] - start_dist[i];
      }
    }
  }
  for (int i = 0; i < path.wall.size(); ++i) {
    if (!res.is_wall(path.wall[i].r, path.wall[i].c)) {
      swap(path.wall[i], path.wall.back());
      path.wall.pop_back();
      i--;
    } else {
      res.flip(path.wall[i].r, path.wall[i].c);
    }
  }
  return path;
}

vector<AreaPath> create_path(const Area& area, Result& res) {
  vector<AreaPath> paths;
  for (int i = 0; i < 70; ++i) {
    AreaPath path = create_single_path(area, res);
    bool useless = false;
    for (int j = 0; j < paths.size(); ++j) {
      bool all_win = true;
      bool all_lose = true;
      for (int k = 0; k < R; ++k) {
        if (path.score[k] < paths[j].score[k]) all_win = false;
        if (path.score[k] > paths[j].score[k]) all_lose = false;
      }
      if (all_lose) {
        useless = true;
        break;
      }
      if (all_win) {
        swap(paths[j], paths.back());
        paths.pop_back();
        j--;
      }
    }
    if (!useless && paths.size() < 15) {
      paths.push_back(path);
    }
  }
  return paths;
}

void combine_paths(const AreaPartition& ap, const vector<vector<AreaPath>>& paths, int& best_score, vi& best_selection) {
  array<int, 4> score = {};
  array<vi, 4> max_len = {};
  for (int i = 0; i < 4; ++i) {
    max_len[i].resize(R);
  }
  for (int i0 = 0; i0 < paths[0].size(); ++i0) {
    score[0] = 0;
    for (int i = 0; i < R; ++i) {
      if (ap.areas[0].start[i]) {
        score[0] += paths[0][i0].score[i];
      } else {
        score[0] += 2 * paths[0][i0].score[i];
        max_len[0][i] = paths[0][i0].score[i];
      }
    }
    for (int i1 = 0; i1 < paths[1].size(); ++i1) {
      score[1] = score[0];
      for (int i = 0; i < R; ++i) {
        if (ap.areas[1].start[i]) {
          score[1] += paths[1][i1].score[i];
          max_len[1][i] = max_len[0][i];
        } else {
          score[1] += 2 * paths[1][i1].score[i];
          max_len[1][i] = max(max_len[0][i], paths[1][i1].score[i]);
        }
      }
      for (int i2 = 0; i2 < paths[2].size(); ++i2) {
        score[2] = score[1];
        for (int i = 0; i < R; ++i) {
          if (ap.areas[2].start[i]) {
            score[2] += paths[2][i2].score[i];
            max_len[2][i] = max_len[1][i];
          } else {
            score[2] += 2 * paths[2][i2].score[i];
            max_len[2][i] = max(max_len[1][i], paths[2][i2].score[i]);
          }
        }
        for (int i3 = 0; i3 < paths[3].size(); ++i3) {
          score[3] = score[2];
          for (int i = 0; i < R; ++i) {
            if (ap.areas[3].start[i]) {
              score[3] += paths[3][i3].score[i];
              max_len[3][i] = max_len[2][i];
            } else {
              score[3] += 2 * paths[3][i3].score[i];
              max_len[3][i] = max(max_len[2][i], paths[3][i3].score[i]);
            }
            score[3] -= max_len[3][i];
            if (score[3] > best_score) {
              best_score = score[3];
              best_selection = {i0, i1, i2, i3};
            }
          }
        }
      }
    }
  }
}

Result solve_part() {
  START_TIMER(0);
  vector<Pos> core_cands;
  for (int i = min(N / 3, 7); i <= max(N - 7, N - N / 3) + 1; ++i) {
    for (int j = min(N / 3, 7); j <= max(N - 7, N - N / 3) + 1; ++j) {
      if (grid[i - 1][j - 1] == 0 && grid[i - 1][j + 1] == 0 && grid[i + 1][j - 1] == 0 && grid[i + 1][j + 1] == 0) {
        core_cands.push_back({i, j});
      }
    }
  }
  Result res = empty_result();
  if (core_cands.empty()) {
    res.score = calc_score(res);
    return res;
  }
  AreaPartition ap = part_areas(core_cands);
  STOP_TIMER(0);
  if (ap.areas.empty()) {
    res.score = calc_score(res);
    return res;
  }
  for (const vector<Pos>& wps : ap.curtains) {
    for (const Pos& wp : wps) {
      assert(!res.is_wall(wp.r, wp.c));
      res.flip(wp.r, wp.c);
    }
  }
  // print_grid(res);
  START_TIMER(1);
  res.flip(ap.core.r, ap.core.c); // temporarily
  vector<vector<AreaPath>> paths(ap.areas.size());
  for (int i = 0; i < ap.areas.size(); ++i) {
    paths[i] = create_path(ap.areas[i], res);
  }
  STOP_TIMER(1);
  START_TIMER(2);
  int score = 0;
  vi best_selection;
  combine_paths(ap, paths, score, best_selection);
  STOP_TIMER(2);
  START_TIMER(3);
  for (int i = 0; i < ap.areas.size(); ++i) {
    const AreaPath& path = paths[i][best_selection[i]];
    for (const Pos& wp : path.wall) {
      assert(!res.is_wall(wp.r, wp.c));
      res.flip(wp.r, wp.c);
    }
  }
  res.flip(ap.core.r, ap.core.c);
  res.score = calc_score(res);
  // debug("est.score:%d score:%d\n", score, res.score);
  STOP_TIMER(3);
  return res;
}

class HardestMaze {
public:
  vector<vector<char>> findSolution() {
    Result best_res = empty_result();
    best_res.score = 0;
    // if (N < 24) {
    //   const int MAX_LEN = 7;
    //   const int REP = 1000 / (N * N);
    //   for (int i = 0; i < 4; ++i) {
    //     for (int j = 0; j < MAX_LEN; ++j) {
    //       for (int k = 0; k < REP; ++k) {
    //         Result res = create_initial_solution(j + 1);
    //         improve(res, TL * (i * MAX_LEN * REP + j * REP + k + 1) / (4 * REP * MAX_LEN));
    //         if (best_res.score < res.score) {
    //           res.rot = i;
    //           best_res = res;
    //         }
    //       }
    //     }
    //     rotate();
    //   }
    // } else {
      // for (int i = 0; i < 4; ++i) {
      //   for (int j = 1; j <= 8; ++j) {
      //     Result res = create_initial_solution(j);
      //     if (best_res.score < res.score) {
      //       res.rot = i;
      //       best_res = res;
      //     }
      //   }
      //   rotate();
      // }
      // for (int i = 0; i < best_res.rot; ++i) {
      //   rotate();
      // }
      // improve(best_res, TL);
    // }
    // debug("best_score:%d rot:%d\n", best_res.score, best_res.rot);
    const ll FINAL_TIME = 100;
    const int multistart = min(200, 400000 / (N * N * N));
    const ll before_time = get_elapsed_msec();
    for (int i = 0; i < multistart; ++i) {
      const ll timelimit = (TL - FINAL_TIME - before_time) * (i + 1) / multistart + before_time;
      Result res = solve_part();
      for (int turn = 0; ; ++turn) {
        const ll prev_time = (TL - FINAL_TIME - before_time) * i / multistart + before_time;
        if (turn == 10 || get_elapsed_msec() > prev_time + 100) {
          debug("part turn:%d score:%d\n", turn, res.score);
          break;
        }
        Result cur_res = solve_part();
        if (res.score < cur_res.score) {
          res = cur_res;
        }
      }
      improve(res, timelimit, false);
      debug("score:%d\n", res.score);
      if (res.score > best_res.score) {
        best_res = res;
      }
    }
    improve(best_res, TL, true);

    vector<vector<char>> ans(N, vector<char>(N));
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        if ((best_res.wall[i + 1] & (1ull << (j + 1))) == 0) {
          ans[i][j] = '.';
        } else {
          ans[i][j] = '#';
        }
      }
    }
    for (int i = 0; i < best_res.rot; ++i) {
      rotate_grid(ans);
    }
    return ans;
  }
};

int main() {
  scanf("%d %d %d", &N, &R, &T);
  debug("N:%d R:%d T:%d\n", N, R, T);
  start_time = get_time();
  for (int i = 0; i < R; i++) {
    scanf("%d %d", &starts[i].r, &starts[i].c);
    starts[i].r++;
    starts[i].c++;
    grid[starts[i].r][starts[i].c] = START_POS | i;
    for (int j = 0; j < T; j++) {
      scanf("%d %d", &targets[i][j].r, &targets[i][j].c);
      targets[i][j].r++;
      targets[i][j].c++;
      grid[targets[i][j].r][targets[i][j].c] = TARGET_POS | (j << 4) | i;
    }
  }
  sur_areas[0] = 1;
  for (int i = 1; i < (1 << 9) - 1; ++i) {
    array<bool, 10> bits;
    bits[0] = i & (1 << 0);
    bits[1] = i & (1 << 1);
    bits[2] = i & (1 << 2);
    bits[3] = i & (1 << 5);
    bits[4] = i & (1 << 8);
    bits[5] = i & (1 << 7);
    bits[6] = i & (1 << 6);
    bits[7] = i & (1 << 3);
    bits[8] = bits[0];
    bits[9] = bits[1];
    for (int j = 1; j <= 7; j += 2) {
      if (!bits[j]) {
        sur_areas[i]++;
        if (!bits[j + 1] && !bits[j + 2]) sur_areas[i]--;
      }
    }
  }
  MIN_DIFF_MIN = R == 1 ? 5 : R <= 3 ? 13 : R <= 4 ? 15 : 19;

  HardestMaze sol;
  vector<vector<char>> ret = sol.findSolution();
  printf("%d\n", N * N);
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      printf("%c\n", ret[i][j]);
    }
  }
  fflush(stdout);
  PRINT_TIMER();
  PRINT_COUNTER();
}