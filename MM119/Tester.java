import com.topcoder.marathon.MarathonController;
import com.topcoder.marathon.MarathonVis;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.Arrays;

public class Tester extends MarathonVis {
	private static final int minN = 10, maxN = 40;
	private static final int minR = 1, maxR = 6;
	private static final int minT = 2, maxT = 6;
	private int N;      //grid size
	private int R;      //number of robots
	private int T;      //number of targets
	private int[][] Starts;       //start locations
	private int[][][] Targets;    //target locations
	private static final int INF = 1000000;
	private static final int[] dr = {0, 1, 0, -1};     //L, U, R, D
	private static final int[] dc = {-1, 0, 1, 0};
	private static final char Empty = '.';
	private static final char Wall = '#';
	private Color[] colors;
	private Color[] colorsLight;
	private boolean[][] Grid;
	private int[][] Dist;
	private int[][] bestInd;
	private char[] Solution;

	protected void generate() {
		N = randomInt(minN, maxN);
		R = randomInt(minR, maxR);
		T = randomInt(minT, maxT);

		if (seed == 1) {
			N = minN;
			R = 2;
			T = minT;
		} else if (seed == 2) {
			N = maxN;
			R = maxR;
			T = maxT;
		}
		if (1000 <= seed && seed < 2000) {
			int si = (int) (seed - 1000);
			N = si * (maxN - minN + 1) / 1000 + minN;
			R = si % (maxR - minR + 1) + minR;
			T = si % (maxT - minT + 1) + minT; // maxR - minR != maxT - minT
		}
		if (parameters.isDefined("N")) N = randomInt(parameters.getIntRange("N"), minN, maxN);
		if (parameters.isDefined("R")) R = randomInt(parameters.getIntRange("R"), minR, maxR);
		if (parameters.isDefined("T")) T = randomInt(parameters.getIntRange("T"), minT, maxT);

		Starts = new int[R][2];
		Targets = new int[R][T][2];

		int[] ind = new int[N * N];
		for (int i = 0; i < ind.length; i++) ind[i] = i;
		shuffle(ind);
		int cur = 0;
		for (int i = 0; i < R; i++, cur++) {
			Starts[i][0] = ind[cur] / N;
			Starts[i][1] = ind[cur] % N;
		}
		for (int i = 0; i < R; i++) {
			for (int k = 0; k < T; k++, cur++) {
				Targets[i][k][0] = ind[cur] / N;
				Targets[i][k][1] = ind[cur] % N;
			}
		}
		bestInd = new int[R][T];
	}

	protected boolean isMaximize() {
		return true;
	}

	protected double run() throws Exception {
		init();
		Solution = callSolution();
		if (Solution == null) {
			if (!isReadActive()) return getErrorScore();
			return fatalError();
		}
		Dist = new int[N * N][N * N];
		for (int i = 0; i < Dist.length; i++) for (int k = 0; k < Dist.length; k++) Dist[i][k] = INF;

		for (int i = 0; i < N * N; i++) Grid[i / N][i % N] = (Solution[i] == Wall);

		for (int i = 0; i < R; i++) {
			if (Grid[Starts[i][0]][Starts[i][1]])
				return fatalError("Cannot have a wall at (" + Starts[i][0] + "," + Starts[i][1] + ")");
			for (int k = 0; k < T; k++)
				if (Grid[Targets[i][k][0]][Targets[i][k][1]])
					return fatalError("Cannot have a wall at (" + Targets[i][k][0] + "," + Targets[i][k][1] + ")");
		}

		for (int r = 0, id = 0; r < N; r++) {
			for (int c = 0; c < N; c++, id++) {
				if (Grid[r][c]) continue;    //skip walls
				Dist[id][id] = 0;
			}
		}

		for (int i = 0; i < R; i++) {
			singleStartShortestPath(coords2id(Starts[i][0], Starts[i][1]));
			for (int j = 0; j < T; j++) {
				singleStartShortestPath(coords2id(Targets[i][j][0], Targets[i][j][1]));
			}
		}

		int[] PathLengths = new int[R];
		int Score = 0;
		for (int q = 0; q < R; q++) {
			int startId = coords2id(Starts[q][0], Starts[q][1]);
			for (int i = 0; i < T; i++) {
				int targetId = coords2id(Targets[q][i][0], Targets[q][i][1]);
				if (Dist[startId][targetId] >= INF)
					return fatalError("Target " + i + " for robot " + q + " is not reachable");
			}
			PathLengths[q] = Integer.MAX_VALUE;
			int[] ind = new int[T];
			for (int i = 0; i < T; i++) ind[i] = i;
			do {
				int total = 0;
				int prevId = startId;
				for (int i = 0; i < T; i++) {
					int nextId = coords2id(Targets[q][ind[i]][0], Targets[q][ind[i]][1]);
					total += Dist[prevId][nextId];
					prevId = nextId;
				}
				if (total < PathLengths[q]) {
					PathLengths[q] = total;
					bestInd[q] = ind.clone();
				}
			} while (nextPermutation(ind));
			Score += PathLengths[q];
		}

		if (hasVis()) {
			addInfo("Score", Score);
			addInfo("Time", getRunTime() + " ms");
			for (int c = 0; c < R; c++) {
				addInfo(colors[c], PathLengths[c]);
			}
			update();
		}
		return Score;
	}

	private void singleStartShortestPath(int sid) {
		ArrayList<Integer> q = new ArrayList<>();
		q.add(sid);
		for (int i = 1; !q.isEmpty(); i++) {
			ArrayList<Integer> next = new ArrayList<>();
			for (int pos : q) {
				int r = pos / N;
				int c = pos % N;
				for (int j = 0; j < 4; j++) {
					int nr = r + dr[j];
					int nc = c + dc[j];
					int npos = coords2id(nr, nc);
					if (!inGrid(nr, nc) || Grid[nr][nc] || Dist[sid][npos] != INF) {
						continue;
					}
					Dist[sid][npos] = i;
					next.add(npos);
				}
			}
			q = next;
		}
	}

	private int coords2id(int r, int c) {
		return r * N + c;
	}

	private boolean nextPermutation(int[] a) {
		int n = a.length;
		int i = n - 2;
		for (; i >= 0; i--)
			if (a[i] < a[i + 1])
				break;
		if (i < 0) return false;

		for (int j = n - 1; j >= i; j--) {
			if (a[j] > a[i]) {
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
				break;
			}
		}
		for (int j = i + 1; j < (n + i + 1) / 2; j++)    //reverse from a[i+1] to a[n-1]
		{
			int temp = a[j];
			a[j] = a[n + i - j];
			a[n + i - j] = temp;
		}
		return true;
	}

	protected void paintContent(Graphics2D g) {
		//draw grid
		g.setStroke(new BasicStroke(0.005f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		for (int r = 0; r < N; r++)
			for (int c = 0; c < N; c++) {
				g.setColor(Color.white);
				g.fillRect(c, r, 1, 1);
				g.setColor(Color.gray);
				g.drawRect(c, r, 1, 1);
			}

		//draw starts and targets
		for (int i = 0; i < R; i++) {
			g.setColor(colors[i]);
			//circle
			// Ellipse2D s = new Ellipse2D.Double(Starts[i][1] + 0.2, Starts[i][0] + 0.2, 0.6, 0.6);
			// g.fill(s);
			//+
			// Rectangle2D s = new Rectangle2D.Double(Starts[i][1] + 0.25, Starts[i][0], 0.5, 1);
			// g.fill(s);
			// Rectangle2D s2 = new Rectangle2D.Double(Starts[i][1], Starts[i][0] + 0.25, 1, 0.5);
			// g.fill(s2);
			//x
			GeneralPath gp = new GeneralPath();
			gp.moveTo(Starts[i][1] + 0.1, Starts[i][0] + 0.1);
			gp.lineTo(Starts[i][1] + 0.9, Starts[i][0] + 0.9);
			gp.moveTo(Starts[i][1] + 0.9, Starts[i][0] + 0.1);
			gp.lineTo(Starts[i][1] + 0.1, Starts[i][0] + 0.9);
			g.setStroke(new BasicStroke(0.25f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.draw(gp);

			for (int k = 0; k < T; k++) {
				g.setColor(colors[i]);
				//circle
				Ellipse2D t = new Ellipse2D.Double(Targets[i][k][1] + 0.2, Targets[i][k][0] + 0.2, 0.6, 0.6);
				g.fill(t);
				//square
				// Rectangle2D t = new Rectangle2D.Double(Targets[i][k][1] + 0.2, Targets[i][k][0] + 0.2, 0.6, 0.6);
				// g.fill(t);
				//+
				// Rectangle2D t = new Rectangle2D.Double(Targets[i][k][1] + 0.25, Targets[i][k][0], 0.5, 1);
				// g.fill(t);
				// Rectangle2D t2 = new Rectangle2D.Double(Targets[i][k][1], Targets[i][k][0] + 0.25, 1, 0.5);
				// g.fill(t2);
			}
		}

		if (Solution != null) {
			//draw walls
			for (int r = 0; r < N; r++)
				for (int c = 0; c < N; c++)
					if (Grid[r][c]) {
						g.setColor(Color.gray);
						g.fillRect(c, r, 1, 1);
					}

			//draw paths
			for (int i = 0; i < R; i++) {
				GeneralPath gp = new GeneralPath();

				int r = Starts[i][0];
				int c = Starts[i][1];
				gp.moveTo(c + 0.5, r + 0.5);

				for (int k = 0; k < T; k++) {
					int targetR = Targets[i][bestInd[i][k]][0];
					int targetC = Targets[i][bestInd[i][k]][1];
					int targetId = coords2id(targetR, targetC);

					while (true) {
						if (r == targetR && c == targetC) break;    //reached our target, so exit

						boolean found = false;
						for (int m = 0; m < dr.length; m++) {
							int r2 = r + dr[m];
							int c2 = c + dc[m];

							if (inGrid(r2, c2) && Dist[targetId][coords2id(r2, c2)] < Dist[targetId][coords2id(r, c)]) {
								r = r2;
								c = c2;
								found = true;
								break;
							}
						}
						if (!found) break;          //catch problems when Dist hasn't been computed yet
						gp.lineTo(c + 0.5, r + 0.5);
					}
				}

				//see-through path
				g.setColor(colorsLight[i]);
				g.setStroke(new BasicStroke(0.4f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				g.draw(gp);
				//central line
				g.setColor(Color.black);
				g.setStroke(new BasicStroke(0.03f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				g.draw(gp);
			}
		}
	}

	private boolean inGrid(int x, int y) {
		return x >= 0 && x < N && y >= 0 && y < N;
	}

	private void init() {
		Grid = new boolean[N][N];

		if (hasVis()) {
			colors = new Color[]{Color.red, Color.cyan, Color.green, Color.blue, Color.orange, Color.magenta};

			colorsLight = new Color[colors.length];
			for (int i = 0; i < colors.length; i++)
				colorsLight[i] = new Color(colors[i].getRed(), colors[i].getGreen(), colors[i].getBlue(), 80);

			setInfoMaxDimension(15, 15);
			setContentRect(0, 0, N, N);
			setDefaultSize(30);

			addInfo("Seed", seed);
			addInfoBreak();
			addInfo("Size N", N);
			addInfo("Robots R", R);
			addInfo("Targets T", T);
			addInfoBreak();
			addInfo("Score", "-");
			addInfoBreak();
			addInfo("Time", "-");
			addInfoBreak();
			addInfo("Path lengths");
			for (int c = 0; c < R; c++) {
				addInfo(colors[c], 0);
			}
			update();
		}
	}

	private char[] callSolution() throws Exception {
		writeLine(N);
		writeLine(R);
		writeLine(T);
		for (int i = 0; i < R; i++) {
			writeLine(Starts[i][0] + " " + Starts[i][1]);
			for (int k = 0; k < T; k++) writeLine(Targets[i][k][0] + " " + Targets[i][k][1]);
		}
		flush();
		if (!isReadActive()) return null;

		startTime();
		int n = readLineToInt(-1);
		if (n != N * N) {
			setErrorMessage("Invalid number of rows: " + getLastLineRead());
			return null;
		}
		char[] ret = new char[n];
		for (int i = 0; i < n; i++) {
			String s = readLine();
			if (s.length() != 1 || !(s.charAt(0) == Empty || s.charAt(0) == Wall)) {
				setErrorMessage("Invalid return in line " + (i + 1) + " : " + getLastLineRead());
				return null;
			}
			ret[i] = s.charAt(0);
		}
		stopTime();
		return ret;
	}

	//shuffle the array randomly
	private void shuffle(int[] a) {
		for (int i = 0; i < a.length; i++) {
			int k = randomInt(i, a.length - 1);
			int temp = a[i];
			a[i] = a[k];
			a[k] = temp;
		}
	}

	public static void main(String[] args) {
		if (Arrays.stream(args).noneMatch("-vis"::equals)) {
			args = Arrays.copyOf(args, args.length + 1);
			args[args.length - 1] = "-novis";
		}
		if (Arrays.stream(args).noneMatch("-exec"::equals)) {
			args = Arrays.copyOf(args, args.length + 2);
			args[args.length - 2] = "-exec";
			args[args.length - 1] = "./main";
		}
		new MarathonController().run(args);
	}
}
