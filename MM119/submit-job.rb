range = 200
array_size = 5
contest_id = 119
solver_version = ARGV[0] || "00"

args = [
	'batch', 'submit-job',
	'--job-name', 'marathon_tester',
	'--job-queue', 'marathon_tester',
	'--job-definition', 'marathon_tester',
]

if array_size > 1
	args << '--array-properties' << "size=#{array_size}"
end
args << '--container-overrides'

solver_path = "#{contest_id}/#{solver_version}"
result_path = "#{solver_path}/00"
envs = "environment=[{name=RANGE,value=#{range}},{name=SUBMISSION_ID,value=#{solver_path}},{name=RESULT_PATH,value=#{result_path}}]"
system('aws', *args, envs)

# 28.upto(29) do |i|
# 	result_path = sprintf("#{solver_path}/%02d", i)
# 	envs = "environment=[{name=RANGE,value=#{range}},{name=SUBMISSION_ID,value=#{solver_path}},{name=RESULT_PATH,value=#{result_path}}]"
# 	system('aws', *args, envs)
# end
