import com.topcoder.marathon.MarathonController;
import com.topcoder.marathon.MarathonTester;

import java.util.Arrays;

public class Tester extends MarathonTester {
	private static final int minN = 10, maxN = 1000;
	private static final int minK = 2;
	private static final double minX = 0, maxX = 3;
	private int N;          //sequence length
	private int K;          //number of different values
	private double X;       //penalty term
	private int[] Seq;
	private int[] SortedSeq;

	protected void generate() {
		N = randomInt(minN, maxN);
		K = randomInt(minK, N);
		X = randomDouble(minX, maxX);
		if (seed == 1) {
			N = minN;
			K = minN / 2;
			X = 1;
		} else if (seed == 2) {
			N = maxN;
		}
		if (1000 <= seed && seed < 2000) {
			X = minX + (seed - 1000 + 0.5) * (maxX - minX) / 1000;
		}
		if (parameters.isDefined("N")) N = randomInt(parameters.getIntRange("N"), minN, maxN);
		if (parameters.isDefined("K")) K = randomInt(parameters.getIntRange("K"), minK, N);
		if (parameters.isDefined("X")) X = randomDouble(parameters.getDoubleRange("X"), minX, maxX);

		Seq = new int[N];
		for (int i = 0; i < N; i++) Seq[i] = randomInt(0, K - 1);
		SortedSeq = Seq.clone();
		Arrays.sort(SortedSeq);

		if (debug) {
			System.out.println("N = " + N);
			System.out.println("K = " + K);
			System.out.println("X = " + X);
			System.out.print("Sequence:");
			for (int i : Seq) System.out.print(" " + i);
			System.out.println();
			System.out.println();
		}
	}

	protected boolean isMaximize() {
		return false;
	}

	protected double run() throws Exception {
		String[] solution = callSolution();
		if (solution == null) {
			if (!isReadActive()) return getErrorScore();
			return fatalError();
		}
		int n = solution.length;
		if (n > N * N / 2) return fatalError("You cannot use more than " + (N * N / 2) + " reversals");

		long score = 0;
		for (int i = 0; i < n; i++) {
			String[] temp = solution[i].split(" ");
			if (temp.length != 2) return fatalError("Invalid format of " + i + "th move: " + solution[i]);

			int start, end;
			try {
				start = Integer.parseInt(temp[0]);
				end = Integer.parseInt(temp[1]);
				if (start < 0 || start >= N) return fatalError("Index " + start + " is out of bounds");
				if (end < 0 || end >= N) return fatalError("Index " + end + " is out of bounds");
				if (start >= end) return fatalError("First index " + start + " must be smaller than second index " + end);
			} catch (Exception e) {
				return fatalError("Invalid format of " + i + "th move: " + solution[i]);
			}
			long moveScore = (long) (Math.floor(Math.pow(end - start + 1, X)));
			score += moveScore;
//			if (debug) {
//				System.out.println("Move " + (i + 1) + ", move score " + moveScore + ", total score " + score);
//				for (int k = 0; k < N; k++) {
//					if (start == k) System.out.print("(");
//					System.out.print(Seq[k]);
//					if (end == k) System.out.print(")");
//					System.out.print(" ");
//				}
//				System.out.println();
//			}
			reverse(Seq, start, end);
		}
		for (int i = 0; i < N; i++)
			if (Seq[i] != SortedSeq[i])
				return fatalError("Your final sequence is not sorted!");
		return score;
	}

	private void reverse(int[] seq, int pos1, int pos2) {
		for (int i = pos1, k = pos2; i < k; i++, k--) {
			int temp = seq[i];
			seq[i] = seq[k];
			seq[k] = temp;
		}
	}

	private String[] callSolution() throws Exception {
		writeLine(N);
		writeLine(K);
		writeLine("" + X);
		for (int i : Seq) writeLine(i);
		flush();
		if (!isReadActive()) return null;

		startTime();
		int n = readLineToInt(-1);
		if (n <= 0) {
			setErrorMessage("Invalid number of reversals: " + getLastLineRead());
			return null;
		}
		String[] ret = new String[n];
		for (int i = 0; i < ret.length; i++)
			ret[i] = readLine();

		stopTime();
		return ret;
	}

	public static void main(String[] args) {
		if (Arrays.stream(args).noneMatch("-exec"::equals)) {
			args = Arrays.copyOf(args, args.length + 2);
			args[args.length - 2] = "-exec";
			args[args.length - 1] = "./main";
		}
		new MarathonController().run(args);
	}
}