#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 2.2e9;
constexpr ll TL = 1000;
#else
#ifdef TESTER
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 5000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 9500;
#endif
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

void debug_vec(const vi& vec) {
  for (int i = 0; i < vec.size(); ++i) {
    debug("%d ", vec[i]);
  }
  debugln();
}

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

int N, K;
double X;
array<int, 1001> orig_seq;
array<int, 1001> target_seq;
array<int, 1001> cost;
array<vi, 1001> pena_pos;
array<int, 1001> best_path;
array<array<int, 1001>, 1001> best_cost_k2_s;
array<array<int, 1001>, 1001> best_path_k2_s;
array<array<int, 1001>, 1001> best_cost_k2_m;
array<array<int, 1001>, 1001> best_path_k2_m;
vi count_n;
int max_rev;
int max_greedy_len;
constexpr double INITIAL_COOLER = 0.003;
constexpr double FINAL_COOLER = 0.90;
double sa_cooler;

inline bool accept(int diff) {
  if (diff <= 0) return true;
  double v = diff * sa_cooler;
  if (v > 7) return false;
  return rnd.nextDouble() < exp(-v);
}

struct Result {
  uint32_t score;
  vector<pair<int, int>> pos;
};

template<class T>
void reverse(T& seq, int pos1, int pos2) {
  while (pos1 < pos2) {
    swap(seq[pos1], seq[pos2]);
    pos1++;
    pos2--;
  }
}   

void set_pena_pos(double f) {
  for (int i = 0, pp = 0; i < K; ++i) {
    assert(target_seq[pp] == i);
    pena_pos[i].assign(N, 0);
    int left = pp;
    while (pp < N && target_seq[pp] == i) {
      pp++;
    }
    int right = pp - 1;
    for (int j = left - 1; j >= 0; --j) {
      pena_pos[i][j] = (int)pow(left - j, f);
    }
    for (int j = right + 1; j < N; ++j) {
      pena_pos[i][j] = (int)pow(j - right, f);
    }
  }
}

Result solve_randomized_greedy(vi seq, uint32_t best_score, int max_len) {
  Result res = {};
  int cur_pena = 0;
  for (int i = 0; i < N; ++i) {
    cur_pena += pena_pos[seq[i]][i];
  }
  vi cand_pos(N - 1);
  iota(cand_pos.begin(), cand_pos.end(), 0);
  const int REP = min(300, N - 8);
  while (cur_pena > 0) {
    if (N > 200 && get_elapsed_msec() > TL) {
      res.score = 1 << 30;
      return res;
    }
    int left = -1;
    int right = -1;
    double best_eff = 1.0;
    for (int i = 0; i < REP; ++i) {
      int cpp = rnd.nextUInt(N - 1 - i) + i;
      swap(cand_pos[i], cand_pos[cpp]);
      int pos = cand_pos[i];
      // even
      int pena_diff = 0;
      for (int lo = pos, hi = pos + 1; 0 <= lo && hi < N && hi - lo < max_len; --lo, ++hi) {
        pena_diff += pena_pos[seq[hi]][lo] + pena_pos[seq[lo]][hi] - pena_pos[seq[lo]][lo] - pena_pos[seq[hi]][hi];
        if (pena_diff > 10000) break;
        double eff = 1.0 * pena_diff / cost[hi - lo + 1];
        if (eff < best_eff) {
          best_eff = eff;
          left = lo;
          right = hi;
        }
      }
      // odd
      pena_diff = 0;
      for (int lo = pos - 1, hi = pos + 1; 0 <= lo && hi < N && hi - lo < max_len; --lo, ++hi) {
        pena_diff += pena_pos[seq[hi]][lo] + pena_pos[seq[lo]][hi] - pena_pos[seq[lo]][lo] - pena_pos[seq[hi]][hi];
        if (pena_diff > 10000) break;
        double eff = 1.0 * pena_diff / cost[hi - lo + 1];
        if (eff < best_eff) {
          best_eff = eff;
          left = lo;
          right = hi;
        }
      }
    }
    if (left == -1) continue;
    const int len = right - left + 1;
    cur_pena += (int)floor(best_eff * cost[len] + 0.5);
    res.score += cost[len];
    if (res.score >= best_score) {
      return res;
    }
    res.pos.emplace_back(left, right);
    reverse(seq, left, right);
    // int valid_pena = 0;
    // for (int i = 0; i < N; ++i) {
    //   valid_pena += pena_pos[seq[i]][i];
    // }
    // if (cur_pena != valid_pena) {
    //   debug("%d %d\n", cur_pena, valid_pena);
    //   exit(1);
    // }
  }
  debug("score:%d count:%lu\n", res.score, res.pos.size());
  return res;
}

Result solve_adj_greedy(vi seq, uint32_t best_score) {
  Result res = {};
  vector<int> unmatched_n(K + 2);
  for (int i = 0; i < K; ++i) {
    unmatched_n[i + 1] = count_n[i] - 1;
  }
  vector<bool> matched_p(N + 1, false);
  vector<bool> matched_a(K + 2, false);
  seq.insert(seq.begin(), 0);
  seq.push_back(K + 1);
  for (int i = 1; i <= N; ++i) {
    seq[i]++;
  }
  int match = 0;
  for (int i = 1; i < N; ++i) {
    if (seq[i] == seq[i + 1]) {
      match++;
      unmatched_n[seq[i]]--;
      matched_p[i] = true;
    }
  }
  for (int i = 0; i <= N; ++i) {
    int ma = max(seq[i], seq[i + 1]);
    int mi = seq[i] + seq[i + 1] - ma;
    if (mi + 1 == ma && unmatched_n[ma] == 0 && !matched_a[ma]) {
      match++;
      matched_p[i] = true;
      matched_a[ma] = true;
    }
  }
  vi cand_pos(N + 1);
  iota(cand_pos.begin(), cand_pos.end(), 0);
  const int REP = min(100, N / 4);
  while (match < N + 1) {
    // debug_vec(seq);
    int left = -1;
    int right = -1;
    bool dual = false;
    int min_len = N + 1;
    for (int i = 0; i < REP || (left == -1 && i <= N); ++i) {
      int cpp = rnd.nextUInt(N + 1 - i) + i;
      swap(cand_pos[i], cand_pos[cpp]);
      int pos = cand_pos[i];
      if (matched_p[pos]) continue;
      int v0 = seq[pos];
      int v1 = seq[pos + 1];
      // left
      {
        int hi = pos;
        for (int lo = pos - 1; 0 < lo; --lo) {
          if (matched_p[lo - 1]) continue;
          int mal = max(seq[lo - 1], v0);
          int mil = seq[lo - 1] + v0 - mal;
          int mar = max(seq[lo], v1);
          int mir = seq[lo] + v1 - mar;
          int m = 0;
          if (mal == mil || (mil + 1 == mal && unmatched_n[mal] == 0 && !matched_a[mal])) {
            m++;
          }
          if (mar == mir || (mir + 1 == mar && unmatched_n[mar] == 0 && !matched_a[mar])) {
            m++;
          }
          if (m == 2 && (!dual || hi - lo + 1 < min_len)) {
            left = lo;
            right = hi;
            dual = true;
            min_len = hi - lo + 1;
          } else if (m == 1 && !dual && hi - lo + 1 < min_len) {
            left = lo;
            right = hi;
            min_len = hi - lo + 1;
          }
        }
      }
      // right
      {
        int lo = pos + 1;
        for (int hi = pos + 2; hi <= N; ++hi) {
          if (matched_p[hi]) continue;
          int mal = max(seq[hi], v0);
          int mil = seq[hi] + v0 - mal;
          int mar = max(seq[hi + 1], v1);
          int mir = seq[hi + 1] + v1 - mar;
          int m = 0;
          if (mal == mil || (mil + 1 == mal && unmatched_n[mal] == 0 && !matched_a[mal])) {
            m++;
          }
          if (mar == mir || (mir + 1 == mar && unmatched_n[mar] == 0 && !matched_a[mar])) {
            m++;
          }
          if (m == 2 && (!dual || hi - lo + 1 < min_len)) {
            left = lo;
            right = hi;
            dual = true;
            min_len = hi - lo + 1;
          } else if (m == 1 && !dual && hi - lo + 1 < min_len) {
            left = lo;
            right = hi;
            min_len = hi - lo + 1;
          }
        }
      }
    }
    if (left == -1) {
      debug("give up match:%d cost:%d\n", match, res.score);
      debug_vec(seq);
      debug_vec(unmatched_n);
      for (int i = 0; i <= N; ++i) {
        debug("%d", (int)matched_p[i]);
      }
      debugln();
      bool ok = false;
      for (int i = 0; i < N / 3; ++i) {
        int pos = rnd.nextUInt(N + 1);
        int lo = pos;
        while (lo > 0 && matched_p[lo - 1]) {
          --lo;
        }
        if (lo == 0) continue;
        int hi = pos;
        while (hi <= N && matched_p[hi]) {
          ++hi;
        }
        if (hi == N + 1) continue;
        if (lo == hi) continue;
        debug("reverse %d %d\n", lo, hi);
        res.score += cost[hi - lo + 1];
        if (res.score + (N + 1 - match + 1) / 2 >= best_score) {
          res.score = 1 << 30;
          return res;
        }
        res.pos.emplace_back(lo - 1, hi - 1);
        reverse(seq, lo, hi);
        ok = true;
        break;
      }
      if (!ok) {
        debugStr("fail\n");
        res.score = 1 << 30;
        return res;
      }
      continue;
    }
    res.score += cost[min_len];
    if (res.score + (N + 1 - match + 1) / 2 >= best_score) {
      res.score = 1 << 30;
      return res;
    }
    res.pos.emplace_back(left - 1, right - 1);
    reverse(seq, left, right);
    reverse(matched_p.begin() + left, matched_p.begin() + right);
    if (seq[left - 1] == seq[left]) {
      match++;
      matched_p[left - 1] = true;
      unmatched_n[seq[left]]--;
      if (unmatched_n[seq[left]] == 0) {
        int p = left - 2;
        while (seq[p] == seq[left]) {
          --p;
        }
        if (seq[p] == seq[left] - 1) {
          assert(!matched_p[p]);
          matched_p[p] = true;
          matched_a[seq[left]] = true;
          match++;
        }
        p = left + 1;
        while (seq[p] == seq[left]) {
          ++p;
        }
        if (seq[p] == seq[left] - 1) {
          assert(!matched_p[p - 1]);
          matched_p[p - 1] = true;
          matched_a[seq[left]] = true;
          match++;
        }
      }
    } else if (abs(seq[left - 1] - seq[left]) == 1 && unmatched_n[max(seq[left], seq[left - 1])] == 0 && !matched_a[max(seq[left], seq[left - 1])]) {
      match++;
      matched_p[left - 1] = true;
      matched_a[max(seq[left], seq[left - 1])] = true;
    }
    if (seq[right + 1] == seq[right]) {
      match++;
      matched_p[right] = true;
      unmatched_n[seq[right]]--;
      if (unmatched_n[seq[right]] == 0) {
        int p = right - 1;
        while (seq[p] == seq[right]) {
          --p;
        }
        if (seq[p] == seq[right] - 1) {
          assert(!matched_p[p]);
          matched_p[p] = true;
          matched_a[seq[right]] = true;
          match++;
        }
        p = right + 2;
        while (seq[p] == seq[right]) {
          ++p;
        }
        if (seq[p] == seq[right] - 1) {
          assert(!matched_p[p - 1]);
          matched_p[p - 1] = true;
          matched_a[seq[right]] = true;
          match++;
        }
      }
    } else if (abs(seq[right + 1] - seq[right]) == 1 && unmatched_n[max(seq[right], seq[right + 1])] == 0 && !matched_a[max(seq[right], seq[right + 1])]) {
      match++;
      matched_p[right] = true;
      matched_a[max(seq[right], seq[right + 1])] = true;
    }
    // debug("left:%d right:%d dual:%d cost:%d\n", left, right, dual, res.score);
    // debug_vec(seq);
    // debug_vec(unmatched_n);
    vector<int> valid_matched(K + 1);
    for (int i = 0; i <= N; ++i) {
      if (seq[i] == seq[i + 1]) {
        valid_matched[seq[i]]++;
      }
    }
    for (int i = 1; i <= K; ++i) {
      if (unmatched_n[i] + valid_matched[i] != count_n[i - 1] - 1) {
        debug("%d %d %d %d\n", unmatched_n[i], valid_matched[i], count_n[i - 1], i);
        assert(false);
      }
    }
  }
  debug("score:%d count:%lu\n", res.score, res.pos.size());
  return res;
}

Result solve_naive(vi seq) {
  Result res = {};
  const bool try3 = cost[3] < cost[2] * 3;
  const bool try4 = cost[4] < cost[2] * 6;
  const bool try5 = cost[5] < cost[2] * 10;
  for (int i = 0; i < N; ++i) {
    for (int j = i; j < N; ++j) {
      if (seq[j] == target_seq[i]) {
        while (j > i) {
          const int space = j - i + 1;
          int move = best_path[space];
          if (try3 && move == 2 && 3 <= space && seq[j - 2] > seq[j - 1]) {
            move = 3;
          }
          if (try4 && move <= 3 && 4 <= space && seq[j - 3] > seq[j - 2] && seq[j - 2] > seq[j - 1]) {
            move = 4;
          }
          if (try5 && move <= 4 && 5 <= space && seq[j - 4] > seq[j - 3] && seq[j - 3] > seq[j - 2] && seq[j - 2] > seq[j - 1]) {
            move = 5;
          }
          reverse(seq, j - move + 1, j);
          res.score += cost[move];
          res.pos.emplace_back(j - move + 1, j);
          j -= move - 1;
        }
        break;
      }
    }
  }
  return res;
}

template<int min_skip>
Result solve_naive2(vi seq, double skip_prob) {
  Result res = {};
  for (int i = 0; i < N; ++i) {
    for (int hi = i; hi < N; ++hi) {
      if (seq[hi] == target_seq[i]) {
        while (hi > i) {
          int lo = hi - 1;
          while (lo > i && hi - lo + 1 < max_rev && seq[lo - 1] > seq[lo]) {
            if (hi - lo + 1 >= min_skip) {
              if (rnd.nextDouble() < skip_prob) break;
            }
            --lo;
          }
          reverse(seq, lo, hi);
          res.score += cost[hi - lo + 1];
          res.pos.emplace_back(lo, hi);
          hi = lo;
        }
        break;
      }
    }
  }
  return res;
}

void recover_path_s(int lo, int mid, int hi, vector<pair<int, int>>& ps) {
  while (lo != mid && mid != hi) {
    if (mid - lo <= hi - mid) {
      int use = best_path_k2_s[hi - lo][mid - lo];
      ps.emplace_back(lo, mid + use - 1);
      lo += use;
      mid += use;
    } else {
      int use = best_path_k2_s[hi - lo][hi - mid];
      ps.emplace_back(mid - use, hi - 1);
      hi -= use;
      mid -= use;
    }
  }
}

void recover_path_m(int lo, int mid, int hi, vector<pair<int, int>>& ps) {
  while (lo != mid && mid != hi) {
    if (mid - lo <= hi - mid) {
      int use = best_path_k2_m[hi - lo][mid - lo];
      recover_path_s(mid - use, mid, hi, ps);
      mid -= use;
      hi -= use;
    } else {
      int use = best_path_k2_m[hi - lo][hi - mid];
      recover_path_s(lo, mid, mid + use, ps);
      lo += use;
      mid += use;
    }
  }
}

int solve_k2_(vi unmatch, int best_score, vector<pair<int, int>>& pos) {
  int score = 0;
  static vi ans;
  ans.clear();
  assert(unmatch.size() % 2 == 1);
  while (unmatch.size() > 1) {
    int best_ump = -1;
    int best_cost = 1 << 30;
    for (int i = 0; i < max(1, min((int)unmatch.size() / 2, 200)); ++i) {
      int ump = rnd.nextUInt(unmatch.size() - 2 + (unmatch.size() - 2) / 2);
      if (ump >= unmatch.size() - 2) {
        ump = (ump - (unmatch.size() - 2)) * 2;
      }
      int lo = unmatch[ump];
      int mid = unmatch[ump + 1];
      int hi = unmatch[ump + 2];
      int cur_cost = best_cost_k2_m[hi - lo][min(mid - lo, hi - mid)];
      if (cur_cost < best_cost) {
        best_cost = cur_cost;
        best_ump = ump;
      }
    }
    score += best_cost;
    if (score >= best_score) return score;
    int lo = unmatch[best_ump];
    int mid = unmatch[best_ump + 1];
    int hi = unmatch[best_ump + 2];
    ans.push_back((lo << 20) | (mid << 10) | hi);
    int nmid = lo + (hi - mid);
    unmatch[best_ump] = nmid;
    unmatch.erase(unmatch.begin() + best_ump + 1, unmatch.begin() + best_ump + 3);
  }
  for (int av : ans) {
    int lo = av >> 20;
    int mid = (av >> 10) & 0x3FF;
    int hi = av & 0x3FF;
    recover_path_m(lo, mid, hi, pos);
  }
  return score;
}

Result solve_k2(vi seq, ll time_limit) {
  vi unmatch;
  seq.insert(seq.begin(), 0);
  seq.push_back(1);
  for (int i = 0; i < seq.size() - 1; ++i) {
    if (seq[i] != seq[i + 1]) {
      unmatch.push_back(i);
    }
  }
  debug("k2 unmatch size:%lu\n", unmatch.size());
  Result best_res = {};
  if (unmatch.size() == 1) {
    return best_res;
  } else if (unmatch.size() == 3) {
    best_res.score = solve_k2_(unmatch, 1 << 30, best_res.pos);
    return best_res;
  }
  best_res.score = 1 << 30;
  vector<pair<int, int>> pos;
  for (int i = 1; ; ++i) {
    if ((i & 0xF) == 0) {
      ll elapsed = get_elapsed_msec();
      if (elapsed > time_limit) {
        debug("k2 best score:%d turn:%d\n", best_res.score, i);
        break;
      }
    }
    pos.clear();
    int score = solve_k2_(unmatch, best_res.score, pos);
    if (score < best_res.score) {
      debug("k2 score:%d turn:%d\n", score, i);
      best_res.score = score;
      swap(best_res.pos, pos);
    }
  }
  return best_res;
}

Result solve_sa(vi seq, ll time_limit) {
  int FACTOR_PENA = 1;
  Result res = {};
  int cur_pena = 0;
  for (int i = 0; i < N; ++i) {
    cur_pena += pena_pos[seq[i]][i];
  }
  vi final_pos(N);
  Result best_res;
  best_res.score = 1 << 30;
  vector<pair<int, int>> buf_pos;
  sa_cooler = INITIAL_COOLER;
  const int UPDATE_INTERVAL = 10000;
  int next_update_turn = UPDATE_INTERVAL;
  ll start_time = get_elapsed_msec();
  for (int turn = 0; ; turn += res.pos.size() + 1) {
    // debug("turn:%d pena:%d cost:%d count:%lu\n", turn, cur_pena, res.score, res.pos.size());
    if (turn > next_update_turn) {
      while (next_update_turn <= turn) {
        next_update_turn += UPDATE_INTERVAL;
      }
      ll elapsed = get_elapsed_msec();
      if (elapsed > time_limit) {
        debug("turn:%d pena:%d cost:%d count:%lu\n", turn, cur_pena, res.score, res.pos.size());
        break;
      }
      if (elapsed > time_limit - 50) {
        sa_cooler = 10.0;
      } else {
        double c0 = log(INITIAL_COOLER);
        double c1 = log(FINAL_COOLER);
        const double ratio = (elapsed - start_time) * 1.0 / (time_limit - start_time);
        sa_cooler = exp(c1 * ratio + c0 * (1.0 - ratio));
      }
    }

    seq.assign(orig_seq.begin(), orig_seq.begin() + N);
    for (const auto& p : res.pos) {
      reverse(seq, p.first, p.second);
    }
    iota(final_pos.begin(), final_pos.end(), 0);
    buf_pos.clear();
#ifdef DEBUG
    // verify
    int verify_pena = 0;
    for (int i = 0; i < N; ++i) {
      verify_pena += pena_pos[seq[i]][i];
    }
    if (verify_pena != cur_pena) {
      debug("%d %d %d\n", verify_pena, cur_pena, turn);
      assert(false);
    }
    int verify_cost = 0;
    for (const auto& pp :res.pos) {
      verify_cost += cost[pp.second - pp.first + 1];
    }
    if (verify_cost != res.score) {
      debug("%d %d\n", verify_cost, res.score);
      assert(false);
    }
#endif
    for (int i = res.pos.size(); i >= 0; --i) {
      // change existing reversal
      if (i < res.pos.size()) {
        const auto& pp = res.pos[i];
        bool done = false;
        // shrink
        if (!done && pp.second - pp.first >= 3) {
          const int lo = pp.first;
          const int hi = pp.second;
          const int flo = final_pos[lo];
          const int fhi = final_pos[hi];
          int pena_diff = pena_pos[seq[lo]][fhi] + pena_pos[seq[hi]][flo] - pena_pos[seq[lo]][flo] - pena_pos[seq[hi]][fhi];
          int val_diff = pena_diff * FACTOR_PENA + (cost[hi - lo - 1] - cost[hi - lo + 1]);
          if (accept(val_diff)) {
            cur_pena += pena_diff;
            res.score += cost[hi - lo - 1] - cost[hi - lo + 1];
            pair<int, int> new_pp = {lo + 1, hi - 1};
            buf_pos.push_back(new_pp);
            reverse(final_pos, new_pp.first, new_pp.second);
            if (cur_pena == 0 && res.score < best_res.score) {
              debug("shr score:%d turn:%d i:%d\n", res.score, turn, i);
              best_res.score = res.score;
              best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
              best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
            }
            done = true;
          }
        }
        // extend
        if (!done && 0 < pp.first && pp.second < N - 1) {
          const int lo = pp.first - 1;
          const int hi = pp.second + 1;
          const int flo = final_pos[lo];
          const int fhi = final_pos[hi];
          int pena_diff = pena_pos[seq[lo]][fhi] + pena_pos[seq[hi]][flo] - pena_pos[seq[lo]][flo] - pena_pos[seq[hi]][fhi];
          int val_diff = pena_diff * FACTOR_PENA + (cost[hi - lo + 1] - cost[hi - lo - 1]);
          if (accept(val_diff)) {
            cur_pena += pena_diff;
            res.score += cost[hi - lo + 1] - cost[hi - lo - 1];
            pair<int, int> new_pp = {lo, hi};
            buf_pos.push_back(new_pp);
            reverse(final_pos, new_pp.first, new_pp.second);
            if (cur_pena == 0 && res.score < best_res.score) {
              debug("ext score:%d turn:%d i:%d\n", res.score, turn, i);
              best_res.score = res.score;
              best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
              best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
            }
            done = true;
          }
        }
        // shift
        if (!done) {
          int pena_diff_base = 0;
          for (int lo = pp.first + 1; lo <= pp.second; ++lo) {
            pena_diff_base += pena_pos[seq[lo]][final_pos[lo - 1]] - pena_pos[seq[lo]][final_pos[lo]];
          }
          // shrink right
          if (pp.second - pp.first >= 2) {
            int pena_diff = pena_diff_base + pena_pos[seq[pp.first]][final_pos[pp.second]] - pena_pos[seq[pp.first]][final_pos[pp.first]];
            int val_diff = pena_diff * FACTOR_PENA + cost[pp.second - pp.first] - cost[pp.second - pp.first + 1];
            if (accept(val_diff)) {
              cur_pena += pena_diff;
              res.score += cost[pp.second - pp.first] - cost[pp.second - pp.first + 1];
              pair<int, int> new_pp = {pp.first, pp.second - 1};
              buf_pos.push_back(new_pp);
              reverse(final_pos, new_pp.first, new_pp.second);
              if (cur_pena == 0 && res.score < best_res.score) {
                debug("sr score:%d turn:%d i:%d\n", res.score, turn, i);
                best_res.score = res.score;
                best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
                best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
              }
              done = true;
            }
          }
          // extend left
          if (!done && pp.first > 0) {
            int pena_diff = pena_diff_base + pena_pos[seq[pp.first]][final_pos[pp.first - 1]] - pena_pos[seq[pp.first]][final_pos[pp.first]];
            pena_diff += pena_pos[seq[pp.first - 1]][final_pos[pp.second]] - pena_pos[seq[pp.first - 1]][final_pos[pp.first - 1]];
            int val_diff = pena_diff * FACTOR_PENA + cost[pp.second - pp.first + 2] - cost[pp.second - pp.first + 1];
            if (accept(val_diff)) {
              cur_pena += pena_diff;
              res.score += cost[pp.second - pp.first + 2] - cost[pp.second - pp.first + 1];
              pair<int, int> new_pp = {pp.first - 1, pp.second};
              buf_pos.push_back(new_pp);
              reverse(final_pos, new_pp.first, new_pp.second);
              if (cur_pena == 0 && res.score < best_res.score) {
                debug("el score:%d turn:%d i:%d\n", res.score, turn, i);
                best_res.score = res.score;
                best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
                best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
              }
              done = true;
            }
          }
        }
        if (!done) {
          int pena_diff_base = 0;
          for (int lo = pp.first; lo < pp.second; ++lo) {
            pena_diff_base += pena_pos[seq[lo]][final_pos[lo + 1]] - pena_pos[seq[lo]][final_pos[lo]];
          }
          // shrink left
          if (pp.second - pp.first >= 2) {
            int pena_diff = pena_diff_base + pena_pos[seq[pp.second]][final_pos[pp.first]] - pena_pos[seq[pp.second]][final_pos[pp.second]];
            int val_diff = pena_diff * FACTOR_PENA + cost[pp.second - pp.first] - cost[pp.second - pp.first + 1];
            if (accept(val_diff)) {
              cur_pena += pena_diff;
              res.score += cost[pp.second - pp.first] - cost[pp.second - pp.first + 1];
              pair<int, int> new_pp = {pp.first + 1, pp.second};
              buf_pos.push_back(new_pp);
              reverse(final_pos, new_pp.first, new_pp.second);
              if (cur_pena == 0 && res.score < best_res.score) {
                debug("sr score:%d turn:%d i:%d\n", res.score, turn, i);
                best_res.score = res.score;
                best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
                best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
              }
              done = true;
            }
          }
          // extend right
          if (!done && pp.second < N - 1) {
            int pena_diff = pena_diff_base + pena_pos[seq[pp.second]][final_pos[pp.second + 1]] - pena_pos[seq[pp.second]][final_pos[pp.second]];
            pena_diff += pena_pos[seq[pp.second + 1]][final_pos[pp.first]] - pena_pos[seq[pp.second + 1]][final_pos[pp.second + 1]];
            int val_diff = pena_diff * FACTOR_PENA + cost[pp.second - pp.first + 2] - cost[pp.second - pp.first + 1];
            if (accept(val_diff)) {
              cur_pena += pena_diff;
              res.score += cost[pp.second - pp.first + 2] - cost[pp.second - pp.first + 1];
              pair<int, int> new_pp = {pp.first, pp.second + 1};
              buf_pos.push_back(new_pp);
              reverse(final_pos, new_pp.first, new_pp.second);
              if (cur_pena == 0 && res.score < best_res.score) {
                debug("el score:%d turn:%d i:%d\n", res.score, turn, i);
                best_res.score = res.score;
                best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
                best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
              }
              done = true;
            }
          }
        }

        // remove
        if (!done) {
          int pena_diff = 0;
          for (int lo = pp.first, hi = pp.second; lo < hi; ++lo, --hi) {
            const int flo = final_pos[lo];
            const int fhi = final_pos[hi];
            pena_diff += pena_pos[seq[hi]][flo] + pena_pos[seq[lo]][fhi] - pena_pos[seq[lo]][flo] - pena_pos[seq[hi]][fhi];
          }
          int val_diff = pena_diff * FACTOR_PENA - cost[pp.second - pp.first + 1];
          if (accept(val_diff)) {
            cur_pena += pena_diff;
            res.score -= cost[pp.second - pp.first + 1];
            if (cur_pena == 0 && res.score < best_res.score) {
              debug("rem score:%d turn:%d i:%d\n", res.score, turn, i);
              best_res.score = res.score;
              best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
              best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
            }
            done = true;
          }
        }
        reverse(seq, pp.first, pp.second);
        if (!done) {
          buf_pos.push_back(pp);
          reverse(final_pos, pp.first, pp.second);
        }
      }

      // insert
      if (i + buf_pos.size() < N * 2) { // limit the number of reversal
        int cand_pos = rnd.nextUInt(N - 1);
        int best_val_diff = 1;
        int best_lo = 0;
        int best_hi = 0;
        // even
        int pena_diff = 0;
        for (int lo = cand_pos, hi = cand_pos + 1; 0 <= lo && hi < N; --lo, ++hi) {
          const int flo = final_pos[lo];
          const int fhi = final_pos[hi];
          pena_diff += pena_pos[seq[hi]][flo] + pena_pos[seq[lo]][fhi] - pena_pos[seq[lo]][flo] - pena_pos[seq[hi]][fhi];
          if (pena_diff > 100) break;
          int val_diff = pena_diff * FACTOR_PENA + cost[hi - lo + 1];
          if (val_diff < best_val_diff) {
            best_val_diff = val_diff;
            best_lo = lo;
            best_hi = hi;
          }
        }
        // odd
        pena_diff = 0;
        for (int lo = cand_pos - 1, hi = cand_pos + 1; 0 <= lo && hi < N; --lo, ++hi) {
          const int flo = final_pos[lo];
          const int fhi = final_pos[hi];
          pena_diff += pena_pos[seq[hi]][flo] + pena_pos[seq[lo]][fhi] - pena_pos[seq[lo]][flo] - pena_pos[seq[hi]][fhi];
          if (pena_diff > 100) break;
          int val_diff = pena_diff * FACTOR_PENA + cost[hi - lo + 1];
          if (val_diff < best_val_diff) {
            best_val_diff = val_diff;
            best_lo = lo;
            best_hi = hi;
          }
        }
        // debug("best_val_diff:%d best_lo:%d best_hi:%d\n", best_val_diff, best_lo, best_hi);
        if (best_lo < best_hi && accept(best_val_diff)) {
          int len = best_hi - best_lo + 1;
          cur_pena += (best_val_diff - cost[len]) / FACTOR_PENA;
          res.score += cost[len];
          buf_pos.emplace_back(best_lo, best_hi);
          reverse(final_pos, best_lo, best_hi);
          if (cur_pena == 0 && res.score < best_res.score) {
            debug("ins score:%d turn:%d i:%d\n", res.score, turn, i);
            best_res.score = res.score;
            best_res.pos.assign(res.pos.begin(), res.pos.begin() + i);
            best_res.pos.insert(best_res.pos.end(), buf_pos.rbegin(), buf_pos.rend());
          }
        }
      }
    }
    swap(res.pos, buf_pos);
    reverse(res.pos.begin(), res.pos.end());
  }
  seq.assign(orig_seq.begin(), orig_seq.begin() + N);
  for (const auto& p : res.pos) {
    reverse(seq, p.first, p.second);
  }
  debug_vec(seq);
  Result res_after = solve_naive(seq);
  Result res_after2 = solve_naive2<3>(seq, 0.0);
  if (res_after2.score < res_after.score) {
    swap(res_after.score, res_after2.score);
    swap(res_after.pos, res_after2.pos);
  }
  Result res_after3 = solve_randomized_greedy(seq, res_after.score, max_greedy_len);
  debug("after score:%d %d %d\n", res_after.score, res_after2.score, res_after3.score);
  if (res_after3.score < res_after.score) {
    swap(res_after.score, res_after3.score);
    swap(res_after.pos, res_after3.pos);
  }
  res.score += res_after.score;
  res.pos.insert(res.pos.end(), res_after.pos.begin(), res_after.pos.end());
  debug("sa score:%d\n", res.score);
  return res;
}

struct Range {
  int lower, upper, min_v, max_v, sep_v;
};

Result solve_recursive(vi seq, ll tl) {
  const int LEN_TH = 1;
  const int K_TH = 1;
  vector<Range> que;
  double total_k2_len = 0;
  que.push_back({0, N, 0, K - 1, -1});
  for (int i = 0; i < que.size(); ++i) {
    // debug_vec(seq);
    Range r = que[i];
    // debug("range;%d %d %d %d %d\n", r.lower, r.upper, r.min_v, r.sep_v, r.max_v);
    if (r.upper - r.lower < LEN_TH || r.max_v - r.min_v < K_TH) {
      continue;
    }
    total_k2_len += pow(r.upper - r.lower, 1.5);
    int sep_k = 0;
    int min_diff = N;
    int left_sum = 0;
    for (int j = r.min_v, sum = 0; j < r.max_v; ++j) {
      sum += count_n[j];
      int diff = abs(sum - (r.upper - r.lower - sum));
      if (diff < min_diff) {
        min_diff = diff;
        sep_k = j + 1;
        left_sum = sum;
      }
    }
    que[i].sep_v = sep_k;
    // debug("partition:[%d %d %d %d %d]\n", r.lower, r.upper, r.min_v, sep_k, r.max_v);
    que.push_back({r.lower, r.lower + left_sum, r.min_v, sep_k - 1, -1});
    que.push_back({r.lower + left_sum, r.upper, sep_k, r.max_v, -1});
  }

  Result res = {};
  ll time_limit = get_elapsed_msec();
  ll left_time = tl - time_limit;
  for (int i = 0; i < que.size(); ++i) {
    const Range& r = que[i];
    // debug("%d range;%d %d %d %d %d\n", i, r.lower, r.upper, r.min_v, r.sep_v, r.max_v);
    if (r.min_v == r.max_v) continue;
    vi bi_seq(r.upper - r.lower, 0);
    for (int j = r.lower; j < r.upper; ++j) {
      if (seq[j] >= r.sep_v) {
        bi_seq[j - r.lower] = 1;
      }
    }
    time_limit += left_time * pow(r.upper - r.lower, 1.5) / total_k2_len;
    debug("k2:%d %d %d %d %d %lld\n", r.lower, r.upper, r.min_v, r.sep_v, r.max_v, time_limit);
    Result sub_res = solve_k2(bi_seq, time_limit);
    res.score += sub_res.score;
    for (const auto& pp : sub_res.pos) {
      res.pos.emplace_back(pp.first + r.lower, pp.second + r.lower);
      reverse(seq, pp.first + r.lower, pp.second + r.lower);
    }
  }
  return res;
}

class ReversalSort {
public:
  Result findSolution() {
    vi initial_seq(orig_seq.begin(), orig_seq.begin() + N);
    Result best_res = solve_naive(initial_seq);
    debug("naive score:%d\n", best_res.score);
    if (max_rev == 2) {
      debugStr("trivial case\n");
      return best_res;
    }
    Result res2 = solve_naive2<3>(initial_seq, 0.0);
    debug("naive2 score:%d\n", res2.score);
    if (res2.score < best_res.score) {
      best_res = res2;
    }
    max_greedy_len = X >= 2.0 ? 7 : min(best_res.score < 100000 ? 200 : best_res.score < 250000 ? 80 : best_res.score < 400000 ? 50 : 20, N);
    const bool small_k = (X < 1.0 && K < 8) || (X < 1.6 && K < 4);
    if (0.3 <= X && X < 1.7) {
      const int REC_COUNT = 20;
      const ll before_time = get_elapsed_msec();
      ll time_budget = TL - before_time;
      if (N < 40) {
        time_budget /= 2;
      }
      for (int i = 0; i < REC_COUNT; ++i) {
        Result res = solve_recursive(initial_seq, time_budget * (i + 1) / REC_COUNT + before_time);
        if (res.score < best_res.score) {
          best_res.score = res.score;
          swap(best_res.pos, res.pos);
          debug("k2.score:%d\n", best_res.score);
        }
      }
    } else {
      if (0.25 < X && X < 1.8) {
        ll before_time = get_elapsed_msec();
        Result res = solve_recursive(initial_seq, (TL - before_time) / 10 + before_time);
        if (res.score < best_res.score) {
          best_res.score = res.score;
          swap(best_res.pos, res.pos);
          debug("k2 score:%d\n", best_res.score);
        }
      }
      if (0.5 <= X && X < 2.0) {
        set_pena_pos(X < 1.57 ? 1.0 : X < 1.77 ? 1.1 : 1.2);
        ll tl = (N < 50 || small_k) ? TL / 2 : TL - 50;
        Result res = solve_sa(initial_seq, tl);
        if (res.score < best_res.score) {
          best_res = res;
        }
      }
    }

    set_pena_pos(1.5);
    ll worst_time = max(N / 5, 20);
    ll before_time = get_elapsed_msec();
    for (int turn = 0; ; ++turn) {
      if (before_time + worst_time > TL - 50) {
        debug("total turn:%d best_score=%u worst_time=%lld\n", turn, best_res.score, worst_time);
        break;
      }
      Result res;
      if (X <= 0.7 || small_k) {
        res = solve_adj_greedy(initial_seq, best_res.score);
      } else if (X <= 2.0 || (N < 50 && (turn & 1))) {
        res = solve_randomized_greedy(initial_seq, best_res.score, max_greedy_len);
      } else if (max_rev == 3) {
        res = solve_naive2<2>(initial_seq, rnd.nextDouble() * 0.05);
      } else {
        res = solve_naive2<3>(initial_seq, rnd.nextDouble() * 0.2);
      }
      if (res.score < best_res.score) {
        best_res = res;
        debug("turn:%d best_score=%u\n", turn, best_res.score);
      }
      ll after_time = get_elapsed_msec();
      worst_time = max(worst_time, after_time - before_time);
      before_time = after_time;
    }
    return best_res;
  }
};

int main() {
  start_time = get_time();
  scanf("%d %d %lf", &N, &K, &X);
  for (int i = 0; i < N; ++i) {
    scanf("%d", &orig_seq[i]);
  }
  copy(orig_seq.begin(), orig_seq.begin() + N, target_seq.begin());
  sort(target_seq.begin(), target_seq.begin() + N);
  // compress
  vi transform(N);
  int prev = -1;
  for (int i = 0, j = 0; i < N; ++i) {
    if (prev == target_seq[i]) continue;
    transform[target_seq[i]] = j++;
    prev = target_seq[i];
  }
  count_n.resize(transform[target_seq[N - 1]] + 2);
  for (int i = 0; i < N; ++i) {
    orig_seq[i] = transform[orig_seq[i]];
    target_seq[i] = transform[target_seq[i]];
    count_n[target_seq[i]]++;
  }
  int effective_k = target_seq[N - 1] + 1;
  debug("N:%d K:%d EK:%d X:%.4f\n", N, K, effective_k, X);
  K = effective_k;
  count_n[K] = 1; // sentinel

  vi dp(N + 1);
  for (int i = 2; i <= N; ++i) {
    cost[i] = (int)pow(i, X);
    best_path[i] = i;
    dp[i] = cost[i];
    for (int j = 2; j < i; ++j) {
      if (dp[i - j + 1] + cost[j] < dp[i]) {
        dp[i] = dp[i - j + 1] + cost[j];
        best_path[i] = j;
      }
    }
  }
  for (int i = 2; i <= N; ++i) {
    fill(best_cost_k2_s[i].begin() + 1, best_cost_k2_s[i].begin() + i, 1 << 24);
    for (int j = 1; j <= (i + 1) / 2; ++j) {
      for (int k = 1; k <= i - j; ++k) {
        int c = cost[j + k] + best_cost_k2_s[i - k][j];
        if (c < best_cost_k2_s[i][j]) {
          best_cost_k2_s[i][j] = c;
          best_path_k2_s[i][j] = k;
        }
      }
    }
    for (int j = (i + 1) / 2 + 1; j < i; ++j) {
      best_cost_k2_s[i][j] = best_cost_k2_s[i][i - j];
    }
  }
  for (int i = 2; i <= N; ++i) {
    fill(best_cost_k2_m[i].begin() + 1, best_cost_k2_m[i].begin() + i, 1 << 24);
    for (int j = 1; j <= (i + 1) / 2; ++j) {
      for (int k = 1; k <= j; ++k) {
        int c = best_cost_k2_s[i - j + k][k] + best_cost_k2_m[i - k][j - k];
        if (c < best_cost_k2_m[i][j]) {
          best_cost_k2_m[i][j] = c;
          best_path_k2_m[i][j] = k;
        }
      }
    }
  }
  max_rev = N;
  for (int i = 3; i <= N; ++i) {
    if (cost[i] >= cost[2] * i * (i - 1) / 2) {
      max_rev = i - 1;
      break;
    }
  }
  debug("time for preprocess:%lld\n", get_elapsed_msec());
  debug("max_rev:%d\n", max_rev);
  debugStr("cost:\n");
  debug_vec(vi(cost.begin() + 2, cost.begin() + min(N, 30)));

  ReversalSort prog;
  const Result ret = prog.findSolution();
  printf("%lu\n", ret.pos.size());
  for (const auto& p : ret.pos) {
    printf("%d %d\n", min(p.first, p.second), max(p.first, p.second));
  }
#ifdef DEBUG
  if (N < 40) {
    for (const auto& p : ret.pos) {
      for (int i = 0; i < N; ++i) {
        if (i == p.first) {
          debugStr("[");
        } else if (i == p.second + 1) {
          debugStr("]");
        } else {
          debugStr(" ");
        }
        debug("%2d", orig_seq[i]);
      }
      if (p.second == N - 1) {
        debugStr("]");
      }
      debugln();
      reverse(orig_seq, p.first, p.second);
    }
  }
  vi histo;
  for (auto& ps : ret.pos) {
    int len = abs(ps.second - ps.first + 1);
    if (histo.size() <= len) {
      histo.resize(len + 1);
    }
    histo[len]++;
  }
  debugStr("length histogram:\n");
  for (int i = 2; i < histo.size(); ++i) {
    if (histo[i] > 0) debug("%d:%d\n", i, histo[i]);
  }
#endif
  fflush(stdout);
  debug("elapsed:%lld\n", get_elapsed_msec());
  PRINT_TIMER();
  PRINT_COUNTER();
}