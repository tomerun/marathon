require 'net/http'
require 'nokogiri'

entrance = URI.parse('http://commons.wikimedia.org/wiki/Special:Random/Image')
http = Net::HTTP.new('commons.wikimedia.org', 443)
http.use_ssl = true
upload_http = Net::HTTP.new('upload.wikimedia.org', 443)
upload_http.use_ssl = true
50.times do
	begin
		res = http.get('/wiki/Special:Random/Image')
		url = URI.parse(res['location'])
		res = http.get(url.path)
		doc = Nokogiri::HTML(res.body)
		anchor = doc.css('div.mw-filepage-resolutioninfo a').first
		next unless anchor
		puts anchor.text
		unless anchor.text =~ /(\d+) × (\d+) pixels/
			puts "invalid size:#{anchor.text}"
			next
		end
		w = $1.to_i
		h = $2.to_i
		if w < 250 || 800 < w || h < 250 || 800 < h
			puts "invalid dimension:#{anchor.text}"
			next
		end
		url = URI.parse(anchor['href'])

		res = upload_http.get(url.path)
		IO.write(File.basename(url.path), res.body)
	rescue Exception => e
		puts e
	end
end
