#include <algorithm>
#include <utility>
#include <vector>
#include <bitset>
#include <string>
#include <iostream>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifndef LOCAL
#define NDEBUG
#else
// #define MEASURE_TIME
// #define DEBUG
#endif
#include <cassert>

using namespace std;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t i64;
typedef int64_t ll;
typedef uint64_t ull;
typedef vector<int> vi;
typedef vector<vi> vvi;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 15000;
#else
#ifdef TEST
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 19000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 19000;
#endif
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

alignas(16) int target[800][800];
alignas(16) int ps[800][800];
int pena[800][801], want[800][800], med[800][800];
array<array<int, 256>, 3> hist;
int N, H, W;
const int MED_WINDOW = 7;

struct Circle {
	int r, c, radius, color;
};

inline int branchfree_abs(int32_t v) {
	int32_t mask = v >> 31;
	return (v + mask) ^ mask;
}

int penalty(int t, int c) {
	int tr = t >> 16;
	int cr = c >> 16;
	int tg = (t >> 8) & 0xFF;
	int cg = (c >> 8) & 0xFF;
	int tb = t & 0xFF;
	int cb = c & 0xFF;
	return branchfree_abs(tr - cr) + branchfree_abs(tg - cg) + branchfree_abs(tb - cb);
}

int penalty(int t, int r, int g, int b) {
	int tr = t >> 16;
	int tg = (t >> 8) & 0xFF;
	int tb = t & 0xFF;
	return branchfree_abs(tr - r) + branchfree_abs(tg - g) + branchfree_abs(tb - b);
}

inline int calc_want(int target, int cur) {
	int ret = 0;
	for (int i = 0; i < 3; ++i) {
		int t = (target >> (8 * i)) & 0xFF;
		int c = (cur >> (8 * i)) & 0xFF;
		int diff = t - c;
		int next = c + diff * 2;
		if (next < 0) next = 0;
		if (next > 0xFF) next = 0xFF;
		ret |= next << (8 * i);
	}
	return ret;
}

inline int calc_want(int target, int r, int g, int b) {
	int ret = 0;
	{
		int t = target >> 16;
		int diff = t - r;
		int next = r + diff * 2;
		if (next < 0) next = 0;
		if (next > 0xFF) next = 0xFF;
		ret |= next << 16;
	}
	{
		int t = (target >> 8) & 0xFF;
		int diff = t - g;
		int next = g + diff * 2;
		if (next < 0) next = 0;
		if (next > 0xFF) next = 0xFF;
		ret |= next << 8;
	}
	{
		int t = target & 0xFF;
		int diff = t - b;
		int next = b + diff * 2;
		if (next < 0) next = 0;
		if (next > 0xFF) next = 0xFF;
		ret |= next;
	}
	return ret;
}

void precalc_median(int r, int c) {
	static int rbuf[MED_WINDOW * MED_WINDOW], gbuf[MED_WINDOW * MED_WINDOW], bbuf[MED_WINDOW * MED_WINDOW];
	static array<int, 16> rhist, ghist, bhist;
	rhist.fill(0);
	ghist.fill(0);
	bhist.fill(0);
	for (int i = 0; i < MED_WINDOW; ++i) {
		for (int j = 0; j < MED_WINDOW; ++j) {
			int color = want[r - MED_WINDOW / 2 + i][c - MED_WINDOW / 2 + j];
			rbuf[i * MED_WINDOW + j] = color >> 16;
			gbuf[i * MED_WINDOW + j] = (uint8_t)(color >> 8);
			bbuf[i * MED_WINDOW + j] = (uint8_t)color;
			rhist[rbuf[i * MED_WINDOW + j] >> 4]++;
			ghist[gbuf[i * MED_WINDOW + j] >> 4]++;
			bhist[bbuf[i * MED_WINDOW + j] >> 4]++;
		}
	}

	int rsum = 0, gsum = 0, bsum = 0;
	int rhi = 0, ghi = 0, bhi = 0;
	for (;; ++rhi) {
		rsum += rhist[rhi];
		if (rsum >= MED_WINDOW * MED_WINDOW / 2) break;
	}
	rsum -= rhist[rhi];
	for (;; ++ghi) {
		gsum += ghist[ghi];
		if (gsum >= MED_WINDOW * MED_WINDOW / 2) break;
	}
	gsum -= ghist[ghi];
	for (;; ++bhi) {
		bsum += bhist[bhi];
		if (bsum >= MED_WINDOW * MED_WINDOW / 2) break;
	}
	bsum -= bhist[bhi];

	rhist.fill(0);
	ghist.fill(0);
	bhist.fill(0);

	for (int i = 0; i < MED_WINDOW * MED_WINDOW; ++i) {
		rhist[rbuf[i] & 0xF] += (rbuf[i] >> 4) == rhi;
		ghist[gbuf[i] & 0xF] += (gbuf[i] >> 4) == ghi;
		bhist[bbuf[i] & 0xF] += (bbuf[i] >> 4) == bhi;
	}

	int red, green, blue;
	int lo = 0;
	for (;; ++lo) {
		rsum += rhist[lo];
		if (rsum >= MED_WINDOW * MED_WINDOW / 2) {
			red = (rhi << 4) | lo;
			break;
		}
	}
	lo = 0;
	for (;; ++lo) {
		gsum += ghist[lo];
		if (gsum >= MED_WINDOW * MED_WINDOW / 2) {
			green = (ghi << 4) | lo;
			break;
		}
	}
	lo = 0;
	for (;; ++lo) {
		bsum += bhist[lo];
		if (bsum >= MED_WINDOW * MED_WINDOW / 2) {
			blue = (bhi << 4) | lo;
			break;
		}
	}
	med[r][c] = (red << 16) | (green << 8) | blue;
}

void apply(const Circle& circle) {
	START_TIMER(0);
	for (int y = max(0, circle.r - circle.radius); y <= min(H - 1, circle.r + circle.radius); ++y) {
		const int width = (int)(sqrt(sq(circle.radius) - sq(circle.r - y)));
		const int left = max(0, circle.c - width);
		const int right = min(W - 1, circle.c + width);
		if (left > right) continue;
		for (int x = left; x <= right; ++x) {
			int red = circle.color >> 16;
			int green = (circle.color >> 8) & 0xFF;
			int blue = circle.color & 0xFF;
			red += (ps[y][x] >> 16);
			green += (ps[y][x] >> 8) & 0xFF;
			blue += ps[y][x] & 0xFF;
			red >>= 1;
			green >>= 1;
			blue >>= 1;
			ps[y][x] = (red << 16) | (green << 8) | blue;
			pena[y][x+1] = pena[y][x] + penalty(target[y][x], red, green, blue);
			want[y][x] = calc_want(target[y][x], red, green, blue);
		}
		if (right < W - 1) {
			int diff = pena[y][right + 1] + penalty(target[y][right + 1], ps[y][right + 1]) - pena[y][right + 2];
			for (int x = right + 1; x < W; ++x) {
				pena[y][x+1] += diff;
			}
		}
	}
	for (int y = max(MED_WINDOW / 2, circle.r - circle.radius - MED_WINDOW / 2); y <= min(H - 1 - MED_WINDOW / 2, circle.r + circle.radius + MED_WINDOW / 2); ++y) {
		int base_y = y < circle.r - MED_WINDOW / 2 ? y + MED_WINDOW / 2 : (y > circle.r + MED_WINDOW / 2 ? y - MED_WINDOW / 2 : circle.r);
		const int width = (int)(sqrt(sq(circle.radius) - sq(circle.r - base_y)));
		const int left = max(0, circle.c - width - MED_WINDOW / 2);
		const int right = min(W - 1, circle.c + width + MED_WINDOW / 2);
		for (int x = left; x <= right; ++x) {
			precalc_median(y, x);
		}
	}
	STOP_TIMER(0);
}

int median(const array<int, 256>& h, int count) {
	int sum = 0;
	for (int i = 0; ; ++i) {
		sum += h[i];
		if (sum >= count / 2) return i;
	}
	return 255;
}

template<bool strict>
pair<int, int> place(int r, int c, int l) {
	START_TIMER(1);
	const int l2 = l * l;
	int color, red, green, blue;
	if (strict) {
		hist[0].fill(0);
		hist[1].fill(0);
		hist[2].fill(0);
		int count = 0;
		for (int y = max(0, r - l); y <= min(H - 1, r + l); ++y) {
			const int width = (int)(sqrt(l2 - sq(r - y)));
			const int left = max(0, c - width);
			const int right = min(W - 1, c + width);
			if (right - left + 1 < 0) continue;
			count += right - left + 1;
			for (int x = left; x <= right; ++x) {
				hist[0][want[y][x] >> 16]++;
				hist[1][(uint8_t)(want[y][x] >> 8)]++;
				hist[2][(uint8_t)want[y][x]]++;
			}
		}
		red = median(hist[0], count);
		green = median(hist[1], count);
		blue = median(hist[2], count);
		color = (red << 16) | (green << 8) | blue;
	} else {
		int mr = min(H - MED_WINDOW / 2 - 1, max(MED_WINDOW / 2, r));
		int mc = min(W - MED_WINDOW / 2 - 1, max(MED_WINDOW / 2, c));
		color = med[mr][mc];
		red = color >> 16;
		green = (color >> 8) & 0xFF;
		blue = color & 0xFF;
	}
	STOP_TIMER(1);

	START_TIMER(2);
	pair<int, int> ret = {0, color};
	const __m128i mask = _mm_set1_epi32(0xFF00);
	const __m128i mask2 = _mm_set1_epi8(1);
	const __m128i col = _mm_set1_epi32(color);
	for (int y = max(0, r - l); y <= min(H - 1, r + l); ++y) {
		const int width = (int)(sqrt(l2 - sq(r - y)));
		const int left = max(0, c - width);
		const int right = min(W - 1, c + width);
		if (left > right) continue;
		ret.first += pena[y][right + 1] - pena[y][left];
		int x = left;
		if (left / 4 != right / 4) {
			for (; x % 4 != 0; ++x) {
				int nr = red + (ps[y][x] >> 16);
				int ng = green + ((ps[y][x] >> 8) & 0xFF);
				int nb = blue + (ps[y][x] & 0xFF);
				nr >>= 1;
				ng >>= 1;
				nb >>= 1;
				ret.first -= penalty(target[y][x], nr, ng, nb);
			}
		}
		for (; x + 3 <= right; x += 4) {
			__m128i p = _mm_load_si128((const __m128i*)&ps[y][x]);
			__m128i t = _mm_load_si128((const __m128i*)&target[y][x]);
			__m128i after = _mm_avg_epu8(p, col);
			__m128i cross = _mm_and_si128(_mm_xor_si128(p, col), mask2);
			after = _mm_sub_epi8(after, cross);
			__m128i ma = _mm_max_epu8(t, after);
			__m128i mi = _mm_min_epu8(t, after);
			__m128i diff = _mm_sub_epi8(ma, mi);
			__m128i gr = _mm_and_si128(diff, mask);
			diff = _mm_xor_si128(diff, gr);
			__m128i sum = _mm_add_epi16(diff, _mm_srli_si128(gr, 1));
			sum = _mm_add_epi16(sum, _mm_srli_si128(diff, 2));
			sum = _mm_add_epi32(sum, _mm_srli_si128(sum, 4));
			sum = _mm_add_epi32(sum, _mm_srli_si128(sum, 8));
			ret.first -= _mm_extract_epi16(sum, 0);
		}
		for (; x <= right; ++x) {
			int nr = red + (ps[y][x] >> 16);
			int ng = green + ((ps[y][x] >> 8) & 0xFF);
			int nb = blue + (ps[y][x] & 0xFF);
			nr >>= 1;
			ng >>= 1;
			nb >>= 1;
			ret.first -= penalty(target[y][x], nr, ng, nb);
		}
	}
	STOP_TIMER(2);
	return ret;
}

Circle initial() {
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			hist[0][want[y][x] >> 16]++;
			hist[1][(want[y][x] >> 8) & 0xFF]++;
			hist[2][want[y][x] & 0xFF]++;
		}
	}
	int red = median(hist[0], H*W);
	int green = median(hist[1], H*W);
	int blue = median(hist[2], H*W);
	return {H / 2, W / 2, (int)(sqrt(H * H + W * W) / 2 + 1), (red << 16) | (green << 8) | blue};
}

int improve(Circle& circle, int gain) {
	int count = (int)(10000 / sqrt(N));
	for (int i = 0; i < count; ++i) {
		int ny = circle.r + rnd.nextUInt(9) - 4;
		int nx = circle.c + rnd.nextUInt(9) - 4;
		int nl = circle.radius + rnd.nextUInt(9) - 4;
		if (nl < 1) nl = 1;
		if (nl > 1000) nl = 1000;
		if (ny == circle.r && nx == circle.c && nl == circle.radius) {
			--i;
			continue;
		}
		auto res = i < count * 1 / 8 ? place<false>(ny, nx, nl) : place<true>(ny, nx, nl);
		if (res.first > gain) {
			gain = res.first;
			circle.r = ny;
			circle.c = nx;
			circle.radius = nl;
			circle.color = res.second;
		}
	}
	return gain;
}

vector<Circle> greedy() {
	vector<Circle> ret(N);
	ret[0] = initial();
	apply(ret[0]);
	vector<pair<Circle, int>> cands;
	vector<vector<uint64_t>> exists((H + 3) / 4, vector<uint64_t>((W + 3) / 4));
	double max_len = 200;
	double min_len = 20;
	int count = (int)(10000 / sqrt(N));
	for (int i = 1; i < N; ++i) {
		for (int j = 0, trial = 0; j < count; ++j, ++trial) {
			if (trial > 10000) break;
			int r = rnd.nextUInt(H);
			int c = rnd.nextUInt(W);
			int l = rnd.nextUInt((int)max_len) + (int)min_len;
			uint64_t bit = 1ull << min(63, l / 4);
			if (exists[r / 4][c / 4] & bit) {
				--j;
				continue;
			}
			exists[r / 4][c / 4] |= bit;
			auto gain = place<false>(r, c, l);
			Circle circle = {r, c, l, gain.second};
			cands.emplace_back(circle, gain.first);
		}
		int bestI = 0;
		for (int j = 1; j < cands.size(); ++j) {
			if (cands[j].second > cands[bestI].second) {
				bestI = j;
			}
		}
		swap(cands[bestI], cands.back());
		Circle best_circle = cands.back().first;
		exists[best_circle.r / 4][best_circle.c / 4] ^= (1ull << min(63, best_circle.radius / 4));
		// cerr << "(" << best_circle.r << "," << best_circle.c << ") " << best_circle.radius << " [";
		// cerr << (best_circle.color >> 16) << "," << ((best_circle.color >> 8) & 0xFF) << "," << (best_circle.color & 0xFF) << endl;
		improve(best_circle, cands.back().second);
		// cerr << "(" << best_circle.r << "," << best_circle.c << ") " << best_circle.radius << " [";
		// cerr << (best_circle.color >> 16) << "," << ((best_circle.color >> 8) & 0xFF) << "," << (best_circle.color & 0xFF) << "]" << endl;
		// cerr << cands.back().second << "->" << gain << endl;
		cands.pop_back();
		for (int j = 0; j < cands.size(); ++j) {
			int d2 = sq(best_circle.r - cands[j].first.r) + sq(best_circle.c - cands[j].first.c);
			if (sq(best_circle.radius + cands[j].first.radius) >= d2) {
				exists[cands[j].first.r / 4][cands[j].first.c / 4] ^= (1ull << min(63, cands[j].first.radius / 4));
				swap(cands[j], cands.back());
				cands.pop_back();
				--j;
			}
		}
		ret[i] = best_circle;
		apply(ret[i]);
		max_len = max(20.0, max_len * 0.99);
		min_len = max(3.0, min_len * 0.99);
	}
	return ret;
}

inline uint32_t want_bi(uint32_t target, uint32_t backward, uint32_t forward, int shift) {
	uint32_t cur = forward + (backward << shift);
	if ((cur >> 24) >= target) return 0;
	uint32_t ret = ((target << 24) - cur) >> shift;
	return ret > 0xFF ? 0xFF : ret;
}

uint32_t sum_col[800][800][3], layer[800][800], wc[800][800];
void hill_climb(vector<Circle>& circles) {
	for (int turn = 0; ; ++turn) {
		if (reach_time_limit()) {
			break;
		}
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				sum_col[i][j][0] = sum_col[i][j][1] = sum_col[i][j][2] = 0;
			}
		}
		for (int i = N - 1; i > 0; --i) {
			if ((i & 0xF) == 0 && reach_time_limit()) break;
			const Circle& c = circles[i];
			for (int y = max(0, c.r - c.radius); y <= min(H - 1, c.r + c.radius); ++y) {
				const int width = (int)(sqrt(sq(c.radius) - sq(c.r - y)));
				const int left = max(0, c.c - width);
				const int right = min(W - 1, c.c + width);
				for (int x = left; x <= right; ++x) {
					uint32_t red = c.color >> 16;
					uint32_t green = (c.color >> 8) & 0xFF;
					uint32_t blue = c.color & 0xFF;
					int shift = (23 - layer[y][x]);
					layer[y][x]++;
					if (shift < 0) continue;
					sum_col[y][x][0] += (red << shift);
					sum_col[y][x][1] += (green << shift);
					sum_col[y][x][2] += (blue << shift);
				}
			}
		}
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				ps[i][j] = 0;
				pena[i][j] = penalty(target[i][j], sum_col[i][j][0] >> 24, sum_col[i][j][1] >> 24, sum_col[i][j][2] >> 24);
				int shift = 23 - layer[i][j];
				if (shift < 0) continue;
				uint32_t red = ps[i][j] >> 16;
				uint32_t green = (ps[i][j] >> 8) & 0xFF;
				uint32_t blue = ps[i][j] & 0xFF;
				wc[i][j] =  want_bi(target[i][j] >> 16,         red,   sum_col[i][j][0], shift) << 16;
				wc[i][j] |= want_bi((target[i][j] >> 8) & 0xFF, green, sum_col[i][j][1], shift) << 8;
				wc[i][j] |= want_bi(target[i][j] & 0xFF,        blue,  sum_col[i][j][2], shift);
			}
		}
		for (int i = 0; i < N; ++i) {
			if (reach_time_limit()) {
				// cerr << "turn:" << turn << " " << i << endl;
				break;
			}
			START_TIMER(4);
			Circle best_circle = circles[i];
			int best_pena = 1 << 30;
			const int iter = min(200, max(50, 10000 / best_circle.radius));
			for (int j = 0; j < iter; ++j) {
				hist[0].fill(0);
				hist[1].fill(0);
				hist[2].fill(0);
				int r = best_circle.r + (j == 0 ? 0 : rnd.nextUInt(7) - 3);
				int c = best_circle.c + (j == 0 ? 0 : rnd.nextUInt(7) - 3);
				int l = best_circle.radius + (j == 0 ? 0 : rnd.nextUInt(7) - 3);
				r = max(-1000, min(r, 1000));
				c = max(-1000, min(c, 1000));
				l = max(1, min(l, 1000));
				int cardinality = 0;
				START_TIMER(5);
				for (int y = max(0, r - l); y <= min(H - 1, r + l); ++y) {
					const int width = (int)(sqrt(sq(l) - sq(r - y)));
					const int left = max(0, c - width);
					const int right = min(W - 1, c + width);
					for (int x = left; x <= right; ++x) {
						int shift = (23 - layer[y][x]);
						if (shift < 0) continue;
						int weight = 1 << (shift < 15 ? 0 : shift - 15);
						hist[0][wc[y][x] >> 16] += weight;
						hist[1][(wc[y][x] >> 8) & 0xFF] += weight;
						hist[2][wc[y][x] & 0xFF] += weight;
						cardinality += weight;
					}
				}
				STOP_TIMER(5);
				START_TIMER(6);
				uint32_t red = median(hist[0], cardinality);
				uint32_t green = median(hist[1], cardinality);
				uint32_t blue = median(hist[2], cardinality);
				STOP_TIMER(6);
				int pena_sum = 0;
				START_TIMER(7);
				for (int y = max(0, r - l); y <= min(H - 1, r + l); ++y) {
					const int width = (int)(sqrt(sq(l) - sq(r - y)));
					const int left = max(0, c - width);
					const int right = min(W - 1, c + width);
					for (int x = left; x <= right; ++x) {
						int shift = (24 - layer[y][x]);
						if (shift < 0) continue;
						pena_sum -= pena[y][x];
						uint32_t rcur = ((ps[y][x] >> 16) + red) >> 1;
						pena_sum += branchfree_abs((target[y][x] >> 16) - (int)((sum_col[y][x][0] + (rcur << shift)) >> 24));
						uint32_t gcur = (((ps[y][x] >> 8) & 0xFF) + green) >> 1;
						pena_sum += branchfree_abs(((target[y][x] >> 8) & 0xFF) - (int)((sum_col[y][x][1] + (gcur << shift)) >> 24));
						uint32_t bcur = ((ps[y][x] & 0xFF) + blue) >> 1;
						pena_sum += branchfree_abs((target[y][x] & 0xFF) - (int)((sum_col[y][x][2] + (bcur << shift)) >> 24));
					}
				}
				STOP_TIMER(7);
				// if (j == 0) cerr << "initial pena:" << pena_sum << endl;
				if (pena_sum < best_pena) {
					best_pena = pena_sum;
					best_circle.r = r;
					best_circle.c = c;
					best_circle.radius = l;
					best_circle.color = (red << 16) | (green << 8) | blue;
				}
			}
			STOP_TIMER(4);
			// cerr << "optimized pena:" << best_pena << endl;
			// cerr << circles[i].r << "," << circles[i].c << " " << circles[i].radius << "[" << (circles[i].color >> 16) << " " << ((circles[i].color >> 8) & 0xFF) << " " << (circles[i].color & 0xFF) << endl;
			circles[i] = best_circle;
			// cerr << circles[i].r << "," << circles[i].c << " " << circles[i].radius << "[" << (circles[i].color >> 16) << " " << ((circles[i].color >> 8) & 0xFF) << " " << (circles[i].color & 0xFF) << endl;
			if (i == N - 1) break;
			for (int y = max(0, circles[i].r - circles[i].radius); y <= min(H - 1, circles[i].r + circles[i].radius); ++y) {
				const int width = (int)(sqrt(sq(circles[i].radius) - sq(circles[i].r - y)));
				const int left = max(0, circles[i].c - width);
				const int right = min(W - 1, circles[i].c + width);
				for (int x = left; x <= right; ++x) {
					uint32_t red = circles[i].color >> 16;
					uint32_t green = (circles[i].color >> 8) & 0xFF;
					uint32_t blue = circles[i].color & 0xFF;
					int shift = (24 - layer[y][x]);
					red += (ps[y][x] >> 16);
					green += (ps[y][x] >> 8) & 0xFF;
					blue += ps[y][x] & 0xFF;
					red >>= 1;
					green >>= 1;
					blue >>= 1;
					ps[y][x] = (red << 16) | (green << 8) | blue;
					wc[y][x]  = want_bi(target[y][x] >> 16,         red,   sum_col[y][x][0], shift - 1) << 16;
					wc[y][x] |= want_bi((target[y][x] >> 8) & 0xFF, green, sum_col[y][x][1], shift - 1) << 8;
					wc[y][x] |= want_bi(target[y][x] & 0xFF,        blue,  sum_col[y][x][2], shift - 1);
					if (shift < 0) {
						red = green = blue = 0;
					} else {
						red <<= shift;
						green <<= shift;
						blue <<= shift;
					}
					pena[y][x] = penalty(target[y][x], (sum_col[y][x][0] + red) >> 24, (sum_col[y][x][1] + green) >> 24, (sum_col[y][x][2] + blue) >> 24);
				}
			}
			// int total = 0;
			// for (int i = 0 ; i < H; ++i) {
			// 	for (int j = 0; j < W; ++j) {
			// 		total += pena[i][j];
			// 	}
			// }
			// cerr << "total:" << total << endl;
			for (int y = max(0, circles[i+1].r - circles[i+1].radius); y <= min(H - 1, circles[i+1].r + circles[i+1].radius); ++y) {
				const int width = (int)(sqrt(sq(circles[i+1].radius) - sq(circles[i+1].r - y)));
				const int left = max(0, circles[i+1].c - width);
				const int right = min(W - 1, circles[i+1].c + width);
				for (int x = left; x <= right; ++x) {
					uint32_t red = circles[i+1].color >> 16;
					uint32_t green = (circles[i+1].color >> 8) & 0xFF;
					uint32_t blue = circles[i+1].color & 0xFF;
					layer[y][x]--;
					int shift = (24 - layer[y][x]);
					if (shift > 0) {
						sum_col[y][x][0] -= red << (shift - 1);
						sum_col[y][x][1] -= green << (shift - 1);
						sum_col[y][x][2] -= blue << (shift - 1);
					}
					red = ps[y][x] >> 16;
					green = (ps[y][x] >> 8) & 0xFF;
					blue = ps[y][x] & 0xFF;
					pena[y][x] = penalty(target[y][x], (sum_col[y][x][0] + (red << shift)) >> 24,
					                                   (sum_col[y][x][1] + (green << shift)) >> 24,
					                                   (sum_col[y][x][2] + (blue << shift)) >> 24);
					wc[y][x] =  want_bi(target[y][x] >> 16,         red,   sum_col[y][x][0], shift - 1) << 16;
					wc[y][x] |= want_bi((target[y][x] >> 8) & 0xFF, green, sum_col[y][x][1], shift - 1) << 8;
					wc[y][x] |= want_bi(target[y][x] & 0xFF,        blue,  sum_col[y][x][2], shift - 1);
				}
			}
		}
	}
}

struct CirclesMix {
  vi drawImage(int h, vi& pixels, int n);
};

vi CirclesMix::drawImage(int h, vi& pixels, int n) {
	start_time = get_time();
	N = n;
	H = h;
	W = pixels.size() / H;
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			target[i][j] = pixels[i * W + j];
		}
	}
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			pena[i][j+1] = pena[i][j] + penalty(target[i][j], ps[i][j]);
			want[i][j] = calc_want(target[i][j], ps[i][j]);
		}
	}

	auto ans = greedy();
	hill_climb(ans);
	// for (auto& v : ans) {
	// 	cerr << "(" << v.first.r << "," << v.first.c << ") " << v.first.radius << " : " << v.second << endl;
	// }
	vi ret;
	for (Circle c : ans) {
		ret.push_back(c.r);
		ret.push_back(c.c);
		ret.push_back(c.radius);
		ret.push_back(c.color);
	}
	PRINT_COUNTER();
	PRINT_TIMER();
	return ret;
}

#if defined(LOCAL)
int main() {
	CirclesMix cm;
	int H, S, N;
	scanf("%d %d", &H, &S);
	vi pixels(S);
	for (int i = 0; i < S; ++i) {
		scanf("%d", &pixels[i]);
	}
	scanf("%d", &N);

	vector<int> ret = cm.drawImage(H, pixels, N);
	printf("%d\n", (int)ret.size());
	for (int i = 0; i < (int)ret.size(); ++i) {
		printf("%d\n", ret[i]);
	}
	fflush(stdout);
}
#endif