import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Tester {
  private static final int maxN = 1000, minN = 20;
  private static final int border = 1000;
  private int N, H, W;
  private static int myN = -1;
  private int[][] givenImage;
  private int[] imgArg;

  private void generate(long seed) throws Exception {
    SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
    rnd.setSeed(seed);
    N = rnd.nextInt(maxN - minN + 1) + minN;
    if (seed == 1)
      N = minN;
    else if (seed == 2)
      N = maxN;
    if (myN != -1) N = myN;

    BufferedImage img = ImageIO.read(new File("img//" + seed + ".jpg"));
    H = img.getHeight();
    W = img.getWidth();
    givenImage = new int[H][W];
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c)
        givenImage[r][c] = img.getRGB(c, r) & 0xFFFFFF;

//    for (int i = 2; i <= 16; ++i) {
//      BufferedImage bi = new BufferedImage((W + i - 1) / i, (H + i - 1) / i, BufferedImage.TYPE_INT_RGB);
//      for (int r = 0; r < H; r += i) {
//        for (int c = 0; c < W; c += i) {
//          ArrayList<Integer> red = new ArrayList<>();
//          ArrayList<Integer> green = new ArrayList<>();
//          ArrayList<Integer> blue = new ArrayList<>();
//          for (int j = 0; j < i && r + j < H; j++) {
//            for (int k = 0; k < i && c + k < W; k++) {
//              red.add(givenImage[r + j][c + k] >> 16);
//              green.add((givenImage[r + j][c + k] >> 8) & 0xFF);
//              blue.add(givenImage[r + j][c + k] & 0xFF);
//            }
//          }
//          Collections.sort(red);
//          Collections.sort(green);
//          Collections.sort(blue);
//          int rc = red.get(red.size() / 2);
//          int gc = green.get(green.size() / 2);
//          int bc = blue.get(blue.size() / 2);
//          bi.setRGB(c / i, r / i, (rc << 16) | (gc << 8) | bc);
//        }
//      }
//      ImageIO.write(bi, "png", new File(i + ".png"));
//    }
//    System.exit(0);

    imgArg = new int[H * W];
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c)
        imgArg[r * W + c] = givenImage[r][c];
  }

  private int comp(int color, int index) {
    return (color >> (8 * index)) & 0xFF;
  }

  private int getDiff(int c1, int c2) {
    int d = 0;
    for (int i = 0; i < 3; ++i)
      d += Math.abs(comp(c1, i) - comp(c2, i));
    return d;
  }

  private int drawOver(int c1, int c2) {
    int c = 0;
    for (int i = 0; i < 3; ++i)
      c += ((comp(c1, i) + comp(c2, i)) / 2) << (8 * i);
    return c;
  }

  private int p2(int t) {
    return t * t;
  }

  private Result runTest(long seed) {
    Result res = new Result();
    res.seed = seed;
    try {
      Runtime rt = Runtime.getRuntime();
      proc = rt.exec(exec);
      Thread thread = new ErrorReader(proc.getErrorStream());
      thread.start();

      generate(seed);
      res.N = N;
      res.H = H;
      res.W = W;
      long startTime = System.currentTimeMillis();
      int[] raw_circles = drawImage(H, imgArg, N);
      res.elapsed = System.currentTimeMillis() - startTime;
      if (raw_circles.length % 4 != 0) {
        throw new RuntimeException("The number of elements in your return must be divisible by 4, and your return contained " + raw_circles.length + ".");
      }
      if (raw_circles.length > 4 * N) {
        throw new RuntimeException("Your return can have at most " + (4 * N) + " elements, and it contained " + raw_circles.length + ".");
      }

      int Nc = raw_circles.length / 4;
      int[] centerRow = new int[Nc];
      int[] centerCol = new int[Nc];
      int[] radius = new int[Nc];
      int[] color = new int[Nc];
      for (int i = 0; i < Nc; ++i) {
        centerRow[i] = raw_circles[4 * i];
        centerCol[i] = raw_circles[4 * i + 1];
        radius[i] = raw_circles[4 * i + 2];
        color[i] = raw_circles[4 * i + 3];
        if (centerRow[i] < -border || centerRow[i] > H - 1 + border) {
          throw new RuntimeException("Center of circle " + i + " must have center at row between " + (-border) + " and " + (H - 1 + border) + ", inclusive.");
        }
        if (centerCol[i] < -border || centerCol[i] > W - 1 + border) {
          throw new RuntimeException("Center of circle " + i + " must have center at column between " + (-border) + " and " + (W - 1 + border) + ", inclusive.");
        }
        if (radius[i] < 0 || radius[i] > border) {
          throw new RuntimeException("Radius of circle " + i + " must be between 0 and " + border + ".");
        }
        if (color[i] < 0 || color[i] > 0xFFFFFF) {
          throw new RuntimeException("Circle " + i + " can only have color between 0 and 0xFFFFFF.");
        }
      }

      // draw circles on the canvas, starting with black
      int[][] drawnImage = new int[H][W];
      for (int r = 0; r < H; ++r)
        Arrays.fill(drawnImage[r], 0);
      for (int i = 0; i < Nc; ++i) {
        // transparency: take 50% of old color and 50% of new color on each component
        for (int r = Math.max(centerRow[i] - radius[i], 0); r <= Math.min(centerRow[i] + radius[i], H - 1); r++) {
          int dc = (int) Math.sqrt(p2(radius[i]) - p2(centerRow[i] - r));
          for (int c = Math.max(centerCol[i] - dc, 0); c <= Math.min(centerCol[i] + dc, W - 1); c++)
            if (p2(centerRow[i] - r) + p2(centerCol[i] - c) <= p2(radius[i]))
              drawnImage[r][c] = drawOver(drawnImage[r][c], color[i]);
        }
      }

      if (vis) {
        BufferedImage bi = new BufferedImage(W, H, BufferedImage.TYPE_INT_RGB);
        for (int r = 0; r < H; ++r)
          for (int c = 0; c < W; ++c)
            bi.setRGB(c, r, drawnImage[r][c]);
        ImageIO.write(bi, "png", new File(seed + ".png"));
      }

      for (int r = 0; r < H; ++r)
        for (int c = 0; c < W; ++c)
          res.score += getDiff(givenImage[r][c], drawnImage[r][c]);
      thread.join(1000);
      return res;
    } catch (Exception e) {
      e.printStackTrace();
      return res;
    } finally {
      proc.destroy();
    }
  }

  private static String exec = "./tester";
  private static boolean vis;
  private Process proc;

  private int[] drawImage(int H, int[] pixels, int N) throws IOException {
    StringBuilder sb = new StringBuilder(pixels.length * 8);
    sb.append(H).append('\n');
    sb.append(pixels.length).append('\n');
    for (int i = 0; i < pixels.length; ++i)
      sb.append(pixels[i]).append('\n');
    sb.append(N).append('\n');
    OutputStream os = proc.getOutputStream();
    os.write(sb.toString().getBytes());
    os.flush();

    InputStream is = proc.getInputStream();
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    int nRet = Integer.parseInt(br.readLine());
    int[] ret = new int[nRet];
    for (int i = 0; i < nRet; ++i)
      ret[i] = Integer.parseInt(br.readLine());
    return ret;
  }

  private static final int THREAD_COUNT = 2;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-N")) myN = Integer.parseInt(args[++i]);
      if (args[i].equals("-exec")) exec = args[++i];
      if (args[i].equals("-vis")) vis = true;
    }
    if (begin != -1 && end != -1) {
      vis = false;
      ArrayList<Long> seeds = new ArrayList<>();
      for (long i = begin; i <= end; ++i) {
        seeds.add(i);
      }
      int len = seeds.size();
      Result[] results = new Result[len];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      int prev = 0;
      for (int i = 0; i < THREAD_COUNT; ++i) {
        int next = len * (i + 1) / THREAD_COUNT;
        threads[i] = new TestThread(prev, next - 1, seeds, results);
        prev = next;
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / results.length));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    int begin, end;
    ArrayList<Long> seeds;
    Result[] results;

    TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
      this.begin = begin;
      this.end = end;
      this.seeds = seeds;
      this.results = results;
    }

    public void run() {
      for (int i = begin; i <= end; ++i) {
        try {
          Tester f = new Tester();
          Result res = f.runTest(seeds.get(i));
          results[i] = res;
        } catch (Exception e) {
          e.printStackTrace();
          results[i] = new Result();
          results[i].seed = seeds.get(i);
        }
      }
    }
  }
}

class Result {
  long seed;
  int N, H, W;
  int score;
  long elapsed;

  public String toString() {
    String ret = String.format("seed:%4d\n", seed);
    ret += String.format("N:%3d H:%3d W:%3d\n", N, H, W);
    ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
    ret += String.format("score:%9d", score);
    return ret;
  }
}

class ErrorReader extends Thread {
  private InputStream error;

  ErrorReader(InputStream is) {
    error = is;
  }

  public void run() {
    try {
      byte[] ch = new byte[50000];
      int read;
      while ((read = error.read(ch)) > 0) {
        String s = new String(ch, 0, read);
        System.err.print(s);
        System.err.flush();
      }
    } catch (Exception e) {
      // do nothing
    }
  }
}
