import java.util.ArrayList;
import java.util.Arrays;

public final class PoisonedWine {
  private static boolean DEBUG = false;
  PoisonTest PoisonTest;
  private int N, S, R, P;
  private int strip;
  private double[][][] expect;
  private int[][][] bestSize;
  private int round;
  private Range unvisitedRange;
  private int safeCount;
  private ArrayList<Range> unknownRanges = new ArrayList<>();
  private ArrayList<Range> poisonRanges = new ArrayList<>();

  public int[] testWine(int numBottles, int testStrips, int testRounds, int numPoison) {
    this.N = numBottles;
    this.S = testStrips;
    this.R = testRounds;
    this.P = numPoison;
    this.strip = S;
    this.unvisitedRange = new Range(0, N);

    if (this.R == 1) {
      solveOneRoundCase();
    } else if (this.S > this.P) {
      solveEasyCase();
    } else {
      solveHardCase();
    }

    ArrayList<Integer> discard = new ArrayList<>();
    for (Range r : unknownRanges) {
      for (int i = r.s; i < r.e; i++) {
        discard.add(i);
      }
    }
    for (Range r : poisonRanges) {
      for (int i = r.s; i < r.e; i++) {
        discard.add(i);
      }
    }
    if (safeCount == 0) {
      for (int i = unvisitedRange.s + N / P / 2; i < unvisitedRange.e; i++) {
        discard.add(i);
      }
    } else {
      for (int i = unvisitedRange.s; i < unvisitedRange.e; i++) {
        discard.add(i);
      }
    }
    int[] ret = new int[discard.size()];
    for (int i = 0; i < discard.size(); i++) {
      ret[i] = discard.get(i);
    }
    if (DEBUG) System.err.println("safe:" + safeCount + " reserved:" + (N - discard.size()));
    return ret;
  }

  private void solveOneRoundCase() {
    if (finalRound()) return;
    int bestLen = 0;
    double bestExpect = 0;
    double[][][] dp = new double[2][P + 1][S + 1];
    for (int len = 1; len * S <= N && len < bestLen + 5; len++) {
      for (double[] a : dp[0]) {
        Arrays.fill(a, 0);
      }
      dp[0][P][0] = 1;
      int t = 1;
      for (int i = 0; i < S; i++, t = 1 - t) {
        for (double[] a : dp[t]) {
          Arrays.fill(a, 0);
        }
        int restLen = N - len * i;
        for (int j = 0; j <= P; j++) {
          for (int k = 0; k <= i; k++) {
            if (dp[1 - t][j][k] == 0) continue;
            if (restLen - len <= j) {
              if (j == 0) {
                dp[t][j][k + 1] += dp[1 - t][j][k];
              } else {
                dp[t][0][k] += dp[1 - t][j][k];
              }
              continue;
            }
            double prob = 1.0;
            for (int l = 0; l < j; l++) {
              prob *= restLen - len - l;
              prob /= restLen - l;
            }
//            System.err.println("len:" + len + " i:" + i + " j:" + j + " k:" + k);
            dp[t][j][k + 1] += dp[1 - t][j][k] * prob;
//            System.err.println("l:0 p:" + prob);
            for (int l = 1; l <= Math.min(len, j); l++) {
              // C(restLen-len, j-l) * C(len, l) / C(restLen, j)
              prob *= len - l + 1;
              prob /= l;
              prob *= j - l + 1;
              prob /= restLen - len - j + l;
//              System.err.println("l:" + l + " p:" + prob);
              dp[t][j - l][k] += dp[1 - t][j][k] * prob;
            }
          }
        }
      }
      double expect = 0;
      for (int i = 0; i <= P; i++) {
        for (int j = 0; j <= S; j++) {
          int gain = len * j;
          if (S - j == P) gain += N - len * S;
          expect += gain * gain * dp[1 - t][i][j];
        }
      }
      if (DEBUG) System.err.println("len:" + len + " exp:" + expect / (N - P) / (N - P));
      if (expect > bestExpect) {
        bestExpect = expect;
        bestLen = len;
      }
    }
    ArrayList<Range> ranges = new ArrayList<>();
    for (int i = 0; i < S; i++) {
      ranges.add(new Range(i * bestLen, (i + 1) * bestLen));
    }
    test(ranges);
  }

  private void solveEasyCase() {
    double[][][] scanningLength = scanningLengthEasyCase();
    int bestI = 0;
    int bestDiff = 1 << 30;
    final int embedLength = 20;
    for (int i = 0; i < embedLength; i++) {
      int len = P * 50 + (10000 - P * 50) * i / (embedLength - 1);
      if (Math.abs(N - len) < bestDiff) {
        bestI = i;
        bestDiff = Math.abs(N - len);
      }
    }
    int narrowingRound = embed[strip][R - 2][P - 1].charAt(bestI) - '0';
    if (DEBUG) System.err.println("narrowingRound:" + narrowingRound);
    for (round = 0; round < R && strip > 0; round++) {
      if (DEBUG) System.err.println("round " + round);
      if (round == R - 1 && finalRound()) return;
      ArrayList<Range> ranges = new ArrayList<>();
      if (unvisitedRange.size() <= P - (S - strip)) {
        unknownRanges.add(new Range(unvisitedRange.s, unvisitedRange.e));
        unvisitedRange.s = unvisitedRange.e;
      }
      if (poisonRanges.size() == P || unvisitedRange.size() == 0) {
        ArrayList<Range> candidate = new ArrayList<>();
        poisonRanges.forEach(r -> {
          if (r.s + 1 < r.e) candidate.add(r);
        });
        ranges.addAll(narrowing(strip - ranges.size(), candidate));
      } else {
        double len = scanningLength[R - round - 1 - narrowingRound][strip][unvisitedRange.size()];
        if (DEBUG) System.err.println("len:" + len);
        int start = unvisitedRange.s;
        for (int i = 0; i < strip; i++) {
          int s = i == 0 ? start : ranges.get(ranges.size() - 1).e;
          int e = start + (int) (len * (i + 1) + 1e-6);
          if (e > N) e = N;
          ranges.add(new Range(s, e));
          if (e == N) break;
        }
      }
      test(ranges);
    }
  }

  private double[][][] scanningLengthEasyCase() {
    double[][][] dp = new double[R][S + 1][N + 1];
    for (int i = 1; i <= S; i++) {
      for (int j = 0; j <= N; j++) {
        dp[0][i][j] = 1.0 * j / i;
      }
    }
    for (int r = 1; r < R; r++) {
      for (int s = S - P + 1; s <= S; s++) {
        int poison = P - (S - s);
        for (int n = poison + 1; n <= N; n++) {
          if (n <= s * r) {
            dp[r][s][n] = 1;
            continue;
          }
          double bestLen = Math.max(1, (int) dp[r][s][n - 1]);
          double minDiff = 1e300;
          for (int len = (int) bestLen; len * s <= n && len < bestLen + 10; len++) {
            double prob = 1.0;
            // C(n-len*s, poison-p) * C(len*s, p) / C(n, poison)
            for (int i = 0; i < poison; i++) {
              prob *= n - len * s - i;
              prob /= n - i;
            }
            double expect = dp[r - 1][s][n - len * s] * prob;
            for (int p = 1; p <= poison; p++) {
              prob *= len * s - p + 1;
              prob /= p;
              prob *= poison - p + 1;
              prob /= n - len * s - poison + p;
              expect += dp[r - 1][s - p][n - len * s] * prob;
            }
            if (Math.abs(len - expect) < minDiff) {
              minDiff = Math.abs(len - expect);
              bestLen = len;
            }
//            System.err.println("r:"+ r + " s:" + s + " n:" + n + " len:" + len + " expect:" + expect);
          }
          dp[r][s][n] = bestLen;
//          System.err.println("r:"+ r + " s:" + s + " n:" + n + " bestLen:" + bestLen + " minDiff:" + minDiff);
        }
      }
    }
    return dp;
  }

  private void solveHardCase() {
    precalc();
    for (round = 0; round < R && strip > 0; round++) {
      if (DEBUG) System.err.println("round " + round);
      if (round == R - 1 && finalRound()) return;
      ArrayList<Range> curRanges = singleRound();
      test(curRanges);
    }
  }

  private ArrayList<Range> singleRound() {
    ArrayList<Range> testRanges = new ArrayList<>();
    int singleLen = determintScanningLength();
    if (singleLen > 0) {
      int start = unvisitedRange.s;
      for (int i = 0; i < strip; i++) {
        if (start + (i + 1) * singleLen >= N) {
          testRanges.add(new Range(start + i * singleLen, N));
          break;
        }
        testRanges.add(new Range(start + i * singleLen, start + (i + 1) * singleLen));
      }
      unvisitedRange.s = testRanges.get(testRanges.size() - 1).e;
    }
    ArrayList<Range> candidate = new ArrayList<>();
    candidate.addAll(unknownRanges);
    poisonRanges.forEach(r -> {
      if (r.s + 1 < r.e) candidate.add(r);
    });
    testRanges.addAll(narrowing(strip - testRanges.size(), candidate));
    return testRanges;
  }

  private int determintScanningLength() {
    int len = unvisitedRange.e - unvisitedRange.s;
    if (len == 0 || poisonRanges.size() == P) {
      return 0;
    }
    int bestI = 0;
    double bestExpect = 0;
    int poison = P - poisonRanges.size();
    double initP = 1.0;
    for (int i = 0; i < poison; i++) {
      initP *= len - strip - i;
      initP /= len - i;
    }
    for (int i = 1; i * strip <= len; i++) {
      double p = initP;
      double sum = 0;
      for (int j = 0; j <= strip; j++) {
//        System.err.println("j:" + j + " expect:" + expect[R - round - 1][strip - j][len - i * strip] + " p:" + p );
        double gain = safeCount + i * (strip - j) + expect[R - round - 1][strip - j][len - i * strip];
        if (j == poison) gain += len - i * strip;
        sum += Math.pow(gain, 2) * p;
        p *= i * strip - j;
        p /= j + 1;
        p /= len - i * strip - poison + j + 1;
        p *= poison - j;
      }
//      if (DEBUG) System.err.println("i:" + i + " exp:" + (sum / (N - P) / (N - P)) + " initP:" + initP);
      if (sum > bestExpect) {
        bestExpect = sum;
        bestI = i;
      } else if (i > bestI + 10) { // early exit
        break;
      }
      for (int j = 0; j < strip; j++) {
        initP *= len - (i + 1) * strip - poison + 1 + j;
        initP /= len - i * strip - j;
      }
    }
    return bestI == 0 ? 1 : bestI;
  }

  private static ArrayList<Range> narrowing(int stripCount, ArrayList<Range> candidate) {
    if (candidate.isEmpty()) return new ArrayList<>();
    int[] partition = new int[candidate.size()];
    for (int i = 0; i < stripCount; ++i) {
      int maxI = 0;
      for (int j = 1; j < candidate.size(); j++) {
        if (candidate.get(maxI).size() * (partition[j] + 1) < candidate.get(j).size() * (partition[maxI] + 1)) {
          maxI = j;
        }
      }
      partition[maxI]++;
    }
    ArrayList<Range> ret = new ArrayList<>();
    for (int i = 0; i < candidate.size(); i++) {
      Range r = candidate.get(i);
      for (int j = 0; j < partition[i]; j++) {
        ret.add(new Range(r.s + r.size() * j / (partition[i] + 1), r.s + r.size() * (j + 1) / (partition[i] + 1)));
      }
    }
    ret.removeIf(r -> r.size() == 0);
    return ret;
  }

  private void test(ArrayList<Range> ranges) {
    String[] query = new String[ranges.size()];
    for (int i = 0; i < ranges.size(); i++) {
      Range r = ranges.get(i);
      assert (r.s < r.e);
      StringBuilder sb = new StringBuilder(String.valueOf(r.s));
      for (int j = r.s + 1; j < r.e; j++) {
        sb.append(",").append(j);
      }
      query[i] = sb.toString();
    }
    int[] res = PoisonTest.useTestStrips(query);
    for (int i = 0; i < ranges.size(); i++) {
      Range r = ranges.get(i);
      if (DEBUG) System.err.println("test:[" + r.s + "-" + r.e + "] -> " + res[i]);
      if (res[i] == 1) {
        --strip;
      } else {
        safeCount += r.size();
      }
      boolean found = false;
      for (Range p : unknownRanges) {
        if (p.contains(r)) {
          found = true;
          if (p.s == r.s) {
            p.s = r.e;
          } else {
            p.e = r.s;
          }
          if (res[i] == 1) {
            poisonRanges.add(r);
          }
          break;
        }
      }
      if (!found) {
        for (Range p : poisonRanges) {
          if (p.contains(r)) {
            found = true;
            if (res[i] == 1) {
              if (p.s == r.s) {
                unknownRanges.add(new Range(r.e, p.e));
                p.e = r.e;
              } else {
                unknownRanges.add(new Range(p.s, r.s));
                p.s = r.s;
              }
            } else {
              if (p.s == r.s) {
                p.s = r.e;
              } else {
                p.e = r.s;
              }
            }
            break;
          }
        }
      }
      if (!found) {
        if (res[i] == 1) {
          poisonRanges.add(r);
        }
      }
    }
    while (true) {
      boolean update = false;
      for (int i = 0; i < ranges.size(); i++) {
        if (ranges.get(i).s == unvisitedRange.s) {
          unvisitedRange.s = ranges.get(i).e;
          update = true;
        }
      }
      if (!update) break;
    }
    if (poisonRanges.size() == P) {
      unknownRanges.clear();
      unvisitedRange.s = N;
    }
    poisonRanges.removeIf(r -> r.s == r.e);
    unknownRanges.removeIf(r -> r.s == r.e);
  }

  private static final int[][] MASK_STRIP = new int[][]{
      {},
      {},
      {1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2},
  };

  private boolean finalRound() {
    if (poisonRanges.isEmpty()) {
      if (!unknownRanges.isEmpty()) return false;
      if (P >= MASK_STRIP.length) return false;
      if (P == 1) {
        SinglePointDetector spd = new SinglePointDetector();
        spd.add(unvisitedRange, strip);
        ArrayList<Integer> discard = spd.test();
        for (int d : discard) {
          poisonRanges.add(new Range(d, d + 1));
        }
        unvisitedRange.s = unvisitedRange.e;
        return true;
      } else {
        if (MASK_STRIP[P][strip] == 1) return false;
        int bits = MASK_STRIP[P][strip];
        if (bits == 4 && strip * (strip - 1) * (strip - 2) >= N * 5) bits = 3;
        if (bits == 3 && strip * (strip - 1) >= N * 3 / 2) bits = 2;
        MultiPointDetector mpd = new MultiPointDetector(unvisitedRange, P, strip, bits);
        ArrayList<Integer> discard = mpd.test();
        for (int d : discard) {
          poisonRanges.add(new Range(d, d + 1));
        }
        unvisitedRange.s = unvisitedRange.e;
        return true;
      }
    } else if (poisonRanges.size() < P) {
      return false;
    } else {
      ArrayList<Range> prs = new ArrayList<>(poisonRanges);
      prs.sort((r1, r2) -> Integer.compare(r2.size(), r1.size()));
      int[] len = new int[P];
      for (int i = 0; i < strip; i++) {
        len[i % P]++;
      }
      SinglePointDetector spd = new SinglePointDetector();
      for (int i = 0; i < P; i++) {
        spd.add(prs.get(i), len[i]);
      }
      ArrayList<Integer> discard = spd.test();
      poisonRanges.clear();
      for (int d : discard) {
        poisonRanges.add(new Range(d, d + 1));
      }
      unvisitedRange.s = unvisitedRange.e;
      return true;
    }
  }

  private void precalc() {
    expect = new double[R + 1][S + 1][N + 1]; // [# of rounds left][# of strips left][size of unexperimented area]
    bestSize = new int[R + 1][S + 1][N + 1];
    for (int i = 1; i < R; i++) {
      for (int j = 1; j <= S; j++) {
        int poison = P - (S - j);
        for (int k = 1; k <= N; k++) {
//        for (int k = Math.max(1, N - (R - i) * S * 200); k <= N; k++) {
          if (poison <= 0) {
            expect[i][j][k] = k;
            continue;
          }
          if (poison >= k) {
            continue;
          }
          double initP = 1.0;
          int initL = k == 1 ? 1 : Math.max(1, bestSize[i][j][k - 1] - 5);
          for (int m = 0; m < poison; m++) {
            // C(l*j, 0) * C(k - l*j, p - 0) / C(k, p)
            initP *= k - initL * j - m;
            initP /= k - m;
          }
          for (int l = initL; l * j <= k; l++) {
            double p = initP;
            double sum = 0;
            for (int m = 0; m < j; m++) {
              double gain = l * (j - m) + expect[i - 1][j - m][k - l * j];
              if (m == poison) {
                gain += k - l * j;
              }
              sum += gain * p;
              // C(l*j, m) * C(k - l*j, p - m) / C(k, p)
              p *= l * j - m;
              p /= m + 1;
              p /= k - l * j - poison + m + 1;
              p *= poison - m;
            }
            if (j == poison) sum += (k - l * j) * p;
            if (sum > expect[i][j][k]) {
              expect[i][j][k] = sum;
              bestSize[i][j][k] = l;
            } else if (l > bestSize[i][j][k] + 10) { // early exit
              break;
            }
            for (int m = 0; m < j; m++) {
              initP *= k - (l + 1) * j - poison + 1 + m;
              initP /= k - l * j - m;
            }
          }
        }
      }
    }
  }

  private class SinglePointDetector {
    ArrayList<Range> source = new ArrayList<>();
    ArrayList<Integer> strips = new ArrayList<>();

    void add(Range r, int s) {
      source.add(r);
      strips.add(s);
    }

    ArrayList<Integer> test() {
      ArrayList<ArrayList<Integer>> list = new ArrayList<>();
      ArrayList<ArrayList<Integer>> listNega = new ArrayList<>();
      for (int s : strips) {
        for (int i = 0; i < s; i++) {
          list.add(new ArrayList<>());
          listNega.add(new ArrayList<>());
        }
      }
      for (int i = 0, pos = 0; i < source.size(); i++) {
        Range r = source.get(i);
        assert (r.s < r.e);
        for (int j = r.s; j < r.e; j++) {
          int bits = (j - r.s) % (1 << strips.get(i));
          for (int k = 0; k < strips.get(i); k++) {
            if ((bits & (1 << k)) != 0) {
              list.get(pos + k).add(j);
            } else {
              listNega.get(pos + k).add(j);
            }
          }
        }
        pos += strips.get(i);
      }
      int[] res = testWrapper(list);

      ArrayList<Integer> ret = new ArrayList<>();
      for (int i = 0, pos = 0; i < source.size(); i++) {
        Range r = source.get(i);
        boolean[] safe = new boolean[r.size()];
        boolean[] danger = new boolean[r.size()];
        boolean any = false;
        for (int j = 0; j < strips.get(i); j++) {
          if (res[pos + j] == 1) {
            any = true;
            for (int k : list.get(pos + j)) {
              danger[k - r.s] = true;
            }
            for (int k : listNega.get(pos + j)) {
              safe[k - r.s] = true;
            }
          } else {
            for (int k : list.get(pos + j)) {
              safe[k - r.s] = true;
            }
            for (int k : listNega.get(pos + j)) {
              danger[k - r.s] = true;
            }
          }
        }
        if (any) {
          for (int j = r.s; j < r.e; j++) {
            if (!danger[j - r.s]) safe[j - r.s] = true;
          }
        }
        for (int j = r.s; j < r.e; j++) {
          if (!safe[j - r.s]) ret.add(j);
        }
        pos += strips.get(i);
      }
      return ret;
    }
  }

  private class MultiPointDetector {
    Range range;
    int poison, strip, bits;

    MultiPointDetector(Range r, int p, int s, int bits) {
      this.range = r;
      this.poison = p;
      this.strip = s;
      this.bits = bits;
    }

    ArrayList<Integer> test() {
      ArrayList<ArrayList<Integer>> list = new ArrayList<>();
      for (int i = 0; i < strip; i++) {
        list.add(new ArrayList<>());
      }
      ArrayList<Integer> masks = new ArrayList<>();
      for (int i = 0; i < (1 << strip); i++) {
        if (Integer.bitCount(i) == bits) {
          masks.add(i);
        }
      }
      for (int i = range.s; i < range.e; i++) {
        int mask = masks.get((i - range.s) % masks.size());
        for (int j = 0; j < strip; j++) {
          if ((mask & (1 << j)) != 0) list.get(j).add(i);
        }
      }
      int[] res = testWrapper(list);
      boolean[] safe = new boolean[range.size()];
      for (int i = 0; i < res.length; i++) {
        if (res[i] == 1) continue;
        for (int pos : list.get(i)) safe[pos - range.s] = true;
      }
      ArrayList<Integer> ret = new ArrayList<>();
      for (int i = 0; i < range.size(); i++) {
        if (!safe[i]) ret.add(i + range.s);
      }
      return ret;
    }
  }

  private int[] testWrapper(ArrayList<ArrayList<Integer>> list) {
    String[] query = new String[list.size()];
    for (int i = 0; i < list.size(); i++) {
      if (list.get(i).isEmpty()) {
        query[i] = "";
        continue;
      }
      StringBuilder sb = new StringBuilder(String.valueOf(list.get(i).get(0)));
      for (int j = 1; j < list.get(i).size(); j++) {
        sb.append(",").append(list.get(i).get(j));
      }
      query[i] = sb.toString();
    }

    int nonnull = 0;
    for (String s : query) {
      if (!s.isEmpty()) ++nonnull;
    }
    String[] realQuery = new String[nonnull];
    for (int i = 0, pos = 0; i < query.length; i++) {
      if (query[i].isEmpty()) continue;
      realQuery[pos++] = query[i];
    }
    int[] ret = PoisonTest.useTestStrips(realQuery);
    int[] realRet = new int[query.length];
    for (int i = 0, pos = 0; i < query.length; i++) {
      if (query[i].isEmpty()) continue;
      realRet[i] = ret[pos++];
    }
    return realRet;
  }

  private static final class Range {
    int s, e;

    Range(int s, int e) {
      this.s = s;
      this.e = e;
    }

    boolean contains(Range r) {
      return this.s <= r.s && r.e <= this.e;
    }

    int size() {
      return this.e - this.s;
    }

    @Override
    public String toString() {
      return "Range(" + s + "," + e + ')';
    }
  }

  private String[][][] embed = new String[21][][];

  {
    embed[5] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "01111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "02222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "02333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "02344434444444444444",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "02344444444444444444",
            "32222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "02334544444544444444",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "02334455454455545545",
            "22222222222222222222",
            "21111111111111111111",
            "00000000000000000000",
        },
        {
            "02233445555555555455",
            "23232332222222222222",
            "22221212112121111112",
            "00000000000000000000",
        },
    };
  }

  {
    embed[6] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "02222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "02232222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "01223333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "01223333344444444444",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "01222333334444444455",
            "23333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "01222233333344444444",
            "23333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "01222233333333444444",
            "13333333333333333333",
            "32222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "01122223333333344444",
            "13333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
    };
  }

  {
    embed[7] = new String[][]{
        {
            "01111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "01222222222222222222",
            "12222122222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "01122223333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "01122222233333333334",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00112222222333333333",
            "13333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "00112222222223333333",
            "13333333333333333333",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "00111222222222333333",
            "13333333333333333333",
            "22222222222222222222",
            "12222222222212222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "00111222222222233333",
            "13444444444444444443",
            "23222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "00111222222222222333",
            "13444444444444444444",
            "23332222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
        },
    };
  }

  {
    embed[8] = new String[][]{
        {
            "01111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00112222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00111122222222222333",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00111112222222222222",
            "13333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00011111222222222222",
            "13333333333333333333",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00011111122222222222",
            "13334444444443444444",
            "23222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "10000000000000000000",
            "00000000000000000000",
        },
        {
            "00011111112222222222",
            "12444444444444444444",
            "23333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "00011111111222222222",
            "12344444444444444444",
            "23333333333333333333",
            "32222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "00011111111122222222",
            "12344444444444444444",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222212212",
            "11111111111111111111",
            "00000000000000000000",
        },
    };
  }

  {
    embed[9] = new String[][]{
        {
            "00111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00011111122222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00001111111112222222",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00001111111111112222",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000111111111111122",
            "12333333333333333333",
            "23333333233333322232",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000111111111111111",
            "12344444444444444444",
            "23333333333333333333",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000011111111111111",
            "12334444444444444444",
            "23333333333333333333",
            "32222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000011111111111111",
            "12334444444444444444",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
        {
            "00000011111111111111",
            "12234444444555555555",
            "13333333333333333333",
            "23222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
    };
  }

  {
    embed[10] = new String[][]{
        {
            "00001111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000011111111111111",
            "12222222222222222222",
            "11222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000001111111111111",
            "12232222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000111111111111",
            "12233333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000011111111111",
            "12233444444444444444",
            "13333333333333333333",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000001111111111",
            "11223334444444444444",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000111111111",
            "11223333444444444444",
            "13333333333333333333",
            "22333233222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000011111111",
            "11222333344444555544",
            "13334444333343433343",
            "23333333333333333333",
            "32222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000011111111",
            "11222333334444445555",
            "13444444444444444444",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
        },
    };
  }

  {
    embed[11] = new String[][]{
        {
            "00000000111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000111111111",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000111111",
            "12223333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000001111",
            "12233333333333333333",
            "12333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000011",
            "11223333333344444444",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222333334444444444",
            "13333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22221112111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222233333344444444",
            "13344444444444444444",
            "23333333333333333333",
            "32222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222233333334444444",
            "13344444444444444444",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01222223333333344444",
            "12344444444444444444",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[12] = new String[][]{
        {
            "00000000000000001111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "12222333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222223333333333333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222233333333334",
            "12333333333333333333",
            "13323323222222322222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222223333333333",
            "12333444333333344333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222233333333",
            "12334444444444444444",
            "13333333333333333333",
            "23222222222222222222",
            "22222222222222222222",
            "22222111111211111112",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222223333333",
            "12334444444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222333333",
            "12234444444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "32222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[13] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "12222223223333333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222233333333333",
            "12333333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222333333333",
            "12333333333333333333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222223333333",
            "12333334444444444444",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22112211221121112211",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222233333",
            "12233444444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222222333",
            "11233344444444444444",
            "13343333333333333333",
            "23333333333333333333",
            "32222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222222223",
            "11223344444444444444",
            "12334444444444444444",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[14] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222223",
            "12233333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12233333333333333333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11233333344444444444",
            "13333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222222222",
            "11223333334444444444",
            "12343333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11100101011101110101",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222222222",
            "11223333334444444444",
            "12334444444444444444",
            "13333333333333333333",
            "22333222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222222322",
            "12222333333444444444",
            "12334444444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "32222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[15] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "12222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12223333333333333333",
            "12222223233232223333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "12222333333333333333",
            "12333333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11222233333344444444",
            "12333333333333333333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22211211111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222222222",
            "11222223333334444444",
            "12334344443444444444",
            "13333333333333333333",
            "23322222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222223333333344444",
            "12333444444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "32222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "01011111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222223222222",
            "11222222333333333444",
            "12333444444444444444",
            "13344333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "12221111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[16] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12222333333333333333",
            "12233333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11222233333333333333",
            "12333333333333333333",
            "12333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01122222222222222222",
            "11222223333333333333",
            "12233333333333333333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01113222222222222222",
            "11222222333333333334",
            "12233334444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222222233333333333",
            "11233344444444444444",
            "12333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222212",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222222222222",
            "11122222223333333333",
            "11223334444444444444",
            "12344444444444443434",
            "13333333333333333333",
            "23333333233333333332",
            "32222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "01111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[17] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00010000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "11222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11222222233333333333",
            "12233333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "10000111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11222222233333333333",
            "12233333333333333333",
            "12333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222222233333333333",
            "12233333333333333333",
            "13333333333333333333",
            "23333322333222232323",
            "22222222222222222222",
            "22222222222222222222",
            "22211111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00111001111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222222223333333333",
            "11223333344444444444",
            "12333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22121211111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "12122222222333333333",
            "11223333334444444444",
            "12334343443443343444",
            "13333333333333333333",
            "23333323333332333333",
            "32222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222232223222",
            "01122222222233333333",
            "11222333444444444444",
            "12334444444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "23222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "00001100000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[18] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "11222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11222222222333333333",
            "12223333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222222222222333333",
            "12223333333333333333",
            "12333333333333333333",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222222222222223333",
            "11223333333333333333",
            "12333333333333333333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222222222222",
            "11122222222222222223",
            "11223333333344444444",
            "12333333333333333333",
            "13333333333333333333",
            "23333332232222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111232222222222222",
            "11122322222222222222",
            "11222333333334444444",
            "12333344444444444444",
            "13333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222222322222",
            "01122222222222222222",
            "11222333333333444444",
            "12233444444444444444",
            "12333333333333333333",
            "23333333333333333333",
            "23332222222222222222",
            "32222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21221111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[19] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "10010101111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11222222222222333333",
            "12222333333333333333",
            "12222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222222222222222333",
            "12222233333333333333",
            "12333333333333333333",
            "12232332222222232222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "01111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222222222222",
            "11122222222222222222",
            "11222233333333333333",
            "12233333333333333333",
            "13333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222232223222",
            "11122222222222222222",
            "11222223333333334444",
            "12233333333333333333",
            "12333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222211111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "10011111011001000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222222222222",
            "01122222222222222222",
            "11222233333333333344",
            "12223344444444444444",
            "12333333333333333333",
            "23333333333333333333",
            "23322222222222222222",
            "32222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222121121111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111123222222222222",
            "01112222222222222222",
            "11222223333333333334",
            "12223334444444444444",
            "12333434433433333333",
            "13333333333333333333",
            "23333333333333333333",
            "23222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

  {
    embed[20] = new String[][]{
        {
            "00000000000000000000",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11222222222222222222",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11122222222222222222",
            "11222222222222222222",
            "12222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11011111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "11112222222222222222",
            "11222222222222222222",
            "12222233333333333333",
            "12222222222222222222",
            "12222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01112222222222222222",
            "11222222222222222222",
            "11222223333333333333",
            "12233333333333333333",
            "12333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222222222222",
            "11122222222222222222",
            "11222222333333333333",
            "12233333333333333333",
            "13333333333333333333",
            "23332222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111222222222222222",
            "11122222222222222222",
            "11222222223333333333",
            "11223333333444344444",
            "12333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111122222222222222",
            "01112222222222222222",
            "11222222222333333333",
            "11223333344444444444",
            "12333433333333333333",
            "13333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "21111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "10111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
        {
            "00000000000000000000",
            "01111113222222222222",
            "01112222222222222222",
            "11122222222233333333",
            "11222333444444444444",
            "12333444444444444444",
            "12333333333333333333",
            "23333333333333333333",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "22222222222222222222",
            "11111111111111111111",
            "11111111111111111111",
            "11111111111111111111",
            "00000000000000000000",
            "00000000000000000000",
            "00000000000000000000",
        },
    };
  }

}