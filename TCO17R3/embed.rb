res = Array.new(21) do |s|
	Array.new(11) do |r|
		Array.new(s + 1) do
			[]
		end
	end
end

IO.foreach(ARGV[0]) do |line|
	if line =~ /s:(\d+) +r:(\d+) +p:(\d+) +n:(\d+) +best:(\d+)/
		s = $1.to_i
		r = $2.to_i
		p = $3.to_i
		n = $4.to_i
		best = $5.to_i
		res[s][r][p] << best
	end
end

puts 'private String[][][] embed = new String[21][][];'
5.upto(20).each do |s|
	as = res[s]
	puts '{'
	puts "embed[#{s}] = new String[][]{"
	as[2..10].each do |rs|
		puts '{'
		rs[1...s].each do |ps|
			puts "\"#{ps.join()}\","
		end
		puts '},'
	end
	puts '};'
	puts '}'
end
