import java.util.ArrayList;
import java.util.SplittableRandom;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public final class Simulation {
  private static boolean DEBUG = false;
  private SplittableRandom rnd = new SplittableRandom();
  final private int N, S, R, P;
  private boolean[] wine;
  private int[] winePos;
  private double[][][] scanningLength;
  private int strip;
  private int round;
  private Range unvisitedRange;
  private ArrayList<Range> unknownRanges = new ArrayList<>();
  private ArrayList<Range> poisonRanges = new ArrayList<>();

  public static void main(String[] args) throws Exception {
    final int ndiv = 20;
    int[][][][] best = new int[21][11][21][ndiv];
    ForkJoinPool pool = new ForkJoinPool(8);
    ArrayList<ForkJoinTask<?>> list = new ArrayList<>();
    for (int s = 5; s <= 20; s++) {
      for (int r = 2; r <= 10; r++) {
        for (int p = 1; p < s; p++) {
          int curS = s;
          int curR = r;
          int curP = p;
          list.add(pool.submit(() -> {
            int minN = curP * 50;
            int maxN = 10000;
            for (int i = 0; i < ndiv; i++) {
              int n = minN + (maxN - minN) * i / (ndiv - 1);
              Simulation sim = new Simulation(n, curS, curR, curP);
              long[] sum = new long[curR];
              final int trial = 100000;
              for (int j = 0; j < trial; j++) {
                sim.setup();
                for (int narrow = 0; narrow < curR; narrow++) {
                  int score = sim.testWine(narrow);
                  sum[narrow] += score * score;
                }
              }
              int maxNarrow = 0;
              for (int j = 1; j < curR; j++) {
                if (sum[j] > sum[maxNarrow]) maxNarrow = j;
              }
              best[curS][curR][curP][i] = maxNarrow;
//            System.out.println("s:" + curS + " r:" + curR + " p:" + curP + " n:" + n);
//            for (int j = 0; j < curR; j++) {
//              System.out.printf("narrow:%d %.6f\n", j, 1.0 * sum[j] / trial / (n - curP) / (n - curP));
//            }
            }
          }));
        }
      }
    }
    for (ForkJoinTask<?> task : list) {
      task.get();
    }
    for (int s = 5; s <= 20; s++) {
      for (int r = 2; r <= 10; r++) {
        for (int p = 1; p < s; p++) {
          int minN = p * 50;
          int maxN = 10000;
          for (int i = 0; i < ndiv; i++) {
            int n = minN + (maxN - minN) * i / (ndiv - 1);
            System.out.println("s:" + s + " r:" + r + " p:" + p + " n:" + n + " best:" + best[s][r][p][i]);
          }
        }
      }
    }
  }

  Simulation(int N, int S, int R, int P) {
    this.N = N;
    this.S = S;
    this.R = R;
    this.P = P;
    this.scanningLength = scanningLengthEasyCase();
    this.wine = new boolean[N];
    this.winePos = new int[N];
    for (int i = 0; i < N; i++) {
      winePos[i] = i;
    }
  }

  private void setup() {
    for (int i = 0; i < P; i++) {
      wine[winePos[i]] = false;
    }
    for (int i = 0; i < P; i++) {
      int p = rnd.nextInt(N - i) + i;
      int tmp = winePos[i];
      winePos[i] = winePos[p];
      winePos[p] = tmp;
    }
    for (int i = 0; i < P; i++) {
      wine[winePos[i]] = true;
    }
  }

  private int testWine(int narrowingRound) {
    this.strip = S;
    this.unvisitedRange = new Range(0, N);
    this.unknownRanges.clear();
    this.poisonRanges.clear();

    solveEasyCase(narrowingRound);

    int ret = 0;
    for (Range r : unknownRanges) {
      ret += r.size();
    }
    for (Range r : poisonRanges) {
      ret += r.size();
    }
    ret += unvisitedRange.size();
    return N - ret;
  }

  private void solveEasyCase(int narrowingRound) {
    for (round = 0; round < R && strip > 0; round++) {
      if (DEBUG) System.err.println("round " + round);
      if (round == R - 1 && finalRound()) return;
      ArrayList<Range> ranges = new ArrayList<>();
      if (unvisitedRange.size() <= P - (S - strip)) {
        unknownRanges.add(new Range(unvisitedRange.s, unvisitedRange.e));
        unvisitedRange.s = unvisitedRange.e;
      }
      if (poisonRanges.size() == P || unvisitedRange.size() == 0) {
        ArrayList<Range> candidate = new ArrayList<>();
        poisonRanges.forEach(r -> {
          if (r.s + 1 < r.e) candidate.add(r);
        });
        ranges.addAll(narrowing(strip - ranges.size(), candidate));
      } else {
        if (R - round - 1 - narrowingRound < 0) {
          System.err.println(round + " " + poisonRanges + " " + unvisitedRange + " " + narrowingRound + " " + winePos[0] + " " + winePos[1]);
        }
        double len = scanningLength[R - round - 1 - narrowingRound][strip][unvisitedRange.size()];
        if (DEBUG) System.err.println("len:" + len);
        int start = unvisitedRange.s;
        for (int i = 0; i < strip; i++) {
          int s = i == 0 ? start : ranges.get(ranges.size() - 1).e;
          int e = start + (int) (len * (i + 1) + 1e-6);
          if (e > N) e = N;
          ranges.add(new Range(s, e));
          if (e == N) break;
        }
      }
      test(ranges);
    }
  }

  private double[][][] scanningLengthEasyCase() {
    double[][][] dp = new double[R][S + 1][N + 1];
    for (int i = 1; i <= S; i++) {
      for (int j = 0; j <= N; j++) {
        dp[0][i][j] = 1.0 * j / i;
      }
    }
    for (int r = 1; r < R; r++) {
      for (int s = S - P + 1; s <= S; s++) {
        int poison = P - (S - s);
        for (int n = poison + 1; n <= N; n++) {
          if (n <= s * r) {
            dp[r][s][n] = 1;
            continue;
          }
          double bestLen = Math.max(1, (int) dp[r][s][n - 1]);
          double minDiff = 1e300;
          for (int len = (int) bestLen; len * s <= n && len < bestLen + 10; len++) {
            double prob = 1.0;
            // C(n-len*s, poison-p) * C(len*s, p) / C(n, poison)
            for (int i = 0; i < poison; i++) {
              prob *= n - len * s - i;
              prob /= n - i;
            }
            double expect = dp[r - 1][s][n - len * s] * prob;
            for (int p = 1; p <= poison; p++) {
              prob *= len * s - p + 1;
              prob /= p;
              prob *= poison - p + 1;
              prob /= n - len * s - poison + p;
              expect += dp[r - 1][s - p][n - len * s] * prob;
            }
            if (Math.abs(len - expect) < minDiff) {
              minDiff = Math.abs(len - expect);
              bestLen = len;
            }
//            System.err.println("r:"+ r + " s:" + s + " n:" + n + " len:" + len + " expect:" + expect);
          }
          dp[r][s][n] = bestLen;
//          System.err.println("r:"+ r + " s:" + s + " n:" + n + " bestLen:" + bestLen + " minDiff:" + minDiff);
        }
      }
    }
    return dp;
  }

  private static ArrayList<Range> narrowing(int stripCount, ArrayList<Range> candidate) {
    if (candidate.isEmpty()) return new ArrayList<>();
    int[] partition = new int[candidate.size()];
    for (int i = 0; i < stripCount; ++i) {
      int maxI = 0;
      for (int j = 1; j < candidate.size(); j++) {
        if (candidate.get(maxI).size() * (partition[j] + 1) < candidate.get(j).size() * (partition[maxI] + 1)) {
          maxI = j;
        }
      }
      partition[maxI]++;
    }
    ArrayList<Range> ret = new ArrayList<>();
    for (int i = 0; i < candidate.size(); i++) {
      Range r = candidate.get(i);
      for (int j = 0; j < partition[i]; j++) {
        ret.add(new Range(r.s + r.size() * j / (partition[i] + 1), r.s + r.size() * (j + 1) / (partition[i] + 1)));
      }
    }
    ret.removeIf(r -> r.size() == 0);
    return ret;
  }

  private void test(ArrayList<Range> ranges) {
    int[] res = new int[ranges.size()];
    for (int i = 0; i < ranges.size(); i++) {
      for (int j = ranges.get(i).s; j < ranges.get(i).e; j++) {
        if (wine[j]) {
          res[i] = 1;
          break;
        }
      }
    }
    for (int i = 0; i < ranges.size(); i++) {
      Range r = ranges.get(i);
      if (DEBUG) System.err.println("test:[" + r.s + "-" + r.e + "] -> " + res[i]);
      if (res[i] == 1) {
        --strip;
      }
      boolean found = false;
      for (Range p : unknownRanges) {
        if (p.contains(r)) {
          found = true;
          if (p.s == r.s) {
            p.s = r.e;
          } else {
            p.e = r.s;
          }
          if (res[i] == 1) {
            poisonRanges.add(r);
          }
          break;
        }
      }
      if (!found) {
        for (Range p : poisonRanges) {
          if (p.contains(r)) {
            found = true;
            if (res[i] == 1) {
              if (p.s == r.s) {
                unknownRanges.add(new Range(r.e, p.e));
                p.e = r.e;
              } else {
                unknownRanges.add(new Range(p.s, r.s));
                p.s = r.s;
              }
            } else {
              if (p.s == r.s) {
                p.s = r.e;
              } else {
                p.e = r.s;
              }
            }
            break;
          }
        }
      }
      if (!found) {
        if (res[i] == 1) {
          poisonRanges.add(r);
        }
      }
    }
    while (true) {
      boolean update = false;
      for (int i = 0; i < ranges.size(); i++) {
        if (ranges.get(i).s == unvisitedRange.s) {
          unvisitedRange.s = ranges.get(i).e;
          update = true;
        }
      }
      if (!update) break;
    }
    if (poisonRanges.size() == P) {
      unknownRanges.clear();
      unvisitedRange.s = N;
    }
    poisonRanges.removeIf(r -> r.s == r.e);
    unknownRanges.removeIf(r -> r.s == r.e);
  }

  private static final int[][] MASK_STRIP = new int[][]{
      {},
      {},
      {1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2},
  };

  private boolean finalRound() {
    if (poisonRanges.isEmpty()) {
      if (!unknownRanges.isEmpty()) return false;
      if (P >= MASK_STRIP.length) return false;
      if (P == 1) {
        SinglePointDetector spd = new SinglePointDetector();
        spd.add(unvisitedRange, strip);
        ArrayList<Integer> discard = spd.test();
        for (int d : discard) {
          poisonRanges.add(new Range(d, d + 1));
        }
        unvisitedRange.s = unvisitedRange.e;
        return true;
      } else {
        if (MASK_STRIP[P][strip] == 1) return false;
        MultiPointDetector mpd = new MultiPointDetector(unvisitedRange, P, strip, MASK_STRIP[P][strip]);
        ArrayList<Integer> discard = mpd.test();
        for (int d : discard) {
          poisonRanges.add(new Range(d, d + 1));
        }
        unvisitedRange.s = unvisitedRange.e;
        return true;
      }
    } else if (poisonRanges.size() < P) {
      return false;
    } else {
      ArrayList<Range> prs = new ArrayList<>(poisonRanges);
      prs.sort((r1, r2) -> Integer.compare(r2.size(), r1.size()));
      int[] len = new int[P];
      for (int i = 0; i < strip; i++) {
        len[i % P]++;
      }
      SinglePointDetector spd = new SinglePointDetector();
      for (int i = 0; i < P; i++) {
        spd.add(prs.get(i), len[i]);
      }
      ArrayList<Integer> discard = spd.test();
      poisonRanges.clear();
      for (int d : discard) {
        poisonRanges.add(new Range(d, d + 1));
      }
      unvisitedRange.s = unvisitedRange.e;
      return true;
    }
  }

  private class SinglePointDetector {
    ArrayList<Range> source = new ArrayList<>();
    ArrayList<Integer> strips = new ArrayList<>();

    void add(Range r, int s) {
      source.add(r);
      strips.add(s);
    }

    ArrayList<Integer> test() {
      ArrayList<ArrayList<Integer>> list = new ArrayList<>();
      ArrayList<ArrayList<Integer>> listNega = new ArrayList<>();
      for (int s : strips) {
        for (int i = 0; i < s; i++) {
          list.add(new ArrayList<>());
          listNega.add(new ArrayList<>());
        }
      }
      for (int i = 0, pos = 0; i < source.size(); i++) {
        Range r = source.get(i);
        assert (r.s < r.e);
        for (int j = r.s; j < r.e; j++) {
          int bits = (j - r.s) % (1 << strips.get(i));
          for (int k = 0; k < strips.get(i); k++) {
            if ((bits & (1 << k)) != 0) {
              list.get(pos + k).add(j);
            } else {
              listNega.get(pos + k).add(j);
            }
          }
        }
        pos += strips.get(i);
      }
      int[] res = new int[list.size()];
      for (int i = 0; i < list.size(); i++) {
        for (int j = 0; j < list.get(i).size(); j++) {
          if (wine[list.get(i).get(j)]) {
            res[i] = 1;
            break;
          }
        }
      }

      ArrayList<Integer> ret = new ArrayList<>();
      for (int i = 0, pos = 0; i < source.size(); i++) {
        Range r = source.get(i);
        boolean[] safe = new boolean[r.size()];
        boolean[] danger = new boolean[r.size()];
        boolean any = false;
        for (int j = 0; j < strips.get(i); j++) {
          if (res[pos + j] == 1) {
            any = true;
            for (int k : list.get(pos + j)) {
              danger[k - r.s] = true;
            }
            for (int k : listNega.get(pos + j)) {
              safe[k - r.s] = true;
            }
          } else {
            for (int k : list.get(pos + j)) {
              safe[k - r.s] = true;
            }
            for (int k : listNega.get(pos + j)) {
              danger[k - r.s] = true;
            }
          }
        }
        if (any) {
          for (int j = r.s; j < r.e; j++) {
            if (!danger[j - r.s]) safe[j - r.s] = true;
          }
        }
        for (int j = r.s; j < r.e; j++) {
          if (!safe[j - r.s]) ret.add(j);
        }
        pos += strips.get(i);
      }
      return ret;
    }
  }

  private class MultiPointDetector {
    Range range;
    int poison, strip, bits;

    MultiPointDetector(Range r, int p, int s, int bits) {
      this.range = r;
      this.poison = p;
      this.strip = s;
      this.bits = bits;
    }

    ArrayList<Integer> test() {
      ArrayList<ArrayList<Integer>> list = new ArrayList<>();
      for (int i = 0; i < strip; i++) {
        list.add(new ArrayList<>());
      }
      ArrayList<Integer> masks = new ArrayList<>();
      for (int i = 0; i < (1 << strip); i++) {
        if (Integer.bitCount(i) == bits) {
          masks.add(i);
        }
      }
      for (int i = range.s; i < range.e; i++) {
        int mask = masks.get((i - range.s) % masks.size());
        for (int j = 0; j < strip; j++) {
          if ((mask & (1 << j)) != 0) list.get(j).add(i);
        }
      }
      int[] res = new int[list.size()];
      for (int i = 0; i < list.size(); i++) {
        for (int j = 0; j < list.get(i).size(); j++) {
          if (wine[list.get(i).get(j)]) {
            res[i] = 1;
            break;
          }
        }
      }
      boolean[] safe = new boolean[range.size()];
      for (int i = 0; i < res.length; i++) {
        if (res[i] == 1) continue;
        for (int pos : list.get(i)) safe[pos - range.s] = true;
      }
      ArrayList<Integer> ret = new ArrayList<>();
      for (int i = 0; i < range.size(); i++) {
        if (!safe[i]) ret.add(i + range.s);
      }
      return ret;
    }
  }

  private static final class Range {
    int s, e;

    Range(int s, int e) {
      this.s = s;
      this.e = e;
    }

    boolean contains(Range r) {
      return this.s <= r.s && r.e <= this.e;
    }

    int size() {
      return this.e - this.s;
    }

    @Override
    public String toString() {
      return "Range(" + s + "," + e + ')';
    }
  }
}