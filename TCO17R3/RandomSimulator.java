public class RandomSimulator {
  private static final int[][] MASK_STRIP = new int[][]{
      {},
      {},
      {1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2},
  };


  public static void main(String[] args) {
    for (int p = 2; p <= 100; p++) {
      for (int s = 5; s <= 20; s++) {
        double orig;
        if (p >= 5 || MASK_STRIP[p][s] == 1) {
          orig = 1.0 * p / (s + 1);
        } else {
          orig = 1.0;
          for (int i = 0; i < MASK_STRIP[p][s]; i++) {
            orig *= p * MASK_STRIP[p][s] - i;
            orig /= s - i;
          }
        }
        double lo = 0.01;
        double hi = 0.99;
        for (int i = 0; i < 100; i++) {
          double m1 = (lo * 2 + hi) / 3;
          double m2 = (lo + hi * 2) / 3;
          if (calc(p, s, m1) < calc(p, s, m2)) {
            hi = m2;
          } else {
            lo = m1;
          }
        }
        System.out.printf("p:%d s:%2d orig:%.6f rand:%.6f\n", p, s, orig, calc(p, s, lo));
      }
    }
  }

  static double calc(int p, int s, double prob) {
    double rest = s * Math.pow(1 - prob, p);
    return Math.pow(1 - prob, rest);
  }
}
