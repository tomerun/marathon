import java.security.SecureRandom;
import java.util.ArrayList;

public class Tester {

  int numBottles;
  int testStrips;
  int testRounds;
  int numPoison;
  static int myN, myS, myR, myP;
  boolean[] bottles;

  private void generateTestCase(long seed) throws Exception {
    SecureRandom r = SecureRandom.getInstance("SHA1PRNG");
    r.setSeed(seed);
    numBottles = r.nextInt(9951) + 50;
    if (10000 <= seed && seed < 20000) {
      numBottles = 50 + (int) ((seed - 10000) / 50) * 50;
    }
    if (myN != 0) numBottles = myN;
    bottles = new boolean[numBottles];
    testStrips = r.nextInt(16) + 5;
    testRounds = r.nextInt(10) + 1;
    numPoison = r.nextInt(numBottles / 50) + 1;
    if (10000 <= seed && seed < 20000) {
      int i = (int) (seed - 10000) % 50;
      numPoison = 1 + numBottles / 50 * i / 50;
    }
    if (myS != 0) testStrips = myS;
    if (myR != 0) testRounds = myR;
    if (myP != 0) numPoison = myP;
    int remain = numPoison;
    while (remain > 0) {
      int x = r.nextInt(numBottles);
      if (bottles[x]) continue;
      bottles[x] = true;
      remain--;
    }
  }

  private Result runTest(long seed) throws Exception {
    Result res = new Result();
    res.seed = seed;
    generateTestCase(seed);
    res.N = this.numBottles;
    res.S = this.testStrips;
    res.R = this.testRounds;
    res.P = this.numPoison;
    PoisonedWine obj = new PoisonedWine();
    obj.PoisonTest = new PoisonTest(this);
    long startTime = System.currentTimeMillis();
    int[] ret = obj.testWine(numBottles, testStrips, testRounds, numPoison);
    res.elapsed = System.currentTimeMillis() - startTime;
    for (int i = 0; i < ret.length; i++) {
      if (ret[i] < 0 || ret[i] >= numBottles) {
        throw new RuntimeException("Invalid return value: " + ret[i]);
      }
      bottles[ret[i]] = false;
    }
    for (int i = 0; i < bottles.length; i++) {
      if (bottles[i]) {
        System.out.println("A poisoned bottle remained.");
        return res;
      }
    }
    double pct = 1.0 * (numBottles - ret.length) / (numBottles - numPoison);
    res.score = pct * pct;
    return res;
  }

  private static final int THREAD_COUNT = 2;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-N")) myN = Integer.parseInt(args[++i]);
      if (args[i].equals("-S")) myS = Integer.parseInt(args[++i]);
      if (args[i].equals("-R")) myR = Integer.parseInt(args[++i]);
      if (args[i].equals("-P")) myP = Integer.parseInt(args[++i]);
    }

    if (begin != -1 && end != -1) {
      ArrayList<Long> seeds = new ArrayList<>();
      for (long i = begin; i <= end; ++i) {
        seeds.add(i);
      }
      int len = seeds.size();
      Result[] results = new Result[len];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      int prev = 0;
      for (int i = 0; i < THREAD_COUNT; ++i) {
        int next = len * (i + 1) / THREAD_COUNT;
        threads[i] = new TestThread(i, prev, next - 1, seeds, results);
        prev = next;
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / results.length));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    int threadId;
    int begin, end;
    ArrayList<Long> seeds;
    Result[] results;

    TestThread(int threadId, int begin, int end, ArrayList<Long> seeds, Result[] results) {
      this.threadId = threadId;
      this.begin = begin;
      this.end = end;
      this.seeds = seeds;
      this.results = results;
    }

    public void run() {
      for (int i = threadId; i < seeds.size(); i += THREAD_COUNT) {
        try {
          Tester f = new Tester();
          Result res = f.runTest(seeds.get(i));
          results[i] = res;
          if (seeds.get(i) % 100 == 0) {
            System.err.println("seed:" + seeds.get(i));
            System.err.flush();
          }
        } catch (Exception e) {
          e.printStackTrace();
          results[i] = new Result();
          results[i].seed = seeds.get(i);
        }
      }
    }
  }

  private static class Result {
    long seed;
    int N, S, R, P;
    double score;
    long elapsed;

    public String toString() {
      String ret = String.format("seed:%4d\n", seed);
      ret += String.format("N:%4d S:%2d R:%2d P:%3d\n", N, S, R, P);
      ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
      ret += String.format("score:%.7f", score);
      return ret;
    }
  }
}

class PoisonTest {
  private Tester tester;

  PoisonTest(Tester tester) {
    this.tester = tester;
  }

  int[] useTestStrips(String[] tests) {
    if (tests.length > tester.testStrips) {
      throw new RuntimeException("testWine() called with " + tests.length + " tests when only " + tester.testStrips + " strips remain");
    }
    if (tester.testRounds <= 0) {
      throw new RuntimeException("testWine() called too many times");
    }
    int[] ret = new int[tests.length];
    for (int i = 0; i < tests.length; i++) {
      boolean poison = false;
      String[] s = tests[i].split(",");
      for (int j = 0; j < s.length; j++) {
        int x = Integer.parseInt(s[j]);
        if (x < 0 || x >= tester.numBottles) {
          throw new RuntimeException("Invalid value " + x + " found in a test request");
        }
        poison |= tester.bottles[x];
      }
      if (poison) {
        ret[i] = 1;
        tester.testStrips--;
      }
    }
    tester.testRounds--;
    return ret;
  }
}