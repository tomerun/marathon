import java.util.ArrayList;
import java.util.SplittableRandom;

public final class HardCaseSimulation {
  private static boolean DEBUG = false;
  private SplittableRandom rnd = new SplittableRandom();
  final private int N, S, R, P;
  private boolean[] wine;
  private int[] winePos;
  private double[][][] expect;
  private int[][][] bestSize;
  private int safeCount;
  private int strip;
  private int round;
  private Range unvisitedRange;
  private ArrayList<Range> unknownRanges = new ArrayList<>();
  private ArrayList<Range> poisonRanges = new ArrayList<>();

  public static void main(String[] args) {
    int n = 1000;
    int s = 5;
    int r = 4;
    int p = 9;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-N")) n = Integer.parseInt(args[++i]);
      if (args[i].equals("-S")) s = Integer.parseInt(args[++i]);
      if (args[i].equals("-R")) r = Integer.parseInt(args[++i]);
      if (args[i].equals("-P")) p = Integer.parseInt(args[++i]);
    }
    HardCaseSimulation simulator = new HardCaseSimulation(n, s, r, p);
    int originalLen = simulator.determintScanningLength();
    System.err.println("originalLen:" + originalLen);
    long[] sum = new long[50];
    int min = Math.max(1, originalLen - sum.length / 2);
    final int trial = 10000;
    for (int i = 0; i < trial; i++) {
      simulator.setup();
      for (int j = 0; j < sum.length; j++) {
        int score = simulator.testWine(min + j);
        sum[j] += score * score;
      }
    }
    System.out.println("s:" + s + " r:" + r + " p:" + p + " n:" + n);
    int safe = n - p;
    for (int j = 0; j < sum.length; j++) {
      System.out.printf("initialLen:%d %.6f\n", min + j, 1.0 * sum[j] / trial / safe / safe);
    }
  }

  HardCaseSimulation(int N, int S, int R, int P) {
    this.N = N;
    this.S = this.strip = S;
    this.R = R;
    this.P = P;
    this.unvisitedRange = new Range(0, N);
    precalc();
    this.wine = new boolean[N];
    this.winePos = new int[N];
    for (int i = 0; i < N; i++) {
      winePos[i] = i;
    }
  }

  private void setup() {
    for (int i = 0; i < P; i++) {
      wine[winePos[i]] = false;
    }
    for (int i = 0; i < P; i++) {
      int p = rnd.nextInt(N - i) + i;
      int tmp = winePos[i];
      winePos[i] = winePos[p];
      winePos[p] = tmp;
    }
    for (int i = 0; i < P; i++) {
      wine[winePos[i]] = true;
    }
  }

  private int testWine(int initialLen) {
    this.strip = S;
    this.unvisitedRange = new Range(0, N);
    this.unknownRanges.clear();
    this.poisonRanges.clear();
    this.safeCount = 0;

    solveHardCase(initialLen);

    int ret = 0;
    for (Range r : unknownRanges) {
      ret += r.size();
    }
    for (Range r : poisonRanges) {
      ret += r.size();
    }
    ret += unvisitedRange.size();
    return N - ret;
  }

  private void solveHardCase(int initialLen) {
    for (round = 0; round < R && strip > 0; round++) {
      if (DEBUG) System.err.println("round " + round);
      if (round == R - 1 && finalRound()) return;
      ArrayList<Range> curRanges = singleRound(initialLen);
      test(curRanges);
    }
  }

  private ArrayList<Range> singleRound(int initialLen) {
    ArrayList<Range> testRanges = new ArrayList<>();
    int singleLen = round == 0 ? initialLen : determintScanningLength();
    if (singleLen > 0) {
      int start = unvisitedRange.s;
      for (int i = 0; i < strip; i++) {
        if (start + (i + 1) * singleLen >= N) {
          testRanges.add(new Range(start + i * singleLen, N));
          break;
        }
        testRanges.add(new Range(start + i * singleLen, start + (i + 1) * singleLen));
      }
      unvisitedRange.s = testRanges.get(testRanges.size() - 1).e;
    }
    ArrayList<Range> candidate = new ArrayList<>();
    candidate.addAll(unknownRanges);
    poisonRanges.forEach(r -> {
      if (r.s + 1 < r.e) candidate.add(r);
    });
    testRanges.addAll(narrowing(strip - testRanges.size(), candidate));
    return testRanges;
  }

  private int determintScanningLength() {
    int len = unvisitedRange.e - unvisitedRange.s;
    if (len == 0 || poisonRanges.size() == P) {
      return 0;
    }
    int bestI = 0;
    double bestExpect = 0;
    int poison = P - poisonRanges.size();
    double initP = 1.0;
    for (int i = 0; i < poison; i++) {
      initP *= len - strip - i;
      initP /= len - i;
    }
    for (int i = 1; i * strip <= len; i++) {
      double p = initP;
      double sum = 0;
      for (int j = 0; j <= strip; j++) {
//        System.err.println("j:" + j + " expect:" + expect[R - round - 1][strip - j][len - i * strip] + " p:" + p );
        double gain = safeCount + i * (strip - j) + expect[R - round - 1][strip - j][len - i * strip];
        if (j == poison) gain += len - i * strip;
        sum += Math.pow(gain, 2) * p;
        p *= i * strip - j;
        p /= j + 1;
        p /= len - i * strip - poison + j + 1;
        p *= poison - j;
      }
//      if (DEBUG) System.err.println("i:" + i + " exp:" + (sum / (N - P) / (N - P)) + " initP:" + initP);
      if (sum > bestExpect) {
        bestExpect = sum;
        bestI = i;
      } else if (i > bestI + 10) { // early exit
        break;
      }
      for (int j = 0; j < strip; j++) {
        initP *= len - (i + 1) * strip - poison + 1 + j;
        initP /= len - i * strip - j;
      }
    }
    return bestI == 0 ? 1 : bestI;
  }

  private static ArrayList<Range> narrowing(int stripCount, ArrayList<Range> candidate) {
    if (candidate.isEmpty()) return new ArrayList<>();
    int[] partition = new int[candidate.size()];
    for (int i = 0; i < stripCount; ++i) {
      int maxI = 0;
      for (int j = 1; j < candidate.size(); j++) {
        if (candidate.get(maxI).size() * (partition[j] + 1) < candidate.get(j).size() * (partition[maxI] + 1)) {
          maxI = j;
        }
      }
      partition[maxI]++;
    }
    ArrayList<Range> ret = new ArrayList<>();
    for (int i = 0; i < candidate.size(); i++) {
      Range r = candidate.get(i);
      for (int j = 0; j < partition[i]; j++) {
        ret.add(new Range(r.s + r.size() * j / (partition[i] + 1), r.s + r.size() * (j + 1) / (partition[i] + 1)));
      }
    }
    ret.removeIf(r -> r.size() == 0);
    return ret;
  }

  private void test(ArrayList<Range> ranges) {
    int[] res = new int[ranges.size()];
    for (int i = 0; i < ranges.size(); i++) {
      for (int j = ranges.get(i).s; j < ranges.get(i).e; j++) {
        if (wine[j]) {
          res[i] = 1;
          break;
        }
      }
    }
    for (int i = 0; i < ranges.size(); i++) {
      Range r = ranges.get(i);
      if (DEBUG) System.err.println("test:[" + r.s + "-" + r.e + "] -> " + res[i]);
      if (res[i] == 1) {
        --strip;
      } else {
        safeCount += r.size();
      }
      boolean found = false;
      for (Range p : unknownRanges) {
        if (p.contains(r)) {
          found = true;
          if (p.s == r.s) {
            p.s = r.e;
          } else {
            p.e = r.s;
          }
          if (res[i] == 1) {
            poisonRanges.add(r);
          }
          break;
        }
      }
      if (!found) {
        for (Range p : poisonRanges) {
          if (p.contains(r)) {
            found = true;
            if (res[i] == 1) {
              if (p.s == r.s) {
                unknownRanges.add(new Range(r.e, p.e));
                p.e = r.e;
              } else {
                unknownRanges.add(new Range(p.s, r.s));
                p.s = r.s;
              }
            } else {
              if (p.s == r.s) {
                p.s = r.e;
              } else {
                p.e = r.s;
              }
            }
            break;
          }
        }
      }
      if (!found) {
        if (res[i] == 1) {
          poisonRanges.add(r);
        }
      }
    }
    while (true) {
      boolean update = false;
      for (int i = 0; i < ranges.size(); i++) {
        if (ranges.get(i).s == unvisitedRange.s) {
          unvisitedRange.s = ranges.get(i).e;
          update = true;
        }
      }
      if (!update) break;
    }
    if (poisonRanges.size() == P) {
      unknownRanges.clear();
      unvisitedRange.s = N;
    }
    poisonRanges.removeIf(r -> r.s == r.e);
    unknownRanges.removeIf(r -> r.s == r.e);
  }

  private static final int[][] MASK_STRIP = new int[][]{
      {},
      {},
      {1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2},
  };

  private boolean finalRound() {
    if (poisonRanges.isEmpty()) {
      if (!unknownRanges.isEmpty()) return false;
      if (P >= MASK_STRIP.length) return false;
      if (P == 1) {
        SinglePointDetector spd = new SinglePointDetector();
        spd.add(unvisitedRange, strip);
        ArrayList<Integer> discard = spd.test();
        for (int d : discard) {
          poisonRanges.add(new Range(d, d + 1));
        }
        unvisitedRange.s = unvisitedRange.e;
        return true;
      } else {
        if (MASK_STRIP[P][strip] == 1) return false;
        MultiPointDetector mpd = new MultiPointDetector(unvisitedRange, P, strip, MASK_STRIP[P][strip]);
        ArrayList<Integer> discard = mpd.test();
        for (int d : discard) {
          poisonRanges.add(new Range(d, d + 1));
        }
        unvisitedRange.s = unvisitedRange.e;
        return true;
      }
    } else if (poisonRanges.size() < P) {
      return false;
    } else {
      ArrayList<Range> prs = new ArrayList<>(poisonRanges);
      prs.sort((r1, r2) -> Integer.compare(r2.size(), r1.size()));
      int[] len = new int[P];
      for (int i = 0; i < strip; i++) {
        len[i % P]++;
      }
      SinglePointDetector spd = new SinglePointDetector();
      for (int i = 0; i < P; i++) {
        spd.add(prs.get(i), len[i]);
      }
      ArrayList<Integer> discard = spd.test();
      poisonRanges.clear();
      for (int d : discard) {
        poisonRanges.add(new Range(d, d + 1));
      }
      unvisitedRange.s = unvisitedRange.e;
      return true;
    }
  }

  private void precalc() {
    expect = new double[R + 1][S + 1][N + 1]; // [# of rounds left][# of strips left][size of unexperimented area]
    bestSize = new int[R + 1][S + 1][N + 1];
    for (int i = 1; i < R; i++) {
      for (int j = 1; j <= S; j++) {
        int poison = P - (S - j);
        for (int k = 1; k <= N; k++) {
//        for (int k = Math.max(1, N - (R - i) * S * 200); k <= N; k++) {
          if (poison <= 0) {
            expect[i][j][k] = k;
            continue;
          }
          if (poison >= k) {
            continue;
          }
          double initP = 1.0;
          int initL = k == 1 ? 1 : Math.max(1, bestSize[i][j][k - 1] - 5);
          for (int m = 0; m < poison; m++) {
            // C(l*j, 0) * C(k - l*j, p - 0) / C(k, p)
            initP *= k - initL * j - m;
            initP /= k - m;
          }
          for (int l = initL; l * j <= k; l++) {
            double p = initP;
            double sum = 0;
            for (int m = 0; m < j; m++) {
              double gain = l * (j - m) + expect[i - 1][j - m][k - l * j];
              if (m == poison) {
                gain += k - l * j;
              }
              sum += gain * p;
              // C(l*j, m) * C(k - l*j, p - m) / C(k, p)
              p *= l * j - m;
              p /= m + 1;
              p /= k - l * j - poison + m + 1;
              p *= poison - m;
            }
            if (j == poison) sum += (k - l * j) * p;
            if (sum > expect[i][j][k]) {
              expect[i][j][k] = sum;
              bestSize[i][j][k] = l;
            } else if (l > bestSize[i][j][k] + 10) { // early exit
              break;
            }
            for (int m = 0; m < j; m++) {
              initP *= k - (l + 1) * j - poison + 1 + m;
              initP /= k - l * j - m;
            }
          }
        }
      }
    }
  }

  private class SinglePointDetector {
    ArrayList<Range> source = new ArrayList<>();
    ArrayList<Integer> strips = new ArrayList<>();

    void add(Range r, int s) {
      source.add(r);
      strips.add(s);
    }

    ArrayList<Integer> test() {
      ArrayList<ArrayList<Integer>> list = new ArrayList<>();
      ArrayList<ArrayList<Integer>> listNega = new ArrayList<>();
      for (int s : strips) {
        for (int i = 0; i < s; i++) {
          list.add(new ArrayList<>());
          listNega.add(new ArrayList<>());
        }
      }
      for (int i = 0, pos = 0; i < source.size(); i++) {
        Range r = source.get(i);
        assert (r.s < r.e);
        for (int j = r.s; j < r.e; j++) {
          int bits = (j - r.s) % (1 << strips.get(i));
          for (int k = 0; k < strips.get(i); k++) {
            if ((bits & (1 << k)) != 0) {
              list.get(pos + k).add(j);
            } else {
              listNega.get(pos + k).add(j);
            }
          }
        }
        pos += strips.get(i);
      }
      int[] res = new int[list.size()];
      for (int i = 0; i < list.size(); i++) {
        for (int j = 0; j < list.get(i).size(); j++) {
          if (wine[list.get(i).get(j)]) {
            res[i] = 1;
            break;
          }
        }
      }

      ArrayList<Integer> ret = new ArrayList<>();
      for (int i = 0, pos = 0; i < source.size(); i++) {
        Range r = source.get(i);
        boolean[] safe = new boolean[r.size()];
        boolean[] danger = new boolean[r.size()];
        boolean any = false;
        for (int j = 0; j < strips.get(i); j++) {
          if (res[pos + j] == 1) {
            any = true;
            for (int k : list.get(pos + j)) {
              danger[k - r.s] = true;
            }
            for (int k : listNega.get(pos + j)) {
              safe[k - r.s] = true;
            }
          } else {
            for (int k : list.get(pos + j)) {
              safe[k - r.s] = true;
            }
            for (int k : listNega.get(pos + j)) {
              danger[k - r.s] = true;
            }
          }
        }
        if (any) {
          for (int j = r.s; j < r.e; j++) {
            if (!danger[j - r.s]) safe[j - r.s] = true;
          }
        }
        for (int j = r.s; j < r.e; j++) {
          if (!safe[j - r.s]) ret.add(j);
        }
        pos += strips.get(i);
      }
      return ret;
    }
  }

  private class MultiPointDetector {
    Range range;
    int poison, strip, bits;

    MultiPointDetector(Range r, int p, int s, int bits) {
      this.range = r;
      this.poison = p;
      this.strip = s;
      this.bits = bits;
    }

    ArrayList<Integer> test() {
      ArrayList<ArrayList<Integer>> list = new ArrayList<>();
      for (int i = 0; i < strip; i++) {
        list.add(new ArrayList<>());
      }
      ArrayList<Integer> masks = new ArrayList<>();
      for (int i = 0; i < (1 << strip); i++) {
        if (Integer.bitCount(i) == bits) {
          masks.add(i);
        }
      }
      for (int i = range.s; i < range.e; i++) {
        int mask = masks.get((i - range.s) % masks.size());
        for (int j = 0; j < strip; j++) {
          if ((mask & (1 << j)) != 0) list.get(j).add(i);
        }
      }
      int[] res = new int[list.size()];
      for (int i = 0; i < list.size(); i++) {
        for (int j = 0; j < list.get(i).size(); j++) {
          if (wine[list.get(i).get(j)]) {
            res[i] = 1;
            break;
          }
        }
      }
      boolean[] safe = new boolean[range.size()];
      for (int i = 0; i < res.length; i++) {
        if (res[i] == 1) continue;
        for (int pos : list.get(i)) safe[pos - range.s] = true;
      }
      ArrayList<Integer> ret = new ArrayList<>();
      for (int i = 0; i < range.size(); i++) {
        if (!safe[i]) ret.add(i + range.s);
      }
      return ret;
    }
  }

  private static final class Range {
    int s, e;

    Range(int s, int e) {
      this.s = s;
      this.e = e;
    }

    boolean contains(Range r) {
      return this.s <= r.s && r.e <= this.e;
    }

    int size() {
      return this.e - this.s;
    }

    @Override
    public String toString() {
      return "Range(" + s + "," + e + ')';
    }
  }

}