import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Csv {

	ArrayList<String> header;
	ArrayList<ArrayList<String>> values;

	private Csv(String filename) throws IOException {
		Path path = FileSystems.getDefault().getPath(filename);
		List<String> lines = Files.readAllLines(path, StandardCharsets.ISO_8859_1);
		header = parseElems(lines.get(0));
		values = new ArrayList<ArrayList<String>>();
		for (int i = 1; i < lines.size(); ++i) {
			String line = lines.get(i);
			if (lines.isEmpty()) break;
			ArrayList<String> elems = parseElems(line);
			if (elems.size() != header.size()) {
				System.err.println(line);
				System.err.println(elems);
				throw new RuntimeException("column size is inconsistent : in row " + values.size() + ", columns size "
						+ elems.size() + " is detected but header size is " + header.size());
			}
			values.add(elems);
		}
	}

	private static ArrayList<String> parseElems(String str) {
		ArrayList<String> ret = new ArrayList<>();
		int pos = 0;
		while (pos < str.length()) {
			while (true) {
				if (pos == str.length()) {
					return ret;
				}
				if (!Character.isWhitespace(str.charAt(pos))) break;
				++pos;
			}
			int start = pos;
			char stop = ',';
			if (str.charAt(pos) == '"') {
				++pos;
				start = pos;
				stop = '"';
			}
			while (pos != str.length()) {
				if (str.charAt(pos) == stop) {
					if (stop == '"' && pos < str.length() - 1 && str.charAt(pos + 1) == '"') {
						++pos; // escaped "
					} else {
						break;
					}
				}
				++pos;
			}
			ret.add(str.substring(start, pos));
			++pos;
			if (stop == '"') ++pos;
		}
		return ret;
	}

	static Csv parse(String filename) {
		try {
			return new Csv(filename);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	void write(String filename) throws IOException {
		FileWriter writer = new FileWriter(new File(filename));
		writeValues(writer, header, null);
		for (int i = 0; i < values.size(); ++i) {
			writeValues(writer, values.get(i), null);
		}
		writer.flush();
	}

	void writeAligned(String filename) throws IOException {
		FileWriter writer = new FileWriter(new File(filename));
		int[] colWidth = new int[header.size()];
		for (int i = 0; i < colWidth.length; ++i) {
			int maxWidth = header.get(i).length();
			for (int j = 0; j < values.size(); ++j) {
				String v = values.get(j).get(i);
				maxWidth = Math.max(maxWidth, v.indexOf(',') != -1 ? v.length() + 2 : v.length());
			}
			colWidth[i] = maxWidth;
		}
		writeValues(writer, header, colWidth);
		for (int i = 0; i < values.size(); ++i) {
			writeValues(writer, values.get(i), colWidth);
		}
		writer.flush();
	}

	private static void writeValues(Writer writer, ArrayList<String> values, int[] width) throws IOException {
		for (int i = 0; i < values.size() - 1; ++i) {
			int space = 0;
			if (values.get(i).indexOf(',') == -1) {
				writer.write(values.get(i));
				if (width != null) space = width[i] - values.get(i).length();
			} else {
				writer.write('"' + values.get(i) + '"');
				if (width != null) space = width[i] - values.get(i).length() - 2;
			}
			writer.write(',');
			for (int j = 0; j < space; ++j) {
				writer.write(' ');
			}
		}
		writer.write(values.get(values.size() - 1) + "\n");
	}

	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.err.println("give input and output filenames");
			return;
		}
		Csv csv = Csv.parse(args[0]);
		if (csv == null) {
			System.err.println("cannot parse " + args[0]);
			return;
		}
		csv.writeAligned(args[1]);
	}

}
