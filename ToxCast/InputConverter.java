import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputConverter {

	private static boolean procSummary = false;

	public static void exec() throws IOException {
		Csv train = Csv.parse("testdata/ToxCast_MM_Data/ToxRefDB_Challenge_Training.csv");
		train.header.set(0, "DSSTox_GSID");
		for (int i = 0; i < train.values.size(); ++i) {
			train.values.get(i).set(0, train.values.get(i).get(0).substring("DSSTox_GSID_".length()));
		}

		System.err.println("generic");
		Csv generic = Csv
				.parse("testdata/ToxCast_MM_Data/Chemical List & Annotations/ToxCast_Generic_Chemicals/csv/ToxCast_Generic_Chemicals_2013_12_10.csv");
		for (int i = 0; i < generic.values.size(); ++i) {
			generic.values.get(i).set(0, generic.values.get(i).get(0).substring("DSSTox_GSID_".length()));
			generic.values.get(i).set(10, generic.values.get(i).get(10).substring("DSSTox_CID_".length()));
		}
		concat(train, generic, 0, 0, new int[] { 1, 10, 4, 5, 6, 8 });

		System.err.println("structure");
		Csv structure = Csv
				.parse("testdata/ToxCast_MM_Data/Chemical List & Annotations/DSSTox/csv/TOX21S_v4a_8599_11Dec2013.csv");
		concat(train, structure, 0, 0, new int[] { 5, 6, 7, 9, 10, 11, 12 });

		System.err.println("bond");
		Csv bond = Csv
				.parse("testdata/ToxCast_MM_Data/Chemical List & Annotations/DSSTox/csv/toxprint_v2_vs_TOX21S_v4a_8599_03Dec2013.csv");
		int[] idx = new int[bond.header.size() - 1];
		for (int i = 0; i < idx.length; ++i) {
			idx[i] = i + 1;
		}
		concat(train, bond, 5, 0, idx);

		System.err.println("summary");
		procSummary = true;
		Csv summary = Csv
				.parse("testdata/ToxCast_MM_Data/ToxCast Assay Data/assay_data/curve_fits/Csv/ToxCast_Summary_AC50_2013_12_10_NO_BSK.csv");
		for (int i = 0; i < summary.values.size(); ++i) {
			ArrayList<String> elem = summary.values.get(i);
			for (int j = 0; j < elem.size(); ++j) {
				if (elem.get(j).equals("1.00E+06")) {
					elem.set(j, "IF");
				}
			}
		}
		idx = new int[summary.header.size() - 1];
		for (int i = 0; i < idx.length; ++i) {
			idx[i] = i + 1;
		}
		concat(train, summary, 1, 0, idx);
		for (ArrayList<String> elem : train.values) {
			while (elem.size() < train.header.size()) {
				elem.add("IF");
			}
		}

		train.write("testdata/merged.csv");
	}

	private static void concat(Csv to, Csv from, int idColTo, int idColFrom, int[] addIdx) {
		for (int idx : addIdx) {
			to.header.add(from.header.get(idx));
		}
		boolean[] used = new boolean[from.values.size()];
		for (int i = 0; i < to.values.size(); ++i) {
			String id = to.values.get(i).get(idColTo);
			boolean found = false;
			for (int j = 0; j < from.values.size(); ++j) {
				if (!from.values.get(j).get(idColFrom).equals(id)) continue;
				for (int idx : addIdx) {
					to.values.get(i).add(from.values.get(j).get(idx));
				}
				found = true;
				used[j] = true;
				break;
			}
			if (!found && procSummary) {
				// calendar date string -> date int value
				for (int j = 0; j < from.values.size(); ++j) {
					String conv = dateToId(from.values.get(j).get(idColFrom));
					if (conv == null || !conv.equals(id)) continue;
					for (int idx : addIdx) {
						to.values.get(i).add(from.values.get(j).get(idx));
					}
					found = true;
					used[j] = true;
					break;
				}
			}
			if (!found) {
				System.err.println("not found:" + i + " " + id);
			}
		}
		if (procSummary) {
			for (int i = 0; i < used.length; ++i) {
				if (!used[i]) {
					System.err.println("not used:" + i + " " + from.values.get(i).get(idColFrom));
				}
			}
		}
	}

	private static Pattern datePat = Pattern.compile("(\\d+)-(\\d+)-(\\d+)");
	private static Calendar origin = new GregorianCalendar(1899, Calendar.DECEMBER, 31);

	private static String dateToId(String dateStr) {
		Matcher match = datePat.matcher(dateStr);
		if (!match.matches()) {
			return null;
		}
		int year = Integer.parseInt(match.group(1));
		int month = Integer.parseInt(match.group(2));
		int day = Integer.parseInt(match.group(3));
		Calendar cal = new GregorianCalendar(year, month - 1, day);
		long diff = cal.getTimeInMillis() - origin.getTimeInMillis();
		final long DAY_MILLIS = 24 * 60 * 60 * 1000;
		long dayDiff = (diff + DAY_MILLIS / 2) / DAY_MILLIS;
		return "" + (dayDiff + 1);
	}
}
