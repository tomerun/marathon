import java.util.ArrayList;

public class Chem {
	int idx;
	double lel; // [-50, 50] or Double.NaN or Double.POSITIVE_INFINITY
	String chemName;
	boolean[] struct = new boolean[LELPredictor.N_STRUCT_COL];
	double[] ac50 = new double[LELPredictor.N_AC50_COL];

	Chem(int idx, ArrayList<String> elems) {
		this.idx = idx;
		String lelStr = elems.get(3);
		this.lel = lelStr.equals("?") ? Double.NaN : Double.parseDouble(lelStr);
		this.chemName = elems.get(2);
		for (int i = 0; i < struct.length; ++i) {
			struct[i] = elems.get(i + LELPredictor.START_STRUCT_COL).equals("1");
		}
		for (int i = 0; i < ac50.length; ++i) {
			String v = elems.get(i + LELPredictor.START_AC50_COL);
			if (v.equals("NA")) {
				ac50[i] = Double.NaN;
			} else if (v.equals("IF")) {
				ac50[i] = Double.POSITIVE_INFINITY;
			} else {
				ac50[i] = Double.parseDouble(v);
			}
		}
	}
}
