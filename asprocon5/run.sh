#!/bin/sh

for (( i = 1; i <= 180; i++ )); do
	input=$(printf "testdata/%03d.txt" $i)
	output=$(printf "output/%03d.txt" $i)
	 ./tester < $input > $output 2>> log.txt
	 echo "test_id:" $(printf "data/%03d.txt" $i) " score:" "$(tools/checker $input $output)"
done