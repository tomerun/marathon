#include <algorithm>
#include <numeric>
#include <utility>
#include <vector>
#include <bitset>
#include <string>
#include <queue>
#include <unordered_map>
#include <set>
#include <iostream>
#include <array>
#include <tuple>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 1900;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 1980;
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

const int INF = 1 << 28;
// const double INITIAL_COOLER = 0.0000001;
// const double FINAL_COOLER = 0.0001;
double sa_cooler;

inline bool accept(ll diff) {
	if (diff <= 0) return true;
	diff = -diff * sa_cooler;
	return diff > -10 && rnd.nextUInt() < (1u << (int)(31.5 + diff));
}

struct Item {
	int ss, cx;
	vector<bool> stepX, stepY;
};

struct Order {
	int i, e, d, q, a;
	double pena_ratio;
};

struct MainResource {
	int m;
};

struct SubResource {
	int m;
};

struct Occupation {
	int st, et, iid;
};

struct OrderAnswer {
	int t1, t2, t3;
	array<int, 10> assign;
};

struct Result {
	vector<OrderAnswer> answers;
	ll pena;
};

struct ResultMain {
	vector<vector<int>> m2o;
	ll pena;
};

struct MainOcc {
	int t, oid;
};

struct State {
	vi ois;
	vi o2mr;
	vvi o2srs;
	ll pena;
};

ll COMBI_PENA_RATIO = 1000;
int I, M, MM, SM, R;
array<array<array<int, 10>, 11>, 100> combi;
vector<Item> items;
vector<Order> orders;
vector<vector<MainResource>> mrs; // mrs[item_id]
vector<vector<vector<SubResource>>> srs;  // srs[item_id][step_id - 1]
array<vector<Occupation>, 100> occs;
array<array<int, 100>, 10> caps; // caps[item_id][main_resource_id]
bool delay_pena_always;
ll pena_diff_cutoff;

void clear_occs() {
	for (int i = 0; i < M; ++i) {
		occs[i].clear();
		occs[i].push_back({0, 0, I});
	}
}

inline void evaluate_single(int m, int oi, int& t, int& pi, ll& pena) {
	const Order& order = orders[oi];
	const Item& item = items[order.i];
	int combi_time = item.cx ? combi[m][pi][order.i] : 0;
	int manu_time = caps[order.i][m] * order.q;
	t = max(order.e, t) + (item.stepX[0] ? combi_time : 0) + manu_time;
	pi = order.i;
	pena += combi_time * items[order.i].cx * COMBI_PENA_RATIO;
	if (delay_pena_always || t > order.d) {
		pena += (ll)ceil(order.pena_ratio * (t - order.d));
	}
}

ll evaluate_move(int m, const vi& ois, int p1, int p2) {
	ll pena = 0;
	int t = 0;
	int pi = I;
	if (p1 < p2) {
		for (int i = 0; i < p1; ++i) {
			evaluate_single(m, ois[i], t, pi, pena);
		}
		for (int i = p1 + 1; i <= p2; ++i) {
			evaluate_single(m, ois[i], t, pi, pena);
		}
		evaluate_single(m, ois[p1], t, pi, pena);
		for (int i = p2 + 1; i < ois.size(); ++i) {
			evaluate_single(m, ois[i], t, pi, pena);
		}
	} else {
		for (int i = 0; i < p2; ++i) {
			evaluate_single(m, ois[i], t, pi, pena);
		}
		evaluate_single(m, ois[p1], t, pi, pena);
		for (int i = p2; i < p1; ++i) {
			evaluate_single(m, ois[i], t, pi, pena);
		}
		for (int i = p1 + 1; i < ois.size(); ++i) {
			evaluate_single(m, ois[i], t, pi, pena);
		}
	}
	return pena;
}

ll evaluate_del(int m, const vi& ois, int pos) {
	ll pena = 0;
	int t = 0;
	int pi = I;
	for (int i = 0; i < pos; ++i) {
		evaluate_single(m, ois[i], t, pi, pena);
	}
	for (int i = pos + 1; i < ois.size(); ++i) {
		evaluate_single(m, ois[i], t, pi, pena);
	}
	return pena;
}

ll evaluate_add(int m, const vi& ois, int pos, int add_oi) {
	ll pena = 0;
	int t = 0;
	int pi = I;
	for (int i = 0; i < pos; ++i) {
		evaluate_single(m, ois[i], t, pi, pena);
	}
	evaluate_single(m, add_oi, t, pi, pena);
	for (int i = pos; i < ois.size(); ++i) {
		evaluate_single(m, ois[i], t, pi, pena);
	}
	return pena;
}

ResultMain solve_only_main(ll timelimit) {
	vi item_neck(I, 10);
	vi best_mri(I);
	for (int i = 0; i < I; ++i) {
		for (int j = 0; j < items[i].ss - 1; ++j) {
			item_neck[i] = min(item_neck[i], (int)srs[i][j].size());
		}
		int max = 999;
		for (int j = 0; j < mrs[i].size(); ++j) {
			if (caps[i][mrs[i][j].m] < max) {
				max = caps[i][mrs[i][j].m];
				best_mri[i] = j;
			}
		}
	}
	vi ois(R);
	iota(ois.begin(), ois.end(), 0);
	sort(ois.begin(), ois.end(), [&item_neck](int i, int j){ 
		if (item_neck[orders[i].i] == item_neck[orders[j].i]) {
			return orders[i].e < orders[j].e;
		} else {
			return item_neck[orders[i].i] < item_neck[orders[j].i];
		}
	});

	ResultMain res;
	res.m2o.resize(MM);
	res.pena = 0;
	ll abs_pena = 0;
	clear_occs();
	vector<ll> penas(MM);
	vi o2m(R);
	for (int i = 0; i < R; ++i) {
		const Order& order = orders[ois[i]];
		const Item& item = items[order.i];
		int min_m = 0;
		ll min_pena = 1LL << 60;
		ll min_abs_pena = 0;
		int min_j = item_neck[order.i] == 1 ? best_mri[order.i] : 0;
		int max_j = item_neck[order.i] == 1 ? best_mri[order.i] : mrs[order.i].size() - 1;
		for (int j = min_j; j <= max_j; ++j) {
			int m = mrs[order.i][j].m;
			const Occupation& prev = occs[m].back();
			int combi_time = item.cx ? combi[m][prev.iid][order.i] : 0;
			int manu_time = caps[order.i][m] * order.q;
			int start_time = max(order.e, prev.et);
			int end_time = start_time + (item.stepX[0] ? combi_time : 0) + manu_time;
			ll pena = 0;
			if (delay_pena_always || end_time > order.d) {
				pena += (ll)ceil((end_time - order.d) * order.pena_ratio);
			}
			ll ap = pena > 0 ? pena : 0;
			pena += combi_time * item.cx * COMBI_PENA_RATIO;
			ap += combi_time * item.cx * COMBI_PENA_RATIO;;
			if (pena < min_pena) {
				min_pena = pena;
				min_abs_pena = ap;
				min_m = m;
			}
		}
		const Occupation& prev = occs[min_m].back();
		int combi_time = item.cx ? combi[min_m][prev.iid][order.i] : 0;
		int manu_time = caps[order.i][min_m] * order.q;
		int start_time = max(order.e, prev.et);
		int end_time = start_time + (item.stepX[0] ? combi_time : 0) + manu_time;
		res.pena += min_pena;
		abs_pena += min_abs_pena;
		penas[min_m] += min_pena;
		occs[min_m].push_back({start_time, end_time, order.i});
		res.m2o[min_m].push_back(ois[i]);
		o2m[ois[i]] = min_m;
	}
	const double initial_cooler = min(0.0001, 1000.0 / abs_pena);
	const double final_cooler = min(0.01, 1000000.0 / abs_pena);
	const double c0 = log(initial_cooler);
	const double c1 = log(final_cooler);
	sa_cooler = initial_cooler;
	debug("pena:%lld turn:0\n ", res.pena);
	const ll before_time = get_elapsed_msec();
	for (int turn = 1; ; ++turn) {
		if ((turn & 0x7FF) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed >= timelimit || res.pena == 0) {
				debug("pena:%lld turn:%d\n ", res.pena, turn);
				break;
			}
			const double ratio = 1.0 * (elapsed - before_time) / (timelimit - before_time);
			sa_cooler = exp(c1 * ratio + c0 * (1 - ratio));
			debug("cooler:%.8f pena:%lld turn:%d\n", sa_cooler, res.pena, turn);
		}
		int oi = rnd.nextUInt(R);
		int m1 = o2m[oi];
		int i1 = orders[oi].i;
		int m2 = mrs[i1][rnd.nextUInt(mrs[i1].size())].m;
		int p1 = find(res.m2o[m1].begin(), res.m2o[m1].end(), oi) - res.m2o[m1].begin();
		if (m1 == m2) {
			if (res.m2o[m1].size() == 1) continue;
			int p2 = rnd.nextUInt(res.m2o[m1].size() - 1);
			if (p1 <= p2) p2++;
			const ll new_pena = evaluate_move(m1, res.m2o[m1], p1, p2);
			const ll diff = new_pena - penas[m1];
			// debug("1 diff:%lld\n", diff);
			if (accept(diff)) {
				penas[m1] = new_pena;
				res.pena += diff;
				if (p1 < p2) {
					rotate(res.m2o[m1].begin() + p1, res.m2o[m1].begin() + p1 + 1, res.m2o[m1].begin() + p2 + 1);
				} else {
					rotate(res.m2o[m1].begin() + p2, res.m2o[m1].begin() + p1, res.m2o[m1].begin() + p1 + 1);
				}
				// debug("1 pena:%lld at turn:%d\n", res.pena, turn);
			}
		} else {
			int p2 = rnd.nextUInt(res.m2o[m2].size() + 1);
			const ll new_pena1 = evaluate_del(m1, res.m2o[m1], p1);
			const ll new_pena2 = evaluate_add(m2, res.m2o[m2], p2, res.m2o[m1][p1]);
			const ll diff = new_pena1 + new_pena2 - penas[m1] - penas[m2];
			// debug("2 diff:%lld\n", diff);
			if (accept(diff)) {
				penas[m1] = new_pena1;
				penas[m2] = new_pena2;
				res.pena += diff;
				o2m[res.m2o[m1][p1]] = m2;
				res.m2o[m2].insert(res.m2o[m2].begin() + p2, res.m2o[m1][p1]);
				res.m2o[m1].erase(res.m2o[m1].begin() + p1);
				// debug("2 pena:%lld at turn:%d\n", res.pena, turn);
			}
		}
	}
	return res;
}

State create_state(const ResultMain& resm, ll best_pena) {
	State state;
	state.pena = 0;
	state.o2mr.resize(R);
	state.o2srs.resize(R);
	vi resm_pos(MM);
	clear_occs();
	for (int i = 0; i < R; ++i) {
		int min_m = 0;
		int min_t = INF;
		array<int, 9> min_sub_assgin;
		array<int, 9> tmp_sub_assign;
		bool stick = false;
		int prev_mr = -1;
		if (i > 0) {
			prev_mr = state.o2mr[state.ois.back()];
			if (resm_pos[prev_mr] < resm.m2o[prev_mr].size()) {
				const int noi = resm.m2o[prev_mr][resm_pos[prev_mr]];
				stick = orders[noi].e <= occs[prev_mr].back().et && orders[noi].i == occs[prev_mr].back().iid;
			}
		}
		if (stick) {
			const int oi = resm.m2o[prev_mr][resm_pos[prev_mr]];
			const Order& order = orders[oi];
			const Item& item = items[order.i];
			const Occupation prev = occs[prev_mr].back();
			const int manu_time = caps[order.i][prev_mr] * order.q;
			int start_time = max(order.e, prev.et);
			int end_time = start_time + manu_time;
			for (int k = 0; k < item.ss - 1; ++k) {
				if (!item.stepY[k + 1]) {
					min_sub_assgin[k] = -1;
					continue;
				}
				// if (max(order.e, occs[min_sub_assgin[k]].back().et) + manu_time <= end_time) {
				// 	continue;
				// }
				const vector<SubResource>& sms = srs[order.i][k];
				int min_sub_time = INF;
				for (int l = 0; l < sms.size(); ++l) {
					int sm = sms[l].m;
					int et = max(order.e, occs[sm].back().et);
					et += manu_time;
					if (min_sub_time > end_time && et < min_sub_time) {
						min_sub_time = et;
						min_sub_assgin[k] = sm;
						if (min_sub_time <= end_time) break;
					}
				}
				if (min_sub_time > end_time) {
					end_time = min_sub_time;
				}
			}
			min_t = end_time;
			min_m = prev_mr;
		} else {
			for (int j = 0; j < MM; ++j) {
				if (resm_pos[j] == resm.m2o[j].size()) continue;
				const int oi = resm.m2o[j][resm_pos[j]];
				const Order& order = orders[oi];
				const Item& item = items[order.i];
				const Occupation prev = occs[j].back();
				const int combi_time = item.cx ? combi[j][prev.iid][order.i] : 0;
				const int manu_time = caps[order.i][j] * order.q;
				int start_time = max(order.e, prev.et);
				int end_time = start_time + (item.stepX[0] ? combi_time : 0) + manu_time;
				for (int k = 0; k < item.ss - 1 && end_time < min_t; ++k) {
					const vector<SubResource>& sms = srs[order.i][k];
					if (combi_time == 0 && !item.stepY[k + 1]) {
						tmp_sub_assign[k] = -1;
						continue;
					}
					int min_sub_time = INF;
					for (int l = 0; l < sms.size(); ++l) {
						int sm = sms[l].m;
						int et = max(order.e, occs[sm].back().et);
						if (item.stepX[k + 1]) {
							et += combi_time;
						}
						et += manu_time;
						if (min_sub_time > end_time && et < min_sub_time) {
							min_sub_time = et;
							tmp_sub_assign[k] = sm;
							if (min_sub_time <= end_time) break;
						}
					}
					if (min_sub_time > end_time) {
						end_time = min_sub_time;
					}
				}
				if (end_time < min_t) {
					min_t = end_time;
					min_m = j;
					copy(tmp_sub_assign.begin(), tmp_sub_assign.begin() + item.ss - 1, min_sub_assgin.begin());
				}
			}
		}
		const int oi = resm.m2o[min_m][resm_pos[min_m]];
		const Order& order = orders[oi];
		const Item& item = items[order.i];
		state.ois.push_back(oi);
		state.o2mr[oi] = min_m;
		state.o2srs[oi].assign(min_sub_assgin.begin(), min_sub_assgin.begin() + item.ss - 1);
		resm_pos[min_m]++;
		const int combi_time = item.cx ? combi[min_m][occs[min_m].back().iid][order.i] : 0;
		const int manu_time = caps[order.i][min_m] * order.q;
		const int st = min_t - manu_time - (item.stepX[0] ? combi_time : 0);
		occs[min_m].push_back({st, min_t, order.i});
		for (int j = 0; j < item.ss - 1; ++j) {
			const int sr = state.o2srs[oi][j];
			if (sr == -1) continue;
			const int et = item.stepY[j + 1] ? min_t : min_t - manu_time;
			const int st = min_t - manu_time - (item.stepX[j + 1] ? combi_time : 0);
			occs[sr].push_back({st, et, order.i});
		}
		state.pena += combi_time * item.cx;
		if (min_t > order.d) {
			state.pena += (ll)ceil(order.pena_ratio * (min_t - order.d));
		}
		if (state.pena >= best_pena) break;
	}
	return state;
}

ll calc_penalty(const ResultMain& resm, ll best_pena) {
	ll pena = 0;
	static vi resm_pos(MM);
	resm_pos.assign(MM, 0);
	clear_occs();
	int prev_mr = -1;
	array<int, 9> min_sub_assgin;
	array<int, 9> tmp_sub_assign;
	vi last_et(MM);
	for (int i = 0; i < R; ++i) {
		int min_m = 0;
		int min_t = INF;
		bool stick = false;
		if (i > 0) {
			if (resm_pos[prev_mr] < resm.m2o[prev_mr].size()) {
				const int noi = resm.m2o[prev_mr][resm_pos[prev_mr]];
				stick = orders[noi].e <= occs[prev_mr].back().et && orders[noi].i == occs[prev_mr].back().iid;
			}
		}
		if (stick) {
			const int oi = resm.m2o[prev_mr][resm_pos[prev_mr]];
			const Order& order = orders[oi];
			const Item& item = items[order.i];
			const Occupation prev = occs[prev_mr].back();
			const int manu_time = caps[order.i][prev_mr] * order.q;
			int start_time = max(order.e, prev.et);
			int end_time = start_time + manu_time;
			for (int k = 0; k < item.ss - 1; ++k) {
				if (!item.stepY[k + 1]) {
					min_sub_assgin[k] = -1;
					continue;
				}
				// if (max(order.e, occs[min_sub_assgin[k]].back().et) + manu_time <= end_time) {
				// 	continue;
				// }
				const vector<SubResource>& sms = srs[order.i][k];
				int min_sub_time = INF;
				for (int l = 0; l < sms.size(); ++l) {
					int sm = sms[l].m;
					int et = max(order.e, occs[sm].back().et);
					et += manu_time;
					if (min_sub_time > end_time && et < min_sub_time) {
						min_sub_time = et;
						min_sub_assgin[k] = sm;
						if (min_sub_time <= end_time) break;
					}
				}
				if (min_sub_time > end_time) {
					end_time = min_sub_time;
				}
			}
			min_t = end_time;
			min_m = prev_mr;
		} else {
			for (int j = 0; j < MM; ++j) {
				if (resm_pos[j] == resm.m2o[j].size()) continue;
				if (last_et[j] >= min_t) continue;
				const int oi = resm.m2o[j][resm_pos[j]];
				const Order& order = orders[oi];
				const Item& item = items[order.i];
				const Occupation prev = occs[j].back();
				const int combi_time = item.cx ? combi[j][prev.iid][order.i] : 0;
				const int manu_time = caps[order.i][j] * order.q;
				int start_time = max(order.e, prev.et);
				int end_time = start_time + (item.stepX[0] ? combi_time : 0) + manu_time;
				for (int k = 0; k < item.ss - 1 && end_time < min_t; ++k) {
					if (combi_time == 0 && !item.stepY[k + 1]) {
						tmp_sub_assign[k] = -1;
						continue;
					}
					const vector<SubResource>& sms = srs[order.i][k];
					int min_sub_time = INF;
					for (int l = 0; l < sms.size(); ++l) {
						int sm = sms[l].m;
						int et = max(order.e, occs[sm].back().et);
						if (item.stepX[k + 1]) {
							et += combi_time;
						}
						et += manu_time;
						if (min_sub_time > end_time && et < min_sub_time) {
							min_sub_time = et;
							tmp_sub_assign[k] = sm;
							if (min_sub_time <= end_time) break;
						}
					}
					if (min_sub_time > end_time) {
						end_time = min_sub_time;
					}
				}
				last_et[j] = end_time;
				if (end_time < min_t) {
					min_t = end_time;
					min_m = j;
					swap(tmp_sub_assign, min_sub_assgin);
					// copy(tmp_sub_assign.begin(), tmp_sub_assign.begin() + item.ss - 1, min_sub_assgin.begin());
				}
			}
		}
		last_et[min_m] = min_t;
		const int oi = resm.m2o[min_m][resm_pos[min_m]];
		const Order& order = orders[oi];
		const Item& item = items[order.i];
		resm_pos[min_m]++;
		prev_mr = min_m;
		const int combi_time = item.cx ? combi[min_m][occs[min_m].back().iid][order.i] : 0;
		const int manu_time = caps[order.i][min_m] * order.q;
		const int st = min_t - manu_time - (item.stepX[0] ? combi_time : 0);
		occs[min_m].push_back({st, min_t, order.i});
		for (int j = 0; j < item.ss - 1; ++j) {
			const int sr = min_sub_assgin[j];
			if (sr == -1) continue;
			const int et = item.stepY[j + 1] ? min_t : min_t - manu_time;
			const int st = min_t - manu_time - (item.stepX[j + 1] ? combi_time : 0);
			occs[sr].push_back({st, et, order.i});
		}
		pena += combi_time * item.cx;
		if (min_t > order.d) {
			pena += (ll)ceil(order.pena_ratio * (min_t - order.d));
		}
		if (pena >= best_pena) break;
	}
	return pena;
}

struct Block {
	int mr;
	vi ois;
	vi sr;
	int iid;
	int st, et;
};

ll calc_penalty(const vector<Block*>& blocks, ll best_pena) {
	ll pena = 0;
	clear_occs();
	for (int i = 0; i < blocks.size(); ++i) {
		const Block& b = *blocks[i];
		const Item& item = items[b.iid];
		const Occupation prev = occs[b.mr].back();
		const int combi_time = item.cx ? combi[b.mr][prev.iid][b.iid] : 0;
		int start_time = 0;
		int end_time = 0;
		{
			const Order& order = orders[b.ois[0]];
			const int manu_time = caps[b.iid][b.mr] * order.q;
			end_time = max(order.e, prev.et) + (item.stepX[0] ? combi_time : 0) + manu_time;
			for (int k = 0; k < item.ss - 1; ++k) {
				if (combi_time == 0 && !item.stepY[k + 1]) {
					continue;
				}
				int sm = b.sr[k];
				int et = max(order.e, occs[sm].back().et);
				if (item.stepX[k + 1]) {
					et += combi_time;
				}
				et += manu_time;
				if (et > end_time) {
					end_time = et;
				}
			}
			start_time = end_time - manu_time - combi_time;
			if (end_time > order.d) {
				pena += (ll)ceil(order.pena_ratio * (end_time - order.d));
			}
		}
		for (int j = 1; j < b.ois.size(); ++j) {
			const Order& order = orders[b.ois[j]];
			const int manu_time = caps[b.iid][b.mr] * order.q;
			end_time = max(order.e, end_time) + manu_time;
			if (end_time > order.d) {
				pena += (ll)ceil(order.pena_ratio * (end_time - order.d));
			}
		}
		pena += combi_time * item.cx;
		occs[b.mr].push_back({0, end_time, b.iid});
		for (int k = 0; k < item.ss - 1; ++k) {
			if (combi_time == 0 && !item.stepY[k + 1]) {
				continue;
			}
			int sm = b.sr[k];
			int et = item.stepY[k + 1] ? end_time : start_time + combi_time;
			occs[sm].push_back({0, et, b.iid});
		}
		if (pena > best_pena + pena_diff_cutoff) break;
	}
	return pena;
}

State improve_state(State& state) {
	vector<Block> blocks;
	clear_occs();
	int start_i = 0;
	int start_st = 0;
	const int MAX_BLOCK_SIZE = max(2, I / 100);
	for (int i = 0; i < R; ++i) {
		const int oi = state.ois[i];
		const int mr = state.o2mr[oi];
		const vi& osrs = state.o2srs[oi];
		const int poi = state.ois[start_i];
		const bool next_block = i >= start_i + MAX_BLOCK_SIZE || mr != state.o2mr[poi] || osrs != state.o2srs[poi] || orders[oi].i != orders[poi].i;
		if (next_block) {
			Block block;
			block.mr = state.o2mr[poi];
			block.sr = state.o2srs[poi];
			block.iid = orders[poi].i;
			block.ois.assign(state.ois.begin() + start_i, state.ois.begin() + i);
			block.st = start_st;
			block.et = occs[block.mr].back().et;
			for (int j = 0; j < items[block.iid].ss - 1; ++j) {
				if (block.sr[j] == -1) {
					block.sr[j] = srs[block.iid][j][0].m;
				}
			}
			blocks.push_back(block);
			start_i = i;
		}
		const Order& order = orders[oi];
		const Item& item = items[order.i];
		const Occupation prev = occs[mr].back();
		const int combi_time = item.cx ? combi[mr][prev.iid][order.i] : 0;
		const int manu_time = caps[order.i][mr] * order.q;
		int start_time = max(order.e, prev.et);
		int end_time = start_time + (item.stepX[0] ? combi_time : 0) + manu_time;
		// debug("%d %d %d %d\n", start_time, end_time, combi_time, manu_time);
		for (int k = 0; k < item.ss - 1; ++k) {
			int sr = osrs[k];
			if (sr == -1) continue;
			int et = max(order.e, occs[sr].back().et);
			if (item.stepX[k + 1]) {
				et += combi_time;
			}
			et += manu_time;
			if (end_time < et) {
				end_time = et;
			}
		}
		occs[mr].push_back({0, end_time, order.i}); // 0: dummy
		for (int k = 0; k < item.ss - 1; ++k) {
			if (osrs[k] == -1) continue;
			const int et = item.stepY[k + 1] ? end_time : end_time - manu_time;
			occs[osrs[k]].push_back({0, et, order.i}); // 0: dummy
		}
		if (i == 0 || next_block) {
			start_st = end_time - combi_time - manu_time;
		}
	}
	{
		const int poi = state.ois[start_i];
		Block block;
		block.mr = state.o2mr[poi];
		block.sr = state.o2srs[poi];
		block.iid = orders[poi].i;
		block.ois.assign(state.ois.begin() + start_i, state.ois.end());
		block.st = start_st;
		block.et = occs[block.mr].back().et;
		for (int j = 0; j < items[block.iid].ss - 1; ++j) {
			if (block.sr[j] == -1) {
				block.sr[j] = srs[block.iid][j][0].m;
			}
		}
		blocks.push_back(block);
	}
	// for (int i = 0; i < blocks.size(); ++i) {
	// 	const Block& b = blocks[i];
	// 	debug("i:%d mr:%d sr1:%d iid:%d size:%lu st:%d et:%d\n", i, b.mr, b.sr.empty() ? -1 : b.sr[0], b.iid, b.ois.size(), b.st, b.et);
	// }
	ll best_pena = state.pena;
	debug("pena:%lld turn:%d\n", best_pena, 0);
	vector<Block*> bps;
	for (int i = 0; i < blocks.size(); ++i) {
		bps.push_back(&blocks[i]);;
	}
	const double initial_cooler = 0.0001;
	const double final_cooler = 0.01;
	const double c0 = log(initial_cooler);
	const double c1 = log(final_cooler);
	sa_cooler = initial_cooler;
	pena_diff_cutoff = (ll)ceil(10 / sa_cooler);
	ll min_pena = best_pena;
	const ll before_time = get_elapsed_msec();
	for (int turn = 1; ; ++turn) {
		if ((turn & 0x7F) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed > TL || best_pena == 0) {
				debug("pena:%lld turn:%d\n", best_pena, turn);
				break;
			}
			const double ratio = 1.0 * (elapsed - before_time) / (TL - before_time);
			sa_cooler = exp(c1 * ratio + c0 * (1 - ratio));
			pena_diff_cutoff = (ll)ceil(10 / sa_cooler);
			// debug("cooler:%.8f pena:%lld turn:%d\n", sa_cooler, best_pena, turn);
		}
		const int bp = rnd.nextUInt(bps.size());
		Block& b = *bps[bp];
		const vector<MainResource>& b_mrs = mrs[b.iid];
		if (b_mrs.size() > 1 && (rnd.nextUInt() & 1) == 0) {
			int nmr = b_mrs[rnd.nextUInt(b_mrs.size() - 1)].m;
			if (nmr == b.mr) nmr = b_mrs.back().m;
			int omr = b.mr;
			b.mr = nmr;
			if ((rnd.nextUInt() & 1) == 0) {
				ll pena = calc_penalty(bps, best_pena);
				if (accept(pena - best_pena)) {
					best_pena = pena;
					if (pena < min_pena) {
						min_pena = pena;
						debug("pena:%lld 1-1 at turn:%d\n", best_pena, turn);
					}
				} else {
					b.mr = omr;
				}
			} else {
				vi osr = b.sr;
				const Item& item = items[b.iid];
				for (int i = 0; i < item.ss - 1; ++i) {
					const vector<SubResource>& b_srs = srs[b.iid][i];
					if (b_srs.size() > 1) {
						b.sr[i] = b_srs[rnd.nextUInt(b_srs.size())].m;
					}
				}
				ll pena = calc_penalty(bps, best_pena);
				if (accept(pena - best_pena)) {
					best_pena = pena;
					if (pena < min_pena) {
						min_pena = pena;
						debug("pena:%lld 1-2 at turn:%d\n", best_pena, turn);
					}
				} else {
					b.mr = omr;
					b.sr = osr;
				}
			}
		} else if ((rnd.nextUInt() & 1) != 0) {
			vi osr = b.sr;
			const Item& item = items[b.iid];
			for (int i = 0; i < item.ss - 1; ++i) {
				const vector<SubResource>& b_srs = srs[b.iid][i];
				if (b_srs.size() > 1) {
					b.sr[i] = b_srs[rnd.nextUInt(b_srs.size())].m;
				}
			}
			ll pena = calc_penalty(bps, best_pena);
			if (accept(pena - best_pena)) {
				best_pena = pena;
				if (pena < min_pena) {
					min_pena = pena;
					debug("pena:%lld 2 at turn:%d\n", best_pena, turn);
				}
			} else {
				b.sr = osr;
			}
		} else {
			int move = (rnd.nextUInt() & 31) - 15;
			if (move >= 0) move++;
			int np = bp + move;
			if (np < 0) np = 0;
			if (np >= bps.size()) np = bps.size() - 1;
			if (np == bp) continue;
			if (np < bp) {
				rotate(bps.begin() + np, bps.begin() + bp, bps.begin() + bp + 1);
			} else {
				rotate(bps.begin() + bp, bps.begin() + bp + 1, bps.begin() + np + 1);
			}
			ll pena = calc_penalty(bps, best_pena);
			if (accept(pena - best_pena)) {
				best_pena = pena;
				if (pena < min_pena) {
					min_pena = pena;
					debug("pena:%lld 3 at turn:%d\n", best_pena, turn);
				}
			} else {
				if (np < bp) {
					rotate(bps.begin() + np, bps.begin() + np + 1, bps.begin() + bp + 1);
				} else {
					rotate(bps.begin() + bp, bps.begin() + np, bps.begin() + np + 1);
				}
			}
		}
	}

	State ret;
	ret.pena = best_pena;
	ret.o2mr.resize(R);
	ret.o2srs.resize(R);
	for (const Block* b : bps) {
		ret.ois.insert(ret.ois.end(), b->ois.begin(), b->ois.end());
		for (int oi : b->ois) {
			ret.o2mr[oi] = b->mr;
			ret.o2srs[oi] = b->sr;
		}
	}

	return ret;
}

Result create_result(State& state) {
	Result res;
	res.answers.resize(R);
	res.pena = 0;
	clear_occs();
	for (int i = 0; i < R; ++i) {
		const int oi = state.ois[i];
		const int mr = state.o2mr[oi];
		vi& srs = state.o2srs[oi];
		const Order& order = orders[oi];
		const Item& item = items[order.i];
		const Occupation prev = occs[mr].back();
		// debug("%d %d %d %d %d\n", i, oi, mr, prev.iid, order.i);
		const int combi_time = item.cx ? combi[mr][prev.iid][order.i] : 0;
		const int manu_time = caps[order.i][mr] * order.q;
		int start_time = max(order.e, prev.et);
		int end_time = start_time + (item.stepX[0] ? combi_time : 0) + manu_time;
		// debug("%d %d %d %d\n", start_time, end_time, combi_time, manu_time);
		for (int k = 0; k < item.ss - 1; ++k) {
			int sr = srs[k];
			if (sr == -1) continue;
			if (combi_time == 0 && !item.stepY[k + 1]) {
				srs[k] = -1;
				continue;
			}
			int et = max(order.e, occs[sr].back().et);
			if (item.stepX[k + 1]) {
				et += combi_time;
			}
			et += manu_time;
			if (end_time < et) {
				end_time = et;
			}
		}
		occs[mr].push_back({0, end_time, order.i}); // 0: dummy
		for (int k = 0; k < item.ss - 1; ++k) {
			if (srs[k] == -1) continue;
			const int et = item.stepY[k + 1] ? end_time : end_time - manu_time;
			occs[srs[k]].push_back({0, et, order.i}); // 0: dummy
		}
		OrderAnswer& ans = res.answers[oi];
		ans.assign[0] = mr;
		copy(srs.begin(), srs.end(), ans.assign.begin() + 1);
		ans.t3 = end_time;
		ans.t2 = end_time - manu_time;
		ans.t1 = ans.t2 - combi_time;
		res.pena += combi_time * item.cx;
		if (end_time > order.d) {
			res.pena += (ll)ceil(order.pena_ratio * (end_time - order.d));
		}
	}
	return res;
}

Result solve() {
	bool sub_y = false;
	for (int i = 0; i < I; ++i) {
		for (int j = 1; j < items[i].ss; ++j) {
			sub_y |= items[i].stepY[j];
		}
	}
	if (!sub_y) {
		State best_state;
		best_state.pena = 1ll << 60;
		vi combi_pena_ratios = {100, 300, 600, 1000};
		// vi combi_pena_ratios = {1, 1, 1, 1};
		const ll before_time = get_elapsed_msec();
		for (int i = 0; i < combi_pena_ratios.size(); ++i) {
			delay_pena_always = (i & 1) == 0;
			COMBI_PENA_RATIO = combi_pena_ratios[i];
			const ll limit = (TL - before_time) * (i + 1) / combi_pena_ratios.size() + before_time;
			ResultMain resm = solve_only_main(limit);
			State state = create_state(resm, best_state.pena);
			debug("try:%d pena:%lld\n", i, state.pena);
			if (state.pena < best_state.pena) {
				swap(state, best_state);
			}
		}
		Result result = create_result(best_state);
		return result;
	} else {
		COMBI_PENA_RATIO = 1000;
		State best_state;
		best_state.pena = 1ll << 60;
		const int NUM_PART = 2;
		for (int part = 0; part < NUM_PART; ++part) {
			delay_pena_always = (part & 1) == 0;
			ll tl = (TL - 1000) * (part + 1) / NUM_PART;
			ResultMain resm = solve_only_main(tl - 100);
			vi ch_ss_idx(I);
			vector<pair<int, int>> ch_ss_pos(I);
			ll best_pena = 1ll << 60;
			for (int turn = 1; ; ++turn) {
				if ((turn & 7) == 0 && get_elapsed_msec() >= tl) {
					debug("turn:%d pena:%lld\n", turn, best_pena);
					break;
				}
				for (int i = 0; i < I; ++i) {
					if (items[i].ss == 1) continue;
					ch_ss_idx[i] = rnd.nextUInt(items[i].ss - 1);
					vector<SubResource>& sr_l = srs[i][ch_ss_idx[i]];
					ch_ss_pos[i].first = rnd.nextUInt(sr_l.size());
					ch_ss_pos[i].second = rnd.nextUInt(sr_l.size());
					swap(sr_l[ch_ss_pos[i].first], sr_l[ch_ss_pos[i].second]);
				}
				ll pena = calc_penalty(resm, best_pena);
				// State state = create_state(resm, part_best_state.pena);
				if (pena < best_pena) {
					debug("pena:%lld at turn:%d\n", pena, turn);
					best_pena = pena;
				} else {
					for (int i = 0; i < I; ++i) {
						if (items[i].ss == 1) continue;
						vector<SubResource>& sr_l = srs[i][ch_ss_idx[i]];
						swap(sr_l[ch_ss_pos[i].first], sr_l[ch_ss_pos[i].second]);
					}
				}
			}
			if (best_pena < best_state.pena) {
				best_state = create_state(resm, 1ll << 60);
			}
		}
		State final_state = improve_state(best_state);
		Result result = create_result(final_state);
		debug("final pena:%lld\n", result.pena);
		return result;
	}
}

int main() {
	start_time = get_elapsed_msec();
	char dummy[10];
	int di;
	int BL, CL;
	scanf("%s %d %d %d %d %d %d", dummy, &I, &M, &MM, &BL, &CL, &R);
	items.resize(I);
	mrs.resize(I);
	srs.resize(I);
	for (int i = 0; i < I; ++i) {
		scanf("%s %d %d", dummy, &di, &items[i].ss);
		items[i].stepX.resize(items[i].ss);
		items[i].stepY.resize(items[i].ss);
		srs[i].resize(items[i].ss);
	}
	for (int i = 0; i < BL; ++i) {
		int item, s, m, c, x, y;
		scanf("%s %d %d %d %d %d %d", dummy, &item, &s, &m, &c, &x, &y);
		items[item].stepX[s] = x;
		items[item].stepY[s] = y;
		if (s == 0) {
			mrs[item].push_back({m});
			caps[item][m] = c;
		} else {
			srs[item][s - 1].push_back({m});
		}
	}
	for (int i = 0; i < I; ++i) {
		items[i].cx = count(items[i].stepX.begin(), items[i].stepX.end(), true);
	}
	for (int i = 0; i < CL; ++i) {
		int m, pre, next, t;
		scanf("%s %d %d %d %d", dummy, &m, &pre, &next, &t);
		combi[m][pre][next] = t;
	}
	orders.resize(R + 1);
	for (int i = 0; i < R; ++i) {
		Order& o = orders[i];
		int pr;
		scanf("%s %d %d %d %d %d %d %d", dummy, &di, &o.i, &o.e, &o.d, &o.q, &pr, &o.a);
		o.pena_ratio = 1.0 * pr / o.a;
	}
	orders[R].i = I; // dummy
	const Result res = solve();
	// debug("pena:%lld\n", res.pena);
	for (int i = 0; i < res.answers.size(); ++i) {
		int s = items[orders[i].i].ss;
		const OrderAnswer& ans = res.answers[i];
		printf("%d %d %d %d", i, ans.t1, ans.t2, ans.t3);
		for (int j = 0; j < s; ++j) {
			printf(" %d", ans.assign[j]);
		}
		printf("\n");
	}
}
