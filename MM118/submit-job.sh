#!/bin/sh -eux

SOLVER_ID=$1
rm solver.zip && zip -r solver.zip DanceFloor.cpp Tester.java com/ run.sh
aws s3 cp solver.zip s3://marathon-tester/118/$SOLVER_ID/solver.zip 
ruby submit-job.rb $SOLVER_ID