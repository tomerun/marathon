#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <bitset>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
#define MEASURE_TIME
#define DEBUG
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 2.2e9;
constexpr ll TL = 2000;
#else
#ifdef TESTER
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 5000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 9600;
#endif
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

constexpr array<int, 8> DR = {-1, 0, 1, 0, -1, 0, 1, 0};
constexpr array<int, 8> DC = {0, 1, 0, -1, 0, 1, 0, -1};
constexpr array<int, 8> DPOS = {-64, 1, 64, -1, -64, 1, 64, -1};
constexpr char DIR_CHARS[6] = "URDL-";
using Dir = int;
constexpr Dir UP = 0;
constexpr Dir RIGHT = 1;
constexpr Dir DOWN = 2;
constexpr Dir LEFT = 3;
constexpr Dir STAY = 4;
array<array<int, 5>, 6> SCORE;
double INITIAL_COOLER;
double FINAL_COOLER;

int N, C, D, S, M;
double difficulty;

struct Constraint {
	int y, x, pos, t;
};

struct Result {
	int score;
	vector<vector<vector<Dir>>> moves;
};

struct CountBuf {
	array<int, 52 * 64> count;
	int turn;

	CountBuf() : turn(0) {
		for (int i = 0; i < N; ++i) {
			fill(count.begin(), count.begin() + (N + 2) * 64, 0);
		}
	}

  void clear() {
    turn++;
  }

  bool get(int pos) const {
    return count[pos] == turn;
  }

  void set(int pos) {
    count[pos] = turn;
  }
};

struct FastDeque {
	array<int, 5000> buf;
	int head, tail;
	FastDeque() : head(2500), tail(2500) {}

	void clear() {
		head = tail = 2500;
	}

	bool empty() const {
		return head == tail;
	}

	int size() const {
		return tail - head;
	}

	int pop_front() {
		return buf[head++];
	}

	int pop_back() {
		return buf[--tail];
	}

	void push_front(int v) {
		buf[--head] = v;
	}

	void push_back(int v) {
		buf[tail++] = v;
	}
};

array<array<uint8_t, 2000>, 52 * 64> tile_colors;
array<vector<Constraint>, 10> constraints;
bitset<52 * 64> visited;
CountBuf bfs_counter;
array<int, 2500> general_buf;
FastDeque deque;
array<int, 52 * 64> bonus;

void clear_visited() {
	visited.reset();
	// fill(visited.begin(), visited.begin() + N, (1ull << N) - 1);
}

inline void set_visited(int pos) {
	visited.set(pos);
}

inline bool is_visited(int pos) {
	return visited[pos];
}

inline bool inside_coord(int p) {
	return 0 < p && p <= N;
}

inline bool inside(int p) {
	return inside_coord(p >> 6) && inside_coord(p & 0x3F);
}

inline int encode(int y, int x) {
	return (y << 6) | x;
}

struct Solver {

	array<int, 52 * 64> visit_count;
	array<int, 52 * 64> con_count;
	array<uint8_t, 52 * 64> color;
	int total_value;
	vector<vector<vi>> moves; // pos[8:19] | can_remove2[4] | can_remove[3] | can_swap[2] | dir[0:1]
	vvi time_to_mi;
	double sa_cooler = INITIAL_COOLER;
	array<int, 6> cc_count = {};
	array<int, 6> cc_count_bk;

	Solver() {
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int pos = encode(i, j);
				visit_count[pos] = C;
				color[pos] = tile_colors[pos][0];
			}
		}
		moves.resize(D);
		time_to_mi.resize(D);
		for (int i = 0; i < D; ++i) {
			moves[i].resize(constraints[i].size() - 1);
			for (int j = 1; j < constraints[i].size(); ++j) {
				while (time_to_mi[i].size() < constraints[i][j].t) {
					time_to_mi[i].push_back(j - 1);
				}
			}
		}
		total_value = 0;
	}

	void load(const vector<vector<vi>> load_moves) {
		moves = load_moves;
		for (int i = 0; i < D; ++i) {
			int pos = constraints[i][0].pos;
			for (int j = 0; j < moves[i].size(); ++j) {
				for (int k = 0; k < moves[i][j].size(); ++k) {
					int dir = moves[i][j][k] & 3;
					pos += DPOS[dir];
					visit_count[pos]++;
				}
			}
		}
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int pos = encode(i, j);
				color[pos] = color_of_pos(pos);
				con_count[pos] = 0;
			}
		}
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int pos = encode(i, j);
				if (i < N && color[pos] == color[pos + 64]) {
					con_count[pos]++;
					con_count[pos + 64]++;
				}
				if (j < N && color[pos] == color[pos + 1]) {
					con_count[pos]++;
					con_count[pos + 1]++;
				}
			}
		}
	}

	void update_initial_dp(int y, int x, int pd, int& v) {
		int pos = encode(y, x);
		int old_color = color[pos];
		int new_color = tile_colors[pos][visit_count[pos] + 1];
		for (int dir = 0; dir < 4; ++dir) {
			int np = pos + DPOS[dir];
			if (!inside(np)) continue;
			int color_adj = tile_colors[np][visit_count[np] + (dir == pd)];
			if (old_color == color_adj) {
				v--;
			} else if (new_color == color_adj) {
				v++;
			}
		}
		int yd = min(y, N - 1 - y);
		if (yd < 4) {
			v += (4 - yd) * 2;
		}
		int xd = min(x, N - 1 - x);
		if (xd < 4) {
			v += (4 - xd) * 2;
		}
	};

	void create_initial_solution() {
		vector<Dir> cands;
		auto edge_add_count = [](int d){ return d >= 4 ? 0 : (4 - d) * (4 - d) * 4; };
		for (int i = 0; i < D; ++i) {
			int y = constraints[i][0].y;
			int x = constraints[i][0].x;
			for (int j = 0; j < constraints[i].size() - 1; ++j) {
				while (true) {
					cands.clear();
					int yd = constraints[i][j + 1].y;
					int xd = constraints[i][j + 1].x;
					if (xd > x) {
						cands.push_back(RIGHT);
					}
					if (xd < x) {
						cands.push_back(LEFT);
					}
					if (yd > y) {
						cands.push_back(DOWN);
					}
					if (yd < y) {
						cands.push_back(UP);
					}
					if (cands.empty()) break;
					Dir dir;
					if (cands.size() == 1) {
						dir = cands[0];
					} else {
						int dy = min(y - 1, N - y);
						int dx = min(x - 1, N - x);
						int add0 = edge_add_count(dy);
						int add1 = edge_add_count(dx);
						for (int k = 0; k < add0; ++k) {
							cands.push_back(cands[0]);
						}
						for (int k = 0; k < add1; ++k) {
							cands.push_back(cands[1]);
						}
						dy = min(yd - 1, N - yd);
						dx = min(xd - 1, N - xd);
						add0 = edge_add_count(dx);
						add1 = edge_add_count(dy);
						for (int k = 0; k < add0; ++k) {
							cands.push_back(cands[0]);
						}
						for (int k = 0; k < add1; ++k) {
							cands.push_back(cands[1]);
						}
						dir = cands[rnd.nextUInt(cands.size())];
					}
					moves[i][j].push_back(dir | (encode(y, x) << 8));
					y += DR[dir];
					x += DC[dir];
					visit_count[encode(y, x)]++;
				}
			}
		}
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int pos = encode(i, j);
				color[pos] = color_of_pos(pos);
			}
		}
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int pos = encode(i, j);
				con_count[pos] = 0;
				for (int k = 0; k < 4; ++k) {
					int ap = pos + DPOS[k];
					if (inside(ap) && color[pos] == color[ap]) {
						con_count[pos]++;
					}
				}
			}
		}
		if (M >= 2) {
			vvi dp(N + 1, vi(N + 1, 0));
			for (int turn = 0; turn < 2000; ++turn) {
				uint32_t ri = rnd.nextUInt();
				const int dancer = ri % D;
				ri /= D;
				uint32_t si = ri % S;
				const int mi = time_to_mi[dancer][si];
				int sy = constraints[dancer][mi].y;
				int sx = constraints[dancer][mi].x;
				int gy = constraints[dancer][mi + 1].y;
				int gx = constraints[dancer][mi + 1].x;
				if (sy == gy || sx == gx) continue;
				vi& dirs = moves[dancer][mi];
				for (int i = 0; i < dirs.size(); ++i) {
					update(dirs[i] >> 8, -1);
				}
				update(encode(gy, gx), -1);
				bool reverse = sy > gy;
				if (reverse) {
					swap(sy, gy);
					swap(sx, gx);
				}
				dp[sy][sx] = 1000;
				if (sx < gx) {
					for (int x = sx + 1; x <= gx; ++x) {
						dp[sy][x] = dp[sy][x - 1] & 0xFFFF;
						update_initial_dp(sy, x, LEFT, dp[sy][x]);
						dp[sy][x] |= LEFT << 16;
					}
					for (int y = sy + 1; y <= gy; ++y) {
						dp[y][sx] = dp[y - 1][sx] & 0xFFFF;
						update_initial_dp(y, sx, UP, dp[y][sx]);
						dp[y][sx] |= UP << 16;
					}
					for (int y = sy + 1; y <= gy; ++y) {
						for (int x = sx + 1; x <= gx; ++x) {
							int vl = dp[y][x - 1] & 0xFFFF;
							update_initial_dp(y, x, LEFT, vl);
							int vu = dp[y - 1][x] & 0xFFFF;
							update_initial_dp(y, x, UP, vu);
							if (vl < vu) {
								dp[y][x] = (UP << 16) | vu;
							} else {
								dp[y][x] = (LEFT << 16) | vl;
							}
						}
					}
				} else {
					for (int x = sx - 1; x >= gx; --x) {
						dp[sy][x] = dp[sy][x + 1] & 0xFFFF;
						update_initial_dp(sy, x, RIGHT, dp[sy][x]);
						dp[sy][x] |= RIGHT << 16;
					}
					for (int y = sy + 1; y <= gy; ++y) {
						dp[y][sx] = dp[y - 1][sx] & 0xFFFF;
						update_initial_dp(y, sx, UP, dp[y][sx]);
						dp[y][sx] |= UP << 16;
					}
					for (int y = sy + 1; y <= gy; ++y) {
						for (int x = sx - 1; x >= gx; --x) {
							int vr = dp[y][x + 1] & 0xFFFF;
							update_initial_dp(y, x, RIGHT, vr);
							int vu = dp[y - 1][x] & 0xFFFF;
							update_initial_dp(y, x, UP, vu);
							if (vr < vu) {
								dp[y][x] = (UP << 16) | vu;
							} else {
								dp[y][x] = (RIGHT << 16) | vr;
							}
						}
					}
				}
				int cp = encode(gy, gx);
				// debug("s:(%d %d) g:(%d %d) rev:%d\n", sy, sx, gy, gx, reverse);
				if (reverse) {
					update(cp, 1);
					for (int i = 0; i < dirs.size(); ++i) {
						int dir = dp[cp >> 6][cp & 0x3F] >> 16;
						dirs[i] = (cp << 8) | dir;
						cp += DPOS[dir];
						update(cp, 1);
					}
				} else {
					update(cp, 1);
					for (int i = dirs.size() - 1; i >= 0; --i) {
						int dir = dp[cp >> 6][cp & 0x3F] >> 16;
						cp += DPOS[dir];
						dirs[i] = (cp << 8) | (dir ^ 2);
						update(cp, 1);
					}
				}
				assert((dirs[0] >> 8) == constraints[dancer][mi].pos);
			}
		}
		for (int i = 0; i < D; ++i) {
			for (int j = 0; j < moves[i].size(); ++j) {
				for (int k = 0; k < moves[i][j].size(); ++k) {
					update_move_pos(moves[i][j], k);
					total_value += bonus[moves[i][j][k] >> 8];
				}
			}
		}
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				total_value += pos_value(encode(i, j));
			}
		}
		// validate();
	}

	void update_move_pos(vi& ms, int idx) {
		int pos = (ms[idx] >> 8) & 0xFFF;
		int dir1 = ms[idx] & 3;
		int dir2 = ((idx + 1) < ms.size()) ? (ms[idx + 1] & 3) : 0;
		int dir3 = ((idx + 2) < ms.size()) ? (ms[idx + 2] & 3) : 0;
		bool can_swap = idx + 1 < ms.size() && (dir1 != dir2) && inside(pos + DPOS[dir2]);
		bool can_remove = idx + 1 < ms.size() && (dir1 ^ dir2) == 2;
		bool can_remove2 = idx + 2 < ms.size() && (dir1 ^ dir3) == 2;
		ms[idx] = (ms[idx] & 0xFFFF03) | (can_swap << 2) | (can_remove << 3) | (can_remove2 << 4);
	}

	inline int color_of_pos(int pos) {
		return tile_colors[pos][visit_count[pos]];
	}

	void change_color(int pos, int diff) {
		visit_count[pos] += diff;
		color[pos] = color_of_pos(pos);
	}

	inline int pos_value(int pos) const {
		return SCORE[color[pos]][con_count[pos]];
	}

	void update(int pos, int diff) {
		int old_color = color[pos];
		visit_count[pos] += diff;
		int new_color = color_of_pos(pos);
		color[pos] = new_color;
		con_count[pos] = 0;
		for (int i = 0; i < 4; ++i) {
			int ap = pos + DPOS[i];
			if (!inside(ap)) continue;
			if (color[ap] == old_color) {
				con_count[ap]--;
			} else if (color[ap] == new_color) {
				con_count[ap]++;
				con_count[pos]++;
			}
		}
	}

	inline int eval_pos(int pos, int old_color, int new_color, int& sur) const {
		if (color[pos] == old_color) {
			return SCORE[old_color][con_count[pos] - 1] - SCORE[old_color][con_count[pos]];
		} else if (color[pos] == new_color) {
			sur++;
			return SCORE[new_color][con_count[pos] + 1] - SCORE[new_color][con_count[pos]];
		} else {
			return 0;
		}
	}

	inline int eval_pos_dual(int pos, int old_color1, int new_color1, int& sur1, int old_color2, int new_color2, int& sur2) const {
		int new_con_count = con_count[pos];
		if (color[pos] == old_color1) {
			new_con_count--;
		} else if (color[pos] == new_color1) {
			sur1++;
			new_con_count++;
		}
		if (color[pos] == old_color2) {
			new_con_count--;
		} else if (color[pos] == new_color2) {
			sur2++;
			new_con_count++;
		}
		return SCORE[color[pos]][new_con_count] - SCORE[color[pos]][con_count[pos]];
	}

	template<int diff>
	int eval_horz(int pos) const {
		const int old_color1 = color[pos];
		const int old_color2 = color[pos + 1];
		const int new_color1 = tile_colors[pos][visit_count[pos] + diff];
		const int new_color2 = tile_colors[pos + 1][visit_count[pos + 1] + diff];
		int ret = -pos_value(pos) - pos_value(pos + 1);
		ret += bonus[pos] * diff + bonus[pos + 1] * diff;
		int sur1 = new_color1 == new_color2;
		int sur2 = sur1;
		if ((pos >> 6) > 1) {
			ret += eval_pos(pos - 64, old_color1, new_color1, sur1);
			ret += eval_pos(pos - 64 + 1, old_color2, new_color2, sur2);
		}
		if ((pos & 0x3F) > 1) {
			ret += eval_pos(pos - 1, old_color1, new_color1, sur1);
		}
		if ((pos & 0x3F) + 2 <= N) {
			ret += eval_pos(pos + 2, old_color2, new_color2, sur2);
		}
		if ((pos >> 6) + 1 <= N) {
			ret += eval_pos(pos + 64, old_color1, new_color1, sur1);
			ret += eval_pos(pos + 64 + 1, old_color2, new_color2, sur2);
		}
		ret += SCORE[new_color1][sur1] + SCORE[new_color2][sur2];
		return ret;
	}

	template<int diff>
	int eval_vert(int pos) const {
		const int old_color1 = color[pos];
		const int old_color2 = color[pos + 64];
		const int new_color1 = tile_colors[pos][visit_count[pos] + diff];
		const int new_color2 = tile_colors[pos + 64][visit_count[pos + 64] + diff];
		int ret = -pos_value(pos) - pos_value(pos + 64);
		ret += bonus[pos] * diff + bonus[pos + 64] * diff;
		int sur1 = new_color1 == new_color2;
		int sur2 = sur1;
		if ((pos >> 6) > 1) {
			ret += eval_pos(pos - 64, old_color1, new_color1, sur1);
		}
		if ((pos & 0x3F) > 1) {
			ret += eval_pos(pos - 1, old_color1, new_color1, sur1);
			ret += eval_pos(pos + 64 - 1, old_color2, new_color2, sur2);
		}
		if ((pos & 0x3F) + 1 <= N) {
			ret += eval_pos(pos + 1, old_color1, new_color1, sur1);
			ret += eval_pos(pos + 64 + 1, old_color2, new_color2, sur2);
		}
		if ((pos >> 6) + 2 <= N) {
			ret += eval_pos(pos + 64 * 2, old_color2, new_color2, sur2);
		}
		ret += SCORE[new_color1][sur1] + SCORE[new_color2][sur2];
		return ret;
	}

	template<int diff>
	int eval_horz2(int pos) const {
		const int old_color1 = color[pos];
		const int old_color2 = color[pos + 2];
		const int new_color1 = tile_colors[pos][visit_count[pos] + diff];
		const int new_color2 = tile_colors[pos + 2][visit_count[pos + 2] - diff];
		int ret = -pos_value(pos) - pos_value(pos + 2);
		ret += bonus[pos] * diff - bonus[pos + 2] * diff;
		int sur1 = 0;
		int sur2 = 0;
		if ((pos >> 6) > 1) {
			ret += eval_pos(pos - 64, old_color1, new_color1, sur1);
			ret += eval_pos(pos - 64 + 2, old_color2, new_color2, sur2);
		}
		if ((pos & 0x3F) > 1) {
			ret += eval_pos(pos - 1, old_color1, new_color1, sur1);
		}
		{
			ret += eval_pos_dual(pos + 1, old_color1, new_color1, sur1, old_color2, new_color2, sur2);
		}
		if ((pos & 0x3F) + 3 <= N) {
			ret += eval_pos(pos + 3, old_color2, new_color2, sur2);
		}
		if ((pos >> 6) + 1 <= N) {
			ret += eval_pos(pos + 64, old_color1, new_color1, sur1);
			ret += eval_pos(pos + 64 + 2, old_color2, new_color2, sur2);
		}
		ret += SCORE[new_color1][sur1] + SCORE[new_color2][sur2];
		return ret;
	}

	template<int diff>
	int eval_vert2(int pos) const {
		const int old_color1 = color[pos];
		const int old_color2 = color[pos + 64 * 2];
		const int new_color1 = tile_colors[pos][visit_count[pos] + diff];
		const int new_color2 = tile_colors[pos + 64 * 2][visit_count[pos + 64 * 2] - diff];
		int ret = -pos_value(pos) - pos_value(pos + 64 * 2);
		ret += bonus[pos] * diff - bonus[pos + 64 * 2] * diff;
		int sur1 = 0;
		int sur2 = 0;
		if ((pos >> 6) > 1) {
			ret += eval_pos(pos - 64, old_color1, new_color1, sur1);
		}
		if ((pos & 0x3F) > 1) {
			ret += eval_pos(pos - 1, old_color1, new_color1, sur1);
			ret += eval_pos(pos + 64 * 2 - 1, old_color2, new_color2, sur2);
		}
		{
			ret += eval_pos_dual(pos + 64, old_color1, new_color1, sur1, old_color2, new_color2, sur2);
		}
		if ((pos & 0x3F) + 1 <= N) {
			ret += eval_pos(pos + 1, old_color1, new_color1, sur1);
			ret += eval_pos(pos + 64 * 2 + 1, old_color2, new_color2, sur2);
		}
		if ((pos >> 6) + 3 <= N) {
			ret += eval_pos(pos + 64 * 3, old_color2, new_color2, sur2);
		}
		ret += SCORE[new_color1][sur1] + SCORE[new_color2][sur2];
		return ret;
	}

	template<int diff>
	int eval_slash(int pos) const {
		const int old_color1 = color[pos];
		const int old_color2 = color[pos + 64 - 1];
		const int new_color1 = tile_colors[pos][visit_count[pos] + diff];
		const int new_color2 = tile_colors[pos + 64 - 1][visit_count[pos + 64 - 1] - diff];
		int ret = -pos_value(pos) - pos_value(pos + 64 - 1);
		ret += bonus[pos] * diff - bonus[pos + 64 - 1] * diff;
		int sur1 = 0;
		int sur2 = 0;
		if ((pos >> 6) > 1) {
			ret += eval_pos(pos - 64, old_color1, new_color1, sur1);
		}
		if ((pos & 0x3F) + 1 <= N) {
			ret += eval_pos(pos + 1, old_color1, new_color1, sur1);
		}
		{
			ret += eval_pos_dual(pos - 1, old_color1, new_color1, sur1, old_color2, new_color2, sur2);
			ret += eval_pos_dual(pos + 64, old_color1, new_color1, sur1, old_color2, new_color2, sur2);
		}
		if ((pos & 0x3F) - 2 > 0) {
			ret += eval_pos(pos + 64 - 2, old_color2, new_color2, sur2);
		}
		if ((pos >> 6) + 2 <= N) {
			ret += eval_pos(pos + 64 * 2 - 1, old_color2, new_color2, sur2);
		}
		ret += SCORE[new_color1][sur1] + SCORE[new_color2][sur2];
		return ret;
	}

	template<int diff>
	int eval_backslash(int pos) const {
		const int old_color1 = color[pos];
		const int old_color2 = color[pos + 64 + 1];
		const int new_color1 = tile_colors[pos][visit_count[pos] + diff];
		const int new_color2 = tile_colors[pos + 64 + 1][visit_count[pos + 64 + 1] - diff];
		int ret = -pos_value(pos) - pos_value(pos + 64 + 1);
		ret += bonus[pos] * diff - bonus[pos + 64 + 1] * diff;
		int sur1 = 0;
		int sur2 = 0;
		if ((pos >> 6) > 1) {
			ret += eval_pos(pos - 64, old_color1, new_color1, sur1);
		}
		if ((pos & 0x3F) > 1) {
			ret += eval_pos(pos - 1, old_color1, new_color1, sur1);
		}
		{
			ret += eval_pos_dual(pos + 1, old_color1, new_color1, sur1, old_color2, new_color2, sur2);
			ret += eval_pos_dual(pos + 64, old_color1, new_color1, sur1, old_color2, new_color2, sur2);
		}
		if ((pos & 0x3F) + 2 <= N) {
			ret += eval_pos(pos + 64 + 2, old_color2, new_color2, sur2);
		}
		if ((pos >> 6) + 2 <= N) {
			ret += eval_pos(pos + 64 * 2 + 1, old_color2, new_color2, sur2);
		}
		ret += SCORE[new_color1][sur1] + SCORE[new_color2][sur2];
		return ret;
	}

	int count_connectivity(int pos, int dir, int cc) {
		bfs_counter.clear();
		bfs_counter.set(pos);
		int y = pos >> 6;
		int x = pos & 0x3F;
		int ret = 0;
		while (dir) {
			ret++;
			int d1 = __builtin_ctz(dir);
			dir &= dir - 1;
			deque.clear();
			deque.push_back(pos + DPOS[d1]);
			bfs_counter.set(pos + DPOS[d1]);
			while (!deque.empty() && dir) {
				int cp = deque.pop_front();
				for (int i = 0; i < 4; ++i) {
					int np = cp + DPOS[i];
					if (!inside(np)) continue;
					if (color[np] != cc) continue;
					if (bfs_counter.get(np)) continue;
					if (abs((np >> 6) - y) + abs((np & 0x3F) - x) == 1) {
						for (int j = 0; j < 4; ++j) {
							if (pos + DPOS[j] == np) {
								dir -= (1 << j);
								break;
							}
						}
					}
					bfs_counter.set(np);
					deque.push_back(np);
					// if (DR[i] * (y - cy) + DC[i] * (x - cx) > 0) {
					// 	deque.push_front((ny << 8) | nx);
					// } else {
					// 	deque.push_back((ny << 8) | nx);
					// }
				}
			}
		}
		return ret;
	}

	void eval_score_change(int pos, int d) {
		int old_color = color[pos];
		visit_count[pos] += d;
		int new_color = color[pos] = color_of_pos(pos);
		int old_dir = 0;
		int new_dir = 0;
		for (int i = 0; i < 4; ++i) {
			int np = pos + DPOS[i];
			if (inside(np)){
				if (color[np] == old_color) {
					old_dir |= 1 << i;
				} else if (color[np] == new_color) {
					new_dir |= 1 << i;
				}
			}
		}
		if (old_dir == 0) {
			cc_count[old_color]--;
		} else if ((old_dir & (old_dir - 1)) != 0) {
			cc_count[old_color] += count_connectivity(pos, old_dir, old_color) - 1;
		}
		if (new_dir == 0) {
			cc_count[new_color]++;
		} else if ((new_dir & (new_dir - 1)) != 0) {
			cc_count[new_color] -= count_connectivity(pos, new_dir, new_color) - 1;
		}
	}

	int eval_score_changes(int pos1, int d1, int pos2, int d2) {
		copy(cc_count.begin(), cc_count.begin() + C, cc_count_bk.begin());
		eval_score_change(pos1, d1);
		eval_score_change(pos2, d2);
		int ret = 0;
		for (int i = 0; i < C; ++i) {
			ret += cc_count[i] * cc_count[i];
		}
		// assert(ret == calc_score());
		return ret;
	}

	int calc_score() {
		fill(cc_count.begin(), cc_count.begin() + C, 0);
		clear_visited();
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int pos = encode(i, j);
				if (is_visited(pos)) continue;
				cc_count[color[pos]]++;
				set_visited(pos);
				general_buf[0] = pos;
				int size = 1;
				for (int k = 0; k < size; ++k) {
					int cp = general_buf[k];
					for (int dir = 0; dir < 4; ++dir) {
						int np = cp + DPOS[dir];
						if (inside(np) && !is_visited(np) && color[cp] == color[np]) {
							set_visited(np);
							general_buf[size++] = np;
						}
					}
				}
			}
		}
		int ret = 0;
		for (int i = 0; i < C; ++i) {
			ret += cc_count[i] * cc_count[i];
		}
		return ret;
	}

	bool accept(int diff) {
		if (diff >= 0) return true;
		double v = diff * sa_cooler;
		if (v < -6) return false;
		return rnd.nextDouble() < exp(v);
	}

	void improve_rough() {
		const int LOOP = difficulty < 50 ? 2000000 : difficulty < 200 ? 6000000 : difficulty < 1000 ? 8000000 : 18000000;
		debug("total_value:%d -> ", total_value);
		for (int turn = 0; turn < LOOP; ++turn) {
			if ((turn & 0x3FFF) == 0) {
				double c0 = log(INITIAL_COOLER);
				double c1 = log(FINAL_COOLER);
				sa_cooler = exp((c1 * turn + c0 * (LOOP - turn)) / LOOP);
				if ((turn & 0xFFFF) == 0 && get_elapsed_msec() > TL) {
					break;
				}
			}
			uint32_t ri = rnd.nextUInt();
			const int dancer = ri % D;
			ri /= D;
			uint32_t si = ri % S;
			const int mi = time_to_mi[dancer][si];
			vi& dirs = moves[dancer][mi];
			si -= constraints[dancer][mi].t;
			const int idx = si < dirs.size() ? si : rnd.nextUInt(dirs.size());
			const bool can_add = dirs.size() + 2 <= constraints[dancer][mi + 1].t - constraints[dancer][mi].t;
			const int can_something = (dirs[idx] >> 2) & 7;
			if (!can_add && !can_something) continue;
			int cp = dirs[idx] >> 8;
			if (can_something & 2) { // remove
				Dir dir = dirs[idx] & 3;
				int diff;
				if (dir & 1) {
					diff = eval_horz<-1>(cp - (dir == LEFT));
				} else {
					diff = eval_vert<-1>(cp - (dir == UP) * 64);
				}
				if (accept(diff)) {
					dirs.erase(dirs.begin() + idx, dirs.begin() + idx + 2);
					for (int i = max(0, idx - 2); i < idx; ++i) {
						update_move_pos(dirs, i);
					}
					total_value += diff;
					update(cp, -1);
					update(cp + DPOS[dir], -1);
					// debug("REMOVE turn:%d value:%d\n", turn, total_value);
					continue;
				}
			}
			if (can_something & 4) { // remove2
				Dir dir = dirs[idx] & 3;
				Dir dir2 = dirs[idx + 1] & 3;
				int diff;
				if (dir2 & 1) {
					diff = eval_horz<-1>(cp + DPOS[dir] - (dir2 == LEFT));
				} else {
					diff = eval_vert<-1>(cp + DPOS[dir] - (dir2 == UP) * 64);
				}
				if (accept(diff)) {
					dirs[idx] = (dirs[idx] ^ dir) | dir2;
					dirs.erase(dirs.begin() + idx + 1, dirs.begin() + idx + 3);
					for (int i = max(0, idx - 2); i <= idx; ++i) {
						update_move_pos(dirs, i);
					}
					total_value += diff;
					update(cp + DPOS[dir], -1);
					update(cp + DPOS[dir] + DPOS[dir2], -1);
					// debug("REMOVE turn:%d value:%d\n", turn, total_value);
					continue;
				}
			}
			if (can_something & 1) { // swap
				Dir dir1 = dirs[idx] & 3;
				Dir dir2 = dirs[idx + 1] & 3;
				int np1 = cp + DPOS[dir1];
				int np2 = cp + DPOS[dir2];
				int ny1 = np1 >> 6;
				int nx1 = np1 & 0x3F;
				int ny2 = np2 >> 6;
				int nx2 = np2 & 0x3F;
				int diff;
				if (ny1 == ny2) {
					diff = nx1 < nx2 ? eval_horz2<-1>(np1) : eval_horz2<1>(np2);
				} else if (nx1 == nx2) {
					diff = ny1 < ny2 ? eval_vert2<-1>(np1) : eval_vert2<1>(np2);
				} else if (DR[dir1] + DR[dir2] == DC[dir1] + DC[dir2]) {
					diff = ny1 < ny2 ? eval_slash<-1>(np1) : eval_slash<1>(np2);
				} else {
					diff = ny1 < ny2 ? eval_backslash<-1>(np1) : eval_backslash<1>(np2);
				}
				if (accept(diff)) {
					dirs[idx] = (dirs[idx] ^ dir1) | dir2;
					dirs[idx + 1] = (np2 << 8) | dir1;
					for (int i = max(0, idx - 2); i <= idx + 1; ++i) {
						update_move_pos(dirs, i);
					}
					total_value += diff;
					update(np1, -1);
					update(np2, 1);
					// debug("SWAP turn:%d value:%d\n", turn, total_value);
					continue;
				}
			}
			if (can_add) {
				static vector<Dir> cand_dir;
				cand_dir.clear();
				int cy = cp >> 6;
				int cx = cp & 0x3F;
				if (cy > 1) {
					for (int i = 0; i < 4 - con_count[cp - 64]; ++i) {
						cand_dir.push_back(UP);
					}
				}
				if (cy < N) {
					for (int i = 0; i < 4 - con_count[cp + 64]; ++i) {
						cand_dir.push_back(DOWN);
				}
				}
				if (cx > 1) {
					for (int i = 0; i < 4 - con_count[cp - 1]; ++i) {
						cand_dir.push_back(LEFT);
					}
				}
				if (cx < N) {
					for (int i = 0; i < 4 - con_count[cp + 1]; ++i) {
						cand_dir.push_back(RIGHT);
					}
				}
				if (!cand_dir.empty()) {
					Dir dir = cand_dir[rnd.nextUInt(cand_dir.size())];
					int diff;
					if (dir & 1) {
						diff = eval_horz<1>(cp - (dir == LEFT));
					} else {
						diff = eval_vert<1>(cp - (dir == UP) * 64);
					}
					if (accept(diff)) {
						int new_pos1 = (cp << 8) | dir;
						int new_pos2 = ((cp + DPOS[dir]) << 8) | (dir ^ 2);
						dirs.insert(dirs.begin() + idx, {new_pos1, new_pos2});
						for (int i = max(0, idx - 2); i <= idx + 1; ++i) {
							update_move_pos(dirs, i);
						}
						total_value += diff;
						update(cp + DPOS[dir], 1);
						update(cp, 1);
						// debug("ADD turn:%d value:%d diff:%d\n", turn, total_value, diff);
						continue;
					}
				}

				int dir = dirs[idx] & 3;
				cand_dir.clear();
				if (inside(cp + DPOS[dir + 1])) {
					cand_dir.push_back(dir + 1);
				}
				if (inside(cp + DPOS[dir + 3])) {
					cand_dir.push_back(dir + 3);
				}
				assert(!cand_dir.empty());
				Dir bend_dir = cand_dir.size() == 1 ? cand_dir[0] : cand_dir[rnd.nextUInt() & 1];
				int diff;
				if (dir & 1) {
					diff = eval_horz<1>(cp + DPOS[bend_dir] - (dir == LEFT));
				} else {
					diff = eval_vert<1>(cp + DPOS[bend_dir] - (dir == UP) * 64);
				}
				if (accept(diff)) {
					bend_dir &= 3;
					dirs[idx] = (dirs[idx] ^ dir) | bend_dir;
					int new_pos1 = ((cp + DPOS[bend_dir]) << 8) | dir;
					int new_pos2 = ((cp + DPOS[bend_dir] + DPOS[dir]) << 8) | (bend_dir ^ 2);
					dirs.insert(dirs.begin() + idx + 1, {new_pos1, new_pos2});
					for (int i = max(0, idx - 2); i <= idx + 2; ++i) {
						update_move_pos(dirs, i);
					}
					total_value += diff;
					update(cp + DPOS[bend_dir], 1);
					update(cp + DPOS[bend_dir] + DPOS[dir], 1);
					// debug("ADD2 turn:%d score:%d\n", turn, next_score);
					continue;
				}
			}
		}
		debug("%d\n", total_value);
		// validate();
	}

	int improve_strict() {
		constexpr int LOOP = 50000;
		int cur_score = calc_score();
		debug("improve_strict score:%d -> ", cur_score);
		for (int turn = 0; turn < LOOP; ++turn) {
			if ((turn & 0xFFF) == 0) {
				auto elapsed = get_elapsed_msec();
				if (elapsed > TL) {
					break;
				}
			}
			improve_strict_single(cur_score);
		}
		debug("%d\n", cur_score);
		return cur_score;
	}

	int improve_strict_final() {
		int cur_score = calc_score();
		for (int turn = 0; ; ++turn) {
			if ((turn & 0x3F) == 0) {
				auto elapsed = get_elapsed_msec();
				if (elapsed > TL) {
					debug("improve_final turn:%d\n", turn);
					break;
				}
			}
			improve_strict_single(cur_score);
		}
		return cur_score;
	}

	void improve_strict_single(int& cur_score) {
		uint32_t ri = rnd.nextUInt();
		const int dancer = ri % D;
		ri /= D;
		uint32_t si = ri % S;
		const int mi = time_to_mi[dancer][si];
		vi& dirs = moves[dancer][mi];
		si -= constraints[dancer][mi].t;
		const int idx = si < dirs.size() ? si : rnd.nextUInt(dirs.size());
		const bool can_add = dirs.size() + 2 <= constraints[dancer][mi + 1].t - constraints[dancer][mi].t;
		const int can_something = (dirs[idx] >> 2) & 7;
		if (!can_add && !can_something) return;
		int cp = dirs[idx] >> 8;
		if (can_something & 2) { // remove
			Dir dir = dirs[idx] & 3;
			int next_score = eval_score_changes(cp, -1, cp + DPOS[dir], -1);
			if (next_score <= cur_score) {
				// if (next_score < cur_score) debug("REMOVE turn:%d score:%d\n", turn, next_score);
				cur_score = next_score;
				dirs.erase(dirs.begin() + idx, dirs.begin() + idx + 2);
				for (int i = max(0, idx - 2); i < idx; ++i) {
					update_move_pos(dirs, i);
				}
				return;
			} else {
				change_color(cp, 1);
				change_color(cp + DPOS[dir], 1);
				copy(cc_count_bk.begin(), cc_count_bk.begin() + C, cc_count.begin());
			}
		}
		if (can_something & 4) { // remove2
			Dir dir = dirs[idx] & 3;
			Dir dir2 = dirs[idx + 1] & 3;
			int next_score = eval_score_changes(cp + DPOS[dir], -1, cp + DPOS[dir] + DPOS[dir2], -1);
			if (next_score <= cur_score) {
				// if (next_score < cur_score) debug("REMOVE2 turn:%d score:%d\n", turn, next_score);
				cur_score = next_score;
				dirs[idx] = (dirs[idx] ^ dir) | dir2;
				dirs.erase(dirs.begin() + idx + 1, dirs.begin() + idx + 3);
				for (int i = max(0, idx - 2); i <= idx; ++i) {
					update_move_pos(dirs, i);
				}
				return;
			} else {
				change_color(cp + DPOS[dir], 1);
				change_color(cp + DPOS[dir] + DPOS[dir2], 1);
				copy(cc_count_bk.begin(), cc_count_bk.begin() + C, cc_count.begin());
			}
		}
		if (can_something & 1) { // swap
			Dir dir1 = dirs[idx] & 3;
			Dir dir2 = dirs[idx + 1] & 3;
			int next_score = eval_score_changes(cp + DPOS[dir1], -1, cp + DPOS[dir2], 1);
			if (next_score <= cur_score) {
				// if (next_score < cur_score) debug("SWAP turn:%d score:%d\n", turn, next_score);
				cur_score = next_score;
				dirs[idx] = (dirs[idx] ^ dir1) | dir2;
				dirs[idx + 1] = ((cp + DPOS[dir2]) << 8) | dir1;
				for (int i = max(0, idx - 2); i <= idx + 1; ++i) {
					update_move_pos(dirs, i);
				}
				return;
			} else {
				change_color(cp + DPOS[dir1], 1);
				change_color(cp + DPOS[dir2], -1);
				copy(cc_count_bk.begin(), cc_count_bk.begin() + C, cc_count.begin());
			}
		}
		if (can_add) {
			static vector<Dir> cand_dir;
			cand_dir.clear();
				int cy = cp >> 6;
				int cx = cp & 0x3F;
			if (cy > 1) {
				for (int i = 0; i < 5 - con_count[cp - 64]; ++i) {
					cand_dir.push_back(UP);
				}
			}
			if (cy < N) {
				for (int i = 0; i < 5 - con_count[cp + 64]; ++i) {
					cand_dir.push_back(DOWN);
			}
			}
			if (cx > 1) {
				for (int i = 0; i < 5 - con_count[cp - 1]; ++i) {
					cand_dir.push_back(LEFT);
				}
			}
			if (cx < N) {
				for (int i = 0; i < 5 - con_count[cp + 1]; ++i) {
					cand_dir.push_back(RIGHT);
				}
			}
			Dir dir = cand_dir[rnd.nextUInt(cand_dir.size())];
			int next_score = eval_score_changes(cp, 1, cp + DPOS[dir], 1);
			if (next_score < cur_score) {
				// debug("ADD turn:%d score:%d\n", turn, next_score);
				cur_score = next_score;
				int new_pos1 = (cp << 8) | dir;
				int new_pos2 = ((cp + DPOS[dir]) << 8) | (dir ^ 2);
				dirs.insert(dirs.begin() + idx, {new_pos1, new_pos2});
				for (int i = max(0, idx - 2); i <= idx + 1; ++i) {
					update_move_pos(dirs, i);
				}
				return;
			} else {
				change_color(cp, -1);
				change_color(cp + DPOS[dir], -1);
				copy(cc_count_bk.begin(), cc_count_bk.begin() + C, cc_count.begin());
			}

			dir = dirs[idx] & 3;
			cand_dir.clear();
			if (inside(cp + DPOS[dir + 1])) {
				cand_dir.push_back(dir + 1);
			}
			if (inside(cp + DPOS[dir + 3])) {
				cand_dir.push_back(dir + 3);
			}
			assert(!cand_dir.empty());
			Dir bend_dir = cand_dir.size() == 1 ? cand_dir[0] : cand_dir[rnd.nextUInt() & 1];
			next_score = eval_score_changes(cp + DPOS[bend_dir], 1,
			                                cp + DPOS[dir] + DPOS[bend_dir], 1);
			if (next_score < cur_score) {
				// debug("ADD2 turn:%d score:%d\n", turn, next_score);
				cur_score = next_score;
				bend_dir &= 3;
				dirs[idx] = (dirs[idx] ^ dir) | bend_dir;
				int new_pos1 = ((cp + DPOS[bend_dir]) << 8) | dir;
				int new_pos2 = ((cp + DPOS[bend_dir] + DPOS[dir]) << 8) | (bend_dir ^ 2);
				dirs.insert(dirs.begin() + idx + 1, {new_pos1, new_pos2});
				for (int i = max(0, idx - 2); i <= idx + 2; ++i) {
					update_move_pos(dirs, i);
				}
				return;
			} else {
				change_color(cp + DPOS[bend_dir], -1);
				change_color(cp + DPOS[dir] + DPOS[bend_dir], -1);
				copy(cc_count_bk.begin(), cc_count_bk.begin() + C, cc_count.begin());
			}
		}
	}

	Result solve() {
		START_TIMER(0);
		create_initial_solution();
		STOP_TIMER(0);
		Result res;
		START_TIMER(1);
		improve_rough();
		STOP_TIMER(1);
		START_TIMER(2);
		res.score = improve_strict();
		STOP_TIMER(2);
		res.moves = moves;
		return res; 
	}

	void validate() {
		int total = 0;
		for (int i = 1; i <= N; ++i) {
			for (int j = 1; j <= N; ++j) {
				int pos = encode(i, j);
				assert(color[pos] == tile_colors[pos][visit_count[pos]]);
				int ac = 0;
				for (int k = 0; k < 4; ++k) {
					int np = pos + DPOS[k];
					if (inside(np) && color[pos] == color[np]) ++ac;
				}
				if (ac != con_count[pos]) {
					debug("%d %d %d %d\n", i, j, ac, con_count[pos]);
				}
				assert(ac == con_count[pos]);
				total += SCORE[color[pos]][ac];
			}
		}
		for (int i = 0; i < D; ++i) {
			for (int j = 0; j < moves[i].size(); ++j) {
				for (int k = 0; k < moves[i][j].size(); ++k) {
					total += bonus[moves[i][j][k] >> 8];
				}
			}
		}
		assert(total == total_value);
	}
};

class DanceFloor {
public:
	vector<string> findSolution() {
		const array<int, 5> default_score = {
			0,
			M < 4 ? 3 : 2,
			difficulty < 200 ? 3 : difficulty < 1400 ? 4 : difficulty < 20000 ? 5 : 6,
			difficulty < 20000 ? 7 : 8,
			8
		};
		for (int i = 0; i < C; ++i) {
			SCORE[i] = default_score;
		}
		Result best_res;
		auto before_time = get_elapsed_msec();
		{
			unique_ptr<Solver> solver(new Solver());
			best_res = solver->solve();
			debug("turn:0 score:%d\n", best_res.score);
		}
		auto after_time = get_elapsed_msec();
		auto worst_time = after_time - before_time;
		for (int turn = 1; ; ++turn) {
			before_time = after_time;
			if (before_time + worst_time + 100 > TL || best_res.score == 1) {
				debug("total turn:%d score:%d worst_time:%lld\n", turn, best_res.score, worst_time);
				break;
			}
			if (difficulty <= 5000) {
				SCORE[turn % C][0] = 0;
				for (int i = 1; i <= 4; ++i) {
					SCORE[turn % C][i] = 0;
				}
			} else {
				if (N > 34) {
					SCORE[turn % C][0] = rnd.nextUInt(2);
					for (int i = 1; i <= 4; ++i) {
						SCORE[turn % C][i] = SCORE[turn % C][i] + rnd.nextUInt(3) - 1;
					}
				} else {
					for (int i = 0; i < C; ++i) {
						SCORE[i][0] = rnd.nextUInt(2);
						for (int j = 1; j <= 4; ++j) {
							SCORE[i][j] = SCORE[i][j] + rnd.nextUInt(3) - 1;
						}
					}
				}
			}
			unique_ptr<Solver> solver(new Solver());
			Result res = solver->solve();
			// for (int i = 0; i < 5; ++i) {
			// 	debug("%2d ", SCORE[turn % C][i]);
			// }
			// debug("score:%d\n", res.score);
			for (int i = 0; i < C; ++i) {
				SCORE[i] = default_score;
			}
			if (res.score < best_res.score) {
				debug("turn:%d score:%d\n", turn, res.score);
				swap(best_res, res);
			}
			after_time = get_elapsed_msec();
			debug("iter time:%lld\n", after_time - before_time);
			worst_time = max(worst_time, after_time - before_time);
		}

		if (best_res.score > 1) {
			unique_ptr<Solver> solver(new Solver());
			solver->load(best_res.moves);
			const int final_score = solver->improve_strict_final();
			if (final_score < best_res.score) {
				debug("improve_final:%d -> %d\n", best_res.score, final_score);
				best_res.score = final_score;
				best_res.moves = solver->moves;
			}
		}

		vector<vector<int>> ret_moves(D);
		for (int i = 0; i < D; ++i) {
			for (int j = 0; j < constraints[i].size() - 1; ++j) {
				for (int k = 0; k < best_res.moves[i][j].size(); ++k) {
					ret_moves[i].push_back(best_res.moves[i][j][k] & 3);
				}
				while (ret_moves[i].size() < constraints[i][j + 1].t) {
					ret_moves[i].push_back(STAY);
				}
			}
		}
		vector<string> ret(S);
		for (int i = 0; i < S; ++i) {
			for (int j = 0; j < D; ++j) {
				ret[i] += DIR_CHARS[ret_moves[j][i]];
			}
		}
		return ret;
	}
};

int main() {
	scanf("%d %d %d %d", &N, &C, &D, &S);
	start_time = get_time();
	difficulty = 1.0 * N * N * N * N * C * C / (S * D);
	if (difficulty < 6000) {
		INITIAL_COOLER = 0.35;
	} else if (difficulty < 10000) {
		INITIAL_COOLER = 0.30;
	} else if (difficulty < 22000) {
		INITIAL_COOLER = 0.25;
	} else {
		INITIAL_COOLER = 0.15;
	}
	FINAL_COOLER = 0.6;
	char buf[10];
	const int EDGE = difficulty < 700 ? 1 : 2;
	for (int y = 1; y <= N; y++) {
		for (int x = 1; x <= N; x++) {
			auto& tcs = tile_colors[encode(y, x)];
			scanf("%s", buf);
			for (int i = 0, ci = 0; i < tcs.size(); ++i) {
				tcs[i] = buf[ci] - '0';
				ci++;
				if (ci == C) ci = 0;
			}
			int yd = min(y - 1, N - y);
			int xd = min(x - 1, N - x);
			bonus[encode(y, x)] = (yd < EDGE ? (EDGE - yd) : 0) + (xd < EDGE ? (EDGE - xd) : 0);
		}
	}
	int P;
	for (int i = 0; i < D; i++) {
		scanf("%d", &P);
		constraints[i].resize(P);
		for (int j = 0; j < P; j++) {
			Constraint& c = constraints[i][j];
			scanf("%d %d %d", &c.x, &c.y, &c.t);
			c.x++;
			c.y++;
			c.pos = encode(c.y, c.x);
			if (0 < j && j < P - 1) {
				int dist = abs(c.y - constraints[i][j - 1].y) + abs(c.x - constraints[i][j - 1].x);
				M = max(M, c.t - constraints[i][j - 1].t - dist);
			}
		}
	}
	debug("N:%d C:%d D:%d S:%d M:%d difficulty:%.3f\n", N, C, D, S, M, difficulty);

	DanceFloor sol;
	vector<string> ret = sol.findSolution();

	printf("%d\n", S);
	for (int i = 0; i < ret.size(); i++) {
		printf("%s\n", ret[i].c_str());
	}
	fflush(stdout);
	PRINT_TIMER();
	PRINT_COUNTER();
}
