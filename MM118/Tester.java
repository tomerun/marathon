import com.topcoder.marathon.MarathonAnimatedVis;
import com.topcoder.marathon.MarathonController;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import java.util.List;

public class Tester extends MarathonAnimatedVis {
	//Ranges
	private static final int minSize = 8, maxSize = 50;
	private static final int minColors = 2, maxColors = 6;
	private static final int minDancers = 2, maxDancers = 10;
	private static final int minSteps = 50, maxSteps = 1500;
	private static final int minMargin = 0, maxMargin = 15;

	//Inputs
	private int size;
	private int numColors;
	private int numDancers;
	private int numSteps;
	private int margin;
	private int[][][] tileColors;
	private int[][] xChoreographies, yChoreographies, tChoreographies;

	//Painting
	private Color[] paintColors;
	private final List<List<int[]>> dancersPath = new ArrayList<List<int[]>>();
	ArrayList<Segment> segments = new ArrayList<>();
	int segmentsVisIdx;

	//State control
	private int[][] tileIdx;
	private int[][] groups;
	private int[] xDancers;
	private int[] yDancers;
	private int[] markDancers;
	private int[] cntByColor;
	private int step;

	static class Segment {
		int[] x, y;
		int maxY;
		Segment(int size) {
			this.x = new int[size];
			this.y = new int[size];
		}
	}

	protected void generate() {
		size = randomInt(minSize, maxSize);
		numColors = randomInt(minColors, maxColors);
		numDancers = randomInt(minDancers, maxDancers);
		numSteps = randomInt(minSteps, maxSteps);
		margin = randomInt(minMargin, maxMargin);

		//Special cases for seeds 1, 2 and 7
		if (seed == 1) {
			size = minSize;
			numColors = minColors;
			numDancers = minDancers;
			numSteps = minSteps;
			margin = minMargin;
		} else if (seed == 2) {
			size = maxSize;
			numColors = maxColors;
			numDancers = maxDancers;
			numSteps = maxSteps;
			margin = maxMargin;
		} else if (seed == 7) {
			size = 11;
			numSteps = 715;
			margin = 15;
		}
		if (1000 <= seed && seed < 2000) {
			numColors = (int) ((seed - 1000) * (maxColors - minColors + 1) / 1000 + minColors);
		}

		//User defined parameters
		if (parameters.isDefined("N")) size = randomInt(parameters.getIntRange("N"), minSize, maxSize);
		if (parameters.isDefined("C")) numColors = randomInt(parameters.getIntRange("C"), minColors, maxColors);
		if (parameters.isDefined("D")) numDancers = randomInt(parameters.getIntRange("D"), minDancers, maxDancers);
		if (parameters.isDefined("S")) numSteps = randomInt(parameters.getIntRange("S"), minSteps, maxSteps);
		if (parameters.isDefined("M")) margin = randomInt(parameters.getIntRange("M"), minMargin, maxMargin);

		//Generate tile colors
		tileColors = new int[size][size][numColors];
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int[] tyx = tileColors[y][x];
				boolean[] used = new boolean[numColors];
				for (int i = 0; i < numColors; ) {
					int c = rnd.nextInt(numColors);
					if (used[c]) continue;
					used[c] = true;
					tyx[i++] = c;
				}
			}
		}

		//Generate choreographies
		xChoreographies = new int[numDancers][];
		yChoreographies = new int[numDancers][];
		tChoreographies = new int[numDancers][];
		int[] xd = new int[numSteps];
		int[] yd = new int[numSteps];
		int[] td = new int[numSteps];
		for (int i = 0; i < numDancers; i++) {
			int t = 0;
			int j = 0;
			while (t < numSteps) {
				int x = rnd.nextInt(size);
				int y = rnd.nextInt(size);
				if (j > 0 && x == xd[j - 1] && y == yd[j - 1]) continue;
				xd[j] = x;
				yd[j] = y;
				if (j > 0) {
					int xPrev = xd[j - 1];
					int yPrev = yd[j - 1];
					int dist = Math.abs(xPrev - x) + Math.abs(yPrev - y);
					t = td[j] = t + randomInt(dist, dist + margin);
				}
				j++;
			}
			if (t > numSteps) {
				j--;
				td[j] = numSteps;
				while (true) {
					int x = rnd.nextInt(size);
					int y = rnd.nextInt(size);
					if (x == xd[j - 1] && y == yd[j - 1]) continue;
					xd[j] = x;
					yd[j] = y;
					int xPrev = xd[j - 1];
					int yPrev = yd[j - 1];
					int dist = Math.abs(xPrev - x) + Math.abs(yPrev - y);
					if (dist <= td[j] - td[j - 1]) break;
				}
				j++;
			}
			xChoreographies[i] = Arrays.copyOf(xd, j);
			yChoreographies[i] = Arrays.copyOf(yd, j);
			tChoreographies[i] = Arrays.copyOf(td, j);
		}

		if (debug) {
			System.out.printf("seed=%4d,N=%2d,C=%1d,D=%2d,M=%2d,S=%4d\n",
					seed, size, numColors, numDancers, margin, numSteps);
		}
	}

	protected boolean isMaximize() {
		return false;
	}

	private void init() {
		tileIdx = new int[size][size];
		groups = new int[size][size];
		xDancers = new int[numDancers];
		yDancers = new int[numDancers];
		markDancers = new int[numDancers];
		cntByColor = new int[numColors];
		for (int i = 0; i < numDancers; i++) {
			xDancers[i] = xChoreographies[i][0];
			yDancers[i] = yChoreographies[i][0];
		}

		if (hasVis()) {
			paintColors = new Color[maxColors];
			paintColors[0] = new Color(250, 20, 20);
			paintColors[1] = new Color(60, 60, 250);
			paintColors[2] = new Color(20, 220, 20);
			paintColors[3] = new Color(240, 240, 20);
			paintColors[4] = new Color(250, 20, 250);
			paintColors[5] = new Color(20, 240, 240);

			setInfoMaxDimension(18, 12 + numColors);
			setContentRect(0, 0, size, size);
			setDefaultSize(30);
//			setDefaultDelay(20);

			for (int i = 0; i < numDancers; i++) {
				List<int[]> path = new ArrayList<int[]>();
				path.add(new int[]{xDancers[i], yDancers[i]});
				dancersPath.add(path);
			}

			addInfo("Seed", seed);
			addInfoBreak();
			addInfo("Size N", size);
			addInfo("Colors C", numColors);
			addInfo("Dancers D", numDancers);
			addInfo("Margin M", margin);
			addInfoBreak();
			addInfo("Steps", numSteps);
			addInfoBreak();
			addInfo("Score", getScore());
			addInfoBreak();
			addInfo("Components");
			for (int c = 0; c < numColors; c++) {
				addInfo(paintColors[c], cntByColor[c]);
			}
			update();
			frame.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					switch (e.getKeyCode()) {
						case KeyEvent.VK_LEFT:
							if (segmentsVisIdx > 0) {
								if (e.isMetaDown()) {
									move(-segments.size());
								} else if (e.isShiftDown()) {
									move(-10);
								} else {
									move(-1);
								}
							}
							break;
						case KeyEvent.VK_RIGHT:
							if (segmentsVisIdx < segments.size() - 1) {
								if (e.isMetaDown()) {
									move(segments.size());
								} else if (e.isShiftDown()) {
									move(10);
								} else {
									move(1);
								}
							}
							break;
					}
				}
			});
		}
	}

	void move(int diff) {
		int old = segmentsVisIdx;
		segmentsVisIdx += diff;
		if (segmentsVisIdx < 0) segmentsVisIdx = 0;
		if (segmentsVisIdx >= segments.size()) segmentsVisIdx = segments.size() - 1;
		if (segmentsVisIdx != old) update();
	}

	protected double run() throws Exception {
		init();

		String[] ret = callSolution();
		if (ret == null) {
			if (!isReadActive()) return getErrorScore();
			return fatalError();
		}
		if (ret.length != numSteps)
			return fatalError("Your solution should return exactly " + numSteps + " steps (returned " + ret.length + ")");

		if (hasVis() && hasDelay()) {
			addInfo("Steps", 0 + " / " + ret.length);
			updateDelay();
		}
		while (step < ret.length) {
			synchronized (updateLock) {
				String s = ret[step++];
//                if (debug) System.out.println("Step " + step + ": " + s);
				if (s.length() != numDancers)
					return fatalError("Step " + step + " does not contain " + numDancers + " moves: " + s);

				for (int i = 0; i < numDancers; i++) {
					char c = s.charAt(i);
					int x = xDancers[i];
					int y = yDancers[i];
					if (c == 'U') y--;
					else if (c == 'D') y++;
					else if (c == 'R') x++;
					else if (c == 'L') x--;
					else if (c != '-')
						return fatalError("Step " + step + " contains an invalid move '" + c + "' for dancer " + i);
					if (y < 0 || x < 0 || y >= size || x >= size) {
						return fatalError("Step " + step + " moved dancer " + i + " out of bounds to (" + x + "," + y + ").");
					}
					xDancers[i] = x;
					yDancers[i] = y;
					if (c != '-' && ++tileIdx[y][x] == tileColors[y][x].length) tileIdx[y][x] = 0;
					int next = markDancers[i] + 1;
					int tm = tChoreographies[i][next];
					if (tm == step) {
						if (x != xChoreographies[i][next] || y != yChoreographies[i][next]) {
							return fatalError("Dancer " + i + " missed mark (" + xChoreographies[i][next] + "," + yChoreographies[i][next] + ") at step " + step);
						}
						if (!dancersPath.isEmpty()) {
							Segment seg = new Segment(dancersPath.get(i).size());
							for (int j = 0; j < dancersPath.get(i).size(); ++j) {
								seg.x[j] = dancersPath.get(i).get(j)[0];
								seg.y[j] = dancersPath.get(i).get(j)[1];
								seg.maxY = Math.max(seg.maxY, seg.y[j]);
							}
							segments.add(seg);
							dancersPath.get(i).clear();
						}
						markDancers[i]++;
					}
					if (!dancersPath.isEmpty()) dancersPath.get(i).add(new int[]{x, y});
				}
			}
			if (hasVis() && (hasDelay() || step == ret.length)) {
				synchronized (updateLock) {
					addInfo("Steps", step + " / " + ret.length);
					addInfo("Score", getScore());
					for (int c = 0; c < numColors; c++) {
						addInfo(paintColors[c], cntByColor[c]);
					}
				}
				updateDelay();
			}
		}
		segments.sort(Comparator.comparing(s1 -> s1.maxY));
		return getScore();
	}

	private void countComponentsByColor() {
		int id = 0;
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				groups[y][x] = id++;
			}
		}
		int[] dx = new int[]{1, -1, 0, 0};
		int[] dy = new int[]{0, 0, 1, -1};
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int g = groups[y][x];
				int c = tileColors[y][x][tileIdx[y][x]];
				for (int i = 0; i < dx.length; i++) {
					int nx = x + dx[i];
					if (nx < 0 || nx >= size) continue;
					int ny = y + dy[i];
					if (ny < 0 || ny >= size) continue;
					int ng = groups[ny][nx];
					if (ng == g) continue;
					int nc = tileColors[ny][nx][tileIdx[ny][nx]];
					if (c != nc) continue;
					for (int ay = 0; ay < size; ay++) {
						for (int ax = 0; ax < size; ax++) {
							if (groups[ay][ax] == ng) groups[ay][ax] = g;
						}
					}
				}
			}
		}
		for (int i = 0; i < numColors; i++) {
			Set<Integer> gc = new HashSet<Integer>();
			for (int y = 0; y < size; y++) {
				for (int x = 0; x < size; x++) {
					int c = tileColors[y][x][tileIdx[y][x]];
					if (c == i) gc.add(groups[y][x]);
				}
			}
			cntByColor[i] = gc.size();
		}
	}

	private int getScore() {
		countComponentsByColor();
		int score = 0;
		for (int i = 0; i < numColors; i++) {
			score += cntByColor[i] * cntByColor[i];
		}
		return score;
	}

	protected void paintContent(Graphics2D g) {
		//Fill floor tiles with their current color
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int c = tileColors[y][x][tileIdx[y][x]];
				Rectangle2D rc = new Rectangle2D.Double(x, y, 1, 1);
				g.setColor(paintColors[c]);
				g.fill(rc);
			}
		}

		//Borders
		g.setStroke(new BasicStroke(0.04f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g.setColor(Color.black);
		for (int i = 0; i <= size; i++) {
			g.draw(new Line2D.Double(0, i, size, i));
			g.draw(new Line2D.Double(i, 0, i, size));
		}

		//Dancers' path
		g.setStroke(new BasicStroke(0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g.setColor(new Color(0, 0, 0, 120));
		if (!segments.isEmpty()) {
			Segment seg = segments.get(segmentsVisIdx);
			Ellipse2D e = new Ellipse2D.Double(seg.x[0] + 0.3, seg.y[0] + 0.3, 0.4, 0.4);
			g.fill(e);

			GeneralPath gp = new GeneralPath();
			gp.moveTo(seg.x[0] + 0.5, seg.y[0] + 0.5);
			for (int i = 0; i < seg.x.length; i++) {
				gp.lineTo(seg.x[i] + 0.5, seg.y[i] + 0.5);
			}
			g.draw(gp);
		}
//		for (int i = 0; i < dancersPath.size(); i++) {
//			List<int[]> path = dancersPath.get(i);
//			if (path.size() < 2) continue;
//			int[] p = path.get(0);
//			Ellipse2D e = new Ellipse2D.Double(p[0] + 0.3, p[1] + 0.3, 0.4, 0.4);
//			g.fill(e);
//
//			GeneralPath gp = new GeneralPath();
//			gp.moveTo(p[0] + 0.5, p[1] + 0.5);
//			for (int j = 1; j < path.size(); j++) {
//				p = path.get(j);
//				gp.lineTo(p[0] + 0.5, p[1] + 0.5);
//			}
//			g.draw(gp);
//		}

		//Dancers
//		g.setStroke(new BasicStroke(0.04f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
//		for (int i = 0; i < numDancers; i++) {
//			Ellipse2D e = new Ellipse2D.Double(xDancers[i] + 0.25, yDancers[i] + 0.25, 0.5, 0.5);
//			g.setColor(Color.black);
//			g.fill(e);
//			g.setColor(Color.white);
//			g.draw(e);
//		}

		//Choreographies
//		for (int i = 0; i < numDancers; i++) {
//			int idx = markDancers[i];
//			if (idx + 1 < xChoreographies[i].length) {
//				int xa = xDancers[i];
//				int ya = yDancers[i];
//				int xb = xChoreographies[i][idx + 1];
//				int yb = yChoreographies[i][idx + 1];
//				if (xa != xb || ya != yb) {
//					Line2D l = new Line2D.Double(xa + 0.5, ya + 0.5, xb + 0.5, yb + 0.5);
//					double angle = Math.atan2(ya - yb, xa - xb);
//					Path2D arrow = new Path2D.Double();
//					arrow.moveTo(xb + 0.5, yb + 0.5);
//					angle += Math.PI / 9;
//					arrow.lineTo(xb + 0.5 + Math.cos(angle) * 0.5, yb + 0.5 + Math.sin(angle) * 0.5);
//					angle -= Math.PI / 4.5;
//					arrow.lineTo(xb + 0.5 + Math.cos(angle) * 0.5, yb + 0.5 + Math.sin(angle) * 0.5);
//					arrow.closePath();
//
//					g.setStroke(new BasicStroke(0.3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
//					g.setColor(new Color(255, 255, 255, 128));
//					g.draw(l);
//					g.setStroke(new BasicStroke(0.03f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
//					g.setColor(new Color(255, 255, 255, 200));
//					g.fill(arrow);
//					g.draw(l);
//				}
//			}
//		}
	}

	private String[] callSolution() throws Exception {
		writeLine(size);
		writeLine(numColors);
		writeLine(numDancers);
		writeLine(numSteps);

		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				sb.delete(0, sb.length());
				int[] tyx = tileColors[y][x];
				for (int c : tyx) {
					sb.append(c);
				}
				writeLine(sb.toString());
			}
		}

		for (int i = 0; i < numDancers; i++) {
			int len = xChoreographies[i].length;
			writeLine(len);
			sb.delete(0, sb.length());
			for (int j = 0; j < len; j++) {
				if (j > 0) sb.append(" ");
				sb.append(xChoreographies[i][j]);
				sb.append(" ");
				sb.append(yChoreographies[i][j]);
				sb.append(" ");
				sb.append(tChoreographies[i][j]);
			}
			writeLine(sb.toString());
		}
		flush();

		if (!isReadActive()) return null;

		startTime();
		int n = readLineToInt(-1);
		if (n < 0) {
			setErrorMessage("Invalid number of steps: " + getLastLineRead());
			return null;
		}
		String[] ret = new String[n];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = readLine();
		}
		stopTime();
		return ret;
	}

	public static void main(String[] args) {
		if (Arrays.stream(args).noneMatch("-vis"::equals)) {
			args = Arrays.copyOf(args, args.length + 1);
			args[args.length - 1] = "-novis";
		}
		if (Arrays.stream(args).noneMatch("-exec"::equals)) {
			args = Arrays.copyOf(args, args.length + 2);
			args[args.length - 2] = "-exec";
			args[args.length - 1] = "./main";
		}
		new MarathonController().run(args);
	}
}