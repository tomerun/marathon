import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public final class SynchronousControl {

	private static final boolean DEBUG = false;
	private static final long TIMELIMIT = 800;
	private static final char EMPTY = '.', WALL = '#', EXIT = 'x';
	private static final String DIR = "NESW";
	private static final int[] DR = { -1, 0, 1, 0 };
	private static final int[] DC = { 0, 1, 0, -1 };
	long start = System.currentTimeMillis();
	Random rand = new Random(42);
	int R, C;
	double V;
	char[] ans = new char[10000];
	int ansI = 0;
	double extraTime = 0;
	double bestTime = 1e9;
	char[][] map;
	char[][][] guide;
	int[][] oil;
	double[] lnTable = new double[10000];
	int rot = 0;
	ArrayList<Point> exits = new ArrayList<Point>();
	ArrayList<Robot> robots = new ArrayList<Robot>();

	String evacuateAll(String[] cave, double V) {
		R = cave.length;
		C = cave[0].length();
		this.V = V;
		for (int i = 0; i < lnTable.length; ++i) {
			lnTable[i] = Math.log(1 + i) / V;
		}
		map = new char[R][C];
		guide = new char[R][C][4];
		oil = new int[R][C];
		ArrayList<Robot> origRobots = new ArrayList<Robot>();
		for (int i = 0; i < R; ++i) {
			for (int j = 0; j < C; ++j) {
				map[i][j] = cave[i].charAt(j);
				if (map[i][j] == EXIT) {
					exits.add(new Point(i, j));
				} else {
					int dir = DIR.indexOf(map[i][j]);
					if (dir != -1) {
						origRobots.add(new Robot(i, j, dir));
						map[i][j] = EMPTY;
					}
				}
			}
		}
		calcPenalty();
		String bestResult = null;
		double worstRepTime = 0;
		int[][] moveI = new int[origRobots.size() / 10][2];
		long curTime = System.currentTimeMillis();
		for (int turn = 0;; ++turn) {
			robots.clear();
			for (int i = 0; i < moveI.length; ++i) {
				moveI[i][0] = rand.nextInt(origRobots.size());
				moveI[i][1] = rand.nextInt(origRobots.size());
			}
			swap(origRobots, moveI);
			for (int i = 0; i < origRobots.size(); ++i) {
				robots.add(origRobots.get(i).clone());
			}
			for (int[] a : oil) {
				Arrays.fill(a, 0);
			}
			String ret = solve();
			if (ret == null) continue;
			if (bestTime > ret.length() + extraTime) {
				bestResult = ret;
				bestTime = ret.length() + extraTime;
				debug(bestTime);
			} else {
				swap(origRobots, moveI);
			}
			long nextTime = System.currentTimeMillis();
			worstRepTime = Math.max(worstRepTime, nextTime - curTime);
			if (nextTime - start + worstRepTime > TIMELIMIT) {
				debug("worstRepTime:" + worstRepTime);
				debug("repeat count:" + turn);
				break;
			}
			curTime = nextTime;
		}
		//		debug(bestResult);
		return bestResult;
	}

	void swap(ArrayList<Robot> robots, int[][] index) {
		for (int i = 0; i < index.length; ++i) {
			int p1 = index[i][0];
			int p2 = index[i][1];
			Robot tmp = robots.get(p1);
			robots.set(p1, robots.get(p2));
			robots.set(p2, tmp);
		}
	}

	String solve() {
		extraTime = 0;
		ansI = 0;
		rot = 0;
		char query;
		while (ansI < 10000 && !robots.isEmpty()) {
			if (ansI + extraTime > bestTime) {
				break;
			}
			Robot curRobot = robots.get(robots.size() - 1);
			query = guide[curRobot.r][curRobot.c][curRobot.dir()];
			ans[ansI++] = query;
			if (query == 'F') {
				double maxTime = 0;
				for (int i = robots.size() - 1; i >= 0; --i) {
					maxTime = Math.max(maxTime, robots.get(i).moveF());
				}
				for (int i = robots.size() - 1; i >= 0; --i) {
					Robot robot = robots.get(i);
					if (robot.moved) {
						++oil[robot.r][robot.c];
						robot.moved = false;
						if (!robot.active) {
							robots.remove(i);
						}
					}
				}
				extraTime += maxTime;
			} else if (query == 'R') {
				rot += 1;
			} else {
				rot += 3;
			}
		}
		if (robots.isEmpty()) {
			return String.valueOf(ans, 0, ansI);
		} else {
			extraTime = 1e6 * robots.size();
			return String.valueOf(ans, 0, ansI);
			//			return null;
		}
	}

	void calcPenalty() {
		ArrayList<PointD> cur = new ArrayList<PointD>();
		for (Point p : exits) {
			for (int i = 0; i < 4; ++i) {
				cur.add(new PointD(p.r, p.c, i));
				guide[p.r][p.c][i] = 'X';
			}
		}
		for (int i = 1; !cur.isEmpty(); ++i) {
			ArrayList<PointD> next = new ArrayList<PointD>();
			for (PointD p : cur) {
				if (guide[p.r][p.c][(p.dir + 1) % 4] == 0) {
					next.add(new PointD(p.r, p.c, (p.dir + 1) % 4));
					guide[p.r][p.c][(p.dir + 1) % 4] = 'L';
				}
				if (guide[p.r][p.c][(p.dir + 3) % 4] == 0) {
					next.add(new PointD(p.r, p.c, (p.dir + 3) % 4));
					guide[p.r][p.c][(p.dir + 3) % 4] = 'R';
				}
				int nr = p.r + DR[(p.dir + 2) % 4];
				int nc = p.c + DC[(p.dir + 2) % 4];
				if (map[nr][nc] != WALL && guide[nr][nc][p.dir] == 0) {
					next.add(new PointD(nr, nc, p.dir));
					guide[nr][nc][p.dir] = 'F';
				}
			}
			cur = next;
		}
	}

	final class Robot {
		int r, c, dir;
		boolean active = true;
		boolean moved = false;

		Robot(int r, int c, int dir) {
			this.r = r;
			this.c = c;
			this.dir = dir;
		}

		public Robot clone() {
			return new Robot(this.r, this.c, this.dir);
		}

		int dir() {
			return (this.dir + rot) % 4;
		}

		double moveF() {
			int gdir = dir();
			int nr = this.r + DR[gdir];
			int nc = this.c + DC[gdir];
			if (map[nr][nc] == EMPTY) {
				this.r = nr;
				this.c = nc;
				this.moved = true;
				return lnTable[oil[nr][nc]];
			} else if (map[nr][nc] == EXIT) {
				this.r = nr;
				this.c = nc;
				this.moved = true;
				this.active = false;
				return lnTable[oil[nr][nc]];
			}
			return 0;
		}

	}

	static final class Point {
		int r, c;

		Point(int r, int c) {
			this.r = r;
			this.c = c;
		}

		public String toString() {
			return "(" + this.r + "," + this.c + ")";
		}
	}

	static final class PointD {
		int r, c, dir;

		PointD(int r, int c, int dir) {
			this.r = r;
			this.c = c;
			this.dir = dir;
		}

		public String toString() {
			return "(" + this.r + "," + this.c + "," + DIR.charAt(this.dir) + ")";
		}
	}

	<T> void shuffle(ArrayList<T> vals) {
		for (int i = 0; i < vals.size() / 2; ++i) {
			int p1 = rand.nextInt(vals.size());
			int p2 = rand.nextInt(vals.size());
			T tmp = vals.get(p1);
			vals.set(p1, vals.get(p2));
			vals.set(p2, tmp);
		}
	}

	static void debug(Object obj) {
		if (DEBUG) System.out.println(obj);
	}

}
