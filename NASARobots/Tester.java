import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Tester {
	static final int SZ_MIN = 40, SZ_MAX = 120;
	static final int ROBOTS_MIN = 20, ROBOTS_MAX = 100;
	static final int EXITS_MIN = 8, EXITS_MAX = 20;
	static final double V_MIN = 5, V_MAX = 10;
	static final double F_MIN = 0.08, F_MAX = 0.12;
	static final int MAX_LENGTH = 100000;

	//direction: N - 0, E - 1, S - 2, W - 3
	static final int[] turnRight = { 1, 2, 3, 0 };
	static final int[] turnLeft = { 3, 0, 1, 2 };
	static final int[] dr = { -1, +0, +1, -0 };
	static final int[] dc = { -0, +1, +0, -1 };
	static final char empty = '.', wall = '#', exit = 'x';
	static final char[] robotdir = { 'N', 'E', 'S', 'W' }; //robot with its initial direction
	static final char LEFT = 'L', RIGHT = 'R', FORWARD = 'F';

	int R, C, N, M;
	double V, F;
	SecureRandom imp;
	volatile char[][] maze;
	volatile int[][] oil;
	volatile ArrayList<Robot> robots;
	volatile MasterDevice masterDevice;

	class Robot {
		int row, col, dir;
		boolean active = true;
		boolean moved = false;

		Robot(int row, int col, int dir) {
			this.row = row;
			this.col = col;
			this.dir = dir;
		}

		double turnLeft() {
			dir = turnLeft[dir];
			return 1;
		}

		double turnRight() {
			dir = turnRight[dir];
			return 1;
		}

		double moveForward() {
			int r = row + dr[dir], c = col + dc[dir];
			if (maze[r][c] == wall) return 1;
			row = r;
			col = c;
			double time = 1 + Math.log(1 + oil[row][col]) / V;
			moved = true;
			if (maze[row][col] == exit) active = false;
			return time;
		}

		void dropOil() { /* should be called after each robot execute the command */
			if (moved) oil[row][col]++;
			moved = false;
		}

		double executeCommand(char ch) {
			if (ch == 'L') return turnLeft();
			if (ch == 'R') return turnRight();
			if (ch == 'F') return moveForward();
			return 1;
		}

		boolean isActive() {
			return active;
		}
	}

	class MasterDevice {
		int nMoves = 0;
		int justEvacuated = 0;
		double totalTime = 0;

		double executeCommand(char ch) {
			double time = 0;
			//at first step all robots try to move
			for (Robot robot : robots)
				time = Math.max(robot.executeCommand(ch), time);
			//at second step robots drop oil (to avoid collision)
			for (Robot robot : robots)
				robot.dropOil();
			nMoves++;
			totalTime += time;
			return time;
		}

		void tryToEvacuate() {
			justEvacuated = 0;
			Iterator<Robot> it = robots.iterator();
			while (it.hasNext())
				if (!it.next().isActive()) {
					it.remove();
					justEvacuated++;
				}
		}

		boolean isCompleted() {
			return robots.isEmpty();
		}

		int numberOfActiveRobots() {
			return robots.size();
		}

		double getScore() {
			return 1000 * Math.sqrt(R * C) * N / totalTime;
		}
	}

	void generate(long seed) throws Exception {
		imp = SecureRandom.getInstance("SHA1PRNG");
		imp.setSeed(seed);
		R = imp.nextInt(SZ_MAX - SZ_MIN + 1) + SZ_MIN;
		C = imp.nextInt(SZ_MAX - SZ_MIN + 1) + SZ_MIN;
		N = imp.nextInt(ROBOTS_MAX - ROBOTS_MIN + 1) + ROBOTS_MIN;
		M = imp.nextInt(EXITS_MAX - EXITS_MIN + 1) + EXITS_MIN;
		V = imp.nextDouble() * (V_MAX - V_MIN) + V_MIN;
		F = imp.nextDouble() * (F_MAX - F_MIN) + F_MIN;
		while (maze == null) {
			maze = generateMaze(R, C, M);
		}
		oil = new int[R][C];
		robots = generateRobots(maze, N);
		masterDevice = new MasterDevice();
	}

	String[] convertMazeToInput() {
		for (Robot robot : robots)
			maze[robot.row][robot.col] = robotdir[robot.dir];
		String[] res = new String[R];
		for (int i = 0; i < R; i++)
			res[i] = new String(maze[i]);
		for (Robot robot : robots)
			maze[robot.row][robot.col] = empty;
		return res;
	}

	private boolean get(int x, int y) {
		boolean res = true;
		for (int i = 0; i <= x % 2 + y % 2 + (x + y) % 3 + 1; i++)
			res = res && imp.nextDouble() > F;
		return res;
	}

	private int floodfill(int[][] board, int startRow, int startCol, int mark) {
		int R = board.length, C = board[0].length;
		ArrayList<Integer> rows = new ArrayList<Integer>();
		ArrayList<Integer> cols = new ArrayList<Integer>();

		rows.add(startRow);
		cols.add(startCol);
		int old = board[startRow][startCol];
		board[startRow][startCol] = mark;
		int dr[] = { -1, +1, -0, +0 };
		int dc[] = { -0, +0, -1, +1 };

		int iter = 0;
		while (iter < rows.size()) {
			int r = rows.get(iter);
			int c = cols.get(iter);
			for (int i = 0; i < 4; i++) {
				int row = r + dr[i];
				int col = c + dc[i];
				if (0 <= row && row < R && 0 <= col && col < C && board[row][col] == old) {
					rows.add(row);
					cols.add(col);
					board[row][col] = mark;
				}
			}
			iter++;
		}
		return rows.size();
	}

	private char[][] generateMaze(int R, int C, int M) {
		int[][] board = new int[R][C];
		for (int row = 0; row < R; row++) {
			for (int col = 0; col < C; col++) {
				board[row][col] = get(row, col) ? 0 : -1;
				if (row == 0 || row == R - 1 || col == 0 || col == C - 1) board[row][col] = -1;
			}
		}

		int maxZoneIndex = 0, maxZoneSize = 0, nextZoneIndex = 1;
		for (int row = 0; row < R; row++)
			for (int col = 0; col < C; col++)
				if (board[row][col] == 0) {
					int size = floodfill(board, row, col, nextZoneIndex);
					if (size > maxZoneSize) {
						maxZoneIndex = nextZoneIndex;
						maxZoneSize = size;
					}
					nextZoneIndex++;
				}
		if (maxZoneSize < R * C / 3) return null;

		char[][] maze = new char[R][C];
		for (int row = 0; row < R; row++)
			for (int col = 0; col < C; col++)
				maze[row][col] = (board[row][col] == maxZoneIndex) ? empty : wall;

		int m = 0;
		while (m < M) {
			int r = imp.nextInt(R);
			int c = imp.nextInt(C);
			if (maze[r][c] == empty) {
				maze[r][c] = exit;
				m++;
			}
		}
		return maze;
	}

	private ArrayList<Robot> generateRobots(char[][] maze, int N) {
		int R = maze.length, C = maze[0].length;
		ArrayList<Robot> robots = new ArrayList<Robot>();
		int n = 0;
		while (n < N) {
			int r = imp.nextInt(R);
			int c = imp.nextInt(C);
			int dir = imp.nextInt(4);
			if (maze[r][c] == empty) {
				maze[r][c] = robotdir[dir];
				robots.add(new Robot(r, c, dir));
				n++;
			}
		}
		return robots;
	}

	// ------------- VISUALIZATION PART -----------------------------------------------------
	static int side;
	static boolean vis, manual, ready;
	static int delay;
	JFrame jf;
	Vis v;

	public class Vis extends JPanel {

		public Vis() {
			jf.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (!manual) return;
					if (masterDevice.isCompleted()) return;
					char keyChar = e.getKeyChar();
					int keyCode = e.getKeyCode();
					char ch = 0;

					if (keyCode == KeyEvent.VK_A || keyCode == KeyEvent.VK_L || keyCode == KeyEvent.VK_LEFT) ch = 'L';
					if (keyCode == KeyEvent.VK_D || keyCode == KeyEvent.VK_R || keyCode == KeyEvent.VK_RIGHT) ch = 'R';
					if (keyCode == KeyEvent.VK_W || keyCode == KeyEvent.VK_F || keyCode == KeyEvent.VK_UP) ch = 'F';

					if (ch != 'L' && ch != 'R' && ch != 'F' && ch != KeyEvent.CHAR_UNDEFINED) {
						if (keyChar != KeyEvent.CHAR_UNDEFINED) {
							System.err.println("The key you pressed (" + (char) keyChar + ") was ignored, since "
									+ "it does not correspond to a valid command.");
						}
						return;
					}
					masterDevice.tryToEvacuate();
					masterDevice.executeCommand(ch);
					draw();
				}
			});
			jf.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
		}

		public void paint(Graphics gr) {
			//create canvas
			BufferedImage bi = new BufferedImage(C * side + 210, R * side + 1, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();

			// background
			g2.setColor(new Color(0xEEEEEE));
			g2.fillRect(0, 0, C * side + 210, R * side + 1);
			g2.setColor(new Color(0xAAAAAA));
			g2.fillRect(0, 0, C * side, R * side);

			//maze with exits
			for (int row = 0; row < R; row++)
				for (int col = 0; col < C; col++) {
					if (maze[row][col] == empty) {
						int cnt = (int) Math.floor(Math.log(1 + oil[row][col]));
						if (oil[row][col] == 0)
							g2.setColor(new Color(255, 255, 200));
						else
							g2.setColor(new Color(255, 255 - 16 * cnt, 100));
					}
					if (maze[row][col] == wall) g2.setColor(Color.BLACK);
					if (maze[row][col] == exit) g2.setColor(Color.RED);
					g2.fillRect(col * side, row * side, side, side);
				}

			int h = (side - 2) / 3; //length of robot's 'head'
			int r = (side - 2 * h) / 3; //size of robot's 'shoulders'
			int w = side - 2 * (h + r);

			//draw robots
			for (Robot robot : robots) {
				if (robot.isActive())
					g2.setColor(Color.BLUE);
				else
					g2.setColor(Color.GREEN);
				int x0 = robot.col * side, y0 = robot.row * side;
				g2.fillRect(x0 + h, y0 + h, side - 2 * h, side - 2 * h);
				switch (robot.dir) {
				case 0:
					g2.fillRect(x0 + h + r, y0, w, h);
					break;
				case 1:
					g2.fillRect(x0 + side - h, y0 + h + r, h, w);
					break;
				case 2:
					g2.fillRect(x0 + h + r, y0 + side - h, w, h);
					break;
				case 3:
					g2.fillRect(x0, y0 + h + r, h, w);
					break;
				}
			}

			// the number of shifts done so far
			double _time_ = (int) (masterDevice.totalTime * 100) / 100.0;
			double _score_ = (int) (masterDevice.getScore() * 10000) / 10000.0;
			double _speed_ = (int) (V * 100) / 100.0;
			char[] nMoves = ("Moves: " + masterDevice.nMoves).toCharArray();
			char[] totalTime = ("Time:  " + _time_).toCharArray();
			char[] score = ("Score: " + (masterDevice.nMoves == 0 ? "infinity" : _score_)).toCharArray();
			char[] just = ("Just evacuated: " + masterDevice.justEvacuated).toCharArray();
			char[] left = ("Robots left: " + robots.size() + " of " + N).toCharArray();
			char[] speed = ("V = " + _speed_).toCharArray();

			g2.setColor(Color.BLACK);
			g2.setFont(new Font("Arial", Font.BOLD, 14));
			g2.drawChars(nMoves, 0, nMoves.length, (C + 1) * side, 5 * side);
			g2.drawChars(totalTime, 0, totalTime.length, (C + 1) * side, 7 * side);
			g2.drawChars(score, 0, score.length, (C + 1) * side, 9 * side);
			g2.drawChars(just, 0, just.length, (C + 1) * side, 15 * side);
			g2.drawChars(left, 0, left.length, (C + 1) * side, 17 * side);
			g2.drawChars(speed, 0, speed.length, (C + 1) * side, 23 * side);

			gr.drawImage(bi, 0, 0, C * side + 210, R * side + 1, null);
		}

	}

	public void draw() {
		if (!vis) return;
		v.repaint();
		try {
			Thread.sleep(delay);
		} catch (Exception e) {}
	}

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		try {
			generate(seed);
			res.R = this.R;
			res.C = this.C;
			res.N = this.N;
			res.M = this.M;
			res.V = this.V;

			if (vis) {
				jf = new JFrame();
				v = new Vis();
				jf.getContentPane().add(v);
				jf.setSize(C * side + 210, R * side + 38);
				jf.setVisible(true);
			}

			if (!manual) {
				long start = System.currentTimeMillis();
				String commands = new SynchronousControl().evacuateAll(convertMazeToInput(), V);
				res.elapsed = System.currentTimeMillis() - start;

				if (commands == null || commands.length() == 0) {
					System.err.println("Your return must contain at least one element!");
					return res;
				}
				if (commands.length() > MAX_LENGTH) {
					System.err.println("Your return is too long!");
					return res;
				}
				for (int i = 0; i < commands.length(); i++) {
					char ch = commands.charAt(i);
					if (ch != 'L' && ch != 'R' && ch != 'F') {
						System.err.println("You can use only three commands: R, L, F!");
						return res;
					}
				}

				for (int i = 0; i < commands.length(); i++) {
					char ch = commands.charAt(i);
					if (masterDevice.isCompleted()) {
						if (i + 1 < commands.length()) {
							System.err.println("Your return contains " + commands.length() + " moves, "
									+ "but all robots are evacuated in " + i + " moves...");
						} else {
							System.err.println("All robots are evacuated in " + commands.length() + " moves.");
						}
						break;
					}
					masterDevice.executeCommand(ch);
					masterDevice.tryToEvacuate();
					draw();
				}

				if (!masterDevice.isCompleted()) {
					int cnt = masterDevice.numberOfActiveRobots();
					String verb = (cnt > 1 ? "are" : "is");
					System.err.println("Not all robots are evacuated! " + cnt + " of them " + verb + " still in the maze!");
					res.turn = masterDevice.nMoves;
					res.time = masterDevice.totalTime;
					return res;
				}
			} else {
				while (!masterDevice.isCompleted()) {
					try {
						Thread.sleep(100);
					} catch (Exception e) {}
				}
			}
			res.turn = masterDevice.nMoves;
			res.time = masterDevice.totalTime;
			res.score = masterDevice.getScore();
			return res;
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results.");
			e.printStackTrace();
			return res;
		}
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 1;
		long begin = -1;
		long end = -1;
		vis = false;
		manual = false;
		delay = 200;
		side = 8;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-begin")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-end")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-manual")) manual = true;
			if (args[i].equals("-delay")) delay = Integer.parseInt(args[++i]);
			if (args[i].equals("-side")) side = Integer.parseInt(args[++i]);
		}
		if (side < 5) side = 5;
		if (side > 20) side = 20;
		if (manual) vis = true;
		if (begin != -1 && end != -1) {
			vis = false;
			manual = false;
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			System.out.println(tester.runTest(seed));
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int R, C, N, M;
		double V;
		int turn;
		double time;
		double score;
		long elapsed;

		public String toString() {
			return "seed " + this.seed + "\nR:" + this.R + " C:" + this.C + " N:" + this.N + " M:" + this.M + "\nturn "
					+ turn + "\ntime " + time + "\nelapsed " + this.elapsed / 1000.0 + "\nscore " + this.score;
		}

	}
}
