import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Tester {
	int N, H;
	String[] grid;

	char letter(int n) {
		if (n < 120601) return 'A';
		if (n < 150605) return 'B';
		if (n < 214709) return 'C';
		if (n < 269452) return 'D';
		if (n < 451895) return 'E';
		if (n < 471847) return 'F';
		if (n < 515266) return 'G';
		if (n < 551922) return 'H';
		if (n < 692039) return 'I';
		if (n < 694686) return 'J';
		if (n < 709080) return 'K';
		if (n < 793595) return 'L';
		if (n < 838324) return 'M';
		if (n < 944965) return 'N';
		if (n < 1048183) return 'O';
		if (n < 1094642) return 'P';
		if (n < 1097221) return 'Q';
		if (n < 1209558) return 'R';
		if (n < 1359610) return 'S';
		if (n < 1463496) return 'T';
		if (n < 1515457) return 'U';
		if (n < 1530849) return 'V';
		if (n < 1543175) return 'W';
		if (n < 1547895) return 'X';
		if (n < 1573656) return 'Y';
		return 'Z';
	}

	void generate(long seed) {
		try {
			SecureRandom r = SecureRandom.getInstance("SHA1PRNG");
			r.setSeed(seed);
			if (seed != 1) {
				N = r.nextInt(46) + 5; //5..50
				if (np != -1) N = np;
				char[][] m = new char[N][N];
				int i, j;
				for (i = 0; i < N; i++)
					for (j = 0; j < N; j++)
						m[i][j] = letter(r.nextInt(1581229));
				grid = new String[N];
				for (i = 0; i < N; i++)
					grid[i] = new String(m[i]);
				H = r.nextInt(N / 3) + 1; //1..floor(N/3)
				if (hp != -1) H = hp;
			} else { //predefined test case
				H = 1;
				N = 5;
				grid = new String[N];
				grid[0] = "GCNQE";
				grid[1] = "XSRTR";
				grid[2] = "EIWTI";
				grid[3] = "RCOLV";
				grid[4] = "NJLOU";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public double runTest(long seed) {
		try {
			int i, j;
			generate(seed);
			if (out) {
				System.out.println("N = " + N);
				System.out.println("Grid = ");
				for (i = 0; i < N; i++)
					System.out.println(grid[i]);
				System.out.println("H = " + H);
			}
			String[] words = Dictionary.getWords();

			//pass the params and get the result
			String[] grille = new GrilleReconstruction().bestGrille(grid, H);
			if (out) {
				System.out.println("Grille = ");
				for (i = 0; i < N; i++)
					System.out.println(grille[i]);
			}

			//check whether the return is square
			for (i = 0; i < N; i++)
				if (grille[i].length() != N) {
					addFatalError("Each element of return must be " + N + " characters long.");
					return 0.0;
				}
			//check valid characters and number of holes per row/col
			int nh;
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++)
					if (grille[i].charAt(j) != 'X' && grille[i].charAt(j) != '.') {
						addFatalError("Each character of return must be 'X' or '.'.");
						return 0.0;
					}
				nh = 0;
				for (j = 0; j < N; j++)
					if (grille[i].charAt(j) == '.') nh++;
				if (nh > H) {
					addFatalError("The grille can have at most " + H + " hole(s) per row.");
					return 0.0;
				}
				nh = 0;
				for (j = 0; j < N; j++)
					if (grille[j].charAt(i) == '.') nh++;
				if (nh > H) {
					addFatalError("The grille can have at most " + H + " hole(s) per column.");
					return 0.0;
				}
			}

			//calculate the total number of holes
			nh = 0;
			for (i = 0; i < N; i++)
				for (j = 0; j < N; j++)
					if (grille[i].charAt(j) == '.') nh++;

			if (nh == 0) return 0.0;
			//get the string read with the returned grille
			char[] t = new char[4 * nh];
			int T = 4 * nh;
			int k = 0;
			//0 deg
			for (i = 0; i < N; i++)
				for (j = 0; j < N; j++)
					if (grille[i].charAt(j) == '.') {
						t[k] = grid[i].charAt(j);
						k++;
					}
			//90 deg
			for (i = 0; i < N; i++)
				for (j = 0; j < N; j++)
					if (grille[N - 1 - j].charAt(i) == '.') {
						t[k] = grid[i].charAt(j);
						k++;
					}
			//180 deg
			for (i = 0; i < N; i++)
				for (j = 0; j < N; j++)
					if (grille[N - 1 - i].charAt(N - 1 - j) == '.') {
						t[k] = grid[i].charAt(j);
						k++;
					}
			//270 deg
			for (i = 0; i < N; i++)
				for (j = 0; j < N; j++)
					if (grille[j].charAt(N - 1 - i) == '.') {
						t[k] = grid[i].charAt(j);
						k++;
					}
			if (out) System.out.println(new String(t));

			//for each word of the list find its occurencies in the resulting string
			//or vice versa - for each substr of the string check whether it's covered with a word
			boolean[][] cov = new boolean[T][15];
			//characters i..i+j, inclusive, 3<=j<=14 (each word length is between 4 and 15)
			for (i = 0; i < T; i++)
				//starting position
				for (j = 3; j < 15 && i + j < T; j++)
					if (Arrays.binarySearch(words, new String(t, i, j + 1)) >= 0) {
						//System.out.println(i+" "+j);
						cov[i][j] = true;
					}

			//find the marking of the string which gives the best score
			//score is sum of "length of word"^1.1 over all words in the marking minus number of unmarked characters
			//sc[i][j] - best score for the prefix which ends AT character i 
			//with length of current unfinished sequence j
			//j<=(i+1)
			//the score is calculated for finished sequences and unused letters only
			double skip = 0.33, newval;
			double[][] sc = new double[T][T + 1];
			//from[i][j] - from which i we came to this state
			int[][] from = new int[T][T + 1];
			//prev[i][j] - from which j we came to this state
			int[][] prev = new int[T][T + 1];
			boolean[] use = new boolean[T];
			for (i = 0; i < T; i++)
				for (j = 0; j <= T; j++)
					from[i][j] = -2; //to note "we can't come here in this way"
			sc[0][0] = -skip;
			from[0][0] = -1;
			prev[0][0] = 0;

			for (i = 1; i < T; i++) { //try skipping the last letter (always possible and always brings us to 0 length seq)
				sc[i][0] = -(i + 1) * skip;
				from[i][0] = i - 1;
				prev[i][0] = 0;
				//check whether there have been non-0 length seq before it
				for (j = 0; j <= i; j++)
					if (from[i - 1][j] > -2 && sc[i - 1][j] + Math.pow(j, 1.1) - skip > sc[i][0]) {
						sc[i][0] = sc[i - 1][j] + Math.pow(j, 1.1) - skip;
						from[i][0] = i - 1;
						prev[i][0] = j;
					}
				//try ending the substring with a word of length k+1
				for (k = 3; k < 15 && i - k >= 0; k++)
					if (cov[i - k][k]) { //a word fits here - continue everything that includes it (but don't score)
						if (i - k == 0) { //no sc[i-k-1][...], so only the prefix of length k+1
							sc[i][k + 1] = 0;
							from[i][k + 1] = -1;
							prev[i][k + 1] = 0;
						} else
							for (j = 0; j <= i - k; j++)
								if (from[i - k - 1][j] > -2) { //we had a prefix of length j there, continue it for k+1 chars
									//but we could have come to the same place with a different prefix of same sum of lengths
									newval = sc[i - k - 1][j];
									if (from[i][j + k + 1] == -2 || sc[i][j + k + 1] <= newval) {
										sc[i][j + k + 1] = newval;
										from[i][j + k + 1] = i - k - 1;
										prev[i][j + k + 1] = j;
									}
								}
					}
			}
			//finally, score the prefixes in the last position
			int jbest = 0;
			double bestval = sc[T - 1][0]; //already scored
			for (j = 1; j <= T; j++)
				if (from[T - 1][j] > -2) {
					newval = sc[T - 1][j] + Math.pow(j, 1.1);
					if (bestval <= newval) {
						jbest = j;
						bestval = newval;
					}
				}

			if (out) { //output the words found in the text (in order)
				System.out.println("Words found in the decoded message (in reverse order):");
			}
			i = T - 1;
			j = jbest;
			while (i >= 0) {
				if (j > 0 && from[i][j] < i - 1) {
					if (out) {
						System.out.println(new String(t, from[i][j] + 1, i - from[i][j]));
					}
					for (k = from[i][j] + 1; k <= i; k++) {
						use[k] = true;
					}
				}
				//get back to from[i][j]
				jbest = prev[i][j];
				i = from[i][j];
				j = jbest;
			}

			//save the image to file, if wanted
			if (vis) {
				final BufferedImage bi = new BufferedImage(N * 80 + 60, N * 20 + 20 + 20 * H, BufferedImage.TYPE_INT_RGB);
				Graphics2D g = (Graphics2D) bi.getGraphics();
				//background
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, bi.getWidth(), bi.getHeight());
				//background of grids
				g.setColor(Color.LIGHT_GRAY);
				g.fillRect(0, 0, N * 20, N * 20);
				g.fillRect(20 + N * 20, 0, N * 20, N * 20);
				g.fillRect(40 + N * 40, 0, N * 20, N * 20);
				g.fillRect(60 + N * 60, 0, N * 20, N * 20);
				//lines in grids
				g.setColor(Color.BLACK);
				for (k = 0; k < 4; k++)
					for (i = 0; i <= N; i++) {
						g.drawLine(k * (20 + 20 * N) + i * 20, 0, k * (20 + 20 * N) + i * 20, N * 20);
						g.drawLine(k * (20 + 20 * N), i * 20, k * (20 + 20 * N) + N * 20, i * 20);
					}
				//whites in place of holes
				g.setColor(Color.WHITE);
				for (i = 0; i < N; i++)
					for (j = 0; j < N; j++) {
						if (grille[i].charAt(j) == '.') //0 deg
							g.fillRect(j * 20 + 1, i * 20 + 1, 19, 19);
						if (grille[N - 1 - j].charAt(i) == '.') //90 deg
							g.fillRect(20 + 20 * N + j * 20 + 1, i * 20 + 1, 19, 19);
						if (grille[N - 1 - i].charAt(N - 1 - j) == '.') //180 deg
							g.fillRect(40 + 40 * N + j * 20 + 1, i * 20 + 1, 19, 19);
						if (grille[j].charAt(N - 1 - i) == '.') //270 deg
							g.fillRect(60 + 60 * N + j * 20 + 1, i * 20 + 1, 19, 19);
					}
				int ptr = 0;
				for (i = 0; i < N; i++)
					for (j = 0; j < N; j++) {
						if (grille[i].charAt(j) == '.') {
							if (use[ptr]) {
								g.setColor(Color.red);
								g.fillRect(j * 20 + 1, i * 20 + 1, 19, 19);
							}
							ptr++;
						}
					}
				for (i = 0; i < N; i++)
					for (j = 0; j < N; j++) {
						if (grille[N - 1 - j].charAt(i) == '.') {
							if (use[ptr]) {
								g.setColor(Color.red);
								g.fillRect(20 + 20 * N + j * 20 + 1, i * 20 + 1, 19, 19);
							}
							ptr++;
						}
					}
				for (i = 0; i < N; i++)
					for (j = 0; j < N; j++) {
						if (grille[N - 1 - i].charAt(N - 1 - j) == '.') { //180 deg
							if (use[ptr]) {
								g.setColor(Color.red);
								g.fillRect(40 + 40 * N + j * 20 + 1, i * 20 + 1, 19, 19);
							}
							ptr++;
						}
					}
				for (i = 0; i < N; i++)
					for (j = 0; j < N; j++) {
						if (grille[j].charAt(N - 1 - i) == '.') { //270 deg
							if (use[ptr]) {
								g.setColor(Color.red);
								g.fillRect(60 + 60 * N + j * 20 + 1, i * 20 + 1, 19, 19);
							}
							ptr++;
						}
					}
				//letters
				g.setColor(Color.BLACK);
				g.setFont(new Font("Arial", Font.BOLD, 14));
				FontMetrics fm = g.getFontMetrics();
				char[] ch = new char[1];
				for (i = 0; i < N; i++)
					for (j = 0; j < N; j++) {
						ch[0] = grid[i].charAt(j);
						g.drawChars(ch, 0, 1, j * 20 + 10 - fm.charWidth(ch[0]) / 2, i * 20 + 8 + fm.getHeight() / 2);
						g.drawChars(ch, 0, 1, 20 + 20 * N + j * 20 + 10 - fm.charWidth(ch[0]) / 2, i * 20 + 8 + fm.getHeight() / 2);
						g.drawChars(ch, 0, 1, 40 + 40 * N + j * 20 + 10 - fm.charWidth(ch[0]) / 2, i * 20 + 8 + fm.getHeight() / 2);
						g.drawChars(ch, 0, 1, 60 + 60 * N + j * 20 + 10 - fm.charWidth(ch[0]) / 2, i * 20 + 8 + fm.getHeight() / 2);
					}
				//and the string of the result
				int cw = (bi.getWidth() - 20) / 20;
				//System.out.println(nh+" "+N);
				for (i = 0; i < 4 * nh; i++) {
					if (use[i])
						g.setColor(Color.red);
					else
						g.setColor(Color.black);
					g.drawChars(t, i, 1, i % cw * 20 + 10 - fm.charWidth(t[i]) / 2,
							N * 20 + 20 * (1 + i / cw) + 8 + fm.getHeight() / 2);
					//g.drawChars(t,  nh+i,1, 20+20*N+i*20+10-fm.charWidth(t[  nh+i])/2, N*20+20+8+fm.getHeight()/2);
					//g.drawChars(t,2*nh+i,1, 40+40*N+i*20+10-fm.charWidth(t[2*nh+i])/2, N*20+20+8+fm.getHeight()/2);
					//g.drawChars(t,3*nh+i,1, 60+60*N+i*20+10-fm.charWidth(t[3*nh+i])/2, N*20+20+8+fm.getHeight()/2);
				}
				if (file.equals("-")) {
					JFrame jf = new JFrame();
					jf.setSize(bi.getWidth() + 10, bi.getHeight() + 50);
					jf.add(new JPanel() {
						public void paint(Graphics g) {
							g.drawImage(bi, 0, 0, null);
						}
					});
					jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					jf.setVisible(true);
				} else {
					ImageIO.write(bi, "png", new File(file + ".png"));
				}
			}

			return Math.max(0.0, bestval);
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results.");
			e.printStackTrace();
			return 0.0;
		}
	}

	static String file;
	static boolean vis;
	static boolean out;
	static int np = -1;
	static int hp = -1;

	public static void main(String[] args) {
		long seed = 1;
		file = "1";
		vis = false;
		out = false;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-vis")) {
				vis = true;
				file = args[++i];
			}
			if (args[i].equals("-out")) out = true;
			if (args[i].equals("-begin")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-end")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-np")) np = Integer.parseInt(args[++i]);
			if (args[i].equals("-hp")) hp = Integer.parseInt(args[++i]);
		}
		Tester tester = new Tester();
		if (begin != -1 && end != -1) {
			for (long i = begin; i <= end; ++i) {
				System.out.println("seed:" + i);
				System.out.println("Score = " + tester.runTest(i));
			}
		} else {
			System.out.println("Score = " + tester.runTest(seed));
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}
}

class Dictionary {
	static String[] cache;

	static String[] getWords() {
		if (cache != null) return cache;
		try {
			FileInputStream in = new FileInputStream("words.txt");
			byte[] b = new byte[2000000];
			int read = in.read(b);
			cache = new String(b, 0, read).split("\r\n");
			return cache;
		} catch (IOException e) {
			return null;
		}
	}
}