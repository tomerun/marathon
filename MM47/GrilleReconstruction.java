import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

public class GrilleReconstruction {
	private static final boolean DEBUG = false;
	private static final long TIMER = 4500;
	private static final double PENALTY = -0.33;
	private int MAX_STATES_COUNT = 500;
	int N;
	int H;
	char[][] input;
	String[] dic;
	Trie trie = new Trie();
	long startTime = System.currentTimeMillis();
	double bestScore = 0;
	ArrayList<Point> bestPos = new ArrayList<Point>();
	int[] decoded;
	double[] powTable;
	PointSorter pointSorter = new PointSorter();

	static int[] hist = { 120601, 30004, 64104, 54743, 182443, 19952, 43419, 36656, 140117, 2647, 14394, 84515, 44729,
			106641, 103218, 46459, 2579, 112337, 150052, 103886, 51961, 15392, 12326, 4720, 25761, 7573 };

	public String[] bestGrille(String[] grid, int H) {
		this.N = grid.length;
		this.H = H;
		input = new char[N][N];
		for (int i = 0; i < N; ++i) {
			input[i] = grid[i].toCharArray();
			for (int j = 0; j < N; ++j) {
				input[i][j] -= 'A';
			}
		}
		dic = Dictionary.getWords();
		trie.add(dic);
		debug("after build trie:" + (System.currentTimeMillis() - startTime));
		decoded = new int[N * H * 4];
		powTable = new double[N * H * 4 + 1];
		for (int i = 0; i < powTable.length; ++i) {
			powTable[i] = Math.pow(i, 1.1);
		}
		if (N * H < 20) {
			MAX_STATES_COUNT = 15000;
		} else if (N * H < 50) {
			MAX_STATES_COUNT = 8000;
		} else if (N * H < 100) {
			MAX_STATES_COUNT = 5000;
		} else if (N * H < 200) {
			MAX_STATES_COUNT = 3000;
		} else if (N * H <= 400) {
			MAX_STATES_COUNT = 1500;
		} else {
			MAX_STATES_COUNT = 800;
		}

		solve();

		boolean[][] ret = new boolean[N][N];
		for (Point p : bestPos) {
			ret[p.y][p.x] = true;
		}
		String[] ans = new String[N];
		for (int i = 0; i < N; ++i) {
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < N; ++j) {
				sb.append(ret[i][j] ? '.' : 'X');
			}
			ans[i] = sb.toString();
		}
		debug("N:" + N + " H:" + H);
		debug("elapsed:" + (System.currentTimeMillis() - startTime));
		return ans;
	}

	void solve() {
		ArrayList<State> sts = new ArrayList<State>();
		sts.add(new State());
		ArrayList<State> add = new ArrayList<State>();
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				add.clear();
				int v = input[i][j];
				for (int k = 0; k < sts.size(); ++k) {
					State st = sts.get(k);
					if (st.rc[i] == H || st.cc[j] == H || st.cur.child[v] == null) continue;
					State born = new State(st, i, j);
					add.add(born);
					if (born.cur.stop) {
						State back = new State();
						back.pos.addAll(born.pos);
						back.rc = born.rc.clone();
						back.cc = born.cc.clone();
						back.ccsqsum = born.ccsqsum;
						add.add(back);
					}
				}
				sts.addAll(add);
				if (sts.size() > MAX_STATES_COUNT) {
					Collections.sort(sts);
					ArrayList<State> next = new ArrayList<State>(MAX_STATES_COUNT);
					for (int k = MAX_STATES_COUNT; k > 0; --k) {
						next.add(sts.get(sts.size() - k));
					}
					sts = next;
				}
			}
			debug("row " + i + " end:" + (System.currentTimeMillis() - startTime));
		}
		Collections.sort(sts);
		debug("sts size:" + sts.size());
		debug("length:" + sts.get(0).pos.size() + "-" + sts.get(sts.size() - 1).pos.size());
		for (int i = sts.size() - 1; i >= 0; --i) {
			if (System.currentTimeMillis() - startTime > TIMER) {
				debug("end at:" + i + " of " + sts.size());
				break;
			}
			State st = sts.get(i);
			boolean update = calcScore(st.pos);
			if (update) {
				debug(Arrays.toString(st.cc));
				debug(st.ccsqsum);
				debug("bestScore:" + bestScore);
			}
		}
		debug("end of search:" + (System.currentTimeMillis() - startTime));

		int base = 1;
		while (base * 4 * 4 < powTable.length && powTable[base * 4 * 4] < bestScore) {
			++base;
		}
		debug("base:" + base);
		ArrayList<Point> randPoints = new ArrayList<Point>();
		int[] rc = new int[N];
		int[] cc = new int[N];
		Random rand = new Random(42);
		int randCount = 0;
		if (base < N * N / 6) {
			OUT: while (true) {
				if (N * H < 60) {
					if ((randCount & 0xF) == 0 && System.currentTimeMillis() - startTime > TIMER - 800) {
						break;
					}
				} else {
					if (System.currentTimeMillis() - startTime > TIMER) {
						break;
					}
				}
				++randCount;
				randPoints.clear();
				Arrays.fill(rc, 0);
				Arrays.fill(cc, 0);
				int count = 0;
				for (int i = 0; count < base; ++i) {
					int x = rand.nextInt((N + 1) / 2);
					int y = rand.nextInt(N / 2);
					boolean found = false;
					for (int j = 0; j < randPoints.size(); ++j) {
						if (randPoints.get(j).x == x && randPoints.get(j).y == y) {
							found = true;
							break;
						}
					}
					if (found) continue;
					randPoints.add(new Point(x, y));
					++count;
				}
				for (Point p : randPoints) {
					++rc[p.y];
					++cc[p.x];
					++rc[p.x];
					++cc[N - 1 - p.y];
					++rc[N - 1 - p.y];
					++cc[N - 1 - p.x];
					++rc[N - 1 - p.x];
					++cc[p.y];
				}
				for (int i = 0; i < N; ++i) {
					if (rc[i] > H || cc[i] > H) {
						continue OUT;
					}
				}
				for (int i = 0, size = randPoints.size(); i < size; ++i) {
					Point p = randPoints.get(i);
					randPoints.add(new Point(N - 1 - p.y, p.x));
					randPoints.add(new Point(N - 1 - p.x, N - 1 - p.y));
					randPoints.add(new Point(p.y, N - 1 - p.x));
				}
				Collections.sort(randPoints, pointSorter);
				boolean update = calcScore(randPoints);
				if (update) {
					debug("bestScore:" + bestScore + " (by random)");
					++base;
					if (base > N * N / 6) break;
				}
			}
		}
		while (System.currentTimeMillis() - startTime < TIMER) {
			++randCount;
			randPoints.clear();
			Arrays.fill(rc, 0);
			Arrays.fill(cc, 0);
			for (int i = 0; i < (int) (N * H * 1.8); ++i) {
				int p = rand.nextInt(N * N);
				int x = p / N;
				int y = p % N;
				if (rc[y] == H || cc[x] == H) continue;
				boolean found = false;
				for (int j = 0; j < randPoints.size(); ++j) {
					if (randPoints.get(j).x == x && randPoints.get(j).y == y) {
						found = true;
						break;
					}
				}
				if (found) continue;
				randPoints.add(new Point(x, y));
				rc[y]++;
				cc[x]++;
			}
			Collections.sort(randPoints, pointSorter);
			boolean update = calcScore(randPoints);
			if (update) {
				debug("bestScore:" + bestScore + " (by random)");
			}
		}
		debug("randCount:" + randCount);
	}

	void rot90(ArrayList<Point> pos) {
		for (int i = 0; i < pos.size(); ++i) {
			Point p = pos.get(i);
			int x = p.x;
			p.x = N - 1 - p.y;
			p.y = x;
		}
		Collections.sort(pos, pointSorter);
	}

	void rot180(ArrayList<Point> pos) {
		for (int i = 0; i < pos.size(); ++i) {
			Point p = pos.get(i);
			p.x = N - 1 - p.x;
			p.y = N - 1 - p.y;
		}
		for (int i = 0; i < pos.size() / 2; ++i) {
			Point tmp = pos.get(i);
			pos.set(i, pos.get(pos.size() - 1 - i));
			pos.set(pos.size() - 1 - i, tmp);
		}
		//		Collections.sort(pos, pointSorter);
	}

	boolean calcScore(ArrayList<Point> pos) {
		//		rot180(pos);
		boolean update = calcScore_(pos);
		//		rot180(pos);
		//		if (N * H < 30) {
		//			rot180(pos);
		//			update |= calcScore_(pos);
		//			rot180(pos);
		//		}
		return update;
	}

	boolean calcScore_(ArrayList<Point> pos) {
		if (pos.isEmpty()) return false;
		int t = pos.size();
		int T = pos.size() * 4;
		ArrayList<Point> tilt = new ArrayList<Point>(t);
		for (int i = 0; i < t; ++i) {
			tilt.add(new Point(N - 1 - pos.get(i).y, pos.get(i).x));
		}
		Collections.sort(tilt, pointSorter);
		for (int i = 0; i < t; ++i) {
			decoded[i] = input[pos.get(i).y][pos.get(i).x];
		}
		for (int i = 0; i < t; ++i) {
			decoded[t + i] = input[tilt.get(i).y][tilt.get(i).x];
		}
		for (int i = 0; i < t; ++i) {
			decoded[2 * t + i] = input[N - 1 - pos.get(t - 1 - i).y][N - 1 - pos.get(t - 1 - i).x];
		}
		for (int i = 0; i < t; ++i) {
			decoded[3 * t + i] = input[N - 1 - tilt.get(t - 1 - i).y][N - 1 - tilt.get(t - 1 - i).x];
		}
		//		for (int i = 0; i < T; ++i) {
		//			System.out.print((char) (decoded[i] + 'A'));
		//		}
		//		System.out.println();
		boolean[][] cov = new boolean[T][15];
		for (int i = 0; i < T; i++) {
			Trie.Node cur = trie.root;
			for (int j = 0; j < 15 && i + j < T; j++) {
				if (cur.child == null) break;
				cur = cur.child[decoded[i + j]];
				if (cur == null) break;
				cov[i][j] = cur.stop;
			}
		}

		double newval;
		double[][] sc = new double[T][T + 1];
		boolean[][] from = new boolean[T][T + 1];
		sc[0][0] = PENALTY;
		from[0][0] = true;
		for (int i = 1; i < T; i++) {
			sc[i][0] = (i + 1) * PENALTY;
			from[i][0] = true;
			for (int j = 0; j <= i; j++) {
				if (from[i - 1][j] && sc[i - 1][j] + powTable[j] + PENALTY > sc[i][0]) {
					sc[i][0] = sc[i - 1][j] + powTable[j] + PENALTY;
					from[i][0] = true;
				}
			}
			for (int k = 3; k < 15 && i - k >= 0; k++) {
				if (cov[i - k][k]) {
					if (i - k == 0) {
						sc[i][k + 1] = 0;
						from[i][k + 1] = true;
					} else {
						for (int j = 0; j <= i - k; j++) {
							if (from[i - k - 1][j]) {
								newval = sc[i - k - 1][j];
								if (!from[i][j + k + 1] || sc[i][j + k + 1] <= newval) {
									sc[i][j + k + 1] = newval;
									from[i][j + k + 1] = true;
								}
							}
						}
					}
				}
			}
		}
		double bestVal = sc[T - 1][0];
		for (int j = 1; j <= T; j++) {
			if (from[T - 1][j]) {
				bestVal = Math.max(bestVal, sc[T - 1][j] + powTable[j]);
			}
		}
		if (bestVal > bestScore) {
			bestScore = bestVal;
			bestPos.clear();
			for (Point p : pos) {
				bestPos.add(new Point(p.x, p.y));
			}
			return true;
		} else {
			return false;
		}
	}

	class State implements Comparable<State> {
		ArrayList<Point> pos = new ArrayList<Point>();
		Trie.Node cur;
		int[] rc = new int[N];
		int[] cc = new int[N];
		int ccsqsum = 0;

		State() {
			cur = trie.root;
		}

		State(State parent, int r, int c) {
			this.pos.ensureCapacity(parent.pos.size() + 1);
			for (int i = 0; i < parent.pos.size(); ++i) {
				this.pos.add(parent.pos.get(i));
				this.rc[parent.pos.get(i).y]++;
				this.cc[parent.pos.get(i).x]++;
			}
			this.pos.add(new Point(c, r));
			this.rc[r]++;
			this.cc[c]++;
			this.ccsqsum = parent.ccsqsum + this.cc[c] * 2 - 1;
			this.cur = parent.cur.child[input[r][c]];
			if (cur.child == null) {
				cur = trie.root;
			}
		}

		public int compareTo(State o) {
			if (this.pos.size() != o.pos.size()) {
				return this.pos.size() - o.pos.size();
			}
			return -this.ccsqsum + o.ccsqsum;
		}

	}

	static class PointSorter implements Comparator<Point> {
		public int compare(Point l, Point r) {
			if (l.y != r.y) return l.y - r.y;
			return l.x - r.x;
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		String[] grid = new String[N];
		for (int i = 0; i < N; ++i) {
			grid[i] = sc.next();
		}
		int H = sc.nextInt();
		String[] grille = new GrilleReconstruction().bestGrille(grid, H);
		for (int i = 0; i < N; ++i) {
			System.out.println(grille[i]);
		}
		System.out.flush();
	}

	static void debug(Object o) {
		if (DEBUG) {
			System.out.println(o);
		}
	}

}

class Trie {
	static class Node {
		Node[] child = null;
		int depth = 0;
		boolean stop = false;

		void add(String str, int i) {
			if (i == str.length()) {
				stop = true;
				return;
			}
			int idx = str.charAt(i) - 'A';
			if (child == null) {
				child = new Node[26];
			}
			if (child[idx] == null) {
				child[idx] = new Node();
				child[idx].depth = this.depth + 1;
			}
			child[idx].add(str, i + 1);
		}
	}

	Node root = new Node();

	void add(String[] dic) {
		for (String word : dic) {
			root.add(word, 0);
		}
	}

}