#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <boost/lexical_cast.hpp>
#include "DATCompression.h"

using namespace std;
typedef vector<int> vi;

double get_time() {
  timeval tv;
  gettimeofday(&tv, 0);
  return tv.tv_sec + tv.tv_usec*1e-6;
}

struct Result{
  int seed;
  int originalSize;
  int compressSize;
  double initTime;
  double comTime;
  double decTime;
  double score;
};

ostream& operator<<(ostream& stm, const Result& res){
  stm << "seed:" << res.seed << endl;
  stm << "osize:" << res.originalSize << endl;
  stm << "csize:" << res.compressSize << endl;
  stm << "comTime:" << res.comTime << endl;
  stm << "decTime:" << res.decTime << endl;
  stm << "score:" << res.score << endl;
  return stm;
} 

vector<int> read(int seed){
  vector<int> ret;
  ret.reserve(1000000);
  string filename = "testdata/" + boost::lexical_cast<string>(seed) + ".dat";
  FILE* file = fopen(filename.c_str(), "r");
  int X,Y,L;
  fscanf(file, "%d %d %d", &X, &Y, &L);
  ret.push_back(X);
  ret.push_back(Y);
  ret.push_back(L);
  for (int i = 0; i < X*Y*L; ++i) {
    int v;
    fscanf(file, "%d", &v);
    ret.push_back(v);
  }
  return ret;
}

Result test(int seed){
  Result res = {seed, 0, 0, 0, 0, 0, 0};
  vector<int> data = read(seed);
  DATCompression obj;
  {
    double start = get_time();
    obj.init();
    res.initTime += get_time() - start;
  }
  res.originalSize = data.size();
//   int X = data[0];
//   int Y = data[1];
//   int L = data[2];
//   int pos = 3;
//   for (int i = 0; i < X*Y; ++i) {
//     int cur = data[pos++];
//     for (int j = 1; j < L; ++j) {
//       cout << data[pos] - cur << "\t";
//       cur = data[pos++];
//     }
//     cout << endl;
//   }
//   for (int i = 0; i < L-1; ++i) {
//     int pos = 3 + i;
//     for (int j = 0; j < Y; ++j) {
//       for (int k = 0; k < X; ++k) {
// 	cout << data[pos+1] - data[pos] << "\t";
// 	pos += L;
//       }
//       cout << endl;
//     }
//     cout << endl;
//   }
  for (int i = 0; i < 200; ++i) {
    double start = get_time();
    vector<int> compressed = obj.compress(data);
//     cout << "compressed:" << endl;
//     for (size_t j = 0; j < compressed.size(); ++j) {
//       cout << setbase(16) << compressed[j] << " ";
//     }
//     cout << setbase(10);
//     cout << endl;
    res.comTime += get_time() - start;
    start = get_time();
//     cout << "compressed size:" << compressed.size() << endl;
    vector<int> expanded = obj.decompress(compressed);
//     cout << "expanded:" << endl;
//     for (size_t j = 0; j < expanded.size(); ++j) {
//       cout << setbase(10) << expanded[j] << " ";
//     }
//     cout << endl;
    res.decTime += get_time() - start;
    if (expanded.size() != data.size()) {
      cerr << "different size" << endl;
      res.score = 0;
      return res;
    }
    if (i == 0) for (size_t j = 0; j < data.size(); ++j) {
      if (expanded[j] != data[j]) {
	cerr << "different at " << j << endl;
	res.score = 0;
	return res;
      }
    }
    res.compressSize = compressed.size();
  }
  res.score = 2.0 * res.originalSize / res.compressSize
    + 1.5 * pow(2.5 / max(2.5, (res.comTime+res.decTime) * 2.1), 0.75);
  return res;
}

int main(int argc, char** argv){
  int seed = 1;
  // example test cases: 19 9 5 12 11
  for (int i = 1; i < argc; ++i) {
    if (string(argv[i]) == string("-seed")) {
      seed = atoi(argv[++i]);
    }
  }
  Result res = test(seed);
  cout << res << endl;
  return 0;
}
