#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <set>
#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <tuple>
#include <sys/time.h>

#ifndef LOCAL
#define NDEBUG
#endif
#include <cassert>

using namespace std;
typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t ll;
typedef uint64_t ull;

#ifndef TEST
#define MEASURE_TIME 1
#define DEBUG 1
#endif

namespace {
const int DR[] = {-1, 0, 1, 0};
const int DC[] = {0, 1, 0, -1};
const string DIR_STR = "URDL";

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 12000;
#else
#ifdef TEST
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 12000;
#else
const double CLOCK_PER_SEC = 3.6e9;
const ll TL = 14500;
#endif
#endif

// msec
inline ll getTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

const u8 EMPTY = 255;
const u8 SENTINEL = 99;
const int VAL_ADJ[] = {0, 1, 8, 10, 26};
const int VAL_ADJ2[] = {0, 0, 4, 10, 32};
typedef vector<vector<u8>> Field;
struct Board;

XorShift rnd;
ll startTime;
Timer timer;
Counter counter;
u64 hv[11][64][64];
int edgePena[64][64];
int evalAmp[64][64];
int visited[64][64];
int visitedIdx;

struct Move {
	int r,c;
	vector<int> dir;
};

ostream& operator<<(ostream& os, const Move& move) {
	os << (move.r-2) << " " << (move.c-2) << " ";
	for (int d : move.dir) {
		os << DIR_STR[d];
	}
	return os;
}

pair<int, int> endPos(const Move& move) {
	int r = move.r;
	int c = move.c;
	for (int d : move.dir) {
		r += 2*DR[d];
		c += 2*DC[d];
	}
	return {r, c};
}

vector<Move> mergeMoves(const vector<Move>& moves) {
	vector<Move> ret;
	if (moves.empty()) return ret;
	ret.push_back(moves[0]);
	auto pos = endPos(moves[0]);
	for (int i = 1; i < moves.size(); ++i) {
		const Move& move = moves[i];
		if (pos == make_pair(move.r, move.c)) {
			vector<int>& dir = ret.back().dir;
			dir.insert(dir.end(), move.dir.begin(), move.dir.end());
		} else {
			ret.push_back(move);
		}
		pos = endPos(move);
	}
	return ret;
}

typedef u32 SMove;
inline SMove createSMove(int r, int c, int d) { return (r << 16) | (c << 8) | d;} 
inline int getSMoveR(SMove move) { return (move >> 16); }
inline int getSMoveC(SMove move) { return (move >> 8) & 0xFF; }
inline int getSMoveD(SMove move) { return (move & 0xFF); }

int N;
int parityCnt[4];
Field initField, bestField;
int bestScore, bestKeyScore, bestLen;
vector<Move> ans;
int turn;
int pegs;

struct Board {
	Field f;
	vector<SMove> moves;
	int val;
	u64 hash;

	Board() : val(0), hash(0) {}

	Board(const Field& origField) : f(origField), val(0), hash(0) {}

	Board(const Board& pv, const SMove& move, int newVal) : f(pv.f), moves(pv.moves), val(newVal), hash(pv.hash) {
		moves.push_back(move);
		const int r = getSMoveR(move);
		const int c = getSMoveC(move);
		const int d = getSMoveD(move);
		u8 peg = f[r][c];
		u8 jumped =  f[r + DR[d]][c + DC[d]];
		hash ^= hv[peg][r][c];
		hash ^= hv[jumped][r + DR[d]][c + DC[d]];
		hash ^= hv[peg][r + 2*DR[d]][c + 2*DC[d]];
		f[r][c] = EMPTY;
		f[r + DR[d]][c + DC[d]] = EMPTY;
		f[r + 2*DR[d]][c + 2*DC[d]] = peg;
	}
};

bool validate(Field field, const Move& move) {
	int r = move.r;
	int c = move.c;
	if (field[r][c] == EMPTY) {
		debug("cannot jump from empty cell (%d, %d)\n", r, c);
		return false;
	}
	for (int d : move.dir) {
		field[r][c] = EMPTY;
		if (field[r+DR[d]][c+DC[d]] == EMPTY) {
			debug("cannot jump over empty cell (%d, %d) %d\n", r, c, d);
			return false;
		}
		if (field[r+2*DR[d]][c+2*DC[d]] != EMPTY) {
			debug("cannot land at not empty cell (%d, %d) %d\n", r, c, d);
			return false;
		}
		field[r+DR[d]][c+DC[d]] = EMPTY;
		r += 2*DR[d];
		c += 2*DC[d];
	}
	return true;
}

bool validate(Field field, const vector<Move>& moves) {
	for (Move move : moves) {
		if (!validate(field, move)) {
			cerr << move << endl;
			return false;
		}
	}
	return true;
}

void printField(const Field& field) {
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			u8 v = field[i+2][j+2];
			char c = (char)(0 <= v && v <= 9 ? v + '0' : '.');
			cerr << c;
		}
		cerr << endl;
	}
}

void apply(Field& f, const Move& move) {
	int r = move.r;
	int c = move.c;
	int peg = f[r][c];
	assert(peg != EMPTY);
	f[r][c] = EMPTY;
	for (int d : move.dir) {
		assert(f[r + DR[d]][c + DC[d]] != EMPTY);
		f[r + DR[d]][c + DC[d]] = EMPTY;
		r += 2*DR[d];
		c += 2*DC[d];
		assert(f[r][c] == EMPTY);
	}
	f[r][c] = peg;
}

typedef tuple<int, const Board*, SMove> Hand;

u64 getHash(const Hand& hand) {
	const Board* b = get<1>(hand);
	SMove move = get<2>(hand);
	const int r = getSMoveR(move);
	const int c = getSMoveC(move);
	const int d = getSMoveD(move);
	u64 hash = b->hash;
	u8 peg = (b->f)[r][c];
	u8 jumped =  (b->f)[r + DR[d]][c + DC[d]];
	hash ^= hv[peg][r][c];
	hash ^= hv[jumped][r + DR[d]][c + DC[d]];
	hash ^= hv[peg][r + 2*DR[d]][c + 2*DC[d]];
	return hash;
}

u32 searchSingle(Field& field, int r, int c, vector<int>* dir) {
	int sum = 0;
	int peg = field[r][c];
	field[r][c] = EMPTY;
	vector<int> jumped;
	for (int len = 0; ; ++len) {
		int dBase = rnd.nextUInt(4);
		bool found = false;
		for (int i = 0; i < 4; ++i) {
			int d = (i + dBase) % 4;
			u8 rem = field[r + DR[d]][c + DC[d]];
			if (rem != EMPTY && field[r + 2*DR[d]][c + 2*DC[d]] == EMPTY) {
				sum += rem;
				jumped.push_back(rem);
				field[r + DR[d]][c + DC[d]] = EMPTY;
				r += 2*DR[d];
				c += 2*DC[d];
				dir->push_back(d);
				found = true;
				break;
			}
		}
		if (!found) {
			for (int i = len - 1; i >= 0; --i) {
				int d = (*dir)[i];
				field[r - DR[d]][c - DC[d]] = jumped[i];
				r -= 2*DR[d];
				c -= 2*DC[d];
			}
			field[r][c] = peg;
			return len * sum;
		}
	}
}

u32 findInitialMove(Field& field, int r, int c, vector<int>* dir) {
	vector<vector<int>> prev(N+4, vector<int>(N+4, -1));
	vector<pair<int, int>> cur;
	cur.emplace_back(r, c);
	pair<int, int> last;
	while (true) {
		vector<pair<int, int>> next;
		for (auto pos : cur) {
			for (int i = 0; i < 4; ++i) {
				if (field[pos.first + DR[i]][pos.second + DC[i]] == EMPTY) continue;
				int nr = pos.first + 2 * DR[i];
				int nc = pos.second + 2 * DC[i];
				if (field[nr][nc] != EMPTY || prev[nr][nc] != -1) continue;
				next.emplace_back(nr, nc);
				prev[nr][nc] = i;
			}
		}
		if (next.empty()) {
			last = cur[rnd.nextUInt(cur.size())];
			break;
		}
		swap(cur, next);
	}
	{
		int cr = last.first;
		int cc = last.second;
		int sum = 0;
		while (cr != r || cc != c) {
			dir->push_back(prev[cr][cc]);
			int d = (prev[cr][cc] + 2) % 4;
			assert(field[cr + DR[d]][cc + DC[d]] != EMPTY);
			sum += field[cr + DR[d]][cc + DC[d]];
			field[cr + DR[d]][cc + DC[d]] = EMPTY;
			cr += 2 * DR[d];
			cc += 2 * DC[d];
		}
		assert(field[cr][cc] != EMPTY);
		field[last.first][last.second] = field[cr][cc];
		field[cr][cc] = EMPTY;
		reverse(dir->begin(), dir->end());
		return dir->size() * sum;
	}
}

const int MAX_PATH_LEN = 14;
const int MAX_SKIP_LEN = 8;
static int pathSum = 0;
static int pathLen = 0;
static int pathDir[MAX_PATH_LEN];
static int pathSkip;
static int skipLen;
static int skipPos[MAX_SKIP_LEN];
static bool pathExtend;
static const double INITIAL_COOL = 0.001;
static const double MAX_COOL = 99999;
static double COOL;

bool transit(u32 curScore, u32 newScore) {
	if (curScore <= newScore) return true;
	const double diff = 1.0 * newScore - curScore;
	if (rnd.nextDouble() < exp(COOL * diff / sqrt(curScore))) return true; 
	return false;
}

bool findPath(Field& field, int cr, int cc, int level, int sum) {
	if (level != 0) {
		int pos = (cr << 8) | cc;
		for (int i = (level & 1); i < skipLen; i += 2) {
			if (pos == skipPos[i]) {
				pathSum = sum;
				pathLen = level;
				pathSkip = i;
				return true;
			}
		}
		if (level == MAX_PATH_LEN) return false;
	}

	int dirBase = rnd.nextUInt(4);
	bool proceed = false;
	for (int i = 0; i < 4; ++i) {
		int d = (i + dirBase) % 4;
		u8 jp = field[cr + DR[d]][cc + DC[d]];
		if (jp == EMPTY) continue;
		pathDir[level] = d;
		if (field[cr + 2 * DR[d]][cc + 2 * DC[d]] != EMPTY) continue;
		field[cr + DR[d]][cc + DC[d]] = EMPTY;
		bool result = findPath(field, cr + 2 * DR[d], cc + 2 * DC[d], level + 1, sum + jp);
		proceed = true;
		field[cr + DR[d]][cc + DC[d]] = jp;
		if (result) return true;
	}
	if (skipLen < MAX_SKIP_LEN && level >= skipLen && !proceed) {
		pathSum = sum;
		pathLen = level;
		pathSkip = skipLen - 1;
		pathExtend = true;
		return true;
	}
	return false;
}

u32 improve(const Field& origField, Field& field, int r, int c, vector<int>& dir, u32 score) {
	vector<int> optDir = dir;
	u32 optScore = score;
	u32 curSum = score / dir.size();
	COOL = INITIAL_COOL;
	int lastUpdateTurn = 0;
	for (int improveTurn = 0; improveTurn < 100000; ++improveTurn) {
		if (improveTurn == lastUpdateTurn + 300) {
			if (COOL == MAX_COOL) {
				break;
			} else if (COOL > 2) {
				COOL = MAX_COOL;
			} else {
				COOL *= 1.5;
			}
			// debug("cool:%f at turn:%d\n", COOL, improveTurn);
			lastUpdateTurn = improveTurn;
		}
		int pos = rnd.nextUInt(dir.size() + 1);
		int cr = r;
		int cc = c;
		for (int i = 0; i < pos; ++i) {
			cr += 2*DR[dir[i]];
			cc += 2*DC[dir[i]];
		}
		skipLen = min(MAX_SKIP_LEN, (int)dir.size() - pos + 1);
		skipPos[0] = (cr << 8) | cc;
		for (int i = 1; i < skipLen; ++i) {
			int skipR = skipPos[i-1] >> 8;
			int skipC = skipPos[i-1] & 0xFF;
			skipR += 2*DR[dir[pos + i - 1]];
			skipC += 2*DC[dir[pos + i - 1]];
			skipPos[i] = (skipR << 8) | skipC;
		}
		pathExtend = false;
		if (!findPath(field, cr, cc, 0, 0)) continue;
		int newLen = dir.size() + (pathLen - pathSkip);
		int newSum = curSum + pathSum;
		{
			int revertR = cr;
			int revertC = cc;
			for (int i = 0; i < pathSkip; ++i) {
				int d = dir[pos + i];
				newSum -= origField[revertR + DR[d]][revertC + DC[d]];
				revertR += 2*DR[d];
				revertC += 2*DC[d];
			}
		}
		u32 newScore = newLen * newSum;
		if (transit(score, newScore)) {
			int jr = cr;
			int jc = cc;
			for (int i = 0; i < pathLen; ++i) {
				int d = pathDir[i];
				assert(field[jr + DR[d]][jc + DC[d]] != EMPTY);
				field[jr + DR[d]][jc + DC[d]] = EMPTY;
				jr += 2*DR[d];
				jc += 2*DC[d];
			}
			for (int i = 0; i < pathSkip; ++i) {
				int d = dir[pos + i];
				assert(origField[cr + DR[d]][cc + DC[d]] != EMPTY);
				field[cr + DR[d]][cc + DC[d]] = origField[cr + DR[d]][cc + DC[d]];
				cr += 2*DR[d];
				cc += 2*DC[d];
			}
			if (pathExtend) {
				// debug("extend:%d %d %d %d\n", pathSkip, pathLen, (int)dir.size(), pos);
				u8 endPeg = field[cr][cc];
				assert(endPeg != EMPTY);
				assert(field[jr][jc] == EMPTY);
				field[cr][cc] = EMPTY;
				field[jr][jc] = endPeg;
			}
			assert(pathExtend || jr == cr);
			assert(pathExtend || jc == cc);
			dir.erase(dir.begin() + pos, dir.begin() + pos + pathSkip);
			dir.insert(dir.begin() + pos, pathDir, pathDir + pathLen);
			score = newScore;
			curSum = newSum;
			if (score > optScore) {
				// debug("score:%d optScore:%d pathLen:%d turn:%d\n", score, optScore, newLen, improveTurn);
				optScore = score;
				optDir = dir;
				lastUpdateTurn = improveTurn;
			}
		}
	}
	dir = optDir;
	return optScore;
}

int searchKeySeq(const Board& board, int sr, int sc) {
	u32 score;
	const Field& origField = board.f;
	Field field = board.f;
	vector<int> dir;
	score = findInitialMove(field, sr, sc, &dir);
	if (score == 0) return 0;
	START_TIMER(7);
	score = improve(origField, field, sr, sc, dir, score);
	STOP_TIMER(7);
	if (score > bestKeyScore) {
		debug("bestKeyScore:%d keyLen:%d val:%d parity:%d\n", score, (int)dir.size(), board.val, (sr & 1) + ((sc & 1) << 1));
		bestKeyScore = score;
		bestLen = dir.size();
		const Move bestMove = {sr, sc, dir};
		bestField = board.f;
		apply(bestField, bestMove);
		ans.clear();
		for (const SMove& move : board.moves) {
			ans.push_back({getSMoveR(move), getSMoveC(move), vector<int>(1, getSMoveD(move))});
		}
		ans.push_back(bestMove);
	}
	return score;
}

int countEdge(const Field& f, int r, int c) {
	int cnt = 0;
	if (f[r + 1][c] != EMPTY && f[r + 2][c] == EMPTY) ++cnt;
	if (f[r - 1][c] != EMPTY && f[r - 2][c] == EMPTY) ++cnt;
	if (f[r][c + 1] != EMPTY && f[r][c + 2] == EMPTY) ++cnt;
	if (f[r][c - 1] != EMPTY && f[r][c - 2] == EMPTY) ++cnt;
	return cnt;
}

template<int d>
int eval1(Field& f, int r, int c) {
	int diff = 0;
	{
		int dir = (d + 1) & 3;
		if (f[r + DR[dir]][c + DC[dir]] != EMPTY && f[r + 2*DR[dir]][c + 2*DC[dir]] == EMPTY) {
			int cnt = countEdge(f, r + 2*DR[dir], c + 2*DC[dir]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[r + 2*DR[dir]][c + 2*DC[dir]];
		}
	}
	{
		int dir = (d + 3) & 3;
		if (f[r + DR[dir]][c + DC[dir]] != EMPTY && f[r + 2*DR[dir]][c + 2*DC[dir]] == EMPTY) {
			int cnt = countEdge(f, r + 2*DR[dir], c + 2*DC[dir]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[r + 2*DR[dir]][c + 2*DC[dir]];
		}
	}
	{
		if (f[r + 2*DR[d]][c + 2*DC[d]] == EMPTY) {
			int cnt = countEdge(f, r + 2*DR[d], c + 2*DC[d]);
			diff += VAL_ADJ[cnt+1] - VAL_ADJ[cnt];
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[r + 2*DR[d]][c + 2*DC[d]];
		}
	}
	const int sr = r - DR[d];
	const int sc = c - DC[d];
	u8 peg = f[sr][sc];
	f[sr][sc] = EMPTY;
	f[r + DR[d]][c + DC[d]] = peg;
	diff += VAL_ADJ[countEdge(f, r, c)] * evalAmp[r][c];
	f[sr][sc] = peg;
	f[r + DR[d]][c + DC[d]] = EMPTY;
	diff += edgePena[r + DR[d]][c + DC[d]] - edgePena[sr][sc];
	return diff;
}

template<int d>
int eval2(Field& f, int r, int c) {
	int diff = 0;
	int d1 = (d + 1) & 3;
	int d2 = (d + 3) & 3;
	{
		int nr = r + DR[d];
		int nc = c + DC[d];
		if (f[nr + DR[d1]][nc + DC[d1]] == EMPTY && f[nr + DR[d2]][nc + DC[d2]] == EMPTY) {
			int cnt = countEdge(f, nr + DR[d1], nc + DC[d1]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d1]][nc + DC[d1]];
			cnt = countEdge(f, nr + DR[d2], nc + DC[d2]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d2]][nc + DC[d2]];
		}
	}
	{
		int nr = r - DR[d];
		int nc = c - DC[d];
		if (f[nr + DR[d1]][nc + DC[d1]] == EMPTY && f[nr + DR[d2]][nc + DC[d2]] == EMPTY) {
			int cnt = countEdge(f, nr + DR[d1], nc + DC[d1]);
			diff += VAL_ADJ[cnt-1] - VAL_ADJ[cnt];
			diff += (VAL_ADJ[cnt-1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d1]][nc + DC[d1]];
			cnt = countEdge(f, nr + DR[d2], nc + DC[d2]);
			diff += (VAL_ADJ[cnt-1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d2]][nc + DC[d2]];
		}
	}
	diff += edgePena[r + DR[d]][c + DC[d]] - edgePena[r - DR[d]][c - DC[d]];
	return diff;
}

int eval1(Field& f, int r, int c, int d) {
	int diff = 0;
	{
		int dir = (d + 1) & 3;
		if (f[r + DR[dir]][c + DC[dir]] != EMPTY && f[r + 2*DR[dir]][c + 2*DC[dir]] == EMPTY) {
			int cnt = countEdge(f, r + 2*DR[dir], c + 2*DC[dir]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[r + 2*DR[dir]][c + 2*DC[dir]];
		}
	}
	{
		int dir = (d + 3) & 3;
		if (f[r + DR[dir]][c + DC[dir]] != EMPTY && f[r + 2*DR[dir]][c + 2*DC[dir]] == EMPTY) {
			int cnt = countEdge(f, r + 2*DR[dir], c + 2*DC[dir]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[r + 2*DR[dir]][c + 2*DC[dir]];
		}
	}
	{
		if (f[r + 2*DR[d]][c + 2*DC[d]] == EMPTY) {
			int cnt = countEdge(f, r + 2*DR[d], c + 2*DC[d]);
			diff += VAL_ADJ[cnt+1] - VAL_ADJ[cnt];
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[r + 2*DR[d]][c + 2*DC[d]];
		}
	}
	const int sr = r - DR[d];
	const int sc = c - DC[d];
	u8 peg = f[sr][sc];
	f[sr][sc] = EMPTY;
	f[r + DR[d]][c + DC[d]] = peg;
	diff += VAL_ADJ[countEdge(f, r, c)] * evalAmp[r][c];
	f[sr][sc] = peg;
	f[r + DR[d]][c + DC[d]] = EMPTY;
	return diff;
}

int eval2(Field& f, int r, int c, int d) {
	int diff = 0;
	int d1 = (d + 1) & 3;
	int d2 = (d + 3) & 3;
	{
		int nr = r + DR[d];
		int nc = c + DC[d];
		if (f[nr + DR[d1]][nc + DC[d1]] == EMPTY && f[nr + DR[d2]][nc + DC[d2]] == EMPTY) {
			int cnt = countEdge(f, nr + DR[d1], nc + DC[d1]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d1]][nc + DC[d1]];
			cnt = countEdge(f, nr + DR[d2], nc + DC[d2]);
			diff += (VAL_ADJ[cnt+1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d2]][nc + DC[d2]];
		}
	}
	{
		int nr = r - DR[d];
		int nc = c - DC[d];
		if (f[nr + DR[d1]][nc + DC[d1]] == EMPTY && f[nr + DR[d2]][nc + DC[d2]] == EMPTY) {
			int cnt = countEdge(f, nr + DR[d1], nc + DC[d1]);
			diff += VAL_ADJ[cnt-1] - VAL_ADJ[cnt];
			diff += (VAL_ADJ[cnt-1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d1]][nc + DC[d1]];
			cnt = countEdge(f, nr + DR[d2], nc + DC[d2]);
			diff += (VAL_ADJ[cnt-1] - VAL_ADJ[cnt]) * evalAmp[nr + DR[d2]][nc + DC[d2]];
		}
	}
	return diff;
}

int eval3(Field& f, int r, int c, int d) {
	int diff = 0;
	int dr = (d + 1) & 3;
	int dl = (d + 3) & 3;
	{
		int nr = r + DR[d];
		int nc = c + DC[d];
		diff -= VAL_ADJ[countEdge(f, nr, nc)];
		if (f[nr + DR[dr]][nc + DC[dr]] != EMPTY && f[nr + 2*DR[dr]][nc + 2*DC[dr]] == EMPTY) {
			int cnt = countEdge(f, nr + 2*DR[dr], nc + 2*DC[dr]);
			diff += VAL_ADJ[cnt-1] - VAL_ADJ[cnt];
		}
		if (f[nr + DR[d]][nc + DC[d]] != EMPTY && f[nr + 2*DR[d]][nc + 2*DC[d]] == EMPTY) {
			int cnt = countEdge(f, nr + 2*DR[d], nc + 2*DC[d]);
			diff += VAL_ADJ[cnt-1] - VAL_ADJ[cnt];
		}
		if (f[nr + DR[dl]][nc + DC[dl]] != EMPTY && f[nr + 2*DR[dl]][nc + 2*DC[dl]] == EMPTY) {
			int cnt = countEdge(f, nr + 2*DR[dl], nc + 2*DC[dl]);
			diff += VAL_ADJ[cnt-1] - VAL_ADJ[cnt];
		}
	}
	{
		int nr = r - DR[d];
		int nc = c - DC[d];
		diff += VAL_ADJ[countEdge(f, nr, nc)-1];
		if (f[nr - DR[dr]][nc - DC[dr]] != EMPTY && f[nr - 2*DR[dr]][nc - 2*DC[dr]] == EMPTY) {
			int cnt = countEdge(f, nr - 2*DR[dr], nc - 2*DC[dr]);
			diff += VAL_ADJ[cnt+1] - VAL_ADJ[cnt];
		}
		if (f[nr - DR[d]][nc - DC[d]] != EMPTY && f[nr - 2*DR[d]][nc - 2*DC[d]] == EMPTY) {
			int cnt = countEdge(f, nr - 2*DR[d], nc - 2*DC[d]);
			diff += VAL_ADJ[cnt+1] - VAL_ADJ[cnt];
		}
		if (f[nr - DR[dl]][nc - DC[dl]] != EMPTY && f[nr - 2*DR[dl]][nc - 2*DC[dl]] == EMPTY) {
			int cnt = countEdge(f, nr - 2*DR[dl], nc - 2*DC[dl]);
			diff += VAL_ADJ[cnt+1] - VAL_ADJ[cnt];
		}
	}
	return diff;
}

bool canMove(const Field& f, int parity) {
	for (int r = 2 + (parity & 1); r < N+ 2; r += 2) {
		for (int c = 2 + (parity >> 1); c < N+ 2; c += 2) {
			if (f[r][c] == EMPTY) continue;
			for (int d = 0; d < 4; ++d) {
				if (f[r + DR[d]][c + DC[d]] != EMPTY && f[r + 2 * DR[d]][c + 2 * DC[d]] == EMPTY) return true;
			}
		}
	}
	return false;
}

int countMaxConnectScore(const Field& f, int parity) {
	int ret = 0;
	++visitedIdx;
	for (int r = 2 + (parity & 1); r < N + 2; r += 2) {
		for (int c = 2 + (parity >> 1); c < N + 2; c += 2) {
			if (f[r][c] != EMPTY || visited[r][c] == visitedIdx) continue;
			bool start = false;
			vector<int> cur = {(r << 8) + c};
			int pos = 0;
			int sum = 0;
			while (pos < cur.size()) {
				int cr = cur[pos] >> 8;
				int cc = cur[pos] & 0xFF;
				int ord = 0;
				for (int d = 0; d < 4; ++d) {
					if (f[cr + DR[d]][cc + DC[d]] == EMPTY) continue;
					int nr = cr + 2 * DR[d];
					int nc = cc + 2 * DC[d];
					++ord;
					if (f[nr][nc] == EMPTY) {
						if (visited[nr][nc] != visitedIdx) {
							cur.push_back((nr << 8) + nc);
							visited[nr][nc] = visitedIdx;
						}
					} else {
						start = true;
					}
				}
				sum += VAL_ADJ2[ord];
				++pos;
			}
			if (start) {
				ret = max(ret, sum);
			}
		}
	}
	return ret;
}

int BEAM_WIDTH;
int BEST_WIDTH;
int RETURN_SIZE;

void enumerateCandidateBoards(int parity, vector<Board>& bestBoards) {
	START_TIMER(0);
	const int evenodd = (parity & 1) ^ (parity >> 1);
	int removePegCnt = 0;
	for (int r = 2; r < N + 2; ++r) {
		for (int c = 2 + ((r & 1) ^ evenodd); c < N + 2; c += 2) {
			if (initField[r][c] == EMPTY) continue;
			if (initField[r-1][c] != EMPTY || initField[r][c-1] != EMPTY || initField[r+1][c] != EMPTY || initField[r][c+1] != EMPTY) ++removePegCnt;
		}
	}
	const int BEST_STORE_TURN = (int)(removePegCnt * 0.55);
	debug("BEST_STORE_TURN:%d\n", BEST_STORE_TURN);
	bestBoards.reserve(RETURN_SIZE);
	vector<vector<Board>> bestBoardsCand;
	vector<Board> curBoards;
	curBoards.emplace_back(initField);
	auto handSorter = [](const Hand& h1, const Hand& h2){return get<0>(h1) > get<0>(h2);};
	auto boardSorter = [](const Board* b1, const Board* b2){return b1->val > b2->val;};
	STOP_TIMER(0);
	for (turn = 0; !curBoards.empty(); ++turn) {
		vector<Hand> hands;
		START_TIMER(1);
		for (Board& b : curBoards) {
			vector<Hand> curHands;
			for (int r = 2 + (parity & 1); r < N+2; r += 2) {
				for (int c = 3 - ((r & 1) ^ evenodd); c < N+2; c += 2) {
					if (b.f[r][c] == EMPTY) continue;
					if (b.f[r + DR[0]][c + DC[0]] != EMPTY && b.f[r + 2*DR[0]][c + 2*DC[0]] == EMPTY) {
						int value = eval2<0>(b.f, r + DR[0], c + DC[0]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 0));
					}
					if (b.f[r + DR[1]][c + DC[1]] != EMPTY && b.f[r + 2*DR[1]][c + 2*DC[1]] == EMPTY) {
						int value = eval1<1>(b.f, r + DR[1], c + DC[1]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 1));
					}
					if (b.f[r + DR[2]][c + DC[2]] != EMPTY && b.f[r + 2*DR[2]][c + 2*DC[2]] == EMPTY) {
						int value = eval2<2>(b.f, r + DR[2], c + DC[2]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 2));
					}
					if (b.f[r + DR[3]][c + DC[3]] != EMPTY && b.f[r + 2*DR[3]][c + 2*DC[3]] == EMPTY) {
						int value = eval1<3>(b.f, r + DR[3], c + DC[3]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 3));
					}
				}
			}
			for (int r = 3 - (parity & 1); r < N+2; r += 2) {
				for (int c = 3 - ((r & 1) ^ evenodd); c < N+2; c += 2) {
					if (b.f[r][c] == EMPTY) continue;
					if (b.f[r + DR[0]][c + DC[0]] != EMPTY && b.f[r + 2*DR[0]][c + 2*DC[0]] == EMPTY) {
						int value = eval1<0>(b.f, r + DR[0], c + DC[0]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 0));
					}
					if (b.f[r + DR[1]][c + DC[1]] != EMPTY && b.f[r + 2*DR[1]][c + 2*DC[1]] == EMPTY) {
						int value = eval2<1>(b.f, r + DR[1], c + DC[1]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 1));
					}
					if (b.f[r + DR[2]][c + DC[2]] != EMPTY && b.f[r + 2*DR[2]][c + 2*DC[2]] == EMPTY) {
						int value = eval1<2>(b.f, r + DR[2], c + DC[2]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 2));
					}
					if (b.f[r + DR[3]][c + DC[3]] != EMPTY && b.f[r + 2*DR[3]][c + 2*DC[3]] == EMPTY) {
						int value = eval2<3>(b.f, r + DR[3], c + DC[3]);
						curHands.emplace_back(value + b.val, &b, createSMove(r, c, 3));
					}
				}
			}
			const int MAX_CHILD_COUNT_FROM_SAME_PARENT = max(4, BEAM_WIDTH / 20);
			if (curBoards.size() > 10 && curHands.size() > MAX_CHILD_COUNT_FROM_SAME_PARENT) {
				nth_element(curHands.begin(), curHands.begin() + MAX_CHILD_COUNT_FROM_SAME_PARENT, curHands.end(), handSorter);
				hands.insert(hands.end(), curHands.begin(), curHands.begin() + MAX_CHILD_COUNT_FROM_SAME_PARENT);
			} else if (curHands.size() > BEAM_WIDTH) {
				nth_element(curHands.begin(), curHands.begin() + BEAM_WIDTH, curHands.end(), handSorter);
				hands.insert(hands.end(), curHands.begin(), curHands.begin() + BEAM_WIDTH);
			} else {
				hands.insert(hands.end(), curHands.begin(), curHands.end());
			}
		}
		STOP_TIMER(1);
		START_TIMER(2);
		if (hands.size() >= BEAM_WIDTH * 3) {
			partial_sort(hands.begin(), hands.begin() + BEAM_WIDTH * 3, hands.end(), handSorter);
		} else if (hands.size() > BEAM_WIDTH) {
			partial_sort(hands.begin(), hands.begin() + BEAM_WIDTH, hands.end(), handSorter);
		}
		vector<Board> nextBoards;
		nextBoards.reserve(BEAM_WIDTH);
		set<u64> hashes;
		// debug("turn:%d hands size:%d\n", turn, (int)hands.size());
		for (int i = 0; i < hands.size() && nextBoards.size() < BEAM_WIDTH; ++i) {
			const Hand& hand = hands[i];
			u64 h = getHash(hand);
			if (hashes.count(h) != 0) {
				ADD_COUNTER(0);
			} else {
				hashes.insert(h);
				nextBoards.emplace_back(*get<1>(hand), get<2>(hand), get<0>(hand));
				if (turn > N && !canMove(nextBoards.back().f, parity)) {
					ADD_COUNTER(2);
					nextBoards.erase(nextBoards.end() - 1);
				}
				ADD_COUNTER(1);
			}
		}
		STOP_TIMER(2);
		START_TIMER(3);
		if (turn >= BEST_STORE_TURN) {
			bestBoardsCand.push_back(vector<Board>(nextBoards.begin(), nextBoards.begin() + min(BEST_WIDTH, (int)nextBoards.size())));
		}
		swap(curBoards, nextBoards);
		STOP_TIMER(3);
	}
	START_TIMER(4);
	vector<const Board*> bestBoardsPtr;
	for (auto& cands : bestBoardsCand) {
		for (Board& cand : cands) {
			// cand.val = countMaxConnectScore(cand.f, parity);
			bestBoardsPtr.push_back(&cand);
		}
	}
	debug("bestBoardCand size:%d\n", (int)bestBoardsPtr.size());
	if (bestBoardsPtr.size() > RETURN_SIZE) {
		int border = RETURN_SIZE * 9 / 10;
		nth_element(bestBoardsPtr.begin(), bestBoardsPtr.begin() + border, bestBoardsPtr.end(), boardSorter);
		random_shuffle(bestBoardsPtr.begin() + border, bestBoardsPtr.end());
	}
	for (int i = 0; i < min(RETURN_SIZE, (int)bestBoardsPtr.size()); ++i) {
		bestBoards.push_back(*bestBoardsPtr[i]);
	}
	STOP_TIMER(4);
}

void cleanup() {
	debug("start cleanup:%lld\n", getTime() - startTime);
	vector<ull> initialExist(N);
	for (int r = 2; r < N+2; ++r) {
		for (int c = 2; c < N+2; ++c) {
			if (bestField[r][c] != EMPTY) initialExist[r-2] |= (1ull << c);
		}
	}
	vector<Move> optMoves;
	int optScore = 0;

	for (turn = 0; ; ++turn) {
		if (getTime() - startTime > TL) {
			debug("cleanup turn:%d\n", turn);
			break;
		}
		Field field = bestField;
		vector<ull> exist = initialExist;
		vector<Move> moves;
		int score = 0;
		while (true) {
			int bestSeqScore = 0;
			Move bestMove;
			for (int r = 2; r < N+2; ++r) {
				ull ex = exist[r-2];
				while (ex != 0) {
					int c = __builtin_ctzll(ex);
					for (int k = 0; k < 5; ++k) {
						vector<int> dir;
						int curScore = searchSingle(field, r, c, &dir);
						if (curScore > bestSeqScore) {
							bestSeqScore = curScore;
							bestMove.r = r;
							bestMove.c = c;
							bestMove.dir = dir;
						}
					}
					ex -= (1ull << c);
				}
			}
			if (bestSeqScore == 0) break;
			score += bestSeqScore;
			moves.push_back(bestMove);
			apply(field, bestMove);
			{
				int cr = bestMove.r;
				int cc = bestMove.c;
				exist[cr - 2] &= ~(1ull << cc);
				for (int i = 0; i < bestMove.dir.size(); ++i) {
					int d = bestMove.dir[i];
					exist[cr + DR[d] - 2] &= ~(1ull << (cc + DC[d]));
					cr += 2*DR[d];
					cc += 2*DC[d];
				}
				exist[cr - 2] |= (1ull << cc);
			}
		}
		if (score > optScore) {
			debug("score:%d at turn %d\n", score, turn);
			optMoves = moves;
			optScore = score;
		}
	}
	ans.insert(ans.end(), optMoves.begin(), optMoves.end());
	bestScore += optScore;
}

void revertUnrelatedMoves(vector<Move>& curAns) {
	if (curAns.empty()) return;
	Field field = initField;
	vector<u8> consumed;
	for (int i = 0; i < curAns.size() - 1; ++i) {
		const Move& move = curAns[i];
		assert(move.dir.size() == 1);
		int d = move.dir[0];
		u8 peg = field[move.r][move.c];
		assert(peg != EMPTY);
		assert(field[move.r + DR[d]][move.c + DC[d]] != EMPTY);
		consumed.push_back(field[move.r + DR[d]][move.c + DC[d]]);
		field[move.r][move.c] = EMPTY;
		field[move.r + DR[d]][move.c + DC[d]] = EMPTY;
		assert(field[move.r + 2 * DR[d]][move.c + 2 * DC[d]] == EMPTY);
		field[move.r + 2 * DR[d]][move.c + 2 * DC[d]] = peg;
	}
	++visitedIdx;
	{
		const Move& keyMove = curAns.back();
		int r = keyMove.r;
		int c = keyMove.c;
		for (int d : keyMove.dir) {
			visited[r][c] = visitedIdx;
			visited[r + DR[d]][c + DC[d]] = visitedIdx;
			r += 2 * DR[d];
			c += 2 * DC[d];
		}
		visited[r][c] = visitedIdx;
	}
	for (int i = curAns.size() - 2; i >= 0; --i) {
		const Move& move = curAns[i];
		int r = move.r;
		int c = move.c;
		int d = move.dir[0];
		if (visited[r][c] == visitedIdx || visited[r + DR[d]][c + DC[d]] == visitedIdx || visited[r + 2*DR[d]][c + 2*DC[d]] == visitedIdx) {
			visited[r][c] = visited[r + DR[d]][c + DC[d]] = visited[r + 2*DR[d]][c + 2*DC[d]] = visitedIdx;
		} else {
			int peg = bestField[r + 2 * DR[d]][c + 2 * DC[d]];
			assert(peg != EMPTY);
			assert(bestField[r + DR[d]][c + DC[d]] == EMPTY);
			assert(bestField[r][c] == EMPTY);
			bestField[r][c] = peg;
			bestField[r + DR[d]][c + DC[d]] = consumed[i];
			bestField[r + 2 * DR[d]][c + 2 * DC[d]] = EMPTY;
			curAns.erase(curAns.begin() + i);
		}
	}
}

int maxDeadendLen;
const Field* countOptimalLenField;

int countOptimalLenDfs(int r, int c, int d, int& count) {
	bool deadend = true;
	int deadendLen = 0;
	for (int i = 0; i < 4; ++i) {
		int nr = r + DR[i];
		int nc = c + DC[i];
		if ((*countOptimalLenField)[nr][nc] == EMPTY) continue;
		if ((*countOptimalLenField)[nr + DR[i]][nc + DC[i]] != EMPTY) continue;
		if (visited[nr][nc] == visitedIdx) {
			if ((i + 2) % 4 != d) deadend = false;
			continue;
		}
		visited[nr][nc] = visitedIdx;
		int childDeadendLen = countOptimalLenDfs(nr + DR[i], nc + DC[i], i, count);
		if (childDeadendLen == 0) {
			deadend = false;
			++count;
		} else {
			deadendLen = max(deadendLen, childDeadendLen + 1);
		}
	}
	if (deadend) {
		maxDeadendLen = max(maxDeadendLen, deadendLen + 1);
	} else {
		deadendLen = 0;
	}
	return deadendLen;
}

int countOptimalLen(int r, int c) {
	++visitedIdx;
	int count = 0;
	maxDeadendLen = 0;
	countOptimalLenDfs(r, c, 9, count);
	return count + maxDeadendLen;
}

vector<pair<vector<Move>, int>> findBigCombo(ll timelimit) {
	typedef tuple<const Board*, int, int, int> Cand;
	vector<Cand> candPos;
	vector<vector<Cand>> candPosParity(4);
	vector<int> candParity = {0, 1, 2, 3};
	bestKeyScore = 0;
	bestLen = 0;
	for (int i = 0; i < N; ++i) {
		fill_n(evalAmp[i+2] + 2, N, 1);
	}

	vector<vector<Board>> bestBoards(4);
	for (int parity : candParity) {  // parity: bit0-row, bit1-col
		enumerateCandidateBoards(parity, bestBoards[parity]);
		START_TIMER(5);
		debug("[%d]end setup:%lld with turn %d\n", parity, getTime() - startTime, turn);
		int maxVal = 0;
		int minVal = 1 << 30;
		for (const Board& board : bestBoards[parity]) {
			maxVal = max(maxVal, board.val);
			minVal = min(minVal, board.val);
			for (int r = 2 + (parity & 1); r < N + 2; r += 2) {
				for (int c = 2 + (parity >> 1); c < N + 2; c += 2) {
					if (board.f[r][c] == EMPTY) continue;
					countOptimalLenField = &(board.f);
					int len = countOptimalLen(r, c);
					if (len >= N / 2) {
						candPosParity[parity].emplace_back(&board, r, c, len);
					}
				}
			}
		}
		sort(candPosParity[parity].begin(), candPosParity[parity].end(), [](const Cand& cand1, const Cand& cand2){ return get<3>(cand1) > get<3>(cand2); });
		debug("maxVal:%d minVal:%d\n", maxVal, minVal);
		debug("[%d]end enum cands:%lld\n", parity, getTime() - startTime);
		STOP_TIMER(5);
		if (getTime() - startTime >= timelimit) break;
	}
	debugStr("\n");
	for (int i = 0; ; ++i) {
		bool add = false;
		for (int j = 0; j < 4; ++j) {
			if (candPosParity[j].size() <= i) continue;
			add = true;
			candPos.push_back(candPosParity[j][i]);
		}
		if (!add) break;
	}
	debug("candPos size:%d\n", (int)candPos.size());
	vector<pair<vector<Move>, int>> ret;
	if (candPos.empty()) {
		bestField = initField;
		return ret;
	}
	const int RETAIN_RESULT_COUNT = 6;
	for (int i = 0; !candPos.empty(); ++i) {
		auto cand = candPos[i % candPos.size()];
		int oldBestLen = bestLen;
		int score = searchKeySeq(*get<0>(cand), get<1>(cand), get<2>(cand));
		if (ret.size() < RETAIN_RESULT_COUNT) {
			ret.push_back({ans, score});
		} else {
			for (int j = 0; j < ret.size(); ++j) {
				if (ret[j].second == score) break;
				if (ret[j].second < score) {
					ret.pop_back();
					ret.insert(ret.begin() + j, {ans, score});
					break;
				}
			}
		}
		START_TIMER(8);
		if (bestLen > oldBestLen) {
			debug("bestLen:%d optimalLen:%d\n", bestLen, get<3>(cand));
			candPos.erase(remove_if(candPos.begin() + i, candPos.end(), [](const Cand& cand) {return get<3>(cand) < bestLen;}), candPos.end());
			debug("candPos size:%d\n", (int)candPos.size());
		}
		STOP_TIMER(8);
		if (getTime() - startTime >= timelimit) {
			debug("search count:%d\n", i);
			break;
		}
	}
	// bestScore += bestKeyScore;
	debugStr("result scores:");
	for (auto& result : ret) {
		debug("%d ", result.second);
	}
	debugln();
	if (!ret.empty()) {
		while (ret[0].second < ret.back().second / 2) {
			ret.erase(ret.begin());
		}
	}
	return ret;
}

vector<Move> extendBigCombo(const vector<Move>& curAns, ll timelimit, int& keyScore) {
	if (curAns.empty()) return curAns;
	vector<vector<bool>> protect(N + 4, vector<bool>(N + 4));
	const Move& combo = curAns.back();
	{
		int r = combo.r;
		int c = combo.c;
		for (int i = 0; i < combo.dir.size(); ++i) {
			int d = combo.dir[i];
			protect[r][c] = true;
			protect[r + DR[d]][c + DC[d]] = true;
			assert(bestField[r + DR[d]][c + DC[d]] == EMPTY);
			r += 2 * DR[d];
			c += 2 * DC[d];
		}
		assert(bestField[r][c] != EMPTY);
		protect[r][c] = true;
	}
	Field f = initField;
	for (int i = 0; i < curAns.size() - 1; ++i) {
		apply(f, curAns[i]);
	}
	Field origField = f;
	apply(f, combo);

	const int WIDTH = 5;
	vector<pair<int, int>> ampPos;
	for (int i = 2 + (combo.r & 1); i < N + 2; i += 2) {
		fill_n(evalAmp[i+2] + 2, N, 1);
		for (int j = 2 + (combo.c & 1); j < N + 2; j += 2) {
			if (protect[i][j]) {
				evalAmp[i][j] = min(WIDTH, 3);
				ampPos.push_back({i, j});
			}
		}
	}
	for (int i = WIDTH; i > 1; --i) {
		vector<pair<int, int>> nextPos;
		for (auto& pos : ampPos) {
			int r = pos.first;
			int c = pos.second;
			for (int d = 0; d < 4; ++d) {
				int nr = r + 2*DR[d];
				int nc = c + 2*DC[d];
				if (2 <= nr && nr < N + 2 && 2 <= nc && nc < N + 2 && evalAmp[nr][nc] == 0) {
					evalAmp[nr][nc] = min(i - 1, 3);
					nextPos.push_back({nr, nc});
				}
			}
		}
		swap(ampPos, nextPos);
	}

	vector<Move> ret = curAns;
	vector<int> cPos;
	for (int i = 2; i < N + 2; ++i) {
		for (int j = 2; j < N + 2; ++j) {
			cPos.push_back((i << 8) + j);
		}
	}
	while (getTime() - startTime < timelimit) {
		Field curField = f;
		Field curOrigField = origField;
		vector<Move> additionalMoves;
		for (int j = 0; j < 3; ++j) {
			random_shuffle(cPos.begin(), cPos.end());
			for (int k = 0; k < N*N; ++k) {
				int r = cPos[k] >> 8;
				int c  =cPos[k] & 0xFF;
				if (protect[r][c]) continue;
				if (curField[r][c] == EMPTY) continue;
				for (int d = 0; d < 4; ++d) {
					int nr = r + DR[d];
					int nc = c + DC[d];
					if (protect[nr][nc] || curField[nr][nc] == EMPTY) continue;
					int nr2 = r + 2*DR[d];
					int nc2 = c + 2*DC[d];
					if (protect[nr2][nc2] || curField[nr2][nc2] != EMPTY) continue;
					bool parityR = (combo.r & 1) == (nr & 1);
					bool parityC = (combo.c & 1) == (nc & 1);
					int diff;
					if (parityR && parityC) {
						diff = eval1(curField, nr, nc, d);
					} else if (!parityR && !parityC) {
						diff = eval2(curField, nr, nc, d);
					} else if ((combo.r & 1) == (r & 1) && (combo.c & 1) == (c & 1)) {
						diff = eval3(curField, nr, nc, d);
					} else {
						diff = 0;
					}
					if (rnd.nextDouble() < exp((diff - 10) / (6-j))) {
						u8 peg = curField[r][c];
						curField[r][c] = EMPTY;
						curField[nr][nc] = EMPTY;
						curField[nr2][nc2] = peg;
						additionalMoves.push_back({r, c, vector<int>(1, d)});
						ADD_COUNTER(j + 3);
						break;
					}
				}
			}
		}
		for (const Move& move : additionalMoves) {
			apply(curOrigField, move);
		}
		vector<int> dir = combo.dir;
		int score = improve(curOrigField, curField, combo.r, combo.c, dir, keyScore);
		if (score > keyScore) {
			debug("extended bestKeyScore:%d keyLen:%d time:%lld\n", score, (int)dir.size(), getTime() - startTime);
			keyScore = score;
			bestKeyScore = score;
			bestLen = dir.size();
			const Move bestMove = {combo.r, combo.c, dir};
			bestField = curOrigField;
			apply(bestField, bestMove);
			ret.clear();
			ret.insert(ret.end(), curAns.begin(), curAns.end() - 1);
			ret.insert(ret.end(), additionalMoves.begin(), additionalMoves.end());
			ret.push_back(bestMove);
			revertUnrelatedMoves(ret);
			break;
		}
	}
	return ret;
}

bool shouldFindMoreCombo() {
	if (getTime() - startTime >= TL * 13 / 14) return false;
	int cnt[4] = {};
	for (int i = 2; i < N + 2; ++i) {
		for (int j = 2; j < N + 2; ++j) {
			if (bestField[i][j] != EMPTY) cnt[(i & 1) + ((j & 1) << 1)]++;
		}
	}
	// debug("count:[%d %d %d %d]\n", cnt[0], cnt[1], cnt[2], cnt[3]);
	double minRestRatio = min({1. * cnt[0] / parityCnt[0], 1. * cnt[1] / parityCnt[1], 1. * cnt[2] / parityCnt[2], 1. * cnt[3] / parityCnt[3]});
	debug("minRestRatio:%.3f\n", minRestRatio);
	return minRestRatio > 0.7;
}

void solve() {
	BEAM_WIDTH = max((int)(10000000.0 / pow(N, 1.8) / pegs), 10);
	BEST_WIDTH = (int)(2000.0 / N);
	RETURN_SIZE = (int)(BEST_WIDTH * 500.0 / N);
	debug("BEAM_WIDTH:%d BEST_WIDTH:%d RETURN_SIZE:%d\n", BEAM_WIDTH, BEST_WIDTH, RETURN_SIZE);
	ll timelimit = TL * 5 / 10;
	auto answers = findBigCombo(timelimit);
	timelimit += (TL - timelimit) / 2;
	const ll extendStartTime = getTime() - startTime;
	int bestScore = 0;
	for (int i = 0; i < answers.size(); ++i) {
		vector<Move> curAns = answers[i].first;
		int curScore = answers[i].second;
		bestField = initField;
		for (const Move& move : curAns) {
			apply(bestField, move);
		}
		revertUnrelatedMoves(curAns);
		{
			START_TIMER(10);
			bool findMoreCombo = shouldFindMoreCombo();
			ll extendLimit = findMoreCombo ? min(getTime() - startTime + 300, TL) : extendStartTime + (TL - 400 - extendStartTime) * (i + 1) / answers.size();
			debug("start extend:%lld %lld\n", getTime() - startTime, extendLimit);
			while (getTime() - startTime < extendLimit) {
				curAns = extendBigCombo(curAns, extendLimit, curScore);
				if (findMoreCombo && !shouldFindMoreCombo()) {
					findMoreCombo = false;
					extendLimit = extendStartTime + (TL - 400 - extendStartTime) * (i + 1) / answers.size();
					debugStr("switch to find no more combo\n");
				}
			}
			STOP_TIMER(10);
		}
		if (curScore > bestScore) {
			ans = curAns;
			bestScore = curScore;
		}
	}
	vector<Move> totalAns = ans;
	bestField = initField;
	for (const Move& move : ans) {
		apply(bestField, move);
	}
	ans.clear();
	while (!totalAns.empty() && shouldFindMoreCombo()) {
		initField = bestField;
		BEAM_WIDTH = (int)(20000.0 / pow(N, 2.0));
		BEST_WIDTH = (int)(2000.0 / N);
		RETURN_SIZE = (int)(BEST_WIDTH * 500.0 / N);
		debugStr("\n");
		debug("BEAM_WIDTH:%d BEST_WIDTH:%d RETURN_SIZE:%d\n", BEAM_WIDTH, BEST_WIDTH, RETURN_SIZE);
		findBigCombo(timelimit);
		timelimit += (TL - timelimit) / 2;
		if (ans.empty()) break;
		revertUnrelatedMoves(ans);
		totalAns.insert(totalAns.end(), ans.begin(), ans.end());
		ans.clear();
	}
	debugStr("\n");
	ans = mergeMoves(totalAns);
	START_TIMER(9);
	cleanup();
	STOP_TIMER(9);
}

class PegJumping {
public:
	PegJumping();
	vector<string> getMoves(const vector<int>& pegValue, const vector<string>& boardInput);
};

PegJumping::PegJumping() {
	startTime = getTime();
}

vector<string> PegJumping::getMoves(const vector<int>& pegValue, const vector<string>& boardInput) {
	N = boardInput.size();
	for (int v = 1; v <= 10; ++v) {
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				hv[v][i+2][j+2] = ((u64)rnd.nextUInt() << 32) | rnd.nextUInt();
			}
		}
	}
	bestScore = 0;
	pegs = 0;
	fill_n(parityCnt, 4, 0);
	memset(visited, 0, sizeof(visited));
	visitedIdx = 0;
	initField.resize(N+4);
	initField[0].assign(N+4, SENTINEL);
	initField[1].assign(N+4, SENTINEL);
	for (int i = 0; i < N; ++i) {
		initField[i+2].assign(N+4, EMPTY);
		initField[i+2][0] = initField[i+2][1] = SENTINEL;
		for (int j = 0; j < N; ++j) {
			if (boardInput[i][j] != '.') {
				initField[i+2][j+2] = pegValue[boardInput[i][j] - '0'];
				++pegs;
				parityCnt[(i & 1) + ((j & 1) << 1)]++;
			}
			edgePena[i+2][j+2] = min({i, N - 1 - i, j, N - 1 - j});
		}
		initField[i+2][N+2] = initField[i+2][N+3] = SENTINEL;
	}
	initField[N+2].assign(N+4, SENTINEL);
	initField[N+3].assign(N+4, SENTINEL);
	debug("N:%d pegs:%d P:%.3f\n", N, pegs, 1 - 1.0 * pegs / (N*N));
	debug("parity count:[%d %d %d %d] [%d %d] \n", parityCnt[0], parityCnt[1], parityCnt[2], parityCnt[3], parityCnt[0] + parityCnt[3], parityCnt[1] + parityCnt[2]);
	solve();

	vector<string> ret;
	for (const Move& move : ans) {
		stringstream stm;
		stm << move;
		ret.push_back(stm.str());
	}
	debugStr("\n");
	PRINT_TIMER();
	PRINT_COUNTER();
	return ret;
}

#if defined(LOCAL) || defined(TEST)
int main() {
	int n, m;
	cin >> m;
	vector<int> pegValue(m);
	for (int i = 0; i < m; ++i) {
		cin >> pegValue[i];
	}
	cin >> n;
	vector<string> board(n);
	for (int i = 0; i < n; ++i) {
		cin >> board[i];
	}
	PegJumping obj;
	vector<string> ret = obj.getMoves(pegValue, board);
	cout << ret.size() << endl;
	for (int i = 0; i < ret.size(); ++i) {
		cout << ret[i] << endl;
	}
	cout.flush();
}
#endif