import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class PegJumping {

	private static final boolean DEBUG = 1 == 1;
	private static final long TL = 1000;
	private static final int[] DR = { -1, 1, 0, 0 };
	private static final int[] DC = { 0, 0, -1, 1 };
	private static final int EMPTY = -1;
	private final long startTime = System.currentTimeMillis();
	ArrayList<Move> ans = new ArrayList<>();
	Random rnd = new Random(42);
	int N;
	int[][] board, initBoard;
	int bestScore;
	int turn;
	int pegs;

	String[] getMoves(int[] pegValue, String[] boardInput) {
		N = boardInput.length;
		initBoard = new int[N][N];
		board = new int[N][N];
		for (int i = 0; i < N; ++i) {
			Arrays.fill(initBoard[i], EMPTY);
			for (int j = 0; j < N; ++j) {
				if (boardInput[i].charAt(j) != '.') {
					initBoard[i][j] = pegValue[boardInput[i].charAt(j) - '0'];
					++pegs;
				}
			}
		}
		for (turn = 1;; ++turn) {
			if (System.currentTimeMillis() - startTime > TL) {
				debug("turn:" + turn);
				break;
			}
			solve();
		}
		String[] ret = new String[ans.size()];
		for (int i = 0; i < ans.size(); ++i) {
			ret[i] = ans.get(i).toString();
		}
		debug("bestScore:" + bestScore);
		return ret;
	}

	void solve() {
		for (int i = 0; i < N; ++i) {
			System.arraycopy(initBoard[i], 0, board[i], 0, N);
		}
		ArrayList<Move> moves = new ArrayList<>();
		int score = 0;
		int parity = turn & 1;
		double force = 0.999;
		double forceMul = 0.99 + (rnd.nextDouble() / 100);
		int rest = pegs;

		while (true) {
			Move best = null;
			int bestSc = 0;
			for (int i = 0; i < N * N * 3; ++i) {
				int r = rnd.nextInt(N);
				int c = rnd.nextInt(N);
				if (board[r][c] == EMPTY) continue;
				if (rnd.nextDouble() < force && ((r + c) & 1) != parity) continue;
				int peg = board[r][c];
				board[r][c] = EMPTY;
				Move cur = new Move(r, c);
				int curSc = 0;
				ArrayList<Integer> removed = new ArrayList<>();
				while (true) {
					int dir = rnd.nextInt(4);
					boolean found = false;
					for (int j = 0; j < 4; ++j) {
						dir = (dir + 1) % 4;
						int nr = r + DR[dir] * 2;
						int nc = c + DC[dir] * 2;
						if (nr < 0 || N <= nr || nc < 0 || N <= nc) continue;
						if (board[nr][nc] != EMPTY) continue;
						if (board[r + DR[dir]][c + DC[dir]] == EMPTY) continue;
						cur.dir.add(dir);
						curSc += board[r + DR[dir]][c + DC[dir]];
						removed.add(board[r + DR[dir]][c + DC[dir]]);
						board[r + DR[dir]][c + DC[dir]] = EMPTY;
						r = nr;
						c = nc;
						found = true;
						break;
					}
					if (!found) break;
				}
				board[r][c] = peg;
				revert(cur, removed);
				curSc *= cur.dir.size();
				if (curSc > bestSc) {
					bestSc = curSc;
					best = cur;
					if (rest > pegs / 2) break;
				}
			}
			if (bestSc == 0) break;
			moves.add(best);
			apply(best);
			rest -= best.dir.size();
			score += bestSc;
			force *= forceMul;
		}
		if (score > bestScore) {
			bestScore = score;
			ans = moves;
			debug("score:" + score + " at turn:" + turn);
		}
	}

	void apply(Move move) {
		int r = move.r;
		int c = move.c;
		int peg = board[r][c];
		board[r][c] = EMPTY;
		for (int d : move.dir) {
			board[r + DR[d]][c + DC[d]] = EMPTY;
			r += DR[d] * 2;
			c += DC[d] * 2;
		}
		board[r][c] = peg;
	}

	void revert(Move move, ArrayList<Integer> removed) {
		int r = move.r;
		int c = move.c;
		for (int i = move.dir.size() - 1; i >= 0; --i) {
			int d = move.dir.get(i);
			r += DR[d] * 2;
			c += DC[d] * 2;
		}
		int peg = board[r][c];
		board[r][c] = EMPTY;
		for (int i = move.dir.size() - 1; i >= 0; --i) {
			int d = move.dir.get(i);
			board[r - DR[d]][c - DC[d]] = removed.get(i);
			r -= DR[d] * 2;
			c -= DC[d] * 2;
		}
		board[r][c] = peg;
	}

	static class Move {
		int r, c;
		ArrayList<Integer> dir = new ArrayList<>();

		public Move(int r, int c) {
			this.r = r;
			this.c = c;
		}

		public String toString() {
			String ret = r + " " + c + " ";
			for (int i = 0; i < dir.size(); ++i) {
				ret += "UDLR".charAt(dir.get(i));
			}
			return ret;
		}
	}

	static void printBoard(int[][] b) {
		if (DEBUG) {
			for (int i = 0; i < b.length; ++i) {
				for (int j = 0; j < b[0].length; ++j) {
					System.err.print((b[i][j] == EMPTY ? '-' : (char) (b[i][j] + '0')) + "");
				}
				System.err.println();
			}
			System.err.println();
		}
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int M = sc.nextInt();
			int[] pegValue = new int[M];
			for (int i = 0; i < M; ++i) {
				pegValue[i] = sc.nextInt();
			}
			int N = sc.nextInt();
			String[] board = new String[N];
			for (int i = 0; i < N; ++i) {
				board[i] = sc.next();
			}
			String[] ret = new PegJumping().getMoves(pegValue, board);
			System.out.println(ret.length);
			for (String jump : ret) {
				System.out.println(jump);
			}
			System.out.flush();
		}
	}
}
