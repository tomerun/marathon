/*
Change log
----------
2015-06-22 :
Initial release
*/

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.ArrayList;

class TestCase {
	private static final int[] DR = { -1, 1, 0, 0 };
	private static final int[] DC = { 0, 0, -1, 1 };
	public static final int MIN_BOARD_SIZE = 20;
	public static final int MAX_BOARD_SIZE = 60;
	public static final int MIN_PEG_TYPES = 1;
	public static final int MAX_PEG_TYPES = 10;
	public static final int MIN_PEG_VALUE = 1;
	public static final int MAX_PEG_VALUE = 10;
	public static final int MIN_PEG_BLOBS = 0;
	public static final int MAX_PEG_BLOBS = 10;
	public static final double MIN_FILL_RATIO = 0.1;
	public static final double MAX_FILL_RATIO = 0.9;

	public int N;
	public int pegStartCnt;
	public int pegNowCnt;
	public int numJumps;
	public int[] pegValue;
	public int score;
	public double fillRatio;
	public int[] lastX;
	public int[] lastY;
	public int[] lastPeg;
	public char[][] board;
	Move[] moves;

	public TestCase(long seed) {
		SecureRandom rnd = null;
		try {
			rnd = SecureRandom.getInstance("SHA1PRNG");
		} catch (Exception e) {
			System.err.println("ERROR: unable to generate test case.");
			System.exit(1);
		}
		rnd.setSeed(seed);

		N = rnd.nextInt(MAX_BOARD_SIZE - MIN_BOARD_SIZE + 1) + MIN_BOARD_SIZE;
		if (seed == 1) N = 20;
		if (1000 <= seed && seed < 1900) N = 20 + ((int) seed - 1000) / 100 * 5;
		if (Tester.specifiedN > 0) N = Tester.specifiedN;
		board = new char[N][N];
		int pegTypeCnt = rnd.nextInt(MAX_PEG_TYPES - MIN_PEG_TYPES + 1) + MIN_PEG_TYPES;
		pegValue = new int[pegTypeCnt];
		for (int i = 0; i < pegTypeCnt; i++) {
			pegValue[i] = rnd.nextInt(MAX_PEG_VALUE - MIN_PEG_VALUE + 1) + MIN_PEG_VALUE;
		}
		fillRatio = rnd.nextDouble() * (MAX_FILL_RATIO - MIN_FILL_RATIO) + MIN_FILL_RATIO;
		if (1000 <= seed && seed < 1900) fillRatio = 0.1 + (seed % 100) * 0.8 / 99;
		if (Tester.specifiedP >= 0) fillRatio = Tester.specifiedP;
		// fill the board
		for (int y = 0; y < N; y++) {
			for (int x = 0; x < N; x++) {
				if (rnd.nextDouble() < fillRatio) {
					board[y][x] = '.';
				} else {
					board[y][x] = (char) ('0' + rnd.nextInt(pegTypeCnt));
				}
			}
		}
		// add the blobs
		int blobCnt = rnd.nextInt(MAX_PEG_BLOBS - MIN_PEG_BLOBS + 1) + MIN_PEG_BLOBS;
		if (Tester.specifiedB >= 0) blobCnt = Tester.specifiedB;
		for (int b = 0; b < blobCnt; b++) {
			int pegType = rnd.nextInt(pegTypeCnt);
			int radi = rnd.nextInt(N / 4) + 1;
			int centerX = rnd.nextInt(N);
			int centerY = rnd.nextInt(N);
			for (int x = centerX - radi; x <= centerX + radi; x++)
				if (x >= 0 && x < N) {
					for (int y = centerY - radi; y <= centerY + radi; y++)
						if (y >= 0 && y < N) {
							int r = (x - centerX) * (x - centerX) + (y - centerY) * (y - centerY);
							if (r <= radi * radi) board[y][x] = (char) ('0' + pegType);
						}
				}
		}
		pegStartCnt = 0;
		for (int y = 0; y < N; y++) {
			for (int x = 0; x < N; x++) {
				if (board[y][x] != '.') pegStartCnt++;
			}
		}
		pegNowCnt = pegStartCnt;
		numJumps = 0;
		score = 0;
	}

	public void doMove() {
		Move move = this.moves[this.numJumps];
		int x = move.x;
		int y = move.y;
		if (x < 0 || x >= this.N || y < 0 || y >= this.N) {
			throw new RuntimeException("ERROR: (" + y + "," + x + ") outside of bounds.");
		}
		if (board[y][x] == '.') {
			throw new RuntimeException("ERROR: (" + y + "," + x + ") does not contain a peg.");
		}
		char peg = board[y][x];
		if (peg == '.') {
			throw new RuntimeException("ERROR: Trying to move an empty cell.");
		}
		int sum = 0;
		for (int i = 0; i < move.dir.length(); i++) {
			int dir = "UDLR".indexOf(move.dir.charAt(i));
			if (dir == -1) {
				throw new RuntimeException("ERROR: Invalid move character [" + move.dir + "].");
			}
			int nx = x + DC[dir];
			int ny = y + DR[dir];
			if (nx >= 0 && nx < N && ny >= 0 && ny < N) {
				if (board[ny][nx] == '.') {
					throw new RuntimeException("ERROR: Can not jump over empty space.");
				} else {
					int nx2 = nx + DC[dir];
					int ny2 = ny + DR[dir];
					if (nx2 >= 0 && nx2 < N && ny2 >= 0 && ny2 < N) {
						if (board[ny2][nx2] != '.') {
							throw new RuntimeException("ERROR: Trying to jump onto another peg.");
						}
						board[y][x] = '.';
						sum += pegValue[(board[ny][nx] - '0')];
						move.jumpPegType[i] = board[ny][nx] - '0';
						board[ny][nx] = '.';
						board[ny2][nx2] = peg;
						x = nx2;
						y = ny2;
					} else {
						throw new RuntimeException("ERROR: Trying to jump outside the board.");
					}
				}
			} else {
				throw new RuntimeException("ERROR: Trying to move outside the board.");
			}
		}
		move.score = move.dir.length() * sum;
		pegNowCnt -= move.dir.length();
		score += move.score;
		numJumps++;
	}

	public void forward() {
		++numJumps;
		setLastMoveInfo();
		Move move = this.moves[numJumps - 1];
		int x = move.x;
		int y = move.y;
		char peg = board[y][x];
		board[y][x] = '.';
		for (int i = 0; i < move.dir.length(); ++i) {
			int d = "UDLR".indexOf(move.dir.charAt(i));
			board[y + DR[d]][x + DC[d]] = '.';
			x += DC[d] * 2;
			y += DR[d] * 2;
		}
		board[y][x] = peg;
		score += move.score;
		pegNowCnt -= move.dir.length();
	}

	public void backward() {
		--numJumps;
		Move move = this.moves[numJumps];
		int x = move.x;
		int y = move.y;
		for (int i = 0; i < move.dir.length(); ++i) {
			int d = "UDLR".indexOf(move.dir.charAt(i));
			x += DC[d] * 2;
			y += DR[d] * 2;
		}
		char peg = board[y][x];
		board[y][x] = '.';
		for (int i = move.dir.length() - 1; i >= 0; --i) {
			int d = "UDLR".indexOf(move.dir.charAt(i));
			board[y - DR[d]][x - DC[d]] = (char) (move.jumpPegType[i] + '0');
			x -= DC[d] * 2;
			y -= DR[d] * 2;
		}
		board[y][x] = peg;
		pegNowCnt += move.dir.length();
		score -= move.score;
		setLastMoveInfo();
	}

	private void setLastMoveInfo() {
		if (numJumps == 0) {
			lastX = new int[0];
			lastY = new int[0];
			lastPeg = new int[0];
			return;
		}
		Move move = this.moves[numJumps - 1];
		lastX = new int[move.dir.length() + 1];
		lastY = new int[move.dir.length() + 1];
		lastPeg = new int[move.dir.length() + 1];
		int x = move.x;
		int y = move.y;
		lastX[0] = x;
		lastY[0] = y;
		for (int i = 0; i < move.dir.length(); ++i) {
			int d = "UDLR".indexOf(move.dir.charAt(i));
			x += DC[d] * 2;
			y += DR[d] * 2;
			lastX[i + 1] = x;
			lastY[i + 1] = y;
			lastPeg[i + 1] = move.jumpPegType[i];
		}
	}

	public String lastScoreStr() {
		if (numJumps == 0) return "";
		Move move = this.moves[numJumps - 1];
		if (move.score == 0) return "0";
		return String.format("%d*%d=%d", move.score / move.dir.length(), move.dir.length(), move.score);
	}
}

class Move {
	int x, y, score;
	String dir;
	int[] jumpPegType;

	Move(String data) {
		String[] s = data.split(" ");
		if (s.length != 3) {
			throw new RuntimeException("ERROR: The move command does not contain 3 values : [" + data + "]");
		}
		this.y = Integer.parseInt(s[0]);
		this.x = Integer.parseInt(s[1]);
		this.dir = s[2];
		this.jumpPegType = new int[this.dir.length()];
	}
}

public class Tester {
	public static String execCommand = "./tester";
	public static int specifiedN = -1;
	public static int specifiedB = -1;
	public static double specifiedP = -1;
	long seed;
	TestCase tc;

	Tester(long seed) {
		this.seed = seed;
		tc = new TestCase(seed);
	}

	public Result runTest() throws Exception {
		Result res = new Result();
		res.seed = seed;
		res.N = tc.N;
		res.P = tc.fillRatio;

		Process solution = Runtime.getRuntime().exec(execCommand);
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(solution.getInputStream()));
			PrintWriter writer = new PrintWriter(solution.getOutputStream());
			new ErrorStreamRedirector(solution.getErrorStream()).start();

			writer.println(tc.pegValue.length);
			for (int v : tc.pegValue)
				writer.println(v);
			writer.println(tc.N);
			for (int y = 0; y < tc.N; y++) {
				writer.println(String.valueOf(tc.board[y]));
			}
			writer.flush();

			long startTime = System.currentTimeMillis();
			int numMoves = Integer.parseInt(reader.readLine());
			res.elapsed = System.currentTimeMillis() - startTime;
			tc.moves = new Move[numMoves];
			for (int i = 0; i < numMoves; i++) {
				String move = reader.readLine();
				tc.moves[i] = new Move(move);
			}
			res.maxScore = 0;
			for (int i = 0; i < numMoves; i++) {
				tc.doMove();
				res.maxScore = Math.max(res.maxScore, tc.moves[i].score);
			}
		} finally {
			solution.destroy();
		}

		res.score = tc.score;
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = -1, begin = -1, end = -1;
		boolean vis = false;
		int cellSize = 17;
		for (int i = 0; i < args.length; i++)
			if (args[i].equals("-exec")) {
				execCommand = args[++i];
			} else if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-b")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-e")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-N")) {
				specifiedN = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-B")) {
				specifiedB = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-P")) {
				specifiedP = Double.parseDouble(args[++i]);
			} else if (args[i].equals("-vis")) {
				vis = true;
			} else if (args[i].equals("-sz")) {
				cellSize = Integer.parseInt(args[++i]);
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}

		if (execCommand == null) {
			System.err.println("ERROR: You did not provide the command to execute your solution."
					+ " Please use -exec <command> for this.");
			System.exit(1);
		}

		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester(seed);
			Result res = tester.runTest();
			System.out.println(res);
			if (vis) {
				for (int i = 0; i < tester.tc.moves.length; ++i) {
					tester.tc.backward();
				}
				Visualizer drawer = new Visualizer(tester.tc, cellSize);
			}
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester(seeds.get(i));
				try {
					Result res = f.runTest();
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int N;
		double P;
		int score, maxScore;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("N:%2d P:%.3f\n", N, P);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("maxScore:%d\n", maxScore);
			ret += String.format("score:%d", score);
			return ret;
		}
	}
}

class ErrorStreamRedirector extends Thread {
	public BufferedReader reader;

	public ErrorStreamRedirector(InputStream is) {
		reader = new BufferedReader(new InputStreamReader(is));
	}

	public void run() {
		while (true) {
			String s;
			try {
				s = reader.readLine();
			} catch (Exception e) {
				//e.printStackTrace();
				return;
			}
			if (s == null) {
				break;
			}
			System.err.println(s);
		}
	}
}
