import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Visualizer extends JFrame {
	public static final int EXTRA_WIDTH = 200;
	public static final int EXTRA_HEIGHT = 50;
	Color[][] colors = new Color[4][11];
	public TestCase tc;
	public int cellSize, N, maxVal;
	Font displayFont = new Font("Ricty", Font.BOLD, 13);
	Font indexFont = new Font("Arial", Font.PLAIN, 10);
	int displayType = -1;

	class DrawerKeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				int rep = e.isShiftDown() ? 10 : 1;
				if (e.isMetaDown()) rep = tc.moves.length - tc.numJumps;
				if (e.isAltDown()) {
					rep = 0;
					int maxScore = 0;
					for (int i = tc.numJumps; i < tc.moves.length; ++i) {
						if (tc.moves[i].score > maxScore) {
							maxScore = tc.moves[i].score;
							rep = i - tc.numJumps + 1;
						}
					}
				}
				for (int i = 0; i < rep; ++i) {
					if (tc.numJumps < tc.moves.length) {
						tc.forward();
					}
				}
				repaint();
			} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				int rep = e.isShiftDown() ? 10 : 1;
				if (e.isMetaDown()) rep = tc.numJumps;
				if (e.isAltDown()) {
					rep = 0;
					int maxScore = 0;
					for (int i = tc.numJumps - 2; i >= 0; --i) {
						if (tc.moves[i].score > maxScore) {
							maxScore = tc.moves[i].score;
							rep = tc.numJumps - i - 1;
						}
					}
				}
				for (int i = 0; i < rep; ++i) {
					if (tc.numJumps > 0) {
						tc.backward();
					}
				}
				repaint();
			} else if (e.getKeyCode() == KeyEvent.VK_1) {
				setDisplayType(1);
			} else if (e.getKeyCode() == KeyEvent.VK_2) {
				setDisplayType(2);
			} else if (e.getKeyCode() == KeyEvent.VK_3) {
				setDisplayType(3);
			} else if (e.getKeyCode() == KeyEvent.VK_4) {
				setDisplayType(0);
			}
		}

		void setDisplayType(int type) {
			displayType = displayType == type ? -1 : type;
			repaint();
		}
	}

	class DrawerPanel extends JPanel {
		public void paint(Graphics gr) {
			Graphics2D g = (Graphics2D) gr;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(new Color(32, 32, 32));
			g.translate(15, 15);
			g.fillRect(0, 0, cellSize * N + 1, cellSize * N + 1);
			g.setColor(Color.BLACK);
			for (int i = 0; i <= N; i++) {
				g.drawLine(i * cellSize, 0, i * cellSize, cellSize * N);
				g.drawLine(0, i * cellSize, cellSize * N, i * cellSize);
			}

			// diaplay pegs
			int[] parityPegCnt = new int[4];
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (tc.board[i][j] >= '0' && tc.board[i][j] <= '9') {
						g.setColor(pegColor(i, j, tc.board[i][j] - '0'));
						g.fillOval(j * cellSize + 1, i * cellSize + 1, cellSize - 1, cellSize - 1);
						parityPegCnt[(i & 1) + ((j & 1) << 1)]++;
					}
				}
			}

			if (displayType == -1) {
				// display move
				if (tc.lastX != null && tc.lastX.length > 0) {
					Stroke origStroke = g.getStroke();
					g.setStroke(new BasicStroke(0.5f));
					g.setFont(indexFont);
					int markMargin = cellSize / 4;
					for (int i = 0; i < tc.lastX.length - 1; i++) {
						g.setColor(pegColor((tc.lastX[i + 1] + tc.lastX[i]) / 2, (tc.lastY[i + 1] + tc.lastY[i]) / 2,
								tc.lastPeg[i + 1]));
						g.drawOval(((tc.lastX[i + 1] + tc.lastX[i]) / 2) * cellSize + 1, ((tc.lastY[i + 1] + tc.lastY[i]) / 2)
								* cellSize + 1, cellSize - 1, cellSize - 1);

						float f = 0.5f + 0.5f * (i + 1) / (tc.lastX.length - 1);
						g.setColor(new Color(f, f, f));
						g.drawLine(tc.lastX[i] * cellSize + cellSize / 2, tc.lastY[i] * cellSize + cellSize / 2, tc.lastX[i + 1]
								* cellSize + cellSize / 2, tc.lastY[i + 1] * cellSize + cellSize / 2);
						g.fillOval(tc.lastX[i] * cellSize + markMargin, tc.lastY[i] * cellSize + markMargin, cellSize - markMargin
								* 2, cellSize - markMargin * 2);
						g.setColor(Color.WHITE);
						drawStringCenter(g, Integer.toString(i), (tc.lastX[i] + tc.lastX[i + 1]) / 2 * cellSize,
								(tc.lastY[i] + tc.lastY[i + 1]) / 2 * cellSize, cellSize, cellSize);
					}
					g.setColor(Color.WHITE);
					g.fillOval(tc.lastX[tc.lastX.length - 1] * cellSize + markMargin, tc.lastY[tc.lastX.length - 1] * cellSize
							+ markMargin, cellSize - markMargin * 2, cellSize - markMargin * 2);
					g.fillOval(tc.lastX[0] * cellSize + markMargin, tc.lastY[0] * cellSize + markMargin, cellSize - markMargin
							* 2, cellSize - markMargin * 2);
					g.setColor(Color.BLACK);
					g.drawOval(tc.lastX[tc.lastX.length - 1] * cellSize + markMargin * 3 / 2, tc.lastY[tc.lastX.length - 1]
							* cellSize + markMargin * 3 / 2, cellSize - markMargin * 3, cellSize - markMargin * 3);
					g.drawOval(tc.lastX[0] * cellSize + markMargin * 3 / 2, tc.lastY[0] * cellSize + markMargin * 3 / 2, cellSize
							- markMargin * 3, cellSize - markMargin * 3);
					g.setStroke(origStroke);
				}
			} else {
				// display graph
				g.setColor(Color.WHITE);
				for (int r = displayType & 1; r < N; r += 2) {
					int y = r * cellSize + cellSize / 2;
					for (int c = displayType >> 1; c < N; c += 2) {
						int x = c * cellSize + cellSize / 2;
						if (r + 2 < N && tc.board[r + 1][c] != '.' && (tc.board[r][c] == '.' || tc.board[r + 2][c] == '.')) {
							g.drawLine(x, y, x, y + 2 * cellSize);
						}
						if (c + 2 < N && tc.board[r][c + 1] != '.' && (tc.board[r][c] == '.' || tc.board[r][c + 2] == '.')) {
							g.drawLine(x, y, x + 2 * cellSize, y);
						}
					}
				}
			}

			// display info area
			g.setFont(displayFont);
			g.translate(-15, -15);
			int horPos = 20 + N * cellSize;
			g.setColor(Color.BLACK);
			g.drawString("N: " + N + "  N^2: " + (N * N), horPos, 30);
			g.drawString("initPegs: " + tc.pegStartCnt, horPos, 50);
			g.drawString("curPegs: " + tc.pegNowCnt, horPos, 70);
			g.drawString("Jumps: " + tc.numJumps, horPos, 90);
			g.drawString("Seq: " + tc.lastScoreStr(), horPos, 110);
			g.drawString("Score: " + tc.score, horPos, 130);
			int legendY = 0;
			int baseY = 140;
			for (int j : new int[] { 0, 3, 2, 1 }) {
				g.setColor(colors[j][maxVal]);
				g.fillOval(horPos, baseY + legendY * cellSize + 1, cellSize - 1, cellSize - 1);
				g.setColor(Color.BLACK);
				g.drawString(Integer.toString(parityPegCnt[j]), horPos + cellSize + 10, baseY + 10 + legendY * cellSize
						+ (cellSize - displayFont.getSize()) / 2);
				++legendY;
			}
			++legendY;
			for (int i = 1; i <= 10; ++i) {
				if (colors[0][i] == null) continue;
				g.setColor(colors[0][i]);
				g.fillOval(horPos + cellSize * 0, baseY + legendY * cellSize + 1, cellSize - 1, cellSize - 1);
				g.setColor(colors[3][i]);
				g.fillOval(horPos + cellSize * 1, baseY + legendY * cellSize + 1, cellSize - 1, cellSize - 1);
				g.setColor(colors[2][i]);
				g.fillOval(horPos + cellSize * 2, baseY + legendY * cellSize + 1, cellSize - 1, cellSize - 1);
				g.setColor(colors[1][i]);
				g.fillOval(horPos + cellSize * 3, baseY + legendY * cellSize + 1, cellSize - 1, cellSize - 1);
				g.setColor(Color.BLACK);
				g.drawString(Integer.toString(i), horPos + cellSize * 4 + 10, baseY + 10 + legendY * cellSize
						+ (cellSize - displayFont.getSize()) / 2);
				++legendY;
			}
		}
	}

	void drawStringCenter(Graphics2D g, String str, int x, int y, int w, int h) {
		FontMetrics metrics = g.getFontMetrics();
		Rectangle2D rect = metrics.getStringBounds(str, g);
		int descent = metrics.getDescent();
		int height = metrics.getHeight();
		int xPos = (int) (x + w / 2.0 - rect.getWidth() / 2.0);
		int yPos = (int) (y + h / 2.0 + height / 2.0 - descent);
		g.drawString(str, xPos, yPos);
	}

	private Color pegColor(int row, int col, int pegKind) {
		return this.colors[(row & 1) + ((col & 1) << 1)][tc.pegValue[pegKind]];
	}

	public Visualizer(TestCase tc_, int cellSize) {
		getContentPane().add(new DrawerPanel());
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				System.exit(0);
			}
		});
		this.tc = tc_;
		this.N = tc.N;
		this.cellSize = cellSize;
		int width = cellSize * N + EXTRA_WIDTH;
		int height = cellSize * N + EXTRA_HEIGHT;
		addKeyListener(new DrawerKeyListener());
		final int MIN_COLOR = 0x20;
		maxVal = Arrays.stream(tc.pegValue).max().getAsInt();
		for (int v : tc.pegValue) {
			int add = (0xFF - MIN_COLOR) * (v + 4) / (maxVal + 4);
			colors[0][v] = new Color(MIN_COLOR, MIN_COLOR + add, MIN_COLOR + add / 2);
			colors[1][v] = new Color(MIN_COLOR + add, MIN_COLOR + add / 2, MIN_COLOR);
			colors[2][v] = new Color(MIN_COLOR + add * 3 / 4, MIN_COLOR + add * 3 / 4, MIN_COLOR);
			colors[3][v] = new Color(MIN_COLOR, MIN_COLOR + add * 3 / 4, MIN_COLOR + add * 3 / 4);
		}

		setSize(width, height);
		setTitle("Visualizer tool for problem PegJump");
		setResizable(false);
		setVisible(true);
	}

	public static void main(String[] args) {

	}
}
