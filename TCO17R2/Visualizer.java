import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Path2D;
import java.util.List;

class Visualizer extends JFrame {
  private static final int EXTRA_WIDTH = 250;
  private static final int EXTRA_HEIGHT = 50;
  private static final double SCALE = 1.4;
  private TestCase testcase;
  private int S;
  private int turn;
  private List<Snapshot> snapshots;
  private boolean showTroops = true;
  private static final Font baseFont = new Font("Ricty", Font.BOLD, 9);
  private static final Font troopFont = new Font("Ricty", Font.BOLD, 9);
  private static final Font infoFont = new Font("Ricty", Font.BOLD, 11);

  private class DrawerKeyListener extends KeyAdapter {
    public void keyPressed(KeyEvent e) {
      switch (e.getKeyCode()) {
        case KeyEvent.VK_LEFT:
          if (turn > 0) {
            if (e.isMetaDown()) {
              turn = 0;
            } else if (e.isShiftDown()) {
              turn = Math.max(0, turn - 10);
            } else {
              --turn;
            }
            repaint();
          }
          break;
        case KeyEvent.VK_RIGHT:
          if (turn < snapshots.size() - 1) {
            if (e.isMetaDown()) {
              turn = snapshots.size() - 1;
            } else if (e.isShiftDown()) {
              turn = Math.min(snapshots.size() - 1, turn + 10);
            } else {
              ++turn;
            }
            repaint();
          }
          break;
        case KeyEvent.VK_SPACE:
          showTroops = !showTroops;
          repaint();
          break;
      }
    }
  }

  private static int getOwnerColor(int owner) {
    int[] colors = {0x000000, 0x0080FE, 0xFE0080, 0xFE8000, 0x00FE80};
    return colors[owner];
  }

  private class DrawerPanel extends JPanel {
    public void paint(Graphics g) {
      final int MARGIN = 5;
      Snapshot snapshot = snapshots.get(turn);
      Graphics2D g2 = (Graphics2D) g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.scale(SCALE, SCALE);
      g2.translate(MARGIN, MARGIN);
      g.setColor(new Color(0xA0A0A0));
      g.fillRect(0, 0, S + 1, S + 1);

      // draw bases
      g.setFont(baseFont);
      for (int b = 0; b < testcase.B; b++) {
        int c = getOwnerColor(snapshot.bases[b].owner);
        g.setColor(new Color(c));
        int x = snapshot.bases[b].x;
        int y = snapshot.bases[b].y;
        Path2D.Double path = new Path2D.Double();
        if (snapshot.bases[b].growthRate == 1) {
          path.moveTo(x, y - 2 * 2 / Math.sqrt(3));
          path.lineTo(x - 2, y + 2 / Math.sqrt(3));
          path.lineTo(x + 2, y + 2 / Math.sqrt(3));
          path.closePath();
        } else if (snapshot.bases[b].growthRate == 2) {
          path.moveTo(x - 2, y - 2);
          path.lineTo(x - 2, y + 2);
          path.lineTo(x + 2, y + 2);
          path.lineTo(x + 2, y - 2);
          path.closePath();
        } else {
          path.moveTo(x, y - 3);
          path.lineTo(x - 3, y);
          path.lineTo(x, y + 3);
          path.lineTo(x + 3, y);
          path.closePath();
        }

        g2.fill(path);
        g.setColor(new Color(c / 2));
        g2.draw(path);
        g2.drawString("" + snapshot.bases[b].size, x + 1, y);
        if (!showTroops) g2.drawString("" + b, x + 1, y + 8);
      }

      if (showTroops) {
        // draw troops
        g.setFont(troopFont);
        for (Troop t : snapshot.troops) {
          int c = getOwnerColor(t.owner);
          g.setColor(new Color(c));
          g.fillOval(t.x - 1, t.y - 1, 3, 3);
          g.setColor(new Color(c / 2));
          g2.drawString("" + t.size, t.x + 1, t.y);
        }
      }

      g.setFont(infoFont);
      g.setColor(Color.BLACK);

      int horPos = S + 15;

      g2.drawString("Speed = " + testcase.speed, horPos, 20);
      g2.drawString("Simulation step = " + turn, horPos, 40);
      g2.drawString(String.format("Score = %.6f", snapshot.score), horPos, 60);

      // output shares of active players using their respective colors
      int totalUnits = 0;
      for (int i = 0; i <= testcase.NOpp; ++i) {
        totalUnits += snapshot.playerUnits[i];
      }
      for (int i = 0; i <= testcase.NOpp; ++i) {
        g2.setColor(new Color(getOwnerColor(i)));
        String str = String.format("#" + i + "(%.3f) %.6f(%5d/%5d)", testcase.powers[i],
            snapshot.playerUnits[i] * 1.0 / totalUnits, snapshot.playerUnits[i], totalUnits);
        g2.drawString(str, horPos, 80 + 20 * i);
      }
    }
  }

  Visualizer(TestCase testcase, List<Snapshot> snapshots) {
    DrawerPanel panel = new DrawerPanel();
    getContentPane().add(panel);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent event) {
        System.exit(0);
      }
    });

    this.testcase = testcase;
    this.snapshots = snapshots;
    this.S = TestCase.S;
    int width = (int) ((S + EXTRA_WIDTH) * SCALE);
    int height = (int) ((S + EXTRA_HEIGHT) * SCALE);

    addKeyListener(new DrawerKeyListener());
    setSize(width, height);
    setTitle("Visualizer tool for problem AbstractWars");
    setResizable(false);
    setVisible(true);
  }
}
