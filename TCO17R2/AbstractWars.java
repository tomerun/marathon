import java.util.*;

public final class AbstractWars {
  private static final boolean DEBUG = false;
  private static final boolean MEASURE_TIME = false;
  int threadId; // for local testing

  private SplittableRandom rnd = new SplittableRandom(42);
  private int B, O, speed;
  private Base[] bases;
  private int[][] baseOwner, basePop, basePopAfterAttack, attackPlan;
  private int[] availableTime;
  private Attacks[] coming;
  Player[] players;
  private int turn = -1;
  private double score;
  private int[] playerPops;
  private boolean newInfoRevealed;
  private int attackPlanCreatedUntil;
  ArrayList<Troop> troops = new ArrayList<>();
  private Route[][] routes;
  private Simulator simulator;
  private boolean emergency;
  private int prevRemainTime = 15000;

  public int init(int[] baseLocations, int speed) {
    this.B = baseLocations.length / 2;
    this.speed = speed;
    bases = new Base[B];
    baseOwner = new int[2000][B];
    basePop = new int[2000][B];
    basePopAfterAttack = new int[2000][B];
    attackPlan = new int[2000][B];
    availableTime = new int[B];
    coming = new Attacks[B];
    for (int i = 0; i < B; ++i) {
      bases[i] = new Base(baseLocations[2 * i], baseLocations[2 * i + 1]);
      coming[i] = new Attacks();
    }
    for (int i = 0; i < B; ++i) {
      bases[i].others = new BaseTarget[B - 1];
      for (int j = 0, pos = 0; j < B; j++) {
        if (j == i) continue;
        double d = dist2(bases[i], bases[j]);
        bases[i].others[pos] = new BaseTarget(j, 1.0 / d, (int) Math.ceil(Math.sqrt(d) / speed));
        ++pos;
      }
      Arrays.sort(bases[i].others, Comparator.comparingDouble(t -> -t.dist2Rev));
      bases[i].adjacentNear = dist2(bases[i], bases[bases[i].others[0].idx]) <= speed * speed;
    }
    initRoutes();
    simulator = new Simulator();
    return 0;
  }

  private void initRoutes() {
    routes = new Route[B][B];
    for (int i = 0; i < B; i++) {
      for (int j = 0; j < B; j++) {
        if (j == i) continue;
        int dist2 = dist2(bases[i], bases[j]);
        int travelTime = (int) Math.ceil(Math.sqrt(dist2) / speed);
        int[] pos = new int[travelTime - 1];
        for (int k = 1; k < travelTime; k++) {
          double partMoved = k * 1.0 / travelTime;
          int x = (int) (bases[i].x + (bases[j].x - bases[i].x) * partMoved);
          int y = (int) (bases[i].y + (bases[j].y - bases[i].y) * partMoved);
          pos[k - 1] = (x << 10) + y;
        }
        routes[i][j] = new Route(i, j, pos);
      }
    }
  }

  public int[] sendTroops(int[] bs, int[] ts) {
    ++turn;
    checkRemainTime();
    newInfoRevealed = false;
    updateScore(bs, ts);
    processBasesInfo(bs);
    if (!emergency) processTroopsInfo(ts);
    return createPlan();
  }

  private void checkRemainTime() {
    if (emergency) return;
    if (prevRemainTime > 4000) {
      if ((turn & 0x1F) != 0) return;
    } else if (prevRemainTime > 2500) {
      if ((turn & 0x7) != 0) return;
    }
    prevRemainTime = Server.getRemainingTime(threadId);
    emergency = prevRemainTime < 1000;
    if (DEBUG) {
      if (emergency) System.err.println("set emergency at turn:" + turn);
    }
  }

  private void updateScore(int[] bs, int[] ts) {
    if (turn == 0) {
      for (int i = 0; i < B; i++) {
        O = Math.max(O, bs[2 * i]);
      }
      playerPops = new int[O + 1];
      players = new Player[O + 1];
      for (int i = 0; i <= O; i++) {
        players[i] = new Player(i);
      }
    }
    Arrays.fill(playerPops, 0);
    for (int i = 0; i < B; i++) {
      playerPops[bs[2 * i]] += bs[2 * i + 1];
    }
    for (int i = 0; i < ts.length / 4; i++) {
      int owner = ts[i * 4];
      int num = ts[i * 4 + 1];
      playerPops[owner] += num;
    }
    int totalPops = 0;
    for (int i = 0; i <= O; i++) {
      totalPops += playerPops[i];
    }
    score += 1.0 * playerPops[0] / totalPops;
    if (DEBUG) {
      if (playerPops[0] == 0) {
        System.err.println("total score:" + score);
      } else if (playerPops[0] == totalPops) {
        System.err.println("total score:" + (score + 2000 - turn - 1));
      }
    }
  }

  private void processBasesInfo(int[] bs) {
    boolean uniform = true;
    for (int i = 0; i < B; i++) {
      baseOwner[turn][i] = bs[2 * i];
      basePop[turn][i] = basePopAfterAttack[turn][i] = bs[2 * i + 1];
      if (baseOwner[turn][i] != baseOwner[turn][0]) {
        uniform = false;
      }
    }
    if (turn == 1) {
      for (int i = 0; i < B; i++) {
        bases[i].growRate = basePop[turn][i] - basePop[turn - 1][i];
      }
      simulator = new Simulator();
    }
    if (turn > 1 && !uniform) {
      // update players
      for (int i = 0; i < B; i++) {
        int owner = baseOwner[turn][i];
        if (owner != baseOwner[turn - 1][i]) continue;
        if (basePop[turn][i] != nextPop(turn - 1, i)) continue;
        if (owner == 0) continue;
        players[owner].attackMin = Math.max(players[owner].attackMin, basePop[turn - 1][i]);
        players[owner].attackMin = Math.min(players[owner].attackMax - 1, players[owner].attackMin);
      }
    }
  }

  private void processTroopsInfo(int[] ts) {
    for (int i = 0; i < B; i++) {
      coming[i].clearArrived();
    }
    int ti = 0;
    boolean unknown = false;
    for (int i = 0; i < ts.length / 4; ++i) {
      int owner = ts[i * 4];
      if (owner == 0) {
        continue;
      }
      int num = ts[i * 4 + 1];
      int pos = (ts[i * 4 + 2] << 10) + ts[i * 4 + 3];
      boolean match = false;
      for (; ti < troops.size(); ++ti) {
        Troop cur = troops.get(ti);
        if (cur.owner == 0) {
          continue;
        }
        if (cur.owner != owner || cur.pop != num) {
          cur.arrivalTime = turn;
          continue;
        }
        match = examineTroop(cur, pos);
        if (!match) {
          cur.arrivalTime = turn;
          continue;
        }
        cur.lastPos = pos;
        ++ti;
        break;
      }
      if (match) continue;
      Troop troop = new Troop(owner, num, turn - 1, pos);
      examineTroop(troop, pos);
      if (troop.from != -1) {
        if (troop.owner != 0) {
          newInfoRevealed = true;
          players[owner].attackMax = Math.min(players[owner].attackMax, basePop[turn - 1][troop.from]);
          basePopAfterAttack[turn - 1][troop.from] -= troop.pop;
        }
      } else {
        if (DEBUG) System.err.println("unknown at turn " + turn + " owner:" + owner + " num:" + num);
        unknown = true;
      }
      troops.add(troop);
      ++ti;
    }
    while (ti < troops.size()) {
      if (troops.get(ti).owner != 0) {
        troops.get(ti).arrivalTime = turn;
      }
      ++ti;
    }
    ti = 0;
    ArrayList<Troop> arrival = new ArrayList<>();
    for (int i = 0; i < troops.size(); i++) {
      Troop t = troops.get(i);
      if (t.arrivalTime == turn) {
        arrival.add(t);
        if (!t.confident && t.from != -1) {
          t.target = -1;
          for (int j = 0; j < B; ++j) {
            if (j == t.from) continue;
            Route r = routes[t.from][j];
            if (r.pos.length + 1 != turn - t.departTime) continue;
            if (r.pos.length > 0 && r.pos[r.pos.length - 1] == t.lastPos) {
              t.target = t.target == -1 ? j : -2;
            }
          }
          if (t.target >= 0) {
            t.confident = true;
            updateLocality(players[t.owner], t.departTime, t.from, t.target);
          }
        }
        if (!t.confident) {
          unknown = true;
        }
      } else {
        troops.set(ti++, t);
      }
    }
    while (ti < troops.size()) {
      troops.remove(troops.size() - 1);
    }
    if (!unknown) {
      // update power
      int[] arrivalIdx = new int[B];
      Arrays.fill(arrivalIdx, -1);
      for (int i = 0; i < arrival.size(); i++) {
        int to = arrival.get(i).target;
        if (arrivalIdx[to] == -1) {
          arrivalIdx[to] = i;
        } else if (arrivalIdx[to] >= 0) {
          arrivalIdx[to] = -2;
        }
      }
      for (int i = 0; i < B; i++) {
        if (bases[i].adjacentNear) continue;
        if (arrivalIdx[i] < 0) continue;
        Troop troop = arrival.get(arrivalIdx[i]);
        int targetOwner = baseOwner[turn - 1][i];
        int before2 = basePopAfterAttack[turn - 1][i];
        before2 = Math.min(1000, before2 + bases[i].growRate + before2 / 100);
        int after1, after2;
        if (baseOwner[turn][i] == targetOwner) {
          after1 = 0;
          after2 = basePop[turn][i];
        } else {
          after1 = basePop[turn][i];
          after2 = 0;
        }
        updatePower(players[troop.owner], players[targetOwner], troop.pop, before2, after1, after2);
      }
    }
  }

  private boolean examineTroop(Troop troop, int pos) {
    if (troop.confident) {
      Route route = routes[troop.from][troop.target];
      if (turn - troop.departTime >= route.length()) return false;
      return route.pos[turn - troop.departTime - 1] == pos;
    }
    final int NOT_FOUND = -1;
    final int MULTIPLE = -2;
    int candFrom = NOT_FOUND;
    int candTo = NOT_FOUND;
    int start = troop.from == -1 ? 0 : troop.from;
    int end = troop.from == -1 ? B : troop.from + 1;
    int nearestTo = -1;
    double nearestDistRev = 0;
    for (int i = start; i < end; i++) {
      if (baseOwner[troop.departTime][i] != troop.owner || basePop[troop.departTime][i] / 2 != troop.pop) continue;
      int tmpCandTo = NOT_FOUND;
      for (int j = 0; j < B - 1; j++) {
        int bi = bases[i].others[j].idx;
        if (baseOwner[troop.departTime][bi] == troop.owner) continue;
        Route r = routes[i][bi];
        if (r.pos.length < turn - troop.departTime) continue;
        if (r.pos[turn - troop.departTime - 1] != pos) continue;
        if (tmpCandTo != NOT_FOUND) {
          tmpCandTo = MULTIPLE;
          if (nearestDistRev < bases[i].others[j].dist2Rev) {
            nearestTo = bi;
            nearestDistRev = bases[i].others[j].dist2Rev;
          }
        } else {
          tmpCandTo = bi;
          nearestTo = bi;
          nearestDistRev = bases[i].others[j].dist2Rev;
        }
      }
      if (tmpCandTo != NOT_FOUND) {
        if (candFrom == NOT_FOUND) {
          candTo = tmpCandTo;
          candFrom = i;
        } else {
          candFrom = MULTIPLE;
          break;
        }
      }
    }
    if (candFrom != MULTIPLE && candFrom != NOT_FOUND) {
      troop.from = candFrom;
      int oldTarget = troop.target;
      if (candTo != MULTIPLE) {
        troop.confident = true;
        troop.target = candTo;
        troop.arrivalTime = troop.departTime + routes[candFrom][candTo].length();
        updateLocality(players[troop.owner], troop.departTime, troop.from, troop.target);
        if (oldTarget >= 0) {
          switchTroopTarget(troop, oldTarget, candTo);
        } else if (oldTarget == -1) {
          coming[candTo].add(troop);
        }
      } else {
        troop.target = nearestTo; // temporary result
        troop.arrivalTime = troop.departTime + routes[candFrom][nearestTo].length();
        if (nearestTo >= 0) {
          if (oldTarget >= 0) {
            switchTroopTarget(troop, oldTarget, nearestTo);
          } else {
            coming[nearestTo].add(troop);
          }
        }
      }
    }
    return candFrom != NOT_FOUND;
  }

  private void switchTroopTarget(Troop troop, int oldTarget, int newTarget) {
    if (oldTarget == newTarget) return;
    coming[oldTarget].remove(troop);
    coming[newTarget].add(troop);
    newInfoRevealed = true;
  }

  private int[] createPlan() {
    if (turn == 0) return new int[0];
    if (turn == 1) {
      int[] basePopBackup = basePop[1].clone();
      int[] basePopAfterAttackBackup = basePopAfterAttack[1].clone();
      createRapidAttackPlan();
      basePop[1] = basePopBackup;
      basePopAfterAttack[1] = basePopAfterAttackBackup;
    } else if (shouldEscape()) {
      createEscapePlan();
    } else if (newInfoRevealed || turn > attackPlanCreatedUntil) {
      if (emergency) {
        createStrategicPlanEasy();
      } else {
//        createStrategicPlanEasy();
        createStrategicPlan();
      }
    }
    ArrayList<Integer> sends = new ArrayList<>();
    for (int i = 0; i < B; i++) {
      if (baseOwner[turn][i] != 0) continue;
      if (availableTime[i] >= turn) {
        if (attackPlan[turn][i] != -1) {
          for (int j = 0; j <= attackPlan[turn][i] >> 24; j++) {
            createTroop(sends, i, attackPlan[turn][i] & 0xFF);
          }
        }
      }
    }
    assert (sends.size() <= 2 * B);
    while (sends.size() > 2 * B) {
      sends.remove(sends.size() - 1);
    }
    int[] ret = new int[sends.size()];
    for (int i = 0; i < sends.size(); i++) {
      ret[i] = sends.get(i);
    }
    return ret;
  }

  private boolean shouldEscape() {
    if (turn < 105) return false;
    for (int i = 1; i <= O; i++) {
      if (playerPops[0] * 4 < playerPops[i]) return true;
    }
    return false;
  }

  private void createTroop(ArrayList<Integer> sends, int from, int target) {
    sends.add(from);
    sends.add(target);
    Troop troop = new Troop(0, basePopAfterAttack[turn][from] / 2, turn, -1);
    basePopAfterAttack[turn][from] -= troop.pop;
    troop.from = from;
    troop.target = target;
    troop.arrivalTime = turn + routes[from][target].length();
    troop.confident = true;
    troops.add(troop);
    coming[target].add(troop);
  }

  private void createRapidAttackPlan() {
    calcBasePriority();
    final int INITIAL_RAPID_SIM_LIMIT = 295;
    int rapidSimLimit = INITIAL_RAPID_SIM_LIMIT;
    ArrayList<Integer> targetsTmp = new ArrayList<>();
    for (int i = 0; i < B; i++) {
      if (baseOwner[turn][i] == 0) continue;
      if (bases[i].priority == 0) break;
      double minD = -1;
      for (int j = 0; j < B - 1; j++) {
        if (baseOwner[turn][bases[i].others[j].idx] == 0) {
          minD = bases[i].others[j].dist2Rev;
          break;
        }
      }
      int danger = (int) (bases[i].priority * (1 << 15));
      int minV = (int) (Math.sqrt(minD) * (1 << 8));
      targetsTmp.add((danger << 16) | (minV << 8) | i);
    }
    Collections.sort(targetsTmp);
    ArrayList<Integer> targets = new ArrayList<>(targetsTmp.size());
    for (int i = targetsTmp.size() - 1; i >= 0; i--) {
      targets.add(targetsTmp.get(i) & 0xFF);
    }
    double bestScore = -1e9;
    int remainTime = Server.getRemainingTime(this.threadId);
    long startTime = System.currentTimeMillis();
    final long FIRST_TIME_LIMIT = 6000;
    long timelimit = FIRST_TIME_LIMIT;
    int[] swapPos = new int[2];
    double[] ranges = new double[]{0.01, 0.1};
    double range = 0.1;
    for (int i = 1; ; i++) {
      if ((i & 0x3F) == 0) {
        range = ranges[rnd.nextInt(ranges.length)];
        long elapsed = System.currentTimeMillis() - startTime;
        if (timelimit == FIRST_TIME_LIMIT && elapsed > FIRST_TIME_LIMIT) {
          if (bestScore > 0) {
            timelimit = remainTime - 2500;
          } else {
            timelimit = FIRST_TIME_LIMIT + 1000;
            rapidSimLimit = 120 - speed * 5;
            bestScore = -1e9; // reset
          }
        } else if (elapsed > timelimit) {
          if (DEBUG) System.err.println("simulate trial:" + i);
          break;
        }
      }
      double curScore = simulator.simulateRapidAttack(rapidSimLimit, targets);
      if (curScore > 0) curScore += 2000 - rapidSimLimit;
      if (curScore > bestScore) {
        bestScore = curScore;
        if (bestScore > 0) {
          rapidSimLimit = Math.min(rapidSimLimit, simulator.finishTime + 5);
        }
        if (DEBUG) System.err.println("bestScore:" + (bestScore + score) + " at " + i);
        for (int j = turn; j <= rapidSimLimit; j++) {
          System.arraycopy(simulator.target[j], 0, attackPlan[j], 0, B);
        }
        System.arraycopy(simulator.availableTurn, 0, availableTime, 0, B);
      } else if (curScore < bestScore - range) {
        for (int j = swapPos.length / 2 - 1; j >= 0; j--) {
          swap(targets, swapPos[2 * j], swapPos[2 * j + 1]);
        }
      }
      if (targets.size() <= 1) break;
      for (int j = 0; j < swapPos.length / 2; j++) {
        int p1 = rnd.nextInt(targets.size());
        int p2 = rnd.nextInt(targets.size() - 1);
        if (p2 >= p1) p2++;
        swap(targets, p1, p2);
        swapPos[j * 2] = p1;
        swapPos[j * 2 + 1] = p2;
      }
    }
    for (int j = rapidSimLimit + 1; j < 2000; j++) {
      Arrays.fill(attackPlan[j], -1);
    }
  }

  private void createEscapePlan() {
    ArrayList<Integer> escaping = new ArrayList<>();
    ArrayList<Integer> full = new ArrayList<>();
    int attackCount = 0;
    for (int i = 0; i < B; i++) {
      if (baseOwner[turn][i] != 0) continue;
      if (basePop[turn][i] > 990) {
        full.add(i);
        continue;
      }
      Attacks ats = coming[i];
      for (int j = ats.start; j < ats.end; j++) {
        Troop troop = ats.troops[j];
        if (troop.owner != 0 && troop.arrivalTime == turn + 1) {
          escaping.add(i);
          break;
        }
      }
      if (attackPlan[turn][i] != -1) {
        attackCount += attackPlan[turn][i] >> 24;
      }
    }
    for (int i = 0; i < full.size() && attackCount < B; i++) {
      int bi = full.get(i);
      int to = -1;
      for (int j = B - 2; j >= 0; j--) {
        int tbi = bases[bi].others[j].idx;
        if (baseOwner[turn][tbi] != 0) {
          to = tbi;
          break;
        }
      }
      if (to == -1) to = bases[bi].others[B - 2].idx;
      attackPlan[turn][bi] = to;
      availableTime[bi] = turn + 1;
      ++attackCount;
    }
    int[] after = new int[escaping.size()];
    for (int i = 0; i < escaping.size(); i++) {
      after[i] = basePop[turn][escaping.get(i)];
    }
    for (int i = 0; attackCount < B; i++) {
      boolean update = false;
      for (int j = 0; j < escaping.size() && attackCount < B; j++) {
        if (after[j] <= 1) continue;
        int bi = escaping.get(j);
        int to = bases[bi].others[B - 2].idx;
        if (i == 0) {
          attackPlan[turn][bi] = to;
        } else {
          attackPlan[turn][bi] += 1 << 24;
        }
        after[j] -= after[j] / 2;
        availableTime[bi] = turn + 1;
        ++attackCount;
        update = true;
      }
      if (!update) break;
    }
  }

  private void createStrategicPlan() {
    int enemyBaseCount = 0;
    for (int i = 0; i < B; i++) {
      if (baseOwner[turn][i] != 0) {
        ++enemyBaseCount;
      }
    }
    if (enemyBaseCount == 0) {
      for (int i = 0; i < B; i++) {
        if (basePop[turn][i] > 988) {
          attackPlan[turn][i] = bases[i].others[B - 2].idx;
          availableTime[i] = turn + 1;
        }
      }
    } else if (enemyBaseCount == 1) {
      int enemyBase = 0;
      for (int i = 0; i < B; i++) {
        if (baseOwner[turn][i] != 0) {
          enemyBase = i;
          break;
        }
      }
      for (int i = 0; i < B; i++) {
        if (basePop[turn][i] > 988) {
          attackPlan[turn][i] = enemyBase;
          availableTime[i] = turn + 1;
        }
      }
    }
    calcBasePriority();
    ArrayList<Troop> created = new ArrayList<>();
    boolean[] finished = new boolean[B];
    final int THRESHOLD_SPAWN = enemyBaseCount <= 1 ? 300 : 500;
    final int THRESHOLD_ATTACK = enemyBaseCount <= 1 ? 300 : 990;
    final int THRESHOLD_PARTIAL = enemyBaseCount <= 1 ? 300 : 990;
    int attackCount = 0;
    for (int i = 0; i < B; i++) {
      if (attackPlan[turn][i] != -1) attackCount += (attackPlan[turn][i] >> 24) + 1;
    }
    for (int i = 0; i < B && attackCount < B; i++) {
      // should attack?
      if (baseOwner[turn][i] != 0) continue;
      if (basePop[turn][i] < THRESHOLD_SPAWN) continue;
      if (availableTime[i] > turn) continue;
      int spawnPop = basePopAfterAttack[turn][i] / 2;
      if (basePop[turn][i] < 990) {
        basePopAfterAttack[turn][i] -= spawnPop;
        boolean spawnOk = isFutureOwnerMe(i) > 0;
        basePopAfterAttack[turn][i] += spawnPop;
        if (!spawnOk) continue;
      }

      int target = -1;

      // where to attack?
      if (target == -1 && basePop[turn][i] >= THRESHOLD_ATTACK) {
        for (int j = 0; j < B - 1; j++) {
          int bi = bases[i].others[j].idx;
          if (finished[bi]) continue;
          if (isFutureOwnerMe(bi) > 0) {
            finished[bi] = true;
            continue;
          }
          Troop troop = new Troop(0, spawnPop, turn, (bases[i].x << 8) | bases[i].y);
          troop.from = i;
          troop.target = bi;
          troop.arrivalTime = turn + routes[i][bi].length();
          coming[bi].add(troop);
          if (basePop[turn][i] < THRESHOLD_PARTIAL) {
            if (isFutureOwnerMe(bi) <= 0) {
              coming[bi].remove(troop);
              continue;
            }
          }
          created.add(troop);
          target = bi;
          break;
        }
      }
      if (target == -1) {
        boolean attackSomewhere = basePop[turn][i] > 988;
        if (!attackSomewhere) {
          Attacks ats = coming[i];
          if (ats.start != ats.end) {
            Troop troop = ats.troops[ats.start];
            if (troop.owner == 0 && troop.arrivalTime == turn + 1 && basePop[turn][i] + troop.pop > 1000) {
              attackSomewhere = true;
            }
          }
        }
        if (attackSomewhere) {
          for (int j = 0; j < B - 1; j++) {
            int bi = bases[i].others[j].idx;
            if (baseOwner[turn][bi] != 0) {
              Troop troop = new Troop(0, spawnPop, turn, (bases[i].x << 8) | bases[i].y);
              troop.from = i;
              troop.target = bi;
              troop.arrivalTime = turn + routes[i][bi].length();
              coming[bi].add(troop);
              created.add(troop);
              target = bi;
              break;
            }
          }
        }
      }
      if (target != -1) {
        attackPlan[turn][i] = target;
        availableTime[i] = turn + 1;
        ++attackCount;
      }
    }

    // revert
    for (Troop troop : created) {
      coming[troop.target].remove(troop);
    }
    attackPlanCreatedUntil = turn;
  }

  private int isFutureOwnerMe(int base) {
    Attacks ats = coming[base];
    int time = turn;
    int pop = basePopAfterAttack[turn][base];
    int owner = baseOwner[turn][base];
    boolean changed = false;
    for (int j = ats.start; j < ats.end; j++) {
      Troop troop = ats.troops[j];
      while (time < troop.arrivalTime) {
        if (owner != 0 && pop >= players[owner].attackMax) {
          pop -= pop / 2;
        }
        pop += pop / 100 + bases[base].growRate;
        pop = Math.min(pop, 1000);
        ++time;
      }
      if (troop.owner == owner) {
        pop += troop.pop;
        pop = Math.min(pop, 1000);
      } else {
        pop -= (int) Math.ceil(troop.pop * players[troop.owner].powerMax / players[owner].powerMax);
        if (pop < 0) {
          pop = -pop - 1;
          owner = troop.owner;
          changed = true;
        }
      }
      if (owner != 0 && pop >= players[owner].attackMax) {
        pop -= pop / 2;
      }
    }
    if (owner != 0) return -1;
    return changed ? pop - 10 : pop;
  }

  private void createStrategicPlanEasy() {
    final int THRESHOLD = 990;
    for (int i = 0; i < B; i++) {
      if (baseOwner[turn][i] != 0) continue;
      if (basePop[turn][i] < THRESHOLD) continue;
      if (availableTime[i] > turn) continue;
      int target = -1;
      for (int j = 0; j < B - 1; j++) {
        int bi = bases[i].others[j].idx;
        if (baseOwner[turn][bi] == 0) continue;
        target = bi;
        break;
      }
//      if (target == -1) {
//        target = bases[i].others[B - 2].idx;
//      }
      if (target == -1) continue;
      attackPlan[turn][i] = target;
      availableTime[i] = turn + 1;
    }
  }

  private static int dist2(Base b1, Base b2) {
    return dist2(b1.x, b1.y, b2.x, b2.y);
  }

  private static int dist2(int x1, int y1, int x2, int y2) {
    return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
  }

  private static void swap(ArrayList<Integer> list, int p1, int p2) {
    int tmp = list.get(p1);
    list.set(p1, list.get(p2));
    list.set(p2, tmp);
  }

  private class Attacks {
    Troop[] troops = new Troop[16];
    int start, end;

    void add(Troop troop) {
      if (end == troops.length) {
        Troop[] newTroops = new Troop[troops.length * 2];
        System.arraycopy(troops, 0, newTroops, 0, troops.length);
        troops = newTroops;
      }
      int insertPos = end;
      for (; insertPos > start; insertPos--) {
        if (troops[insertPos - 1].compareTo(troop) > 0) {
          troops[insertPos] = troops[insertPos - 1];
        } else {
          break;
        }
      }
      troops[insertPos] = troop;
      ++end;
    }

    void remove(Troop troop) {
      for (int i = end - 1; i >= start; i--) {
        if (troops[i] == troop) {
          for (int j = i + 1; j < end; j++) {
            troops[j - 1] = troops[j];
          }
          troops[end - 1] = null;
          --end;
          break;
        }
      }
    }

    void clearArrived() {
      while (start < end && troops[start].arrivalTime == turn) {
        ++start;
      }
      fit();
    }

    private void fit() {
      if (start >= troops.length / 2) {
        int len = end - start;
        for (int i = start; i < end; i++) {
          troops[i - start] = troops[i];
        }
        for (int i = len; i < end; i++) {
          troops[i] = null;
        }
        start = 0;
        end = len;
      }
    }
  }

  private class Simulator {

    int[][] target;
    int[] availableTurn;
    int[] playerPops;
    int[] myMovingPops, attackCount;
    int lastAttackTime, finishTime;

    Simulator() {
      target = new int[2000][B];
      availableTurn = new int[B];
      playerPops = new int[O + 1];
      myMovingPops = new int[2000];
      attackCount = new int[2000];
      for (int i = 0; i < target.length; i++) {
        Arrays.fill(target[i], -1);
      }
    }

    double simulateRapidAttack(int until, ArrayList<Integer> order) {
      for (int i = turn; i <= lastAttackTime; i++) {
        Arrays.fill(target[i], -1);
        myMovingPops[i] = 0;
        attackCount[i] = 0;
      }
      for (int i = lastAttackTime + 1; i < until; i++) {
        myMovingPops[i] = 0;
        attackCount[i] = 0;
      }
      lastAttackTime = 0;
      for (int i = 0; i < B; i++) {
        availableTurn[i] = baseOwner[turn][i] == 0 ? turn : -1;
      }
      int[] froms = new int[B];
      int fromsSize = 0;
      double gain = 0;
      for (int oi = 0; oi < order.size(); ++oi) {
        int to = order.get(oi);
        fromsSize = 0;
        for (int i = 0; i < B; i++) {
          if (availableTurn[i] != -1) {
            int arriveTime = availableTurn[i] + routes[i][to].length();
            froms[fromsSize++] = (arriveTime << 20) | (availableTurn[i] << 8) | i;
          }
        }
        Arrays.sort(froms, 0, fromsSize);
        boolean win = false;
        for (int step = turn + 1; step < until; step++) {
          basePop[step][to] = nextPop(step - 1, to);
          baseOwner[step][to] = baseOwner[step - 1][to];
          FROM_LOOP:
          for (int i = 0; i < fromsSize; i++) {
            int arriveTime = froms[i] >> 20;
            int from = froms[i] & 0xFF;
            if (arriveTime > step) {
              if (arriveTime == step + 1 && i > 0) { // sort by depart time, base id
                for (int j = i; j < fromsSize && (froms[j] >> 20) == step + 1; j++) {
                  int swap = froms[j];
                  for (int k = j - 1; ; k--) {
                    if (k == -1) {
                      froms[0] = swap;
                      break;
                    }
                    assert ((froms[k] >> 20) == arriveTime);
                    if (swap < froms[k]) {
                      froms[k + 1] = froms[k];
                    } else {
                      froms[k + 1] = swap;
                      break;
                    }
                  }
                }
              }
              break;
            }
            int attackTime = step - routes[from][to].length();
            assert (availableTurn[from] == attackTime);
            availableTurn[from] = attackTime + 1;
            froms[i] = ((arriveTime + 1) << 20) | ((attackTime + 1) << 8) | from;
            basePopAfterAttack[attackTime][from] = basePop[attackTime][from];
            baseOwner[attackTime + 1][from] = 0;

            int before = basePopAfterAttack[attackTime][from];
            if ((before == 3 || before == 4) && basePop[step][to] >= before - 1 && attackCount[attackTime] < B - 1) {
              int attackNum = before - 1;
              basePopAfterAttack[attackTime][from] = 1;
              basePop[attackTime + 1][from] = 1 + bases[from].growRate;
              basePop[step][to] -= attackNum;
              target[attackTime][from] = (1 << 24) + to;
              attackCount[attackTime] += 2;
              lastAttackTime = Math.max(lastAttackTime, attackTime);
              myMovingPops[attackTime + 1] += attackNum;
              myMovingPops[step] -= attackNum;
            } else {
              basePop[attackTime + 1][from] = nextPop(attackTime, from);
              for (int j = 0; j < 3 && basePopAfterAttack[attackTime][from] > 1 && attackCount[attackTime] < B; j++) {
                int attackNum = basePopAfterAttack[attackTime][from] / 2;
                basePopAfterAttack[attackTime][from] -= attackNum;
                basePop[attackTime + 1][from] = nextPop(attackTime, from);
                basePop[step][to] -= attackNum > 6 ? (int) Math.ceil(attackNum * (1.0 / 1.1999)) : attackNum;
                if (j == 0) {
                  target[attackTime][from] = to;
                  lastAttackTime = Math.max(lastAttackTime, attackTime);
                } else {
                  target[attackTime][from] += (1 << 24);
                }
                attackCount[attackTime]++;
//            System.err.println("attack:" + from + "->" + to + " at step " + attackTime + " with " + attackNum);
                myMovingPops[attackTime + 1] += attackNum;
                myMovingPops[step] -= attackNum;
                if (basePop[step][to] < 0) {
                  basePop[step][to] = -basePop[step][to];
                  if (basePop[step][to] < attackNum) {
                    basePop[step][to]--;
                  }
                  if (basePop[step][to] > 0) {
                    baseOwner[step][to] = 0;
                    availableTurn[to] = step;
                    break FROM_LOOP;
                  }
                }
              }
            }
          }
          basePopAfterAttack[step][to] = basePop[step][to];
          if (baseOwner[step][to] == 0) {
            win = true;
            break;
          }
          if (basePop[step][to] > 990) {
            for (int i = turn; i <= lastAttackTime; i++) {
              for (int j = 0; j < B; j++) {
                if ((target[i][j] & 0xFF) == to) target[i][j] = -1; // cancel attack
              }
            }
            return -10000 + gain;
          }
        }
        if (!win) {
          for (int i = turn; i <= lastAttackTime; i++) {
            for (int j = 0; j < B; j++) {
              if ((target[i][j] & 0xFF) == to) target[i][j] = -1; // cancel attack
            }
          }
          return -5000 + gain;
        }
        gain += bases[to].priority;
      }
      double score = 0;
      int myMovingSum = 0;
      for (int step = turn + 1; step < until; step++) {
        myMovingSum += myMovingPops[step];
        Arrays.fill(playerPops, 0);
        playerPops[0] += myMovingSum;
        for (int i = 0; i < B; i++) {
          if (availableTurn[i] < step) {
            basePop[step][i] = basePopAfterAttack[step][i] = nextPop(step - 1, i);
            baseOwner[step][i] = baseOwner[step - 1][i];
          } else if (availableTurn[i] == step) {
            basePopAfterAttack[step][i] = basePop[step][i];
          }
//          System.err.println(i + " " + baseOwner[step][i] + " " + basePop[step][i] + " " + availableTurn[i]);
          playerPops[baseOwner[step][i]] += basePop[step][i];
        }
        int totalPop = 0;
        for (int i = 0; i <= O; i++) {
          totalPop += playerPops[i];
        }
        if (playerPops[0] == totalPop) {
          score += until - step;
          finishTime = step;
          break;
        }
        score += 1.0 * playerPops[0] / totalPop;
//        if (DEBUG) {
//          System.err.println("step:" + step + " myMovingSum:" + myMovingSum);
//          System.err.println((score + AbstractWars.this.score) + " " + Arrays.toString(playerPops));
//        }
      }
      int totalPop = 0;
      for (int i = 0; i <= O; i++) {
        totalPop += playerPops[i];
      }
      assert (playerPops[0] == totalPop);
      return score;
    }
  }

  private int nextPop(int turn, int baseIdx) {
    int prev = basePopAfterAttack[turn][baseIdx];
    return prev == 0 ? 0 : Math.min(1000, prev + prev / 100 + bases[baseIdx].growRate);
  }

  private void calcBasePriority() {
    for (int i = 0; i < B; i++) {
      double sumMy = 0;
      double sumAll = 0;
      for (int j = 0; j < B - 1; j++) {
        int bi = bases[i].others[j].idx;
        sumAll += bases[i].others[j].dist2Rev;
        if (baseOwner[turn][bi] == 0) sumMy += bases[i].others[j].dist2Rev;
      }
      bases[i].priority = sumMy / sumAll * bases[i].growRate / 3; // normalize to [0-1]
//      System.err.println("dang " + i + " " + bases[i].priority);
    }
  }

  private static final class Base {
    int x, y, growRate;
    BaseTarget[] others;
    boolean adjacentNear;
    double priority;

    Base(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }

  private static final class BaseTarget {
    int idx, travelTime;
    double dist2Rev;

    BaseTarget(int idx, double dist2Rev, int travelTime) {
      this.idx = idx;
      this.dist2Rev = dist2Rev;
      this.travelTime = travelTime;
    }
  }

  private static final class Troop implements Comparable<Troop> {
    int owner, pop, from, target, departTime, arrivalTime, lastPos;
    boolean confident;

    Troop(int owner, int pop, int departTime, int pos) {
      this.owner = owner;
      this.pop = pop;
      this.from = -1;
      this.target = -1;
      this.departTime = departTime;
      this.arrivalTime = -1;
      this.lastPos = pos;
    }

    @Override
    public int compareTo(Troop o) {
      if (this.arrivalTime != o.arrivalTime) return Integer.compare(this.arrivalTime, o.arrivalTime);
      if (this.departTime != o.departTime) return Integer.compare(this.departTime, o.departTime);
      if (this.owner != o.owner) return Integer.compare(this.owner, o.owner);
      if (this.from != o.from) return Integer.compare(this.from, o.from);
      return this.pop - o.pop;
    }
  }

  private static final class Route {
    int from, to;
    int[] pos;

    Route(int from, int to, int[] pos) {
      this.from = from;
      this.to = to;
      this.pos = pos;
    }

    int length() {
      return pos.length + 1;
    }
  }

  private void updateLocality(Player player, int departTurn, int departBase, int targetBase) {
    // Bayesian inference
    if (player.id == 0) return;
    int my = baseOwner[departTurn][departBase];
    ArrayList<Double> dists = new ArrayList<>();
    double targetDist = 0;
    for (int i = 0; i < B - 1; i++) {
      BaseTarget bt = bases[departBase].others[i];
      if (baseOwner[departTurn][bt.idx] == my) continue;
      dists.add(bt.dist2Rev);
      if (bt.idx == targetBase) targetDist = bt.dist2Rev;
    }
    double sumProb = 0;
    for (int i = 0; i < player.locality.length; i++) {
      double sum = 0;
      for (int j = 0; j < dists.size(); j++) {
        sum += Math.pow(dists.get(j), 1.0 + 2.0 * i / (player.locality.length - 1));
      }
      double prob = Math.pow(targetDist, 1.0 + 2.0 * i / (player.locality.length - 1)) / sum;
      player.locality[i] = player.locality[i] * prob;
      sumProb += player.locality[i];
    }
    for (int i = 0; i < player.locality.length; i++) {
      player.locality[i] /= sumProb;
    }
  }

  private static void updatePower(Player p1, Player p2, int before1, int before2, int after1, int after2) {
    if (p1.id == p2.id) return;
    if (after2 != 0) {
      updatePower(p2, p1, before2, before1, after2, after1);
      return;
    }
    int diff1 = before1 - after1;
    int diff2 = before2;
    if (diff1 < 6 && diff2 < 6) return;
    // (diff1 - 1) * p1 < diff2 * p2 <= diff1 * p1
    // -> diff2 / diff1 * p2 <= p1 < diff2 / (diff1 - 1) * p2

    double newMin1 = p2.powerMin * diff2 / diff1;
    double newMax1 = p2.powerMax * diff2 / (diff1 - 1);
    double newMin2 = p1.powerMin * (diff1 - 1) / diff2;
    double newMax2 = p1.powerMax * diff1 / diff2;
    p1.powerMin = Math.max(p1.powerMin, Math.min(p1.powerMax, newMin1));
    p1.powerMax = Math.min(p1.powerMax, Math.max(p1.powerMin, newMax1));
    p2.powerMin = Math.max(p2.powerMin, Math.min(p2.powerMax, newMin2));
    p2.powerMax = Math.min(p2.powerMax, Math.max(p2.powerMin, newMax2));
//    System.err.println(p1.id + " " + p2.id + " " + before1 + " " + before2 + " " + after1);
//    System.err.println(p1.powerMin + " " + p1.powerMax + " " + p2.powerMin + " " + p2.powerMax);
  }

  static final class Player {
    int id;
    int attackMin = 500, attackMax = 999;
    double powerMin = 1.0, powerMax = 1.2;
    double[] locality;

    Player(int id) {
      this.id = id;
      if (id == 0) {
        powerMin = powerMax = 1.0;
      } else {
        locality = new double[32];
        Arrays.fill(locality, 1.0 / locality.length);
      }
    }
  }

  static final class Timer {
    ArrayList<Long> sum = new ArrayList<>();
    ArrayList<Long> start = new ArrayList<>();

    void start(int i) {
      if (MEASURE_TIME) {
        while (sum.size() <= i) {
          sum.add(0L);
          start.add(0L);
        }
        start.set(i, System.currentTimeMillis());
      }
    }

    void stop(int i) {
      if (MEASURE_TIME) {
        sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
      }
    }

    void print() {
      if (MEASURE_TIME && !sum.isEmpty()) {
        System.err.print("[");
        for (int i = 0; i < sum.size(); ++i) {
          System.err.print(sum.get(i) + ", ");
        }
        System.err.println("]");
      }
    }
  }
}
