#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:B, :O, :S, :elapsed, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /B:*(\d+) O:*(\d+) S:*(\d+)/
      testcase.B = $1.to_i
      testcase.O = $2.to_i
      testcase.S = $3.to_i
    elsif line =~ /elapsed:(.+)/
      testcase.elapsed = $1.to_f
    elsif line =~ /score:(.+)/
      testcase.score = $1.to_f
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  sum_score_diff_relative = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    sum_score_diff += s2 - s1
    count += 1
    if s1 < s2
      win += 1
    elsif s1 > s2
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  20.step(100, 10) do |param|
    puts "B:#{param}-#{param + 10 - 1}"
    calc(from, to) { |from, to| from.B.between?(param, param + 10 - 1) }
  end
  puts "---------------------"
  1.step(4, 1) do |param|
    puts "O:#{param}"
    calc(from, to) { |from, to| from.O == param }
  end
  puts "---------------------"
  1.step(10, 1) do |param|
    puts "S:#{param}"
    calc(from, to) { |from, to| from.S == param }
  end
  puts "---------------------"
  0.step(1999, 200) do |param|
    puts "score:#{param}-#{param + 200}"
    calc(from, to) { |from, to| from.score.between?(param, param + 200) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
