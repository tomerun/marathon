import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class Troop {
  int owner;
  int size;
  int x, y;
  int sourceId;
  int targetId;
  int depTime;
  int arrivalTime;

  public Troop clone() {
    Troop ret = new Troop();
    ret.owner = this.owner;
    ret.size = this.size;
    ret.x = this.x;
    ret.y = this.y;
    ret.sourceId = this.sourceId;
    ret.targetId = this.targetId;
    ret.depTime = this.depTime;
    ret.arrivalTime = this.arrivalTime;
    return ret;
  }
}

class Base {
  int x, y;
  int owner;
  int size;
  int growthRate;

  public Base clone() {
    Base ret = new Base();
    ret.x = this.x;
    ret.y = this.y;
    ret.owner = this.owner;
    ret.size = this.size;
    ret.growthRate = this.growthRate;
    return ret;
  }
}

class Snapshot {
  Troop[] troops;
  Base[] bases;
  double score;
  long[] playerUnits;
}

class TestCase {
  static final int SIMULATION_TIME = 2000;
  static final int PERSON_CAP = 1000;
  static final int S = 600;
  private static final int MIN_BASE_COUNT = 20;
  private static final int MAX_BASE_COUNT = 100;
  private static final int MIN_GROWTH_RATE = 1;
  private static final int MAX_GROWTH_RATE = 3;
  private static final int MIN_PERSONNEL = 1;
  private static final int MAX_PERSONNEL = 10;
  private static final int MIN_SPEED = 1;
  private static final int MAX_SPEED = 10;
  private static final int MIN_OPPONENTS = 1;
  private static final int MAX_OPPONENTS = 4;

  int NOpp;        // player 0 is the player, players 1..NOpp are AI opponents
  double[] powers; // all opponents except player have strong troops
  int B;
  Base[] bases;
  int speed;

  TestCase(long seed) throws Exception {
    SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
    rnd.setSeed(seed);

    B = rnd.nextInt(MAX_BASE_COUNT - MIN_BASE_COUNT + 1) + MIN_BASE_COUNT;
    speed = rnd.nextInt(MAX_SPEED - MIN_SPEED + 1) + MIN_SPEED;
    NOpp = rnd.nextInt(MAX_OPPONENTS - MIN_OPPONENTS + 1) + MIN_OPPONENTS;

    if (seed == 1) {
      B = MIN_BASE_COUNT;
      speed = MIN_SPEED;
      NOpp = MIN_OPPONENTS;
    } else if (seed == 2) {
      B = MAX_BASE_COUNT;
      speed = MAX_SPEED;
      NOpp = MAX_OPPONENTS;
    }
    if (1000 <= seed && seed < 2000) {
      NOpp = 1 + (int) (seed - 1000) / 250;
      speed = 1 + (int) (seed - 1000) % 250 / 25;
    }

    bases = new Base[B];
    HashSet<Integer> locations = new HashSet<>();
    for (int i = 0; i < B; i++) {
      int bx, by;
      while (true) {
        bx = rnd.nextInt(S - 2) + 1;
        by = rnd.nextInt(S - 2) + 1;
        Integer loc = S * bx + by;
        if (locations.contains(loc)) continue;
        locations.add(loc);
        break;
      }
      bases[i] = new Base();
      bases[i].x = bx;
      bases[i].y = by;
      bases[i].owner = rnd.nextInt(NOpp + 1);
      bases[i].size = rnd.nextInt(MAX_PERSONNEL - MIN_PERSONNEL + 1) + MIN_PERSONNEL;
      bases[i].growthRate = rnd.nextInt(MAX_GROWTH_RATE - MIN_GROWTH_RATE + 1) + MIN_GROWTH_RATE;
    }

    powers = new double[NOpp + 1];
    powers[0] = 1.0;
    for (int i = 1; i <= NOpp; ++i)
      powers[i] = 1.0 + rnd.nextDouble() * 0.2;
  }
}

class World {
  private TestCase tc;
  private int curStep = -1;
  List<Troop> troops = new ArrayList<>();
  double playerScore = 0;
  long totalUnits;
  long[] playersUnits;
  boolean simComplete = false;
  boolean perfect = true;

  World(TestCase tc) {
    this.tc = tc;
  }

  void updateScore() {
    totalUnits = 0;
    playersUnits = new long[tc.NOpp + 1];
    for (int i = 0; i < tc.B; ++i) {
      totalUnits += tc.bases[i].size;
      playersUnits[tc.bases[i].owner] += tc.bases[i].size;
    }
    for (Troop t : troops) {
      totalUnits += t.size;
      playersUnits[t.owner] += t.size;
    }
    if (totalUnits > 0)
      playerScore += playersUnits[0] * 1.0 / totalUnits;
    if (playersUnits[0] == totalUnits || playersUnits[0] == 0) {
      simComplete = true;
    }
  }

  void updateTroopDepartures(int owner, int[] attacks) {
    String warnPrefix = "WARNING: time step = " + curStep + ". ";
    // run through attacks, ignoring invalid ones (but print warning if user's one is invalid)
    for (int i = 0; i < attacks.length / 2; ++i) {
      int from = attacks[2 * i];
      int to = attacks[2 * i + 1];
      if (from < 0 || from >= tc.B || to < 0 || to >= tc.B) {
        System.err.println(warnPrefix + "Invalid base index in troop sending attempt " + i + ", ignoring.");
        continue;
      }
      if (tc.bases[from].owner != owner) {
        System.err.println(warnPrefix + "Base not owned by you in troop sending attempt " + i + ", ignoring.");
        continue;
      }
      if (from == to) {
        System.err.println(warnPrefix + "Sending troop from the base to itself in troop sending attempt " + i + ", ignoring.");
        continue;
      }
      if (tc.bases[from].size < 2) {
//        System.err.println(warnPrefix + "Source base has less than 2 units in troop sending attempt " + i + ", ignoring.");
        continue;
      }
      if (owner != 0) perfect = false;

      // spawn a new troop from source base
      Troop t = new Troop();
      t.owner = owner;
      t.size = tc.bases[from].size / 2;
      t.x = tc.bases[from].x;
      t.y = tc.bases[from].y;
      t.sourceId = from;
      t.targetId = to;
      t.depTime = curStep;
      int dx = t.x - tc.bases[to].x;
      int dy = t.y - tc.bases[to].y;
      int moveT = (int) Math.ceil(Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)) / tc.speed);
      t.arrivalTime = t.depTime + moveT;
      troops.add(t);
      tc.bases[from].size -= t.size;
    }
  }

  void updateTroopArrivals() {
    for (int t = 0; t < troops.size(); ) {
      if (troops.get(t).arrivalTime != curStep) {
        t++;
        continue;
      }
      // let troop interact with the base
      int town = troops.get(t).owner;
      int tsize = troops.get(t).size;
      int ttarget = troops.get(t).targetId;
      if (town == tc.bases[ttarget].owner) {
        // reinforcement scenario
        tc.bases[ttarget].size += tsize;
      } else {
        // attack scenario
        // compare sizes of troop and base with respect to their powers
        // attack/defense power = size * powers[owner]
        double pTroop = tsize * tc.powers[town];
        double pBase = tc.bases[ttarget].size * tc.powers[tc.bases[ttarget].owner];
        if (pBase >= pTroop) {
          // base wins but loses as many units as is necessary to overpower all units of the troop (rounding up)
          // it's possible that the base becomes empty
          tc.bases[ttarget].size = Math.max(0, tc.bases[ttarget].size - (int) Math.ceil(pTroop / tc.powers[tc.bases[ttarget].owner]));
          // empty bases preserve their ownership, but that doesn't affect anything except visualization
        } else {
          // troop wins, occupies the bases but loses units
          tc.bases[ttarget].size = Math.max(0, tsize - (int) Math.ceil(pBase / tc.powers[town]));
          tc.bases[ttarget].owner = town;
        }
      }
      // make sure that after troop arrivals the base doesn't hold more than cap units
      if (tc.bases[ttarget].size > TestCase.PERSON_CAP)
        tc.bases[ttarget].size = TestCase.PERSON_CAP;
      troops.remove(t);
    }
  }

  void startNewStep() {
    curStep++;
    for (int i = 0; i < tc.B; ++i) {
      if (tc.bases[i].size > 0)
        tc.bases[i].size += tc.bases[i].growthRate + tc.bases[i].size / 100;
      if (tc.bases[i].size > TestCase.PERSON_CAP)
        tc.bases[i].size = TestCase.PERSON_CAP;
    }
    for (Troop t : troops) {
      if (t.arrivalTime == curStep) {
        t.x = tc.bases[t.targetId].x;
        t.y = tc.bases[t.targetId].y;
        continue;
      }
      double partMoved = (curStep - t.depTime) * 1.0 / (t.arrivalTime - t.depTime);
      double x = tc.bases[t.sourceId].x + (tc.bases[t.targetId].x - tc.bases[t.sourceId].x) * partMoved;
      double y = tc.bases[t.sourceId].y + (tc.bases[t.targetId].y - tc.bases[t.sourceId].y) * partMoved;
      t.x = (int) x;
      t.y = (int) y;
    }
  }

}

class RealAI {
  private SecureRandom rnd;
  int attackT;
  double locality;
  private int B;
  private int owner;
  private int[] baseX, baseY;
  private ArrayList<Integer> others;

  RealAI(long seed, int own) throws Exception {
    rnd = SecureRandom.getInstance("SHA1PRNG");
    rnd.setSeed(seed);
    owner = own;
    // once the base personnel reaches this threshold, the base sends out troops
    attackT = TestCase.PERSON_CAP / 2 + rnd.nextInt(TestCase.PERSON_CAP / 2);
    // the higher the locality, the more value there is in attacking nearby bases
    locality = rnd.nextDouble() * 2 + 1;
  }

  int init(int[] baseLocations, int speed) {
    B = baseLocations.length / 2;
    baseX = new int[B];
    baseY = new int[B];
    for (int i = 0; i < B; ++i) {
      baseX[i] = baseLocations[2 * i];
      baseY[i] = baseLocations[2 * i + 1];
    }
    return 0;
  }

  // picks a random base to attack based on distance to the opponent bases: the closer the base, the higher the chances are
  private int getRandomBase(int sourceInd) {
    double[] probs = new double[others.size()];
    double sp = 0;
    for (int i = 0; i < others.size(); ++i) {
      int ind = others.get(i);
      int dx = baseX[sourceInd] - baseX[ind];
      int dy = baseY[sourceInd] - baseY[ind];
      probs[i] = Math.pow(1.0 / (Math.pow(dx, 2) + Math.pow(dy, 2)), locality);
      sp += probs[i];
    }

    double r = rnd.nextDouble() * sp;
    double s = 0;
    for (int i = 0; i < others.size(); ++i) {
      s += probs[i];
      if (s >= r)
        return others.get(i);
    }
    return others.get(others.size() - 1);
  }

  int[] sendTroops(int[] bases, int[] troops) {
    // compile the list of bases owned by other players
    others = new ArrayList<>();
    for (int i = 0; i < B; ++i)
      if (bases[2 * i] != owner)
        others.add(i);
    if (others.size() == 0) {
      // noone to fight!
      return new int[0];
    }

    ArrayList<Integer> att = new ArrayList<>();
    for (int i = 0; i < B; ++i) {
      if (bases[2 * i] == owner && bases[2 * i + 1] > attackT) {
        // send troops to a random base of different ownership
        att.add(i);
        att.add(getRandomBase(i));
      }
    }
    int[] ret = new int[att.size()];
    for (int i = 0; i < att.size(); ++i)
      ret[i] = att.get(i);
    return ret;
  }
}

public class Tester {
  private static boolean vis = false;
  private AbstractWars solution = new AbstractWars();
  private RealAI[] realAIs;

  private int callInit(int owner, int[] baseLocations, int speed) throws IOException {
    if (owner > 0) {
      return realAIs[owner - 1].init(baseLocations, speed);
    } else {
      return solution.init(baseLocations, speed);
    }
  }

  private int[] callSendTroops(int owner, int[] bases, int[] troops) throws IOException {
    if (owner > 0) {
      return realAIs[owner - 1].sendTroops(bases, troops);
    } else {
      return solution.sendTroops(bases, troops);
    }
  }

  private Result runTest(long seed, int threadId) throws Exception {
    TestCase tc = new TestCase(seed);
    Result res = new Result();
    res.seed = seed;
    res.B = tc.B;
    res.O = tc.NOpp;
    res.S = tc.speed;
    res.powers = tc.powers.clone();
    ArrayList<Snapshot> snapshots = new ArrayList<>();
    solution.threadId = threadId;

    realAIs = new RealAI[tc.NOpp];
    res.thresholds = new int[tc.NOpp];
    for (int i = 1; i <= tc.NOpp; ++i) {
      realAIs[i - 1] = new RealAI(Long.parseLong(seed + "" + i), i);
      res.thresholds[i - 1] = realAIs[i - 1].attackT;
    }

    int[] baseLoc = new int[tc.B * 2];
    for (int i = 0; i < tc.B; ++i) {
      baseLoc[2 * i] = tc.bases[i].x;
      baseLoc[2 * i + 1] = tc.bases[i].y;
    }
    testStartTime[threadId] = System.currentTimeMillis();
    callInit(0, baseLoc, tc.speed);
    for (int i = 1; i <= tc.NOpp; ++i) {
      callInit(i, baseLoc, tc.speed);
    }

    World world = new World(tc);
    for (int step = 0; step < TestCase.SIMULATION_TIME; step++) {
      world.startNewStep();
      world.updateTroopArrivals();
      if (vis) {
        Snapshot snapshot = new Snapshot();
        snapshot.bases = new Base[tc.B];
        for (int i = 0; i < tc.B; i++) {
          snapshot.bases[i] = tc.bases[i].clone();
        }
        snapshot.troops = new Troop[world.troops.size()];
        for (int i = 0; i < snapshot.troops.length; i++) {
          snapshot.troops[i] = world.troops.get(i).clone();
        }
        snapshots.add(snapshot);
      }

      // let players do their turns ("simultaneously", so that neither has more information than the other)
      int[] basesArg = new int[tc.B * 2];
      for (int i = 0; i < tc.B; i++) {
        basesArg[2 * i] = tc.bases[i].owner;
        basesArg[2 * i + 1] = tc.bases[i].size;
      }

      int[] troopsArg = new int[world.troops.size() * 4];
      int it = 0;
      for (Troop t : world.troops) {
        troopsArg[it++] = t.owner;
        troopsArg[it++] = t.size;
        troopsArg[it++] = t.x;
        troopsArg[it++] = t.y;
      }
//      for (int i = 0; i < world.troops.size(); i++) {
//        Troop expect = world.troops.get(i);
//        System.err.printf("[%1d %3d %2d %2d %4d] ", expect.owner, expect.size, expect.sourceId, expect.targetId, expect.arrivalTime);
//      }
//      System.err.println();

      String errPrefix = "ERROR: time step = " + step + ". ";
      for (int owner = 0; owner <= tc.NOpp; owner++) {
        int[] attacks;
        attacks = callSendTroops(owner, basesArg, troopsArg);
        // validate size of user's return
        int na = attacks.length;
        if (na % 2 > 0) {
          throw new RuntimeException(errPrefix + "Return from sendTroops must have even length.");
        }
        na /= 2;
        if (na > tc.B) {
          throw new RuntimeException(errPrefix + "You can send at most B troops on each step.");
        }
        world.updateTroopDepartures(owner, attacks);
      }
//      for (int i = 0; i < solution.troops.size(); i++) {
//        AbstractWars.Troop actual = solution.troops.get(i);
//        System.err.printf("[%1d %3d %2d %2d %4d] ", actual.owner, actual.pop, actual.from, actual.target, actual.arrivalTime);
//      }
//      System.err.println();

      world.updateScore();
      if (vis) {
        snapshots.get(step).playerUnits = world.playersUnits.clone();
        snapshots.get(step).score = world.playerScore;
      }

      if (world.simComplete) {
        res.earlyExit = step * (world.playersUnits[0] == 0 ? -1 : 1);
        world.playerScore += (TestCase.SIMULATION_TIME - step - 1) * world.playersUnits[0] * 1.0 / world.totalUnits;
        break;
      }
    }
//    System.err.println("powers:");
//    for (int i = 1; i <= tc.NOpp; i++) {
//      System.err.printf("%.5f %.5f %.5f\n", tc.powers[i], solution.players[i].powerMin, solution.players[i].powerMax);
//    }
//    System.err.println("attackT");
//    for (int i = 1; i <= tc.NOpp; i++) {
//      System.err.printf("%d %d %d\n", realAIs[i - 1].attackT, solution.players[i].attackMin, solution.players[i].attackMax);
//    }
//    System.err.println("locality");
//   for (int i = 1; i <= tc.NOpp; i++) {
//     System.err.printf("%.5f\n", realAIs[i - 1].locality);
//     for (int j = 0; j < solution.players[i].locality.length; ++j) {
//       System.err.printf("%.5f %.5f\n", 1.0 + 2.0 * j / (solution.players[i].locality.length - 1), solution.players[i].locality[j]);
//     }
//   }
    res.elapsed = System.currentTimeMillis() - testStartTime[threadId];
    res.score = world.playerScore;
    res.perfect = world.perfect;
    if (vis) {
      new Visualizer(tc, snapshots);
    }
    return res;
  }

  private static final int THREAD_COUNT = 35;
  static long[] testStartTime = new long[THREAD_COUNT];

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-vis")) vis = true;
    }

    if (begin != -1 && end != -1) {
      vis = false;
      ArrayList<Long> seeds = new ArrayList<>();
      for (long i = begin; i <= end; ++i) {
        seeds.add(i);
      }
      int len = seeds.size();
      Result[] results = new Result[len];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      int prev = 0;
      for (int i = 0; i < THREAD_COUNT; ++i) {
        int next = len * (i + 1) / THREAD_COUNT;
        threads[i] = new TestThread(i, prev, next - 1, seeds, results);
        prev = next;
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / results.length));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed, 0);
      System.out.println(res);
    }
  }


  private static class TestThread extends Thread {
    int threadId;
    int begin, end;
    ArrayList<Long> seeds;
    Result[] results;

    TestThread(int threadId, int begin, int end, ArrayList<Long> seeds, Result[] results) {
      this.threadId = threadId;
      this.begin = begin;
      this.end = end;
      this.seeds = seeds;
      this.results = results;
    }

    public void run() {
      for (int i = begin; i <= end; ++i) {
        try {
          Tester f = new Tester();
          Result res = f.runTest(seeds.get(i), this.threadId);
          results[i] = res;
        } catch (Exception e) {
          e.printStackTrace();
          results[i] = new Result();
          results[i].seed = seeds.get(i);
        }
      }
    }
  }

  private static class Result {
    long seed;
    int B, O, S;
    double[] powers;
    int[] thresholds;
    double score;
    long elapsed;
    int earlyExit = Integer.MAX_VALUE;
    boolean perfect;

    public String toString() {
      String ret = String.format("seed:%4d\n", seed);
      ret += String.format("B:%2d O:%1d S:%1d\n", B, O, S);
      if (powers != null && thresholds != null) {
        ret += "powers:";
        for (int i = 1; i < powers.length; ++i) {
          ret += String.format("%.4f", powers[i]) + (i == powers.length - 1 ? "\n" : " ");
        }
        ret += "attackT:";
        for (int i = 0; i < thresholds.length; ++i) {
          ret += String.format("%3d", thresholds[i]) + (i == thresholds.length - 1 ? "\n" : " ");
        }
      }
      if (earlyExit > 0 && earlyExit != Integer.MAX_VALUE) {
        ret += " win at turn " + earlyExit + (perfect ? " (perfect)" : "") + "\n";
      } else if (earlyExit <= 0) {
        ret += "lose at turn " + -earlyExit + "\n";
      }
      ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
      ret += String.format("score:%.7f", score);
      return ret;
    }
  }
}

class Server {
  private static final int TIMELIMIT = 10000;

  static int getRemainingTime(int threadId) {
    return TIMELIMIT - (int) (System.currentTimeMillis() - Tester.testStartTime[threadId]);
  }
}

