from bayes_opt import BayesianOptimization
import json
import re
import subprocess


RE_SCORE = re.compile(r'score:(\d+) optimal:(\d+)')


def execute(**kwargs):
    opt = ' '.join(f'-{k.upper()} {v}' for k, v in kwargs.items())
    with open('result.txt', 'w') as f:
        subprocess.run(f'java Tester -b 20000 -e 49999 {opt}'.split(' '),
                       stdout=f)
    with open('result.txt', 'r') as f:
        log = f.read()
        raw_scores = [float(match[1]) / float(match[2]) for match in re.finditer(RE_SCORE, log)]
    return sum(s for s in raw_scores) / len(raw_scores)


param_bounds = {'mi': (10, 200),
                'tu': (30, 50),
                'neg': (0.2, 1),
                'pow': (0.5, 1.0)}
optimizer = BayesianOptimization(f=execute, pbounds=param_bounds)
optimizer.maximize(init_points=100, n_iter=200, acq='ucb')

print(json.dumps(optimizer.res['max'], indent=2))
print(json.dumps(optimizer.res['all'], indent=2))
