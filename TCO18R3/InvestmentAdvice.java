import java.util.Arrays;

public class InvestmentAdvice {

  int MIN_INVEST = 100;
  int FINISH_TURN = 40;
  double NEG_RATIO = 0.5;
  double PROB_POW = 0.9;
  private static final int MAX_INVEST = 400_000;
  private static final double INV_SQRT_2PI = 1.0 / Math.sqrt(2 * Math.PI);
  private int[][] advice, invest, result;
  double[][] expect;
  Investor[] is;
  private int money, timeLeft, roundsLeft, turn;
  private int N;

  public int[] getInvestments(int[] advice, int[] recent, int money, int timeLeft, int roundsLeft) {
    if (turn == 0) {
      this.N = advice.length;
      this.advice = new int[roundsLeft][N];
      this.result = new int[roundsLeft][N];
      this.invest = new int[roundsLeft][N];
      this.expect = new double[roundsLeft][N];
      this.is = new Investor[N];
      for (int i = 0; i < N; i++) {
        is[i] = new Investor(i);
        is[i].probs = new double[20][20]; // p[x][y] := stdev=0.01*x, accuracy=0.05*y
        for (int j = 0; j < is[i].probs.length; j++) {
          Arrays.fill(is[i].probs[j], 1.0 / (is[i].probs.length * is[i].probs[0].length));
        }
      }
    }
    System.arraycopy(advice, 0, this.advice[turn], 0, N);
    if (turn > 0) {
      for (int i = 0; i < N; i++) {
        result[turn - 1][i] = recent[i];
      }
    }
    if (turn > 0 && turn % 20 == 0) {
      MIN_INVEST /= 2;
    }
    if (turn == FINISH_TURN || roundsLeft == 1) {
      MIN_INVEST = 0;
    }
    this.money = money;
    this.timeLeft = timeLeft;
    this.roundsLeft = roundsLeft;
    solve();
    return invest[turn++];
  }

  private void solve() {
    for (int i = 0; i < N; i++) {
      if (turn > 0 && invest[turn - 1][i] > 0) updateProbs(is[i]);
      is[i].expect = expect[turn][i] = calcExpect(is[i]);
    }
    Investor[] investors = is.clone();
    Arrays.sort(investors, (Investor i1, Investor i2) -> Double.compare(i2.expect, i1.expect));
    int moneyLeft = money;
    for (int i = 0; i < N; i++) {
      if (advice[turn][i] > 0) {
        invest[turn][i] = MIN_INVEST;
      } else {
        invest[turn][i] = (int)(MIN_INVEST * NEG_RATIO);
      }
      moneyLeft -= invest[turn][i];
    }
    for (int i = 0; i < N; i++) {
      if (investors[i].expect >= 0) {
        int add = Math.min(MAX_INVEST - invest[turn][investors[i].i], moneyLeft);
        invest[turn][investors[i].i] += add;
        moneyLeft -= add;
      }
    }
  }

  private void updateProbs(Investor person) {
    double[][] p = person.probs;
    final double ad = advice[turn - 1][person.i] * 0.01;
    final double adG = gaussian(ad, 0.1);
    double sum = 0;
    final double stdevStep = 0.2 / p.length;
    final double accStep = 1.0 / p[0].length;
    final double actual = (result[turn - 1][person.i] + 0.5) / invest[turn - 1][person.i];
    for (int i = 0; i < p.length; i++) {
      final double stdev = (i + 0.5) * stdevStep;
      final double relG = gaussian(ad - actual, stdev);
      for (int j = 0; j < p[0].length; j++) {
        final double acc = (j + 0.5) * accStep;
        double p1 = acc * relG; // accurate
        double p2 = (1.0 - acc) * adG; // inaccurate
//        System.err.println(i + " " + j + " " + p1 + " " + p2 + " " + p[i][j]);
        p[i][j] *= Math.pow(p1 + p2, PROB_POW);
        sum += p[i][j];
      }
    }
    for (int i = 0; i < p.length; i++) {
      for (int j = 0; j < p[0].length; j++) {
        p[i][j] /= sum;
      }
    }
  }

  private double calcExpect(Investor person) {
    double ret = 0;
    double[][] p = person.probs;
    final double ad = advice[turn][person.i] * 0.01;
    final double adG = gaussian(ad, 0.1);
    final double stdevStep = 0.2 / p.length;
    final double accStep = 1.0 / p[0].length;
    for (int i = 0; i < p.length; i++) {
      final double stdev = (i + 0.5) * stdevStep;
      final double a = -(100 * stdev * stdev + 1) / (2 * stdev * stdev);
      final double b = ad / (2 * stdev * stdev);
      final double c = -ad * b;
      final double expR = Math.exp(c - b * b / a);
      final double v1 = expR * Math.sqrt(1.0 / -(a * Math.PI)) / (2 * 0.1 * stdev);
      for (int j = 0; j < p[0].length; j++) {
        final double acc = (j + 0.5) * accStep;
        double p1 = acc * v1;
        double p2 = (1 - acc) * adG;
        double csum = acc * b / a * v1;
        ret += -p[i][j] * csum / (p1 + p2);
      }
    }
    return ret;
  }

  private static double gaussian(double v, double stdev) {
    return INV_SQRT_2PI / stdev * Math.exp(-v * v * 0.5 / (stdev * stdev));
  }

  static class Investor {
    int i;
    double expect;
    double[][] probs;

    Investor(int i) {
      this.i = i;
    }

    double accExpect() {
      double ret = 0;
      final double accStep = 1.0 / probs[0].length;
      for (int i = 0; i < probs.length; i++) {
        for (int j = 0; j < probs[0].length; j++) {
          final double acc = (j + 0.5) * accStep;
          ret += probs[i][j] * acc;
        }
      }
      return ret;
    }
  }

}