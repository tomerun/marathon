import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Tester {

  private static final int TIME_LIMIT = 10000;
  private static final int INITIAL_MONEY = 1_000_000;
  private static DebugOption debug = DebugOption.None;
  private Random r;
  private int numExperts;
  private double[] accuracy;
  private double[] stDev;
  private int numPeriods;
  private double[][] actual;
  private int[][] reported;
  private int[][] result;
  private int[][] invest;
  private boolean[][] correct;
  private int money = INITIAL_MONEY;
  private int[] moneyHist;
  private int timeLeft = TIME_LIMIT;
  private static int myN = -1, myL = -1;

  private enum DebugOption {
    None, Console, Csv;
  }

  private void generateTestCase(long seed) {
    r = new Random(seed);
    numExperts = 10 + r.nextInt(41);
    if (myN != -1) numExperts = myN;
    stDev = new double[numExperts];
    accuracy = new double[numExperts];
    for (int i = 0; i < numExperts; i++) {
      stDev[i] = r.nextDouble() * 20;
      accuracy[i] = r.nextDouble();
    }
    numPeriods = 10 + r.nextInt(91);
    if (1000 <= seed && seed < 2000) {
      numPeriods = 10 + (int) (seed - 1000) * 91 / 1000;
    }
    if (myL != -1) numPeriods = myL;
    result = new int[numPeriods + 1][numExperts];
    actual = new double[numPeriods][numExperts];
    reported = new int[numPeriods][numExperts];
    invest = new int[numPeriods][numExperts];
    correct = new boolean[numPeriods][numExperts];
    moneyHist = new int[numPeriods + 1];
    moneyHist[0] = INITIAL_MONEY;
  }

  private void generateNextDay(int day) {
    for (int i = 0; i < numExperts; i++) {
      actual[day][i] = Math.min(1, Math.max(-1, r.nextGaussian() * 0.1));
      if (r.nextDouble() < accuracy[i]) {
        reported[day][i] = (int) Math.round(Math.min(100,
            Math.max(-100, r.nextGaussian() * stDev[i] + actual[day][i] * 100)));
        correct[day][i] = true;
      } else {
        reported[day][i] = (int) Math.round(100 * Math.min(1, Math.max(-1, r.nextGaussian() * 0.1)));
      }
    }
  }

  private void applyResults(int day) {
    for (int i = 0; i < numExperts; i++) {
      result[day + 1][i] = (int) Math.floor(invest[day][i] * actual[day][i]);
      money += result[day + 1][i];
    }
    moneyHist[day + 1] = money;
  }

  private void validateOutput(int[] invest) {
    if (invest.length != numExperts) throw new RuntimeException("Return was the wrong length.");
    int total = 0;
    for (int i = 0; i < invest.length; i++) {
      total += invest[i];
      if (invest[i] < 0) throw new RuntimeException("Investing a negative amount is not allowed.");
      if (invest[i] > 400000)
        throw new RuntimeException("Investing more than 400,000 with a single expert is not allowed.");
    }
    if (total > money) throw new RuntimeException("Attempted to invest more money than currently held.");
  }

  private int calcOptimal() {
    int m = INITIAL_MONEY;
    for (int i = 0; i < numPeriods; i++) {
      ArrayList<Integer> investors = new ArrayList<>();
      for (int j = 0; j < numExperts; j++) {
        if (correct[i][j] && actual[i][j] > 0) {
          investors.add(((int) (actual[i][j] * 10000) << 16) | j);
        }
      }
      Collections.sort(investors);
      int add = 0;
      for (int j = investors.size() - 1; j >= 0; j--) {
        int ii = investors.get(j) & 0xFFFF;
        double a = actual[i][ii];
        int invest = Math.min(m, 400000);
        m -= invest;
        add += (int) Math.floor(invest * (1.0 + a));
      }
      m += add;
    }
    return m;
  }

  private static final int THREAD_COUNT = 35;

  private Result runTest(long seed) {
    generateTestCase(seed);
    Result res = new Result();
    res.seed = seed;
    res.N = numExperts;
    res.L = numPeriods;
    InvestmentAdvice obj = new InvestmentAdvice();
    if (mi > 0 && tu > 0 && neg > 0 && pow > 0) {
      obj.MIN_INVEST = (int) mi;
      obj.FINISH_TURN = (int) tu;
      obj.NEG_RATIO = neg;
      obj.PROB_POW = pow;
    }
    try {
      for (int i = 0; i < numPeriods; i++) {
        generateNextDay(i);
        long start = System.currentTimeMillis();
        int[] rcv = obj.getInvestments(reported[i], result[i], money, timeLeft, numPeriods - i);
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        timeLeft -= elapsed;
        if (timeLeft < 10) {
          throw new RuntimeException("Time limit exceeded.");
        }
        validateOutput(rcv);
        System.arraycopy(rcv, 0, invest[i], 0, numExperts);
        applyResults(i);
      }
      res.score = money;
      res.optimal = calcOptimal();
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (debug == DebugOption.Console) {
      for (int i = 0; i < numPeriods; i++) {
        System.err.printf("Period %d: %d(%+d)\n", i, moneyHist[i + 1], (moneyHist[i + 1] - moneyHist[i]));
        for (int j = 0; j < numExperts; j++) {
          System.err.printf("%2d advice:%+3d actual:%7.3f expect:%7.3f invest:%6d -> %+6d\n",
              j, reported[i][j], actual[i][j] * 100, obj.expect[i][j] * 100, invest[i][j], result[i + 1][j]);
        }
        System.err.println();
      }
      for (int i = 0; i < numExperts; i++) {
        System.err.printf("%2d acc:%5.3f stdev:%5.3f\n", i, accuracy[i], stDev[i]);
        for (int j = 0; j < obj.is[i].probs.length; j++) {
          double sum = 0;
          for (int k = 0; k < obj.is[i].probs[0].length; k++) {
            sum += obj.is[i].probs[j][k];
            System.err.printf("%.4f ", obj.is[i].probs[j][k]);
          }
          System.err.printf(" | %.4f\n", sum);
        }
        System.err.println("------------------");
        for (int j = 0; j < obj.is[i].probs[0].length; j++) {
          double sum = 0;
          for (int k = 0; k < obj.is[i].probs.length; k++) {
            sum += obj.is[i].probs[k][j];
          }
          System.err.printf("%.4f ", sum);
        }
        System.err.println();
      }
    } else if (debug == DebugOption.Csv) {
      System.err.print("accuracy,stdev");
      for (int i = 0; i < numPeriods; i++) {
        System.err.print("," + i);
      }
      System.err.println();
      for (int i = 0; i < numExperts; i++) {
        System.err.printf("%.3f,%.3f", accuracy[i], stDev[i]);
        for (int j = 0; j < numPeriods; j++) {
          System.err.printf(",%d", reported[j][i]);
        }
        System.err.println();
        System.err.print(",");
        for (int j = 0; j < numPeriods; j++) {
          System.err.printf(",%.3f", actual[j][i] * 100);
        }
        System.err.println();
        System.err.print(",");
        for (int j = 0; j < numPeriods; j++) {
          System.err.printf(",%s", correct[j][i] ? "1" : "");
        }
        System.err.println();
      }
    }
    res.elapsed = TIME_LIMIT - timeLeft;
    return res;
  }

  static double mi, tu, neg, pow;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-N")) myN = Integer.parseInt(args[++i]);
      if (args[i].equals("-L")) myL = Integer.parseInt(args[++i]);
      if (args[i].equals("-debug")) debug = DebugOption.Console;
      if (args[i].equals("-csv")) debug = DebugOption.Csv;
      if (args[i].equals("-MI")) mi = Double.parseDouble(args[++i]);
      if (args[i].equals("-TU")) tu = Double.parseDouble(args[++i]);
      if (args[i].equals("-NEG")) neg = Double.parseDouble(args[++i]);
      if (args[i].equals("-POW")) pow = Double.parseDouble(args[++i]);
    }

    if (begin != -1 && end != -1) {
      BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
      for (long i = begin; i <= end; ++i) {
        q.add(i);
      }
      Result[] results = new Result[(int) (end - begin + 1)];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i] = new TestThread(q, results, begin);
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      double sum_rel = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
        sum_rel += result.score * 1.0 / result.optimal;
      }
      System.out.println("ave:" + (sum / (end - begin + 1)));
      System.out.println("ave_rel:" + (sum_rel / (end - begin + 1)));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    long seedMin;
    Result[] results;
    BlockingQueue<Long> q;

    TestThread(BlockingQueue<Long> q, Result[] results, long seedMin) {
      this.q = q;
      this.results = results;
      this.seedMin = seedMin;
    }

    public void run() {
      while (true) {
        Long seed = q.poll();
        if (seed == null) break;
        try {
          Tester f = new Tester();
          Result res = f.runTest(seed);
          results[(int) (seed - seedMin)] = res;
        } catch (Exception e) {
          e.printStackTrace();
          Result res = new Result();
          res.seed = seed;
          results[(int) (seed - seedMin)] = res;
        }
      }
    }
  }
}

class Result {
  long seed;
  int N, L;
  int score, optimal;
  long elapsed;

  public String toString() {
    String ret = String.format("seed:%4d\n", seed);
    ret += String.format("N:%3d L:%2d\n", N, L);
    ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
    ret += String.format("score:%d optimal:%d", score, optimal);
    return ret;
  }
}
