#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:N, :L, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /N: *(\d+) L: *(\d+)/
      testcase.N = $1.to_i
      testcase.L = $2.to_i
    elsif line =~ /^score:(\d+) optimal:(\d+)/
      testcase.score = $1.to_f / $2.to_f
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    count += 1
    sum_score_diff += s2 - s1
    if s1 < s2
      win += 1
    elsif s1 > s2
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff * 1000000.0 / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  10.step(50, 7) do |param|
    upper = param + 7
    puts "N:#{param}-#{upper}"
    calc(from, to) { |from, to| from.N.between?(param, upper) }
  end
  puts "---------------------"
  10.step(100, 13) do |param|
    upper = param + 13
    puts "L:#{param}-#{upper}"
    calc(from, to) { |from, to| from.L.between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
