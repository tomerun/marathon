#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

def get_scores(filename)
  scores = []
  seed = 0;
  IO.foreach(filename){ |line|
    index = line.index("seed")
    if index == 0
      seed = line[(index+5)..-1].to_i
    end
    if line =~ /^score:(\d+)/
      scores[seed] = $1.to_i
    end
  }
  return scores
end

from = get_scores(ARGV[0])
to = get_scores(ARGV[1])
seed_begin = ARGV[2] ? ARGV[2].to_i : nil
seed_end = ARGV[3] ? ARGV[3].to_i : nil

sum = 0
sum_score_diff = 0
count = 0
win = 0
lose = 0
width = [from.length, to.length].min.to_s.length

(seed_begin || 1).upto(seed_end || from.length - 1) do |seed|
  next if !from[seed] || !to[seed]
  count += 1;
  diff = (to[seed] - from[seed]).to_f / from[seed]
  puts sprintf("%#{width}d % .4f", seed, diff)
  sum_score_diff += diff
  if diff > 0
    win += 1
  elsif diff < 0
    lose += 1
  end
end

puts
puts "ave: #{(1.0 * sum_score_diff / count)}"
puts "win: #{win}"
puts "lose:#{lose}"
puts "tie: #{count - win - lose}"
