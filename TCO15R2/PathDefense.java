import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;

public class PathDefense {

	private static final boolean DEBUG = 1 == 0;
	private static final boolean MEASURE_TIME = 1 == 0;
	private static final int[] DR = { 1, 0, -1, 0 };
	private static final int[] DC = { 0, -1, 0, 1 };
	private static final int DOWN = (1 << 0);
	private static final int LEFT = (1 << 1);
	private static final int UP = (1 << 2);
	private static final int RIGHT = (1 << 3);
	private static final double PATH_DAMAGE_RATIO_BASE = 0.1;
	int NUM_PATH = 200;
	XorShift rnd = new XorShift();
	Timer timer = new Timer();
	int[][] nextDir = new int[16][12];
	int creepHealth, creepMoney;
	int[] entR, entC;
	Base[] bases;
	TowerType[] towerTypes;
	ArrayList<Tower> towers = new ArrayList<>();
	int purchaseTh;
	ArrayList<PlaceCand> placeCands = new ArrayList<>();
	int minPrice = 999;
	Creep[] creeps = new Creep[2000];
	ArrayList<Creep> liveCreeps = new ArrayList<>();
	char[][] board;
	int[][] dir;
	int[][][] dirRecord;
	BitSet[][] pathPos;
	int[] pathDamages;
	double[] pathDamagesRatio;
	boolean needPurchase = true;
	int N, estimatedZ, appear, maxId;
	int turn = 0;
	boolean retire = false;

	int init(String[] board, int money, int creepHealth, int creepMoney, int[] tt) {
		timer.start(0);
		this.N = board.length;
		this.board = new char[N][];
		for (int i = 0; i < N; ++i) {
			this.board[i] = board[i].toCharArray();
		}
		this.dir = new int[N][N];
		this.dirRecord = new int[N][N][4];
		this.pathPos = new BitSet[N][N];
		this.creepHealth = creepHealth;
		this.creepMoney = creepMoney;
		initTowerTypes(tt);
		for (int i = 1; i < nextDir.length; ++i) {
			ArrayList<Integer> bits = new ArrayList<>();
			int v = i;
			while (v != 0) {
				bits.add(Integer.numberOfTrailingZeros(v));
				v -= Integer.lowestOneBit(v);
			}
			for (int j = 0; j < nextDir[i].length; ++j) {
				nextDir[i][j] = bits.get(j % bits.size());
			}
		}
		initBases();
		initPaths();
		initPlaceCands();
		timer.stop(0);
		return 0;
	}

	void initTowerTypes(int[] tt) {
		this.towerTypes = new TowerType[tt.length / 3];
		final int n = towerTypes.length;
		for (int i = 0; i < n; ++i) {
			this.towerTypes[i] = new TowerType(tt[3 * i], tt[3 * i + 1], tt[3 * i + 2], i);
			minPrice = Math.min(minPrice, this.towerTypes[i].cost);
		}
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				if (towerTypes[i].range <= towerTypes[j].range && towerTypes[i].damage <= towerTypes[j].damage
						&& towerTypes[i].cost >= towerTypes[j].cost) {
					if (towerTypes[i].range < towerTypes[j].range || towerTypes[i].damage < towerTypes[j].damage
							|| towerTypes[i].cost > towerTypes[j].cost) {
						towerTypes[i].ignore = true;
						break;
					} else {
						towerTypes[i].ignore = j < i;
					}
				}
			}
		}
		for (int i = 0; i < n; ++i) {
			debug(String.format("Tower%2d [range:%d damage:%d cost:%2d] %s", i, tt[3 * i], tt[3 * i + 1], tt[3 * i + 2],
					towerTypes[i].ignore ? "[IGNORE]" : ""));
		}
		TowerType[] tts = this.towerTypes.clone();
		Arrays.sort(tts, (o1, o2) -> {
			return Integer.compare(o1.damage * o1.range, o2.damage * o2.range);
		});
		this.purchaseTh = tts[n - 1].cost;
		debug("purchaceTh:" + purchaseTh);
	}

	void initPaths() {
		NUM_PATH = entR.length * 3;
		pathDamages = new int[NUM_PATH];
		pathDamagesRatio = new double[NUM_PATH];
		Arrays.fill(pathDamagesRatio, 1.0 / PATH_DAMAGE_RATIO_BASE);
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				if (board[i][j] == '.') pathPos[i][j] = new BitSet(NUM_PATH);
			}
		}
		for (int i = 0; i < NUM_PATH; ++i) {
			int r = entR[i % entR.length];
			int c = entC[i % entR.length];
			int prevDir = 0;
			boolean ok = false;
			while (!ok) {
				for (int j = 0;; ++j) {
					if (j == 2 * N * N) {
						for (int y = 0; y < N; ++y) {
							for (int x = 0; x < N; ++x) {
								if (pathPos[y][x] != null) pathPos[y][x].clear(i);
							}
						}
						break;
					}
					if (board[r][c] != '.') {
						ok = true;
						break;
					}
					pathPos[r][c].set(i);
					int cand = dir[r][c] & ~prevDir;
					int d = nextDir[cand][rnd.nextInt(12)];
					r += DR[d];
					c += DC[d];
				}
			}
		}
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				for (int k = 0; k < 4; ++k) {
					if ((dir[i][j] & (1 << k)) != 0) dirRecord[i][j][k] = 1;
				}
			}
		}
	}

	void initPlaceCands() {
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				if (board[i][j] != '#') continue;
				for (TowerType tt : towerTypes) {
					if (tt.ignore) continue;
					int cells = 0;
					PlaceCand pc = new PlaceCand(i, j, tt);
					pc.pathDamage = new int[NUM_PATH];
					pc.pathTarget = new BitSet(NUM_PATH);
					for (int r = Math.max(0, i - tt.range); r <= Math.min(N - 1, i + tt.range); ++r) {
						for (int c = Math.max(0, j - tt.range); c <= Math.min(N - 1, j + tt.range); ++c) {
							if (board[r][c] == '.' && dist2(r, c, i, j) <= tt.range * tt.range) {
								++cells;
								for (int pos = pathPos[r][c].nextSetBit(0); pos >= 0; pos = pathPos[r][c].nextSetBit(pos + 1)) {
									pc.pathDamage[pos] += tt.damage;
								}
								pc.pathTarget.or(pathPos[r][c]);
							}
						}
					}
					if (cells > 0) placeCands.add(pc);
				}
			}
		}
	}

	void initBases() {
		ArrayList<Base> list = new ArrayList<>();
		for (int i = 0; i < 8; ++i) {
			list.add(null);
		}
		int bc = 0;
		HashSet<Integer> entrances = new HashSet<Integer>();
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				if ('0' <= board[i][j] && board[i][j] <= '9') {
					list.set(board[i][j] - '0', initBase(i, j, entrances));
					++bc;
				}
			}
		}
		bases = new Base[bc];
		for (int i = 0; i < bc; ++i) {
			bases[i] = list.get(i);
		}
		entR = new int[entrances.size()];
		entC = new int[entrances.size()];
		int i = 0;
		for (int e : entrances) {
			entR[i] = e >> 8;
			entC[i] = e & 0xFF;
			++i;
		}
		debug(Arrays.toString(bases));
	}

	Base initBase(int sr, int sc, HashSet<Integer> entrances) {
		Base base = new Base(sr, sc);
		int[][] escape = new int[N][N];
		ArrayDeque<Integer> q = new ArrayDeque<Integer>();
		q.add((sr << 8) + sc);
		escape[sr][sc] = 0xFF;
		ArrayList<Integer> entrance = new ArrayList<>();
		while (!q.isEmpty()) {
			int cur = q.poll();
			int cr = cur >> 8;
			int cc = cur & 0xFF;
			if (cr == 0 || cr == N - 1 || cc == 0 || cc == N - 1) {
				entrance.add(cur);
				continue;
			}
			// up
			if (cr <= sr && board[cr - 1][cc] == '.') {
				if (escape[cr - 1][cc] == 0) {
					q.add(((cr - 1) << 8) + cc);
				}
				escape[cr - 1][cc] |= DOWN;
			}
			// left
			if (cc <= sc && board[cr][cc - 1] == '.') {
				if (escape[cr][cc - 1] == 0) {
					q.add(((cr) << 8) + cc - 1);
				}
				escape[cr][cc - 1] |= RIGHT;
			}
			// down
			if (cr >= sr && board[cr + 1][cc] == '.') {
				if (escape[cr + 1][cc] == 0) {
					q.add(((cr + 1) << 8) + cc);
				}
				escape[cr + 1][cc] |= UP;
			}
			// right
			if (cc >= sc && board[cr][cc + 1] == '.') {
				if (escape[cr][cc + 1] == 0) {
					q.add(((cr) << 8) + cc + 1);
				}
				escape[cr][cc + 1] |= LEFT;
			}
		}
		entrances.addAll(entrance);
		boolean[][] visited = new boolean[N][N];
		for (int ent : entrance) {
			q.clear();
			q.add(ent);
			visited[ent >> 8][ent & 0xFF] = true;
			while (!q.isEmpty()) {
				int cur = q.poll();
				int cr = cur >> 8;
				int cc = cur & 0xFF;
				if (cr < sr && (escape[cr][cc] & DOWN) != 0) {
					dir[cr][cc] |= DOWN;
					if (!visited[cr + 1][cc]) {
						q.add(((cr + 1) << 8) + cc);
						visited[cr + 1][cc] = true;
					}
				}
				if (cr > sr && (escape[cr][cc] & UP) != 0) {
					dir[cr][cc] |= UP;
					if (!visited[cr - 1][cc]) {
						q.add(((cr - 1) << 8) + cc);
						visited[cr - 1][cc] = true;
					}
				}
				if (cc < sc && (escape[cr][cc] & RIGHT) != 0) {
					dir[cr][cc] |= RIGHT;
					if (!visited[cr][cc + 1]) {
						q.add((cr << 8) + cc + 1);
						visited[cr][cc + 1] = true;
					}
				}
				if (cc > sc && (escape[cr][cc] & LEFT) != 0) {
					dir[cr][cc] |= LEFT;
					if (!visited[cr][cc - 1]) {
						q.add((cr << 8) + cc - 1);
						visited[cr][cc - 1] = true;
					}
				}
			}
		}
		base.ignore = entrance.isEmpty();
		return base;
	}

	int[] placeTowers(int[] creepInfo, int money, int[] baseHealth) {
		++turn;
		for (int i = 0; i < bases.length; ++i) {
			bases[i].life = baseHealth[i];
		}
		updateCreeps(creepInfo);

		ArrayList<PlaceCand> placeInfo = new ArrayList<>();

		while (!retire && money >= purchaseTh && !placeCands.isEmpty() && needPurchase) {
			PlaceCand bestCand = null;
			double bestV = 0;
			timer.start(1);
			for (PlaceCand cand : placeCands) {
				if (cand.type.cost > money) continue;
				double v = 0;
				for (int i = cand.pathTarget.nextSetBit(0); i >= 0; i = cand.pathTarget.nextSetBit(i + 1)) {
					v += cand.pathDamage[i] * pathDamagesRatio[i];
				}
				v /= cand.type.cost;
				if (v > bestV) {
					bestV = v;
					bestCand = cand;
				}
			}
			timer.stop(1);
			if (bestCand == null || bestCand.type.cost > money) break;
			timer.start(2);
			placeInfo.add(bestCand);
			money -= bestCand.type.cost;
			board[bestCand.r][bestCand.c] = (char) ('A' + bestCand.type.idx);
			needPurchase = false;
			for (int i = 0; i < NUM_PATH; ++i) {
				pathDamages[i] += bestCand.pathDamage[i];
				if (pathDamages[i] > creepHealth * 8 * 8) {
					pathDamagesRatio[i] = 0;
				} else {
					pathDamagesRatio[i] = 1.0 / (pathDamages[i] + PATH_DAMAGE_RATIO_BASE);
					needPurchase = true;
				}
			}
			for (int i = placeCands.size() - 1; i >= 0; --i) {
				if (placeCands.get(i).r == bestCand.r && placeCands.get(i).c == bestCand.c) {
					placeCands.remove(i);
				}
			}
			timer.stop(2);
		}
		if (!retire) {
			int maxDamage = 0;
			for (int i = 0; i < NUM_PATH; ++i) {
				maxDamage = Math.max(maxDamage, pathDamages[i]);
			}
			if (maxDamage < creepHealth * (1 << (turn / 500))) {
				retire = true;
				debug("retire at turn " + turn + " maxDamage:" + maxDamage);
				placeInfo.clear();
			}
		}

		int[] ret = new int[placeInfo.size() * 3];
		for (int i = 0; i < placeInfo.size(); ++i) {
			ret[3 * i] = placeInfo.get(i).c;
			ret[3 * i + 1] = placeInfo.get(i).r;
			ret[3 * i + 2] = placeInfo.get(i).type.idx;
		}

		if (turn == 2000) {
			timer.print();
		}

		return ret;
	}

	void updateCreeps(int[] creepInfo) {
		liveCreeps.clear();
		int C = creepInfo.length / 4;
		liveCreeps.ensureCapacity(C);
		for (int i = 0; i < C; ++i) {
			int id = creepInfo[4 * i];
			int hp = creepInfo[4 * i + 1];
			int col = creepInfo[4 * i + 2];
			int row = creepInfo[4 * i + 3];
			if (creeps[id] == null) {
				creeps[id] = new Creep(row, col, id);
				++appear;
				maxId = Math.max(maxId, id);
			} else {
				for (int j = 0; j < 4; ++j) {
					if (row == creeps[id].r + DR[j] && col == creeps[id].c + DC[j]) {
						creeps[id].prevDir = j;
						dirRecord[creeps[id].r][creeps[id].c][j]++;
					}
				}
				creeps[id].r = row;
				creeps[id].c = col;
				creeps[id].hp = hp;
			}
			liveCreeps.add(creeps[id]);
		}
	}

	static int dist2(int r1, int c1, int r2, int c2) {
		return (r2 - r1) * (r2 - r1) + (c2 - c1) * (c2 - c1);
	}

	class Creep {
		int r, c, id, hp;
		int prevDir;

		Creep(int r, int c, int id) {
			this.r = r;
			this.c = c;
			this.id = id;
			this.hp = creepHealth;
		}

		public Creep clone() {
			Creep ret = new Creep(this.r, this.c, this.id);
			ret.hp = this.hp;
			ret.prevDir = this.prevDir;
			return ret;
		}
	}

	static class Base {
		int r, c, life = 1000;
		boolean ignore = false;

		Base(int r, int c) {
			this.r = r;
			this.c = c;
		}

		public String toString() {
			return String.format("[%d, %d] life=%4d%s", r, c, life, ignore ? " [IGNORE]" : "");
		}
	}

	static class TowerType {
		final int range, damage, cost, idx;
		boolean ignore;

		public TowerType(int range, int damage, int cost, int idx) {
			this.range = range;
			this.damage = damage;
			this.cost = cost;
			this.idx = idx;
		}
	}

	static class Tower {
		final int r, c;
		final TowerType type;

		public Tower(int r, int c, TowerType type) {
			this.r = r;
			this.c = c;
			this.type = type;
		}
	}

	static class PlaceCand {
		int r, c;
		int[] pathDamage;
		BitSet pathTarget;
		TowerType type;

		public PlaceCand(int r, int c, TowerType type) {
			this.r = r;
			this.c = c;
			this.type = type;
		}
	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;
		static final double TO_DOUBLE = 1.0 / (1L << 31);

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		boolean nextBoolean() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return (w & 8) == 0;
		}

		double nextDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return Math.abs(w * TO_DOUBLE);
		}

		double nextSDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w * TO_DOUBLE;
		}
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}
}
