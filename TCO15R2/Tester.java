import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Constants {
	public static final int SIMULATION_TIME = 2000;
	public static final int[] DY = new int[] { 1, 0, -1, 0 };
	public static final int[] DX = new int[] { 0, -1, 0, 1 };
	public static final int DOWN = 1;
	public static final int LEFT = 2;
	public static final int UP = 4;
	public static final int RIGHT = 8;
}

class CreepType {
	public static final int MIN_CREEP_HEALTH = 1;
	public static final int MAX_CREEP_HEALTH = 20;
	public static final int MIN_CREEP_MONEY = 1;
	public static final int MAX_CREEP_MONEY = 20;
	int health;
	int money;

	public CreepType(Random rnd) {
		health = rnd.nextInt(MAX_CREEP_HEALTH - MIN_CREEP_HEALTH + 1) + MIN_CREEP_HEALTH;
		money = rnd.nextInt(MAX_CREEP_MONEY - MIN_CREEP_MONEY + 1) + MIN_CREEP_MONEY;
	}
}

class Creep {
	int id;
	int health;
	int x, y;
	int spawnTime;
	ArrayList<Integer> moves = new ArrayList<Integer>();
}

class TowerType {
	public static final int MIN_TOWER_RANGE = 1;
	public static final int MAX_TOWER_RANGE = 5;
	public static final int MIN_TOWER_DAMAGE = 1;
	public static final int MAX_TOWER_DAMAGE = 5;
	public static final int MIN_TOWER_COST = 5;
	public static final int MAX_TOWER_COST = 40;
	int range;
	int damage;
	int cost;

	public TowerType(Random rnd) {
		range = rnd.nextInt(MAX_TOWER_RANGE - MIN_TOWER_RANGE + 1) + MIN_TOWER_RANGE;
		damage = rnd.nextInt(MAX_TOWER_DAMAGE - MIN_TOWER_DAMAGE + 1) + MIN_TOWER_DAMAGE;
		cost = rnd.nextInt(MAX_TOWER_COST - MIN_TOWER_COST + 1) + MIN_TOWER_COST;
	}

}

class Tower {
	int x, y, type;
}

class AttackVis {
	int x1, y1, x2, y2;

	public AttackVis(int _x1, int _y1, int _x2, int _y2) {
		x1 = _x1;
		y1 = _y1;
		x2 = _x2;
		y2 = _y2;
	}
}

class TestCase {
	public static final int MIN_CREEP_COUNT = 500;
	public static final int MAX_CREEP_COUNT = 2000;
	public static final int MIN_TOWER_TYPES = 1;
	public static final int MAX_TOWER_TYPES = 20;
	public static final int MIN_BASE_COUNT = 1;
	public static final int MAX_BASE_COUNT = 8;
	public static final int MIN_WAVE_COUNT = 1;
	public static final int MAX_WAVE_COUNT = 15;
	public static final int MIN_BOARD_SIZE = 20;
	public static final int MAX_BOARD_SIZE = 60;

	public int boardSize;
	public int money;
	public CreepType creepType;
	public int towerTypeCnt;
	public TowerType[] towerTypes;
	public char[][] board;
	public int pathCnt;
	public int[] spawnX;
	public int[] spawnY;
	public int[][] boardPath;
	public int tx, ty;
	public int baseCnt;
	public int[] baseX;
	public int[] baseY;
	public int creepCnt;
	public Creep[] creeps;
	public int waveCnt;

	public void connect(Random rnd, int x1, int y1, int x2, int y2) {
		while (x1 != x2 || y1 != y2) {
			if (board[y1][x1] >= '0' && board[y1][x1] <= '9') return;
			board[y1][x1] = '.';
			int x1_ = x1;
			int y1_ = y1;
			if (x1 == x2) {
				if (y2 > y1) {
					y1++;
				} else {
					y1--;
				}
			} else if (y1 == y2) {
				if (x2 > x1) {
					x1++;
				} else {
					x1--;
				}
			} else {
				int nx = x1;
				int ny = y1;
				if (x2 > x1)
					nx++;
				else
					nx--;
				if (y2 > y1)
					ny++;
				else
					ny--;
				if (board[ny][x1] == '.') {
					y1 = ny;
				} else if (board[y1][nx] == '.') {
					x1 = nx;
				} else {
					if (rnd.nextInt(2) == 0)
						y1 = ny;
					else
						x1 = nx;
				}
			}
			if (x1 > x1_)
				boardPath[y1_][x1_] |= 8;
			else if (x1 < x1_)
				boardPath[y1_][x1_] |= 2;
			else if (y1 > y1_)
				boardPath[y1_][x1_] |= 1;
			else if (y1 < y1_) boardPath[y1_][x1_] |= 4;
		}
	}

	public void addPath(Random rnd, int baseIdx) {
		int sx = 0, sy = 0;
		boolean nextTo = false;
		int tryEdge = 0;
		do {
			tryEdge++;
			if (tryEdge > boardSize) break;
			nextTo = false;
			sx = rnd.nextInt(boardSize - 1) + 1;
			if (rnd.nextInt(2) == 0) {
				sy = rnd.nextInt(2) * (boardSize - 1);
				if (sx > 0 && board[sy][sx - 1] == '.') nextTo = true;
				if (sx + 1 < boardSize && board[sy][sx + 1] == '.') nextTo = true;
			} else {
				sy = sx;
				sx = rnd.nextInt(2) * (boardSize - 1);
				if (sy > 0 && board[sy - 1][sx] == '.') nextTo = true;
				if (sy + 1 < boardSize && board[sy + 1][sx] == '.') nextTo = true;
			}
		} while (nextTo || board[sy][sx] != '#');
		if (tryEdge > boardSize) return;
		board[sy][sx] = '.';
		spawnX[baseIdx] = sx;
		spawnY[baseIdx] = sy;
		if (sx == 0) {
			boardPath[sy][sx] |= Constants.RIGHT;
			sx++;
		} else if (sy == 0) {
			boardPath[sy][sx] |= Constants.DOWN;
			sy++;
		} else if (sx == boardSize - 1) {
			boardPath[sy][sx] |= Constants.LEFT;
			sx--;
		} else {
			boardPath[sy][sx] |= Constants.UP;
			sy--;
		}
		int b = baseIdx % baseCnt;
		if (baseIdx >= baseCnt) b = rnd.nextInt(baseCnt);
		connect(rnd, sx, sy, baseX[b], baseY[b]);
	}

	public TestCase(long seed) {
		SecureRandom rnd = null;
		try {
			rnd = SecureRandom.getInstance("SHA1PRNG");
		} catch (Exception e) {
			System.err.println("ERROR: unable to generate test case.");
			System.exit(1);
		}
		rnd.setSeed(seed);
		boolean genDone = true;
		do {
			boardSize = rnd.nextInt(MAX_BOARD_SIZE - MIN_BOARD_SIZE + 1) + MIN_BOARD_SIZE;
			if (seed == 1) boardSize = 20;
			if (Tester.specifiedN != -1) boardSize = Tester.specifiedN;
			board = new char[boardSize][boardSize];
			boardPath = new int[boardSize][boardSize];
			creepType = new CreepType(rnd);
			if (1001 <= seed && seed <= 1800) {
				creepType.health = (int) ((seed - 1001) / 40 + 1);
				creepType.money = (int) ((seed - 1001) % 40 / 2 + 1);
			}

			towerTypeCnt = rnd.nextInt(MAX_TOWER_TYPES - MIN_TOWER_TYPES + 1) + MIN_TOWER_TYPES;
			towerTypes = new TowerType[towerTypeCnt];
			money = 0;
			for (int i = 0; i < towerTypeCnt; i++) {
				towerTypes[i] = new TowerType(rnd);
				money += towerTypes[i].cost;
			}
			baseCnt = rnd.nextInt(MAX_BASE_COUNT - MIN_BASE_COUNT + 1) + MIN_BASE_COUNT;
			if (Tester.specifiedB != -1) baseCnt = Tester.specifiedB;
			for (int y = 0; y < boardSize; y++) {
				for (int x = 0; x < boardSize; x++) {
					board[y][x] = '#';
					boardPath[y][x] = 0;
				}
			}
			baseX = new int[baseCnt];
			baseY = new int[baseCnt];
			for (int i = 0; i < baseCnt; i++) {
				int bx, by;
				do {
					bx = rnd.nextInt(boardSize - 8) + 4;
					by = rnd.nextInt(boardSize - 8) + 4;
				} while (board[by][bx] != '#');
				board[by][bx] = (char) ('0' + i);
				baseX[i] = bx;
				baseY[i] = by;
			}

			pathCnt = rnd.nextInt(baseCnt * 10 - baseCnt + 1) + baseCnt;
			if (Tester.specifiedP != -1) pathCnt = Tester.specifiedP;
			spawnX = new int[pathCnt];
			spawnY = new int[pathCnt];
			for (int i = 0; i < pathCnt; i++) {
				addPath(rnd, i);
			}

			creepCnt = rnd.nextInt(MAX_CREEP_COUNT - MIN_CREEP_COUNT + 1) + MIN_CREEP_COUNT;
			if (seed == 1) creepCnt = MIN_CREEP_COUNT;
			if (Tester.specifiedZ != -1) creepCnt = Tester.specifiedZ;
			creeps = new Creep[creepCnt];
			for (int i = 0; i < creepCnt; i++) {
				Creep c = new Creep();
				int j = rnd.nextInt(pathCnt);
				c.x = spawnX[j];
				c.y = spawnY[j];
				c.id = i;
				c.spawnTime = rnd.nextInt(Constants.SIMULATION_TIME);
				c.health = creepType.health * (1 << (c.spawnTime / 500));
				creeps[i] = c;
			}
			// waves
			waveCnt = rnd.nextInt(MAX_WAVE_COUNT - MIN_WAVE_COUNT + 1) + MIN_WAVE_COUNT;
			if (Tester.specifiedW != -1) waveCnt = Tester.specifiedW;
			int wi = 0;
			for (int w = 0; w < waveCnt; w++) {
				int wavePath = rnd.nextInt(pathCnt);
				int waveSize = 5 + rnd.nextInt(creepCnt / 20);
				int waveStartT = rnd.nextInt(Constants.SIMULATION_TIME - waveSize);
				for (int i = 0; i < waveSize; i++) {
					if (wi >= creepCnt) break;
					creeps[wi].x = spawnX[wavePath];
					creeps[wi].y = spawnY[wavePath];
					creeps[wi].spawnTime = waveStartT + rnd.nextInt(waveSize);
					creeps[wi].health = creepType.health * (1 << (creeps[wi].spawnTime / 500));
					wi++;
				}
				if (wi >= creepCnt) break;
			}

			// determine paths for each creep
			genDone = true;
			for (Creep c : creeps) {
				int x = c.x;
				int y = c.y;
				int prevx = -1;
				int prevy = -1;
				int tryPath = 0;
				while (!(board[y][x] >= '0' && board[y][x] <= '9')) {
					int dir = 0;
					tryPath++;
					if (tryPath > boardSize * boardSize) break;
					// select a random direction that will lead to a base, don't go back to where the creep was in the previous time step
					int tryDir = 0;
					do {
						if (tryDir == 15) {
							tryDir = -1;
							break;
						}
						dir = rnd.nextInt(4);
						tryDir |= (1 << dir);
					} while ((boardPath[y][x] & (1 << dir)) == 0
							|| (x + Constants.DX[dir] == prevx && y + Constants.DY[dir] == prevy));
					if (tryDir < 0) break;
					c.moves.add(dir);
					prevx = x;
					prevy = y;
					x += Constants.DX[dir];
					y += Constants.DY[dir];
				}
				if (!(board[y][x] >= '0' && board[y][x] <= '9')) {
					genDone = false;
					break;
				}
			}
		} while (!genDone);
	}
}

class Drawer extends JFrame {
	public static final int EXTRA_WIDTH = 200;
	public static final int EXTRA_HEIGHT = 100;

	public World world;
	public DrawerPanel panel;
	public int cellSize, boardSize;
	public int width, height;
	public boolean pauseMode = false;
	public boolean stepMode = false;
	public boolean debugMode = false;
	Font fontBoard;
	Font fontInfo;

	class DrawerKeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			synchronized (keyMutex) {
				if (e.getKeyChar() == ' ') {
					pauseMode = !pauseMode;
				}
				if (e.getKeyChar() == 'd') {
					debugMode = !debugMode;
				}
				if (e.getKeyChar() == 's') {
					stepMode = !stepMode;
				}
				keyPressed = true;
				keyMutex.notifyAll();
			}
		}
	}

	class DrawerPanel extends JPanel {
		public void paint(Graphics gorig) {
			Graphics2D g = (Graphics2D) gorig;
			g.setFont(fontBoard);
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			synchronized (world.worldLock) {
				int cCnt[][] = new int[boardSize][boardSize];
				g.setColor(new Color(0, 128, 0));
				g.fillRect(15, 15, cellSize * boardSize + 1, cellSize * boardSize + 1);
				g.setColor(Color.BLACK);
				for (int i = 0; i <= boardSize; i++) {
					g.drawLine(15 + i * cellSize, 15, 15 + i * cellSize, 15 + cellSize * boardSize);
					g.drawLine(15, 15 + i * cellSize, 15 + cellSize * boardSize, 15 + i * cellSize);
				}
				final int SIZE_ARROW = cellSize / 5;
				for (int i = 0; i < boardSize; i++) {
					for (int j = 0; j < boardSize; j++) {
						if (world.tc.board[i][j] == '.') {
							cCnt[i][j] = 0;
							g.setColor(new Color(32, 32, 32));
							g.fillRect(15 + j * cellSize + 1, 15 + i * cellSize + 1, cellSize - 1, cellSize - 1);
							g.setColor(Color.WHITE);
							int xc = 15 + j * cellSize + cellSize / 2;
							int yc = 15 + i * cellSize + cellSize / 2;
							int top = 15 + i * cellSize + 2;
							int bottom = 15 + (i + 1) * cellSize - 2;
							int left = 15 + j * cellSize + 2;
							int right = 15 + (j + 1) * cellSize - 2;
							if ((world.tc.boardPath[i][j] & Constants.DOWN) != 0) {
								g.drawLine(xc, top, xc, bottom);
								g.drawLine(xc, bottom, xc - SIZE_ARROW, bottom - SIZE_ARROW);
								g.drawLine(xc, bottom, xc + SIZE_ARROW, bottom - SIZE_ARROW);
							}
							if ((world.tc.boardPath[i][j] & Constants.UP) != 0) {
								g.drawLine(xc, top, xc, bottom);
								g.drawLine(xc, top, xc - SIZE_ARROW, top + SIZE_ARROW);
								g.drawLine(xc, top, xc + SIZE_ARROW, top + SIZE_ARROW);
							}
							if ((world.tc.boardPath[i][j] & Constants.LEFT) != 0) {
								g.drawLine(left, yc, right, yc);
								g.drawLine(left, yc, left + SIZE_ARROW, yc - SIZE_ARROW);
								g.drawLine(left, yc, left + SIZE_ARROW, yc + SIZE_ARROW);
							}
							if ((world.tc.boardPath[i][j] & Constants.RIGHT) != 0) {
								g.drawLine(left, yc, right, yc);
								g.drawLine(right, yc, right - SIZE_ARROW, yc - SIZE_ARROW);
								g.drawLine(right, yc, right - SIZE_ARROW, yc + SIZE_ARROW);
							}
						}
					}
				}

				g.setColor(Color.WHITE);
				for (int i = 0; i < boardSize; i++) {
					for (int j = 0; j < boardSize; j++) {
						if (world.tc.board[i][j] >= '0' && world.tc.board[i][j] <= '9') {
							g.fillRect(15 + j * cellSize + 1, 15 + i * cellSize + 1, cellSize - 1, cellSize - 1);
						}
					}
				}
				// draw the health of each base
				for (int i = 0; i < world.tc.baseCnt; i++) {
					g.setColor(Color.WHITE);
					int x = world.tc.baseX[i];
					int y = world.tc.baseY[i];
					g.fillRect(15 + x * cellSize + 1, 15 + y * cellSize + 1, cellSize - 1, cellSize - 1);

					g.setColor(new Color(0x00, 0xCC, 0xFF));
					int height = (int) ((cellSize - 1) * world.baseHealth[i] / 1000.0);
					g.fillRect(15 + x * cellSize + 1, 15 + (y + 1) * cellSize - height, cellSize - 1, height);

					g.setColor(Color.BLACK);
					drawStringCenter(g, "" + i, 15 + x * cellSize + 1, 15 + y * cellSize + 1, cellSize - 2, cellSize - 2);
				}

				// draw towers
				Stroke origStroke = g.getStroke();
				g.setStroke(new BasicStroke(0.7f));
				Color towerColor = new Color(0x99, 0xFF, 0x00);
				for (int i = 0; i < boardSize; i++) {
					for (int j = 0; j < boardSize; j++) {
						if (world.tc.board[i][j] >= 'A') {
							int ttype = world.tc.board[i][j] - 'A';
							TowerType tt = world.tc.towerTypes[ttype];

							g.setColor(towerColor);
							//							int ttype = world.tc.board[i][j] - 'A';
							//							float hue = (float) (ttype) / world.tc.towerTypeCnt;
							//							g.setColor(Color.getHSBColor(hue, 0.9f, 1.0f));
							g.fillOval(15 + j * cellSize + 2, 15 + i * cellSize + 2, cellSize - 4, cellSize - 4);

							g.setColor(new Color(0x99, 0x33, 0x00));
							drawStringCenter(g, "" + tt.damage, 15 + j * cellSize + 1, 15 + i * cellSize + 1, cellSize - 2,
									cellSize - 2);
							//							g.drawString("" + tt.damage, 15 + j * cellSize + 1, 15 + (i + 1) * cellSize - 1);

							// draw area of attack
							if (debugMode) {
								g.setColor(towerColor);
								int r = world.tc.towerTypes[ttype].range;
								g.drawOval(15 + (j - r) * cellSize + 1, 15 + (i - r) * cellSize + 1, cellSize * (r * 2 + 1) - 1,
										cellSize * (r * 2 + 1) - 1);
								//							g.setColor(new Color(128, 128, 128, 30));
								//							g.fillOval(15 + (j - r) * cellSize + 1, 15 + (i - r) * cellSize + 1, cellSize * (r * 2 + 1) - 1, cellSize
								//									* (r * 2 + 1) - 1);
							}
						}
					}
				}
				g.setStroke(origStroke);
				for (Creep c : world.tc.creeps)
					if (c.health > 0 && c.spawnTime < world.curStep) {
						float h = Math.min(1.f, (float) (c.health) / (world.tc.creepType.health));
						g.setColor(new Color(h, 0, 0));
						g.fillRect(15 + c.x * cellSize + 1 + cCnt[c.y][c.x], 15 + c.y * cellSize + 1 + cCnt[c.y][c.x],
								cellSize - 1, cellSize - 1);
						cCnt[c.y][c.x] += 2;
					}

				g.setColor(Color.GREEN);
				for (AttackVis a : world.attacks) {
					g.drawLine(15 + a.x1 * cellSize + cellSize / 2, 15 + a.y1 * cellSize + cellSize / 2, 15 + a.x2 * cellSize
							+ cellSize / 2, 15 + a.y2 * cellSize + cellSize / 2);
				}

				g.setColor(Color.BLACK);

				int horPos = 40 + boardSize * cellSize;

				int strY = 20;
				int strH = 16;
				g.setFont(fontInfo);
				g.drawString("Board size = " + boardSize, horPos, strY);
				g.drawString("Simulation Step = " + world.curStep, horPos, strY + strH * 1);
				g.drawString("Creeps Spawned = " + world.numSpawned, horPos, strY + strH * 2);
				g.drawString("Creeps killed = " + world.numKilled, horPos, strY + strH * 3);
				g.drawString("Towers placed = " + world.numTowers, horPos, strY + strH * 4);
				g.drawString("Money gained = " + world.moneyIncome, horPos, strY + strH * 5);
				g.drawString("Money spend = " + world.moneySpend, horPos, strY + strH * 6);
				g.drawString("Money = " + world.totMoney, horPos, strY + strH * 7);
				CreepType ct = world.tc.creepType;
				g.drawString("Creep Heapth = " + ct.health + " * " + (1 << (world.curStep / 500)), horPos, strY + strH * 8);
				g.drawString("Creep Gain = " + ct.money, horPos, strY + strH * 9);
				int baseh = 0;
				for (int i = 0; i < world.baseHealth.length; i++) {
					g.setColor(Color.GREEN);
					g.fillRect(horPos + 30, 205 + i * 20, world.baseHealth[i] / 10, 19);
					g.setColor(Color.BLACK);
					g.fillRect(horPos + 30 + world.baseHealth[i] / 10, 205 + i * 20, 100 - world.baseHealth[i] / 10, 19);
					g.drawString(Integer.toString(world.baseHealth[i]), horPos, 220 + i * 20);
					baseh += world.baseHealth[i];
				}
				g.drawString("Base health = " + baseh, horPos, 195);
				int score = world.totMoney + baseh;
				g.drawString("Score = " + score, horPos, 225 + world.baseHealth.length * 20);
			}
		}
	}

	void drawStringCenter(Graphics2D g, String str, int x, int y, int w, int h) {
		FontMetrics metrics = g.getFontMetrics();
		Rectangle2D rect = metrics.getStringBounds(str, g);
		int descent = metrics.getDescent();
		int height = metrics.getHeight();
		int xPos = (int) (x + w / 2.0 - rect.getWidth() / 2.0);
		int yPos = (int) (y + h / 2.0 + height / 2.0 - descent);
		g.drawString(str, xPos, yPos);
	}

	class DrawerWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			System.exit(0);
		}
	}

	final Object keyMutex = new Object();
	boolean keyPressed;

	public void processPause() {
		synchronized (keyMutex) {
			if (!stepMode && !pauseMode) {
				return;
			}
			keyPressed = false;
			while (!keyPressed) {
				try {
					keyMutex.wait();
				} catch (InterruptedException e) {
					// do nothing
				}
			}
		}
	}

	public Drawer(World world, int cellSize) {
		super();
		panel = new DrawerPanel();
		getContentPane().add(panel);
		addWindowListener(new DrawerWindowListener());
		this.world = world;
		boardSize = world.tc.boardSize;
		this.cellSize = cellSize;
		width = cellSize * boardSize + EXTRA_WIDTH;
		height = cellSize * boardSize + EXTRA_HEIGHT;
		fontBoard = new Font("Ricty", Font.BOLD, cellSize - 2);
		fontInfo = new Font("Ricty", Font.BOLD, 12);
		addKeyListener(new DrawerKeyListener());
		setSize(width, height);
		setTitle("Visualizer tool for problem PathDefense");
		setResizable(false);
		setVisible(true);
	}
}

class World {
	final Object worldLock = new Object();
	TestCase tc;
	int totMoney;
	int curStep = -1;
	int numSpawned = 0;
	int numKilled = 0;
	int numTowers = 0;
	int moneyIncome = 0;
	int moneySpend = 0;
	int[] baseHealth;
	List<Tower> towers = new ArrayList<Tower>();
	List<AttackVis> attacks = new ArrayList<AttackVis>();
	HashSet<Creep> liveCreeps = new HashSet<>();

	public World(TestCase tc) {
		this.tc = tc;
		totMoney = tc.money;
		baseHealth = new int[tc.baseCnt];
		Arrays.fill(baseHealth, 1000);
	}

	public void updateCreeps() {
		synchronized (worldLock) {
			for (Creep c : tc.creeps)
				if (c.health > 0 && c.spawnTime < curStep) {
					int dir = c.moves.get(curStep - c.spawnTime - 1);
					c.x += Constants.DX[dir];
					c.y += Constants.DY[dir];
					if (tc.board[c.y][c.x] >= '0' && tc.board[c.y][c.x] <= '9') {
						// reached a base
						int b = tc.board[c.y][c.x] - '0';
						// decrease the health of the base
						baseHealth[b] = Math.max(0, baseHealth[b] - c.health);
						c.health = 0;
						liveCreeps.remove(c);
					}
				} else if (c.spawnTime == curStep) {
					numSpawned++;
					liveCreeps.add(c);
				}
		}
	}

	public void updateAttack() {
		synchronized (worldLock) {
			attacks.clear();
			for (Tower t : towers) {
				int cdist = tc.towerTypes[t.type].range * tc.towerTypes[t.type].range;
				Creep targetCreep = null;
				for (Creep c : liveCreeps) {
					int dst = (t.x - c.x) * (t.x - c.x) + (t.y - c.y) * (t.y - c.y);
					// nearest creep?
					if (dst < cdist) {
						cdist = dst;
						targetCreep = c;
					} else if (dst == cdist) {
						// creep with smallest id gets attacked first if they are the same distance away
						if (targetCreep == null || c.id < targetCreep.id) {
							targetCreep = c;
						}
					}
				}
				if (targetCreep != null) {
					targetCreep.health -= tc.towerTypes[t.type].damage;
					attacks.add(new AttackVis(t.x, t.y, targetCreep.x, targetCreep.y));
					if (targetCreep.health <= 0) {
						// killed it!
						totMoney += tc.creepType.money;
						numKilled++;
						moneyIncome += tc.creepType.money;
						liveCreeps.remove(targetCreep);
					}
				}
			}
		}
	}

	public void startNewStep() {
		curStep++;
	}
}

public class Tester {
	public static boolean vis = false;
	public static boolean debug = false;
	public static int cellSize = 16;
	public static int delay = 30;
	public static boolean startPaused = false;
	static int specifiedN = -1, specifiedB = -1, specifiedP = -1, specifiedZ = -1, specifiedW = -1;

	public Result runTest(long seed) {
		Result res = new Result();
		TestCase tc = new TestCase(seed);
		res.seed = seed;
		res.N = tc.boardSize;
		res.B = tc.baseCnt;
		res.P = tc.pathCnt;
		res.Z = tc.creepCnt;
		res.HP = tc.creepType.health;
		res.G = tc.creepType.money;

		long startTime = System.currentTimeMillis();
		PathDefense obj = new PathDefense();
		res.elapsed += System.currentTimeMillis() - startTime;

		String[] boardStr = new String[tc.boardSize];
		for (int i = 0; i < tc.boardSize; ++i) {
			boardStr[i] = String.valueOf(tc.board[i]);
		}
		int[] towerTypeData = new int[tc.towerTypeCnt * 3];
		for (int i = 0; i < tc.towerTypeCnt; i++) {
			towerTypeData[3 * i] = tc.towerTypes[i].range;
			towerTypeData[3 * i + 1] = tc.towerTypes[i].damage;
			towerTypeData[3 * i + 2] = tc.towerTypes[i].cost;
		}
		startTime = System.currentTimeMillis();
		obj.init(boardStr, tc.money, tc.creepType.health, tc.creepType.money, towerTypeData);
		res.elapsed += System.currentTimeMillis() - startTime;

		World world = new World(tc);
		Drawer drawer = null;
		if (vis) {
			drawer = new Drawer(world, cellSize);
			drawer.debugMode = debug;
			if (startPaused) {
				drawer.pauseMode = true;
			}
		}

		for (int t = 0; t < Constants.SIMULATION_TIME; t++) {
			world.startNewStep();

			int[] creeps = new int[world.liveCreeps.size() * 4];
			int ci = 0;
			for (Creep c : world.tc.creeps) {
				if (c.health > 0 && c.spawnTime < world.curStep) {
					creeps[ci++] = c.id;
					creeps[ci++] = c.health;
					creeps[ci++] = c.x;
					creeps[ci++] = c.y;
				}
			}
			try {
				startTime = System.currentTimeMillis();
				int[] newTowers = obj.placeTowers(creeps, world.totMoney, world.baseHealth);
				res.elapsed += System.currentTimeMillis() - startTime;
				int commandCnt = newTowers.length;
				if ((commandCnt % 3) != 0) {
					System.err.println("ERROR: Return array from placeTowers must be a multiple of 3.");
					return res;
				}
				if (commandCnt > 0) {
					for (int i = 0; i < newTowers.length; i += 3) {
						Tower newT = new Tower();
						newT.x = newTowers[i];
						newT.y = newTowers[i + 1];
						newT.type = newTowers[i + 2];
						if (newT.x < 0 || newT.x >= tc.boardSize || newT.y < 0 || newT.y >= tc.boardSize) {
							System.err.println("ERROR: Placement (" + newT.x + "," + newT.y + ") outside of bounds.");
							return res;
						}
						if (tc.board[newT.y][newT.x] != '#') {
							System.err.println("ERROR: Cannot place a tower at (" + newT.x + "," + newT.y + ").");
							return res;
						}
						if (newT.type < 0 || newT.type >= tc.towerTypeCnt) {
							System.err.println("ERROR: Trying to place an illegal tower type.");
							return res;
						}
						if (world.totMoney < tc.towerTypes[newT.type].cost) {
							System.err.println("ERROR: Not enough money to place tower.");
							return res;
						}
						world.totMoney -= tc.towerTypes[newT.type].cost;
						tc.board[newT.y][newT.x] = (char) ('A' + newT.type);
						world.towers.add(newT);
						world.numTowers++;
						world.moneySpend += tc.towerTypes[newT.type].cost;
					}
				}
			} catch (Exception e) {
				System.err.println("ERROR: time step = " + t + " (0-based). Unable to get the build commands"
						+ " from your solution.");
				e.printStackTrace();
				return res;
			}

			world.updateCreeps();
			world.updateAttack();

			if (vis) {
				drawer.processPause();
				drawer.repaint();
				try {
					Thread.sleep(delay);
				} catch (Exception e) {
					// do nothing
				}
			}
		}

		int score = world.totMoney;
		for (int b = 0; b < world.baseHealth.length; b++)
			score += world.baseHealth[b];

		res.money = world.totMoney;
		res.gain = world.moneyIncome;
		res.spent = world.moneySpend;
		res.health = score - world.totMoney;
		res.score = score;
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 1, begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-b")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-e")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-N")) {
				specifiedN = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-B")) {
				specifiedB = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-P")) {
				specifiedP = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-Z")) {
				specifiedZ = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-W")) {
				specifiedW = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-vis")) {
				vis = true;
			} else if (args[i].equals("-debug")) {
				debug = true;
			} else if (args[i].equals("-sz")) {
				cellSize = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-delay")) {
				delay = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-pause")) {
				startPaused = true;
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}

		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int N, B, P, Z, HP, G;
		int money, health, gain, spent;
		int score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("N:%2d B:%2d P:%2d\n", N, B, P);
			ret += String.format("Z:%4d HP:%2d G:%2d\n", Z, HP, G);
			ret += String.format("health:%4d money:%5d gain:%5d spent:%5d\n", health, money, gain, spent);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("score:%d", score);
			return ret;
		}
	}
}
