import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Tester {

	long seed;
	String message;
	String[] state = new String[27];
	double errorRate;
	int[] checkSum;
	Random r = new Random();
	long costC, costM = 0;

	private void generateTestCase(long x) {
		try {
			r = SecureRandom.getInstance("SHA1PRNG");
		} catch (Exception e) {
			//
		}
		r.setSeed(x);
		int length = r.nextInt(9901) + 100;
		if (x == 1) length = 20;
		if (x == 2) length = 40;
		if (x == 3) length = 80;
		checkSum = new int[length + 1];
		char[] chars = new char[length];
		for (int i = 0; i < length; i++) {
			int pick = r.nextInt(26);
			chars[i] = (char) ('A' + pick);
			checkSum[i + 1] = (checkSum[i] + pick) % 26;
		}
		message = String.valueOf(chars);
		errorRate = 0.01 + r.nextDouble() * 0.49;
		if (x == 1) errorRate = 0.10;
		if (x == 2) errorRate = 0.12;
		if (x == 3) errorRate = 0.15;
		if (10000 <= x && x < 20000) {
			errorRate = (0.50 - 0.01) * (x - 10000) / (19999 - 10000) + 0.01;
		}
		state[26] = "";
		for (int i = 0; i < 26; i++) state[i] = "" + (char) ('A' + i);
		for (int i = 26; i > 0; i--) {
			int pick = r.nextInt(i + 1);
			if (pick == i) continue;
			String temp = state[pick];
			state[pick] = state[i];
			state[i] = temp;
		}
//		System.err.println("real order:");
//		for (int i = 0; i < state.length; i++) {
//			System.err.printf("%2d ", (state[i].equals("") ? 26 : state[i].charAt(0) - 'A'));
//		}
//		System.err.println();
	}

	public int getChecksum(int start, int length) {
		if ((start < 0) || (length < 1) || (length > message.length()) || (start + length > message.length())) {
			throw new RuntimeException("Invalid parameters to getChecksum: " + start + ", " + length + "\n");
		}
		costC += 5;
		return (26 + checkSum[start + length] - checkSum[start]) % 26;
	}

	public String getMessage(int start, int length) {
		if ((start < 0) || (length < 1) || (length > message.length()) || (start + length > message.length())) {
			throw new RuntimeException("Invalid parameters to getMessage: " + start + ", " + length + "\n");
		}
		costM += length;
		StringBuilder ret = new StringBuilder();
		for (int i = start; i < start + length; i++) {
			if (r.nextDouble() < errorRate)
				ret.append(state[(int) Math.sqrt(Math.floor(r.nextDouble() * 729))]);
			else
				ret.append(message.charAt(i));
		}
		return ret.toString();
	}

	public int editDistance(String s1, String s2) {
		if (s1.equals(s2)) return 0;
		int[][] dp = new int[s1.length() + 1][s2.length() + 1];
		for (int i = 0; i <= s1.length(); i++) {
			for (int j = Math.max(0, i - 100); j <= Math.min(i + 100, s2.length()); j++) {
				if (i == 0) {
					dp[i][j] = j;
				} else if (j == 0) {
					dp[i][j] = i;
				} else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					dp[i][j] = dp[i - 1][j - 1];
				} else {
					dp[i][j] = 1 + Math.min(Math.min(dp[i - 1][j], dp[i][j - 1]), dp[i - 1][j - 1]);
				}
			}
		}
		return dp[s1.length()][s2.length()];
	}

	public Result runTest() {
		generateTestCase(this.seed);
		costM -= message.length();
		Result res = new Result();
		res.seed = this.seed;
		res.L = message.length();
		res.P = errorRate;
		long beforeTime = System.currentTimeMillis();
		try {
			MessageChecksum solver = new MessageChecksum();
			solver.Sender = this;
			String ret = solver.receiveMessage(message.length(), getMessage(0, message.length()));
			res.elapsed = System.currentTimeMillis() - beforeTime;
			res.correctness = editDistance(message, ret);
			res.costM = costM;
			res.costC = costC;
//			System.err.println("expect:" + message);
//			System.err.println("actual:" + ret);
		} catch (Exception e) {
			res.elapsed = System.currentTimeMillis() - beforeTime;
			res.correctness = 999999;
			res.costM = res.costC = 999999;
			e.printStackTrace();
		}
		return res;
	}

	Tester(long seed) {
		this.seed = seed;
	}

	private static final int THREAD_COUNT = 3;

	public static void main(String[] args) {
		long seed = 0, begin = -1, end = -1;
		double dummy = MessageChecksum.PROB_TH + MessageChecksum.CONFIDENCE_TH + MessageChecksum.VERIFY_PROB_1 + MessageChecksum.VERIFY_PROB_2;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-p1")) MessageChecksum.PROB_TH = Double.parseDouble(args[++i]);
			if (args[i].equals("-p2")) MessageChecksum.CONFIDENCE_TH = Double.parseDouble(args[++i]);
			if (args[i].equals("-p3")) MessageChecksum.VERIFY_PROB_1 = Double.parseDouble(args[++i]);
			if (args[i].equals("-p4")) MessageChecksum.VERIFY_PROB_2 = Double.parseDouble(args[++i]);
		}
		if (begin != -1 && end != -1) {
			BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
			BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
			for (long i = begin; i <= end; ++i) {
				q.add(i);
			}
			Result[] results = new Result[(int) (end - begin + 1)];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i] = new TestThread(q, receiver);
				threads[i].start();
			}
			int printed = 0;
			try {
				for (int i = 0; i < (int) (end - begin + 1); i++) {
					Result res = receiver.poll(160, TimeUnit.SECONDS);
					results[(int) (res.seed - begin)] = res;
					for (; printed < results.length && results[printed] != null; printed++) {
						System.out.println(results[printed]);
						System.out.println();
					}
				}
			} catch (InterruptedException e) {
				for (int i = printed; i < results.length; i++) {
					System.out.println(results[i]);
					System.out.println();
				}
				e.printStackTrace();
			}

			double sum = 0;
			for (Result result : results) {
				sum += result.score();
			}
			System.out.println("ave:" + (sum / (end - begin + 1)));
		} else {
			Tester tester = new Tester(seed);
			Result res = tester.runTest();
			System.out.println(res);
		}
	}

	private static class TestThread extends Thread {
		BlockingQueue<Result> results;
		BlockingQueue<Long> q;

		TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
			this.q = q;
			this.results = results;
		}

		public void run() {
			while (true) {
				Long seed = q.poll();
				if (seed == null) break;
				try {
					Tester f = new Tester(seed);
					Result res = f.runTest();
					results.add(res);
				} catch (Exception e) {
					e.printStackTrace();
					Result res = new Result();
					res.seed = seed;
					results.add(res);
				}
			}
		}
	}
}

class Result {
	long seed;
	int L;
	double P;
	int correctness;
	long costM, costC;
	long elapsed;

	long score() {
		return (correctness + 1) * (costM + costC + 100);
	}

	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append(String.format("seed:%4d\n", seed));
		ret.append(String.format("L:%5d P:%.4f\n", L, P));
		ret.append(String.format("elapsed:%.4f\n", elapsed / 1000.0));
		ret.append(String.format("correct:%4d costM:%6d costC:%5d\n", correctness, costM, costC));
		ret.append(String.format("score:%d", score()));
		return ret.toString();
	}
}


