import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MessageChecksum {
	Tester Sender;
	private static final boolean DEBUG = false;
	private static final int CHAR_EMPTY = 26;
	static double PROB_TH = 0.976;
	static double CONFIDENCE_TH = 0.890;
	static double VERIFY_PROB_1 = 0.9998;
	static double VERIFY_PROB_2 = 0.994;
	int L;
	double P;
	int pos;
	int getMessageCount, getChecksumCount;
	int[] initialMessage;
	int[] ans;
	int[] firstRead;
	ArrayList<Integer> realPos = new ArrayList<>();
	ArrayList<Integer> initialPos = new ArrayList<>();
	int[] posLo, posHi;
	boolean[] posSkipped;
	int[][] history;
	int[] errorCount = new int[27];
	double[] errorProb = new double[27];
	double[][] ansProb;
	double[] confidence;

	public String receiveMessage(int length, String s) {
		L = length;
		ans = new int[L];
		initialMessage = new int[s.length()];
		for (int i = 0; i < s.length(); i++) {
			initialMessage[i] = s.charAt(i) - 'A';
		}
		debug("gap:" + (L - initialMessage.length));
		confidence = new double[L];
		history = new int[L][27];
		ansProb = new double[L][26];
		initialize();
		solve();
//		if (DEBUG) {
//			P = 0;
//			int[] tmp = new int[27];
//			for (int i = 0; i < 27; i++) {
//				tmp[i] = (errorCount[i] << 5) + i;
//				P += errorCount[i];
//			}
//			P /= getMessageCount;
//			debug(String.format("P:%.4f", P));
//			Arrays.sort(tmp);
//			for (int i = 0; i < 27; i++) {
//				System.err.printf("%2d ", (tmp[i] & 31));
//			}
//			System.err.println();
//		}
		char[] ret = new char[L];
		for (int i = 0; i < L; i++) {
			ret[i] = (char) ('A' + ans[i]);
//			if (DEBUG && ret[i] != Sender.message.charAt(i)) {
//				for (int j = 0; j < 26; j++) {
//					System.err.print((char)(j + 'A') + ":" + history[i][j] + "  ");
//				}
//				System.err.println();
//				for (int j = 0; j < 26; j++) {
//					System.err.println((char)(j + 'A') + ":" + ansProb[i][j] + "  ");
//				}
//				System.err.println(i + ":" + " expect:" + (Sender.message.charAt(i)) + " actual:" + (ret[i]));
//				for (int j = 0; j < posProb.prob[0].length; j++) {
//					int idx = posProb.upper[i] - posProb.range + j + 1;
//					System.err.println("  " + idx + " " + (char)(initialMessage[idx] + 'A') + ":" + posProb.prob[i][j]);
//				}
//				System.err.println();
//			}
		}
//		for (int i = 0; i < 27; i++) {
//			System.err.println((char)(i + 'A') + ":" + errorProb[i]);
//		}
//		debug("expect:" + Sender.message);
//		debug("actual:" + String.valueOf(ret));
		return String.valueOf(ret);
	}

	void initialize() {
		firstRead = new int[L];
		for (int i = 0; i < L; i++) {
			int ch = getMessageChar(i);
			while (ch == CHAR_EMPTY) {
				ch = getMessageChar(i);
			}
			firstRead[i] = ch;
		}
//			for (int i = 0; i < initialMessage.length; i++) {
//				System.err.print((char) (initialMessage[i] + 'A'));
//			}
//			System.err.println();
//			for (int i = 0; i < firstRead.length; i++) {
//				System.err.print((char) (firstRead[i] + 'A'));
//			}
//			System.err.println();
		final int gap = L - initialMessage.length;
		int[][] dp = new int[L + 1][gap + 1];
		for (int i = 0; i < L; i++) {
			for (int j = Math.max(0, gap - i); j <= gap; j++) {
				if (j > 0) dp[i + 1][j - 1] = Math.max(dp[i + 1][j - 1], dp[i][j]);
				if (i - gap + j < initialMessage.length) {
					if (firstRead[i] == initialMessage[i - gap + j]) {
						dp[i + 1][j] = Math.max(dp[i + 1][j], dp[i][j] + 1);
					} else {
						dp[i + 1][j] = Math.max(dp[i + 1][j], dp[i][j]);
					}
				}
			}
		}
		int p1 = L;
		int p2 = initialMessage.length;
		realPos.add(L);
		initialPos.add(initialMessage.length);
		while (p1 > 0 && p2 > 0) {
			int initI = gap - (p1 - p2);
			int cur = dp[p1][initI];
			if (firstRead[p1 - 1] == initialMessage[p2 - 1] && dp[p1 - 1][initI] + 1 == cur) {
				realPos.add(p1 - 1);
				initialPos.add(p2 - 1);
				p1--;
				p2--;
			} else if (firstRead[p1 - 1] != initialMessage[p2 - 1] && dp[p1 - 1][initI] == cur) {
				p1--;
				p2--;
			} else {
				p1--;
				assert (p1 >= p2);
			}
		}
		realPos.add(-1);
		initialPos.add(-1);
		Collections.reverse(realPos);
		Collections.reverse(initialPos);
		posLo = new int[L];
		posHi = new int[L];
		posSkipped = new boolean[L];
		for (int i = 0; i < L; i++) {
			int ai = Collections.binarySearch(realPos, i);
			if (ai >= 0) {
				posLo[i] = posHi[i] = initialPos.get(ai);
			} else {
				ai = -ai - 2;
				int realLo = realPos.get(ai);
				int realHi = realPos.get(ai + 1);
				int initLo = initialPos.get(ai);
				int initHi = initialPos.get(ai + 1);
				int countInit = initHi - 1 - initLo;
				posLo[i] = countInit < realHi - i ? initLo + 1 : initHi - (realHi - i);
				posHi[i] = countInit < i - realLo ? initHi - 1 : initLo + (i - realLo);
				posSkipped[i] = realHi - 1 - realLo > countInit;
			}
		}
	}

	void solve() {
		calcErrorProb();
		ArrayList<Integer> steps = new ArrayList<>();
		steps.add(0);
		steps.add(pos);
		final int maxStep = 1000;
		for (; pos + 3 <= L; ) {
			double confidenceMul = 1.0;
			int idx = pos;
			for (; confidenceMul > CONFIDENCE_TH && idx < pos + maxStep && idx < L; idx++) {
				int div;
				if (posSkipped[idx]) {
					div = posHi[idx] < posLo[idx] ? 1 : (posHi[idx] - posLo[idx] + 1);
				} else {
					div = (posHi[idx] - posLo[idx] + 1);
				}
				double all = posSkipped[idx] ? P * errorProb[CHAR_EMPTY] : 0;
				for (int j = posLo[idx]; j <= posHi[idx]; ++j) {
					double prob = 1.0 / div;
					all += prob * P * errorProb[initialMessage[j]];
					ansProb[idx][initialMessage[j]] += prob * (1 - P);
				}
//				double all = P * errorProb[CHAR_EMPTY];
//				for (int j = Math.min(initialMessage.length - 1, idx); j >= 0 && idx - j <= L - initialMessage.length; j--) {
//					double prob = posProb.getProb(idx - 1, j);
//					all += prob * P * errorProb[initialMessage[j]];
//					ansProb[idx][initialMessage[j]] += prob * (1 - P);
//				}
				double sum = 0;
				for (int j = 0; j < 26; j++) {
					ansProb[idx][j] += all;
					sum += ansProb[idx][j];
				}
				for (int j = 0; j < 26; j++) {
					ansProb[idx][j] /= sum;
				}
				for (int j = 0; ; j++) {
					int ch = j == 0 ? firstRead[idx] : getMessageChar(idx);
					if (ch == CHAR_EMPTY) continue;
					int maxCh = updateAnsProb(idx, ch);
					if (ansProb[idx][maxCh] > PROB_TH) {
						debug(j + " " + maxCh + " " + ansProb[idx][maxCh]);
						confidenceMul *= ansProb[idx][maxCh];
						confidence[idx] = ansProb[idx][maxCh];
						ans[idx] = maxCh;
						for (int k = 0; k < 27; k++) {
							if (k != ans[idx]) errorCount[k] += history[idx][k];
							getMessageCount += history[idx][k];
						}
						calcErrorProb();
						break;
					}
				}
			}

			if (verifyChecksum(steps.get(steps.size() - 2), pos, idx)) {
//				if (P < 0.21) {
//					resetErrorCount(idx);
//				}
			}
			pos = idx;
			steps.add(pos);
		}
		for (; pos < L; pos++) {
			ans[pos] = getChecksum(pos, 1);
		}
	}

	boolean verifyChecksum(int prev, int start, int end) {
		int begin = start - (start - prev) / 2;
		int checksum = getChecksum(begin, end - begin);
		int mysum = 0;
		for (int i = begin; i < end; i++) {
			mysum += ans[i];
		}
		if ((mysum - checksum) % 26 == 0) return false;
		int[] cands = new int[end - start];
		for (int i = start; i < end; i++) {
			cands[i - start] = (((int) (confidence[i] * 100000)) << 10) + (i - start);
		}
		Arrays.sort(cands);
		for (int i = 0; i < end - start; i++) {
			int p = (cands[i] & 0x3FF) + start;
			int real = -1;
			while (real == -1) {
				int ch = getMessageChar(p);
				while (ch == CHAR_EMPTY) {
					ch = getMessageChar(p);
				}
				int maxCh = updateAnsProb(p, ch);
				if (maxCh == ans[p]) {
					if (ansProb[p][maxCh] > VERIFY_PROB_1) {
						real = maxCh;
					}
				} else {
					if (ansProb[p][maxCh] > VERIFY_PROB_2) {
						real = maxCh;
					}
				}
			}
			debug(p + " ans:" + ans[p] + " real:" + real);
			if (ans[p] == real) continue;
			mysum += real - ans[p];
			ans[p] = real;
			if ((mysum - checksum) % 26 == 0) return true;
		}
		int prevChecksum = 0;
		for (int i = prev; i < start; i++) {
			prevChecksum += ans[i];
		}
		mysum = prevChecksum;
		int prevFixPos = -1;
		for (int i = begin; i < start; i++) {
			int real = getChecksum(i, 1);
			debug("ans:" + ans[i] + " real:" + real);
			if (ans[i] == real) continue;
			mysum += real - ans[i];
			ans[i] = real;
			prevFixPos = i;
			break;
		}
		if (prevFixPos != -1) {
			for (int i = prev; i < begin; i++) {
				int real = getChecksum(i, 1);
				debug(i + " ans:" + ans[i] + " real:" + real);
				if (ans[i] == real) continue;
				mysum += real - ans[i];
				ans[i] = real;
				if ((mysum - prevChecksum) % 26 == 0) return true;
			}
			for (int i = prevFixPos + 1; i < start; i++) {
				int real = getChecksum(i, 1);
				debug("ans:" + ans[i] + " real:" + real);
				if (ans[i] == real) continue;
				mysum += real - ans[i];
				ans[i] = real;
				if ((mysum - prevChecksum) % 26 == 0) return true;
			}
		}
		for (int i = start; i < end; i++) {
			int real = getChecksum(i, 1);
			debug(i + " ans:" + ans[i] + " real:" + real);
			if (ans[i] == real) continue;
			mysum += real - ans[i];
			ans[i] = real;
			if ((mysum - prevChecksum) % 26 == 0) return true;
		}
		return true;
	}

	int updateAnsProb(int idx, int ch) {
		double sumProb = 0;
		for (int k = 0; k < 26; k++) {
			if (k == ch) {
				ansProb[idx][k] = ansProb[idx][k] * (1 - P) + ansProb[idx][k] * P * errorProb[ch];
			} else {
				ansProb[idx][k] *= P * errorProb[ch];
			}
			sumProb += ansProb[idx][k];
		}
		int maxCh = 0;
		for (int k = 0; k < 26; k++) {
			ansProb[idx][k] /= sumProb;
			if (ansProb[idx][maxCh] < ansProb[idx][k]) maxCh = k;
		}
		return maxCh;
	}

	void resetErrorCount(int idx) {
		Arrays.fill(errorCount, 0);
		for (int i = 0; i < idx; i++) {
			for (int j = 0; j < 27; j++) {
				if (j != ans[i]) errorCount[j] += history[i][j];
			}
		}
		calcErrorProb();
	}

	void calcErrorProb() {
		final double PSEUDO_COUNT = 2;
		P = 0;
		double sum = PSEUDO_COUNT * 27;
		for (int i = 0; i < 27; i++) {
			sum += errorCount[i];
			P += errorCount[i];
		}
		double baseP = Math.pow(1.0 * (realPos.size() - 2) / L, 0.9);
		P += baseP;
		P /= (getMessageCount + 1);
		debug(String.format("P:%.4f", P));
		P = Math.min(P, 0.50);
		P = Math.max(P, 0.01);
		for (int i = 0; i < 27; i++) {
			errorProb[i] = 1.0 * (errorCount[i] + PSEUDO_COUNT) / sum;
		}
	}

	int getMessageChar(int pos) {
//		++getMessageCount;
		String s = Sender.getMessage(pos, 1);
		int ret = s.isEmpty() ? CHAR_EMPTY : s.charAt(0) - 'A';
		history[pos][ret]++;
		return ret;
	}

	int getChecksum(int pos, int len) {
		debug(pos + " " + len);
		++getChecksumCount;
		return Sender.getChecksum(pos, len);
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}
}

