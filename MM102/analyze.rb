#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:W, :C, :I, :CF, :CV, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /W: *(\d+) C: *(\d+) I: *(\d+) CF: *(\d+) CV: *(\d+)/
      testcase.W = $1.to_i
      testcase.C = $2.to_i
      testcase.I = $3.to_i
      testcase.CF = $4.to_i
      testcase.CV = $5.to_i
    elsif line =~ /score: *(\d+)/
      testcase.score = $1.to_i
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    count += 1
    if s1 > s2
      sum_score_diff += 1.0 - (1.0 * s2 / s1)
      win += 1
    elsif s1 < s2
      sum_score_diff -= 1.0 - (1.0 * s1 / s2)
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff * 1000000.0 / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  (1).step(20, 4) do |param|
    upper = param + 3
    puts "W:#{sprintf('%2d', param)}-#{sprintf('%2d', upper)}"
    calc(from, to) { |from, to| from.W.between?(param, upper) }
  end
  puts "---------------------"
  (1).step(1000, 200) do |param|
    upper = param + 199
    puts "C:#{sprintf('%4d', param)}-#{sprintf('%4d', upper)}"
    calc(from, to) { |from, to| from.C.between?(param, upper) }
  end
  puts "---------------------"
  (10).step(100, 20) do |param|
    upper = param + 19
    puts "I:#{sprintf('%3d', param)}-#{sprintf('%3d', upper)}"
    calc(from, to) { |from, to| from.I.between?(param, upper) }
  end
  puts "---------------------"
  (5).step(50, 10) do |param|
    upper = param + 9
    puts "CF:#{sprintf('%2d', param)}-#{sprintf('%2d', upper)}"
    calc(from, to) { |from, to| from.CF.between?(param, upper) }
  end
  puts "---------------------"
  (1).step(20, 3) do |param|
    upper = param + 2
    puts "CV:#{sprintf('%2d', param)}-#{sprintf('%2d', upper)}"
    calc(from, to) { |from, to| from.CV.between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
