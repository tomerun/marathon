import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class TrucksAndCouriers {

	private static final boolean DEBUG = true;
	private static final long TL = 4000;
	private static final double INITIAL_COOLER = 0.05;
	private static final double FINAL_COOLER = 2;
	private static final int INF = 99999999;
	private static final int MAX_PHASE = 20;
	private final long startTime = System.currentTimeMillis();
	private final SplittableRandom rnd = new SplittableRandom(42);
	private int W, C, I;
	private double CF, CV;
	private WareHouse[] whs;
	private Customer[] csts;
	private Base[][] basePos = new Base[1001][1001];
	private double cooler = INITIAL_COOLER;
	private static final int moveWindow = 20;
	private ArrayList<ArrayList<Base>> i2w = new ArrayList<>();
	private int[][] dpIdx;
	private int[][] dpCost;
	private int[][] dpFrom;
	private int dpI;

	public String[] planShipping(int truckFixed, int truckVariable, int[] whX, int[] whY, int[] whI,
															 int[] whQ, int[] customerX, int[] customerY, int[] customerItem) {
		C = customerX.length;
		csts = new Customer[C];
		for (int i = 0; i < C; i++) {
			csts[i] = new Customer(customerY[i], customerX[i], customerItem[i]);
//			debug(String.format("%d %d %d", customerY[i], customerX[i], customerItem[i]));
		}
		for (int i = 0; i < whI.length; i++) {
			I = Math.max(I, whI[i] + 1);
//			debug(String.format("(%d %d) %d %d", whY[i], whX[i], whI[i], whQ[i]));
		}
		CF = truckFixed;
		CV = truckVariable;
		ArrayList<WareHouse> list = new ArrayList<>();
		for (int i = 0; i < whX.length; i++) {
			int idx = -1;
			for (int j = 0; j < list.size(); j++) {
				if (list.get(j).r == whY[i] && list.get(j).c == whX[i]) {
					idx = j;
					break;
				}
			}
			if (idx == -1) {
				idx = list.size();
				list.add(new WareHouse(whY[i], whX[i]));
			}
			list.get(idx).items[whI[i]] += whQ[i];
		}
		whs = list.toArray(new WareHouse[0]);
		W = whs.length;
		ArrayList<Convey> ans = solve();
		String[] ret = new String[ans.size()];
		for (int i = 0; i < ans.size(); i++) {
			ret[i] = ans.get(i).toString();
//			debug(ret[i]);
		}
		return ret;
	}

	private ArrayList<Convey> solve() {
		State state = createInitialSolution();
		dpIdx = new int[MAX_PHASE][state.junctions.size() + W];
		dpCost = new int[MAX_PHASE][state.junctions.size() + W];
		dpFrom = new int[MAX_PHASE][state.junctions.size() + W];
		state = simulatedAnnealing(state);
		debug("junctions:" + state.junctions.size());
		return state.serialize();
	}

	private State simulatedAnnealing(State state) {
		int bestScore = state.score;
		final long beginTime = System.currentTimeMillis() - startTime;
		final double INITIAL_CV = 0.1;
		final double FINAL_CV = CV;
		CV = INITIAL_CV;
		for (int turn = 1; ; turn++) {
			if ((turn & 0xFFF) == 0) {
				final long elapsed = System.currentTimeMillis() - startTime;
				if (elapsed > TL) {
					debug("score:" + state.score + " bestScore:" + bestScore + " turn:" + turn);
					break;
				}
				double ratio = 1.0 * (elapsed - beginTime) / (TL - beginTime);
				double c1 = Math.log(INITIAL_COOLER);
				double c2 = Math.log(FINAL_COOLER);
				cooler = elapsed > TL - 50 ? 100 : Math.exp(c1 + (c2 - c1) * ratio);

				debug("prev score:" + state.score);
				double nextCV = ratio > 0.9 ? FINAL_CV : INITIAL_CV + (FINAL_CV - INITIAL_CV) * ratio;
				for (Base b : state.warehouses) {
					for (Truck t : b.depart) {
						int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
						int oldCost = (int) (CV * d);
						int newCost = (int) (nextCV * d);
						state.score += newCost - oldCost;
					}
				}
				for (Base b : state.junctions) {
					for (Truck t : b.depart) {
						int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
						int oldCost = (int) (CV * d);
						int newCost = (int) (nextCV * d);
						state.score += newCost - oldCost;
					}
				}
				bestScore = state.score;

				int s = 0;
				for (Base b : state.warehouses) {
					for (Truck t : b.depart) {
						int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
						s += CF + (int) (CV * d);
					}
				}
				for (Base b : state.junctions) {
					for (Truck t : b.depart) {
						int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
						s += CF + (int) (CV * d);
					}
				}
				for (Customer c : csts) {
					int d = Math.abs(c.r - c.from.r) + Math.abs(c.c - c.from.c);
					s += d;
				}
				debug("real prev score:" + s);
				s = 0;
				for (Base b : state.warehouses) {
					for (Truck t : b.depart) {
						int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
						s += CF + (int) (nextCV * d);
					}
				}
				for (Base b : state.junctions) {
					for (Truck t : b.depart) {
						int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
						s += CF + (int) (nextCV * d);
					}
				}
				for (Customer c : csts) {
					int d = Math.abs(c.r - c.from.r) + Math.abs(c.c - c.from.c);
					s += d;
				}
				debug("real next score:" + s);

				CV = nextCV;
				debug("score:" + state.score + " cooler:" + cooler + " CV:" + CV + " turn:" + turn + " ratio:" + ratio);


				//				if ((turn & 0xFFFFF) == 0) {
//					state.postProcess();
//				}
			}
			PriorityQueue<Integer> q = new PriorityQueue<>();
//			final int type = rnd.nextInt() & 1;
			final int type = 1;
			if (type == 0) {
				// move a base
				int pos = rnd.nextInt(state.junctions.size());
				Base b = state.junctions.get(pos);
				for (int i = 0; i < 20; i++) {
					if (!b.depart.isEmpty() || !b.csts.isEmpty()) break;
					pos = rnd.nextInt(state.junctions.size());
					b = state.junctions.get(pos);
				}
				int nr = b.r + rnd.nextInt(moveWindow * 2 + 1) - moveWindow;
				int nc = b.c + rnd.nextInt(moveWindow * 2 + 1) - moveWindow;
				if (nr < 0) nr = 0;
				if (nr > 1000) nr = 1000;
				if (nc < 0) nc = 0;
				if (nc > 1000) nc = 1000;
				int diff = 0;
				for (Truck m : b.arrive) {
					int od = Math.abs(m.from.r - b.r) + Math.abs(m.from.c - b.c);
					int md = Math.abs(m.from.r - nr) + Math.abs(m.from.c - nc);
					diff += (int) (md * CV) - (int) (od * CV);
				}
				for (Truck m : b.depart) {
					int od = Math.abs(m.to.r - b.r) + Math.abs(m.to.c - b.c);
					int md = Math.abs(m.to.r - nr) + Math.abs(m.to.c - nc);
					diff += (int) (md * CV) - (int) (od * CV);
				}
				for (Customer c : b.csts) {
					int od = Math.abs(c.r - b.r) + Math.abs(c.c - b.c);
					int md = Math.abs(c.r - nr) + Math.abs(c.c - nc);
					diff += md - od;
				}
				if (accept(diff)) {
					basePos[b.r][b.c] = null;
					b.r = nr;
					b.c = nc;
					basePos[nr][nc] = b;
					state.score += diff;
					if (state.score < bestScore) {
						bestScore = state.score;
					}
				}
			} else {
				// reconfigure a path
				Customer cst = csts[rnd.nextInt(C)];
				int diff = -Math.abs(cst.r - cst.from.r) - Math.abs(cst.c - cst.from.c);
				for (Truck t : cst.path) {
					t.count--;
					if (t.count == 0) {
						diff -= t.cost();
					}
				}
//				debug("prev path:" + cst.path);
				Base start = cst.path.isEmpty() ? cst.from : cst.path.get(cst.path.size() - 1).from;
				start.rest[cst.i]++;
				++dpI;
				q.clear();
				for (Base b : i2w.get(cst.i)) {
					if (b.rest[cst.i] == 0) continue;
					q.add(b.idx);
					dpIdx[0][b.idx] = dpI;
					dpCost[0][b.idx] = 0;
					dpFrom[0][b.idx] = -1;
				}
				int minCost = INF;
				int minIdx = -1;
				int minPhase = -1;
				while (!q.isEmpty()) {
					int cur = q.poll();
					int cost = cur >> 18;
					if (cost >= minCost) break;
					int phase = (cur >> 12) & 0x3F;
					int idx = cur & 0xFFF;
					Base b = state.getBase(idx);
					int curCost = cost + Math.abs(b.r - cst.r) + Math.abs(b.c - cst.c);
					if (curCost < minCost) {
						if (b != cst.from || curCost != -diff) {
							minCost = curCost;
							minIdx = idx;
							minPhase = phase;
						}
					}
					if (phase == MAX_PHASE - 1) continue;
					for (Truck t : b.depart) {
						if (t.phase <= phase) continue;
						if (t.count == 0) continue;
						if (dpIdx[t.phase][t.to.idx] != dpI || cost < dpCost[t.phase][t.to.idx]) {
							dpIdx[t.phase][t.to.idx] = dpI;
							dpCost[t.phase][t.to.idx] = cost;
							dpFrom[t.phase][t.to.idx] = cur;
//							debug("up1:" + t.phase + " " + idx + "->" + t.to.idx + " " + cost);
							q.add(encode(cost, t.phase, t.to.idx));
//							for (int i = t.phase + 1; i < MAX_PHASE; i++) {
//								if (dpIdx[i][t.to.idx] == dpI && dpCost[i][t.to.idx] <= cost) break;
//								dpIdx[i][t.to.idx] = dpI;
//								dpCost[i][t.to.idx] = cost;
//								dpFrom[i][t.to.idx] = cur;
//							}
						}
					}
					int phaseRange = Math.min(3, MAX_PHASE - 1 - phase);
					for (int i = 0; i < 3; i++) {
						int nextPhase = phase + 1 + rnd.nextInt(phaseRange);
						int nextIdx = rnd.nextInt(W + state.junctions.size());
						Base nb = state.getBase(nextIdx);
						int nextCost = (int) (cost + CF + CV * (Math.abs(nb.r - b.r) + Math.abs(nb.c - b.c)));
						if (nextCost >= minCost) continue;
						if (dpIdx[nextPhase][nextIdx] != dpI || nextCost < dpCost[nextPhase][nextIdx]) {
							dpIdx[nextPhase][nextIdx] = dpI;
							dpCost[nextPhase][nextIdx] = nextCost;
							dpFrom[nextPhase][nextIdx] = cur;
							q.add(encode(nextCost, nextPhase, nextIdx));
//							debug("up2:" + nextPhase + " " + idx + "->" + nextIdx + " " + nextCost);
						}
					}
				}
				diff += minCost;
				if (minCost != INF && accept(diff)) {
//					debug("diff:" + diff + " minIdx:" + minIdx + " minPhase:" + minPhase);
					int prevCost = Math.abs(cst.r - cst.from.r) + Math.abs(cst.c - cst.from.c);
					cst.from.csts.remove(cst);
					for (Truck t : cst.path) {
						if (t.count == 0) {
							t.from.depart.remove(t);
							t.to.arrive.remove(t);
							prevCost += t.cost();
						}
					}
//					debug("prev path:" + cst.path + " prev cost:" + prevCost);
					Base cb = state.getBase(minIdx);
					cb.csts.add(cst);
					cst.from = cb;
					cst.path.clear();
//					int afterCost = Math.abs(cst.r - cst.from.r) + Math.abs(cst.c - cst.from.c);
					while (true) {
						int prev = dpFrom[minPhase][minIdx];
						if (prev == -1) {
							assert (cb.rest[cst.i] > 0);
							cb.rest[cst.i]--;
							break;
						}
//						debug("prevCost:" + (prev >> 18));
						int prevPhase = (prev >> 12) & 0x3F;
						int prevIdx = prev & 0xFFF;
						Base pb = state.getBase(prevIdx);
						Truck pt = null;
//						debug(pb.depart);
						for (Truck t : pb.depart) {
							if (t.to == cb && t.phase == minPhase) {
								pt = t;
								break;
							}
						}
						if (pt == null) {
							pt = new Truck(pb, cb);
							pt.phase = minPhase;
							pt.count = 1;
							pb.depart.add(pt);
							cb.arrive.add(pt);
//							debug("afterCost:" + pt.cost() + " idx:" + prevIdx);
//							afterCost += pt.cost();
						} else {
							pt.count++;
						}
						cst.path.add(pt);
						minPhase = prevPhase;
						minIdx = prevIdx;
						cb = pb;
					}
//					debug("after Cost:" + afterCost);
					state.score += diff;
//					int s = 0;
//					for (Base b : state.warehouses) {
//						for (Truck t : b.depart) {
//							int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
//							s += CF + (int) (CV * d);
//						}
//					}
//					for (Base b : state.junctions) {
//						for (Truck t : b.depart) {
//							int d = Math.abs(t.from.r - t.to.r) + Math.abs(t.from.c - t.to.c);
//							s += CF + (int) (CV * d);
//						}
//					}
//					for (Customer c : csts) {
//						int d = Math.abs(c.r - c.from.r) + Math.abs(c.c - c.from.c);
//						s += d;
//					}
//					debug("real prev score:" + s + " score:" + state.score);
//					if (s != state.score) {
//						debug(cst);
//						debug(cst.path);
//						System.exit(1);
//					}
//					debug(cst);
//					debug(cst.path);
					if (state.score < bestScore) {
						bestScore = state.score;
					}
				} else {
					for (Truck t : cst.path) {
						t.count++;
					}
					start.rest[cst.i]--;
				}
			}
		}
		return state;
	}

	private int encode(int cost, int phase, int idx) {
		return (cost << 18) | (phase << 12) | idx;
	}

	private boolean accept(int diff) {
		if (diff <= 0) return true;
		return rnd.nextDouble() < Math.exp(-diff * cooler);
	}

	private State createInitialSolution() {
		ArrayList<ArrayList<Integer>> i2w = new ArrayList<>();
		for (int i = 0; i < I; i++) {
			i2w.add(new ArrayList<>());
		}
		ArrayList<Integer> wr = new ArrayList<>();
		ArrayList<Integer> wc = new ArrayList<>();
		ArrayList<Integer> wq = new ArrayList<>();
		ArrayList<Integer> wi = new ArrayList<>();
		for (int i = 0; i < W; i++) {
			for (int j = 0; j < I; j++) {
				if (whs[i].items[j] > 0) {
					i2w.get(j).add(wr.size());
					wr.add(whs[i].r);
					wc.add(whs[i].c);
					wq.add(whs[i].items[j]);
					wi.add(i);
				}
			}
		}
		int w = wr.size();
		MinCostFlow mcf = new MinCostFlow(C + w + 2);
		final int src = 0;
		final int sink = 1 + C + w;
		for (int i = 0; i < C; i++) {
			mcf.addEdge(src, 1 + i, 0, 1);
			int cr = csts[i].r;
			int cc = csts[i].c;
			for (int j : i2w.get(csts[i].i)) {
				mcf.addEdge(1 + i, 1 + C + j, Math.abs(cr - wr.get(j)) + Math.abs(cc - wc.get(j)), 1);
			}
		}
		for (int i = 0; i < w; i++) {
			mcf.addEdge(1 + C + i, sink, 0, wq.get(i));
		}
		int cost = mcf.calc(C);
		State state = new State();
		state.score = cost;
		debug("score without trucks:" + cost);
		for (int i = 0; i < C; i++) {
			for (MinCostFlow.Edge e : mcf.g.get(1 + i)) {
				if (e.to != src && e.cap == 0) {
					Base from = state.warehouses[wi.get(e.to - C - 1)];
					from.csts.add(csts[i]);
					from.rest[csts[i].i]--;
					csts[i].from = from;
					break;
				}
			}
		}
		return state;
	}

	class State {
		Base[] warehouses;
		ArrayList<Base> junctions = new ArrayList<>();
		int score;

		State() {
			warehouses = new Base[W];
			for (int i = 0; i < I; i++) {
				i2w.add(new ArrayList<>());
			}
			for (int i = 0; i < W; i++) {
				warehouses[i] = new Base(whs[i].r, whs[i].c, i);
				warehouses[i].rest = whs[i].items.clone();
				basePos[whs[i].r][whs[i].c] = warehouses[i];
				for (int j = 0; j < I; j++) {
					if (warehouses[i].rest[j] > 0) {
						i2w.get(j).add(warehouses[i]);
					}
				}
			}
			for (int i = 0; i < 500; i++) {
				int r = rnd.nextInt(1001);
				int c = rnd.nextInt(1001);
				if (basePos[r][c] != null) {
					--i;
					continue;
				}
				junctions.add(new Base(r, c, i + W));
				basePos[r][c] = junctions.get(i);
			}
		}

		Base getBase(int idx) {
			return idx < W ? warehouses[idx] : junctions.get(idx - W);
		}

		ArrayList<Convey> serialize() {
			ArrayList<Convey> ret = new ArrayList<>();
			ArrayList<Truck> trucks = new ArrayList<>();
			for (int i = 0; i < W; i++) {
				warehouses[i].rest = whs[i].items.clone();
				trucks.addAll(warehouses[i].depart);
			}
			for (Base junction : junctions) {
				junction.rest = new int[I];
				trucks.addAll(junction.depart);
			}
			debug("trucks size:" + trucks.size());
			trucks.sort(Comparator.comparingInt((Truck t) -> t.phase));
			for (Truck t : trucks) {
				t.items = new int[I];
			}
			for (Customer c : csts) {
				for (Truck t : c.path) {
					t.items[c.i]++;
				}
			}
			for (Truck t : trucks) {
				int size = 0;
				for (int i = 0; i < I; i++) {
					if (t.items[i] > 0) ++size;
				}
				int[] items = new int[size * 2];
				for (int i = 0, pos = 0; i < I; i++) {
					if (t.items[i] > 0) {
						items[pos] = i;
						items[pos + 1] = t.items[i];
						pos += 2;
					}
				}
				ret.add(new Convey(true, t.from.r, t.from.c, t.to.r, t.to.c, items));
			}
			for (Base b : warehouses) {
				for (Customer c : b.csts) {
					ret.add(new Convey(false, b.r, b.c, c.r, c.c, new int[]{c.i, 1}));
				}
			}
			for (Base b : junctions) {
				for (Customer c : b.csts) {
					ret.add(new Convey(false, b.r, b.c, c.r, c.c, new int[]{c.i, 1}));
				}
			}
			return ret;
		}
	}

	class Base {
		int r, c, idx;
		ArrayList<Truck> depart = new ArrayList<>();
		ArrayList<Truck> arrive = new ArrayList<>();
		ArrayList<Customer> csts = new ArrayList<>();
		int[] rest;

		Base(int r, int c, int idx) {
			this.r = r;
			this.c = c;
			this.idx = idx;
		}

		public String toString() {
			return String.format("(%d %d)", r, c);
		}
	}

	class Truck {
		Base from, to;
		int count, phase;
		int[] items;

		Truck(Base from, Base to) {
			this.from = from;
			this.to = to;
		}

		int cost() {
			int d = Math.abs(from.r - to.r) + Math.abs(from.c - to.c);
			return (int) (CF + CV * d);
		}

		public String toString() {
			return String.format("(%d %d)->(%d %d) %d %d", from.r, from.c, to.r, to.c, count, phase);
		}
	}

	static class Convey {
		boolean isTruck;
		int sr, sc, gr, gc;
		int[] items;

		Convey(boolean isTruck, int sr, int sc, int gr, int gc, int[] items) {
			this.isTruck = isTruck;
			this.sr = sr;
			this.sc = sc;
			this.gr = gr;
			this.gc = gc;
			this.items = items;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(this.isTruck ? 'T' : 'C');
			sb.append(',').append(sc).append(',').append(sr).append(',').append(gc).append(',').append(gr);
			for (int i = 0; i < items.length; i += 2) {
				for (int j = 0; j < items[i + 1]; j++) {
					sb.append(',').append(items[i]);
				}
			}
			return sb.toString();
		}
	}

	class WareHouse {
		int r, c;
		int[] items;

		WareHouse(int r, int c) {
			this.r = r;
			this.c = c;
			this.items = new int[I];
		}
	}

	class Customer {
		int r, c, i;
		Base from;
		ArrayList<Truck> path = new ArrayList<>();

		Customer(int r, int c, int i) {
			this.r = r;
			this.c = c;
			this.i = i;
		}

		public String toString() {
			return "Customer{r=" + r + ", c=" + c + ", i=" + i + '}';
		}
	}

	private static <T> void removeFromArrayList(ArrayList<T> list, int pos) {
		list.set(pos, list.get(list.size() - 1));
		list.remove(list.size() - 1);
	}

	static class MinCostFlow {
		int size;
		ArrayList<ArrayList<Edge>> g = new ArrayList<>();

		static class Edge {
			int from, to, cost, cap;
			Edge rev;

			Edge(int from, int to, int cost, int cap) {
				this.from = from;
				this.to = to;
				this.cost = cost;
				this.cap = cap;
			}
		}

		MinCostFlow(int size) {
			this.size = size;
			for (int i = 0; i < size; ++i) {
				g.add(new ArrayList<>());
			}
		}

		void addEdge(int from, int to, int c, int capacity) {
			Edge e1 = new Edge(from, to, c, capacity);
			Edge e2 = new Edge(to, from, -c, 0);
			e1.rev = e2;
			e2.rev = e1;
			g.get(from).add(e1);
			g.get(to).add(e2);
		}

		int calc(int flow) {
			final int INF = 1 << 25;
			int total = 0;
			int[] h = new int[size];
			Arrays.fill(h, 0);
			Edge[] prev = new Edge[size];
			while (flow > 0) {
				int[] dist = new int[size];
				Arrays.fill(dist, INF);
				dist[0] = 0;
				PriorityQueue<State> q = new PriorityQueue<>();
				q.add(new State(0, dist[0]));
				while (!q.isEmpty()) {
					State st = q.poll();
					if (st.cost > dist[st.v]) continue;
					for (Edge e : g.get(st.v)) {
						if (e.cap == 0) continue;
						int nCost = dist[st.v] + e.cost + h[st.v] - h[e.to];
						if (dist[e.to] > nCost) {
							dist[e.to] = nCost;
							q.add(new State(e.to, nCost));
							prev[e.to] = e;
						}
					}
				}
				if (dist[size - 1] == INF) return -1;
				for (int i = 0; i < size; ++i) {
					h[i] += dist[i];
				}
				int f = Integer.MAX_VALUE;
				for (int pos = size - 1; pos != 0; pos = prev[pos].from) {
					f = Math.min(f, prev[pos].cap);
				}
				for (int pos = size - 1; pos != 0; pos = prev[pos].from) {
					prev[pos].cap -= f;
					prev[pos].rev.cap += f;
				}
				total += h[size - 1] * f;
				flow -= f;
			}
			return total;
		}

		static class State implements Comparable<State> {
			int v;
			int cost;

			State(int v, int cost) {
				this.v = v;
				this.cost = cost;
			}

			public int compareTo(State o) {
				return this.cost - o.cost;
			}
		}

	}

	private static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	private static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	private static int[] readArray(BufferedReader br) throws Exception {
		int[] ret = new int[Integer.parseInt(br.readLine())];
		for (int i = 0; i < ret.length; i++) ret[i] = Integer.parseInt(br.readLine());
		return ret;
	}

	public static void main(String[] args) {
		try {
			TrucksAndCouriers sol = new TrucksAndCouriers();
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			int truckFixed = Integer.parseInt(br.readLine());
			int truckVariable = Integer.parseInt(br.readLine());
			int[] warehouseX = readArray(br);
			int[] warehouseY = readArray(br);
			int[] warehouseItem = readArray(br);
			int[] warehouseQuantity = readArray(br);
			int[] customerX = readArray(br);
			int[] customerY = readArray(br);
			int[] customerItem = readArray(br);
			String[] ret = sol.planShipping(truckFixed, truckVariable, warehouseX, warehouseY,
					warehouseItem, warehouseQuantity, customerX, customerY, customerItem);
			System.out.println(ret.length);
			for (int i = 0; i < ret.length; i++) System.out.println(ret[i]);
			System.out.flush();
		} catch (Exception e) {
		}
	}
}