import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Tester {
	private int truckCost, courierCost;
	private int truckFixed, truckVariable, numWarehouses, numItems, numCustomers;
	private int[] customerX, customerY, customerItem;
	private int[] warehouseX, warehouseY, warehouseItem, warehouseQuantity;
	private HashMap<Integer, Integer> warehouses = new HashMap<>();
	private HashMap<Integer, Integer> customers = new HashMap<>();

	private static int encode(int x, int y, int item) {
		return (item << 20) | (x << 10) | y;
	}

	private void incr(HashMap<Integer, Integer> map, int x, int y, int item, int qty) {
		int key = encode(x, y, item);
		if (map.containsKey(key)) {
			map.put(key, map.get(key) + qty);
		} else {
			map.put(key, qty);
		}
	}

	private void decr(HashMap<Integer, Integer> map, int x, int y, int item) {
		int key = encode(x, y, item);
		if (!map.containsKey(key)) {
			System.err.println(y+ " " + x + " " + item);
		}
		int v = map.get(key);
		if (v == 1) {
			map.remove(key);
		} else {
			map.put(key, v - 1);
		}
	}

	private void generateTestCase(long seed) {
		Random r = new Random(seed);
		try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			sr.setSeed(seed);
			r = sr;
		} catch (NoSuchAlgorithmException ex) {
		}
		truckFixed = r.nextInt(46) + 5;
		truckVariable = r.nextInt(20) + 1;
		if (1000 <= seed && seed < 2000) {
			truckVariable = (int) (seed - 1000) / 50 + 1;
		}
		numItems = r.nextInt(91) + 10;
		numCustomers = r.nextInt(981) + 20;
		customerX = new int[numCustomers];
		customerY = new int[numCustomers];
		customerItem = new int[numCustomers];
		int[] itemCount = new int[numItems];
		for (int i = 0; i < numCustomers; i++) {
			customerX[i] = r.nextInt(1001);
			customerY[i] = r.nextInt(1001);
			itemCount[customerItem[i] = r.nextInt(numItems)]++;
			incr(customers, customerX[i], customerY[i], customerItem[i], 1);
		}
		numWarehouses = r.nextInt(18) + 3;
		int[] wx = new int[numWarehouses];
		int[] wy = new int[numWarehouses];
		for (int i = 0; i < numWarehouses; i++) {
			wx[i] = r.nextInt(1001);
			wy[i] = r.nextInt(1001);
		}
		ArrayList<Integer> wwx = new ArrayList<>();
		ArrayList<Integer> wwy = new ArrayList<>();
		ArrayList<Integer> wwi = new ArrayList<>();
		ArrayList<Integer> wwq = new ArrayList<>();
		for (int i = 0; i < numItems; i++) {
			if (itemCount[i] == 0) continue;
			itemCount[i] += r.nextInt(1 + itemCount[i] / 2);
			int[] w = new int[3];
			w[0] = r.nextInt(numWarehouses);
			w[1] = r.nextInt(numWarehouses);
			w[2] = r.nextInt(numWarehouses);
			int[] c = new int[3];
			for (int j = 0; j < itemCount[i]; j++)
				c[r.nextInt(3)]++;
			for (int j = 0; j < 3; j++) {
				if (c[j] == 0) continue;
				wwx.add(wx[w[j]]);
				wwy.add(wy[w[j]]);
				wwi.add(i);
				wwq.add(c[j]);
				incr(warehouses, wx[w[j]], wy[w[j]], i, c[j]);
			}
		}
		warehouseX = new int[wwx.size()];
		warehouseY = new int[wwx.size()];
		warehouseItem = new int[wwx.size()];
		warehouseQuantity = new int[wwx.size()];
		for (int i = 0; i < wwx.size(); i++) {
			warehouseX[i] = wwx.get(i);
			warehouseY[i] = wwy.get(i);
			warehouseItem[i] = wwi.get(i);
			warehouseQuantity[i] = wwq.get(i);
		}
	}

	private void truck(int startX, int startY, int destX, int destY, int[] items) {
		for (int item : items) {
			decr(warehouses, startX, startY, item);
			incr(warehouses, destX, destY, item, 1);
		}
		truckCost += truckFixed + truckVariable * (Math.abs(destX - startX) + Math.abs(destY - startY));
	}

	private void courier(int startX, int startY, int destX, int destY, int item) {
		decr(warehouses, startX, startY, item);
		decr(customers, destX, destY, item);
		courierCost += Math.abs(destX - startX) + Math.abs(destY - startY);
	}

	private Result runTest(long seed) {
		generateTestCase(seed);
		Result res = new Result();
		res.seed = seed;
		res.W = numWarehouses;
		res.C = numCustomers;
		res.I = numItems;
		res.CF = truckFixed;
		res.CV = truckVariable;
		TrucksAndCouriers obj = new TrucksAndCouriers();
		long beforeTime = System.currentTimeMillis();
		String[] ret;
		try {
			ret = obj.planShipping(truckFixed, truckVariable, warehouseX, warehouseY,
					warehouseItem, warehouseQuantity, customerX, customerY, customerItem);
		} catch (Exception e) {
			res.elapsed = System.currentTimeMillis() - beforeTime;
			res.score = 99999999;
			e.printStackTrace();
			return res;
		}
		res.elapsed = System.currentTimeMillis() - beforeTime;
		for (String s : ret) {
			String[] t = s.split(",");
			if (!t[0].equals("C") && !t[0].equals("T")) {
				throw new RuntimeException("Invalid shipment type: " + t[0]);
			}
			if (t.length < 6) {
				throw new RuntimeException("No items specified in shipment: " + s);
			}
			if (t[0].equals("C") && t.length > 6) {
				throw new RuntimeException("Cannot ship multiple items with a courier: " + s);
			}
			int[] itemList = new int[t.length - 5];
			int startX = Integer.parseInt(t[1]);
			int startY = Integer.parseInt(t[2]);
			int endX = Integer.parseInt(t[3]);
			int endY = Integer.parseInt(t[4]);
			for (int i = 0; i < itemList.length; i++)
				itemList[i] = Integer.parseInt(t[i + 5]);
			if (t[0].equals("C")) {
				courier(startX, startY, endX, endY, itemList[0]);
			} else {
				truck(startX, startY, endX, endY, itemList);
			}
		}
		int customersLeft = customers.size();
		if (customersLeft > 0) {
			System.err.println("customersLeft:" + customersLeft);
		}
		res.score = truckCost + courierCost;
		res.st = truckCost;
		res.sc = courierCost;
		res.score += customersLeft * 10000;
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) {
		long seed = 1;
		long begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
		}
		if (begin != -1 && end != -1) {
			BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
			BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
			for (long i = begin; i <= end; ++i) {
				q.add(i);
			}
			Result[] results = new Result[(int) (end - begin + 1)];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i] = new TestThread(q, receiver);
				threads[i].start();
			}
			int printed = 0;
			try {
				for (int i = 0; i < (int) (end - begin + 1); i++) {
					Result res = receiver.poll(60, TimeUnit.SECONDS);
					results[(int) (res.seed - begin)] = res;
					for (; printed < results.length && results[printed] != null; printed++) {
						System.out.println(results[printed]);
						System.out.println();
					}
				}
			} catch (InterruptedException e) {
				for (int i = printed; i < results.length; i++) {
					System.out.println(results[i]);
					System.out.println();
				}
				e.printStackTrace();
			}

			double sum = 0;
			for (Result result : results) {
				sum += result.score;
			}
			System.out.println("ave:" + (sum / (end - begin + 1)));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	private static class TestThread extends Thread {
		BlockingQueue<Result> results;
		BlockingQueue<Long> q;

		TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
			this.q = q;
			this.results = results;
		}

		public void run() {
			while (true) {
				Long seed = q.poll();
				if (seed == null) break;
				try {
					Tester f = new Tester();
					Result res = f.runTest(seed);
					results.add(res);
				} catch (Exception e) {
					e.printStackTrace();
					Result res = new Result();
					res.seed = seed;
					results.add(res);
				}
			}
		}
	}
}

class Result {
	long seed;
	int W, C, I, CF, CV;
	int score, st, sc;
	long elapsed;

	public String toString() {
		String ret = String.format("seed:%4d\n", seed);
		ret += String.format("W:%2d C:%4d I:%3d CF:%2d CV:%2d\n", W, C, I, CF, CV);
		ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
		ret += String.format("score:%7d st:%6d sc:%6d", score, st, sc);
		return ret;
	}
}
