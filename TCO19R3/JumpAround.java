import java.io.*;
import java.util.*;

public final class JumpAround {
    private long startTime = System.currentTimeMillis();
    private static final long TL = 9600;
    private static final boolean DEBUG = false;
    private static final int INF = 1000000;
    private static final int[] DR = {0, 1, 0, -1, 0, 2, 0, -2};
    private static final int[] DC = {-1, 0, 1, 0, -2, 0, 2, 0};
    private static char[] MOVE_NAME = "LDRULDRU".toCharArray();
    private SplittableRandom rnd = new SplittableRandom(42);
    private int N, C;
    private double NP;
    private int[][] sposIdx, eposIdx;
    private Pos[] spos, epos;
    private boolean[][] wall, peg, pegOrig;
    private int[][] pathDir;
    private CountBuf bfsbuf;
    private IntQue q1, q2;

    String[] findSolution(int N, int NumPegs, char[] grid) {
        this.N = N;
        this.C = NumPegs;
        this.NP = 1.0 * C / (N * N);
        initialize(grid);
        int[][] distGraph = createDistGraph();
        int[] matching = createMatching(distGraph);
        long singleTime = 0;
        Result bestRes = new Result();
        bestRes.pena = INF;
        int[] bestMatching = matching;
        for (int turn = 1; ; turn++) {
            long beforeTime = System.currentTimeMillis();
            if (beforeTime - startTime + singleTime > 1000) {
                debugf("turn:%d singleTime:%d bestScore:%d\n", turn, singleTime, bestRes.score());
                break;
            }
            Result res = solve(matching, bestRes.score());
            if (res.score() < bestRes.score()) {
                bestRes = res;
                bestMatching = matching;
                debugf("score:%d at turn %d\n", res.score(), turn);
            }
            long afterTime = System.currentTimeMillis();
            singleTime = Math.max(singleTime, afterTime - beforeTime);
            if ((turn & 0xFF) == 0) {
                matching = createMatching(distGraph);
            }
        }
        int[][] near = new int[C][Math.min(C, 20)];
        ArrayList<ArrayList<Integer>> dists = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            dists.add(new ArrayList<>());
        }
        for (int i = 0; i < C; i++) {
            for (int j = 0; j < dists.size(); j++) {
                dists.get(j).clear();
            }
            near[i][0] = bestMatching[i];
            for (int j = 0; j < C; j++) {
                if (j == bestMatching[i]) continue;
                if (distGraph[i][j] <= dists.size()) {
                    dists.get(distGraph[i][j] - 1).add(j);
                }
            }
            int pos = 1;
            for (int j = 0; j < dists.size() && pos < near[0].length; j++) {
                for (int k = 0; k < dists.get(j).size() && pos < near[0].length; k++, pos++) {
                    near[i][pos] = dists.get(j).get(k);
                }
            }
            while (pos < near[0].length) {
                near[i][pos] = near[i][0];
                pos++;
            }
        }
        bestRes = improve(bestMatching, bestRes, near);
        return bestRes.output();
    }

    private void initialize(char[] grid) {
        bfsbuf = new CountBuf(N + 4);
        sposIdx = new int[N + 4][N + 4];
        eposIdx = new int[N + 4][N + 4];
        wall = new boolean[N + 4][N + 4];
        peg = new boolean[N + 4][N + 4];
        pegOrig = new boolean[N + 4][N + 4];
        pathDir = new int[N + 4][N + 4];
        q1 = new IntQue(N * N);
        q2 = new IntQue(N * N);
        for (int i = 0; i < N + 4; i++) {
            Arrays.fill(sposIdx[i], -1);
            Arrays.fill(eposIdx[i], -1);
            wall[i][0] = wall[i][1] = wall[i][N + 2] = wall[i][N + 3] = true;
        }
        Arrays.fill(wall[0], true);
        Arrays.fill(wall[1], true);
        Arrays.fill(wall[N + 2], true);
        Arrays.fill(wall[N + 3], true);
        spos = new Pos[C];
        epos = new Pos[C];
        int sidx = 0;
        int eidx = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                char cell = grid[i * N + j];
                if (cell == 'P') {
                    spos[sidx] = new Pos(i + 2, j + 2);
                    pegOrig[i + 2][j + 2] = true;
                    sposIdx[i + 2][j + 2] = sidx++;
                } else if (cell == 'X') {
                    epos[eidx] = new Pos(i + 2, j + 2);
                    eposIdx[i + 2][j + 2] = eidx++;
                } else if (cell == '#') {
                    wall[i + 2][j + 2] = true;
                }
            }
        }
    }

    private Result solve(int[] matching, int bestScore) {
        Result res = new Result();
        int[] order = new int[C];
        for (int i = 0; i < C; i++) {
            order[i] = i;
        }
        for (int i = 0; i < C - 1; i++) {
            int to = rnd.nextInt(C - i) + i;
            swap(order, i, to);
        }
        solveWithOrder(matching, order, res, bestScore);
        if (res.score() < bestScore) res.order = order;
        return res;
    }

    private Result improve(int[] matching, Result bestRes, int[][] near) {
        long singleTime = 0;
        int[] moveOrderF = new int[1];
        int[] moveOrderT = new int[moveOrderF.length];
        final boolean useMoveMatch = NP < 0.1 + (50 - N) * (0.15 / 20);
        int[] moveMatchF = new int[useMoveMatch ? 1 : 0];
        int[] moveMatchT = new int[moveMatchF.length];
        int[] matchRev = new int[C];
        for (int i = 0; i < C; i++) {
            matchRev[matching[i]] = i;
        }
        for (int turn = 1; ; turn++) {
            long beforeTime = System.currentTimeMillis();
            if (beforeTime - startTime + singleTime > TL) {
                debugf("turn:%d singleTime:%d bestScore:%d\n", turn, singleTime, bestRes.score());
                break;
            }
            int[] order = bestRes.order;
            for (int i = 0; i < moveOrderF.length; i++) {
                moveOrderF[i] = rnd.nextInt(C);
                moveOrderT[i] = rnd.nextInt(C - 1);
                if (moveOrderF[i] <= moveOrderT[i]) moveOrderT[i]++;
                swap(order, moveOrderF[i], moveOrderT[i]);
            }
            for (int i = 0; i < moveMatchF.length; i++) {
                moveMatchF[i] = rnd.nextInt(C);
                int ti = near[moveMatchF[i]][rnd.nextInt(near[0].length)];
                moveMatchT[i] = matchRev[ti];
                swap(matching, moveMatchF[i], moveMatchT[i]);
            }
            Result res = new Result();
            solveWithOrder(matching, order, res, bestRes.score());
            if (res.score() <= bestRes.score()) {
//                if (res.score() < bestRes.score()) {
//                    debugf("score:%d at turn %d\n", res.score(), turn);
//                }
                bestRes = res;
                bestRes.order = order;
                for (int i = 0; i < moveMatchF.length; i++) {
                    swap(matchRev, matching[moveMatchF[i]], matching[moveMatchT[i]]);
                }
            } else {
                for (int i = moveOrderF.length - 1; i >= 0; i--) {
                    swap(order, moveOrderF[i], moveOrderT[i]);
                }
                for (int i = moveMatchF.length - 1; i >= 0; i--) {
                    swap(matching, moveMatchF[i], moveMatchT[i]);
                }
            }
            long afterTime = System.currentTimeMillis();
            singleTime = Math.max(singleTime, afterTime - beforeTime);
        }
        return bestRes;
    }

    private void solveWithOrder(int[] matching, int[] order, Result res, int bestScore) {
        for (int i = 0; i < N; i++) {
            System.arraycopy(pegOrig[i + 2], 2, peg[i + 2], 2, N);
        }
        for (int i = 0; i < C; i++) {
            int si = order[i];
            int ei = matching[si];
            ArrayList<Integer> path = findPath(si, ei);
            if (path != null) {
                res.addMoves(spos[si], path);
                res.cost += path.size();
                for (int j = 0; j < path.size() - 1; j++) {
                    if (path.get(j) >= 4 && path.get(j + 1) >= 4) res.cost--;
                }
                assert (peg[spos[si].r][spos[si].c]);
                assert (!peg[epos[ei].r][epos[ei].c]);
                peg[spos[si].r][spos[si].c] = false;
                peg[epos[ei].r][epos[ei].c] = true;
            } else {
                res.pena += N * 20;
            }
            if (i < C - 1 && res.score() >= bestScore) {
                res.pena++;
                break;
            }
        }
    }

    private ArrayList<Integer> findPath(int si, int ei) {
        final int sr = spos[si].r;
        final int sc = spos[si].c;
        final int er = epos[ei].r;
        final int ec = epos[ei].c;
        q1.clear();
        q1.push((sr << 6) | sc);
        q2.clear();
        bfsbuf.clear();
        bfsbuf.set(sr, sc);
        pathDir[er][ec] = -1;
        BFS:
        while (q1.size() > 0 || q2.size() > 0) {
            final int stop = q1.tail;
            while (q2.size() > 0) {
                int cur = q2.pop();
                int cr = cur >> 6;
                int cc = cur & 0x3F;
                for (int i = 0; i < 4; i++) {
                    int nr = cr + DR[i];
                    int nc = cc + DC[i];
                    if (!wall[nr][nc] && !peg[nr][nc] && !bfsbuf.get(nr, nc)) {
                        bfsbuf.set(nr, nc);
                        pathDir[nr][nc] = i;
                        if (nr == er && nc == ec) {
                            break BFS;
                        }
                        q1.push((nr << 6) | nc);
                    }
                }
            }
            while (q1.head != stop) {
                int cur = q1.pop();
                int cr = cur >> 6;
                int cc = cur & 0x3F;
                for (int i = 0; i < 4; i++) {
                    int nr = cr + DR[i];
                    int nc = cc + DC[i];
                    if (!wall[nr][nc] && !peg[nr][nc]) {
                        if (!bfsbuf.get(nr, nc)) {
                            bfsbuf.set(nr, nc);
                            pathDir[nr][nc] = i;
                            if (nr == er && nc == ec) {
                                break BFS;
                            }
                            q1.push((nr << 6) | nc);
                        }
                    } else {
                        nr += DR[i];
                        nc += DC[i];
                        if (!wall[nr][nc] && !peg[nr][nc] && !bfsbuf.get(nr, nc)) {
                            bfsbuf.set(nr, nc);
                            pathDir[nr][nc] = i + 4;
                            if (nr == er && nc == ec) {
                                break BFS;
                            }
                            q2.push((nr << 6) | nc);
                        }
                    }
                }
            }
            for (int qi = q2.head; qi < q2.tail; qi++) {
                int cur = q2.buf[qi];
                int cr = cur >> 6;
                int cc = cur & 0x3F;
                for (int i = 0; i < 4; i++) {
                    int nr = cr + DR[i];
                    int nc = cc + DC[i];
                    if (!wall[nr][nc] && !peg[nr][nc]) continue;
                    nr += DR[i];
                    nc += DC[i];
                    if (!wall[nr][nc] && !peg[nr][nc] && !bfsbuf.get(nr, nc)) {
                        bfsbuf.set(nr, nc);
                        pathDir[nr][nc] = i + 4;
                        if (nr == er && nc == ec) {
                            break BFS;
                        }
                        q2.push((nr << 6) | nc);
                    }
                }
            }
        }
        if (pathDir[er][ec] == -1) {
            return null;
        }
        ArrayList<Integer> path = new ArrayList<>();
        int cr = er;
        int cc = ec;
        while (cr != sr || cc != sc) {
            assert (!peg[cr][cc]);
            final int dir = pathDir[cr][cc];
            path.add(dir);
            cr -= DR[dir];
            cc -= DC[dir];
        }
        Collections.reverse(path);
        return path;
    }

    private int[][] createDistGraph() {
        int[][] g = new int[C][C];
        for (int i = 0; i < C; i++) {
            calcDist(spos[i].r, spos[i].c, eposIdx, g[i]);
        }
        return g;
    }

    private int[] createMatching(int[][] g) {
        int[] s2e = new int[C];
        int[] e2s = new int[C];
        int[] order = new int[C];
        for (int i = 0; i < C; i++) {
            order[i] = i;
        }
        for (int i = 0; i < C - 1; i++) {
            int to = rnd.nextInt(C - i) + i;
            swap(order, i, to);
        }
        Arrays.fill(e2s, -1);
        int sum = 0;
        for (int i = 0; i < C; i++) {
            int min = INF + 1;
            int minI = 0;
            for (int j = 0; j < C; j++) {
                if (e2s[j] != -1) continue;
                if (g[order[i]][j] < min) {
                    min = g[order[i]][j];
                    minI = j;
                }
            }
            s2e[order[i]] = minI;
            e2s[minI] = order[i];
            sum += min;
        }
        double tempInv = 0.3;
        int si1 = 0;
        for (int i = 0; i < 1000000; i++) {
            int ei1 = s2e[si1];
            int ei2 = rnd.nextInt(C - 1);
            if (ei2 >= ei1) ei2++;
            int si2 = e2s[ei2];
            int diff = g[si1][ei2] + g[si2][ei1] - g[si1][ei1] - g[si2][ei2];
            if (transit(diff, tempInv + i * 0.00001)) {
                sum += diff;
                s2e[si1] = ei2;
                s2e[si2] = ei1;
                e2s[ei1] = si2;
                e2s[ei2] = si1;
            }
            si1++;
            if (si1 == C) si1 = 0;
        }
        debugf("cost:%d\n", sum);
        return s2e;
    }

    private boolean transit(int diff, double tempInv) {
        if (diff <= 0) return true;
        return rnd.nextDouble() < Math.exp(-diff * tempInv);
    }

    private void calcDist(int r, int c, int[][] target, int[] res) {
        Arrays.fill(res, INF);
        int dist = 1;
        int count = 0;
        q1.clear();
        q2.clear();
        q1.push((r << 6) | c);
        bfsbuf.clear();
        bfsbuf.set(r, c);
        while (count < Math.min(C, N * 5) && q1.size() > 0) {
            int stop = q1.tail;
            q2.clear();
            while (q1.head < stop) {
                int cur = q1.pop();
                int cr = cur >> 6;
                int cc = cur & 0x3F;
                for (int i = 0; i < 4; i++) {
                    int nr = cr + DR[i];
                    int nc = cc + DC[i];
                    if (!wall[nr][nc] && !bfsbuf.get(nr, nc)) {
                        bfsbuf.set(nr, nc);
                        q1.push((nr << 6) | nc);
                        if (target[nr][nc] != -1) {
                            res[target[nr][nc]] = dist;
                            count++;
                        }
                    }
                }
                for (int i = 0; i < 4; i++) {
                    int nr = cr + DR[i];
                    int nc = cc + DC[i];
                    if (!wall[nr][nc]) continue;
                    nr += DR[i];
                    nc += DC[i];
                    if (!wall[nr][nc] && !bfsbuf.get(nr, nc)) {
                        bfsbuf.set(nr, nc);
                        q1.push((nr << 6) | nc);
                        q2.push((nr << 6) | nc);
                        if (target[nr][nc] != -1) {
                            res[target[nr][nc]] = dist;
                            count++;
                        }
                    }
                }
            }
            while (q2.size() > 0) {
                int cur = q2.pop();
                int cr = cur >> 6;
                int cc = cur & 0x3F;
                for (int i = 0; i < 4; i++) {
                    int nr = cr + DR[i];
                    int nc = cc + DC[i];
                    if (!wall[nr][nc]) continue;
                    nr += DR[i];
                    nc += DC[i];
                    if (!wall[nr][nc] && !bfsbuf.get(nr, nc)) {
                        bfsbuf.set(nr, nc);
                        q1.push((nr << 6) | nc);
                        q2.push((nr << 6) | nc);
                        if (target[nr][nc] != -1) {
                            res[target[nr][nc]] = dist;
                            count++;
                        }
                    }
                }
            }
            dist++;
        }
    }

    private static void swap(int[] a, int f, int t) {
        int tmp = a[f];
        a[f] = a[t];
        a[t] = tmp;
    }

    static class Result {
        ArrayList<ArrayList<Integer>> moves = new ArrayList<>();
        ArrayList<Pos> pos = new ArrayList<>();
        int pena, cost;
        int[] order;

        int score() {
            return pena + cost;
        }

        void addMoves(Pos p, ArrayList<Integer> dirs) {
            moves.add(dirs);
            pos.add(p);
        }

        String[] output() {
            ArrayList<String> ret = new ArrayList<>();
            for (int i = 0; i < pos.size(); i++) {
                int cr = pos.get(i).r;
                int cc = pos.get(i).c;
                String s = (cr - 2) + " " + (cc - 2) + " ";
                ArrayList<Integer> ms = moves.get(i);
                int mi = 0;
                while (true) {
                    if (ms.get(mi) < 4) {
                        s += "S " + MOVE_NAME[ms.get(mi)];
                        cr += DR[ms.get(mi)];
                        cc += DC[ms.get(mi)];
                        mi++;
                    } else {
                        s += "J ";
                        do {
                            s += MOVE_NAME[ms.get(mi)];
                            cr += DR[ms.get(mi)];
                            cc += DC[ms.get(mi)];
                            mi++;
                        } while (mi < ms.size() && ms.get(mi) >= 4);
                    }
                    ret.add(s);
                    if (mi == ms.size()) break;
                    s = (cr - 2) + " " + (cc - 2) + " ";
                }
            }
            return ret.toArray(new String[0]);
        }
    }

    static class Pos {
        final int r, c;

        Pos(int r, int c) {
            this.r = r;
            this.c = c;
        }

        @Override
        public String toString() {
            return "Pos(" + r + "," + c + ')';
        }
    }

    static class IntQue {
        int head, tail;
        int[] buf;

        IntQue(int size) {
            buf = new int[size];
        }

        void push(int v) {
            buf[tail++] = v;
        }

        int pop() {
            return buf[head++];
        }

        int size() {
            return tail - head;
        }

        void clear() {
            head = tail = 0;
        }
    }

    static class CountBuf {
        int[][] count;
        int turn;

        CountBuf(int size) {
            count = new int[size][size];
        }

        void clear() {
            turn++;
        }

        boolean get(int r, int c) {
            return count[r][c] == turn;
        }

        void set(int r, int c) {
            count[r][c] = turn;
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.parseInt(br.readLine());
        int NumPegs = Integer.parseInt(br.readLine());
        char[] grid = new char[N * N];
        for (int i = 0; i < N * N; i++) grid[i] = br.readLine().charAt(0);
        JumpAround jp = new JumpAround();
        String[] ret = jp.findSolution(N, NumPegs, grid);
        PrintWriter writer = new PrintWriter(System.out);
        writer.println(ret.length);
        for (String s : ret) writer.println(s);
        writer.flush();
    }

    static void debug(String str) {
        if (DEBUG) System.err.println(str);
    }

    static void debugf(String format, Object... obj) {
        if (DEBUG) System.err.printf(format, obj);
    }

    static void debug(Object... obj) {
        if (DEBUG) System.err.println(Arrays.deepToString(obj));
    }
}