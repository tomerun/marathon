import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.SecureRandom;
import java.util.*;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;


class TestCase {
    private static final int minN = 5, maxN = 50;
    private static final double maxWallP = 0.5;
    private static final int[] dr = {0, 1, 0, -1};
    private static final int[] dc = {-1, 0, 1, 0};
    static final char EMPTY = '.';
    static final char WALL = '#';
    static final char TARGET = 'X';
    static final char PEG = 'P';

    //inputs
    int N;
    int NumPegs;
    double wallP;
    char[][] Grid;
    char[][] GridCur;

    //outputs
    int numMoves;
    int[] lastX;
    int[] lastY;

    private SecureRandom rnd;

    TestCase(long seed) throws Exception {
        rnd = SecureRandom.getInstance("SHA1PRNG");
        rnd.setSeed(seed);
        while (true) {
            N = rnd.nextInt(maxN - minN + 1) + minN;
            if (seed == 1) N = minN;
            if (seed == 2) N = maxN;
            NumPegs = rnd.nextInt(N * N / 4 - N + 1) + N;
            int[] ind = new int[N * N];
            for (int i = 0; i < ind.length; i++) ind[i] = i;
            shuffle(ind);
            wallP = rnd.nextDouble() * maxWallP;
            if (1000 <= seed && seed < 2000) {
                wallP = (seed - 1000 + 0.5) / 1000 * maxWallP;
            }
            Grid = new char[N][N];
            for (int i = 0; i < N * N; i++) {
                char c = '.';
                if (i < NumPegs) c = 'P';
                else if (i < 2 * NumPegs) c = 'X';
                else if (rnd.nextDouble() < wallP) c = '#';
                Grid[ind[i] / N][ind[i] % N] = c;
            }
            if (everythingReachable()) break;
        }
        //make a copy of the grid for making moves
        GridCur = new char[N][];
        for (int i = 0; i < N; i++) GridCur[i] = Grid[i].clone();
        numMoves = 0;
    }

    private void shuffle(int[] a) {
        for (int i = 0; i < a.length; i++) {
            int k = (int) (rnd.nextDouble() * (a.length - i) + i);
            int temp = a[i];
            a[i] = a[k];
            a[k] = temp;
        }
    }

    void doSlideMove(int x, int y, char chMove) {
        lastX = new int[2];
        lastY = new int[2];
        lastX[0] = x;
        lastY[0] = y;

        int dx = 0;
        int dy = 0;
        if (chMove == 'L') dx = -1;
        else if (chMove == 'R') dx = 1;
        else if (chMove == 'U') dy = -1;
        else if (chMove == 'D') dy = 1;
        if (dx == 0 && dy == 0) {
            throw new RuntimeException("ERROR: Invalid move character [" + chMove + "].");
        }

        int nx = x + dx;
        int ny = y + dy;
//        System.err.println("(" + y + "," + x + ") -> (" + ny + "," + nx + ")");
        if (inGrid(ny, nx)) {
            if (GridCur[ny][nx] != WALL && GridCur[ny][nx] != PEG) {
                lastX[1] = nx;
                lastY[1] = ny;
                GridCur[ny][nx] = PEG;
                GridCur[y][x] = EMPTY;
            } else {
                throw new RuntimeException("ERROR: Cannot do jump move in a slide sequence");
            }
        } else {
            throw new RuntimeException("ERROR: Trying to move outside the Grid.");
        }
        numMoves++;
    }

    void doJumpMove(int x, int y, String mv) {
        lastX = new int[mv.length() + 1];
        lastY = new int[mv.length() + 1];
        lastX[0] = x;
        lastY[0] = y;
        for (int i = 0; i < mv.length(); i++) {
            int dx = 0;
            int dy = 0;
            char chMove = mv.charAt(i);
            if (chMove == 'L') dx = -1;
            else if (chMove == 'R') dx = 1;
            else if (chMove == 'U') dy = -1;
            else if (chMove == 'D') dy = 1;
            if (dx == 0 && dy == 0) {
                throw new RuntimeException("ERROR: Invalid move character [" + chMove + "].");
            }

            int nx = x + dx;
            int ny = y + dy;
//            System.err.println("(" + y + "," + x + ") -> (" + (ny + dy) + "," + (nx + dx) + ")");
            if (inGrid(ny, nx)) {
                if (GridCur[ny][nx] != WALL && GridCur[ny][nx] != PEG) {
                    throw new RuntimeException("ERROR: Cannot do slide move in a jump sequence");
                } else {
                    int nx2 = nx + dx;
                    int ny2 = ny + dy;
                    if (inGrid(ny2, nx2)) {
                        if (GridCur[ny2][nx2] == PEG) {
                            throw new RuntimeException("ERROR: Trying to jump onto another peg.");
                        }
                        if (GridCur[ny2][nx2] == WALL) {
                            throw new RuntimeException("ERROR: Trying to jump onto to a wall.");
                        }
                        GridCur[y][x] = EMPTY;
                        GridCur[ny2][nx2] = PEG;
                        x = nx2;
                        y = ny2;
                        lastX[i + 1] = x;
                        lastY[i + 1] = y;
                    } else {
                        throw new RuntimeException("ERROR: Trying to jump outside the Grid.");
                    }
                }
            } else {
                throw new RuntimeException("ERROR: Trying to move outside the Grid.");
            }
        }
        numMoves++;
    }


    //returns false if some of the cells are not reachable, meaning the puzzle is possibly not solvable
    //otherwise returns true, which means the puzzle is definitely solvable
    private boolean everythingReachable() {
        //get starting non-wall location
        int startR = -1;
        int startC = -1;
        loop:
        for (int r = 0; r < N; r++)
            for (int c = 0; c < N; c++)
                if (Grid[r][c] != WALL) {
                    startR = r;
                    startC = c;
                    break loop;
                }

        List<Loc> Q = new ArrayList<>();
        Q.add(new Loc(startR, startC));
        boolean[][] seen = new boolean[N][N];
        while (Q.size() > 0) {
            Loc L = Q.remove(0);
            if (seen[L.r][L.c]) continue;

            seen[L.r][L.c] = true;

            for (int m = 0; m < dr.length; m++) {
                int r2 = L.r + dr[m];
                int c2 = L.c + dc[m];
                if (inGrid(r2, c2) && Grid[r2][c2] != WALL) Q.add(new Loc(r2, c2));

                int r3 = L.r + 2 * dr[m];
                int c3 = L.c + 2 * dc[m];
                if (inGrid(r3, c3) && Grid[r3][c3] != WALL) Q.add(new Loc(r3, c3));
            }
        }

        for (int r = 0; r < N; r++)
            for (int c = 0; c < N; c++)
                if (Grid[r][c] != WALL && !seen[r][c]) {
                    return false;
                }

        return true;
    }

    int countUncovered() {
        int count = 0;
        for (int r = 0; r < N; r++)
            for (int c = 0; c < N; c++)
                if (Grid[r][c] == TARGET && GridCur[r][c] != PEG)
                    count++;

        return count;
    }

    int score() {
        return numMoves + 2 * countUncovered() * N;
    }

    private boolean inGrid(int r, int c) {
        return (r >= 0 && r < N && c >= 0 && c < N);
    }
}

class Loc {
    int r;
    int c;

    Loc(int R, int C) {
        r = R;
        c = C;
    }
}

class Drawer extends JFrame {
    private static final int EXTRA_WIDTH = 200;
    private static final int EXTRA_HEIGHT = 50;
    private static Font font = new Font("Arial", Font.BOLD, 14);
    private TestCase tc;
    private int cellSize;
    private boolean[][] isWall, isTarget, isPeg;
    private int turn;
    ArrayList<State> states = new ArrayList<>();

    static class State {
        ArrayList<Loc> move = new ArrayList<>();
        int uncovered, score;

        State(int uncovered, int score) {
            this.uncovered = uncovered;
            this.score = score;
        }
    }

    class DrawerKeyListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_LEFT:
                    if (turn > 0) {
                        if (e.isMetaDown()) {
                            move(-states.size());
                        } else if (e.isShiftDown()) {
                            move(-10);
                        } else {
                            move(-1);
                        }
                        repaint();
                    }
                    break;
                case KeyEvent.VK_RIGHT:
                    if (turn < states.size() - 1) {
                        if (e.isMetaDown()) {
                            move(states.size());
                        } else if (e.isShiftDown()) {
                            move(10);
                        } else {
                            move(1);
                        }
                        repaint();
                    }
                    break;
            }
        }
    }

    class DrawerPanel extends JPanel {
        public void paint(Graphics g) {
            int startX = 10;
            int startY = 10;

            // background
            g.setColor(new Color(0xDDDDDD));
            g.fillRect(0, 0, tc.N * cellSize + 230, tc.N * cellSize + 40);
            g.setColor(Color.WHITE);
            g.fillRect(startX, startY, tc.N * cellSize, tc.N * cellSize);

            //paint thin lines between cells
            g.setColor(Color.BLACK);
            for (int i = 0; i <= tc.N; i++)
                g.drawLine(startX, startY + i * cellSize, startX + tc.N * cellSize, startY + i * cellSize);
            for (int i = 0; i <= tc.N; i++)
                g.drawLine(startX + i * cellSize, startY, startX + i * cellSize, startY + tc.N * cellSize);


            //draw grid contents
            for (int r = 0; r < tc.N; r++)
                for (int c = 0; c < tc.N; c++) {
                    if (isWall[r][c]) {
                        g.setColor(Color.GRAY);
                        g.fillRect(startX + c * cellSize + 1, startY + r * cellSize + 1, cellSize - 1, cellSize - 1);
                    }
                    if (isTarget[r][c]) {
                        g.setColor(Color.GREEN);
                        g.fillRect(startX + c * cellSize + 1, startY + r * cellSize + 1, cellSize - 1, cellSize - 1);
                    }
                    if (isPeg[r][c]) {
                        g.setColor(Color.BLUE);
                        g.fillOval(startX + c * cellSize + cellSize / 4, startY + r * cellSize + cellSize / 4, cellSize / 2, cellSize / 2);
                    }
                }

            State state = states.get(turn);
            if (!state.move.isEmpty()) {
                g.setColor(Color.RED);
                for (int i = 0; i < state.move.size() - 1; i++) {
                    Loc l1 = state.move.get(i);
                    Loc l2 = state.move.get(i + 1);
                    g.drawLine(startX + l1.c * cellSize + cellSize / 2, startY + l1.r * cellSize + cellSize / 2,
                            startX + l2.c * cellSize + cellSize / 2, startY + l2.r * cellSize + cellSize / 2);
                }
            }

            g.setColor(Color.BLACK);
            g.setFont(font);
            g.drawString("Pegs " + tc.NumPegs, cellSize * tc.N + 25, 30);
            g.drawString("Uncovered " + state.uncovered, cellSize * tc.N + 25, 50);
            g.drawString("Moves " + turn, cellSize * tc.N + 25, 70);
            g.drawString("SCORE " + state.score, cellSize * tc.N + 25, 120);
        }
    }

    private void move(int diff) {
        if (diff > 0) {
            for (int i = 0; i < diff && turn < states.size() - 1; ++i) {
                turn++;
                State s = states.get(turn);
                Loc l1 = s.move.get(0);
                Loc l2 = s.move.get(s.move.size() - 1);
                isPeg[l1.r][l1.c] = false;
                isPeg[l2.r][l2.c] = true;
            }
        } else {
            for (int i = 0; i < -diff && turn > 0; ++i) {
                State s = states.get(turn);
                Loc l1 = s.move.get(0);
                Loc l2 = s.move.get(s.move.size() - 1);
                isPeg[l1.r][l1.c] = true;
                isPeg[l2.r][l2.c] = false;
                turn--;
            }
        }
    }

    class DrawerWindowListener extends WindowAdapter {
        public void windowClosing(WindowEvent event) {
            System.exit(0);
        }
    }

    Drawer(TestCase tc_, int cellSize) {
        DrawerPanel panel = new DrawerPanel();
        getContentPane().add(panel);
        addWindowListener(new DrawerWindowListener());
        this.tc = tc_;
        this.cellSize = cellSize;
        this.isWall = new boolean[tc.N][tc.N];
        this.isTarget = new boolean[tc.N][tc.N];
        this.isPeg = new boolean[tc.N][tc.N];
        for (int i = 0; i < tc.N; i++) {
            for (int j = 0; j < tc.N; j++) {
                isWall[i][j] = tc.Grid[i][j] == TestCase.WALL;
                isTarget[i][j] = tc.Grid[i][j] == TestCase.TARGET;
                isPeg[i][j] = tc.Grid[i][j] == TestCase.PEG;
            }
        }
        State firstState = new State(tc.countUncovered(), tc.score());
        this.states.add(firstState);
        int width = cellSize * tc.N + EXTRA_WIDTH;
        int height = cellSize * tc.N + EXTRA_HEIGHT;
        addKeyListener(new DrawerKeyListener());
        setSize(width, height);
        setTitle("Visualizer tool for problem JumpAround");
        setResizable(false);
    }
}


public class Tester {
    private static boolean vis;
    private static int cellSize = 20;
    private static final int THREAD_COUNT = 2;

    private Result runTest(long seed) throws Exception {
        TestCase tc = new TestCase(seed);
        Result res = new Result();
        res.seed = seed;
        res.N = tc.N;
        res.C = tc.NumPegs;
        res.P = tc.wallP;

        char[] grid1D = new char[tc.N * tc.N];
        for (int i = 0; i < tc.N; i++) {
            for (int j = 0; j < tc.N; j++) {
                grid1D[i * tc.N + j] = tc.Grid[i][j];
            }
        }
        JumpAround solution = new JumpAround();
        long startTime = System.currentTimeMillis();
        String[] allMoves = solution.findSolution(tc.N, tc.NumPegs, grid1D);
        res.elapsed = System.currentTimeMillis() - startTime;
        int numMoves = allMoves.length;

        Drawer drawer = null;
        if (vis) {
            drawer = new Drawer(tc, cellSize);
        }

        if (numMoves > tc.N * tc.NumPegs) {
            throw new RuntimeException("ERROR: Return array is too large.");
        }
        for (int i = 0; i < numMoves; i++) {
            String smove = allMoves[i];
            String[] s = smove.split(" ");
            if (s.length != 4) {
                throw new RuntimeException("ERROR: The move command with index " + i + " does not contain 4 space separated values. Value is [" + smove + "]");
            }
            int y = Integer.parseInt(s[0]);
            int x = Integer.parseInt(s[1]);
            if (x < 0 || x >= tc.N || y < 0 || y >= tc.N) {
                throw new RuntimeException("ERROR: (" + y + "," + x + ") outside of bounds.");
            }
            if (tc.GridCur[y][x] != TestCase.PEG) {
                throw new RuntimeException("ERROR: (" + y + "," + x + ") does not contain a peg.");
            }
            if (s[2].equals("S")) {
                if (s[3].length() > 1) {
                    throw new RuntimeException("ERROR: slide move has too many instructions: " + s[3]);
                }
                tc.doSlideMove(x, y, s[3].charAt(0));
            } else if (s[2].equals("J")) {
                tc.doJumpMove(x, y, s[3]);
            } else {
                throw new RuntimeException("ERROR: unrecognised move type: " + s[2]);
            }
            if (vis) {
                Drawer.State state = new Drawer.State(tc.countUncovered(), tc.score());
                for (int j = 0; j < tc.lastX.length; ++j) {
                    state.move.add(new Loc(tc.lastY[j], tc.lastX[j]));
                }
                drawer.states.add(state);
            }
        }
        if (vis) {
            drawer.setVisible(true);
        }
        res.score = tc.score();
        res.move = tc.numMoves;
        res.pena = res.score - res.move;
        return res;
    }

    public static void main(String[] args) throws Exception {
        long seed = 1, begin = -1, end = -1;
        for (int i = 0; i < args.length; i++)
            if (args[i].equals("-seed")) {
                seed = Long.parseLong(args[++i]);
            } else if (args[i].equals("-b")) {
                begin = Long.parseLong(args[++i]);
            } else if (args[i].equals("-e")) {
                end = Long.parseLong(args[++i]);
            } else if (args[i].equals("-vis")) {
                vis = true;
            } else if (args[i].equals("-sz")) {
                cellSize = Integer.parseInt(args[++i]);
            } else {
                System.out.println("WARNING: unknown argument " + args[i] + ".");
            }
        if (seed == 1) cellSize = 50;

        if (begin != -1 && end != -1) {
            BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
            BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
            for (long i = begin; i <= end; ++i) {
                q.add(i);
            }
            Result[] results = new Result[(int) (end - begin + 1)];
            TestThread[] threads = new TestThread[THREAD_COUNT];
            for (int i = 0; i < THREAD_COUNT; ++i) {
                threads[i] = new TestThread(q, receiver);
                threads[i].start();
            }
            int printed = 0;
            try {
                for (int i = 0; i < (int) (end - begin + 1); i++) {
                    Result res = receiver.poll(160, TimeUnit.SECONDS);
                    results[(int) (res.seed - begin)] = res;
                    for (; printed < results.length && results[printed] != null; printed++) {
                        System.out.println(results[printed]);
                        System.out.println();
                    }
                }
            } catch (InterruptedException e) {
                for (int i = printed; i < results.length; i++) {
                    System.out.println(results[i]);
                    System.out.println();
                }
                e.printStackTrace();
            }

            double sum = 0;
            for (Result result : results) {
                sum += result.score;
            }
            System.out.println("ave:" + (sum / (end - begin + 1)));
        } else {
            Tester tester = new Tester();
            Result res = tester.runTest(seed);
            System.out.println(res);
        }

    }

    private static class TestThread extends Thread {
        BlockingQueue<Result> results;
        BlockingQueue<Long> q;

        TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
            this.q = q;
            this.results = results;
        }

        public void run() {
            while (true) {
                Long seed = q.poll();
                if (seed == null) break;
                try {
                    Tester f = new Tester();
                    Result res = f.runTest(seed);
                    results.add(res);
                } catch (Exception e) {
                    e.printStackTrace();
                    Result res = new Result();
                    res.seed = seed;
                    results.add(res);
                }
            }
        }
    }

    static class Result {
        long seed;
        int N, C;
        double P;
        int score, move, pena;
        long elapsed;

        public String toString() {
            String ret = String.format("seed:%4d\n", seed);
            ret += String.format("N:%2d C:%4d P:%.4f\n", N, C, P);
            ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
            ret += String.format("score:%6d move:%5d pena:%d*%d*2=%d", score, move, pena / N / 2, N, pena);
            return ret;
        }
    }

}
