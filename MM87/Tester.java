/*
Change log
----------
2015-08-13 : Initial release
*/

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

// for compile
class Population {
	static int queryRegion(int x1, int y1, int x2, int y2) {
		return 0;
	}
}

public class Tester {
	final static int MIN_SZ = 50, MAX_SZ = 500;
	final static double BASE_PROB = 0.5;
	final static double CELL_PROB = 0.25;
	final static double SURROUND_PROB = 0.2;

	Random r;
	PopulationMapping solver;
	int width, height; // size of the map
	int total;
	boolean[][] land;
	int landArea = 0;
	String[] map;
	int[][] pop;
	int target;
	int[] centers = new int[0];
	int[] centersScale = new int[0];
	int maxpopoverall = 0;
	String[] returnMap;

	private int centerDistance(int y, int x) {
		int best = width + height;
		for (int i = 0; i < centers.length; i++) {
			int yy = centers[i] / 10000;
			int xx = centers[i] % 10000;
			best = Math.min(best, (Math.abs(y - yy) + Math.abs(x - xx)) / centersScale[i]);
		}
		return Math.max(best, 1);
	}

	private void generateMap() {
		int w = 6;
		int h = 6;
		land = new boolean[h][w];
		for (int i = 1; i < 5; i++)
			for (int j = 1; j < 5; j++)
				land[i][j] = r.nextDouble() < BASE_PROB;
		while (w < width || h < height) {
			boolean[][] newland = new boolean[h * 2][w * 2];
			for (int y = 0; y < h * 2; y++)
				for (int x = 0; x < w * 2; x++) {
					double prob = BASE_PROB + (land[y / 2][x / 2] ? CELL_PROB : -CELL_PROB);
					if (y % 2 == 0 && y > 0) prob += (land[y / 2 - 1][x / 2] ? SURROUND_PROB : -SURROUND_PROB);
					if (y % 2 == 1 && y < h - 1) prob += (land[y / 2 + 1][x / 2] ? SURROUND_PROB : -SURROUND_PROB);
					if (x % 2 == 0 && x > 0) prob += (land[y / 2][x / 2 - 1] ? SURROUND_PROB : -SURROUND_PROB);
					if (x % 2 == 1 && x < w - 1) prob += (land[y / 2][x / 2 + 1] ? SURROUND_PROB : -SURROUND_PROB);
					newland[y][x] = r.nextDouble() < prob;
				}
			land = newland;
			w *= 2;
			h *= 2;
		}
		map = new String[height];
		for (int y = 0; y < height; y++) {
			map[y] = "";
			for (int x = 0; x < width; x++)
				if (land[y][x]) {
					map[y] += "X";
					landArea++;
				} else {
					map[y] += ".";
				}
		}
	}

	private void generateCenters() {
		int numCenters = 5 + r.nextInt((width + height) / 100);
		centers = new int[numCenters];
		centersScale = new int[numCenters];
		for (int i = 0; i < numCenters; i++) {
			while (centers[i] == 0) {
				int y = r.nextInt(height);
				int x = r.nextInt(width);
				if (land[y][x]) {
					centers[i] = y * 10000 + x;
					centersScale[i] = 1 + r.nextInt(5);
				}
			}
		}
	}

	public void generateTestCase(long seed) {
		r = new Random(seed);
		width = r.nextInt(MAX_SZ - MIN_SZ + 1) + MIN_SZ;
		height = r.nextInt(MAX_SZ - MIN_SZ + 1) + MIN_SZ;
		if (seed < 3) {
			width = 50 + (int) seed * 10;
			height = 50 + (int) seed * 10;
		}
		pop = new int[height][width];
		generateMap();
		generateCenters();
		total = 0;
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
				if (land[y][x]) {
					int maxPop = (width + height) * 8 / centerDistance(y, x);
					total += pop[y][x] = r.nextInt(maxPop + 1);
					maxpopoverall = Math.max(maxpopoverall, pop[y][x]);
				}
		target = 97 - (int) Math.sqrt(r.nextInt(96 * 96));
		if (1000 <= seed && seed < 2000) {
			double move = 96 * 96 * ((2000 - seed) / 1000.0);
			target = 97 - (int) Math.sqrt(move);
		}
	}

	public Result runTest(long seed) {
		generateTestCase(seed);
		Result result = new Result();
		result.seed = seed;
		result.H = this.height;
		result.W = this.width;
		result.C = this.centers.length;
		result.T = this.target;
		result.totalLand = this.landArea;
		if (vis) {
			jf = new JFrame();
			v = new Vis();
			jf.getContentPane().add(v);
			returnMap = new String[0];
			jf.setSize(width * scale + 50, height * scale + 40);
			jf.setVisible(true);
			draw();
		}
		long startTime = System.currentTimeMillis();
		solver = new PopulationMapping();
		solver.population = this.pop;
		String[] ret = solver.mapPopulation(target, map, total);
		result.elapsed = System.currentTimeMillis() - startTime;
		if (ret.length != height) {
			addFatalError("Return array length must be the same as the initial map height.");
			return result;
		}
		for (int i = 0; i < height; i++) {
			if (ret[i].length() != width) {
				addFatalError("Element " + i + " of return array length must be the same as the initial map width.");
				return result;
			}
		}
		if (PopulationMapping.DEBUG && PopulationMapping.ENABLE_REV_ENG) {
			BufferedImage bi = new BufferedImage(width * scale + 50, height * scale + 1, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();
			Tester.this.paint(g2, solver.predictedPop, false);
			try {
				ImageIO.write(bi, "png", new File("predict.png"));
			} catch (Exception e) {}
		}
		if (vis) {
			returnMap = ret;
			try {
				Thread.sleep(50);
			} catch (Exception e) {
				e.printStackTrace();
			}
			draw();
		}
		long maxTarget = (long) target * total / 100;
		long counted = 0;
		for (int y = 0; y < ret.length; y++)
			for (int x = 0; x < ret[y].length(); x++) {
				if (ret[y].charAt(x) == 'X' && land[y][x]) {
					counted += pop[y][x];
					result.selectedArea++;
				}
			}
		if (counted > maxTarget) {
			addFatalError("Selected areas count too much population:" + counted + "/" + maxTarget);
			return result;
		}

		result.score = 1.0 * result.selectedArea * Math.pow(0.996, solver.queries.size());
		result.queryCount = solver.queries.size();
		result.bestQueryCount = solver.bestQueryCount;
		result.bestScore = solver.bestScore;

		if (PopulationMapping.DEBUG && PopulationMapping.ENABLE_REV_ENG) {
			for (int i = 0; i < centers.length; ++i) {
				System.err.print("(" + centers[i] / 10000 + "," + centers[i] % 10000 + ")[" + centersScale[i] + "], ");
			}
			System.err.println();
		}
		return result;
	}

	static int scale;
	static boolean vis;
	static int delay;
	JFrame jf;
	Vis v;

	void draw() {
		if (!vis) return;
		v.repaint();
		try {
			Thread.sleep(delay);
		} catch (Exception e) {}
	}

	void paint(Graphics2D g2, int[][] pop, boolean showQuery) {
		// background
		g2.setColor(new Color(0xEEEEEE));
		g2.fillRect(0, 0, width * scale + 150, height * scale + 1);
		g2.setColor(new Color(0xAAAAAA));
		g2.fillRect(0, 0, width * scale, height * scale);
		boolean highlight = showQuery && (returnMap.length > 0);
		// map cells: make oceans light-blue, population in shades of red, selected areas of population in green
		for (int i = 0; i < height; ++i)
			for (int j = 0; j < width; ++j) {
				if (!land[i][j]) {
					g2.setColor(new Color(0x00CCCC));
				} else {
					int mult = ((maxpopoverall - pop[i][j]) * 255) / maxpopoverall;
					if (mult > 255) mult = 255;
					int c;
					if (!highlight || returnMap[i].charAt(j) != 'X') {
						c = 0xFF0000 + 0x000101 * mult;
					} else {
						c = 0x770077 + 0x0100 * mult;
					}
					g2.setColor(new Color(c));
				}
				g2.fillRect(j * scale + 1, i * scale + 1, scale, scale);
			}

		if (solver != null && showQuery) {
			g2.setColor(new Color(0x00AAFF));
			for (PopulationMapping.Query q : solver.queries) {
				g2.drawRect(q.x1 * scale, q.y1 * scale, (q.x2 - q.x1 + 1) * scale + 1, (q.y2 - q.y1 + 1) * scale + 1);
			}
		}
	}

	public class Vis extends JPanel {
		boolean showQuery = true;

		public void paint(Graphics gr) {
			BufferedImage bi = new BufferedImage(width * scale + 150, height * scale + 1, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();
			Tester.this.paint(g2, pop, showQuery);
			gr.drawImage(bi, 0, 0, width * scale + 150, height * scale + 1, null);
		}

		public Vis() {
			jf.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
			jf.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_SPACE) {
						showQuery = !showQuery;
						repaint();
					}
				}
			});
			scale = Math.max(2, Math.min(1400 / width, 850 / height));
		}
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = -1, begin = -1, end = -1;
		vis = false;
		delay = 1000;
		scale = 2;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-delay")) delay = Integer.parseInt(args[++i]);
			if (args[i].equals("-scale")) scale = Integer.parseInt(args[++i]);
		}

		if (begin != -1 && end != -1) {
			for (int id = 0; id <= 0; ++id) {
//				PopulationMapping.percentage_mul = id * 0.1;
				try (PrintWriter writer = new PrintWriter(String.format("log/%02d.txt", id))) {
					ArrayList<Long> seeds = new ArrayList<Long>();
					for (long i = begin; i <= end; ++i) {
						seeds.add(i);
					}
					int len = seeds.size();
					Result[] results = new Result[len];
					TestThread[] threads = new TestThread[THREAD_COUNT];
					int prev = 0;
					for (int i = 0; i < THREAD_COUNT; ++i) {
						int next = len * (i + 1) / THREAD_COUNT;
						threads[i] = new TestThread(prev, next - 1, seeds, results);
						prev = next;
					}
					for (int i = 0; i < THREAD_COUNT; ++i) {
						threads[i].start();
					}
					for (int i = 0; i < THREAD_COUNT; ++i) {
						threads[i].join();
					}
					double sum = 0;
					for (int i = 0; i < results.length; ++i) {
						writer.println(results[i]);
						writer.println();
						sum += results[i].score;
					}
					writer.println("ave:" + (sum / results.length));
				}
			}
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int H, W, C, T;
		int totalLand, selectedArea, queryCount, bestQueryCount;
		double score, bestScore;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("H:%3d W:%3d C:%2d T:%2d\n", H, W, C, T);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("totalLand:%d selectedArea:%d queryCount:%d\n", totalLand, selectedArea, queryCount);
			ret += String.format("bestScore:%.4f bestQueryCount:%d\n", bestScore, bestQueryCount);
			ret += String.format("score:%.4f", score);
			return ret;
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}
}
