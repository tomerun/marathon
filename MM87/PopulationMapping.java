import java.util.*;
import java.util.function.IntBinaryOperator;

public final class PopulationMapping {

	private static final boolean EXTREME = false;
	private static final boolean SUBMIT = false;
	static final boolean ENABLE_REV_ENG = false;
	static final boolean DEBUG = false;
	private static final long TL = SUBMIT ? 19500 : 10000;
	private static final int MIN_TOWN = 5;
	private static final int MIN_SCALE = 1;
	private static final int MAX_SCALE = 5;
	private static final int START_PREDICT = 15;
	final long startTime = System.currentTimeMillis();
	Random rnd = new Random(42);
	int[][] population; // for local test
	int totalPoplation, totalLand;
	double maxPercentage;
	ArrayList<Query> queries = new ArrayList<>();
	int H, W;
	int threshold;
	boolean[][] land;
	int[][] sumLand;
	boolean[][] ans;
	double bestScore;
	int bestQueryCount;
	Comparator<Query> queryDensitySorter = (Query q1, Query q2) -> {
		return Double.compare(getDensity(q1), getDensity(q2));
	};
	Comparator<Query> queryPositionSorter = (Query q1, Query q2) -> {
		if (q1.x1 != q2.x1) return Integer.compare(q1.x1, q2.x1);
		if (q1.x2 != q2.x2) return Integer.compare(q1.x2, q2.x2);
		if (q1.y1 != q2.y1) return Integer.compare(q1.y1, q2.y1);
		return Integer.compare(q1.y2, q2.y2);
	};
	int[][] posToQuery;
	int[][] predictedPop;
	int[][] predictedPopSum;
	int[] landPos;
	double COOLING;

	String[] mapPopulation(int maxPercentage, String[] worldMap, int totalPopulation) {
		this.totalPoplation = totalPopulation;
		H = worldMap.length;
		W = worldMap[0].length();
		land = new boolean[H][W];
		ans = new boolean[H][W];
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				land[i][j] = worldMap[i].charAt(j) == 'X';
			}
		}
		sumLand = new int[H + 1][W + 1];
		for (int i = 1; i <= H; ++i) {
			for (int j = 1; j <= W; ++j) {
				sumLand[i][j] = land[i - 1][j - 1] ? 1 : 0;
				sumLand[i][j] += sumLand[i - 1][j] + sumLand[i][j - 1] - sumLand[i - 1][j - 1];
			}
		}
		threshold = (int) ((long) totalPopulation * maxPercentage / 100);
		this.maxPercentage = 1.0 * threshold / totalPopulation;
		totalLand = getLandCount(0, 0, W - 1, H - 1);
		landPos = new int[totalLand];
		int pos = 0;
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (land[i][j]) landPos[pos++] = (i << 10) + j;
			}
		}
		predictedPop = new int[H][W];
		predictedPopSum = new int[H + 1][W + 1];
		posToQuery = new int[H][W];
		debug("totalPop:" + totalPopulation + " totalLand:" + totalLand + " ave:" + (1. * totalPopulation / totalLand));

		State answer = solve();
		int usedPop = 0;
		for (Query q : answer.used) {
			addAnswer(q);
			usedPop += q.pop;
		}
		int extraLand = (threshold - usedPop) / ((H + W) * 8);
		debug();
		debug("threshold:" + threshold + " used:" + usedPop + " room:" + (threshold - usedPop) + " extraLand:" + extraLand);
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W && extraLand > 0; ++j) {
				if (land[i][j] && !ans[i][j]) {
					ans[i][j] = true;
					--extraLand;
				}
			}
		}
		if (!SUBMIT && DEBUG) buildOptimalSolution(false);
		String[] ret = new String[H];
		for (int i = 0; i < H; ++i) {
			char[] row = new char[W];
			for (int j = 0; j < W; ++j) {
				row[j] = ans[i][j] ? 'X' : '.';
			}
			ret[i] = String.valueOf(row);
		}
		return ret;
	}

	State solve() {
		final int BLOCK = 5;
		int total = 0;
		for (int i = 0; i < BLOCK; i++) {
			for (int j = 0; j < BLOCK; j++) {
				Query q = new Query(W * i / BLOCK, H * j / BLOCK, W * (i + 1) / BLOCK - 1, H * (j + 1) / BLOCK - 1);
				if (i == BLOCK - 1 && j == BLOCK - 1) {
					q.pop = totalPoplation - total;
					queries.add(q);
					break;
				}
				q.pop = query(q);
				total += q.pop;
			}
		}
		ArrayList<Query> used = new ArrayList<>();
		calculateBestAnswer(queries, used);
		State ans = new State();
		ans.used = used;


//		State ans = partitionLands();
		//		double score = calculateBestAnswer(qs, ans.qs);
		//		if (ENABLE_REV_ENG) {
		//			ArrayList<Query> extraAns = reverseEngineering(qs, score);
		//			if (extraAns != null) {
		//				ans.qs = extraAns;
		//				ans.use = false;
		//			}
		//		}
		return ans;
	}

	ArrayList<Query> reverseEngineering(ArrayList<Query> qs, double curScore) {
		//		ArrayList<Town> towns = predict(qs);
		//		debug("predicted towns:" + towns);
		ArrayList<Query> ret = null;
		//		ArrayList<Query> extraQuery = pickup(towns);
		//		if (extraQuery != null) {
		//			int sumLand = 0;
		//			for (Query q : extraQuery) {
		//				sumLand += getLandCount(q);
		//			}
		//			double vScore = calcScore(totalLand - sumLand, qs.size() - 1 + extraQuery.size());
		//			debug("virtual score:" + vScore + " land:" + (totalLand - sumLand));
		//			if (vScore > curScore) {
		//				int sumPop = 0;
		//				for (Query q : extraQuery) {
		//					q.pop = query(q);
		//					sumPop += q.pop;
		//				}
		//				debug("picked up areas pop:" + sumPop);
		//				if (totalPoplation - sumPop <= threshold) {
		//					debug("SUCCEED");
		//					ret = extraQuery;
		//				} else {
		//					debug("FAILED");
		//				}
		//			}
		//		}
		return ret;
	}

	ArrayList<Query> pickup(ArrayList<Town> towns) {
		ArrayList<Query> qs = new ArrayList<Query>();
		int sumPop = 0;
		for (Town town : towns) {
			Query q = new Query(town.c, town.r, town.c, town.r);
			boolean dup = false;
			for (Query oq : qs) {
				if (oq.intersect(q)) {
					dup = true;
				}
			}
			if (!dup) {
				qs.add(q);
				sumPop += predictedPop[town.r][town.c];
			}
		}
		ArrayList<Query> bestQs = new ArrayList<>();
		final int targetPop = (int) ((totalPoplation - threshold) * 1.05);
		debug("targetPop:" + targetPop + "/" + (totalPoplation - threshold));
		int sumLand = qs.size();
		int minLand = totalLand;
		IntBinaryOperator calcScore = (int pop, int land) -> {
			return (pop < targetPop ? (pop - targetPop) * 1 : 0) - land;
		};
		int score = calcScore.applyAsInt(sumPop, sumLand);
		debug("start Pickup");
		for (int i = 0; i < 100000; ++i) {
			final int qi = rnd.nextInt(qs.size());
			Query cq = qs.get(qi);
			Query mq = cq.clone();
			int select = rnd.nextInt(8);
			if (select == 0) {
				if (mq.x1 > 0) mq.x1--;
			} else if (select == 1) {
				if (mq.x1 < mq.x2) mq.x1++;
			} else if (select == 2) {
				if (mq.y1 > 0) mq.y1--;
			} else if (select == 3) {
				if (mq.y1 < mq.y2) mq.y1++;
			} else if (select == 4) {
				if (mq.x2 < W - 1) mq.x2++;
			} else if (select == 5) {
				if (mq.x1 < mq.x2) mq.x2--;
			} else if (select == 6) {
				if (mq.y2 < H - 1) mq.y2++;
			} else if (select == 7) {
				if (mq.y1 < mq.y2) mq.y2--;
			}
			boolean dup = false;
			for (Query oq : qs) {
				if (oq != cq && oq.intersect(mq)) {
					dup = true;
					break;
				}
			}
			if (dup) continue;
			int oldPop = cq.getSum(predictedPopSum);
			int oldLand = getLandCount(cq);
			int newPop = mq.getSum(predictedPopSum);
			int newLand = getLandCount(mq);
			int newSumPop = sumPop - oldPop + newPop;
			int newSumLand = sumLand - oldLand + newLand;
			if (newSumPop > targetPop && newSumLand < minLand) {
				minLand = newSumLand;
				debug("turn:" + i + " minLand:" + newSumLand + " sumPop:" + newSumPop);
				bestQs.clear();
				bestQs.addAll(qs);
				bestQs.set(qi, mq);
			}
			int newScore = calcScore.applyAsInt(newSumPop, newSumLand);
			if (newScore >= score) {
				score = newScore;
				sumPop = newSumPop;
				sumLand = newSumLand;
				qs.set(qi, mq);
			}
		}
		if (bestQs.isEmpty()) return null;
		return bestQs;
	}

	ArrayList<Town> predict(ArrayList<Query> qs, ArrayList<Town> towns, long timeLimit) {
		for (int i = 0; i < qs.size(); ++i) {
			Query q = qs.get(i);
			for (int j = q.y1; j <= q.y2; ++j) {
				for (int k = q.x1; k <= q.x2; ++k) {
					posToQuery[j][k] = i;
				}
			}
		}
		ArrayList<Town> bestTowns = simulatedAnnealing(qs, towns, timeLimit);
		final int MAX_POP = (W + H) * 8;
		for (int i = 0; i < landPos.length; ++i) {
			int r = landPos[i] >> 10;
			int c = landPos[i] & 0x3FF;
			int dist = 999;
			for (Town town : bestTowns) {
				dist = Math.min(dist, (Math.abs(r - town.r) + Math.abs(c - town.c)) / town.scale);
			}
			if (dist == 0) dist = 1;
			predictedPop[r][c] = (MAX_POP / dist + 1) / 2;
		}
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				predictedPopSum[i + 1][j + 1] = predictedPop[i][j] + predictedPopSum[i][j + 1] + predictedPopSum[i + 1][j] - predictedPopSum[i][j];
			}
		}
		return bestTowns;
	}

	ArrayList<Town> simulatedAnnealing(ArrayList<Query> qs, ArrayList<Town> towns, long timeLimit) {
		final int MAX_TOWN = MIN_TOWN + (W + H) / 100 - 1;
		int MOVE_RANGE = (W + H) / 10;
		COOLING = 5;
		ArrayList<Town> bestTowns = new ArrayList<>(towns);
		double diff = calcDiff(qs, towns);
		double bestDiff = diff;
		int lastBestTurn = 0;
		debug("start prediction:" + diff + " tl:" + timeLimit);
		for (int turn = 0; ; ++turn) {
			if (System.currentTimeMillis() - startTime > timeLimit) {
				debug("total turn:" + turn);
				break;
			}
			if (turn - lastBestTurn > 300) {
				lastBestTurn = turn;
				COOLING *= 1.5;
				MOVE_RANGE = Math.max(5, (int) (MOVE_RANGE * 0.9));
				debug("turn:" + turn + " bestDiff:" + bestDiff + " COOLING:" + COOLING + " MOVE_RANGE:" + MOVE_RANGE);
				if (COOLING >= 1000) {
					debug("total turn:" + turn);
					break;
				} else if (COOLING > 50) {
					COOLING = 1000;
					MOVE_RANGE = 5;
				}
			}
			ArrayList<Town> nextTowns = new ArrayList<>(towns);
			int select = rnd.nextInt(16);
			boolean mutate = false;
			if (towns.size() < MAX_TOWN && select == 0) { // add new town
				for (int i = 0; i < 10; ++i) {
					int r = rnd.nextInt(H);
					int c = rnd.nextInt(W);
					if (land[r][c]) {
						nextTowns.add(new Town(r, c, (MIN_SCALE + MAX_SCALE) / 2));
						mutate = true;
						break;
					}
				}
			} else if (towns.size() > 1 && select == 1) { // remove a town
				int ti = rnd.nextInt(towns.size());
				nextTowns.remove(ti);
				mutate = true;
			} else if (select == 2) { // increase scale of a town
				int ti = rnd.nextInt(towns.size());
				if (nextTowns.get(ti).scale < MAX_SCALE) {
					Town newTown = nextTowns.get(ti).clone();
					newTown.scale++;
					nextTowns.set(ti, newTown);
					mutate = true;
				}
			} else if (select == 3) { // decrease scale of a town
				int ti = rnd.nextInt(towns.size());
				if (nextTowns.get(ti).scale > MIN_SCALE) {
					Town newTown = nextTowns.get(ti).clone();
					newTown.scale--;
					nextTowns.set(ti, newTown);
					mutate = true;
				}
			} else { // move a town
				for (int i = 0; i < 10; ++i) {
					int ti = rnd.nextInt(towns.size());
					int dr = rnd.nextInt(MOVE_RANGE * 2 + 1) - MOVE_RANGE;
					int dc = rnd.nextInt(MOVE_RANGE * 2 + 1) - MOVE_RANGE;
					Town newTown = nextTowns.get(ti).clone();
					newTown.r += dr;
					newTown.c += dc;
					if (newTown.r < 0) newTown.r = 0;
					if (newTown.r >= H) newTown.r = H - 1;
					if (newTown.c < 0) newTown.c = 0;
					if (newTown.c >= W) newTown.c = W - 1;
					if (land[newTown.r][newTown.c]) {
						nextTowns.set(ti, newTown);
						mutate = true;
						break;
					}
				}
			}
			if (!mutate) continue;
			double curDiff = calcDiff(qs, nextTowns);
			if (transit(diff, curDiff)) {
				diff = curDiff;
				towns = nextTowns;
				if (diff < bestDiff) {
					//					debug("turn:" + turn + " diff:" + curDiff);
					bestDiff = diff;
					lastBestTurn = turn;
					bestTowns.clear();
					for (Town t : towns) {
						bestTowns.add(t.clone());
					}
				}
			}
		}
		return bestTowns;
	}

	double calcDiff(ArrayList<Query> qs, ArrayList<Town> towns) {
		final int MAX_POP = (W + H) * 8;
		int[] sum = new int[qs.size()];
		for (int i = 0; i < landPos.length; ++i) {
			int r = landPos[i] >> 10;
			int c = landPos[i] & 0x3FF;
			int dist = 999;
			for (Town town : towns) {
				dist = Math.min(dist, (Math.abs(r - town.r) + Math.abs(c - town.c)) / town.scale);
			}
			if (dist == 0) dist = 1;
			sum[posToQuery[r][c]] += MAX_POP / dist;
		}
		double ret = 0;
		for (int i = 0; i < qs.size(); ++i) {
			Query q = qs.get(i);
			ret += sq(Math.abs(sum[i] / 2.0 - q.pop) / q.pop);
		}
		return ret;
	}

	boolean transit(double prev, double next) {
		if (next <= prev) return true;
		return rnd.nextDouble() < Math.exp((prev - next) / prev * COOLING);
	}

	static double sq(double v) {
		return v * v;
	}

	static class Town {
		int r, c, scale;

		public Town(int r, int c, int scale) {
			this.r = r;
			this.c = c;
			this.scale = scale;
		}

		@Override
		public Town clone() {
			return new Town(this.r, this.c, this.scale);
		}

		@Override
		public String toString() {
			return "(" + r + "," + c + ")[" + scale + "]";
		}
	}

	double calculateBestAnswer(ArrayList<Query> qs, ArrayList<Query> used) {
		int[] landCount = new int[qs.size()];
		for (int i = 0; i < qs.size(); ++i) {
			landCount[i] = getLandCount(qs.get(i));
		}
		int[] dp = new int[totalLand + 1];
		int[] lastUsed = new int[dp.length];
		int[][] prev = new int[qs.size()][dp.length];
		final int INF = 1 << 29;
		Arrays.fill(dp, INF);
		Arrays.fill(lastUsed, -1);
		for (int[] a : prev) {
			Arrays.fill(a, -1);
		}
		dp[0] = 0;
		lastUsed[0] = -2;
		for (int i = 0; i < qs.size(); ++i) {
			for (int j = dp.length - 1 - landCount[i]; j >= 0; --j) {
				if (dp[j] + qs.get(i).pop < dp[j + landCount[i]]) {
					assert (lastUsed[j] != -1);
					dp[j + landCount[i]] = dp[j] + qs.get(i).pop;
					prev[i][j + landCount[i]] = lastUsed[j];
					lastUsed[j + landCount[i]] = i;
				}
			}
		}
		double score = 0;
		for (int i = dp.length - 1; i >= 0; --i) {
			if (dp[i] > threshold) continue;
			score = calcScore(i, qs.size() - 1);
			if (score > bestScore) {
				bestQueryCount = qs.size() - 1;
				bestScore = score;
			}
			debug(String.format("queries:%2d pop:%8d land:%5d den:%.4f score:%.4f %s", qs.size(), dp[i], i, 1.0 * dp[i] / i, score,
					(score < bestScore * 0.99 ? "*" : "")));
			int pos = lastUsed[i];
			int land = i;
			while (land > 0) {
				Query q = qs.get(pos);
				used.add(q);
				int nextPos = prev[pos][land];
				land -= landCount[pos];
				pos = nextPos;
			}
			assert (land == 0);
			break;
		}
		return score;
	}

	double calculateBestScore(ArrayList<Query> qs) {
		int[] dp = new int[totalLand + 1];
		final int INF = 1 << 29;
		Arrays.fill(dp, INF);
		dp[0] = 0;
		for (int i = 0; i < qs.size(); ++i) {
			final int landCount = getLandCount(qs.get(i));
			for (int j = dp.length - 1 - landCount; j >= 0; --j) {
				if (dp[j] + qs.get(i).pop < dp[j + landCount]) {
					dp[j + landCount] = dp[j] + qs.get(i).pop;
				}
			}
		}
		double score = 0;
		for (int i = dp.length - 1; i >= 0; --i) {
			if (dp[i] > threshold) continue;
			score = calcScore(i, qs.size() - 1);
			break;
		}
		return score;
	}

	State partitionLands() {
		final int MAX_PART_COUNT = EXTREME ? 300 : 30;
		final int[] optimalCheckTurns = {12, 12, 12, 12, 11, 9, 8, 7, 6, 5};
		final int optimalCheckTurn = optimalCheckTurns[(int) (maxPercentage * 10)];
		State state = new State();
		state.qs.add(new Query(0, 0, W - 1, H - 1, totalPoplation));
		for (int i = 0; i < MAX_PART_COUNT; ++i) {
			int optimalLand = calculateHypotheticalLand(state.qs);
			debug("optimalLand:" + optimalLand);
			if (i > optimalCheckTurn && calcScore(optimalLand, i + 1) <= bestScore) {
				debug("optimal score " + calcScore(optimalLand, i + 1) + " is below the bestScore " + bestScore);
				break;
			}

			Partition nextPart = selectNextPart(state);
			if (nextPart == null) break;
			debugOutputQueries(state.qs, state.used, state.added, nextPart.q);
			debug("");
			state.doPartition(nextPart);
		}
		debug("---");
		debugOutputQueries(state.qs, state.used, state.added, null);
		if (!state.towns.isEmpty()) {
			debug("predicted towns:" + state.towns);
		}
		return state;
	}

	int calculateHypotheticalLand(ArrayList<Query> qs) {
		Query[] qlist = qs.toArray(new Query[0]);
		Arrays.sort(qlist, queryDensitySorter);
		int optimalLand = 0;
		int optimalPop = 0;
		for (Query q : qlist) {
			if (optimalPop + q.pop <= threshold) {
				optimalLand += getLandCount(q);
				optimalPop += q.pop;
			} else {
				optimalLand += (int) ((long) getLandCount(q) * (threshold - optimalPop) / q.pop);
				break;
			}
		}
		return optimalLand;
	}

	Partition selectNextPart(State state) {
		if (!ENABLE_REV_ENG || state.qs.size() < START_PREDICT) {
			return selectNextPartFirst(state);
		} else {
			Partition part = selectNextPartSecond(state);
			return part == null ? selectNextPartFirst(state) : part;
		}
	}

	Partition selectNextPartFirst(State state) {
		final double maxDensityRatio = Math.max(3.0, 2.0 * threshold / totalPoplation);
		Partition nextPart = null;
		ArrayList<Partition> parts = new ArrayList<>();
		Query[] qs = state.qs.toArray(new Query[0]);
		Arrays.sort(qs, queryDensitySorter);
		Query firstNotUsed = null;
		for (Query q : qs) {
			if (!state.used.contains(q) && firstNotUsed == null) {
				firstNotUsed = q;
			}
			if (firstNotUsed != null) {
				Partition part = new Partition(q, state.qs.size() - 1);
				if (part.pos != -1) parts.add(part);
			}
		}
		{
			double usedDensity = 1.0 * state.usedPop / state.usedLand;
			Query selectedUsed = null;
			double usedV = 0;
			for (Query q : state.used) {
				double v = getDensity(q) / usedDensity * q.pop / state.usedPop;
				if (v > usedV) {
					selectedUsed = q;
					usedV = v;
				}
			}
			debug("usedV:" + usedV);
			if (usedV > 0.5) {
				Partition part = new Partition(selectedUsed, state.qs.size() - 1);
				part.diff *= usedV;
				if (part.pos != -1) parts.add(part);
			}
		}
		for (Partition part : parts) {
			Query q = part.q;
			if (state.used.contains(q) && getDensity(q) < 1.0 * state.usedPop / state.usedLand) continue;
			if (state.qs.size() > 8 && !state.used.contains(q) && getDensity(q) > maxDensityRatio * state.usedPop / state.usedLand)
				continue;
			if (nextPart == null || part.diff < nextPart.diff) {
				nextPart = part;
			}
		}
		if (nextPart == null) {
			for (Partition part : parts) {
				Query q = part.q;
				if (state.used.contains(q)) continue;
				if (nextPart == null || getDensity(q) / Math.pow(q.pop, 0.3) < getDensity(nextPart.q) / Math.pow(nextPart.q.pop, 0.3)) {
					nextPart = part;
				}
			}
		}
		return nextPart;
	}

	Partition selectNextPartSecond(State state) {
		Partition bestPart = null;
		int bestScore = calculateHypotheticalLand(state.qs);
		for (int i = 0; i < state.qs.size(); ++i) {
			Query q1 = state.qs.get(i);
			ArrayList<Query> uqs = new ArrayList<>(state.qs);
			uqs.remove(q1);
			Partition part = new Partition(q1, state.qs.size() - 1);
			if (part.pos == -1) continue;
			Query[] parted = part.cut();
			parted[0].pop = parted[0].getSum(predictedPopSum);
			parted[1].pop = parted[1].getSum(predictedPopSum);
			uqs.add(parted[0]);
			uqs.add(parted[1]);
			int vs = calculateHypotheticalLand(uqs);
			//						double vs = calculateBestScore(uqs);
			if (vs > bestScore) {
				bestScore = vs;
				bestPart = part;
			}
			for (int j = i; j < uqs.size(); ++j) {
				Partition part2 = new Partition(uqs.get(j), state.qs.size());
				if (part2.pos == -1) continue;
				Query[] parted2 = part2.cut();
				parted2[0].pop = parted2[0].getSum(predictedPopSum);
				parted2[1].pop = parted2[1].getSum(predictedPopSum);
				ArrayList<Query> uuqs = new ArrayList<>(uqs);
				uuqs.remove(j);
				uuqs.add(parted2[0]);
				uuqs.add(parted2[0]);
				vs = calculateHypotheticalLand(uuqs);
				//							vs = calculateBestScore(uqs);
				if (vs > bestScore) {
					bestScore = vs;
					bestPart = part;
				}
			}
		}
		final int PART = 6;
		for (int i = 0; i < state.qs.size(); ++i) {
			Query q = state.qs.get(i);
			ArrayList<Query> uqs = new ArrayList<>(state.qs);
			uqs.remove(q);
			uqs.add(null);
			uqs.add(null);

			// horz
			for (int j = 0; j < PART; ++j) {
				int pos = (q.x1 * j + (q.x2 + 1) * (PART - j)) / PART;
				if (pos <= q.x1 || q.x2 + 1 <= pos) continue;
				Query pq1 = new Query(q.x1, q.y1, pos - 1, q.y2);
				Query pq2 = new Query(pos, q.y1, q.x2, q.y2);
				pq1.pop = pq1.getSum(predictedPopSum);
				pq2.pop = pq2.getSum(predictedPopSum);
				uqs.set(uqs.size() - 2, pq1);
				uqs.set(uqs.size() - 1, pq2);
				int vs = calculateHypotheticalLand(uqs);
				if (vs > bestScore) {
					bestScore = vs;
					bestPart = new Partition(q, pos, true);
				}
			}

			// vert
			for (int j = 0; j < PART; ++j) {
				int pos = (q.y1 * j + (q.y2 + 1) * (PART - j)) / PART;
				if (pos <= q.y1 || q.y2 + 1 <= pos) continue;
				Query pq1 = new Query(q.x1, q.y1, q.x2, pos - 1);
				Query pq2 = new Query(q.x1, pos, q.x2, q.y2);
				pq1.pop = pq1.getSum(predictedPopSum);
				pq2.pop = pq2.getSum(predictedPopSum);
				uqs.set(uqs.size() - 2, pq1);
				uqs.set(uqs.size() - 1, pq2);
				int vs = calculateHypotheticalLand(uqs);
				if (vs > bestScore) {
					bestScore = vs;
					bestPart = new Partition(q, pos, false);
				}
			}
		}
		debug("select best score:" + (bestPart == null ? -1 : bestScore));
		return bestPart;
	}

	void debugOutputQueries(ArrayList<Query> qs, ArrayList<Query> used, Query[] prev, Query next) {
		if (DEBUG) {
			Query[] qa = qs.toArray(new Query[0]);
			Arrays.sort(qa, queryDensitySorter);
			for (Query q : qa) {
				debug(String.format("query Pop:%7d Land:%5d den:%8.4f %s%s%s", q.pop, getLandCount(q), 1.0 * q.pop / getLandCount(q),
						used.contains(q) ? "*" : "", q == next ? "n" : "", q == prev[0] || q == prev[1] ? "p" : ""));
			}
		}
	}

	void addAnswer(Query q) {
		for (int i = q.y1; i <= q.y2; ++i) {
			for (int j = q.x1; j <= q.x2; ++j) {
				ans[i][j] ^= true;
			}
		}
	}

	int getLandCount(Query q) {
		return getLandCount(q.x1, q.y1, q.x2, q.y2);
	}

	// inclusive
	int getLandCount(int x1, int y1, int x2, int y2) {
		return sumLand[y2 + 1][x2 + 1] - sumLand[y2 + 1][x1] - sumLand[y1][x2 + 1] + sumLand[y1][x1];
	}

	double getDensity(Query q) {
		return 1.0 * q.pop / getLandCount(q);
	}

	void shrink(Query q) {
		while (q.x1 < q.x2 && getLandCount(q.x1, q.y1, q.x1, q.y2) == 0) {
			q.x1++;
		}
		while (q.x1 < q.x2 && getLandCount(q.x2, q.y1, q.x2, q.y2) == 0) {
			q.x2--;
		}
		while (q.y1 < q.y2 && getLandCount(q.x1, q.y1, q.x2, q.y1) == 0) {
			q.y1++;
		}
		while (q.y1 < q.y2 && getLandCount(q.x1, q.y2, q.x2, q.y2) == 0) {
			q.y2--;
		}
	}

	final class Partition implements Comparable<Partition> {
		Query q;
		boolean horz;
		int pos = -1;
		double diff;

		Partition(Query q, int pos, boolean horz) {
			this.q = q;
			this.horz = horz;
			this.pos = pos;
		}

		Partition(Query q, int queryCount) {
			final int MIN_LAND_DIV = 140;
			final int CONSIDER_LAND_SIZE = 2;
			final double OFFSET = 0.2;
			this.q = q;
			int landCount = getLandCount(q);
			diff = Double.MAX_VALUE;
			final double percentage = (maxPercentage < 0.06 ? 0.2 : 1.2) * maxPercentage;

			final int MIN_LAND = totalLand / MIN_LAND_DIV;

			// horz
			if (q.x2 - q.x1 > 3) {
				long sum = 0;
				long sum2 = 0;
				for (int i = q.x1; i <= q.x2; ++i) {
					int c = getLandCount(i, q.y1, i, q.y2);
					sum += c * i;
					sum2 += c * i * i;
				}
				double ave = 1. * sum / landCount;
				double ave2 = 1. * sum2 / landCount;
				double totalVar = Math.sqrt(ave2 - ave * ave);

				long curSum = 0;
				long curSum2 = 0;
				int curCount = 0;
				for (int i = q.x1; i <= q.x2; ++i) {
					int c = getLandCount(i, q.y1, i, q.y2);
					curSum += c * i;
					curSum2 += c * i * i;
					curCount += c;
					if (i < q.x1 + (q.x2 - q.x1) * OFFSET) continue;
					if (i > q.x1 + (q.x2 - q.x1) * (1 - OFFSET)) continue;
					if (curCount < MIN_LAND) continue;
					if (curCount > landCount - MIN_LAND) break;
					double aveLeft = 1. * curSum / curCount;
					double ave2Left = 1. * curSum2 / curCount;
					double aveRight = 1. * (sum - curSum) / (landCount - curCount);
					double ave2Right = 1. * (sum2 - curSum2) / (landCount - curCount);
					double sumVar = (Math.sqrt(ave2Left - aveLeft * aveLeft) + Math.sqrt(ave2Right - aveRight * aveRight)) / 2;
					double curDiff = sumVar - totalVar;
					if (queryCount > CONSIDER_LAND_SIZE) {
						curDiff *= Math.pow(Math.min(curCount, landCount - curCount), percentage);
					}
					if (curDiff < diff) {
						diff = curDiff;
						horz = true;
						pos = i + 1;
					}
				}
			}

			// vert
			if (q.y2 - q.y1 > 3) {
				long sum = 0;
				long sum2 = 0;
				for (int i = q.y1; i <= q.y2; ++i) {
					int c = getLandCount(q.x1, i, q.x2, i);
					sum += c * i;
					sum2 += c * i * i;
				}
				double ave = 1. * sum / landCount;
				double ave2 = 1. * sum2 / landCount;
				double totalVar = Math.sqrt(ave2 - ave * ave);

				long curSum = 0;
				long curSum2 = 0;
				int curCount = 0;
				for (int i = q.y1; i <= q.y2; ++i) {
					int c = getLandCount(q.x1, i, q.x2, i);
					curSum += c * i;
					curSum2 += c * i * i;
					curCount += c;
					if (i < q.y1 + (q.y2 - q.y1) * OFFSET) continue;
					if (i > q.y1 + (q.y2 - q.y1) * (1 - OFFSET)) continue;
					if (curCount < MIN_LAND) continue;
					if (curCount > landCount - MIN_LAND) break;
					double aveTop = 1. * curSum / curCount;
					double ave2Top = 1. * curSum2 / curCount;
					double aveBottom = 1. * (sum - curSum) / (landCount - curCount);
					double ave2Bottom = 1. * (sum2 - curSum2) / (landCount - curCount);
					double sumVar = (Math.sqrt(ave2Top - aveTop * aveTop) + Math.sqrt(ave2Bottom - aveBottom * aveBottom)) / 2;
					double curDiff = sumVar - totalVar;
					if (queryCount > CONSIDER_LAND_SIZE) {
						curDiff *= Math.pow(Math.min(curCount, landCount - curCount), percentage);
					}
					if (curDiff < diff) {
						diff = curDiff;
						horz = false;
						pos = i + 1;
					}
				}
			}
			diff *= Math.pow(q.pop, percentage);
			//			debug(diff + " " + horz + " " + pos);
		}

		Query[] cut() {
			Query[] ret = new Query[2];
			if (horz) {
				ret[0] = new Query(q.x1, q.y1, pos - 1, q.y2);
				ret[1] = new Query(pos, q.y1, q.x2, q.y2);
			} else {
				ret[0] = new Query(q.x1, q.y1, q.x2, pos - 1);
				ret[1] = new Query(q.x1, pos, q.x2, q.y2);
			}
			shrink(ret[0]);
			shrink(ret[1]);
			return ret;
		}

		public int compareTo(Partition o) {
			return Double.compare(this.diff, o.diff);
		}
	}

	static final class Query {
		int x1, y1, x2, y2, pop; // coordinates are inclusive

		public Query(int x1, int y1, int x2, int y2) {
			this.x1 = Math.min(x1, x2);
			this.y1 = Math.min(y1, y2);
			this.x2 = Math.max(x1, x2);
			this.y2 = Math.max(y1, y2);
		}

		public Query(int x1, int y1, int x2, int y2, int pop) {
			this(x1, y1, x2, y2);
			this.pop = pop;
		}

		@Override
		public Query clone() {
			return new Query(this.x1, this.y1, this.x2, this.y2, this.pop);
		}

		@Override
		public int hashCode() {
			int result = ((x1 << 16) + x2);
			result = 31 * result + ((y1 << 16) + y2);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			Query other = (Query) obj;
			if (x1 != other.x1) return false;
			if (x2 != other.x2) return false;
			if (y1 != other.y1) return false;
			if (y2 != other.y2) return false;
			return true;
		}

		boolean intersect(Query other) {
			if (this.x2 < other.x1) return false;
			if (this.x1 > other.x2) return false;
			if (this.y2 < other.y1) return false;
			if (this.y1 > other.y2) return false;
			return true;
		}

		int getSum(int[][] sumTable) {
			return sumTable[y2 + 1][x2 + 1] - sumTable[y2 + 1][x1] - sumTable[y1][x2 + 1] + sumTable[y1][x1];
		}
	}

	final class State {
		ArrayList<Query> qs = new ArrayList<>();
		ArrayList<Query> used = new ArrayList<>();
		ArrayList<Town> towns = new ArrayList<>();
		Query[] added = new Query[2];
		double score;
		long usedPop;
		int usedLand;

		void doPartition(Partition part) {
			added = part.cut();
			qs.remove(part.q);
			added = part.cut();
			added[0].pop = query(added[0]);
			added[1].pop = part.q.pop - added[0].pop;
			for (int j = 0; j < 2; ++j) {
				qs.add(added[j]);
			}

			used.clear();
			score = calculateBestAnswer(qs, used);
			usedPop = 0;
			usedLand = 0;
			for (Query q : used) {
				usedPop += q.pop;
				usedLand += getLandCount(q);
			}
			if (ENABLE_REV_ENG) updatePrediction();
		}

		void updatePrediction() {
			if (qs.size() < START_PREDICT) return;
			if (qs.size() == START_PREDICT) {
				while (towns.size() < MIN_TOWN) {
					int r = rnd.nextInt(H);
					int c = rnd.nextInt(W);
					if (land[r][c]) {
						towns.add(new Town(r, c, (MIN_SCALE + MAX_SCALE) / 2));
					}
				}
			}
			long timeLimit;
			final long elapsed = System.currentTimeMillis() - startTime;
			if (qs.size() == START_PREDICT) {
				timeLimit = (TL - elapsed) / 2;
			} else {
				timeLimit = Math.min(TL / 15, (TL - elapsed) / 2);
			}
			this.towns = predict(this.qs, this.towns, timeLimit + elapsed);
		}
	}

	int query(Query q) {
		queries.add(q);
		if (SUBMIT) {
			return Population.queryRegion(q.x1, q.y1, q.x2, q.y2);
		} else {
			int ret = 0;
			for (int i = q.y1; i <= q.y2; ++i) {
				for (int j = q.x1; j <= q.x2; ++j) {
					ret += population[i][j];
				}
			}
			return ret;
		}
	}

	static double calcScore(int land, int queryCount) {
		return land * Math.pow(0.996, queryCount);
	}

	void buildOptimalSolution(boolean overwrite) {
		if (overwrite) {
			for (boolean[] a : ans) {
				Arrays.fill(a, false);
			}
		}
		final int STRIDE = 3;
		ArrayList<Long> pos = new ArrayList<>();
		for (int i = 0; i < H; i += STRIDE) {
			for (int j = 0; j < W; j += STRIDE) {
				int landNum = 0;
				int pop = 0;
				for (int k = 0; k < STRIDE && i + k < H; ++k) {
					for (int l = 0; l < STRIDE && j + l < W; ++l) {
						if (land[i + k][j + l]) {
							++landNum;
							pop += population[i + k][j + l];
						}
					}
				}
				if (landNum > 0) pos.add(((long) (pop / landNum) << 49) | ((long) pop << 32) | (i << 16) | j);
			}
		}
		Collections.sort(pos);
		int sum = 0;
		int count = 0;
		for (int i = 0; ; ++i) {
			int curPop = (int) (pos.get(i) >> 32) & 0x1FFFF;
			if (sum + curPop > threshold) break;
			sum += curPop;
			int r = (int) ((pos.get(i) >> 16) & 0xFFFF);
			int c = (int) (pos.get(i) & 0xFFFF);
			for (int j = 0; j < STRIDE && r + j < H; ++j) {
				for (int k = 0; k < STRIDE && c + k < W; ++k) {
					if (land[r + j][c + k]) ++count;
					if (overwrite) ans[r + j][c + k] = true;
				}
			}
		}
		debug("optimal land:" + count);
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}
}
