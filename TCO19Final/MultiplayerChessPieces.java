import java.io.*;
import java.util.*;

public final class MultiplayerChessPieces {

    private final long startTime = System.currentTimeMillis();
    private final static long TL = 5000;
    private final static boolean DEBUG = true;
    private final SplittableRandom rnd = new SplittableRandom(42);
    private final static char[] TYPE_CHARS = "QRBKN".toCharArray();
    private final static int EMPTY = -1;
    private final static int QUEEN = 0;
    private final static int ROOK = 1;
    private final static int BISHOP = 2;
    private final static int KING = 3;
    private final static int KNIGHT = 4;
    private final static int[][] DRS = {
            {1, 1, 1, 0, 0, -1, -1, -1},
            {0, 1, 0, -1},
            {1, 1, -1, -1},
            {1, 1, 1, 0, 0, -1, -1, -1},
            {2, 2, -2, -2, 1, 1, -1, -1}
    };
    private final static int[][] DCS = {
            {1, 0, -1, 1, -1, 1, 0, -1},
            {-1, 0, 1, 0},
            {1, -1, 1, -1},
            {1, 0, -1, 1, -1, 1, 0, -1},
            {1, -1, 1, -1, 2, -2, 2, -2}
    };
    private int N, C;
    private boolean[][] wall;
    private double[] points;
    private int[] pointsOrig;
    private int[][] types;
    private int[][] pid;
    private int[][][] attack;
    private int[] typesOrds = new int[5];
    private final static double INITIAL_COOLER = 0.01;
    private final static double FINAL_COOLER = 0.1;
    private double cooler = INITIAL_COOLER;
    private double INITIAL_RAW_SCORE_RATIO = 1.0;
    private double FINAL_RAW_SCORE_RATIO = 1.0;
    private double rawScoreRatio;
    private int[] centerR, centerC;
    private double revC;
    Imager imager = new Imager();

    public char[] findSolution(int N, int C, char[] grid, int[] points) {
        this.N = N;
        this.C = C;
        this.revC = 1.0 / C;
        if (C == 2) {
            INITIAL_RAW_SCORE_RATIO = 0.5;
            FINAL_RAW_SCORE_RATIO = 10.0;
        }
        this.pointsOrig = new int[]{points[1], points[2], points[3], points[0], points[4]};
        this.points = new double[this.pointsOrig.length];
        for (int i = 0; i < 5; i++) {
            typesOrds[i] = -((this.pointsOrig[i] << 8) | i);
        }
        Arrays.sort(typesOrds);
        for (int i = 0; i < 5; i++) {
            typesOrds[i] = (-typesOrds[i]) & 0xFF;
        }
        wall = new boolean[N + 4][N + 4];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (grid[i * N + j] == '#') {
                    wall[i + 2][j + 2] = true;
                }
            }
            wall[i + 2][0] = wall[i + 2][1] = wall[i + 2][N + 2] = wall[i + 2][N + 3] = true;
        }
        Arrays.fill(wall[0], true);
        Arrays.fill(wall[1], true);
        Arrays.fill(wall[N + 2], true);
        Arrays.fill(wall[N + 3], true);
        types = new int[N + 4][N + 4];
        pid = new int[N + 4][N + 4];
        attack = new int[C][N + 4][N + 4];
        centerR = new int[C];
        centerC = new int[C];
        final int multiStart = 1;
        State result = new State();
        result.score = -1;
        for (int i = 0; i < multiStart; i++) {
            for (int j = 0; j < N + 4; j++) {
                Arrays.fill(types[j], EMPTY);
            }
            initializeCenterPos();
//            if (DEBUG) {
//                char[][] cps = new char[N][N];
//                for (int j = 0; j < N; j++) {
//                    Arrays.fill(cps[j], '-');
//                }
//                for (int j = 0; j < C; j++) {
//                    cps[centerR[j] - 2][centerC[j] - 2] = (char) (j + '0');
//                }
//                for (int j = 0; j < N; j++) {
//                    debug(String.valueOf(cps[j]));
//                }
//            }
            State st = solve(TL * (i + 1) / multiStart);
            if (st.score > result.score) {
                result = st;
            }
        }

        char[] out = new char[2 * N * N];
        for (int r = 0, cur = 0; r < N; r++)
            for (int c = 0; c < N; c++, cur++) {
                if (!wall[r + 2][c + 2] && result.types[r + 2][c + 2] != EMPTY) {
                    out[cur] = TYPE_CHARS[result.types[r + 2][c + 2]];
                    out[cur + N * N] = (char) (result.pid[r + 2][c + 2] + '0');
                } else {
                    out[cur] = wall[r + 2][c + 2] ? '#' : '.';
                }
            }

        if (DEBUG) {
            debug("pid");
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    System.err.print(types[i + 2][j + 2] == -1 ? '-' : (char) ('0' + pid[i + 2][j + 2]));
                }
                System.err.println();
            }
            debug("type");
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    System.err.print(out[i * N + j]);
                }
                System.err.println();
            }
//            debug("attack");
//            for (int p = 0; p < C; p++) {
//                for (int i = 0; i < N; i++) {
//                    for (int j = 0; j < N; j++) {
//                        System.err.printf("%2d", attack[p][i + 2][j + 2]);
//                    }
//                    System.err.println();
//                }
//                System.err.println();
//            }
//            System.err.println();
        }
        return out;
    }

    void initializeCenterPos() {
//        if (C <= 4) {
//            int[] rs = {2, 2, N + 1, N + 1};
//            int[] cs = {2, N + 1, 2, N + 1};
//            for (int i = 0; i < C - 1; i++) {
//                int p = rnd.nextInt(C - i) + i;
//                swap(rs, i, p);
//                swap(cs, i, p);
//            }
//            for (int i = 0; i < C; i++) {
//                centerR[i] = rs[i];
//                centerC[i] = cs[i];
//            }
//            return;
//        } else if (C == 5) {
//            centerR = new int[]{2, 2, N + 1, N + 1, (N + 1) / 2};
//            centerC = new int[]{2, N + 1, 2, N + 1, (N + 1) / 2};
//            for (int i = 0; i < C - 1; i++) {
//                int p = rnd.nextInt(C - i) + i;
//                swap(centerR, i, p);
//                swap(centerC, i, p);
//            }
//            return;
//        } else if (C == 6) {
//            centerR = new int[]{2, 2, N + 1, N + 1, 0, 0};
//            centerC = new int[]{2, N + 1, 2, N + 1, 0, 0};
//            if (rnd.nextBoolean()) {
//                centerR[4] = centerR[5] = (N + 1) / 2;
//                centerC[4] = (N + 1) / 3;
//                centerC[5] = (N + 1) * 2 / 3;
//            } else {
//                centerC[4] = centerC[5] = (N + 1) / 2;
//                centerR[4] = (N + 1) / 3;
//                centerR[5] = (N + 1) * 2 / 3;
//            }
//            for (int i = 0; i < C - 1; i++) {
//                int p = rnd.nextInt(C - i) + i;
//                swap(centerR, i, p);
//                swap(centerC, i, p);
//            }
//            return;
//        }
        for (int i = 0; i < C; i++) {
            centerR[i] = rnd.nextInt(N) + 2;
            centerC[i] = rnd.nextInt(N) + 2;
        }
        int v = evalCenterPos();
        for (int i = 0; i < 2000; i++) {
            int p = i % C;
            int pr = centerR[p];
            int pc = centerC[p];
            if (i < 1000) {
                centerR[p] = rnd.nextInt(N) + 2;
                centerC[p] = rnd.nextInt(N) + 2;
            } else {
                centerR[p] += rnd.nextInt(5) - 2;
                centerC[p] += rnd.nextInt(5) - 2;
                if (centerR[p] < 2) centerR[p] = 2;
                if (centerR[p] > N + 1) centerR[p] = N + 1;
                if (centerC[p] < 2) centerC[p] = 2;
                if (centerC[p] > N + 1) centerC[p] = N + 1;
            }
            int nv = evalCenterPos();
            if (nv < v) {
                centerR[p] = pr;
                centerC[p] = pc;
            } else {
                v = nv;
            }
        }
    }

    int evalCenterPos() {
        int ret = Integer.MAX_VALUE;
        for (int i = 0; i < C; i++) {
            int edge = Math.min(Math.min(centerR[i] - 2, N + 1 - centerR[i]), Math.min(centerC[i] - 2, N + 1 - centerC[i]));
            ret = Math.min(ret, 4 * edge);
            for (int j = 0; j < C; j++) {
                if (j == i) continue;
                ret = Math.min(ret, Math.abs(centerR[i] - centerR[j]) + Math.abs(centerC[i] - centerC[j]));
            }
        }
        return ret;
    }

    State solve(long timelimit) {
        cooler = INITIAL_COOLER;
        rawScoreRatio = INITIAL_RAW_SCORE_RATIO;
        double[] scores = new double[C];
        int[] counts = new int[C];
        State bestState = new State();
        PosSet emptyPos = new PosSet(N + 4);
        PosSet placedPos = new PosSet(N + 4);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (!wall[i + 2][j + 2]) {
                    emptyPos.add(i + 2, j + 2);
                }
            }
        }
        double[] selProb = new double[C];
        double curScore = 0;
        long beginTime = System.currentTimeMillis();
        int bestCount = 0;
        for (int turn = 0; ; turn++) {
            if ((turn & 0x3FFF) == 0) {
                long curTime = System.currentTimeMillis();
                if (curTime - startTime > timelimit - 50) {
                    debugfln("turn:%d", turn);
                    break;
                }
                double ratio = 1.0 * (curTime - beginTime) / (timelimit - 50 - (beginTime - startTime));
                double c0 = Math.log(INITIAL_COOLER);
                double c1 = Math.log(FINAL_COOLER);
                cooler = Math.exp(c0 * (1 - ratio) + c1 * ratio);
                rawScoreRatio = INITIAL_RAW_SCORE_RATIO * (1 - ratio) + FINAL_RAW_SCORE_RATIO * ratio;
                for (int i = 0; i < points.length; i++) {
                    points[i] = ratio * pointsOrig[i];
                }
                Arrays.fill(scores, 0);
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        int t = types[i + 2][j + 2];
                        if (t != EMPTY) {
                            scores[pid[i + 2][j + 2]] += points[t];
                        }
                    }
                }
                curScore = evaluate(scores, counts);
            }
//            if ((rnd.nextInt() & 0x3FFF) == 0) {
//                int h = rnd.nextInt(3) + 1;
//                int w = rnd.nextInt(3) + 1;
//                int r = rnd.nextInt(N - h) + 2;
//                int c = rnd.nextInt(N - w) + 2;
//                int ps = 0;
//                for (int i = 0; i < h; i++) {
//                    for (int j = 0; j < w; j++) {
//                        final int t = types[r + i][c + j];
//                        if (t != EMPTY) {
//                            ps |= 1 << pid[r + i][c + j];
//                        }
//                    }
//                }
//                if (Integer.bitCount(ps) > 1) {
//                    for (int i = 0; i < h; i++) {
//                        for (int j = 0; j < w; j++) {
//                            final int t = types[r + i][c + j];
//                            if (t != EMPTY && canRemove(r + i, c + j)) {
//                                final int p = pid[r + i][c + j];
//                                scores[p] -= points[t];
//                                counts[p]--;
//                                remove(r + i, c + j);
//                                emptyPos.add(r + i, c + j);
//                                placedPos.remove(r + i, c + j);
//                            }
//                        }
//                    }
////                curScore = evaluate(scores, counts);
//                    continue;
//                }
//            }
            int np;
            if (placedPos.size == 0 || (emptyPos.size > 0 && (rnd.nextInt() & 0xF) != 0)) {
                np = emptyPos.get();
            } else {
                np = placedPos.get();
            }
            int r = np >> 8;
            int c = np & 0xFF;
            if (types[r][c] == EMPTY) {
                int p = selectPerson(r, c, selProb, scores);
                if (p < 0) continue;
//                int i = rnd.nextInt(5);
                for (int i = 0; i < 5; i++) {
                    if (canPlace(r, c, typesOrds[i], p)) {
                        scores[p] += points[typesOrds[i]];
                        counts[p]++;
                        double score = evaluate(scores, counts);
                        if (accept(score - curScore)) {
                            curScore = score;
                            place(r, c, typesOrds[i], p);
                            emptyPos.remove(r, c);
                            placedPos.add(r, c);
                            debug(1 + " " + turn + " " + Arrays.toString(scores));
                            double newScore = Integer.MAX_VALUE;
                            for (int j = 0; j < C; j++) {
                                newScore = Math.min(newScore, scores[j]);
                            }
                            if (newScore > bestState.score) {
                                debugfln("score:%.3f %.3f at turn %d", newScore, score, turn);
                                bestState.score = newScore;
                                copyToState(bestState);
                            }
                            break;
                        } else {
                            scores[p] -= points[typesOrds[i]];
                            counts[p]--;
                        }
                    }
                }
            } else {
                final int p = pid[r][c];
                final int t = types[r][c];
                boolean any = false;
                if (C >= 8) {
//                    int nt = rnd.nextInt(4);
//                    if (nt >= t) nt++;
                    for (int i = 0; i < 5; i++) {
                        int nt = typesOrds[i];
                        if (nt == t) break;
                        if (canPlace(r, c, nt, p)) {
                            scores[p] += points[nt] - points[t];
                            double score = evaluate(scores, counts);
                            if (accept(score - curScore)) {
                                curScore = score;
                                replace(r, c, nt);
                                debug(2 + " " + turn + " " + Arrays.toString(scores));
                                double newScore = Integer.MAX_VALUE;
                                for (int j = 0; j < C; j++) {
                                    newScore = Math.min(newScore, scores[j]);
                                }
                                if (newScore > bestState.score) {
                                    ++bestCount;
//                                    if ((bestCount & 0x3) == 0) {
//                                        Imager.createImage(wall, types, pid, pointsOrig, C);
//                                    }
                                    debugfln("score:%.3f %.3f at turn %d", newScore, score, turn);
                                    bestState.score = newScore;
                                    copyToState(bestState);
                                }
                                any = true;
                                break;
                            } else {
                                scores[p] -= points[nt] - points[t];
                            }
                        }
                    }
                }
                if (!any) {
                    // skip remove?
//                    boolean surrounded = true;
//                    for (int i = 0; i < DRS[KING].length; i++) {
//                        int nr = r + DRS[KING][i];
//                        int nc = c + DCS[KING][i];
//                        if (!wall[nr][nc] && (types[nr][nc] == EMPTY || (types[nr][nc] != EMPTY && pid[nr][nc] != p))) {
//                            surrounded = false;
//                            break;
//                        }
//                    }
                    if (canRemove(r, c)) {
                        scores[p] -= points[t];
                        counts[p]--;
                        double score = evaluate(scores, counts);
                        if (accept(score - curScore)) {
                            curScore = score;
                            remove(r, c);
                            emptyPos.add(r, c);
                            placedPos.remove(r, c);
                            debug(3 + " " + turn + " " + Arrays.toString(scores));
                        } else {
                            scores[p] += points[t];
                            counts[p]++;
                        }
                    }
                }
            }
//            if (!validate()) {
//                copyToState(bestState);
//                return bestState;
//            }
        }
        copyFromState(bestState);
        Arrays.fill(scores, 0);
//        Imager.createImage(wall, types, pid, pointsOrig, C);
        for (int i = 2; i < N + 2; i++) {
            for (int j = 2; j < N + 2; j++) {
                if (types[i][j] != EMPTY) {
                    scores[pid[i][j]] += points[types[i][j]];
                }
            }
        }
        for (int i = 2; i < N + 2; i++) {
            for (int j = 2; j < N + 2; j++) {
                if (wall[i][j]) continue;
                final int t = types[i][j];
                if (t == EMPTY) {
                    int p = selectPerson(i, j, selProb, scores);
                    if (p >= 0) {
                        for (int k = 0; k < 5; k++) {
                            int nt = typesOrds[k];
                            if (canPlace(i, j, nt, p)) {
                                scores[p] += points[nt];
                                place(i, j, nt, p);
                                break;
                            }
                        }
                    }
                } else {
                    for (int k = 0; k < 5; k++) {
                        final int p = pid[i][j];
                        final int nt = typesOrds[k];
                        if (nt == t) break;
                        if (canPlace(i, j, nt, p)) {
                            scores[p] += points[nt] - points[t];
                            remove(i, j);
                            place(i, j, nt, p);
                            break;
                        }
                    }
                }
            }
        }
//        Imager.createImage(wall, types, pid, pointsOrig, C);
        double newScore = Integer.MAX_VALUE;
        for (int i = 0; i < C; i++) {
            newScore = Math.min(newScore, scores[i]);
        }
        debugfln("score:%.3f at last phase", newScore);
        bestState.score = newScore;
        copyToState(bestState);
        return bestState;
    }

    int selectPerson(int r, int c, double[] selProb, double[] scores) {
        int p = -1;
        for (int i = 0; i < C; i++) {
//                    debugfln("attacke:%d %d", i, attack[i][r][c]);
            if (attack[i][r][c] > 0) {
                if (p != -1) {
                    return -2;
                }
                p = i;
            }
        }
        if (p == -1) {
            double minScore = 1e10;
            for (int i = 0; i < C; i++) {
                if (scores[i] < minScore) {
                    minScore = scores[i];
                    p = i;
                }
            }
//            double sum = 0;
//            for (int i = 0; i < C; i++) {
//                double dist = Math.abs(r - centerR[i]) + Math.abs(c - centerC[i]);
//                selProb[i] = 1.0 / Math.pow(1 + dist, 2);
//                sum += selProb[i];
//            }
//            p = C - 1;
//            double v = rnd.nextDouble(sum);
//            for (int i = 0; i < C; i++) {
//                v -= selProb[i];
//                if (v <= 0) {
//                    p = i;
//                    break;
//                }
//            }
        }
        return p;
    }

    void copyToState(State st) {
        for (int j = 2; j <= N + 1; j++) {
            System.arraycopy(types[j], 2, st.types[j], 2, N);
            System.arraycopy(pid[j], 2, st.pid[j], 2, N);
        }
        for (int j = 0; j < C; j++) {
            for (int k = 2; k <= N + 1; k++) {
                System.arraycopy(attack[j][k], 2, st.attack[j][k], 2, N);
            }
        }
    }

    void copyFromState(State st) {
        for (int j = 2; j <= N + 1; j++) {
            System.arraycopy(st.types[j], 2, types[j], 2, N);
            System.arraycopy(st.pid[j], 2, pid[j], 2, N);
        }
        for (int j = 0; j < C; j++) {
            for (int k = 2; k <= N + 1; k++) {
                System.arraycopy(st.attack[j][k], 2, attack[j][k], 2, N);
            }
        }
    }

    double evaluate(double[] scores, int[] counts) {
        double aveS = 0;
        double aveC = 0;
        double ret = Integer.MAX_VALUE;
        double max = 0;
        for (int i = 0; i < C; i++) {
            aveS += scores[i];
            aveC += counts[i];
            ret = Math.min(ret, scores[i]);
            max = Math.max(max, scores[i]);
        }
        ret *= rawScoreRatio; // raw score
        ret -= max * revC;
        debug("value:" + ret);
//        aveS *= revC;
//        aveC *= revC;
//        for (int i = 0; i < C; i++) {
//            ret -= 0.5 * revC * (scores[i] - aveS) * (scores[i] - aveS); // variance of scores
//            ret -= 4 * revC * (counts[i] - aveC) * (counts[i] - aveC); // variance of counts
//        }
        return ret;
    }

    boolean accept(double diff) {
        debug("diff:" + diff);
        if (diff >= 0) return true;
        double v = diff * cooler;
        if (v < -8) return false;
        return rnd.nextDouble() < Math.exp(v);
    }

    boolean canPlace(int r, int c, int t, int p) {
        if (t <= 2) {
            // sliding
            for (int i = 0; i < DRS[t].length; i++) {
                int dr = DRS[t][i];
                int dc = DCS[t][i];
                int nr = r + dr;
                int nc = c + dc;
                while (!wall[nr][nc]) {
                    if (types[nr][nc] != EMPTY) {
                        if (pid[nr][nc] != p) return false;
                        break;
                    }
                    nr += dr;
                    nc += dc;
                }
            }
        } else {
            for (int i = 0; i < DRS[t].length; i++) {
                int dr = DRS[t][i];
                int dc = DCS[t][i];
                int nr = r + dr;
                int nc = c + dc;
                if (types[nr][nc] != EMPTY && pid[nr][nc] != p) {
                    return false;
                }
            }
        }
        return true;
    }

    void place(int r, int c, int t, int p) {
//        debugfln("place r:%d c:%d t:%d p:%d", r - 2, c - 2, t, p);
        placeThis(r, c, t, p);
        if (attack[p][r][c] > 0) {
            block(r, c, p, ROOK);
            block(r, c, p, BISHOP);
        }
    }

    void placeThis(int r, int c, int t, int p) {
        types[r][c] = t;
        pid[r][c] = p;
        if (t <= 2) {
            // sliding
            for (int i = 0; i < DRS[t].length; i++) {
                int dr = DRS[t][i];
                int dc = DCS[t][i];
                int nr = r + dr;
                int nc = c + dc;
                while (!wall[nr][nc]) {
                    attack[p][nr][nc]++;
                    if (types[nr][nc] != EMPTY) {
                        assert (pid[nr][nc] == p);
                        break;
                    }
                    nr += dr;
                    nc += dc;
                }
            }
        } else {
            for (int i = 0; i < DRS[t].length; i++) {
                int dr = DRS[t][i];
                int dc = DCS[t][i];
                int nr = r + dr;
                int nc = c + dc;
                if (!wall[nr][nc]) {
                    attack[p][nr][nc]++;
                    if (types[nr][nc] != EMPTY) {
                        assert (pid[nr][nc] == p);
                    }
                }
            }
        }
    }

    void block(int r, int c, int p, int t) {
        for (int i = 0; i < DRS[t].length; i++) {
            int dr = DRS[t][i];
            int dc = DCS[t][i];
            int pr = r - dr;
            int pc = c - dc;
            while (!wall[pr][pc]) {
                if (types[pr][pc] != EMPTY) {
                    if (pid[pr][pc] == p && (types[pr][pc] == QUEEN || types[pr][pc] == t)) {
//                        debugfln("block %d %d %d %d", r - 2, c - 2, pr - 2, pc - 2);
                        int nr = r + dr;
                        int nc = c + dc;
                        while (!wall[nr][nc]) {
                            attack[p][nr][nc]--;
                            if (types[nr][nc] != EMPTY) {
                                assert (pid[nr][nc] == p);
                                break;
                            }
                            nr += dr;
                            nc += dc;
                        }
                    }
                    break;
                }
                pr -= dr;
                pc -= dc;
            }
        }
    }


    boolean canRemove(int r, int c) {
        final int p = pid[r][c];
        if (attack[p][r][c] > 0) {
            if (!canExtend(r, c, p, ROOK)) return false;
            if (!canExtend(r, c, p, BISHOP)) return false;
        }
        return true;
    }

    void remove(int r, int c) {
//        debugfln("remove r:%d c:%d t:%d p:%d", r - 2, c - 2, t, p);
        final int p = pid[r][c];
        removeThis(r, c);
        if (attack[p][r][c] > 0) {
            extend(r, c, p, ROOK);
            extend(r, c, p, BISHOP);
        }
    }

    void removeThis(int r, int c) {
        final int t = types[r][c];
        final int p = pid[r][c];
        types[r][c] = EMPTY;
        pid[r][c] = -1;
        if (t <= 2) {
            // sliding
            for (int i = 0; i < DRS[t].length; i++) {
                int dr = DRS[t][i];
                int dc = DCS[t][i];
                int nr = r + dr;
                int nc = c + dc;
                while (!wall[nr][nc]) {
                    attack[p][nr][nc]--;
                    if (types[nr][nc] != EMPTY) {
                        assert (pid[nr][nc] == p);
                        break;
                    }
                    nr += dr;
                    nc += dc;
                }
            }
        } else {
            for (int i = 0; i < DRS[t].length; i++) {
                int dr = DRS[t][i];
                int dc = DCS[t][i];
                int nr = r + dr;
                int nc = c + dc;
                if (!wall[nr][nc]) {
                    attack[p][nr][nc]--;
                    if (types[nr][nc] != EMPTY) {
                        assert (pid[nr][nc] == p);
                    }
                }
            }
        }
    }

    boolean canExtend(int r, int c, int p, int t) {
        for (int i = 0; i < DRS[t].length; i++) {
            int dr = DRS[t][i];
            int dc = DCS[t][i];
            int pr = r - dr;
            int pc = c - dc;
            while (!wall[pr][pc]) {
                if (types[pr][pc] != EMPTY) {
                    if (pid[pr][pc] == p && (types[pr][pc] == QUEEN || types[pr][pc] == t)) {
                        int nr = r + dr;
                        int nc = c + dc;
                        while (!wall[nr][nc]) {
                            if (types[nr][nc] != EMPTY) {
                                if (pid[nr][nc] != p) {
                                    return false;
                                }
                                break;
                            }
                            nr += dr;
                            nc += dc;
                        }
                    }
                    break;
                }
                pr -= dr;
                pc -= dc;
            }
        }
        return true;
    }

    void extend(int r, int c, int p, int t) {
        for (int i = 0; i < DRS[t].length; i++) {
            int dr = DRS[t][i];
            int dc = DCS[t][i];
            int pr = r - dr;
            int pc = c - dc;
            while (!wall[pr][pc]) {
                if (types[pr][pc] != EMPTY) {
                    if (pid[pr][pc] == p && (types[pr][pc] == QUEEN || types[pr][pc] == t)) {
                        int nr = r + dr;
                        int nc = c + dc;
                        while (!wall[nr][nc]) {
//                            debugfln("extend %d %d %d %d", nr-2, nc-2, t, attack[p][nr][nc]);
                            attack[p][nr][nc]++;
                            if (types[nr][nc] != EMPTY) {
                                assert (pid[nr][nc] == p);
                                break;
                            }
                            nr += dr;
                            nc += dc;
                        }
                    }
                    break;
                }
                pr -= dr;
                pc -= dc;
            }
        }
    }

    void replace(int r, int c, int t) {
        final int p = pid[r][c];
        removeThis(r, c);
        placeThis(r, c, t, p);
    }

    boolean validate() {
        boolean ok = true;
        int[][][] at = new int[C][N + 4][N + 4];
        for (int i = 2; i < N + 2; i++) {
            for (int j = 2; j < N + 2; j++) {
                int t = types[i][j];
                if (t == EMPTY) continue;
                if (wall[i][j]) {
                    debugfln("wall %d %d", i - 2, j - 2);
                    ok = false;
                }
                int p = pid[i][j];
                if (t <= 2) {
                    // sliding
                    for (int k = 0; k < DRS[t].length; k++) {
                        int dr = DRS[t][k];
                        int dc = DCS[t][k];
                        int nr = i + dr;
                        int nc = j + dc;
                        while (!wall[nr][nc]) {
                            at[p][nr][nc]++;
                            if (types[nr][nc] != EMPTY) {
                                if (pid[nr][nc] != p) {
                                    debugfln("%d %d %d %d", i - 2, j - 2, nr - 2, nc - 2);
                                    ok = false;
                                }
                                break;
                            }
                            nr += dr;
                            nc += dc;
                        }
                    }
                } else {
                    for (int k = 0; k < DRS[t].length; k++) {
                        int dr = DRS[t][k];
                        int dc = DCS[t][k];
                        int nr = i + dr;
                        int nc = j + dc;
                        at[p][nr][nc]++;
                        if (types[nr][nc] != EMPTY && pid[nr][nc] != p) {
                            debugfln("%d %d %d %d", i - 2, j - 2, nr - 2, nc - 2);
                            ok = false;
                        }
                    }
                }
            }
        }
        for (int p = 0; p < C; p++) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (!wall[i + 2][j + 2] && at[p][i + 2][j + 2] != attack[p][i + 2][j + 2]) {
                        debugfln("%d %d %d %d %d", p, i, j, at[p][i + 2][j + 2], attack[p][i + 2][j + 2]);
                        ok = false;
                    }
                }
            }
        }
        return ok;
    }

    class State {
        int[][] types, pid;
        int[][][] attack;
        double score;

        State() {
            types = new int[N + 4][N + 4];
            pid = new int[N + 4][N + 4];
            attack = new int[C][N + 4][N + 4];
        }

        State(int[][] ot, int[][] op, int score) {
            this.score = score;
            types = new int[N + 4][];
            pid = new int[N + 4][];
            for (int i = 2; i <= N + 1; i++) {
                types[i] = ot[i].clone();
                pid[i] = op[i].clone();
            }
        }
    }


    static void debug(String str) {
        if (DEBUG) System.err.println(str);
    }

    static void debug(Object... obj) {
        if (DEBUG) System.err.println(Arrays.deepToString(obj));
    }

    static void debugf(String fmt, Object... obj) {
        if (DEBUG) System.err.printf(fmt, obj);
    }

    static void debugfln(String fmt, Object... obj) {
        if (DEBUG) System.err.printf(fmt + "\n", obj);
    }

    final class IntAry {
        int[] a;
        int size;

        IntAry() {
            a = new int[16];
        }

        IntAry(int cap) {
            a = new int[cap];
        }

        IntAry(int[] a) {
            this.a = a;
            this.size = a.length;
        }

        IntAry(IntAry that) {
            a = that.a.clone();
            size = that.size;
        }

        @Override
        public String toString() {
            return Arrays.toString(Arrays.copyOf(a, size));
        }

        int getRnd() {
            return size == 1 ? a[0] : a[rnd.nextInt(size)];
        }

        IntAry copyOf(int from, int to) {
            return new IntAry(Arrays.copyOfRange(this.a, from, to));
        }

        void add(int v) {
            if (size == a.length) {
                int[] na = new int[a.length * 2];
                System.arraycopy(a, 0, na, 0, size);
                a = na;
            }
            a[size++] = v;
        }

        void addAll(IntAry ia) {
            if (this.size + ia.size < a.length) {
                int[] na = new int[this.size + ia.size];
                System.arraycopy(a, 0, na, 0, size);
                a = na;
            }
            System.arraycopy(ia.a, 0, a, size, ia.size);
            size += ia.size;
        }

        void clear() {
            size = 0;
        }

        int pop() {
            size--;
            return a[size];
        }

        int back() {
            return a[size - 1];
        }

        void remove(int pos) {
            System.arraycopy(a, pos + 1, a, pos, size - pos - 1);
            size--;
        }

        void swapRemove(int pos) {
            swap(a, pos, size - 1);
            size--;
        }

        void sort() {
            Arrays.sort(a, 0, size);
        }
    }

    static void swap(int[] a, int p1, int p2) {
        int tmp = a[p1];
        a[p1] = a[p2];
        a[p2] = tmp;
    }

    class PosSet {
        int[][] pos;
        int[] list;
        int size;

        PosSet(int n) {
            pos = new int[n][n];
            list = new int[n * n];
            for (int i = 0; i < n; i++) {
                Arrays.fill(pos[i], -1);
            }
        }

        int get() {
            int p = rnd.nextInt(size);
            return list[p];
        }

        void remove(int r, int c) {
            int p = pos[r][c];
            size--;
            if (p != size) {
                swap(list, p, size);
                pos[list[p] >> 8][list[p] & 0xFF] = p;
            }
            pos[r][c] = -1;
        }

        void add(int r, int c) {
            pos[r][c] = size;
            list[size++] = (r << 8) + c;
        }

    }

    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            int N = Integer.parseInt(br.readLine());
            int C = Integer.parseInt(br.readLine());
            char[] grid = new char[N * N];
            for (int i = 0; i < N * N; i++) grid[i] = br.readLine().charAt(0);
            int[] points = new int[5];
            for (int i = 0; i < 5; i++) points[i] = Integer.parseInt(br.readLine());

            MultiplayerChessPieces cp = new MultiplayerChessPieces();
            char[] ret = cp.findSolution(N, C, grid, points);

            System.out.println(ret.length);
            for (int i = 0; i < ret.length; i++) System.out.println(ret[i]);
        } catch (Exception e) {
        }
    }
}