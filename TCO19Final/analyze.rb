#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:N, :C, :P, :score)

def get_scores(filename)
  scores = []
  seed = 0
  n = 0
  c = 0
  IO.foreach(filename) do |line|
    if line =~ /seed *(\d+)/
      seed = $1.to_i
      scores[seed] = TestCase.new
    elsif line =~ /N: *(\d+) C: *(\d+) P: *([0-9.]+)/
      scores[seed].N = $1.to_i
      scores[seed].C = $2.to_i
      scores[seed].P = $3.to_f
    elsif line =~ /score: *([0-9]+)/
      scores[seed].score = $1.to_i
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    next unless filter.call(pair[0], pair[1])
    count += 1
    s1 = pair[0].score
    s2 = pair[1].score
    if s1 > s2
      sum_score_diff += (1.0 * s2 / s1) - 1.0
      lose += 1
    elsif s1 < s2
      sum_score_diff += 1.0 - (1.0 * s1 / s2)
      win += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff * 1000000.0 / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  8.step(50, 9) do |param|
    upper = param + 8
    puts "N:#{sprintf('%2d', param)}-#{sprintf('%2d', upper)}"
    calc(from, to) { |from, to| from.N.between?(param, upper) }
  end
  puts "---------------------"
  2.step(8, 1) do |param|
    upper = param
    puts "C:#{sprintf('%d', param)}-#{sprintf('%d', upper)}"
    calc(from, to) { |from, to| from.C.between?(param, upper) }
  end
  puts "---------------------"
  (0.15).step(0.6499, 0.1) do |param|
    upper = param + 0.1
    puts "P:#{sprintf('%.2f', param)}-#{sprintf('%.2f', upper)}"
    calc(from, to) { |from, to| from.P.between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
