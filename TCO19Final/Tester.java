import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.security.*;
import java.net.URL;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import javax.swing.*;


public class Tester {

    private static int minN = 8, maxN = 50;
    private static int minC = 2, maxC = 8;
    private static double minWallP = 0.15, maxWallP = 0.65;

    final static int[] KingDr = {1, 1, 1, 0, 0, -1, -1, -1};   //used for queen too
    final static int[] KingDc = {1, 0, -1, 1, -1, 1, 0, -1};
    final static int[] RookDr = {0, 1, 0, -1};
    final static int[] RookDc = {-1, 0, 1, 0};
    final static int[] BishopDr = {1, 1, -1, -1};
    final static int[] BishopDc = {1, -1, 1, -1};
    final static int[] KnightDr = {2, 2, -2, -2, 1, 1, -1, -1};
    final static int[] KnightDc = {1, -1, 1, -1, 2, -2, 2, -2};
    final static char EMPTY = '.';
    final static char WALL = '#';
    //notation from https://en.wikipedia.org/wiki/Descriptive_notation
    final static char KING = 'K';
    final static char QUEEN = 'Q';
    final static char ROOK = 'R';
    final static char BISHOP = 'B';
    final static char KNIGHT = 'N';
    final static char[] names = {KING, QUEEN, ROOK, BISHOP, KNIGHT};
    final static String[] names2 = {"King", "Queen", "Rook", "Bishop", "Knight"};

    //inputs
    int N;
    int C;
    double wallP;
    char[][] Grid;
    int[] Points;

    //outputs
    int Score;
    int[] Scores;

    void generate(long seed) throws Exception {
        SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
        r1.setSeed(seed);
        N = r1.nextInt(maxN - minN + 1) + minN;
        if (seed == 1) N = minN;
        if (seed == 2) N = maxN;
        C = r1.nextInt(maxC - minC + 1) + minC;
        wallP = r1.nextDouble() * (maxWallP - minWallP) + minWallP;

        if (seed == 1) wallP = 0.19962386685404293;

        Grid = new char[N][N];
        for (int i = 0; i < N; i++)
            for (int k = 0; k < N; k++)
                if (r1.nextDouble() < wallP)
                    Grid[i][k] = WALL;
                else
                    Grid[i][k] = EMPTY;

        Points = new int[names.length];
        for (int i = 0; i < names.length; i++) {
            if (names[i] == KING)
                Points[i] = r1.nextInt(8 - 3 + 1) + 3;
            if (names[i] == QUEEN)
                Points[i] = r1.nextInt(4 * N - 3 * N + 1) + 3 * N;
            if (names[i] == ROOK)
                Points[i] = r1.nextInt(N * 5 / 2 - N * 3 / 2 + 1) + N * 3 / 2;
            if (names[i] == BISHOP)
                Points[i] = r1.nextInt(2 * N - N + 1) + N;
            if (names[i] == KNIGHT)
                Points[i] = r1.nextInt(8 - 2 + 1) + 2;
        }
    }

    public Result runTest(long seed) throws Exception {
        generate(seed);
        Result res = new Result();
        res.seed = seed;
        res.N = N;
        res.C = C;
        res.P = wallP;
        res.points = Points;

        char[] grid1d = new char[N * N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                grid1d[i * N + j] = Grid[i][j];
            }
        }
        long startTime = System.currentTimeMillis();
        MultiplayerChessPieces sol = new MultiplayerChessPieces();
        char[] solution = sol.findSolution(N, C, grid1d, Points);
        res.elapsed = System.currentTimeMillis() - startTime;

        //construct output board and check for invalid characters
        Board = new char[N][N];
        for (int i = 0, cur = 0; i < N; i++)
            for (int k = 0; k < N; k++, cur++) {
                Board[i][k] = solution[cur];

                if (!isLegal(Board[i][k])) {
                    throw new RuntimeException("Cell (" + i + "," + k + ") has illegal character " + Board[i][k]);
                }
                if (Grid[i][k] == WALL && Board[i][k] != WALL) {
                    throw new RuntimeException("You cannot remove the wall at cell (" + i + "," + k + ")");
                }
                if (Grid[i][k] == EMPTY && Board[i][k] == WALL) {
                    throw new RuntimeException("You cannot add a wall to cell (" + i + "," + k + ")");
                }
            }

        Players = new int[N][N];
        for (int i = 0, cur = N * N; i < N; i++)
            for (int k = 0; k < N; k++, cur++) {
                if (Board[i][k] == WALL || Board[i][k] == EMPTY) continue;
                if (solution[cur] < '0' || solution[cur] >= '0' + C) {
                    throw new RuntimeException("Illegal colour " + solution[cur] + " at cell (" + i + "," + k + ")");
                }
                Players[i][k] = solution[cur] - '0';
            }

        //check that no piece attacks any other piece of a different colour
        for (int r = 0; r < N; r++)
            for (int c = 0; c < N; c++) {
                if (Board[r][c] == KING) checkAttackSimple(r, c, KingDr, KingDc, "King");
                if (Board[r][c] == QUEEN) checkAttackComplex(r, c, KingDr, KingDc, "Queen");
                if (Board[r][c] == ROOK) checkAttackComplex(r, c, RookDr, RookDc, "Rook");
                if (Board[r][c] == BISHOP) checkAttackComplex(r, c, BishopDr, BishopDc, "Bishop");
                if (Board[r][c] == KNIGHT) checkAttackSimple(r, c, KnightDr, KnightDc, "Knight");
            }

        if (Debug) {
            System.err.println("received board pieces");
            for (int r = 0; r < N; r++) {
                for (int c = 0; c < N; c++) System.err.print(Board[r][c]);
                System.err.println();
            }
            System.err.println("received board player id");
            for (int i = 0, cur = N * N; i < N; i++) {
                for (int k = 0; k < N; k++, cur++)
                    System.err.print(solution[cur]);
                System.err.println();
            }
        }

        Scores = new int[C];
        for (int r = 0; r < N; r++)
            for (int c = 0; c < N; c++)
                for (int m = 0; m < names.length; m++)
                    if (names[m] == Board[r][c])
                        Scores[Players[r][c]] += Points[m];

        Score = Integer.MAX_VALUE;
        for (int i = 0; i < C; i++) Score = Math.min(Score, Scores[i]);

        if (vis) {
            jf.setSize(N * SZ + 230, N * SZ + 50);
            jf.setVisible(true);
            draw();
        }

        res.score = Score;
        return res;
    }


    void checkAttackSimple(int r, int c, int[] dr, int[] dc, String piece) {
        for (int m = 0; m < dr.length; m++) {
            int r2 = r + dr[m];
            int c2 = c + dc[m];
            if (inGrid(r2, c2) && isPiece(Board[r2][c2]) && Players[r][c] != Players[r2][c2]) {
                throw new RuntimeException(piece + " at (" + r + "," + c + ") attacks piece at (" + r2 + "," + c2 + ")");
            }
        }
    }

    void checkAttackComplex(int r, int c, int[] dr, int[] dc, String piece) {
        for (int m = 0; m < dr.length; m++) {
            int r2 = r;
            int c2 = c;
            while (inGrid(r2, c2) && Board[r2][c2] != WALL) {
                r2 += dr[m];
                c2 += dc[m];
                if (inGrid(r2, c2) && isPiece(Board[r2][c2])) {
                    //blocked by piece of own colour
                    if (Players[r][c] == Players[r2][c2]) break;
                        //attacking a piece of different colour is not allowed
                    else {
                        throw new RuntimeException(piece + " at (" + r + "," + c + ") attacks piece at (" + r2 + "," + c2 + ")");
                    }
                }
            }
        }
    }

    boolean isLegal(char c) {
        return c == EMPTY || c == WALL || c == KING || c == QUEEN || c == ROOK || c == BISHOP || c == KNIGHT;
    }

    boolean isPiece(char c) {
        return c == KING || c == QUEEN || c == ROOK || c == BISHOP || c == KNIGHT;
    }

    boolean inGrid(int r, int c) {
        return (r >= 0 && r < N && c >= 0 && c < N);
    }

    JFrame jf;
    Vis v;
    static boolean vis;
    static int SZ;
    static boolean Debug;
    char[][] Board;
    int[][] Players;
    //pics from https://commons.wikimedia.org/wiki/Category:PNG_chess_pieces/Standard_transparent
    Image[] pics;

    void draw() {
        if (!vis) return;
        v.repaint();
    }

    public class Vis extends JPanel implements MouseListener, WindowListener {
        public void paint(Graphics g) {

            Color[] colors = {Color.BLUE, Color.MAGENTA, Color.YELLOW, Color.RED, Color.CYAN, Color.PINK, Color.GREEN, Color.ORANGE};

            int startX = 10;
            int startY = 10;

            // background
            g.setColor(new Color(0xDDDDDD));
            g.fillRect(0, 0, N * SZ + 230, N * SZ + 40);
            g.setColor(Color.WHITE);
            g.fillRect(startX, startY, N * SZ, N * SZ);

            //paint thin lines between cells
            g.setColor(Color.BLACK);
            for (int i = 0; i <= N; i++)
                g.drawLine(startX, startY + i * SZ, startX + N * SZ, startY + i * SZ);
            for (int i = 0; i <= N; i++)
                g.drawLine(startX + i * SZ, startY, startX + i * SZ, startY + N * SZ);


            //paint characters
            g.setFont(new Font("Arial", Font.PLAIN, 14));
            g.setColor(Color.BLACK);

            int[] playerCount = new int[C];
            for (int r = 0; r < N; r++)
                for (int c = 0; c < N; c++)
                    if (Board[r][c] == WALL) {
                        g.setColor(Color.GRAY);
                        g.fillRect(startX + c * SZ + 1, startY + r * SZ + 1, SZ - 1, SZ - 1);
                    } else if (isPiece(Board[r][c])) {
                        for (int m = 0; m < names.length; m++)
                            if (Board[r][c] == names[m]) {
                                g.setColor(colors[Players[r][c]]);
                                playerCount[Players[r][c]]++;
                                g.fillRect(startX + c * SZ + 1, startY + r * SZ + 1, SZ - 1, SZ - 1);
                                g.drawImage(pics[m], startX + c * SZ, startY + r * SZ, SZ, SZ, null);
                            }
                    }


            //display score and info
            g.setColor(Color.BLACK);
            g.setFont(new Font("Arial", Font.BOLD, 14));
            g.drawString("SCORE " + Score, SZ * N + 25, 30);

            //display points
            for (int i = 0; i < names.length; i++) {
                g.setColor(Color.BLACK);
                g.drawString(names2[i] + ": " + Points[i] + " points", SZ * N + 25, 70 + 20 * i);
            }

            //display player scores
            for (int i = 0; i < C; i++) {
                g.setColor(colors[i]);
                g.drawString("Player " + i + ": " + Scores[i] + "(" + playerCount[i] + ")", SZ * N + 25, 200 + 20 * i);
            }


        }


        // -------------------------------------
        public Vis() {
            jf.addWindowListener(this);
        }

        // -------------------------------------
        //WindowListener
        public void windowClosing(WindowEvent e) {
            System.exit(0);
        }

        public void windowActivated(WindowEvent e) {
        }

        public void windowDeactivated(WindowEvent e) {
        }

        public void windowOpened(WindowEvent e) {
        }

        public void windowClosed(WindowEvent e) {
        }

        public void windowIconified(WindowEvent e) {
        }

        public void windowDeiconified(WindowEvent e) {
        }

        // -------------------------------------
        //MouseListener
        public void mouseClicked(MouseEvent e) {
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }
    }

    Image loadImage(String name) {
        try {
            URL img = Tester.class.getResource(name);
            if (img == null) return null;
            Image im = Toolkit.getDefaultToolkit().createImage(img);
            return im;
        } catch (Exception e) {
            return null;
        }
    }

    public Tester() throws Exception {
        if (vis) {
            jf = new JFrame();
            v = new Vis();
            jf.getContentPane().add(v);

            pics = new Image[names.length];
            for (int i = 0; i < names.length; i++)
                pics[i] = loadImage("images/" + names2[i].toLowerCase() + ".png");    //TODO: make sure it is OS agnostic

            //TODO: need this?
            MediaTracker mt = new MediaTracker(jf);
            for (int i = 0; i < pics.length; i++) {
                if (pics[i] != null)
                    mt.addImage(pics[i], i);
            }
            mt.waitForAll();
        }
    }

    private static final int THREAD_COUNT = 2;

    public static void main(String[] args) throws Exception {
        long seed = 1;
        long begin = -1;
        long end = -1;
        SZ = 20;
        Debug = false;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
            if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
            if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
            if (args[i].equals("-vis")) vis = true;
            if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
            if (args[i].equals("-debug")) Debug = true;
        }
        if (seed == 1) SZ = 50;

        if (begin != -1 && end != -1) {
            BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
            BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
            for (long i = begin; i <= end; ++i) {
                q.add(i);
            }
            Result[] results = new Result[(int) (end - begin + 1)];
            TestThread[] threads = new TestThread[THREAD_COUNT];
            for (int i = 0; i < THREAD_COUNT; ++i) {
                threads[i] = new TestThread(q, receiver);
                threads[i].start();
            }
            int printed = 0;
            try {
                for (int i = 0; i < (int) (end - begin + 1); i++) {
                    Result res = receiver.poll(160, TimeUnit.SECONDS);
                    results[(int) (res.seed - begin)] = res;
                    for (; printed < results.length && results[printed] != null; printed++) {
                        System.out.println(results[printed]);
                        System.out.println();
                    }
                }
            } catch (InterruptedException e) {
                for (int i = printed; i < results.length; i++) {
                    System.out.println(results[i]);
                    System.out.println();
                }
                e.printStackTrace();
            }

            double sum = 0;
            for (Result result : results) {
                sum += result.score;
            }
            System.out.println("ave:" + (sum / (end - begin + 1)));
        } else {
            Tester tester = new Tester();
            Result res = tester.runTest(seed);
            System.out.println(res);
        }
    }

    private static class TestThread extends Thread {
        BlockingQueue<Result> results;
        BlockingQueue<Long> q;

        TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
            this.q = q;
            this.results = results;
        }

        public void run() {
            while (true) {
                Long seed = q.poll();
                if (seed == null) break;
                try {
                    Tester f = new Tester();
                    Result res = f.runTest(seed);
                    results.add(res);
                } catch (Exception e) {
                    e.printStackTrace();
                    Result res = new Result();
                    res.seed = seed;
                    res.points = new int[5];
                    results.add(res);
                }
            }
        }
    }

    static class Result {
        long seed;
        int N, C;
        int[] points;
        int score;
        double P;
        long elapsed;

        public String toString() {
            String ps = String.format("Points: %1d %3d %3d %3d %1d", points[0], points[1], points[2], points[3], points[4]);
            return String.format("seed %d\nN:%2d C:%d P:%.3f\n%s\nscore:%d\nelapsed:%d", seed, N, C, P, ps, score, elapsed);
        }
    }
}
