#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <set>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <sys/time.h>

using namespace std;
typedef int64_t ll;
typedef uint64_t ull;

//#define MEASURE_TIME 1
#define DEBUG 1
//#define SUBMIT 1

const bool SUBMARINE = false;
const int DR[] = {-1, 0, 1, 0, -1, 0, 1, 0,};
const int DC[] = {0, 1, 0, -1, 0, 1, 0, -1,};
const int MAX_WIDTH = 4096;
const int RND_VAL = 64;
const int INITIAL_BEAM_WIDTH[3][9] = {
	{1750, 1500, 1300, 1200, 1100,  900,  800,  740, 700},
	{1800, 1500, 1350, 1200, 1000,  920,  850,  800, 700},
	{1900, 1800, 1700, 1600, 1350, 1200, 1050, 1000, 930}
};

#ifdef SUBMIT
#define CLOCK_PER_SEC 3.6e9
const ll TL = 29600;
const ll TL2 = 29900;
const bool EASY_COMPILE = false;
#else
#define CLOCK_PER_SEC 2.0e9
const ll TL = 13000;
const ll TL2 = 13100;
const bool EASY_COMPILE = false;
#endif

// msec
inline ll getTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUIntMask() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w & (RND_VAL-1);
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}
};

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<double> sum; // msec

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]) / CLOCK_PER_SEC * 1000;
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)sum[i] << ", ";
		}
		cerr << "]" << endl;
	}
};

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#else
#define debug(format, ...)
#endif

const int T = 10000;
int RMCNT_SHIFT = 7;
int COLORS_VAL_BASE[] = { 0, 0, 8, 16, 27, 64 };
const int CAND_THREE_BR = 1;
const int CAND_THREE_BL = 2;
const int CAND_THREE_TR = 3;
const int CAND_THREE_TL = 4;
const int CAND_TWO_DOUBLE_HORZ = 5;
const int CAND_TWO_DOUBLE_VERT = 6;
const int CAND_TWO_DOUBLE_X = 7;
const int CAND_TWO_T = 8;
const int CAND_TWO_B = 9;
const int CAND_TWO_L = 10;
const int CAND_TWO_R = 11;
const int CAND_TWO_SL = 12;
const int CAND_TWO_BK = 13;
const int CAND_ALL = 14;
XorShift rnd;
ll startTime;
Timer timer;
Counter counter;

int C;
vector<int> ans(T * 3);
int colorsVal[1 << 12];
int cand[1 << 12];
int candVal[16];
int destT, destB, destL, destR;
ull origField[20];
int bi;
int easyScore;

namespace rng {
	int pos;
	ull rndVal[80000];

	void init(int startSeed) {
		rng::pos = 0;
		ull cur = startSeed;
		for (int i = 0; i < sizeof(rndVal) / sizeof(rndVal[0]); ++i) {
			rng::rndVal[i] = (cur % C) + 1;
			cur = (cur * 48271) % 2147483647;
			rng::rndVal[i] |= ((cur % C) + 1) << 3;
			cur = (cur * 48271) % 2147483647;
		}
	}

	ull next() {
		return rng::rndVal[pos++];
	}

	void setPos(int p) {
		rng::pos = p;
	}
}

struct Hand {
	uint32_t param;
	int v;

	Hand() {}

	Hand(int r, int c, int d, int val, int boardI) : v(val) {
		param = (r-2) | ((c-2) << 4) | ((d & 3) << 8) | (boardI << 10);
	}

	Hand(uint32_t n, int val, int boardI) : v(val) {
		param = n | (boardI << 10);
	}

	int r() const {
		return ((param >> 0) & 0xF) + 2;
	}

	int c() const {
		return ((param >> 4) & 0xF) + 2;
	}

	int d() const {
		return (param >> 8) & 0x3;
	}

	int boardI() const {
		return (param >> 10) & 0xFFF;
	}

	uint32_t next() const {
		return param >> 22;
	}
};

bool operator<(const Hand& l, const Hand& r) {
	return l.v > r.v;
}

// void partition(vector<Hand>& hands, int lim) {
// 	int l = 0;
// 	int r = hands.size();
// 	while (true) {
// 		if (lim == 0 || lim == r - l) break;
// 		int pp = rnd.nextUInt(r - l) + l;
// 		swap(hands[l], hands[pp]);
// 		int right = r;
// 		int left = l + 1;
// 		while (left < right) {
// 			int compare = hands[l].v - hands[left].v;
// 			bool move = compare > 0 || (compare == 0 && rnd.nextUInt(2) == 1);
// 			if (move) {
// 				--right;
// 				swap(hands[left], hands[right]);
// 			} else {
// 				++left;
// 			}
// 		}
// 		int large = right - l;
// 		if (large == lim) break;
// 		if (large > lim) {
// 			r = right;
// 		} else {
// 			l = right;
// 			lim -= large;
// 		}
// 	}
// }

struct Board {
	ull f[20];
	ull v[15];
	ull v2[15];
	int score, val;
	Hand hand;
};

Board gBoards[2][MAX_WIDTH];
uint32_t handsHist[T+1][MAX_WIDTH];
vector<Hand> hands;

template<int N> struct BoardSupport {

static inline bool hit(const Board& board, int r, int c, int target) {
	return ((board.f[r] >> (3 * c)) & 0x7) == target;
}

static inline int colors(const Board& board, int r, int c) {
	int colors = (int) ((board.f[r] >> (c * 3)) & 0x3F) << 6;
	colors |= (int) ((board.f[r + 1] >> (c * 3)) & 0x3F);
	return cand[colors];
}

static void swapAndRemove(Board& board, int r, int c, int dir) {
	int nr = r + DR[dir];
	int nc = c + DC[dir];
	ull c1 = (board.f[r] >> (c * 3)) & 0x7;
	ull c2 = (board.f[nr] >> (nc * 3)) & 0x7;
	if (c1 == c2) return;
	board.f[r] ^= ((c1 ^ c2) << (c * 3));
	board.f[nr] ^= ((c1 ^ c2) << (nc * 3));
	board.score += remove(board, r, c, nr, nc);
	int top = max(2, destT - 1);
	int bottom = min(destB, N);
	int left = max(2, destL - 1);
	int right = min(destR, N);
	for (int i = top; i <= bottom; ++i) {
		for (int j = left; j <= right; ++j) {
			int cs = colors(board, i, j);
			int oldCs = (int) ((board.v[i-2] >> ((j-2) * 4)) & 0xF);
			board.val += candVal[cs];
			board.val -= candVal[oldCs];
			board.v[i-2] ^= ((ull) oldCs ^ cs) << ((j-2) * 4);
		}
		board.v2[i-2] = board.v[i-2];
	}
	if (top != 2) board.v2[top-3] = board.v[top-3];
	if (bottom != N) board.v2[bottom-1] = board.v[bottom-1];
	if (N <= 10 || C != 4) {
		if (top > 3) board.v2[top-4] = board.v[top-4];
		if (bottom < N-1) board.v2[bottom] = board.v[bottom];
	}
}

static int remove(Board& board, int top, int left, int bottom, int right) {
	rng::setPos(board.score * 2);
	int ret = 0;
	if (top > bottom) {
		swap(top, bottom);
	}
	if (left > right) {
		swap(left, right);
	}
	destT = top;
	destL = left;
	destB = bottom;
	destR = right;
	if (top != 2) --top;
	if (left != 2) --left;
	if (bottom == N + 1) --bottom;
	if (right == N + 1) --right;
	while (true) {
START_SEARCH_REMOVE:
		for (int i = top; i <= bottom; ++i) {
			ull v1 = (board.f[i] ^ board.f[i+1]);
			v1 |= (v1 << 3);
			ull v2 = board.f[i] ^ (board.f[i] << 3);
			ull v3 = v1 | v2;
			ull mask = 0x7ULL << ((left + 1) * 3);
			for (int j = left; j <= right; ++j, mask <<= 3) {
				if ((v3 & mask) == 0) {
					ull f1 = board.f[i] & (mask | (mask >> 3));
					ull f2 = board.f[i + 1] & (mask | (mask >> 3));
					destT = min(destT, i);
					destB = max(destB, i + 1);
					destL = min(destL, j);
					destR = max(destR, j + 1);
					top = i == 2 ? 2 : i - 1;
					if (j == left && j != 2) --left;
					if (i == bottom && bottom != N) ++bottom;
					if (j == right && right != N) ++right;
					++ret;
					board.f[i] ^= f1;
					board.f[i] |= rng::next() << (j * 3);
					board.f[i + 1] ^= f2;
					board.f[i + 1] |= rng::next() << (j * 3);
					goto START_SEARCH_REMOVE;
				}
			}
		}
		break;
	}
	return ret;
}

static int evaluate(Board& board, int r, int c, int dir) {
	int nr = r;
	int nc = c;
	switch (dir & 3) {
	case 0:
		--r;
		break;
	case 1:
		++nc;
		break;
	case 2:
		++nr;
		break;
	case 3:
		--c;
		break;
	}
	ull c1 = (board.f[r] >> (c * 3)) & 0x7;
	ull c2 = (board.f[nr] >> (nc * 3)) & 0x7;
	board.f[r] ^= ((c1 ^ c2) << (c * 3));
	board.f[nr] ^= ((c1 ^ c2) << (nc * 3));

	rng::setPos(board.score * 2);
	int destT = r;
	int destB = nr;
	int top = r == 2 ? 2 : r - 1;
	int bottom = nr == N + 1 ? N : nr;
	int left = c == 2 ? 2 : c - 1;
	int right = nc == N + 1 ? N : nc;
	int ret = board.score;
	int diff = 0;

	// first remove
	for (int i = top; i <= bottom; ++i) {
		ull v1 = (board.f[i] ^ board.f[i+1]);
		v1 |= (v1 << 3);
		ull v2 = board.f[i] ^ (board.f[i] << 3);
		ull v3 = v1 | v2;
		ull mask = 0x7ULL << ((left + 1) * 3);
		for (int j = left; j <= right; ++j, mask <<= 3) {
			if ((v3 & mask) == 0) {
				ull f1 = board.f[i] & (mask | (mask >> 3));
				ull f2 = board.f[i + 1] & (mask | (mask >> 3));
				destT = min(destT, i);
				destB = max(destB, i + 1);
				top = i == 2 ? 2 : i - 1;
				if (j == left && j != 2) --left;
				if (i == bottom && bottom != N) ++bottom;
				if (j == right && right != N) ++right;
				board.f[i] ^= f1;
				board.f[i] |= rng::next() << (j * 3);
				board.f[i + 1] ^= f2;
				board.f[i + 1] |= rng::next() << (j * 3);
				++ret;
				goto START_SEARCH_EVAL;
			}
		}
	}

	{
START_SEARCH_EVAL:
		diff = 0;
		for (int i = top; i <= bottom; ++i) {
			ull row = board.v[i-2] >> ((left-2) * 4);
			for (int j = left; j <= right; ++j, row >>= 4) {
				int here = colors(board, i, j);
				if (here == CAND_ALL) {
					destT = min(destT, i);
					destB = max(destB, i + 1);
					top = i == 2 ? 2 : i - 1;
					if (j == left && j != 2) --left;
					if (i == bottom && bottom != N) ++bottom;
					if (j == right && right != N) ++right;
					ret += 2;
					board.f[i] &= ~(0x3FULL << (j * 3));
					board.f[i] |= rng::next() << (j * 3);
					board.f[i + 1] &= ~(0x3FULL << (j * 3));
					board.f[i + 1] |= rng::next() << (j * 3);
					diff = 0;
					goto END_SEARCH_EVAL;
				} else {
					diff += candVal[here];
					diff -= candVal[(int) (row & 0xF)];
				}
			}
		}
	}
END_SEARCH_EVAL:

// 	while (true) {
// START_SEARCH_EVAL:
// 		diff = 0;
// 		for (int i = top; i <= bottom; ++i) {
// 			ull row = v[i] >> (left * 4);
// 			for (int j = left; j <= right; ++j, row >>= 4) {
// 				int here = colors(i, j);
// 				if (here == 5) {
// 					destT = min(destT, i);
// 					destB = max(destB, i + 1);
// 					top = i == 0 ? 0 : i - 1;
// 					if (j == left && j != 0) --left;
// 					if (i == bottom && bottom != N - 2) ++bottom;
// 					if (j == right && right != N - 2) ++right;
// 					++ret;
// 					f[i] &= ~(0x3FULL << (j * 3));
// 					f[i] |= rng::next() << (j * 3);
// 					f[i + 1] &= ~(0x3FULL << (j * 3));
// 					f[i + 1] |= rng::next() << (j * 3);
// 					goto START_SEARCH_EVAL;
// 				} else {
// 					diff += COLORS_VAL_BASE[here];
// 					diff -= COLORS_VAL_BASE[(int) (row & 0xF)];
// 				}
// 			}
// 		}
// 		break;
// 	}

	ADD_COUNTER(ret - board.score);
	ret <<= RMCNT_SHIFT;
	ret += diff + board.val;

	// restore
	for (int i = destT; i <= destB; ++i) {
		board.f[i] = origField[i];
	}
	ret += rnd.nextUIntMask();
	return ret;
}

static int evaluateEasy(const Board& board) {
	ADD_COUNTER(0);
	int ret = board.score << RMCNT_SHIFT;
	ret += board.val;
	ret += rnd.nextUIntMask();
	return ret;
}

static void evaluateBlock(Board& board, int r, int c, int cs) {
	int before = hands.size();
	switch (cs) {
	case CAND_THREE_BR:
		evaluateBlockTri<0>(board, r + 1, c + 1);
		break;
	case CAND_THREE_BL:
		evaluateBlockTri<1>(board, r + 1, c);
		break;
	case CAND_THREE_TL:
		evaluateBlockTri<2>(board, r, c);
		break;
	case CAND_THREE_TR:
		evaluateBlockTri<3>(board, r, c + 1);
		break;
	case CAND_TWO_DOUBLE_HORZ:
		evaluateBlockBi<2>(board, r, c + 1);
		evaluateBlockBi<0>(board, r + 1, c);
		break;
	case CAND_TWO_DOUBLE_VERT:
		evaluateBlockBi<1>(board, r, c);
		evaluateBlockBi<3>(board, r + 1, c + 1);
		break;
	case CAND_TWO_DOUBLE_X:
		evaluateBlockDiag<1>(board, r, c + 1);
		evaluateBlockDiag<0>(board, r, c);
		break;
	case CAND_TWO_T:
		evaluateBlockBi<2>(board, r, c + 1);
		break;
	case CAND_TWO_B:
		evaluateBlockBi<0>(board, r + 1, c);
		break;
	case CAND_TWO_L:
		evaluateBlockBi<1>(board, r, c);
		break;
	case CAND_TWO_R:
		evaluateBlockBi<3>(board, r + 1, c + 1);
		break;
	case CAND_TWO_SL:
		evaluateBlockDiag<1>(board, r, c + 1);
		break;
	case CAND_TWO_BK:
		evaluateBlockDiag<0>(board, r, c);
		break;
	}
	if (hands.size() == before) {
		// hopeless
		board.v2[r-2] ^= (((ull)cs) << ((c-2)*4));
	}
}

template<int dir>
static void evaluateBlockTri(Board& board, int r, int c) {
	const ull rows[5] = {board.f[r-2] >> (3 * (c-2)),
	                     board.f[r-1] >> (3 * (c-2)),
	                     board.f[r  ] >> (3 * (c-2)),
	                     board.f[r+1] >> (3 * (c-2)),
	                     board.f[r+2] >> (3 * (c-2))};
	int target = (int) (rows[2 + DR[dir]] >> (3 * (2 + DC[dir])) & 0x7);
	bool reach = false;
	if (((rows[2 + DR[dir+1]] >> (3 * (2 + DC[dir+1]))) & 0x7) == target) {
		evalAndAdd(board, r, c, dir + 1);
		reach = true;
	}
	if (((rows[2 + DR[dir+2]] >> (3 * (2 + DC[dir+2]))) & 0x7) == target) {
		evalAndAdd(board, r, c, dir + 2);
		reach = true;
	}
	if (N > 10 && C == 4) return;
	if (reach) return;
	if (bi > hands.size() / 2) return;

	if (((rows[2 + DR[dir] * 2] >> (3 * (2 + DC[dir] * 2))) & 0x7) == target) {
		evalAndAddEasy(r, c, dir);
		setNextHand(r + DR[dir], c + DC[dir], dir);
	}
	if (((rows[2 + DR[dir] + DR[dir+1]] >> (3 * (2 + DC[dir] + DC[dir+1]))) & 0x7) == target) {
		evalAndAddEasy(r, c, dir);
		setNextHand(r + DR[dir], c + DC[dir], dir + 1);
		evalAndAddEasy(r + DR[dir+1], c + DC[dir+1], dir);
		setNextHand(r, c, dir + 1);
	}
	if (((rows[2 + DR[dir+1] * 2] >> (3 * (2 + DC[dir+1] * 2))) & 0x7) == target) {
		evalAndAddEasy(r + DR[dir+1], c + DC[dir+1], dir + 1);
		setNextHand(r, c, dir + 1);
	}
	if (((rows[2 + DR[dir+1] + DR[dir+2]] >> (3 * (2 + DC[dir+1] + DC[dir+2]))) & 0x7) == target) {
		evalAndAddEasy(r + DR[dir+1], c + DC[dir+1], dir + 2);
		setNextHand(r, c, dir + 1);
		evalAndAddEasy(r + DR[dir+2], c + DC[dir+2], dir + 1);
		setNextHand(r, c, dir + 2);
	}
	if (((rows[2 + DR[dir+2] * 2] >> (3 * (2 + DC[dir+2] * 2))) & 0x7) == target) {
		evalAndAddEasy(r + DR[dir+2], c + DC[dir+2], dir + 2);
		setNextHand(r, c, dir + 2);
	}
	if (((rows[2 + DR[dir+2] + DR[dir+3]] >> (3 * (2 + DC[dir+2] + DC[dir+3]))) & 0x7) == target) {
		evalAndAddEasy(r, c, dir + 3);
		setNextHand(r + DR[dir+3], c + DC[dir+3], dir + 2);
		evalAndAddEasy(r + DR[dir+2], c + DC[dir+2], dir + 3);
		setNextHand(r, c, dir + 2);
	}
	if (((rows[2 + DR[dir+3] * 2] >> (3 * (2 + DC[dir+3] * 2))) & 0x7) == target) {
		evalAndAddEasy(r, c, dir + 3);
		setNextHand(r + DR[dir+3], c + DC[dir+3], dir + 3);
	}
}

template<int dir>
static void evaluateBlockBi(Board& board, int r, int c) {
	if (bi > hands.size() / 2) return;
	int target = (int) ((board.f[r] >> (3 * c)) & 0x7);
	int d1 = -1;
	int d2 = -1;
	int tr = r + DR[dir] * 2;
	int tc = c + DC[dir] * 2;
	if (hit(board, tr, tc, target)) {
		d1 = dir;
	} else {
		tr += -DR[dir] + DR[dir + 3];
		tc += -DC[dir] + DC[dir + 3];
		if (hit(board, tr, tc, target)) {
			d1 = dir + 3;
		}
	}
	if (d1 == -1) return;

	tr = r + DR[dir] * 2 + DR[dir + 1];
	tc = c + DC[dir] * 2 + DC[dir + 1];
	if (hit(board, tr, tc, target)) {
		d2 = dir;
	} else {
		tr += -DR[dir] + DR[dir + 1];
		tc += -DC[dir] + DC[dir + 1];
		if (hit(board, tr, tc, target)) {
			d2 = dir + 1;
		}
	}
	if (d2 == -1) return;

	evalAndAddEasy(r + DR[dir], c + DC[dir], d1);
	setNextHand(r + DR[dir] + DR[dir + 1], c + DC[dir] + DC[dir + 1], d2);
}

template<int dir>
static void evaluateBlockDiag(Board& board, int r, int c) {
	if (bi > hands.size() / 2) return;
	int target = (int) ((board.f[r] >> (3 * c)) & 0x7);
	int d1 = -1;
	int d2 = -1;
	int tr = r + DR[dir + 1] * 2;
	int tc = c + DC[dir + 1] * 2;
	if (hit(board, tr, tc, target)) {
		d1 = dir + 1;
	} else {
		tr += DR[dir] - DR[dir + 1];
		tc += DC[dir] - DC[dir + 1];
		if (hit(board, tr, tc, target)) {
			d1 = dir;
		}
	}
	if (d1 == -1) return;

	tr = r + DR[dir + 2] * 2;
	tc = c + DC[dir + 2] * 2;
	if (hit(board, tr, tc, target)) {
		d2 = dir + 2;
	} else {
		tr += -DR[dir + 2] + DR[dir + 3];
		tc += -DC[dir + 2] + DC[dir + 3];
		if (hit(board, tr, tc, target)) {
			d2 = dir + 3;
		}
	}
	if (d2 == -1) return;

	evalAndAddEasy(r + DR[dir + 1], c + DC[dir + 1], d1);
	setNextHand(r + DR[dir + 2], c + DC[dir + 2], d2);
}

static void evalAndAdd(Board& board, int r, int c, int d) {
	int v = evaluate(board, r, c, d);
	hands.emplace_back(r, c, d, v, bi);
}

static void evalAndAddEasy(int r, int c, int d) {
	ADD_COUNTER(0);
	// int v = easyScore + rnd.nextUIntMask();
	int v = easyScore + (1 << (RMCNT_SHIFT - 1)) * 2 / 3 + rnd.nextUIntMask();
	hands.emplace_back(r, c, d, v, bi);
}

static void setNextHand(int r, int c, int d) {
	hands.back().param |= (((r-2) | ((c-2) << 4) | ((d & 3) << 8)) << 22);
}

};

class SquareRemover {
public:
	SquareRemover();
	vector<int> playIt(int colors, const vector<string>& boardStr, int startSeed);
	template<int N> int solve();
	template<int N> void solvePre(const vector<string>& boardStr);
};

SquareRemover::SquareRemover() {
	startTime = getTime();
}

template<int N> int SquareRemover::solve() {
	typedef BoardSupport<N> BoardSupport;
	Board& initialBoard = gBoards[0][0];
	int BEAM_WIDTH = INITIAL_BEAM_WIDTH[C-4][N-8];
	{
		initialBoard.val = 0;
		for (int i = 0; i < N - 1; ++i) {
			initialBoard.v[i] = 0;
			for (int j = 0; j < N - 1; ++j) {
				int c = BoardSupport::colors(initialBoard, i+2, j+2);
				initialBoard.v[i] |= ((ull) c) << (4 * j);
				initialBoard.val += candVal[c];
			}
			initialBoard.v2[i] = initialBoard.v[i];
		}
	}
	ull searchStartTime = getTime();
	hands.reserve(BEAM_WIDTH * 10);
	int boardsNum = 1;
	int nBoardsNum = 0;
	debug("width change:%d %d\n", 0, BEAM_WIDTH);
	int t = 0;
	for (int turn = 0; turn < T; ++turn, t = 1-t) {
		if (turn > 0 && turn % 1024 == 0) {
			ull curTime = getTime();
			ull elapsed = curTime - searchStartTime;
			double timeATurn = 1.0 * elapsed / 1024;
			double expectRestTime = (T - turn) * timeATurn;
			ull restTime = TL - (curTime - startTime);
			BEAM_WIDTH = (int) (((1.0 * BEAM_WIDTH * restTime / expectRestTime) + BEAM_WIDTH) / 2);
			BEAM_WIDTH = min(BEAM_WIDTH, MAX_WIDTH);
			debug("width change:%d %d\n", turn, BEAM_WIDTH);
			searchStartTime = curTime;
		}
		if (turn == 9970) RMCNT_SHIFT += 3;
		if (turn > 9700 && (turn & 31) == 0) {
			ull restTime = getTime() - startTime;
			// debug("%d restTime:%.3f\n", turn, restTime / 1000.0);
			if (restTime > TL2) {
				BEAM_WIDTH = 10; // emergency mode
			}
		}
		hands.clear();

		for (bi = 0; bi < boardsNum; ++bi) {
			Board& b = gBoards[t][bi];
			START_TIMER(0);
			copy_n(&(b.f[2]), N, &origField[2]);
			STOP_TIMER(0);
			START_TIMER(8);
			uint32_t nextMove = b.hand.next();
			if (nextMove != 0) {
				Hand h(nextMove, 0, bi);
				h.v = BoardSupport::evaluate(b, h.r(), h.c(), h.d());
				hands.push_back(h);
				continue;
			}
			STOP_TIMER(8);
			START_TIMER(1);
			easyScore = (b.score << RMCNT_SHIFT) + b.val;
				for (int i = 2; i < N + 1; ++i) {
					ull cs = b.v2[i-2];
					while (cs != 0) {
						int left = __builtin_ctzll(cs) & ~3;
						ull css = cs & (0xFULL << left);
						BoardSupport::evaluateBlock(b, i, (left >> 2) + 2, css >> left);
						cs ^= css;
					}
				}
			STOP_TIMER(1);
		}

		if (hands.empty()) {
			// panic
			ADD_COUNTER(14);
			START_TIMER(7);
			for (bi = 0; bi < boardsNum; ++bi) {
				Board& b = gBoards[t][bi];
				copy_n(&(b.f[2]), N, &origField[2]);
				easyScore = (b.score << RMCNT_SHIFT) + b.val;
				for (int i = 2; i < N + 1; ++i) {
					for (int j = 2; j < N + 1; ++j) {
						ull mask = 0x7ULL << (j * 3);
						if ((b.f[i] & mask) != ((b.f[i] >> 3) & mask)) {
							int value = BoardSupport::evaluateEasy(b);
							hands.push_back(Hand(i, j, 1, value, bi));
						}
						if ((b.f[i] & mask) != (b.f[i + 1] & mask)) {
							int value = BoardSupport::evaluateEasy(b);
							hands.push_back(Hand(i, j, 2, value, bi));
						}
					}
				}
			}
			STOP_TIMER(7);
		}

		START_TIMER(3);
		int REM_WIDTH = BEAM_WIDTH * 3 / 2;
		if (hands.size() > REM_WIDTH) {
			nth_element(hands.begin(), hands.begin() + REM_WIDTH - 1, hands.end());
		}
		sort(hands.begin(), hands.begin() + min(REM_WIDTH, (int)hands.size()));
		STOP_TIMER(3);

		START_TIMER(4);
		int bestVal = 0;
		for (int i = 0; nBoardsNum < BEAM_WIDTH && i < hands.size() && i < REM_WIDTH; ++i) {
			Hand& h = hands[i];
			const Board& b = gBoards[t][h.boardI()];
			Board& nb = gBoards[1-t][nBoardsNum];
			START_TIMER(5);
			memcpy(&nb, &b, sizeof(Board) - sizeof(Hand));
			nb.hand = h;
			STOP_TIMER(5);
			START_TIMER(6);
			BoardSupport::swapAndRemove(nb, h.r(), h.c(), h.d());
			if (nb.val < bestVal - (3 << RMCNT_SHIFT)) continue;
			bool duplicate = false;
			if ((turn & 7) == 0) {
				for (int j = 0; j < nBoardsNum; ++j) {
					if (gBoards[1-t][j].val != nb.val) continue;
					bool same = true;
					for (int r = 0; r < N; ++r) {
						if (gBoards[1-t][j].f[r+2] != nb.f[r+2]) {
							same = false;
							break;
						}
					}
					if (same) {
						duplicate = true;
						break;
					}
				}
			}
			STOP_TIMER(6);
			if (duplicate) continue;
			handsHist[turn+1][nBoardsNum] = h.param;
			bestVal = max(bestVal, nb.val);
			++nBoardsNum;
		}
		STOP_TIMER(4);

		boardsNum = nBoardsNum;
		nBoardsNum = 0;
	}

	const Board* maxBoard = &gBoards[t][0];
	for (int i = 1; i < boardsNum; ++i) {
		if (gBoards[t][i].score > maxBoard->score) {
			maxBoard = &gBoards[t][i];
		}
	}
	int history = maxBoard->hand.param;
	for (int i = T - 1; i >= 0; --i) {
		ans[3 * i] = history & 0xF;
		ans[3 * i + 1] = (history >> 4) & 0xF;
		ans[3 * i + 2] = (history >> 8) & 0x3;
		history = handsHist[i][(history >> 10) & 0xFFF];
	}
	return maxBoard->score;
}

template<int N> void SquareRemover::solvePre(const vector<string>& boardStr) {
	typedef BoardSupport<N> BoardSupport;
	Board& initialBoard = gBoards[0][0];
	for (int i = 0; i < N; ++i) {
		initialBoard.f[i+2] = 0;
		for (int j = 0; j < N; ++j) {
			initialBoard.f[i+2] |= ((ull) (boardStr[i][j] - '0' + 1)) << (3 * (j + 2));
		}
	}
	initialBoard.score = BoardSupport::remove(initialBoard, 2, 2, N, N);
	debug("initial score:%d\n", initialBoard.score);

#ifndef DEBUG
	solve<N>();
#else
	Board debugBoard = initialBoard;
	int finalScore = solve<N>();
	int stopTurn = -1;
	rng::setPos(debugBoard.score * 2);
	int histS[20] = {};
	int histE[15] = {};
	int prevS = debugBoard.score;
	int contZero = 0;
	for (int i = 0; i < T; ++i) {
		BoardSupport::swapAndRemove(debugBoard, ans[i * 3] + 2, ans[i * 3 + 1] + 2, ans[i * 3 + 2]);
		int diff = debugBoard.score - prevS;
		prevS += diff;
		if (diff == 0) {
			contZero++;
		} else {
			histE[contZero]++;
			contZero = 0;
		}
		histS[diff]++;
		if (stopTurn == -1 && prevS >= finalScore * 9 / 10) {
			stopTurn = i;
		}
	}
	cerr << "histS:[";
	for (int i = 0; i < sizeof(histS) / sizeof(histS[0]); ++i) {
		cerr << histS[i] << ", ";
	}
	cerr << "]" << endl;
	cerr << "histE:[";
	for (int i = 0; i < sizeof(histE) / sizeof(histE[0]); ++i) {
		cerr << histE[i] << ", ";
	}
	cerr << "]" << endl;
	if (SUBMARINE) {
		for (int i = stopTurn; i < T; ++i) {
			ans[i * 3] = ans[i * 3 + 1] = 0;
			ans[i * 3 + 2] = 1;
		}
	}

	debug("score:%d\n", finalScore);
	PRINT_TIMER();
	PRINT_COUNTER();
#endif
}

vector<int> SquareRemover::playIt(int colors, const vector<string>& boardStr, int startSeed) {
	int L = boardStr.size();
	C = colors;
	rng::init(startSeed);
	memset(gBoards, 0, sizeof(gBoards));
	RMCNT_SHIFT = 7;
	for (int i = 1; i < 8; ++i) {
		for (int j = 1; j < 8; ++j) {
			for (int k = 1; k < 8; ++k) {
				for (int l = 1; l < 8; ++l) {
					int idx = (j << 9) | (i << 6) | (l << 3) | k;
					if (i == j && j == k && k == l) {
						colorsVal[idx] = 5;
						cand[idx] = CAND_ALL;
					} else if (i == j && j == k) {
						colorsVal[idx] = 4;
						cand[idx] = CAND_THREE_BR;
					} else if (i == j && j == l) {
						colorsVal[idx] = 4;
						cand[idx] = CAND_THREE_BL;
					} else if (i == k && k == l) {
						colorsVal[idx] = 4;
						cand[idx] = CAND_THREE_TR;
					} else if (j == k && k == l) {
						colorsVal[idx] = 4;
						cand[idx] = CAND_THREE_TL;
					} else if (i == j && k == l) {
						colorsVal[idx] = 3;
						cand[idx] = C == 4 ? 0 : CAND_TWO_DOUBLE_HORZ;
					} else if (i == k && j == l) {
						colorsVal[idx] = 3;
						cand[idx] = C == 4 ? 0 : CAND_TWO_DOUBLE_VERT;
					} else if (i == l && j == k) {
						colorsVal[idx] = 3;
						cand[idx] = C == 4 ? 0 : CAND_TWO_DOUBLE_X;
					} else if (i == j) {
						colorsVal[idx] = 2;
						cand[idx] = C == 4 ? 0 : CAND_TWO_T;
					} else if (k == l) {
						colorsVal[idx] = 2;
						cand[idx] = C == 4 ? 0 : CAND_TWO_B;
					} else if (i == k) {
						colorsVal[idx] = 2;
						cand[idx] = C == 4 ? 0 : CAND_TWO_L;
					} else if (j == l) {
						colorsVal[idx] = 2;
						cand[idx] = C == 4 ? 0 : CAND_TWO_R;
					} else if (i == l) {
						colorsVal[idx] = 2;
						cand[idx] = C == 4 ? 0 : CAND_TWO_BK;
					} else if (j == k) {
						colorsVal[idx] = 2;
						cand[idx] = C == 4 ? 0 : CAND_TWO_SL;
					} else {
						colorsVal[idx] = 0;
						cand[idx] = 0;
					}
				}
			}
		}
	}
	candVal[CAND_ALL] = COLORS_VAL_BASE[5];
	candVal[CAND_THREE_BR] = COLORS_VAL_BASE[4];
	candVal[CAND_THREE_BL] = COLORS_VAL_BASE[4];
	candVal[CAND_THREE_TR] = COLORS_VAL_BASE[4];
	candVal[CAND_THREE_TL] = COLORS_VAL_BASE[4];
	candVal[CAND_TWO_DOUBLE_HORZ] = COLORS_VAL_BASE[3];
	candVal[CAND_TWO_DOUBLE_VERT] = COLORS_VAL_BASE[3];
	candVal[CAND_TWO_DOUBLE_X] = COLORS_VAL_BASE[3];
	candVal[CAND_TWO_T] = COLORS_VAL_BASE[2];
	candVal[CAND_TWO_B] = COLORS_VAL_BASE[2];
	candVal[CAND_TWO_L] = COLORS_VAL_BASE[2];
	candVal[CAND_TWO_R] = COLORS_VAL_BASE[2];
	candVal[CAND_TWO_BK] = COLORS_VAL_BASE[2];
	candVal[CAND_TWO_SL] = COLORS_VAL_BASE[2];
	candVal[0] = COLORS_VAL_BASE[1];

	switch (L) {
		case 8:
			solvePre<8>(boardStr);
			break;
		case 9:
			if (!EASY_COMPILE) solvePre<9>(boardStr);
			break;
		case 10:
			if (!EASY_COMPILE) solvePre<10>(boardStr);
			break;
		case 11:
			solvePre<11>(boardStr);
			break;
		case 12:
			if (!EASY_COMPILE) solvePre<12>(boardStr);
			break;
		case 13:
			if (!EASY_COMPILE) solvePre<13>(boardStr);
			break;
		case 14:
			solvePre<14>(boardStr);
			break;
		case 15:
			if (!EASY_COMPILE) solvePre<15>(boardStr);
			break;
		case 16:
			if (!EASY_COMPILE) solvePre<16>(boardStr);
			break;
	}

	return ans;
}

// int main() {
// 	int c, n, seed;
// 	cin >> c >> n;
// 	vector<string> board;
// 	for (int i = 0; i < n; ++i) {
// 		string line;
// 		cin >> line;
// 		board.push_back(line);
// 	}
// 	cin >> seed;
// 	SquareRemover obj;
// 	vector<int> ret = obj.playIt(c, board, seed);
// 	for (int i = 0; i < ret.size(); ++i) {
// 		cout << ret[i] << endl;
// 	}
// 	cout.flush();
// }

