import java.io.FileWriter;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;

class PseudorandomGenerator {
	final static int MOD = Integer.MAX_VALUE;
	final static int MUL = 48271;
	final static int KEEP = 20;
	LinkedList<Integer> next = new LinkedList<Integer>();
	int range, last;

	public int getNextColor() {
		int ans = next.poll();
		last = (int) (((long) last * MUL) % MOD);
		next.add(last % range);
		return ans;
	}

	public PseudorandomGenerator(int seed, int range) {
		this.range = range;
		int cur = seed;
		for (int i = 0; i < KEEP; i++) {
			last = cur;
			next.add(cur % range);
			cur = (int) (((long) cur * MUL) % MOD);
		}
	}
}

class TestCase {
	final int MIN_COLORS = 4, MAX_COLORS = 6;
	final int MIN_N = 8, MAX_N = 16;
	int colors;
	int N;
	int startSeed;
	String[] board;

	public TestCase(long seed) {
		SecureRandom rnd = null;
		try {
			rnd = SecureRandom.getInstance("SHA1PRNG");
		} catch (Exception e) {
			System.err.println("ERROR: unable to generate test case.");
			System.exit(1);
		}
		rnd.setSeed(seed);
		colors = rnd.nextInt(MAX_COLORS - MIN_COLORS + 1) + MIN_COLORS;
		N = rnd.nextInt(MAX_N - MIN_N + 1) + MIN_N;
		startSeed = rnd.nextInt(PseudorandomGenerator.MOD - 1) + 1;
		if (1000 <= seed && seed < 1300) {
			colors = ((int) seed - 1000) / 100 + 4;
			int sizeGen = (int) seed % 100;
			N = (int) (sizeGen * (MAX_N - MIN_N + 1) / 100.0) + MIN_N;
		}
		board = new String[N];
		for (int i = 0; i < N; i++) {
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < N; j++) {
				sb.append((char) ((int) '0' + rnd.nextInt(colors)));
			}
			board[i] = sb.toString();
		}

		//		try (FileWriter writer = new FileWriter(String.format("testdata/%04d.txt", seed))) {
		//			writer.write(N + " " + colors + " " + startSeed + "\n");
		//			for (int i = 0; i < N; ++i) {
		//				writer.write(board[i] + "\n");
		//			}
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//		}
	}
}

class World {
	final static int[] DR = new int[] { -1, 0, 1, 0 };
	final static int[] DC = new int[] { 0, 1, 0, -1 };

	int N;
	int[][] board;
	PseudorandomGenerator pg;
	int score;

	public World(TestCase tc) {
		N = tc.N;
		board = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				board[i][j] = tc.board[i].charAt(j) - '0';
			}
		}
		pg = new PseudorandomGenerator(tc.startSeed, tc.colors);
	}

	public void removeSquares() {
		while (true) {
			boolean find = false;
			for (int i = 0; i + 1 < N && !find; i++) {
				for (int j = 0; j + 1 < N && !find; j++) {
					if (board[i][j] == board[i][j + 1] && board[i][j] == board[i + 1][j] && board[i][j] == board[i + 1][j + 1]) {
						find = true;
						score++;
						board[i][j] = pg.getNextColor();
						board[i][j + 1] = pg.getNextColor();
						board[i + 1][j] = pg.getNextColor();
						board[i + 1][j + 1] = pg.getNextColor();
					}
				}
			}
			if (!find) break;
		}
	}

	public void applyMove(int moveId, int r1, int c1, int dir) throws Exception {
		if (r1 < 0 || r1 >= N || c1 < 0 || c1 >= N) {
			throw new Exception("In move " + moveId + " (0-based), the cell chosen by your solution is outside"
					+ " the board: row = " + r1 + ", col = " + c1 + ".");
		}
		int r2 = r1 + DR[dir];
		int c2 = c1 + DC[dir];
		if (r2 < 0 || r2 >= N || c2 < 0 || c2 >= N) {
			throw new Exception("In move " + moveId + " (0-based), the direction chosen by your solution"
					+ " points to a cell outside the board: row = " + r2 + ", col = " + c2 + ".");
		}
		int tmp = board[r1][c1];
		board[r1][c1] = board[r2][c2];
		board[r2][c2] = tmp;
	}
}

public class Tester {
	public static final int ANSWER_STEPS = 10000;
	public static final int ANSWER_LENGTH = 3 * ANSWER_STEPS;

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		TestCase tc = new TestCase(seed);
		res.C = tc.colors;
		res.N = tc.board.length;
		long starttime = System.currentTimeMillis();
		try {
			SquareRemover obj = new SquareRemover();
			int[] ans = obj.playIt(tc.colors, tc.board, tc.startSeed);
			res.elapsed = System.currentTimeMillis() - starttime;

			World world = new World(tc);
			world.removeSquares();
			for (int i = 0; i < ANSWER_STEPS; i++) {
				try {
					world.applyMove(i, ans[3 * i], ans[3 * i + 1], ans[3 * i + 2]);
				} catch (Exception e) {
					System.err.println("ERROR: " + e.getMessage());
					return res;
				}
				world.removeSquares();
			}

			res.score = world.score;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) {
		long seed = 1;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-b")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-e")) {
				end = Long.parseLong(args[++i]);
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}
		try {
			if (begin != -1 && end != -1) {
				ArrayList<Long> seeds = new ArrayList<Long>();
				for (long i = begin; i <= end; ++i) {
					seeds.add(i);
				}
				int len = seeds.size();
				Result[] results = new Result[len];
				TestThread[] threads = new TestThread[THREAD_COUNT];
				int prev = 0;
				for (int i = 0; i < THREAD_COUNT; ++i) {
					int next = len * (i + 1) / THREAD_COUNT;
					threads[i] = new TestThread(prev, next - 1, seeds, results);
					prev = next;
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].start();
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].join();
				}
				double sum = 0;
				for (int i = 0; i < results.length; ++i) {
					System.out.println(results[i]);
					System.out.println();
					sum += results[i].score;
				}
				System.out.println("ave:" + (sum / results.length));
			} else {
				//				Thread.sleep(20000);
				Tester tester = new Tester();
				Result res = tester.runTest(seed);
				System.err.println(res);
			}
		} catch (Exception e) {
			System.err.println("ERROR: Unexpected error while running your test case.");
			e.printStackTrace();
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int N, C;
		double score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d   C:%1d   N:%2d\n", seed, C, N);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			return ret + "score:  " + this.score;
		}
	}

}
