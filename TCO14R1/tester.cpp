#include <vector>
#include <string>
#include <cstdio>
#include <iostream>
#include <fstream>
#include "SquareRemover.cpp"

struct Result {
	int seed,N,C;
	int score;
	ll elapsed;
};

void print(const Result& res) {
	printf("seed:%4d   C:%d   N:%2d\n", res.seed, res.C, res.N);
	printf("elapsed:%.5f\n", res.elapsed / 1000.0);
	printf("score:  %d\n", res.score);
}

struct RNG {
	uint64_t cur;
	int C;

	RNG(int init, int C_) : cur(init), C(C_) {}

	int next() {
		int ret = cur % C;
		cur = (cur * 48271) % 2147483647;
		return ret;
	}
};

int remove(vector<string>& board, RNG& rng) {
	int ret = 0;
	while (true) {
		bool found = false;
		for (int i = 0; i < board.size()-1 && !found; ++i) {
			for (int j = 0; j < board.size() - 1 && !found; ++j) {
				if (board[i][j] == board[i][j+1] && board[i][j] == board[i+1][j] && board[i][j] == board[i+1][j+1]) {
					++ret;
					board[i  ][j  ] = (char)(rng.next() + '0');
					board[i  ][j+1] = (char)(rng.next() + '0');
					board[i+1][j  ] = (char)(rng.next() + '0');
					board[i+1][j+1] = (char)(rng.next() + '0');
					found = true;
				}
			}
		}
		if (!found) break;
	}
	return ret;
}

bool inside(int pos, int N) {
	return 0 <= pos && pos < N;
}

int calcScore(vector<string>& board, vector<int> hands, const Result& res, int startSeed) {
	int ret = 0;
	RNG rng(startSeed, res.C);
	ret += remove(board, rng);
	for (int t = 0; t < hands.size() / 3; ++t) {
		int r = hands[t * 3];
		int c = hands[t * 3 + 1];
		int d = hands[t * 3 + 2];
		if (!inside(r, res.N) || !inside(c, res.N) || !inside(r + DR[d], res.N) || !inside(c + DC[d], res.N)) {
			cerr << "invalid position:" << r << " " << c << " " << d << endl;
		}
		swap(board[r][c], board[r + DR[d]][c + DC[d]]);
		ret += remove(board, rng);
	}
	return ret;
}

Result runTest(int seed) {
	Result res;
	res.seed = seed;
	char fileName[30];
	sprintf(fileName, "testdata/%04d.txt", seed);
	fstream stm(fileName);
	int startSeed;
	stm >> res.N >> res.C >> startSeed;
	vector<string> board;
	for (int i = 0; i < res.N; ++i) {
		string line;
		stm >> line;
		board.push_back(line);
	}
	ll before = getTime();
	SquareRemover obj;
	vector<int> ret = obj.playIt(res.C, board, startSeed);
	res.elapsed = getTime() - before;
	res.score = calcScore(board, ret, res, startSeed);
	return res;
}

int main(int argc, char** argv) {
	int seed = 1, begin = -1, end = -1;
	for (int i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "-seed") == 0) {
			seed = atoi(argv[++i]);
		} else if (strcmp(argv[i], "-b") == 0) {
			begin = atoi(argv[++i]);
		} else if (strcmp(argv[i], "-e") == 0) {
			end = atoi(argv[++i]);
		}
	}
	if (begin != -1 && end != -1) {
		int sum = 0;
		for (int i = begin; i <= end; ++i) {
			Result res = runTest(i);
			print(res);
			printf("\n");
			fflush(stdout);
			sum += res.score;
		}
		printf("ave:%f\n", 1.0 + sum / (end - begin + 1));
	} else {
		Result res = runTest(seed);
		print(res);
	}
}

