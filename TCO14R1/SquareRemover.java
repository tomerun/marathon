import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class SquareRemover {
	private static final boolean DEBUG = 1 == 1;
	private static final boolean MEASURE_TIME = 1 == 1;
	private static final boolean SUBMARINE = false;
	private static final long TL = 14000;
	private static final int T = 10000;
	private int RMCNT_SHIFT = 7;
	private int RND_VAL = 256;
	private static final int[] DR = { -1, 0, 1, 0, -1, 0, 1, 0, };
	private static final int[] DC = { 0, 1, 0, -1, 0, 1, 0, -1, };
	private static final int[] COLORS_VAL_BASE = { 0, 0, 8, 16, 27, 64 };
	private static final int INITIAL_BEAM_WIDTH = 4500;
	private static final int CAND_THREE = 1 << 10;
	private static final int CAND_THREE_BR = CAND_THREE + 1;
	private static final int CAND_THREE_BL = CAND_THREE + 2;
	private static final int CAND_THREE_TR = CAND_THREE + 3;
	private static final int CAND_THREE_TL = CAND_THREE + 4;
	private static final int CAND_TWO_DOUBLE = 1 << 9;
	private static final int CAND_TWO_DOUBLE_LR = CAND_TWO_DOUBLE + 1;
	private static final int CAND_TWO_DOUBLE_TB = CAND_TWO_DOUBLE + 2;
	private static final int CAND_TWO_DOUBLE_X = CAND_TWO_DOUBLE + 3;
	private static final int CAND_TWO = 1 << 8;
	private static final int CAND_TWO_T = CAND_TWO + 1;
	private static final int CAND_TWO_B = CAND_TWO + 2;
	private static final int CAND_TWO_L = CAND_TWO + 3;
	private static final int CAND_TWO_R = CAND_TWO + 4;
	private static final int CAND_TWO_SL = CAND_TWO + 5;
	private static final int CAND_TWO_BK = CAND_TWO + 6;
	private final XorShift rnd = new XorShift();
	private final long startTime = System.currentTimeMillis();
	Timer timer = new Timer();
	Counter counter = new Counter();
	int[] colorsVal = new int[1 << 12];
	int[] cand = new int[1 << 12];
	Gen gen;
	int C, N;
	int[] ans = new int[T * 3];
	long[] origField;
	Board initialBoard;
	int destT, destB, destL, destR;

	public int[] playIt(int colors, String[] boardStr, int startSeed) {
		C = colors;
		N = boardStr.length;
		gen = new Gen(C, startSeed);
		long[] init = new long[N];
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				init[i] |= ((long) (boardStr[i].charAt(j) - '0')) << (3 * j);
			}
		}
		initialBoard = new Board(init);
		initialBoard.score = initialBoard.remove(0, 0, N - 2, N - 2);
		origField = new long[N];
		for (int i = 0; i < 8; ++i) {
			for (int j = 0; j < 8; ++j) {
				for (int k = 0; k < 8; ++k) {
					for (int l = 0; l < 8; ++l) {
						int idx = (j << 9) | (i << 6) | (l << 3) | k;
						if (i == j && j == k && k == l) {
							colorsVal[idx] = 5;
						} else if (i == j && j == k) {
							colorsVal[idx] = 4;
							cand[idx] = CAND_THREE_BR;
						} else if (i == j && j == l) {
							colorsVal[idx] = 4;
							cand[idx] = CAND_THREE_BL;
						} else if (i == k && k == l) {
							colorsVal[idx] = 4;
							cand[idx] = CAND_THREE_TR;
						} else if (j == k && k == l) {
							colorsVal[idx] = 4;
							cand[idx] = CAND_THREE_TL;
						} else if (i == j && k == l) {
							colorsVal[idx] = 3;
							cand[idx] = CAND_TWO_DOUBLE_LR;
						} else if (i == k && j == l) {
							colorsVal[idx] = 3;
							cand[idx] = CAND_TWO_DOUBLE_TB;
						} else if (i == l && j == k) {
							colorsVal[idx] = 3;
							cand[idx] = CAND_TWO_DOUBLE_X;
						} else if (i == j) {
							colorsVal[idx] = 2;
							cand[idx] = CAND_TWO_T;
						} else if (k == l) {
							colorsVal[idx] = 2;
							cand[idx] = CAND_TWO_B;
						} else if (i == k) {
							colorsVal[idx] = 2;
							cand[idx] = CAND_TWO_L;
						} else if (j == l) {
							colorsVal[idx] = 2;
							cand[idx] = CAND_TWO_R;
						} else if (i == l) {
							colorsVal[idx] = 2;
							cand[idx] = CAND_TWO_BK;
						} else if (j == k) {
							colorsVal[idx] = 2;
							cand[idx] = CAND_TWO_SL;
						} else {
							colorsVal[idx] = 1;
						}
					}
				}
			}
		}

		int finalScore = solve();
		for (int i = 0; i < T; ++i) {
			ans[i * 3 + 2] &= 3;
		}

		if (DEBUG) {
			int stopTurn = -1;
			gen.setPos(initialBoard.score * 2);
			int[] histS = new int[15];
			int[] histE = new int[15];
			int prevS = initialBoard.score;
			int contZero = 0;
			for (int i = 0; i < T; ++i) {
				initialBoard.swapAndRemove(ans[i * 3], ans[i * 3 + 1], ans[i * 3 + 2]);
				int diff = initialBoard.score - prevS;
				prevS += diff;
				if (diff == 0) {
					contZero++;
				} else {
					histE[contZero]++;
					contZero = 0;
				}
				histS[diff]++;
				if (stopTurn == -1 && prevS >= finalScore * 9 / 10) {
					stopTurn = i;
				}
			}
			debug("histS", histS);
			debug("histE", histE);
			if (SUBMARINE) {
				for (int i = stopTurn; i < T; ++i) {
					ans[i * 3] = ans[i * 3 + 1] = 0;
					ans[i * 3 + 2] = 1;
				}
			}
		}
		debug("score", finalScore);
		timer.print();
		counter.print();
		return ans;
	}

	ArrayList<Hand> handsBoard = new ArrayList<>();
	int bi;

	int solve() {
		int BEAM_WIDTH = INITIAL_BEAM_WIDTH / N;
		int CAP_SAME_PARENT = BEAM_WIDTH / 15;
		ArrayList<Board> bs = new ArrayList<>();
		{
			Board board = new Board(initialBoard.f);
			board.score = initialBoard.score;
			for (int i = 0; i < N - 1; ++i) {
				for (int j = 0; j < N - 1; ++j) {
					int c = board.colors(i, j);
					board.v[i] |= ((long) c) << (4 * j);
					board.val += COLORS_VAL_BASE[c];
				}
			}
			board.hand = new Hand(0, 0, 0, null);
			bs.add(board);
		}
		long searchStartTime = System.currentTimeMillis();
		ArrayList<Board> next = new ArrayList<>(BEAM_WIDTH);
		ArrayList<Hand> hands = new ArrayList<>(BEAM_WIDTH * 10);
		for (int turn = 0; turn < T; ++turn) {
			if (turn > 0 && turn % 1024 == 0) {
				long curTime = System.currentTimeMillis();
				long elapsed = curTime - searchStartTime;
				double timeATurn = 1.0 * elapsed / 1024;
				double expectRestTime = (T - turn) * timeATurn;
				long restTime = TL - (curTime - startTime);
				BEAM_WIDTH = (int) (((1.0 * BEAM_WIDTH * restTime / expectRestTime) + BEAM_WIDTH) / 2);
				CAP_SAME_PARENT = Math.max(3, BEAM_WIDTH / 15);
				debug("width change", turn, BEAM_WIDTH);
				searchStartTime = curTime;
			}
			if (turn == 9970) RMCNT_SHIFT += 3;
			hands.clear();
			for (bi = 0; bi < bs.size(); ++bi) {
				Board b = bs.get(bi);
				timer.start(0);
				System.arraycopy(b.f, 0, origField, 0, N);
				timer.stop(0);
				timer.start(8);
				if (b.hand.next != -1) {
					Hand h = new Hand(b.hand.next, 0, bi, b.hand);
					int v = b.evaluate(h.r(), h.c(), h.d());
					h.v = v;
					hands.add(h);
					continue;
				}
				timer.stop(8);
				timer.start(1);
				handsBoard.clear();
				for (int i = 0; i < N - 1; ++i) {
					for (int j = 0; j < N - 1; ++j) {
						b.evaluateBlock(i, j);
					}
				}
				timer.stop(1);
				timer.start(2);
				if (handsBoard.size() > CAP_SAME_PARENT) {
					partition(handsBoard, CAP_SAME_PARENT);
					for (int i = 0; i < CAP_SAME_PARENT; ++i) {
						hands.add(handsBoard.get(i));
					}
				} else {
					hands.addAll(handsBoard);
				}
				timer.stop(2);
			}
			if (hands.isEmpty()) {
				// panic
				timer.start(7);
				for (bi = 0; bi < bs.size(); ++bi) {
					Board b = bs.get(bi);
					System.arraycopy(b.f, 0, origField, 0, N);
					handsBoard.clear();
					for (int i = 0; i < N - 1; ++i) {
						for (int j = 0; j < N - 1; ++j) {
							long mask = 0x7L << (j * 3);
							if ((b.f[i] & mask) != ((b.f[i] >> 3) & mask)) {
								int value = b.evaluateEasy();
								handsBoard.add(new Hand(i, j, 1, value, bi, b.hand));
							}
							if ((b.f[i] & mask) != (b.f[i + 1] & mask)) {
								int value = b.evaluateEasy();
								handsBoard.add(new Hand(i, j, 2, value, bi, b.hand));
							}
						}
					}
					if (handsBoard.size() > CAP_SAME_PARENT) {
						partition(handsBoard, CAP_SAME_PARENT);
						for (int i = 0; i < CAP_SAME_PARENT; ++i) {
							hands.add(handsBoard.get(i));
						}
					} else {
						hands.addAll(handsBoard);
					}
				}
				timer.stop(7);
			}

			timer.start(3);
			if (hands.size() > BEAM_WIDTH) {
				partition(hands, BEAM_WIDTH);
			}
			timer.stop(3);

			timer.start(4);
			for (int i = 0; next.size() < BEAM_WIDTH && i < hands.size(); ++i) {
				Hand h = hands.get(i);
				timer.start(5);
				Board nb = new Board(bs.get(h.boardI()));
				timer.stop(5);
				timer.start(6);
				nb.swapAndRemove(h.r(), h.c(), h.d());
				timer.stop(6);
				nb.hand = h;
				next.add(nb);
			}
			timer.stop(4);

			bs.clear();
			ArrayList<Board> tmp = bs;
			bs = next;
			next = tmp;
		}

		Board maxBoard = bs.get(0);
		for (int i = 0; i < bs.size(); ++i) {
			if (bs.get(i).score > maxBoard.score) {
				maxBoard = bs.get(i);
			}
		}
		Hand hand = maxBoard.hand;
		for (int i = T - 1; i >= 0; --i) {
			ans[3 * i] = hand.r();
			ans[3 * i + 1] = hand.c();
			ans[3 * i + 2] = hand.d();
			hand = hand.p;
		}
		return maxBoard.score;
	}

	// make largest lim elements occupy first part
	void partition(ArrayList<Hand> hands, int lim) {
		int l = 0;
		int r = hands.size();
		while (true) {
			if (lim == 0 || lim == r - l) break;
			int pp = rnd.nextInt(r - l) + l;
			Hand pivot = hands.get(pp);
			hands.set(pp, hands.get(l));
			hands.set(l, pivot);
			int right = r;
			int left = l + 1;
			while (left < right) {
				int compare = pivot.compareTo(hands.get(left));
				boolean move = compare < 0 || (compare == 0 && rnd.nextBoolean());
				if (move) {
					--right;
					Hand tmp = hands.get(right);
					hands.set(right, hands.get(left));
					hands.set(left, tmp);
				} else {
					++left;
				}
			}
			int large = right - l;
			if (large == lim) break;
			if (large > lim) {
				r = right;
			} else {
				l = right;
				lim -= large;
			}
		}
	}

	class Board {
		long[] f;
		long[] v;
		int score;
		int val;
		Hand hand;

		Board(long[] orig) {
			this.f = orig.clone();
			this.v = new long[N - 1];
		}

		Board(Board orig) {
			this.f = orig.f.clone();
			this.v = orig.v.clone();
			score = orig.score;
			val = orig.val;
		}

		void swapAndRemove(int r, int c, int dir) {
			int nr = r + DR[dir];
			int nc = c + DC[dir];
			long c1 = (f[r] >> (c * 3)) & 0x7;
			long c2 = (f[nr] >> (nc * 3)) & 0x7;
			if (c1 == c2) return;
			f[r] ^= (c1 << (c * 3));
			f[r] |= (c2 << (c * 3));
			f[nr] ^= (c2 << (nc * 3));
			f[nr] |= (c1 << (nc * 3));
			score += remove(r, c, nr, nc);
			int top = Math.max(0, destT - 1);
			int bottom = Math.min(destB, N - 2);
			int left = Math.max(0, destL - 1);
			int right = Math.min(destR, N - 2);
			for (int i = top; i <= bottom; ++i) {
				for (int j = left; j <= right; ++j) {
					int colors = colors(i, j);
					int oldColors = (int) ((v[i] >> (j * 4)) & 0xF);
					val += COLORS_VAL_BASE[colors];
					val -= COLORS_VAL_BASE[oldColors];
					v[i] ^= ((long) oldColors ^ colors) << (j * 4);
				}
			}
		}

		int evaluate(int r, int c, int dir) {
			int nr = r;
			int nc = c;
			switch (dir & 3) {
			case 0:
				--r;
				break;
			case 1:
				++nc;
				break;
			case 2:
				++nr;
				break;
			case 3:
				--c;
				break;
			}
			long c1 = (f[r] >> (c * 3)) & 0x7;
			long c2 = (f[nr] >> (nc * 3)) & 0x7;
			f[r] ^= (c1 << (c * 3));
			f[r] |= (c2 << (c * 3));
			f[nr] ^= (c2 << (nc * 3));
			f[nr] |= (c1 << (nc * 3));

			gen.setPos(this.score * 2);
			int destT = r;
			int destL = c;
			int destB = nr;
			int destR = nc;
			int top = Math.max(0, destT - 1);
			int bottom = Math.min(destB, N - 2);
			int left = Math.max(0, destL - 1);
			int right = Math.min(destR, N - 2);
			int ret = this.score;
			int diff = 0;
			while (true) {
				boolean find = false;
				diff = 0;
				for (int i = top; i <= bottom && !find; ++i) {
					for (int j = left; j <= right && !find; ++j) {
						int here = colors(i, j);
						if (here == 5) {
							find = true;
							destT = Math.min(destT, i);
							destB = Math.max(destB, i + 1);
							destL = Math.min(destL, j);
							destR = Math.max(destR, j + 1);
							top = i == 0 ? 0 : i - 1;
							if (j == left && j != 0) --left;
							if (i == bottom && bottom != N - 2) ++bottom;
							if (j == right && right != N - 2) ++right;
							++ret;
							f[i] &= ~(0x3FL << (j * 3));
							f[i] |= gen.next() << (j * 3);
							f[i + 1] &= ~(0x3FL << (j * 3));
							f[i + 1] |= gen.next() << (j * 3);
						} else {
							diff += COLORS_VAL_BASE[here];
							diff -= COLORS_VAL_BASE[(int) ((v[i] >> (j * 4)) & 0xF)];
						}
					}
				}
				if (!find) {
					break;
				}
			}

			counter.add(ret - this.score);
			ret <<= RMCNT_SHIFT;
			ret += diff + val;

			// restore
			for (int i = destT; i <= destB; ++i) {
				f[i] = origField[i];
			}
			ret += rnd.nextInt(RND_VAL);
			return ret;
		}

		int evaluateEasy() {
			counter.add(0);
			int ret = this.score << RMCNT_SHIFT;
			ret += this.val;
			ret += rnd.nextInt(RND_VAL);
			return ret;
		}

		void evaluateBlock(int r, int c) {
			int colors = (int) ((f[r] >> (c * 3)) & 0x3F) << 6;
			colors |= (int) ((f[r + 1] >> (c * 3)) & 0x3F);
			switch (cand[colors]) {
			case CAND_THREE_BR:
				evaluateBlockTri(r + 1, c + 1, 0);
				break;
			case CAND_THREE_BL:
				evaluateBlockTri(r + 1, c, 1);
				break;
			case CAND_THREE_TL:
				evaluateBlockTri(r, c, 2);
				break;
			case CAND_THREE_TR:
				evaluateBlockTri(r, c + 1, 3);
				break;
			case CAND_TWO_DOUBLE_LR:
				evaluateBlockBi(r, c, 1);
				evaluateBlockBi(r + 1, c + 1, 3);
				break;
			case CAND_TWO_DOUBLE_TB:
				evaluateBlockBi(r, c + 1, 2);
				evaluateBlockBi(r + 1, c, 0);
				break;
			case CAND_TWO_DOUBLE_X:
				evaluateBlockSlash(r, c + 1, 1);
				evaluateBlockSlash(r, c, 0);
				break;
			case CAND_TWO_T:
				evaluateBlockBi(r, c + 1, 2);
				break;
			case CAND_TWO_B:
				evaluateBlockBi(r + 1, c, 0);
				break;
			case CAND_TWO_L:
				evaluateBlockBi(r, c, 1);
				break;
			case CAND_TWO_R:
				evaluateBlockBi(r + 1, c + 1, 3);
				break;
			case CAND_TWO_SL:
				evaluateBlockSlash(r, c + 1, 1);
				break;
			case CAND_TWO_BK:
				evaluateBlockSlash(r, c, 0);
				break;
			}
		}

		void evaluateBlockTri(int r, int c, int dir) {
			int target = (int) (f[r + DR[dir]] >> (3 * (c + DC[dir])) & 0x7);
			boolean reach = false;
			int tr = r + DR[dir + 1];
			int tc = c + DC[dir + 1];
			if (hit(tr, tc, target)) {
				evalAndAdd(r, c, dir + 1);
				reach = true;
			}
			tr = r + DR[dir + 2];
			tc = c + DC[dir + 2];
			if (hit(tr, tc, target)) {
				evalAndAdd(r, c, dir + 2);
				reach = true;
			}
			if (C == 4) return;
			if (reach) return;

			tr = r + DR[dir] * 2;
			tc = c + DC[dir] * 2;
			if (hit(tr, tc, target)) {
				evalAndAddEasy(r, c, dir);
				setNextHand(r + DR[dir], c + DC[dir], dir);
			}
			tr += -DR[dir] + DR[dir + 1];
			tc += -DC[dir] + DC[dir + 1];
			if (hit(tr, tc, target)) {
				evalAndAddEasy(r, c, dir);
				setNextHand(r + DR[dir], c + DC[dir], dir + 1);
				evalAndAddEasy(r + DR[dir + 1], c + DC[dir + 1], dir);
				setNextHand(r, c, dir + 1);
			}
			tr += -DR[dir] + DR[dir + 1];
			tc += -DC[dir] + DC[dir + 1];
			if (hit(tr, tc, target)) {
				evalAndAddEasy(r + DR[dir + 1], c + DC[dir + 1], dir + 1);
				setNextHand(r, c, dir + 1);
			}
			tr += -DR[dir + 1] + DR[dir + 2];
			tc += -DC[dir + 1] + DC[dir + 2];
			if (hit(tr, tc, target)) {
				evalAndAddEasy(r + DR[dir + 1], c + DC[dir + 1], dir + 2);
				setNextHand(r, c, dir + 1);
				evalAndAddEasy(r + DR[dir + 2], c + DC[dir + 2], dir + 1);
				setNextHand(r, c, dir + 2);
			}
			tr += -DR[dir + 1] + DR[dir + 2];
			tc += -DC[dir + 1] + DC[dir + 2];
			if (hit(tr, tc, target)) {
				evalAndAddEasy(r + DR[dir + 2], c + DC[dir + 2], dir + 2);
				setNextHand(r, c, dir + 2);
			}
			tr += -DR[dir + 2] + DR[dir + 3];
			tc += -DC[dir + 2] + DC[dir + 3];
			if (hit(tr, tc, target)) {
				evalAndAddEasy(r, c, dir + 3);
				setNextHand(r + DR[dir + 3], c + DC[dir + 3], dir + 2);
				evalAndAddEasy(r + DR[dir + 2], c + DC[dir + 2], dir + 3);
				setNextHand(r, c, dir + 2);
			}
			tr += -DR[dir + 2] + DR[dir + 3];
			tc += -DC[dir + 2] + DC[dir + 3];
			if (hit(tr, tc, target)) {
				evalAndAddEasy(r, c, dir + 3);
				setNextHand(r + DR[dir + 3], c + DC[dir + 3], dir + 3);
			}
		}

		void evaluateBlockBi(int r, int c, int dir) {
			if (C == 4) return;
			int target = (int) ((f[r] >> (3 * c)) & 0x7);
			int d1 = -1;
			int d2 = -1;
			int tr = r + DR[dir] * 2;
			int tc = c + DC[dir] * 2;
			if (hit(tr, tc, target)) {
				d1 = dir;
			} else {
				tr = r + DR[dir] + DR[dir + 3];
				tc = c + DC[dir] + DC[dir + 3];
				if (hit(tr, tc, target)) {
					d1 = dir + 3;
				}
			}
			if (d1 == -1) return;

			tr = r + DR[dir] * 2 + DR[dir + 1];
			tc = c + DC[dir] * 2 + DC[dir + 1];
			if (hit(tr, tc, target)) {
				d2 = dir;
			} else {
				tr = r + DR[dir] + DR[dir + 1] * 2;
				tc = c + DC[dir] + DC[dir + 1] * 2;
				if (hit(tr, tc, target)) {
					d2 = dir + 1;
				}
			}
			if (d2 == -1) return;

			evalAndAddEasy(r + DR[dir], c + DC[dir], d1);
			setNextHand(r + DR[dir] + DR[dir + 1], c + DC[dir] + DC[dir + 1], d2);
		}

		void evaluateBlockSlash(int r, int c, int dir) {
			if (C == 4) return;
			int target = (int) ((f[r] >> (3 * c)) & 0x7);
			int d1 = -1;
			int d2 = -1;
			int tr = r + DR[dir + 1] * 2;
			int tc = c + DC[dir + 1] * 2;
			if (hit(tr, tc, target)) {
				d1 = dir + 1;
			} else {
				tr = r + DR[dir] + DR[dir + 1];
				tc = c + DC[dir] + DC[dir + 1];
				if (hit(tr, tc, target)) {
					d1 = dir;
				}
			}
			if (d1 == -1) return;

			tr = r + DR[dir + 2] * 2;
			tc = c + DC[dir + 2] * 2;
			if (hit(tr, tc, target)) {
				d2 = dir + 2;
			} else {
				tr = r + DR[dir + 2] + DR[dir + 3];
				tc = c + DC[dir + 2] + DC[dir + 3];
				if (hit(tr, tc, target)) {
					d2 = dir + 3;
				}
			}
			if (d2 == -1) return;

			evalAndAddEasy(r + DR[dir + 1], c + DC[dir + 1], d1);
			setNextHand(r + DR[dir + 2], c + DC[dir + 2], d2);
		}

		boolean hit(int r, int c, int target) {
			return 0 <= r && r < N && 0 <= c && c < N && ((f[r] >> (3 * c)) & 0x7) == target;
		}

		void evalAndAdd(int r, int c, int d) {
			int v = evaluate(r, c, d);
			handsBoard.add(new Hand(r, c, d, v, bi, this.hand));
		}

		void evalAndAddEasy(int r, int c, int d) {
			int v = evaluateEasy();
			handsBoard.add(new Hand(r, c, d, v, bi, this.hand));
		}

		void setNextHand(int r, int c, int d) {
			handsBoard.get(handsBoard.size() - 1).next = r | (c << 4) | ((d & 3) << 8);
		}

		int remove(int top, int left, int bottom, int right) {
			gen.setPos(this.score * 2);
			int ret = 0;
			if (top > bottom) {
				int tmp = top;
				top = bottom;
				bottom = tmp;
			}
			if (left > right) {
				int tmp = left;
				left = right;
				right = tmp;
			}
			destT = top;
			destL = left;
			destB = bottom;
			destR = right;
			if (top > 0) --top;
			if (left > 0) --left;
			if (bottom == N - 1) --bottom;
			if (right == N - 1) --right;
			while (true) {
				boolean find = false;
				for (int i = top; i <= bottom && !find; ++i) {
					long mask = 0x3FL << (left * 3);
					for (int j = left; j <= right && !find; ++j, mask <<= 3) {
						long f1 = f[i] & mask;
						long f2 = f[i + 1] & mask;
						if (f1 == f2 && (((f1 >> 3) & (0x7L << (3 * j))) == (f1 & (0x7L << (3 * j))))) {
							find = true;
							destT = Math.min(destT, i);
							destB = Math.max(destB, i + 1);
							destL = Math.min(destL, j);
							destR = Math.max(destR, j + 1);
							top = i == 0 ? 0 : i - 1;
							if (j == left && j != 0) --left;
							if (i == bottom && bottom != N - 2) ++bottom;
							if (j == right && right != N - 2) ++right;
							++ret;
							f[i] ^= f1;
							f[i] |= gen.next() << (j * 3);
							f[i + 1] ^= f2;
							f[i + 1] |= gen.next() << (j * 3);
						}
					}
				}
				if (!find) break;
			}
			return ret;
		}

		int colors(int r, int c) {
			int colors = (int) ((f[r] >> (c * 3)) & 0x3F) << 6;
			colors |= (int) ((f[r + 1] >> (c * 3)) & 0x3F);
			return colorsVal[colors];
		}

		void print() {
			char[] line = new char[N];
			for (int i = 0; i < N; ++i) {
				for (int j = 0; j < N; ++j) {
					line[j] = (char) (((f[i] >> (j * 3)) & 0x7) + '0');
				}
				debug(String.valueOf(line));
			}
		}
	}

	static class Hand implements Comparable<Hand> {
		int param;
		int next;
		int v;
		Hand p;

		public Hand(int r, int c, int d, int v, int boardI, Hand parent) {
			this.param = r | (c << 4) | ((d & 3) << 8) | (boardI << 12);
			this.next = -1;
			this.v = v;
			this.p = parent;
		}

		public Hand(int next, int v, int boardI, Hand parent) {
			this.param = next | (boardI << 12);
			this.next = -1;
			this.v = v;
			this.p = parent;
		}

		int r() {
			return (param >> 0) & 0xF;
		}

		int c() {
			return (param >> 4) & 0xF;
		}

		int d() {
			return (param >> 8) & 0xF;
		}

		int boardI() {
			return param >> 12;
		}

		public int compareTo(Hand o) {
			return o.v - this.v;
		}
	}

	static class Gen {
		int pos;
		long[] val = new long[80000];

		Gen(int C, int seed) {
			long cur = seed;
			for (int i = 0; i < val.length; ++i) {
				val[i] = (int) cur % C;
				cur = (cur * 48271) % 2147483647;
				val[i] |= ((int) cur % C) << 3;
				cur = (cur * 48271) % 2147483647;
			}
		}

		long next() {
			return val[pos++];
		}

		void setPos(int pos) {
			this.pos = pos;
		}
	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int colors = sc.nextInt();
			int n = sc.nextInt();
			String[] board = new String[n];
			for (int i = 0; i < n; ++i) {
				board[i] = sc.next();
			}
			int seed = sc.nextInt();
			int[] ret = new SquareRemover().playIt(colors, board, seed);
			for (int i = 0; i < ret.length; ++i) {
				System.out.println(ret[i]);
			}
			System.out.flush();
		}
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}

	}

	static final class Counter {
		ArrayList<Integer> count = new ArrayList<Integer>();

		void add(int i) {
			if (DEBUG) {
				while (count.size() <= i) {
					count.add(0);
				}
				count.set(i, count.get(i) + 1);
			}
		}

		void print() {
			if (DEBUG) {
				System.err.print("[");
				for (int i = 0; i < count.size(); ++i) {
					System.err.print(count.get(i) + ", ");
				}
				System.err.println("]");
			}
		}

	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;
		static final double TO_DOUBLE = 1.0 / (1L << 31);

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		boolean nextBoolean() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return (w & 8) == 0;
		}

		double nextDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return Math.abs(w * TO_DOUBLE);
		}

		double nextSDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w * TO_DOUBLE;
		}

	}

}
