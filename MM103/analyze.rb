#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:N, :C, :D, :score)

def get_scores(filename)
  scores = []
  seed = 0
  n = 0
  c = 0
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      seed = $1.to_i
    elsif line =~ /N: *(\d+) C: *(\d+)/
      n = $1.to_i
      c = $2.to_i
    elsif line =~ /score:/
      scores[seed] = line.split(/\s+/).drop(1).map.with_index do |s, d|
        TestCase.new(n, c, d + 10, s.to_i)
      end
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    91.times do |i|
      next unless filter.call(pair[0][i], pair[1][i])
      count += 1
      s1 = [pair[0][i].score, 0].max
      s2 = [pair[1][i].score, 0].max
      if s2 == 0
        if s1 > 0
          sum_score_diff -= 1.0;
          lose += 1
        end
      elsif s1 < s2
        sum_score_diff += 1.0 - (1.0 * s1 / s2)
        win += 1
      elsif s1 > s2
        sum_score_diff -= 1.0 - (1.0 * s2 / s1)
        lose += 1
      else
        #
      end
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff * 1000000.0 / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  (10).step(100, 19) do |param|
    upper = param + 18
    puts "N:#{sprintf('%3d', param)}-#{sprintf('%3d', upper)}"
    calc(from, to) { |from, to| from.N.between?(param, upper) }
  end
  puts "---------------------"
  (50).step(100, 13) do |param|
    upper = param + 12
    puts "C:#{sprintf('%3d', param)}-#{sprintf('%3d', upper)}"
    calc(from, to) { |from, to| from.C.between?(param, upper) }
  end
  puts "---------------------"
  (10).step(100, 19) do |param|
    upper = param + 18
    puts "D:#{sprintf('%3d', param)}-#{sprintf('%3d', upper)}"
    calc(from, to) { |from, to| from.D.between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
