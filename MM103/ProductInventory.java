import java.util.Arrays;
import java.util.SplittableRandom;

public class ProductInventory {
	private SplittableRandom rnd = new SplittableRandom(42);
	private int N, C;
	private int day = 0;
	private int[] BP, SP, EX;
	private int ALL_GAIN;
	private int[][] expireOn;
	private int[] initialSell;
	private int[] inventory;
	private int[][] sellCount, sellCountSum;
	private boolean[][] fullSell;
	private Distribution[][] dists = new Distribution[5][100];
	private double[][][] distsLikelyhood;
//	private static Distribution[][] allDists = new Distribution[105][100];

//	static {
//		SplittableRandom rnd = new SplittableRandom(42);
//		for (int i = 40; i < allDists.length; i++) {
//			for (int j = 0; j < allDists[0].length; j++) {
//				allDists[i][j] = new Distribution(i, rnd);
//			}
//		}
//	}

	public int init(int[] buy, int[] sell, int[] expiration) {
		N = buy.length;
		BP = buy;
		SP = sell;
		EX = expiration;
		for (int i = 0; i < N; i++) {
			ALL_GAIN += SP[i] - BP[i];
		}
		inventory = new int[N];
		sellCount = new int[N][101];
		sellCountSum = new int[N][102];
		expireOn = new int[N][120];
		fullSell = new boolean[N][101];
		distsLikelyhood = new double[dists.length][dists[0].length][N];
		return 0;
	}

	public int[] order(int[] yesterday) {
		if (day == 0) {
			initialSell = yesterday.clone();
			for (int i = 0; i < N; i++) {
				C += yesterday[i];
			}
			C = (int) (C / 0.05 / 10 / N);
			C = Math.max(50, Math.min(100, C));
			for (int i = 0; i < dists.length; i++) {
//				dists[i] = allDists[C - (dists.length - 2) + i];
				for (int j = 0; j < dists[0].length; j++) {
					dists[i][j] = new Distribution(C - (dists.length - 2) + i, rnd);
				}
			}
		} else {
			for (int i = 0; i < N; i++) {
				int sub = yesterday[i];
				fullSell[i][day] = sub == inventory[i];
				inventory[i] -= sub;
				for (int j = day - 1; sub > 0; j++) {
					if (expireOn[i][j] >= sub) {
						expireOn[i][j] -= sub;
						sub = 0;
					} else {
						sub -= expireOn[i][j];
						expireOn[i][j] = 0;
					}
				}
				sellCount[i][day] = yesterday[i];
				sellCountSum[i][day + 1] = sellCountSum[i][day] + yesterday[i];
				inventory[i] -= expireOn[i][day - 1];
			}
		}

		int[] order = new int[N];
		Distribution[] selectedDist = new Distribution[N];
		for (int i = 0; i < N; i++) {
			double[] prob = new double[21];
			for (int j = 0; j < dists.length; j++) {
				for (int k = 0; k < dists[0].length; k++) {
					Distribution d = dists[j][k];
					if (day == 0) {
						distsLikelyhood[j][k][i] = d.prob10[initialSell[i]];
					} else {
						distsLikelyhood[j][k][i] = Math.pow(distsLikelyhood[j][k][i], 0.97) * (fullSell[i][day] ? d.probAbove[sellCount[i][day]] : d.prob[sellCount[i][day]]);
					}
					for (int l = 0; l < prob.length; l++) {
						prob[l] += d.prob[l] * distsLikelyhood[j][k][i];
					}
				}
			}
			double probSum = 0;
			for (int j = 0; j < prob.length; j++) {
				probSum += prob[j];
			}
			for (int j = 0; j < prob.length; j++) {
				prob[j] /= probSum;
			}
			selectedDist[i] = new Distribution(prob);
//			System.err.printf("cust %2d:", i);
//			for (int j = 0; j < 20; j++) {
//				System.err.printf("%.6f ", prob[j]);
//			}
//			System.err.printf("%.6f", selectedDist[i].ave);
//			System.err.println();
		}
		for (int i = 0; i < N; i++) {
			double maxExpect = -1e99;
			for (int j = 0; inventory[i] + j < 20; j++) {
				double expect = calcExpect(i, inventory[i] + j, j, selectedDist[i]);
				if (expect > maxExpect) {
					maxExpect = expect;
					order[i] = j;
				}
			}
			if (day >= 98 && SP[i] <= BP[i]) {
				order[i] = 0;
			}
			expireOn[i][day + EX[i]] += order[i];
			inventory[i] += order[i];
		}
		day++;
		return order;
	}

	private double calcExpect(int itemId, int itemCount, int buyCount, Distribution dist) {
		double ret = 0;
		if (day < 10) {
			for (int sell = 0; sell < 20; sell++) {
				double v = -buyCount * BP[itemId];
				if (sell < itemCount) {
					v += sell * SP[itemId];
					if (EX[itemId] > 0) {
						v += Math.min(dist.ave, itemCount - sell) * BP[itemId];
					}
					if (EX[itemId] > 1 && itemCount - sell - dist.ave > 0) {
						v += Math.min(dist.ave, itemCount - sell - dist.ave) * BP[itemId] * (day == 9 ? 90.0 / 91 : 1.0);
					}
				} else {
					v += itemCount * SP[itemId];
					v -= (sell - itemCount) * 0.05 * 0.05 * ALL_GAIN * (((10 - day) + (100 - day)) / 2.0);
				}
				ret += dist.prob[sell] * v;
			}
		} else {
			for (int sell = 0; sell < 20; sell++) {
				double v = -buyCount * BP[itemId] * (101 - day);
				if (sell < itemCount) {
					v += sell * SP[itemId] * (101 - day);
					if (EX[itemId] > 0) {
						v += Math.min(dist.ave, itemCount - sell) * BP[itemId] * (100 - day);
					}
					if (EX[itemId] > 1 && itemCount - sell - dist.ave > 0 && day < 99) {
						v += Math.min(dist.ave, itemCount - sell - dist.ave) * BP[itemId] * (99 - day);
					}
				} else {
					v += itemCount * SP[itemId] * (101 - day);
					v -= (sell - itemCount) * 0.05 * 0.05 * ALL_GAIN * ((100 - day) * (101 - day) / 2.0);
				}
				ret += dist.prob[sell] * v;
			}
		}
		return ret;
	}

	static class Distribution {
		int c;
		double[] prob, probAbove, prob10;
		double ave;

		Distribution(int c, SplittableRandom rnd) {
			this.c = c;
			double[][] dp = new double[2][c + 1];
			dp[0][0] = 1.0;
			for (int i = 0, t = 0; i < c; i++, t ^= 1) {
				double purchaseP = rnd.nextDouble() * 0.1;
				ave += purchaseP;
				for (int j = 0; j <= i + 1; j++) {
					dp[t ^ 1][j] = dp[t][j] * (1 - purchaseP);
					if (j > 0) dp[t ^ 1][j] += dp[t][j - 1] * purchaseP;
				}
			}
			this.prob = dp[c % 2];
			this.probAbove = new double[c + 1];
			this.probAbove[c] = this.prob[c];
			for (int i = c - 1; i >= 0; i--) {
				this.probAbove[i] = this.probAbove[i + 1] + this.prob[i];
			}
			double[][] dp10 = new double[2][c * 10 + 1];
			dp10[0][0] = 1.0;
			for (int i = 0, t = 0; i < 10; i++, t ^= 1) {
				Arrays.fill(dp10[t ^ 1], 0);
				for (int j = 0; j <= i * c; j++) {
					for (int k = 0; k <= c && j + k < dp10[0].length; k++) {
						dp10[t ^ 1][j + k] += dp10[t][j] * prob[k];
					}
				}
			}
			this.prob10 = dp10[0];
		}

		Distribution(double[] ps) {
			this.prob = ps;
			for (int i = 0; i < ps.length; i++) {
				this.ave += i * ps[i];
			}
			this.probAbove = new double[prob.length];
			this.probAbove[prob.length - 1] = this.prob[prob.length - 1];
			for (int i = prob.length - 2; i >= 0; i--) {
				this.probAbove[i] = this.probAbove[i + 1] + this.prob[i];
			}
			double[][] dp10 = new double[2][(prob.length - 1) * 10 + 1];
			dp10[0][0] = 1.0;
			for (int i = 0, t = 0; i < 10; i++, t ^= 1) {
				Arrays.fill(dp10[t ^ 1], 0);
				for (int j = 0; j <= i * (prob.length - 1); j++) {
					for (int k = 0; k <= (prob.length - 1) && j + k < dp10[0].length; k++) {
						dp10[t ^ 1][j + k] += dp10[t][j] * prob[k];
					}
				}
			}
			this.prob10 = dp10[0];
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("c:" + c + "\n");
			for (int i = 0; i < this.prob.length; i++) {
				sb.append(String.format("%.6f ", this.prob[i]));
			}
			sb.append("\n");
			for (int i = 0; i < this.prob.length; i++) {
				sb.append(String.format("%.6f ", this.probAbove[i]));
			}
			sb.append("\n");
			for (int i = 0; i < this.prob10.length; i++) {
				sb.append(String.format("%.6f ", this.prob10[i]));
			}
			return sb.toString();
		}
	}
}