import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;
import java.util.SplittableRandom;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Tester {

	private static String save;
	private static boolean debug;
	private static int radix = 10;
	private int day = 0;
	private int dayWidth;
	private BufferedImage image;
	private Color[] colors;
	private SplittableRandom r;
	private int[] buy;
	private int[] sell;
	private int[] expiration;
	private boolean[] dead;
	private double[][] purchase;
	private double[] annoyance;
	private int[] purchases;
	private int[] sellToCustomer;
	private int[][] inventory;
	private boolean[] unsatisfiedCustomer;
	private int[] unsatisfiedProduct;

	private Result generateTestCase(long x) {
		r = new SplittableRandom(x);
		Random r;
		try {
			r = SecureRandom.getInstance("SHA1PRNG");
			r.setSeed(x);
		} catch (Exception e) {
			r = new Random(x);
		}
		int itemCount = r.nextInt(91) + 10;
		int custCount = r.nextInt(51) + 50;
		if (x < 5) {
			itemCount = custCount = (int) x * 5;
		}
		if (1000 <= x && x < 2000) {
			itemCount = 10 + 91 * (int) (x - 1000) / 1000;
		}
		buy = new int[itemCount];
		sell = new int[itemCount];
		expiration = new int[itemCount];
		purchases = new int[itemCount];
		sellToCustomer = new int[itemCount];
		inventory = new int[16][itemCount];
		annoyance = new double[custCount];
		purchase = new double[custCount][itemCount];
		unsatisfiedCustomer = new boolean[custCount];
		dead = new boolean[custCount];
		colors = new Color[itemCount];
		unsatisfiedProduct = new int[itemCount];

		Random cr = new Random(0);
		for (int i = 0; i < itemCount; i++) {
			buy[i] = r.nextInt(10) + 1;
			sell[i] = buy[i] / 2 + r.nextInt(1 + 3 * buy[i] - buy[i] / 2);
			expiration[i] = r.nextInt(16);
			if (save != null) colors[i] = new Color(cr.nextInt(256), cr.nextInt(256), cr.nextInt(256));
		}
		for (int i = 0; i < custCount; i++) {
			annoyance[i] = r.nextDouble() * 0.1;
			for (int j = 0; j < itemCount; j++) {
				purchase[i][j] = r.nextDouble() * 0.1;
				for (int k = 0; k < 10; k++) {
					if (r.nextDouble() < purchase[i][j]) {
						purchases[j]++;
					}
				}
			}
		}
//		for (int i = 0; i < itemCount; i++) {
//			double[][] dp = new double[custCount + 1][custCount + 1];
//			dp[0][0] = 1;
//			for (int j = 0; j < custCount; j++) {
//				for (int k = 0; k < custCount; k++) {
//					dp[j + 1][k] += dp[j][k] * (1 - purchase[j][i]);
//					dp[j + 1][k + 1] += dp[j][k] * purchase[j][i];
//				}
//			}
//			System.err.printf("cust %2d:", i);
//			double ave = 0;
//			for (int j = 0; j < 20; j++) {
//				System.err.printf("%.6f ", dp[custCount][j]);
//				ave += j * dp[custCount][j];
//			}
//			System.err.printf("%.6f ", ave);
//			System.err.println();
//		}
		if (debug) {
			System.err.print("BP");
			for (int i = 0; i < itemCount; i++) {
				System.err.printf("%2s", Integer.toString(buy[i], radix));
			}
			System.err.print("\nSP");
			for (int i = 0; i < itemCount; i++) {
				System.err.printf("%2s", Integer.toString(sell[i], radix));
			}
			System.err.print("\nEX");
			for (int i = 0; i < itemCount; i++) {
				System.err.printf("%2s", Integer.toString(expiration[i], radix));
			}
		}
		Result res = new Result();
		res.seed = x;
		res.N = itemCount;
		res.C = custCount;
		return res;
	}

	private boolean tryBuy(int itemNumber) {
		for (int i = 0; i < 16; i++) {
			if (inventory[i][itemNumber] > 0) {
				inventory[i][itemNumber]--;
				return true;
			}
		}
		return false;
	}

	private int processDay(int[] order) {
		int ret = 0;
		Arrays.fill(purchases, 0);
		for (int i = 0; i < order.length; i++) {
			ret -= buy[i] * order[i];
			inventory[expiration[i]][i] = order[i];
		}

		Arrays.fill(unsatisfiedCustomer, false);
		Arrays.fill(sellToCustomer, 0);
		Arrays.fill(unsatisfiedProduct, 0);
		for (int i = 0; i < dead.length; i++) {
			if (debug) {
				System.err.printf("\n%2d", i);
			}
			if (dead[i]) {
				if (debug) {
					for (int j = 0; j < purchase[i].length; j++) {
						System.err.print(" d");
					}
				}
				continue;
			}
			for (int j = 0; j < purchase[i].length; j++) {
				char st = '-';
				if (r.nextDouble() < purchase[i][j]) {
					if (tryBuy(j)) {
						st = 'o';
						sellToCustomer[j]++;
						ret += sell[j];
						purchases[j]++;
					} else {
						st = 'x';
						unsatisfiedProduct[j]++;
						if (r.nextDouble() < annoyance[i]) {
							dead[i] = true;
							unsatisfiedCustomer[i] = true;
						}
					}
				}
				if (debug) {
					System.err.print(" " + st);
				}
			}
		}
		for (int i = 0; i < 15; i++) {
			inventory[i] = inventory[i + 1];
		}
		inventory[15] = new int[buy.length];
		day++;
		return ret;
	}

	private Result runTest(long seed) throws IOException {
		Result res = generateTestCase(seed);
		if (save != null) {
			dayWidth = dead.length;
			image = new BufferedImage(20 + 100 * dayWidth, 20 + (1 + purchases.length) * 10, BufferedImage.TYPE_INT_ARGB);
		}
		long startTime = System.currentTimeMillis();
		ProductInventory solver = new ProductInventory();
		solver.init(buy, sell, expiration);
		res.elapsed += System.currentTimeMillis() - startTime;
		int profit = 0;
		for (int k = 0; k < 100; k++) {
//			double[][] dp = new double[res.C + 1][21];
//			for (int i = 0; i < res.N; i++) {
//				dp[0][0] = 1.0;
//				for (int j = 0; j < res.C; j++) {
//					dp[j + 1][dp[0].length - 1] = dp[j][dp[0].length - 1];
//					for (int l = dp[0].length - 2; l >= 0; l--) {
//						dp[j + 1][l + 1] += dp[j][l] * purchase[j][i];
//						dp[j + 1][l] = dp[j][l] * (1 - purchase[j][i]);
//					}
//				}
//				solver.realProbs[i] = dp[res.C].clone();
//			}
			startTime = System.currentTimeMillis();
			int[] ret = solver.order(purchases);
			res.elapsed += System.currentTimeMillis() - startTime;
			if (ret.length != buy.length) {
				System.err.println("return value from order() is the wrong length: " + ret.length + " (expected " + buy.length + ")");
				return res;
			}
			for (int i = 0; i < ret.length; i++) {
				if (ret[i] < 0 || ret[i] > 100) {
					System.err.println("Element " + i + " of return value from order() must be between 0-100: received " + ret[i]);
					return res;
				}
			}
			if (debug) {
				System.err.printf("\n--------day%2d--------", k);
				System.err.print("\nSL");
				for (int i = 0; i < res.N; i++) {
					System.err.printf("%2s", Integer.toString(purchases[i], radix));
				}
				System.err.print("\nOD");
				for (int i = 0; i < res.N; i++) {
					System.err.printf("%2s", Integer.toString(ret[i], radix));
				}
				System.err.print("\nQU");
				for (int i = 0; i < res.N; i++) {
					int sum = ret[i];
					for (int j = 0; j < 15; j++) {
						sum += inventory[j][i];
					}
					System.err.printf("%2s", Integer.toString(sum, radix));
				}
			}
			int thisProfit = processDay(ret);
			profit += thisProfit;
			res.rawScore[k + 1] = profit;
			res.score[k + 1] = Math.max(0, profit);
			if (save != null) {
				visualize();
			}
		}
		if (debug) {
			System.err.println();
		}
		if (save != null) {
			ImageIO.write(image, "png", new File(save));
		}
		return res;
	}

	private void visualize() {
		Graphics2D g = image.createGraphics();

		for (int i = 0; i < sellToCustomer.length; i++) {
			int w = 5 * dayWidth * sellToCustomer[i] / unsatisfiedCustomer.length;
			g.setColor(colors[i]);
			g.fillRect(10 + (day - 1) * dayWidth, 10 + i * 10, w, 10);

			if (unsatisfiedProduct[i] > 0) {
				g.drawRect(10 + (day - 1) * dayWidth + w, 10 + i * 10, 5 * dayWidth * unsatisfiedProduct[i] / unsatisfiedCustomer.length, 10);
			}
		}

		String unhappy = "\u2639";
		StringBuilder unhappies = new StringBuilder();
		for (int i = 0; i < unsatisfiedCustomer.length; i++) {
			if (unsatisfiedCustomer[i]) {
				unhappies.append(unhappy);
			}
		}

		g.setColor(Color.RED);
		g.drawString(unhappies.toString(), 10 + (day - 1) * dayWidth, 10 + (1 + sellToCustomer.length) * 10);

		g.setColor(Color.BLACK);
		g.drawString((day - 1) + "", 10 + (day - 1) * dayWidth, 10);
		g.drawLine(10 + day * dayWidth, 0, 10 + day * dayWidth, image.getHeight());
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws IOException {
		long seed = 1;
		long begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-save")) save = args[++i];
			if (args[i].equals("-debug")) debug = true;
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
		}
		if (begin != -1 && end != -1) {
			save = null;
			debug = false;
			BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
			BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
			for (long i = begin; i <= end; ++i) {
				q.add(i);
			}
			Result[] results = new Result[(int) (end - begin + 1)];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i] = new TestThread(q, receiver);
				threads[i].start();
			}
			int printed = 0;
			try {
				for (int i = 0; i < (int) (end - begin + 1); i++) {
					Result res = receiver.poll(60, TimeUnit.SECONDS);
					results[(int) (res.seed - begin)] = res;
					for (; printed < results.length && results[printed] != null; printed++) {
						System.out.println(results[printed]);
						System.out.println();
					}
				}
			} catch (InterruptedException e) {
				for (int i = printed; i < results.length; i++) {
					System.out.println(results[i]);
					System.out.println();
				}
				e.printStackTrace();
			}

			double sum = 0;
			for (Result result : results) {
				for (int i = 10; i <= 100; i++) {
					sum += result.score[i];
				}
			}
			System.out.println("ave:" + (sum / ((end - begin + 1) * (100 - 10 + 1))));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	private static class TestThread extends Thread {
		BlockingQueue<Result> results;
		BlockingQueue<Long> q;

		TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
			this.q = q;
			this.results = results;
		}

		public void run() {
			while (true) {
				Long seed = q.poll();
				if (seed == null) break;
				try {
					Tester f = new Tester();
					Result res = f.runTest(seed);
					results.add(res);
				} catch (Exception e) {
					e.printStackTrace();
					Result res = new Result();
					res.seed = seed;
					results.add(res);
				}
			}
		}
	}
}

class Result {
	long seed;
	int N, C;
	int[] score = new int[101], rawScore = new int[101];
	long elapsed;

	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append(String.format("seed:%4d\n", seed));
		ret.append(String.format("N:%3d C:%3d\n", N, C));
		ret.append(String.format("elapsed:%.4f\n", elapsed / 1000.0));
		ret.append("score:");
		for (int i = 10; i <= 100; i++) {
			ret.append(String.format(" %6d", rawScore[i]));
		}
		return ret.toString();
	}
}
