import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Tester {

	private static String save;
	private static String orig;
	private static final long TL = 10000;
	private long timeLimit = TL;

	private static int myR, myF, myP;
	private int precision;
	private int nRenders;
	private final int nLayers = 4;
	private int nFonts;
	private Font[] fonts;

	private BufferedImage original;
	private int[][][] originalCmyk = new int[4][][];
	private int[][][] layers = new int[4][][];
	private Graphics2D graphics;
	private TypesetterArt solver;

	private int[] callRender(int iteration) {
		long startMillis = System.currentTimeMillis();
		int[] ret = solver.render(iteration, (int) timeLimit);
		timeLimit -= System.currentTimeMillis() - startMillis;
		return ret;
	}

	private void callInit() {
		int w = original.getWidth();
		int h = original.getHeight();
		int[] image = new int[nLayers * w * h];
		for (int k = 0; k < nLayers; k++) {
			for (int i = 0; i < h; i++) {
				for (int j = 0; j < w; j++) {
					image[k * h * w + i * w + j] = originalCmyk[k][i][j];
				}
			}
		}
		long startMillis = System.currentTimeMillis();
		solver.init(nRenders, nFonts, precision, w, h, image);
		timeLimit -= System.currentTimeMillis() - startMillis;
	}

	private void callNextGlyphs() {
		int w = 0;
		int h = 0;
		int[] bitmask = new int[0];

		while (true) {
			long startMillis = System.currentTimeMillis();
			int[] parts = solver.nextGlyph(w, h, bitmask, (int) timeLimit);
			timeLimit -= System.currentTimeMillis() - startMillis;
			if (parts.length == 0) {
				break;
			}
			if (parts.length != 3) {
				throw new RuntimeException("[ERROR]\tNext Glyph should return 0 or 3 integers\n");
			}

			int letterId = parts[0];
			int fontId = parts[1];
			int sizeId = parts[2];

			if (letterId < 32 || letterId > 126) {
				throw new RuntimeException(String.format("[ERROR]\tChar ID: %d\n", letterId));
			}
			if (fontId < 0 || fontId >= nFonts) {
				throw new RuntimeException(String.format("[ERROR]\tFont ID: %d\n", fontId));
			}
			if (sizeId < 8 || sizeId > 72) {
				throw new RuntimeException(String.format("[ERROR]\tSize ID: %d\n", sizeId));
			}

			Font f = fonts[fontId].deriveFont((float) sizeId);
			FontMetrics metrics = graphics.getFontMetrics(f);
			h = metrics.getHeight();
			w = (int) Math.ceil(metrics.getStringBounds("" + (char) letterId, graphics).getWidth());
			if (w == 0) {
				bitmask = new int[0];
				continue;
			}
			BufferedImage temp = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_BINARY);
			Graphics2D g = temp.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
			g.setFont(f);
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, w, h);
			g.setColor(Color.BLACK);
			g.drawString("" + (char) letterId, 0, h);
			bitmask = new int[(w + 31) / 32 * h];
			int pos = 0;
			for (int i = 0; i < h; i++) {
				for (int j = 0; j < w; j += 32, pos++) {
					for (int k = j; k < j + 32 && k < w; k++) {
						if ((temp.getRGB(k, i) & 0XFF) == 0) {
							bitmask[pos] |= 1 << (k - j);
						}
					}
				}
			}
		}
	}

	private void createLayer(int iteration, int[] data) {
		if (data.length == 0) return;
		int fontId = data[0];
		int sizeId = data[1];
		int colorId = data[2];
		int saturation = data[3];

		if (fontId < 0 || fontId >= nFonts) {
			throw new RuntimeException(String.format("[ERROR]\tLayer: %d, Font ID: %d\n", iteration, fontId));
		} else if (sizeId < 8 || sizeId > 72) {
			throw new RuntimeException(String.format("[ERROR]\tLayer: %d, Size ID: %d\n", iteration, sizeId));
		} else if (colorId < 0 || colorId >= nLayers) {
			throw new RuntimeException(String.format("[ERROR]\tLayer: %d, Color ID: %d\n", iteration, colorId));
		} else if (saturation < 0 || saturation > 255) {
			throw new RuntimeException(String.format("[ERROR]\tLayer: %d, Saturation: %d\n", iteration, saturation));
		}
		Font f = fonts[fontId].deriveFont((float) sizeId);
		for (int i = 4; i < data.length; i++) {
			if (data[i] != (int) '\n' && (data[i] < 32 || data[i] > 126)) {
				throw new RuntimeException(String.format("[ERROR]\tLayer: %d, Index: %d, Char ID: %d\n", iteration, i - 3, data[i]));
			}
		}

		BufferedImage temp = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
		Graphics2D tempG = temp.createGraphics();
		FontMetrics metrics = graphics.getFontMetrics(f);

		tempG.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		tempG.setFont(f);
		tempG.setColor(Color.WHITE);
		tempG.fillRect(0, 0, temp.getWidth(), temp.getHeight());

		StringBuilder builder = new StringBuilder();

		int lineNo = 1;
		int left = 0;
		Map<Integer, BufferedImage> letterCache = new HashMap<>();
		for (int i = 4; i < data.length; i++) {
			if (data[i] != (int)'\n') {
				int h = metrics.getHeight();
				int w = (int)Math.ceil(metrics.getStringBounds("" + (char)data[i], graphics).getWidth());

				if (w > 0) {
					if (!letterCache.containsKey(data[i])) {
						BufferedImage letter = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_BINARY);
						Graphics2D letterG = letter.createGraphics();

						letterG.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
						letterG.setFont(f);
						letterG.setColor(Color.WHITE);
						letterG.fillRect(0, 0, w, h);

						letterG.setColor(Color.BLACK);
						letterG.drawString("" + (char)data[i], 0, h);

						letterCache.put(data[i], letter);
					}
					tempG.drawImage(letterCache.get(data[i]), left, (lineNo - 1) * h, null);
					left += w;
				}
			}
			else {
				lineNo++;
				left = 0;
			}
		}

		for (int y = 0; y < temp.getHeight(); y++) {
			for (int x = 0; x < temp.getWidth(); x++) {
				if ((temp.getRGB(x, y) & 0XFF) == 0) {
					int cmykComponent = layers[colorId][y][x] + saturation;
					layers[colorId][y][x] = Math.min(255, cmykComponent);
				}
			}
		}

//		int lineNo = 1;
//		for (int i = 4; i < data.length; i++) {
//			if (data[i] != (int) '\n') {
//				builder.append((char) data[i]);
//			}
//			if (data[i] == (int) '\n' || i == data.length - 1) {
//				tempG.setColor(Color.BLACK);
//				tempG.drawString(builder.toString(), 0, lineNo * metrics.getHeight());
//
//				builder.setLength(0);
//				lineNo++;
//			}
//		}
	}

	private double score() throws IOException {
		if (save != null) {
			BufferedImage mix = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_INT_RGB);
			for (int i = 0; i < original.getHeight(); i++) {
				for (int j = 0; j < original.getWidth(); j++) {
					int rgbR = (int) Math.round(255 * (1 - layers[0][i][j] / 255.0) * (1 - layers[3][i][j] / 255.0));
					int rgbG = (int) Math.round(255 * (1 - layers[1][i][j] / 255.0) * (1 - layers[3][i][j] / 255.0));
					int rgbB = (int) Math.round(255 * (1 - layers[2][i][j] / 255.0) * (1 - layers[3][i][j] / 255.0));
					mix.setRGB(j, i, (rgbR << 16) | (rgbG << 8) | rgbB);
				}
			}
			ImageIO.write(mix, "png", new File(save));
		}

		double res = 0;
		int div = 0;
		for (int i = 0; i < original.getHeight(); i += precision) {
			for (int j = 0; j < original.getWidth(); j += precision) {
				for (int k = 0; k < nLayers; k++) {
					double originalSum = 0;
					double actualSum = 0;
					int n = 0;
					for (int top = 0; top < precision && top + i < original.getHeight(); top++) {
						for (int left = 0; left < precision && left + j < original.getWidth(); left++) {
							originalSum += originalCmyk[k][i + top][j + left];
							actualSum += layers[k][i + top][j + left];
							n++;
						}
					}
					if (n != 0) {
						originalSum /= n * 255;
						actualSum /= n * 255;
//						System.err.println(k + " "+ (i / precision) + " " + (j / precision) + " " + originalSum + " " + actualSum);
						res += Math.pow(1 - Math.abs(actualSum - originalSum), 2);
						div++;
					}
				}
			}
		}
		res /= div;
		return res * 1000000;
	}

	public Result runTest(long seed) throws Exception {
		fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		random.setSeed(seed);

		if (myR == 0) {
			nRenders = 1 + random.nextInt(16);
		} else {
			nRenders = myR;
		}
		if (myF == 0) {
			nFonts = 10 + random.nextInt(16);
		} else {
			nFonts = myF;
		}
		if (myP == 0) {
			precision = 1 + random.nextInt(16);
		} else {
			precision = myP;
		}
		String fileName = (orig == null ? "testdata/" + seed + ".jpg" : orig);
		Collections.shuffle(Arrays.asList(fonts), random);

		original = ImageIO.read(new File(fileName));
		graphics = original.createGraphics();
		for (int k = 0; k < nLayers; k++) {
			originalCmyk[k] = new int[original.getHeight()][original.getWidth()];
		}
		for (int i = 0; i < original.getHeight(); i++) {
			for (int j = 0; j < original.getWidth(); j++) {
				int rgb = original.getRGB(j, i);
				double r = ((rgb >> 16) & 0XFF) / 255.0;
				double g = ((rgb >> 8) & 0XFF) / 255.0;
				double b = (rgb & 0XFF) / 255.0;

				double k = 1.0 - Math.max(Math.max(r, g), b);
				double c = k > 1 - 1E-2 ? (r + k > 1 - 1E-2 ? 1 : 0) : (1 - r - k) / (1 - k);
				double m = k > 1 - 1E-2 ? (g + k > 1 - 1E-2 ? 1 : 0) : (1 - g - k) / (1 - k);
				double y = k > 1 - 1E-2 ? (b + k > 1 - 1E-2 ? 1 : 0) : (1 - b - k) / (1 - k);

				originalCmyk[0][i][j] = (int) Math.round(c * 255);
				originalCmyk[1][i][j] = (int) Math.round(m * 255);
				originalCmyk[2][i][j] = (int) Math.round(y * 255);
				originalCmyk[3][i][j] = (int) Math.round(k * 255);
			}
		}

		long startMillis = System.currentTimeMillis();
		solver = new TypesetterArt();
		timeLimit -= System.currentTimeMillis() - startMillis;
		Result res = new Result();
		res.seed = seed;
		res.R = nRenders;
		res.F = nFonts;
		res.P = precision;
		callInit();
		callNextGlyphs();
		for (int k = 0; k < nLayers; k++) {
			layers[k] = new int[original.getHeight()][original.getWidth()];
		}
		for (int i = 0; i < nRenders; i++) {
			createLayer(i, callRender(i));
		}
		res.elapsed = TL - timeLimit;
		res.score = score();
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 0, begin = -1, end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-save")) save = args[++i];
			if (args[i].equals("-orig")) orig = args[++i];
			if (args[i].equals("-R")) myR = Integer.parseInt(args[++i]);
			if (args[i].equals("-F")) myF = Integer.parseInt(args[++i]);
			if (args[i].equals("-P")) myP = Integer.parseInt(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
		}
		if (begin != -1 && end != -1) {
			save = orig = null;
			BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
			BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
			for (long i = begin; i <= end; ++i) {
				q.add(i);
			}
			Result[] results = new Result[(int) (end - begin + 1)];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i] = new TestThread(q, receiver);
				threads[i].start();
			}
			int printed = 0;
			try {
				for (int i = 0; i < (int) (end - begin + 1); i++) {
					Result res = receiver.poll(160, TimeUnit.SECONDS);
					results[(int) (res.seed - begin)] = res;
					for (; printed < results.length && results[printed] != null; printed++) {
						System.out.println(results[printed]);
						System.out.println();
					}
				}
			} catch (InterruptedException e) {
				for (int i = printed; i < results.length; i++) {
					System.out.println(results[i]);
					System.out.println();
				}
				e.printStackTrace();
			}

			double sum = 0;
			for (Result result : results) {
				sum += result.score;
			}
			System.out.println("ave:" + (sum / (end - begin + 1)));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	private static class TestThread extends Thread {
		BlockingQueue<Result> results;
		BlockingQueue<Long> q;

		TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
			this.q = q;
			this.results = results;
		}

		public void run() {
			while (true) {
				Long seed = q.poll();
				if (seed == null) break;
				try {
					Tester f = new Tester();
					Result res = f.runTest(seed);
					results.add(res);
				} catch (Exception e) {
					e.printStackTrace();
					Result res = new Result();
					res.seed = seed;
					results.add(res);
				}
			}
		}
	}
}

class Result {
	long seed;
	int R, F, P;
	double score;
	long elapsed;

	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append(String.format("seed:%4d\n", seed));
		ret.append(String.format("R:%2d F:%2d P:%2d\n", R, F, P));
		ret.append(String.format("elapsed:%.4f\n", elapsed / 1000.0));
		ret.append(String.format("score:%.3f", score));
		return ret.toString();
	}
}
