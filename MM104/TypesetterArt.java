import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.SplittableRandom;

public class TypesetterArt {
	private static final boolean DEBUG = false;
	private static final long TIME_OPTIMIZE = 1000;
	private static final int MIN_FONT_SIZE = 8;
	private static final int MAX_FONT_SIZE = 9;
	private static final int MIN_CHAR = 32;
	private static final int MAX_CHAR = 126;
	private static final int[] USE_CHARS_1 = new int[]{
			32, // space
			95, 96, 46, 44, 45, 126, 58, 94, 34, 59, 39, 60, // low pixel density
			66, 108, 73, 69, 64, 82, 56, 36, 102, 77, 68, 100, // high pixel density
			33, 40, 41, 42, 43, 47, 49, 61, 74, 76, 84, 91, 92, 93, 105, 123, 124, 125 // simple shape
	};
	private static final int[] USE_CHARS_2 = {32, 33, 34, 39, 40, 41, 42, 43, 44, 45, 46, 47, 49, 58, 59, 61, 73, 74, 76, 84, 91, 92, 93, 94, 95, 96, 105, 108, 123, 124, 125, 126};
	private int[] USE_CHARS;
	private SplittableRandom rnd = new SplittableRandom(42);
	private int R, F, P, W, H;
	private int[][][] originalSum, current, currentSum;
	private double[][] pixelCountDiv, scoreBuf;
	private int[][] sumBuf, generalBuf;
	private FontImageGetter fontImageGetter;
	private FontInfo[][][] fonts;
	private Typeset[] typesets;
	private long singleSolveTime;

	public int init(int nRenders, int nFonts, int precision, int width, int height, int[] image) {
		this.R = nRenders;
		this.F = nFonts;
		this.P = precision;
		this.W = width;
		this.H = height;
		USE_CHARS = R < 10 ? USE_CHARS_1 : USE_CHARS_2;
		debug("R:" + R + " F:" + F + " P:" + P + " W:" + W + " H:" + H);
		int[][][] original = new int[4][H][W];
		this.originalSum = new int[4][(H + P - 1) / P][(W + P - 1) / P];
		this.current = new int[4][H][W];
		this.currentSum = new int[4][(H + P - 1) / P][(W + P - 1) / P];
		this.pixelCountDiv = new double[(H + P - 1) / P][(W + P - 1) / P];
		this.sumBuf = new int[(H + P - 1) / P][(W + P - 1) / P];
		this.generalBuf = new int[(H + P - 1) / P][(W + P - 1) / P];
		this.scoreBuf = new double[(H + P - 1) / P][(W + P - 1) / P];
		int index = 0;
		for (int k = 0; k < 4; k++) {
			for (int i = 0; i < H; i++) {
				System.arraycopy(image, index, original[k][i], 0, W);
				index += W;
				int ri = i / P;
				for (int j = 0; j < W; j++) {
					originalSum[k][ri][j / P] += original[k][i][j];
					if (k == 0) pixelCountDiv[ri][j / P]++;
				}
			}
		}
		for (int i = 0; i < pixelCountDiv.length; i++) {
			for (int j = 0; j < pixelCountDiv[0].length; j++) {
				pixelCountDiv[i][j] = 1.0 / (255.0 * pixelCountDiv[i][j]);
			}
		}
		fonts = new FontInfo[F][MAX_FONT_SIZE - MIN_FONT_SIZE + 1][MAX_CHAR - MIN_CHAR + 1];
		fontImageGetter = new FontImageGetter();
		return 0;
	}

	public int[] nextGlyph(int width, int height, int[] bitmask, int remainingMillis) {
		return fontImageGetter.next(width, height, bitmask);
//		debug(fontId + " ch:" + letterId + " " + (char)letterId + " h:" + height + " w:" + width);
	}

	public int[] render(int iteration, int remainingMillis) {
		debug("iteration:" + iteration + " remain:" + remainingMillis);
		if (iteration == 0) {
			final long timelimit = System.currentTimeMillis() + remainingMillis;
			typesets = solve(timelimit);
		}
		Typeset ts = typesets[iteration];
		if (ts == null) return new int[0];
		int strSize = 0;
		for (int i = 0; i < ts.lines.size(); i++) {
			strSize += ts.lines.get(i).size() + 1;
		}
		strSize--;
		int[] ret = new int[strSize + 4];
		ret[0] = ts.fontId;
		ret[1] = ts.fontSize;
		ret[2] = ts.layer;
		ret[3] = ts.saturation;
		for (int i = 0, pos = 4; i < ts.lines.size(); i++) {
			for (int j = 0; j < ts.lines.get(i).size(); j++) {
				ret[pos++] = ts.lines.get(i).get(j);
			}
			if (i < ts.lines.size() - 1) {
				ret[pos++] = '\n';
			}
		}
		return ret;
	}

	private Typeset[] solve(long timelimit) {
		double[] layerScore = new double[4];
		for (int i = 0; i < 4; i++) {
			layerScore[i] = calcScore(i);
		}
		Typeset[] ret = createInitialSolution(timelimit, layerScore);
		if (DEBUG) {
			double finalScore = 0;
			for (int i = 0; i < 4; i++) {
				debug("layer " + i + ":" + layerScore[i]);
				finalScore += layerScore[i];
			}
			debug("score:" + finalScore / 4);
			debug("singleSolveTime:" + singleSolveTime);
		}
		improve(ret, layerScore, timelimit);
		if (DEBUG) {
			double finalScore = 0;
			for (int i = 0; i < 4; i++) {
				debug("layer " + i + ":" + layerScore[i]);
				finalScore += layerScore[i];
			}
			debug("score:" + finalScore / 4);
		}
		return ret;
	}

	private Typeset[] createInitialSolution(long timelimit, double[] layerScore) {
		Typeset[] ret = new Typeset[R];
		long initTime = System.currentTimeMillis();
		int[] layerSelectCount = new int[4];
		for (int turn = 0; turn < R; turn++) {
			int tsi = turn % R;
			double maxLS = 0;
			int layer = -1;
			for (int i = 0; i < 4; i++) {
				double ls = (1 - layerScore[i]) / (layerSelectCount[i] + 1);
				if (ls > maxLS) {
					maxLS = ls;
					layer = i;
				}
				debug("layer " + i + ":" + layerScore[i]);
			}
			debug("selected layer:" + layer);
			layerSelectCount[layer]++;
			ret[tsi] = solveSingle(layer, initTime + (timelimit - TIME_OPTIMIZE - initTime) * (turn + 1) / R);
			apply(ret[tsi]);
			layerScore[layer] = calcScore(layer);
		}
		return ret;
	}

	private void apply(Typeset ts) {
		FontInfo[] curFonts = fonts[ts.fontId][ts.fontSize - MIN_FONT_SIZE];
		final int lineHeight = ts.lineHeight();
		for (int i = 0; i < ts.lines.size(); i++) {
			int cr = lineHeight * i;
			int cc = 0;
			for (char ch : ts.lines.get(i)) {
				FontInfo fi = curFonts[ch - MIN_CHAR];
				for (int k = 0; k < fi.r.length; k++) {
					int r = cr + fi.r[k];
					int c = cc + fi.c[k];
					if (r >= H || c >= W) continue;
					int prev = current[ts.layer][r][c];
					current[ts.layer][r][c] += ts.saturation;
					currentSum[ts.layer][r / P][c / P] += Math.min(current[ts.layer][r][c], 255) - Math.min(prev, 255);
				}
				cc += fi.w;
			}
		}
	}

	private Typeset solveSingle(int layer, long timelimit) {
		double maxDiff = -1e10;
		Typeset bestResult = null;
		for (int i = 0; ; i++) {
			long beforeTime = System.currentTimeMillis();
			if (i > 0 && beforeTime + singleSolveTime + 200 > timelimit) break;
			Typeset ts = new Typeset();
			ts.layer = layer;
			ts.fontId = rnd.nextInt(F);
			while (fonts[ts.fontId][0][0].w == 0) {
				ts.fontId = rnd.nextInt(F);
			}
			ts.fontSize = MIN_FONT_SIZE + rnd.nextInt(MAX_FONT_SIZE - MIN_FONT_SIZE + 1);
			if (P <= 2) {
				ts.saturation = rnd.nextInt(200) + 50;
			} else {
				ts.saturation = rnd.nextInt(106) + 150;
			}
			FontInfo[] curFonts = fonts[ts.fontId][ts.fontSize - MIN_FONT_SIZE];
			for (int j = 0; j < sumBuf.length; j++) {
				System.arraycopy(currentSum[layer][j], 0, sumBuf[j], 0, currentSum[layer][j].length);
				System.arraycopy(currentSum[layer][j], 0, generalBuf[j], 0, currentSum[layer][j].length);
			}
			for (int j = 0; j < scoreBuf.length; j++) {
				for (int k = 0; k < scoreBuf[0].length; k++) {
					double s = 1.0 - Math.abs(originalSum[layer][j][k] - sumBuf[j][k]) * pixelCountDiv[j][k];
					scoreBuf[j][k] = s * s;
				}
			}
			double sum = 0;
			for (int cr = 0; cr < H; cr += ts.lineHeight()) {
				ArrayList<Character> line = new ArrayList<>();
				int cc = 0;
				while (cc < W) {
					char ch = 32;
					double bestDiff = 0;
					for (int uch : USE_CHARS) {
						FontInfo fi = curFonts[uch - MIN_CHAR];
						if (fi.isEmpty()) continue;
						for (int j = 0; j < fi.r.length; j++) {
							int r = cr + fi.r[j];
							int c = cc + fi.c[j];
							if (r >= H || c >= W) continue;
							int prev = current[layer][r][c];
							if (prev >= 255) continue;
							int next = current[layer][r][c] + ts.saturation;
							if (next > 255) next = 255;
							sumBuf[r / P][c / P] += next - prev;
						}
						double diff = 0;
						final int top = (cr + fi.top) / P;
						final int bottom = (Math.min(H - 1, cr + fi.bottom)) / P;
						final int left = (cc + fi.left) / P;
						final int right = (Math.min(W - 1, cc + fi.right)) / P;
						for (int ri = top; ri <= bottom; ri++) {
							for (int ci = left; ci <= right; ci++) {
								double nextScore = 1.0 - Math.abs(originalSum[layer][ri][ci] - sumBuf[ri][ci]) * pixelCountDiv[ri][ci];
								diff += nextScore * nextScore - scoreBuf[ri][ci];
								sumBuf[ri][ci] = generalBuf[ri][ci];
							}
						}
						if (diff > bestDiff) {
							bestDiff = diff;
							ch = (char) uch;
						}
					}
					line.add(ch);
					sum += bestDiff;
					if (ch != 32) {
						FontInfo fi = curFonts[ch - MIN_CHAR];
						for (int j = 0; j < fi.r.length; j++) {
							int r = cr + fi.r[j];
							int c = cc + fi.c[j];
							if (r >= H || c >= W) continue;
							int prev = current[layer][r][c];
							if (prev >= 255) continue;
							int next = current[layer][r][c] + ts.saturation;
							if (next > 255) next = 255;
							sumBuf[r / P][c / P] += next - prev;
							generalBuf[r / P][c / P] += next - prev;
						}
						final int top = (cr + fi.top) / P;
						final int bottom = (Math.min(H - 1, cr + fi.bottom)) / P;
						final int left = (cc + fi.left) / P;
						final int right = (Math.min(W - 1, cc + fi.right)) / P;
						for (int ri = top; ri <= bottom; ri++) {
							for (int ci = left; ci <= right; ci++) {
								double nextScore = 1.0 - Math.abs(originalSum[layer][ri][ci] - sumBuf[ri][ci]) * pixelCountDiv[ri][ci];
								scoreBuf[ri][ci] = nextScore * nextScore;
							}
						}
					}
					cc += curFonts[ch - MIN_CHAR].w;
				}
				ts.lines.add(line);
			}
			if (sum > maxDiff) {
				maxDiff = sum;
				bestResult = ts;
			}
			long elapsed = System.currentTimeMillis() - beforeTime;
//			debug(String.format("sum:%.6f elapsed:%d fid:%d size:%d satu:%d",
//					sum / (pixelCountDiv.length * pixelCountDiv[0].length), elapsed, ts.fontId, ts.fontSize, ts.saturation));
			singleSolveTime = Math.max(singleSolveTime, elapsed);
		}
		return bestResult;
	}

	void improve(Typeset[] tss, double[] layerScore, long timelimit) {
		int step = 16;
		while (true) {
			for (int i = 0; i < tss.length; i++) {
				if (System.currentTimeMillis() + 200 > timelimit) {
					return;
				}
				debug(String.format("layer:%d satu:%d %.6f", tss[i].layer, tss[i].saturation, layerScore[tss[i].layer]));
				optimizeSaturation(tss[i], layerScore, step, timelimit);
				debug(String.format(" -> satu:%d %.6f", tss[i].saturation, layerScore[tss[i].layer]));
			}
			for (int i = 0; i < tss.length; i++) {
				if (System.currentTimeMillis() + 300 > timelimit) {
					break;
				}
				debug(String.format("layer:%d %.6f", tss[i].layer, layerScore[tss[i].layer]));
				optimizeLine(tss[i], layerScore, timelimit);
				debug(String.format(" -> %.6f", layerScore[tss[i].layer]));
//				verify(tss, layerScore);
			}
			step = Math.max(1, step / 2);
		}
	}

	void optimizeLine(Typeset ts, double[] layerScore, long timelimit) {
		FontInfo[] curFonts = fonts[ts.fontId][ts.fontSize - MIN_FONT_SIZE];
		int[][] backupCurrentSum = new int[20][currentSum[0][0].length];
		int cr = 0;
		for (ArrayList<Character> line : ts.lines) {
			if (System.currentTimeMillis() + 200 > timelimit) break;
			final int top = cr / P;
			final int bottom = Math.min(H - 1, cr + ts.lineHeight() - 1) / P;
			double beforeScore = 0;
			for (int i = top; i <= bottom; i++) {
				for (int j = 0; j < currentSum[0][0].length; j++) {
					double s = 1.0 - Math.abs(originalSum[ts.layer][i][j] - currentSum[ts.layer][i][j]) * pixelCountDiv[i][j];
					beforeScore += s * s;
				}
				System.arraycopy(currentSum[ts.layer][i], 0, backupCurrentSum[i - top], 0, currentSum[ts.layer][i].length);
			}
			for (int i = 0, cc = 0; i < line.size(); i++) {
				int ch = line.get(i);
				FontInfo fi = curFonts[ch - MIN_CHAR];
				if (!fi.isEmpty()) {
					for (int j = 0; j < fi.r.length; j++) {
						int r = cr + fi.r[j];
						int c = cc + fi.c[j];
						if (r >= H || c >= W) continue;
						int prev = current[ts.layer][r][c];
						current[ts.layer][r][c] -= ts.saturation;
						int next = current[ts.layer][r][c];
						if (next >= 255) continue;
						if (prev > 255) prev = 255;
						currentSum[ts.layer][r / P][c / P] += next - prev;
					}
				}
				cc += fi.w;
			}
//			debug(line);
			ArrayList<Character> newline = new ArrayList<>();
			for (int j = top; j <= bottom; j++) {
				System.arraycopy(currentSum[ts.layer][j], 0, sumBuf[j], 0, currentSum[ts.layer][j].length);
				System.arraycopy(currentSum[ts.layer][j], 0, generalBuf[j], 0, currentSum[ts.layer][j].length);
			}
			for (int j = top; j <= bottom; j++) {
				for (int k = 0; k < scoreBuf[0].length; k++) {
					double s = 1.0 - Math.abs(originalSum[ts.layer][j][k] - sumBuf[j][k]) * pixelCountDiv[j][k];
					scoreBuf[j][k] = s * s;
				}
			}
			int cc = 0;
			while (cc < W) {
				char ch = 32;
				double bestDiff = 0;
				for (int uch : USE_CHARS) {
					FontInfo fi = curFonts[uch - MIN_CHAR];
					if (fi.isEmpty()) continue;
					for (int j = 0; j < fi.r.length; j++) {
						int r = cr + fi.r[j];
						int c = cc + fi.c[j];
						if (r >= H || c >= W) continue;
						int prev = current[ts.layer][r][c];
						if (prev >= 255) continue;
						int next = current[ts.layer][r][c] + ts.saturation;
						if (next > 255) next = 255;
						sumBuf[r / P][c / P] += next - prev;
					}
					double diff = 0;
					final int t = (cr + fi.top) / P;
					final int b = (Math.min(H - 1, cr + fi.bottom)) / P;
					final int left = (cc + fi.left) / P;
					final int right = (Math.min(W - 1, cc + fi.right)) / P;
					for (int ri = t; ri <= b; ri++) {
						for (int ci = left; ci <= right; ci++) {
							double nextScore = 1.0 - Math.abs(originalSum[ts.layer][ri][ci] - sumBuf[ri][ci]) * pixelCountDiv[ri][ci];
							diff += nextScore * nextScore - scoreBuf[ri][ci];
							sumBuf[ri][ci] = generalBuf[ri][ci];
						}
					}
					if (diff > bestDiff) {
						bestDiff = diff;
						ch = (char) uch;
					}
				}
				newline.add(ch);
				if (ch != 32) {
					FontInfo fi = curFonts[ch - MIN_CHAR];
					for (int j = 0; j < fi.r.length; j++) {
						int r = cr + fi.r[j];
						int c = cc + fi.c[j];
						if (r >= H || c >= W) continue;
						int prev = current[ts.layer][r][c];
						current[ts.layer][r][c] += ts.saturation;
						int next = current[ts.layer][r][c];
						if (prev >= 255) continue;
						if (next > 255) next = 255;
						sumBuf[r / P][c / P] += next - prev;
						generalBuf[r / P][c / P] += next - prev;
					}
					final int t = (cr + fi.top) / P;
					final int b = (Math.min(H - 1, cr + fi.bottom)) / P;
					final int left = (cc + fi.left) / P;
					final int right = (Math.min(W - 1, cc + fi.right)) / P;
					for (int ri = t; ri <= b; ri++) {
						for (int ci = left; ci <= right; ci++) {
							double nextScore = 1.0 - Math.abs(originalSum[ts.layer][ri][ci] - sumBuf[ri][ci]) * pixelCountDiv[ri][ci];
							scoreBuf[ri][ci] = nextScore * nextScore;
						}
					}
				}
				cc += curFonts[ch - MIN_CHAR].w;
			}
			double afterScore = 0;
			for (int i = top; i <= bottom; i++) {
				System.arraycopy(sumBuf[i], 0, currentSum[ts.layer][i], 0, currentSum[ts.layer][i].length);
				for (int j = 0; j < currentSum[ts.layer][0].length; j++) {
					double s = 1.0 - Math.abs(originalSum[ts.layer][i][j] - currentSum[ts.layer][i][j]) * pixelCountDiv[i][j];
					afterScore += s * s;
				}
			}
//			if (DEBUG) {
//				beforeScore /= (bottom - top + 1) * currentSum[0][0].length;
//				afterScore /= (bottom - top + 1) * currentSum[0][0].length;
//				debug(beforeScore + " " + afterScore + " " + (afterScore - beforeScore));
//			}
			if (afterScore < beforeScore) {
				// revert
				for (int i = top; i <= bottom; i++) {
					System.arraycopy(backupCurrentSum[i - top], 0, currentSum[ts.layer][i], 0, currentSum[ts.layer][i].length);
				}
				cc = 0;
				for (int i = 0; i < newline.size(); i++) {
					int ch = newline.get(i);
					FontInfo fi = curFonts[ch - MIN_CHAR];
					if (!fi.isEmpty()) {
						for (int j = 0; j < fi.r.length; j++) {
							int r = cr + fi.r[j];
							int c = cc + fi.c[j];
							if (r >= H || c >= W) continue;
							current[ts.layer][r][c] -= ts.saturation;
						}
					}
					cc += fi.w;
				}
				cc = 0;
				for (int i = 0; i < line.size(); i++) {
					int ch = line.get(i);
					FontInfo fi = curFonts[ch - MIN_CHAR];
					if (!fi.isEmpty()) {
						for (int j = 0; j < fi.r.length; j++) {
							int r = cr + fi.r[j];
							int c = cc + fi.c[j];
							if (r >= H || c >= W) continue;
							current[ts.layer][r][c] += ts.saturation;
						}
					}
					cc += fi.w;
				}
			} else {
				line.clear();
				line.addAll(newline);
				layerScore[ts.layer] += (afterScore - beforeScore) / (currentSum[0].length * currentSum[0][0].length);
			}
			cr += ts.lineHeight();
		}
	}

	void verify(Typeset[] tss, double[] layerScore) {
		int[][][] pixel = new int[4][H][W];
		int[][][] sum = new int[4][(H + P - 1) / P][(W + P - 1) / P];
		for (Typeset ts : tss) {
			FontInfo[] font = fonts[ts.fontId][ts.fontSize - MIN_FONT_SIZE];
			int cr = 0;
			for (ArrayList<Character> line : ts.lines) {
				int cc = 0;
				for (char ch : line) {
					FontInfo fi = font[ch - MIN_CHAR];
					for (int i = 0; i < fi.r.length; i++) {
						int r = cr + fi.r[i];
						int c = cc + fi.c[i];
						if (r < H && c < W) pixel[ts.layer][r][c] += ts.saturation;
					}
					cc += fi.w;
				}
				cr += ts.lineHeight();
			}
		}
		for (int layer = 0; layer < 4; layer++) {
			for (int i = 0; i < H; i++) {
				for (int j = 0; j < W; j++) {
					sum[layer][i / P][j / P] += Math.min(pixel[layer][i][j], 255);
				}
			}
		}
		double[] score = new double[4];
		for (int layer = 0; layer < 4; layer++) {
			for (int i = 0; i < sum[0].length; i++) {
				for (int j = 0; j < sum[0][0].length; j++) {
					double s = 1.0 - Math.abs(originalSum[layer][i][j] - sum[layer][i][j]) * pixelCountDiv[i][j];
					score[layer] += s * s;
					if (currentSum[layer][i][j] != sum[layer][i][j]) {
						debug(layer + " " + i + " " + j + " " + currentSum[layer][i][j] + " " + sum[layer][i][j]);
					}
				}
			}
			score[layer] /= (sum[0].length * sum[0][0].length);
		}
		for (int i = 0; i < 4; i++) {
			if (Math.abs(score[i] - layerScore[i]) > 1e-5) {
				debug(i + " " + score[i] + " " + layerScore[i]);
				System.exit(0);
			}
		}
	}

	void optimizeSaturation(Typeset ts, double[] layerScore, int step, long timelimit) {
		double best = layerScore[ts.layer];
		int newSatu = -1;
		if (ts.saturation + step <= 255) {
			double s = scoreWithSaturation(ts, ts.saturation + step);
			if (s > layerScore[ts.layer]) {
				best = s;
				newSatu = ts.saturation + step;
				for (int satu = ts.saturation + step * 2; satu <= 255; satu += step) {
					s = scoreWithSaturation(ts, satu);
					if (s > best) {
						best = s;
						newSatu = satu;
					} else {
						break;
					}
				}
			}
		}
		if (newSatu == -1 && ts.saturation - step >= 0) {
			double s = scoreWithSaturation(ts, ts.saturation - step);
			if (s > layerScore[ts.layer]) {
				best = s;
				newSatu = ts.saturation - step;
				for (int satu = ts.saturation - step * 2; satu >= 0; satu -= step) {
					s = scoreWithSaturation(ts, satu);
					if (s > best) {
						best = s;
						newSatu = satu;
					} else {
						break;
					}
				}
			}
		}
		if (newSatu != -1) {
			FontInfo[] fis = fonts[ts.fontId][ts.fontSize - MIN_FONT_SIZE];
			final int diffSaturation = newSatu - ts.saturation;
			int cr = 0;
			for (ArrayList<Character> line : ts.lines) {
				int cc = 0;
				for (int i = 0; i < line.size(); i++) {
					int ch = line.get(i);
					FontInfo fi = fis[ch - MIN_CHAR];
					for (int j = 0; j < fi.r.length; j++) {
						int r = cr + fi.r[j];
						int c = cc + fi.c[j];
						if (r >= H || c >= W) continue;
						int prev = current[ts.layer][r][c];
						current[ts.layer][r][c] += diffSaturation;
						int next = prev + diffSaturation;
						currentSum[ts.layer][r / P][c / P] += Math.min(next, 255) - Math.min(prev, 255);
					}
					cc += fi.w;
				}
				cr += ts.lineHeight();
			}
			ts.saturation = newSatu;
			layerScore[ts.layer] = best;
		}
	}

	double scoreWithSaturation(Typeset ts, int newSaturation) {
		final int diffSaturation = newSaturation - ts.saturation;
		FontInfo[] fis = fonts[ts.fontId][ts.fontSize - MIN_FONT_SIZE];
		for (int j = 0; j < sumBuf.length; j++) {
			System.arraycopy(currentSum[ts.layer][j], 0, sumBuf[j], 0, currentSum[ts.layer][j].length);
		}
		int cr = 0;
		for (ArrayList<Character> line : ts.lines) {
			int cc = 0;
			for (int i = 0; i < line.size(); i++) {
				int ch = line.get(i);
				FontInfo fi = fis[ch - MIN_CHAR];
				for (int j = 0; j < fi.r.length; j++) {
					int r = cr + fi.r[j];
					int c = cc + fi.c[j];
					if (r >= H || c >= W) continue;
					int prev = current[ts.layer][r][c];
					int next = prev + diffSaturation;
					sumBuf[r / P][c / P] += Math.min(next, 255) - Math.min(prev, 255);
				}
				cc += fi.w;
			}
			cr += ts.lineHeight();
		}
		double score = 0;
		for (int i = 0; i < currentSum[0].length; i++) {
			for (int j = 0; j < currentSum[0][0].length; j++) {
				double s = 1.0 - Math.abs(originalSum[ts.layer][i][j] - sumBuf[i][j]) * pixelCountDiv[i][j];
				score += s * s;
			}
		}
		return score / (currentSum[0].length * currentSum[0][0].length);
	}

	private double calcScore(int layer) {
		double sum = 0;
		for (int j = 0; j < currentSum[0].length; j++) {
			for (int k = 0; k < currentSum[0][0].length; k++) {
				double expect = originalSum[layer][j][k];
				double actual = currentSum[layer][j][k];
				double s = 1 - Math.abs(expect - actual) * pixelCountDiv[j][k];
				sum += s * s;
			}
		}
		return sum / (currentSum[0].length * currentSum[0][0].length);
	}

	class Typeset {
		ArrayList<ArrayList<Character>> lines = new ArrayList<>();
		int fontId, fontSize, layer, saturation;

		int lineHeight() {
			return fonts[fontId][fontSize - MIN_FONT_SIZE][0].h;
		}
	}

	class FontImageGetter {
		int fontId = 0;
		int fontSize = MIN_FONT_SIZE;
		int ch = -1;

		int[] next(int width, int height, int[] bitmask) {
			if (height > 0) {
				FontInfo fi = new FontInfo(width, height);
				int count = 0;
				for (int bits : bitmask) {
					count += Integer.bitCount(bits);
				}
				fi.r = new int[count];
				fi.c = new int[count];
				int pos = 0;
				int minR = 99;
				for (int i = 0; i < height; i++) {
					for (int j = 0; j < width; j += 32) {
						int bits = bitmask[i * ((width + 31) / 32) + j / 32];
						if (bits != 0) {
							minR = Math.min(minR, i);
						}
						for (int k = 0; k < 32 && j + k < width; k++) {
							if (((bits >> k) & 1) != 0) {
								fi.r[pos] = i;
								fi.c[pos] = j + k;
								fi.top = Math.min(fi.top, i);
								fi.bottom = Math.max(fi.bottom, i);
								fi.left = Math.min(fi.left, j + k);
								fi.right = Math.max(fi.right, j + k);
								pos++;
							}
						}
					}
				}
//				debug(USE_CHARS[ch] + " " + height + " " + width + " " + fi.top + " " + fi.bottom + " " + fi.left + " " + fi.right);
				fonts[fontId][fontSize - MIN_FONT_SIZE][USE_CHARS[ch] - MIN_CHAR] = fi;
				for (int i = 0; i < ch; i++) {
					FontInfo of = fonts[fontId][fontSize - MIN_FONT_SIZE][USE_CHARS[i] - MIN_CHAR];
					if (Arrays.equals(fi.r, of.r) && Arrays.equals(fi.c, of.c)) {
//						debug(fontId + " " + fontSize + " " + USE_CHARS[ch] + " " + USE_CHARS[i] + " " + fi.r.length);
						fi.r = fi.c = new int[0]; // make it empty
						break;
					}
				}
			}
			ch++;
			if (ch == USE_CHARS.length) {
				ch = 0;
				fontSize++;
			}
			if (fontSize == MAX_FONT_SIZE + 1) {
				fontSize = MIN_FONT_SIZE;
				fontId++;
			}
			if (fontId == F) return new int[0];
			return new int[]{USE_CHARS[ch], fontId, fontSize};
		}
	}

	class FontInfo {
		int[] r, c;
		int h, w;
		int top, bottom, left, right;

		FontInfo(int w, int h) {
			this.h = h;
			this.w = w;
			this.top = h;
			this.bottom = 0;
			this.left = w;
			this.right = 0;
		}

		boolean isEmpty() {
			return this.r.length == 0;
		}
	}

	public static void main(String[] args) throws IOException {
		TypesetterArt solution = new TypesetterArt();

		try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
			int nRenders = Integer.parseInt(in.readLine());
			int nFonts = Integer.parseInt(in.readLine());
			int precision = Integer.parseInt(in.readLine());
			int width = Integer.parseInt(in.readLine());
			int height = Integer.parseInt(in.readLine());
			{
				int n = Integer.parseInt(in.readLine());
				int[] image = new int[n];

				for (int i = 0; i < n; i++) {
					image[i] = Integer.parseInt(in.readLine());
				}

				System.err.printf("Calling init\n");

				int d = solution.init(nRenders, nFonts, precision, width, height, image);

				System.err.printf("Return from init: %d\n", d);

				System.out.printf("%d\n", d);
				System.out.flush();
			}

			boolean callNext = true;
			while (callNext) {
				int glyphWidth = Integer.parseInt(in.readLine());
				int glyphHeight = Integer.parseInt(in.readLine());
				int n = Integer.parseInt(in.readLine());
				int[] bitmask = new int[n];
				for (int i = 0; i < n; i++) {
					bitmask[i] = Integer.parseInt(in.readLine());
				}
				int remainingMillis = Integer.parseInt(in.readLine());

				System.err.printf("Calling next glyph\n");

				int[] ret = solution.nextGlyph(glyphWidth, glyphHeight, bitmask, remainingMillis);

				System.err.printf("Return from next glyph size: %d\n", ret.length);

				if (ret.length == 0) {
					callNext = false;
					System.out.printf("\n");
				} else {
					System.out.printf("%d %d %d\n", ret[0], ret[1], ret[2]);
				}
				System.out.flush();
			}

			for (int i = 0; i < nRenders; i++) {
				int remainingMillis = Integer.parseInt(in.readLine());

				System.err.printf("Calling render\n");

				int[] ret = solution.render(i, remainingMillis);

				System.err.printf("Return from render size: %d\n", ret.length);

				System.out.printf("%d\n", ret.length);
				for (int x : ret) {
					System.out.printf("%d\n", x);
				}
				System.out.flush();
			}
		}
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}
}