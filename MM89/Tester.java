/* Change log
2015-10-12 : Remove ret from search method
*/

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Path {
	int startRow, startCol;
	String dirs;

	Path(int row, int col, String dirs) {
		startRow = row;
		startCol = col;
		this.dirs = dirs;
	}

	public String toString() {
		return "(" + startRow + "," + startCol + ") " + dirs;
	}
}

public class Tester {
	static final int[] dr = { 0, 1, 0, -1 };
	static final int[] dc = { 1, 0, -1, 0 };
	static final char[] rls = { 'R', 'U', 'L', 'S', 'E' };
	String[] maze;
	char[][] M;
	int W, H, F, N;

	void generate(long seed) throws Exception {
		SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
		r1.setSeed(seed);
		if (seed <= 3) {
			W = H = (int) seed * 10;
		} else if (seed == 10) {
			W = H = 80;
		} else {
			W = r1.nextInt(71) + 10;
			H = r1.nextInt(71) + 10;
		}
		M = new char[H][W];
		for (int i = 0; i < H; i++)
			M[i][0] = M[i][W - 1] = '.';
		for (int j = 0; j < W; j++)
			M[0][j] = M[H - 1][j] = '.';

		for (int i = 1; i < H - 1; i++)
			for (int j = 1; j < W - 1; j++)
				M[i][j] = rls[r1.nextInt(29) / 7];
		// remove corners randomly and mirror the result 3 times
		int crem = W / 2 - 1, rrem = r1.nextInt(H / 4) + H / 4 - 1;
		N = (W - 2) * (H - 2);
		for (int i = 1; i < rrem; i++) {
			if (crem < 3) break;
			crem = r1.nextInt(crem / 3) + (2 * crem) / 3;
			for (int j = 1; j < crem; j++) {
				M[i][j] = '.';
				M[H - i - 1][j] = '.';
				M[i][W - j - 1] = '.';
				M[H - i - 1][W - j - 1] = '.';
				N -= 4;
			}
		}
		F = r1.nextInt(N / 3 - N / 10) + N / 10;

		maze = new String[H];
		for (int i = 0; i < H; i++)
			maze[i] = new String(M[i]);
		if (debug) {
			for (int i = 0; i < H; i++) {
				addFatalError(maze[i]);
			}
		}

		// preprocess: mark all border cells with *
		for (int r = 0; r < H; ++r)
			for (int c = 0; c < W; ++c) {
				if (M[r][c] != '.') continue;
				for (int i = 0; i < 4; ++i)
					if (!outside(r + dr[i], c + dc[i])) M[r][c] = '*';
			}
	}

	boolean outside(int r, int c) {
		return r < 0 || r > H - 1 || c < 0 || c > W - 1 || M[r][c] == '.' || M[r][c] == '*';
	}

	boolean[][] visitedOnePath;
	boolean[][] visitedOverall;
	Vector<Path> paths;

	void search(int curR, int curC, int curDir, int curLen, int startRow, int startCol, String pathSoFar) {
		int newR = curR + dr[curDir];
		int newC = curC + dc[curDir];
		if (visitedOnePath[newR][newC]) {
			return;
		}
		visitedOnePath[newR][newC] = true;
		String newPath = pathSoFar + curDir;
		// proceed from new cell in new direction depending on the type of the cell
		switch (M[newR][newC]) {
		case '*':
			for (int r = 0; r < H; ++r)
				for (int c = 0; c < W; ++c)
					visitedOverall[r][c] = visitedOverall[r][c] || visitedOnePath[r][c];
			paths.add(new Path(startRow, startCol, newPath));
			break;
		case 'S':
			search(newR, newC, curDir, curLen + 1, startRow, startCol, newPath);
			break;
		case 'R':
			search(newR, newC, (curDir + 1) % 4, curLen + 1, startRow, startCol, newPath);
			break;
		case 'U':
			search(newR, newC, (curDir + 2) % 4, curLen + 1, startRow, startCol, newPath);
			break;
		case 'L':
			search(newR, newC, (curDir + 3) % 4, curLen + 1, startRow, startCol, newPath);
			break;
		case 'E':
			for (int newDir = 0; newDir < 4; ++newDir)
				search(newR, newC, newDir, curLen + 1, startRow, startCol, newPath);
		}
		visitedOnePath[newR][newC] = false;
	}

	double getScore() {
		// count the % of cells visited by good paths in the maze. path must
		// 1) start in any * (. cell adjacent to a non-. cell)
		// 2) end in any * cell
		// 3) contain no loops (i.e. can't visit one cell twice)

		visitedOnePath = new boolean[H][W];
		visitedOverall = new boolean[H][W];
		paths = new Vector<>();
		// start from each * cell and do full search to find all paths which go from it
		for (int r = 0; r < H; ++r)
			for (int c = 0; c < W; ++c)
				if (M[r][c] == '*') for (int dir = 0; dir < 4; ++dir)
					if (!outside(r + dr[dir], c + dc[dir])) search(r, c, dir, 0, r, c, "");

		int nvis = 0;
		for (int r = 0; r < H; ++r)
			for (int c = 0; c < W; ++c)
				if (!outside(r, c) && visitedOverall[r][c]) nvis++;
		return nvis * 1.0 / N;
	}

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		try {
			generate(seed);
			res.H = H;
			res.W = W;
			res.N = N;
			res.F = F;
			if (SZ < 0) {
				SZ = Math.min(900 / (H + 1), 1400 / (W + 1));
			}

			long startTime = System.currentTimeMillis();
			String[] ret = improve(maze, F);
			res.elapsed = System.currentTimeMillis() - startTime;

			if (ret.length > F) {
				addFatalError("Your return contained more than F elements.");
				return res;
			}
			for (int i = 0; i < ret.length; i++) {
				String[] s = ret[i].split(" ");
				if (s.length != 3) {
					addFatalError("Element " + i + " of your return must be formatted as \"R C T\"");
					return res;
				}
				int R, C;
				try {
					R = Integer.parseInt(s[0]);
					C = Integer.parseInt(s[1]);
				} catch (Exception e) {
					addFatalError("R and C in element " + i + " of your return must be integers.");
					return res;
				}
				if (outside(R, C)) {
					addFatalError("R and C in element " + i + " of your return must specify a cell within the maze.");
					return res;
				}
				if (s[2].length() != 1) {
					addFatalError("T in element " + i + " of your return must be a single character.");
					return res;
				}
				char T = s[2].charAt(0);
				if (T != 'R' && T != 'U' && T != 'L' && T != 'S') {
					addFatalError("T in element " + i + " of your return must be 'R','U','L' or 'S'.");
					return res;
				}
				if (M[R][C] == 'E') {
					addFatalError("Element " + i + " of your return is trying to change a cell that contains 'E'.");
					return res;
				}

				M[R][C] = T;
			}

			res.score = getScore();

			if (vis) {
				jf.setSize((W + 2) * SZ + 30, (H + 2) * SZ + 40);
				v.repaint();
				jf.setVisible(true);
			}

			if (debug) {
				addFatalError("Paths after fix: " + paths.size());
				System.out.println(paths);
			}
			if (proc != null) {
				try {
					proc.destroy();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return res;
		} catch (Exception e) {
			System.err.println("An exception occurred while trying to get your program's results.");
			e.printStackTrace();
			return res;
		}
	}

	// ------------- visualization part ------------
	static String exec = "./tester";
	static boolean vis, debug;
	static int SZ = -1;
	Process proc;
	JFrame jf;
	Vis v;

	String[] improve(String[] maze, int F) throws IOException {
		if (proc == null) return new String[0];
		StringBuffer sb = new StringBuffer();
		sb.append(maze.length).append('\n');
		for (int i = 0; i < maze.length; i++)
			sb.append(maze[i]).append('\n');
		sb.append(F).append('\n');
		OutputStream os = proc.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();

		InputStream is = proc.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		int R = Integer.parseInt(br.readLine());
		String[] ret = new String[R];
		for (int i = 0; i < R; i++)
			ret[i] = br.readLine();
		return ret;
	}

	public class Vis extends JPanel {
		int selectR = -1, selectC = -1, dir = -1;

		Vis() {
			addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					int r = e.getY() / SZ - 1;
					int c = e.getX() / SZ - 1;
					if (r < 0 || r > H || c < 0 || c > W) {
						selectR = selectC = dir = -1;
					} else {
						selectR = r;
						selectC = c;
					}
					repaint();
				}
			});
		}

		public void paint(Graphics g) {
			BufferedImage bi = new BufferedImage((W + 2) * SZ, (H + 2) * SZ, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) bi.getGraphics();
			//background
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, (W + 2) * SZ, (H + 2) * SZ);
			//mark visited cells
			g2.setColor(new Color(0xDDDDDD));
			for (int i = 0; i < H; i++)
				for (int j = 0; j < W; j++)
					if (!outside(i, j) && visitedOverall[i][j]) g2.fillRect(SZ + j * SZ, SZ + i * SZ, SZ, SZ);
			if (selectR != -1 && selectC != -1) {
				g2.setColor(new Color(0xDD0000));
				g2.fillRect(SZ + selectC * SZ, SZ + selectR * SZ, SZ, SZ);
			}

			//grid
			g2.setColor(Color.BLACK);
			for (int i = 0; i <= H; i++)
				g2.drawLine(SZ + 0, SZ + i * SZ, SZ + W * SZ, SZ + i * SZ);
			for (int i = 0; i <= W; i++)
				g2.drawLine(SZ + i * SZ, SZ + 0, SZ + i * SZ, SZ + H * SZ);
			//bold lines on the border of the maze (maze is symmetrical, so no worry about row order)
			for (int i = 0; i < H; i++)
				for (int j = 0; j <= W; j++)
					if (outside(i, j - 1) != outside(i, j)) g2.drawLine(SZ + j * SZ + 1, SZ + i * SZ, SZ + j * SZ + 1, SZ + (i + 1) * SZ);
			for (int j = 0; j < W; j++)
				for (int i = 0; i <= H; i++)
					if (outside(i - 1, j) != outside(i, j)) g2.drawLine(SZ + j * SZ, SZ + i * SZ + 1, SZ + (j + 1) * SZ, SZ + i * SZ + 1);

			//cells
			g2.setFont(new Font("Arial", Font.BOLD, Math.max(10, SZ / 2)));
			FontMetrics fm = g2.getFontMetrics();
			char[] ch = new char[1];
			for (int i = 0; i < H; i++)
				for (int j = 0; j < W; j++)
					if (!outside(H - i - 1, j)) {
						ch[0] = M[H - i - 1][j];
						g2.drawChars(ch, 0, 1, SZ * (j + 1) + (SZ / 2) - fm.charWidth(ch[0]) / 2, SZ * (H - i) + (SZ / 2) - 1 + fm.getHeight() / 2);
					}

			// path #pathIndex (can click through)
			//			if (pathIndex > -1 && paths.size() > 0) {
			//				g2.setColor(Color.BLUE);
			//				Path p = paths.elementAt(pathIndex);
			//				int r = p.startRow;
			//				int c = p.startCol;
			//				g2.fillOval(SZ + c * SZ + SZ / 4, SZ + r * SZ + SZ / 4, SZ / 2, SZ / 2);
			//				for (int d = 0; d < p.dirs.length(); ++d) {
			//					int newDir = (int) (p.dirs.charAt(d) - '0');
			//					int newR = r + dr[newDir];
			//					int newC = c + dc[newDir];
			//					g2.drawLine(SZ + c * SZ + SZ / 2, SZ + r * SZ + SZ / 2, SZ + newC * SZ + SZ / 2, SZ + newR * SZ + SZ / 2);
			//					r = newR;
			//					c = newC;
			//				}
			//			}
			drawPath(g2);

			g.drawImage(bi, 0, 0, (W + 2) * SZ, (H + 2) * SZ, null);
		}

		void drawPath(Graphics2D g) {
			if (selectR == -1 || selectC == -1 || dir == -1) return;
			g.setColor(Color.BLUE);
			g.fillOval(SZ + selectC * SZ + SZ / 2 - SZ / 16, SZ + selectR * SZ + SZ / 2 - SZ / 16, SZ / 8, SZ / 8);

			// forward
			ArrayList<Integer> pos = new ArrayList<>();
			int idx = 0;
			pos.add((dir << 16) + (selectR << 8) + selectC);
			while (idx < pos.size()) {
				int cur = pos.get(idx++);
				int cd = cur >> 16;
				int cr = (cur >> 8) & 0xFF;
				int cc = cur & 0xFF;
				int nr = cr + dr[cd];
				int nc = cc + dc[cd];
				drawArray(g, cr, cc, nr, nc);
				if (outside(nr, nc)) continue;
				int nd, npos;
				switch (M[nr][nc]) {
				case 'R':
					nd = (cd + 1) % 4;
					npos = getPos(nd, nr, nc);
					if (!pos.contains(npos)) pos.add(npos);
					break;
				case 'L':
					nd = (cd + 3) % 4;
					npos = getPos(nd, nr, nc);
					if (!pos.contains(npos)) pos.add(npos);
					break;
				case 'U':
					nd = (cd + 2) % 4;
					npos = getPos(nd, nr, nc);
					if (!pos.contains(npos)) pos.add(npos);
					break;
				case 'S':
					npos = getPos(cd, nr, nc);
					if (!pos.contains(npos)) pos.add(npos);
					break;
				case 'E':
					for (int i = 0; i < 4; ++i) {
						nd = (cd + i) % 4;
						npos = getPos(nd, nr, nc);
						if (!pos.contains(npos)) pos.add(npos);
					}
					break;
				default:
					break;
				}
			}
		}

		void drawArray(Graphics2D g, int fr, int fc, int tr, int tc) {
			g.drawLine(SZ + fc * SZ + SZ / 2, SZ + fr * SZ + SZ / 2, SZ + tc * SZ + SZ / 2, SZ + tr * SZ + SZ / 2);
			int cx = SZ + tc * SZ + SZ / 2;
			int cy = SZ + tr * SZ + SZ / 2;
			if (fr == tr) {
				int dc = (tc - fc) * SZ / 8;
				cx -= dc;
				g.drawLine(cx, cy, cx - dc, cy + dc);
				g.drawLine(cx, cy, cx - dc, cy - dc);
			} else {
				int dr = (tr - fr) * SZ / 8;
				cy -= dr;
				g.drawLine(cx, cy, cx + dr, cy - dr);
				g.drawLine(cx, cy, cx - dr, cy - dr);
			}
		}

		int getPos(int dir, int r, int c) {
			return (dir << 16) + (r << 8) + c;
		}
	}

	public Tester() {
		try {
			if (vis) {
				jf = new JFrame();
				v = new Vis();
				jf.getContentPane().add(v);
				jf.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						if (proc != null) try {
							proc.destroy();
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						System.exit(0);
					}
				});
				jf.addKeyListener(new KeyAdapter() {
					public void keyPressed(KeyEvent e) {
						if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
							v.dir = 0;
						} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
							v.dir = 2;
						} else if (e.getKeyCode() == KeyEvent.VK_UP) {
							v.dir = 3;
						} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
							v.dir = 1;
						} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
							v.dir = -1;
						}
						v.repaint();
					}
				});

			}
			if (exec != null) {
				try {
					Runtime rt = Runtime.getRuntime();
					proc = rt.exec(exec);
					new ErrorReader(proc.getErrorStream()).start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 1;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-exec")) exec = args[++i];
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-debug")) debug = true;
			if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
		}
		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int H, W, N, F;
		double score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("H:%2d W:%2d N:%4d F:%4d\n", H, W, N, F);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("score:%.5f", score);
			return ret;
		}
	}
}

class ErrorReader extends Thread {
	InputStream error;

	public ErrorReader(InputStream is) {
		error = is;
	}

	public void run() {
		try {
			byte[] ch = new byte[50000];
			int read;
			while ((read = error.read(ch)) > 0) {
				String s = new String(ch, 0, read);
				System.out.print(s);
				System.out.flush();
			}
		} catch (Exception e) {}
	}
}
