#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <sys/time.h>

#ifndef LOCAL
#define NDEBUG
#endif
#include <cassert>

using namespace std;
using u8 = uint8_t;
using u32 = uint32_t;
using u64 = uint64_t;
using ll = int64_t;
using ull = uint64_t;

#ifndef TEST
#define MEASURE_TIME 1
#define DEBUG 1
#endif

namespace {
const int DR[] = {-1, 0, 1, 0};
const int DC[] = {0, 1, 0, -1};
const string DIR_STR = "SRUL";

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 5000;
#else
#ifdef TEST
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 5000;
#else
const double CLOCK_PER_SEC = 3.6e9;
const ll TL = 9500;
#endif
#endif

// msec
inline ll getTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

XorShift rnd;
ll startTime;
Timer timer;
Counter counter;

class MazeFixing {
public:
	MazeFixing();
	vector<string> improve(vector<string>& maze, int F);
};

MazeFixing::MazeFixing() {
	startTime = getTime();
}

vector<string> MazeFixing::improve(vector<string>& maze, int F) {
	return {};
}


#if defined(LOCAL) || defined(TEST)
int main() {
	int H,F;
	cin >> H;
	vector<string> maze(H);
	for (auto& row : maze) {
		cin >> row;
	}
	cin >> F;

	MazeFixing obj;
	auto ret = obj.improve(maze, F);
	cout << ret.size() << endl;
	for (const auto& elem : ret) {
		cout << elem << endl;
	}
	cout.flush();
}
#endif
