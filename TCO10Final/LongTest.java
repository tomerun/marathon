class LongTest {
	long seed;
	Visualizer vis;
	CollapsingMaze obj;
	long time;

	public LongTest(long seed, Visualizer v) {
		vis = v;
		this.seed = seed;
		long prevTime = System.currentTimeMillis();
		obj = new CollapsingMaze();
		time = System.currentTimeMillis() - prevTime;
	}

	public void init(String[] s, int T) {
		if (vis != null) {
			vis.doupdate();
		}
		long prevTime = System.currentTimeMillis();
		obj.init(s, T);
		time += System.currentTimeMillis() - prevTime;
	}

	public int move(int[] colR, int[] colC, int curR, int curC) {
		if (vis != null) {
			vis.doupdate();
		}
		long prevTime = System.currentTimeMillis();
		int moveResult = obj.move(colR, colC, curR, curC);
		time += System.currentTimeMillis() - prevTime;
		return moveResult;
	}

}
