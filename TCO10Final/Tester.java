import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

public class Tester {
	int W, H, D, T, P;
	double p, q;
	double exp;
	int[][] maze;
	boolean ready = false;
	int[][] drilled;
	boolean done;
	int[] dr = { -1, 0, 1, 0 };
	int[] dc = { 0, 1, 0, -1 };
	Random rnd;

	int curR, curC;
	int[] colR, colC;

	int adj(int r, int c, int id) {
		int ret = 0;
		id = 1 << id;
		for (int i = 0; i < 4; i++) {
			int rr = r + dr[i];
			int cc = c + dc[i];
			if (rr >= 0 && cc >= 0 && rr <= H - 1 && cc <= W - 1 && maze[rr][cc] > 0 && (maze[rr][cc] & id) > 0) {
				ret++;
			}
		}
		return ret;
	}

	void set(int r, int c, int id) {
		if (maze[r][c] < 0) maze[r][c] = 0;
		maze[r][c] |= 1 << id;
	}

	void dfs(int r, int c, int id) {
		int[] qr = new int[W * H];
		int[] qc = new int[W * H];
		qr[0] = r;
		qc[0] = c;
		int tl = 1;
		int[] dirs = new int[4];
		set(r, c, id);
		done = false;
		while (tl > 0) {
			tl--;
			r = qr[tl];
			c = qc[tl];
			if (c != 0 && adj(r, c, id) != 1) continue;
			set(r, c, id);
			for (int i = 0; i < 4; i++) {
				dirs[i] = i;
				int j = rnd.nextInt(i + 1);
				int tmp = dirs[j];
				dirs[j] = dirs[i];
				dirs[i] = tmp;
			}
			for (int i = 0; i < 4; i++) {
				int dir = dirs[i];
				int rr = r + dr[dir];
				int cc = c + dc[dir];
				if (rr > 0 && cc > 0 && rr < H - 1 && (cc < W - 1 || !done)) {
					int adj = adj(rr, cc, id);
					if (adj == 1) {
						if (cc == W - 1) {
							set(rr, cc, id);
							done = true;
						} else {
							qr[tl] = rr;
							qc[tl] = cc;
							tl++;
						}
					}
				}
			}
		}
	}

	int[][] sol;
	int[] openr;
	int[] openc;
	int openptr;
	int[] qr, qc, qd;

	void collapseInit() {
		if (openr == null) {
			openr = new int[H * W];
			openc = new int[H * W];
			qr = new int[D * D * 4 + 1];
			qc = new int[D * D * 4 + 1];
			qd = new int[D * D * 4 + 1];
		}
	}

	int collapseTime() {
		collapseInit();
		openptr = 0;
		for (int i = 0; i < H; i++) {
			for (int j = 0; j < W; j++) {
				if (maze[i][j] >= 0) {
					openr[openptr] = i;
					openc[openptr] = j;
					openptr++;
				}
			}
		}
		int collapsed = 0;
		int collapses = 0;
		while (true) {
			if (collapsed > openptr * 0.8) {
				collapsed = 0;
				openptr = 0;
				for (int i = 0; i < H; i++) {
					for (int j = 0; j < W; j++) {
						if (maze[i][j] >= 0) {
							openr[openptr] = i;
							openc[openptr] = j;
							openptr++;
						}
					}
				}
			}
			if (openptr == 0) {
				return collapses;
			}
			int idx, r, c;
			do {
				idx = rnd.nextInt(openptr);
				r = openr[idx];
				c = openc[idx];
			} while (maze[r][c] < 0);
			int dm = rnd.nextInt(D + 1);
			int h = 0, t = 1;
			qr[0] = r;
			qc[0] = c;
			qd[0] = 0;
			maze[r][c] = -2;
			collapsed++;
			while (h < t) {
				r = qr[h];
				c = qc[h];
				int d = qd[h];
				h++;
				for (int i = 0; i < 4 && d < dm; i++) {
					int rr = r + dr[i];
					int cc = c + dc[i];
					if (rr >= 0 && rr < H && cc >= 0 && cc < W && maze[rr][cc] >= 0) {
						maze[rr][cc] = -2;
						collapsed++;
						qr[t] = rr;
						qc[t] = cc;
						qd[t] = d + 1;
						t++;
					}
				}
			}
			collapses++;
		}
	}

	void collapse() {
		collapseInit();
		openptr = 0;
		for (int i = 0; i < H; i++) {
			for (int j = 0; j < W; j++) {
				if (maze[i][j] >= 0) {
					openr[openptr] = i;
					openc[openptr] = j;
					openptr++;
				}
			}
		}
		if (openptr == 0) {
			return;
		}
		int idx = rnd.nextInt(openptr);
		int r = openr[idx];
		int c = openc[idx];
		int dm = rnd.nextInt(D + 1);
		int h = 0, t = 1;
		qr[0] = r;
		qc[0] = c;
		qd[0] = 0;
		maze[r][c] = -2;
		while (h < t) {
			r = qr[h];
			c = qc[h];
			int d = qd[h];
			h++;
			for (int i = 0; i < 4 && d < dm; i++) {
				int rr = r + dr[i];
				int cc = c + dc[i];
				if (rr >= 0 && rr < H && cc >= 0 && cc < W && maze[rr][cc] >= 0) {
					maze[rr][cc] = -2;
					qr[t] = rr;
					qc[t] = cc;
					qd[t] = d + 1;
					t++;
				}
			}
		}
		colR = new int[t];
		colC = new int[t];
		for (int i = 0; i < t; i++) {
			colR[i] = qr[i];
			colC[i] = qc[i];
		}
	}

	int[] adjs;
	int[][] rsa, csa;
	int[][] trsa, tcsa;
	int[] ptrs;

	int pruneOne(int id) {
		int[] rs = rsa[id];
		int[] cs = csa[id];
		int[] trs = trsa[id];
		int[] tcs = tcsa[id];
		int ptr = ptrs[id];
		if (rs == null) {
			rs = rsa[id] = new int[H * W];
			cs = csa[id] = new int[H * W];
			trs = trsa[id] = new int[H * W];
			tcs = tcsa[id] = new int[H * W];
			for (int i = 1; i + 1 < H; i++) {
				for (int j = 1; j + 1 < W; j++) {
					rs[ptr] = i;
					cs[ptr] = j;
					ptr++;
				}
			}
		}
		for (int i = 0; i < ptr; i++) {
			int r = rs[i];
			int c = cs[i];
			if (maze[r][c] >= 0) {
				adjs[i] = adj(r, c, id);
			}
		}
		int tptr = 0;
		int ret = 0;
		for (int i = 0; i < ptr; i++) {
			if (adjs[i] <= 1) {
				int r = rs[i];
				int c = cs[i];
				if (maze[r][c] >= 0 && (maze[r][c] & (1 << id)) > 0) {
					maze[r][c] &= ~(1 << id);
					if (maze[r][c] == 0) {
						ret++;
						maze[r][c] = -1;
					}
					for (int d = 0; d < 4; d++) {
						int rr = r + dr[d];
						int cc = c + dc[d];
						if ((maze[rr][cc] & (1 << id)) > 0 && rr >= 1 && rr + 1 < H && cc >= 1 && cc + 1 < W) {
							trs[tptr] = rr;
							tcs[tptr] = cc;
							tptr++;
						}
					}
				}
			}
		}
		trsa[id] = rs;
		rsa[id] = trs;
		tcsa[id] = cs;
		csa[id] = tcs;
		ptrs[id] = tptr;
		return tptr == 0 ? -1 : ret;
	}

	void prune(double target, int ids) {
		adjs = new int[H * W];
		tcsa = new int[ids][];
		rsa = new int[ids][];
		csa = new int[ids][];
		trsa = new int[ids][];
		tcsa = new int[ids][];
		ptrs = new int[ids];
		int empty = 0;
		for (int i = 0; i < H; i++) {
			for (int j = 0; j < W; j++) {
				if (maze[i][j] >= 0) {
					empty++;
				}
			}
		}
		while (empty > target * W * H) {
			int diff = 0;
			boolean changed = false;
			for (int id = 0; id < ids; id++) {
				int remed = pruneOne(id);
				if (remed >= 0) {
					diff += remed;
					changed = true;
				}
			}
			if (!changed) {
				break;
			}
			empty -= diff;
		}
	}

	void reset() {
		for (int i = 0; i < H; i++) {
			Arrays.fill(maze[i], -1);
		}
	}

	public void init(long seed) {
		try {
			rnd = SecureRandom.getInstance("SHA1PRNG");
			rnd.setSeed(seed);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int _W = rnd.nextInt(901) + 100;
		int _H = rnd.nextInt(901) + 100;
		if (W <= 0) {
			W = _W;
		}
		if (H <= 0) {
			H = _H;
		}
		T = rnd.nextInt(91) + 10; // drill time
		D = rnd.nextInt(20); //bfs dist
		p = rnd.nextDouble() / 10; //frequency
		q = rnd.nextDouble() / 10; //frequency
		if (seed == 1) {
			W = 100;
			H = 100;
			T = 10;
			p = 0;
			q = 0.1;
		}
		int reps = rnd.nextInt(3) + 1;
		maze = new int[H][W];
		drilled = new int[H][W];
		reset();
		done = false;
		curC = 0;
		for (int i = 0; i < reps; i++) {
			curR = rnd.nextInt(H);
			dfs(curR, curC, i);
		}
		prune(0.0, reps);
		for (int i = 0; i < H; i++) {
			for (int j = 0; j < W; j++) {
				if (maze[i][j] >= 0) {
					if (rnd.nextDouble() < q) {
						maze[i][j] = 1;
					} else {
						maze[i][j] = 0;
					}
				}
			}
		}
		maze[curR][curC] = 0;
		int[][] tmp = maze;
		maze = new int[H][W];
		for (int k = 0; k < 10; k++) {
			for (int i = 0; i < H; i++) {
				maze[i] = tmp[i].clone();
			}
			int rounds = collapseTime();
			//			System.out.println("collapseTime:" + rounds);
			if (p == 0) {
				exp += rounds * q / 10 / 0.01;
			} else {
				exp += rounds * q / 10 / p;
			}
		}
		maze = tmp;
		ready = true;
	}

	String[] mazeAsString() {
		String[] m = new String[H];
		for (int i = 0; i < H; i++) {
			m[i] = "";
			for (int j = 0; j < W; j++) {
				if (curR == i && curC == j) {
					m[i] += '^';
				} else if (maze[i][j] == 0) {
					m[i] += '.';
				} else if (maze[i][j] == 1) {
					m[i] += '*';
				} else {
					m[i] += '#';
				}
			}
		}
		return m;
	}

	int L;
	int time;
	static final long TL = CollapsingMaze.TIMELIMIT + 200;

	public Result runTest(LongTest lt) {
		Result result = new Result();
		result.seed = lt.seed;
		try {
			init(lt.seed);
			result.W = this.W;
			result.H = this.H;
			result.T = this.T;
			result.D = this.D;
			result.p = this.p;
			result.q = this.q;
			String[] m = mazeAsString();
			lt.init(m, T);
			boolean moved = false;
			while (true) {
				if (rnd.nextDouble() < p) {
					collapse();
				} else {
					colR = colC = new int[0];
				}
				if (maze[curR][curC] < 0) {
					//					System.err.println("You were smushed! :(");
					result.status = "smushed";
					break;
				}
				int dir = lt.move(colR, colC, curR, curC);
				time++;
				result.elapsed = lt.time;
				if (result.elapsed > TL) {
					result.status = "time limit exceeded";
					break;
				}
				if (dir < 0 || dir > 3) {
					System.err.println("Invalid direction: " + dir);
					break;
				}
				int rr = curR + dr[dir];
				int cc = curC + dc[dir];
				if (rr < 0 || cc < 0 || rr == H || cc == W) {
					System.err.println("Attempted to move off the board");
					break;
				}
				if (maze[rr][cc] < 0) {
					drilled[rr][cc]++;
					if (drilled[rr][cc] == T) {
						drilled[rr][cc] = 0;
						maze[rr][cc] = 0;
					}
				} else {
					curR = rr;
					curC = cc;
				}
				if (maze[curR][curC] == 1) {
					L++;
					maze[curR][curC] = 0;
				}
				if (m[curR].charAt(curC) != '^') {
					moved = true;
				}
				if ((curC == 0 || curR == 0 || curC == W - 1 || curR == H - 1) && m[curR].charAt(curC) != '#' && moved) {
					//					System.err.println("You collected " + L + " coins and escaped successfully!  Your score is " + L / exp + " ("
					//							+ L + "/" + exp + ")");
					result.score = L / exp;
					break;
				}
			}
			result.obtained = lt.obj.coinsCount;
			result.total = lt.obj.coinR.length;
			if (CollapsingMaze.DEBUG) lt.obj.timer.print();
		} catch (Exception e) {
			e.printStackTrace();
			result.status = "exception";
		}
		return result;
	}

}
