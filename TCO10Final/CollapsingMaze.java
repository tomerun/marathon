import java.util.ArrayList;
import java.util.Arrays;

public class CollapsingMaze {

	static final boolean DEBUG = false;
	static final boolean MEASURE_TIME = false;
	static final long TIMELIMIT = 6000;
	static final int INF = 1 << 20;
	static final int[] DR = { -1, 0, 1, 0 };
	static final int[] DC = { 0, 1, 0, -1 };
	static final int OPEN = 0;
	static final int BLOCK = 1;
	static final int COIN = 2;
	long elapsed = 0;
	Timer timer = new Timer();
	int W, H, T;
	int[][] m;
	int[][] weight;
	int[][] lenC, prevC, lenG, prevG;
	int[] goalR, goalC;
	int[] coinR, coinC;
	int[][] reservedIdx;
	int curReserveI;
	boolean[][] ignore;
	double[][] bonus;
	int cr, cc;
	Path path;
	boolean urgent = false;
	int turn = 0;
	int collapseCount, collapseBlocks, openBlocks;
	int coinsCount, coinsLeft;

	int init(String[] maze, int T) {
		long t = System.currentTimeMillis();
		timer.start(0);
		H = maze.length;
		W = maze[0].length();
		this.T = T;
		m = new int[H][W];
		weight = new int[H][W];
		int[] tgR = new int[6];
		int[] tgC = new int[6];
		ArrayList<Integer> tmpCoin = new ArrayList<Integer>();
		int goalCount = 0;
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				weight[i][j] = 1;
				char c = maze[i].charAt(j);
				if (c == '#') {
					m[i][j] = BLOCK;
					weight[i][j] = T + 1;
				} else {
					if (j == 0 || j == W - 1) {
						tgR[goalCount] = i;
						tgC[goalCount] = j;
						++goalCount;
					}
					if (c == '^') {
						m[i][j] = OPEN;
						cr = i;
					} else if (c == '.' || j == 0 || j == W - 1) {
						m[i][j] = OPEN;
					} else if (c == '*') {
						m[i][j] = COIN;
						tmpCoin.add((i << 10) + j);
					}
					++openBlocks;
				}
			}
		}
		goalR = new int[goalCount];
		goalC = new int[goalCount];
		for (int i = 0; i < goalCount; ++i) {
			goalR[i] = tgR[i];
			goalC[i] = tgC[i];
		}
		coinR = new int[tmpCoin.size()];
		coinC = new int[tmpCoin.size()];
		for (int i = 0; i < tmpCoin.size(); ++i) {
			coinR[i] = tmpCoin.get(i) >> 10;
			coinC[i] = tmpCoin.get(i) & 0x3FF;
		}
		coinsLeft = coinR.length;
		lenC = new int[H][W];
		prevC = new int[H][W];
		lenG = new int[H][W];
		prevG = new int[H][W];
		reservedIdx = new int[H][W];
		bonus = new double[H][W];
		for (int i = 0; i < coinR.length; ++i) {
			for (int j = 0; j < i; ++j) {
				int d = Math.abs(coinR[i] - coinR[j]) + Math.abs(coinC[i] - coinC[j]);
				if (d >= 50) continue;
				bonus[coinR[i]][coinC[i]] += 0.0001 * (50 - d);
				bonus[coinR[j]][coinC[j]] += 0.0001 * (50 - d);
			}
		}
		setIgnore();
		timer.stop(0);
		elapsed += System.currentTimeMillis() - t;
		return 0;
	}

	int move(int[] colR, int[] colC, int curR, int curC) {
		long t = System.currentTimeMillis();
		boolean needReplan = false;
		boolean needUpdateGoal = false;
		if (!urgent && colR.length > 0 && (collapseCount & 0x1F) == 0) {
			needUpdateGoal = true;
		}
		++turn;
		if (colR.length > 0) {
			++collapseCount;
			collapseBlocks += colR.length;
			openBlocks -= colR.length;
		}
		if (curR != cr || curC != cc) {
			if (m[curR][curC] == BLOCK) {
				++openBlocks;
			} else if (m[curR][curC] == COIN) {
				++coinsCount;
				--coinsLeft;
				for (int i = 0; i < coinR.length; ++i) {
					int d = Math.abs(coinR[i] - curR) + Math.abs(coinC[i] - curC);
					if (d < 50 && d != 0) {
						bonus[coinR[i]][coinC[i]] -= 0.0001 * (50 - d);
					}
				}
			}
			if (m[curR][curC] != OPEN) {
				m[curR][curC] = OPEN;
				weight[cr][cc] = 1;
			}
			cr = curR;
			cc = curC;
			path.step();
		}
		for (int i = 0; i < colR.length; ++i) {
			int cr = colR[i];
			int cc = colC[i];
			if (m[cr][cc] == COIN) {
				--coinsLeft;
			}
			m[cr][cc] = BLOCK;
			weight[cr][cc] = T + 1;
			if (reservedIdx[cr][cc] == curReserveI) {
				needReplan = true;
				if (urgent) needUpdateGoal = true;
			}
		}

		if (turn == 1 || needUpdateGoal) {
			updateGoalDist((H + W) * 5, -1);
			timer.print();
		}
		if (turn == 1 || needReplan || path.complete()) {
			path = plan();
			++curReserveI;
			for (int i = 0; i < path.r.length; ++i) {
				reservedIdx[path.r[i]][path.c[i]] = curReserveI;
			}
		}
		int ret = path.next();

		elapsed += System.currentTimeMillis() - t;
		return ret;
	}

	void updateGoalDist(int limit, int target) {
		timer.start(1);
		int[] init = new int[goalR.length];
		for (int i = 0; i < goalR.length; ++i) {
			init[i] = (goalR[i] << 10) + goalC[i];
		}
		bfs(init, lenG, prevG, limit, coinsLeft, target);
		timer.stop(1);
	}

	Path plan() {
		int goalLen = INF;
		for (int i = 0; i < goalR.length; ++i) {
			int d = Math.abs(cr - goalR[i]) + Math.abs(cc - goalC[i]);
			if (d < goalLen) {
				goalLen = d;
			}
		}
		goalLen /= 2;
		goalLen += lenG[cr][cc];
		int aveSurvive = INF;
		if (collapseCount > 4) {
			double surviveRatio = 0.4 / (10.0 * ((coinsCount + 0.01) / coinR.length));
			double p = 1. * collapseCount / turn;
			double aveBlocks = 1. * collapseBlocks / collapseCount;
			aveSurvive = (int) (openBlocks / (p * aveBlocks));
			aveSurvive *= surviveRatio;
			if (urgent) {
				aveSurvive /= 2;
			}
			debug(p + " " + aveBlocks + " " + aveSurvive + "  " + goalLen);
		}
		if (goalLen < aveSurvive && elapsed < TIMELIMIT * 0.5 && coinsLeft > 0) {
			timer.start(2);
			bfs(new int[] { (cr << 10) + cc }, lenC, prevC, (H + W) * 1, 15, -1);
			timer.stop(2);

			int candR = 0;
			int candC = 0;
			int best = INF;
			for (int i = 0; i < coinR.length; ++i) {
				int r = coinR[i];
				int c = coinC[i];
				if (m[r][c] != COIN) continue;
				int dist = lenC[r][c];
				if (urgent) {
					dist += lenG[r][c];
				}
				dist /= (1 + bonus[r][c]);
				if (dist < best) {
					best = dist;
					candR = r;
					candC = c;
				}
			}

			if (best < aveSurvive && prevC[candR][candC] != -1) {
				ArrayList<Integer> guide = new ArrayList<>();
				while (true) {
					guide.add((candR << 10) + candC);
					if (candR == cr && candC == cc) break;
					int dr = DR[prevC[candR][candC]];
					int dc = DC[prevC[candR][candC]];
					candR += dr;
					candC += dc;
				}
				return new Path(guide, true);
			}
		}

		timer.start(3);
		debug("urgent mode");
		urgent = true;
		if (goalLen >= INF) {
			System.err.println("fail");
			updateGoalDist((H + W) * 500, (cr << 10) + cc);
			goalLen = lenG[cr][cc];
		}
		if (goalLen >= INF) {
			System.err.println("fail!!!");
			int bestI = 0;
			int minLen = INF;
			for (int i = 0; i < goalR.length; ++i) {
				int d = Math.abs(goalR[i] - cr) + Math.abs(goalC[i] - cc);
				if (d < minLen) {
					minLen = d;
					bestI = i;
				}
			}
			timer.stop(3);
			return new Path(cr, cc, goalR[bestI], goalC[bestI]);
		}
		ArrayList<Integer> guide = new ArrayList<>();
		int candR = cr;
		int candC = cc;
		while (true) {
			guide.add((candR << 10) + candC);
			if (prevG[candR][candC] == -1) break;
			int dr = DR[prevG[candR][candC]];
			int dc = DC[prevG[candR][candC]];
			candR += dr;
			candC += dc;
		}
		timer.stop(3);
		return new Path(guide, false);
	}

	int[][] list = new int[0][];
	int[] listC = new int[0];

	void bfs(int[] init, int[][] len, int[][] prev, int limit, int coins, int target) {
		if (list.length <= limit) {
			int[][] prevList = list;
			list = new int[limit + 1][];
			for (int i = 0; i < prevList.length; ++i) {
				list[i] = prevList[i];
			}
			listC = new int[limit + 1];
		} else {
			Arrays.fill(listC, 0);
		}
		for (int i = 0; i < H; ++i) {
			Arrays.fill(len[i], INF);
			Arrays.fill(prev[i], -1);
		}
		for (int i = 0; i < init.length; ++i) {
			int tr = init[i] >> 10;
			int tc = init[i] & 0x3FF;
			len[tr][tc] = 0;
		}
		if (list[0] == null || list[0].length < init.length) {
			list[0] = init.clone();
		} else {
			for (int i = 0; i < init.length; ++i) {
				list[0][i] = init[i];
			}
		}
		listC[0] = init.length;
		int upper = 1;
		for (int i = 0; i < limit; ++i) {
			if (upper <= i) break;
			int[] cur = list[i];
			if (cur == null) continue;
			for (int j = 0; j < listC[i]; ++j) {
				if (cur[j] == target) target = -1;
				int row = cur[j] >> 10;
				int col = cur[j] & 0x3FF;
				if (len[row][col] < i) continue;
				if (m[row][col] == COIN) {
					--coins;
				}
				if (target == -1 && coins == 0) {
					return;
				}
				for (int k = 0; k < 4; ++k) {
					int nr = row + DR[k];
					int nc = col + DC[k];
					if (nr < 0 || H <= nr || nc < 0 || W <= nc) continue;
					int nw = i + weight[nr][nc];
					if (nw < len[nr][nc] && !ignore[row][col]) {
						len[nr][nc] = nw;
						prev[nr][nc] = (k + 2) & 3;
						if (nw < limit) {
							upper = Math.max(upper, nw + 1);
							if (list[nw] == null) {
								list[nw] = new int[16];
							} else if (listC[nw] == list[nw].length) {
								int[] tmp = list[nw];
								list[nw] = new int[listC[nw] * 2];
								for (int l = 0; l < listC[nw]; ++l) {
									list[nw][l] = tmp[l];
								}
							}
							list[nw][listC[nw]] = (nr << 10) + nc;
							++listC[nw];
						}
					}
				}
			}
		}
	}

	void setIgnore() {
		ignore = new boolean[H][W];
		for (int i = 0; i < H; ++i) {
			Arrays.fill(ignore[i], true);
		}
		int SIZE = 5;
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (m[i][j] == BLOCK) continue;
				for (int k = Math.max(0, i - SIZE); k <= Math.min(H - 1, i + SIZE); ++k) {
					int width = SIZE - Math.abs(i - k);
					for (int l = Math.max(0, j - width); l <= Math.min(W - 1, j + width); ++l) {
						ignore[k][l] = false;
					}
				}
			}
		}
	}

	static class Path {
		int[] r, c;
		int pos;

		Path(ArrayList<Integer> points, boolean rev) {
			this.r = new int[points.size()];
			this.c = new int[points.size()];
			if (rev) {
				for (int i = 0; i < points.size(); ++i) {
					this.r[points.size() - 1 - i] = points.get(i) >> 10;
					this.c[points.size() - 1 - i] = points.get(i) & 0x3FF;
				}
			} else {
				for (int i = 0; i < points.size(); ++i) {
					this.r[i] = points.get(i) >> 10;
					this.c[i] = points.get(i) & 0x3FF;
				}
			}
			this.pos = 0;
		}

		Path(int r1, int c1, int r2, int c2) {
			int d = Math.abs(r2 - r1) + Math.abs(c2 - c1) + 1;
			this.r = new int[d];
			this.c = new int[d];
			int diff = r2 > r1 ? 1 : -1;
			int pos = 0;
			for (int i = 0; i < Math.abs(r2 - r1); ++i, ++pos) {
				r[pos] = r1 + i * diff;
				c[pos] = c1;
			}
			diff = c2 > c1 ? 1 : -1;
			for (int i = 0; i < Math.abs(c2 - c1); ++i, ++pos) {
				r[pos] = r2;
				c[pos] = c1 + i * diff;
			}
			r[pos] = r2;
			c[pos] = c2;
		}

		int next() {
			if (r[pos] - 1 == r[pos + 1]) return 0;
			if (r[pos] + 1 == r[pos + 1]) return 2;
			if (c[pos] + 1 == c[pos + 1]) return 1;
			return 3;
		}

		void step() {
			pos++;
		}

		boolean complete() {
			return pos == r.length - 1;
		}
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}

	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

}
