import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Visualizer extends JFrame {
	Object moveLock = new Object();
	Tester cm;
	static MazeVis mv;
	int delay;
	int[][] visited;
	int curR, curC;
	int zoom = 10;

	public Visualizer(Tester cm, int delay) {
		this.cm = cm;
		this.delay = delay;
	}

	public void doupdate() {
		if (visited == null) {
			visited = new int[cm.maze.length][cm.maze[0].length];
		}
		if (curR != cm.curR || curC != cm.curC) {
			visited[cm.curR][cm.curC]++;
		}
		int width = vis.getWidth();
		int height = vis.getHeight();
		boolean changed = false;
		if (width < 200) {
			changed = true;
			width = 1000;
		}
		if (height < 100) {
			changed = true;
			height = 700;
		}
		if (width > visited[0].length * zoom + 20) {
			changed = true;
			width = visited[0].length * zoom + 20;
		}
		if (height > visited.length * zoom + 100) {
			changed = true;
			height = visited.length * zoom + 100;
		}
		if (changed) {
			vis.setSize(width, height);
		}
		curR = cm.curR;
		curC = cm.curC;
		validate();
		repaint();
		try {
			if (delay > 0) {
				Thread.sleep(delay);
			} else if (delay == -1) {
				synchronized (moveLock) {
					moveLock.wait();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class MazeVis extends JPanel {
		int left, right, top, bottom;

		public void paint(Graphics g) {
			if (cm.maze == null) {
				return;
			}
			while (!cm.ready) {
				try {
					Thread.sleep(10);
				} catch (Exception e) {}
			}
			int EDGE = 100 / zoom;
			Graphics2D g2 = (Graphics2D) g;
			Font f = new Font(g2.getFont().getName(), Font.PLAIN, 20);
			g2.setFont(f);
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			FontMetrics fm = g2.getFontMetrics();
			int th = fm.getAscent();
			int width = getWidth();
			int height = getHeight() - th - 10;
			if (cm.curC - left < EDGE && cm.curC > EDGE || right - 1 - cm.curC < EDGE && cm.curC < cm.W - EDGE
					|| cm.curR - top < EDGE && cm.curR > EDGE || bottom - 1 - cm.curR < EDGE && cm.curR < cm.H - EDGE
					|| right == 0) {
				int rlsize = width / zoom;
				int tbsize = height / zoom;
				left = (cm.curC - rlsize / 2);
				top = (cm.curR - tbsize / 2);
				if (left + rlsize > cm.W) {
					left = cm.W - rlsize;
				}
				if (top + tbsize > cm.H) {
					top = cm.H - tbsize;
				}
				if (left < 0) {
					left = 0;
				}
				if (top < 0) {
					top = 0;
				}
				right = left + rlsize;
				bottom = top + tbsize;
				if (right > cm.W) {
					right = cm.W;
				}
				if (bottom > cm.H) {
					bottom = cm.H;
				}
			}
			int S = zoom;
			Color coin = new Color(255, 186, 0);
			for (int i = top; i < bottom; i++) {
				for (int j = left; j < right; j++) {
					if (cm.maze[i][j] == -1) {
						int prog = cm.drilled[i][j] * 256 / cm.T;
						if (prog >= 256) prog = 255;
						g.setColor(new Color(prog, prog, prog));
					} else if (cm.maze[i][j] == -2) {
						int prog = cm.drilled[i][j] * 256 / cm.T;
						if (prog >= 256) prog = 255;
						g.setColor(new Color(255, prog, prog));
					} else if (i == cm.curR && j == cm.curC) {
						g.setColor(Color.blue);
					} else if (visited != null && visited[i][j] == 1) {
						g.setColor(Color.green);
					} else if (visited != null && visited[i][j] > 1) {
						g.setColor(Color.green);
					} else if (cm.maze[i][j] == 1) {
						g.setColor(coin);
						//} else if (cm.maze[i][j] == 2){
						//g.setColor(Color.yellow);
					} else {
						g.setColor(Color.white);
					}
					g.fillRect((j - left) * S, 10 + th + (i - top) * S, S, S);
				}
			}
			g.setColor(Color.gray);
			g.fillRect(0, 0, 300, th + 10);
			g.setColor(Color.black);
			g.drawString("Coins so far = " + cm.L + ", time = " + cm.time, 0, th + 5);
		}
	}

	static Visualizer vis;
	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		boolean vison = false;
		long seed = 1;
		long begin = -1;
		long end = -1;
		int delay = 20;
		int zoom = 3;
		String output = null;
		int H = 0, W = 0;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-begin")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-end")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-delay")) {
				delay = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-vis")) {
				vison = true;
			} else if (args[i].equals("-output")) {
				output = args[++i];
			} else if (args[i].equals("-zoom")) {
				zoom = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-H")) {
				H = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-W")) {
				W = Integer.parseInt(args[++i]);
			}
		}
		if (H < 10 && H != 0) {
			H = 10;
		}
		if (H > 1000) {
			H = 1000;
		}
		if (W < 10 && W != 0) {
			W = 10;
		}
		if (W > 1000) {
			W = 1000;
		}
		Tester test = new Tester();
		test.W = W;
		test.H = H;
		if (output != null) {
			test.init(seed);
			BufferedImage bi = new BufferedImage(test.W * zoom, test.H * zoom, BufferedImage.TYPE_INT_RGB);
			for (int i = 0; i < test.H; i++) {
				for (int j = 0; j < test.W; j++) {
					for (int x = 0; x < zoom; x++) {
						for (int y = 0; y < zoom; y++) {
							int rgb;
							if (i == test.curR && j == test.curC) {
								rgb = 0x0000ff;
							} else if (test.maze[i][j] == 0) {
								rgb = 0xffffff;
							} else if (test.maze[i][j] == 1) {
								rgb = 0xffba00;
							} else {
								rgb = 0;
							}
							bi.setRGB(j * zoom + x, i * zoom + y, rgb);
						}
					}
				}
			}
			try {
				ImageIO.write(bi, "png", new File(output));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
		if (vison) {
			vis = new Visualizer(test, delay);
		}

		if (!vison && begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			final LongTest lt = new LongTest(seed, vis);
			if (vison) {
				vis.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				vis.addKeyListener(new KeyAdapter() {
					public void keyTyped(KeyEvent ke) {
						if (vis.delay == -1 && ke.getKeyChar() == ' ') {
							synchronized (vis.moveLock) {
								vis.moveLock.notify();
							}
						}
					}
				});
				mv = vis.new MazeVis();
				vis.zoom = zoom;
				vis.getContentPane().add(mv);
				vis.setVisible(true);
			}
			Result res = test.runTest(lt);
			System.err.println(res);
		}
		if (vison) {
			vis.doupdate();
		}
	}

}

class TestThread extends Thread {
	int begin, end;
	ArrayList<Long> seeds;
	Result[] results;

	TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
		this.begin = begin;
		this.end = end;
		this.seeds = seeds;
		this.results = results;
	}

	public void run() {
		for (int i = begin; i <= end; ++i) {
			Tester f = new Tester();
			try {
				final LongTest lt = new LongTest(seeds.get(i), null);
				Result res = f.runTest(lt);
				results[i] = res;
			} catch (Exception e) {
				e.printStackTrace();
				results[i] = new Result();
				results[i].seed = seeds.get(i);
			}
		}
	}
}

class Result {
	long seed;
	int W, H, T, D;
	double p, q;
	double score;
	int obtained, total;
	long elapsed;
	String status = "survived";

	public String toString() {
		String ret = String.format("seed:%4d\n", seed);
		ret += String.format("W:%4d H:%4d T:%3d D:%2d\n", W, H, T, D);
		ret += String.format("p:%6.4f q:%6.4f\n", p, q);
		ret += "status: " + status + "\n";
		ret += String.format("obtain:%d total:%d\n", this.obtained, this.total);
		ret += String.format("elapsed:%5.3f\n", this.elapsed / 1000.0);
		ret += String.format("score:  %5.5f", this.score);
		return ret;
	}
}
