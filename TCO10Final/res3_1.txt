seed:   1
W: 100 H: 100 T: 10 D:15
p:0.0000 q:0.1000
status: survived
obtain:255 total:255
elapsed:0.186
score:  0.11263

seed:   2
W: 362 H: 210 T: 24 D:15
p:0.0553 q:0.0878
status: smushed
obtain:99 total:1294
elapsed:0.201
score:  0.00000

seed:   3
W: 681 H: 323 T: 83 D:18
p:0.0549 q:0.0832
status: smushed
obtain:524 total:3740
elapsed:1.735
score:  0.00000

seed:   4
W: 651 H: 986 T: 79 D: 2
p:0.0586 q:0.0200
status: survived
obtain:159 total:328
elapsed:2.967
score:  0.06496

seed:   5
W: 203 H: 677 T: 36 D:17
p:0.0797 q:0.0949
status: smushed
obtain:63 total:835
elapsed:0.092
score:  0.00000

seed:   6
W: 159 H: 812 T: 65 D: 5
p:0.0334 q:0.0452
status: survived
obtain:64 total:198
elapsed:0.066
score:  0.04597

seed:   7
W: 992 H: 470 T: 46 D: 6
p:0.0555 q:0.0680
status: survived
obtain:402 total:6114
elapsed:4.093
score:  0.01822

seed:   8
W: 290 H: 141 T: 69 D: 8
p:0.0208 q:0.0176
status: survived
obtain:32 total:91
elapsed:0.067
score:  0.04381

seed:   9
W: 352 H: 366 T: 83 D:12
p:0.0889 q:0.0243
status: smushed
obtain:35 total:242
elapsed:0.094
score:  0.00000

seed:  10
W: 612 H: 713 T: 84 D: 1
p:0.0853 q:0.0839
status: survived
obtain:281 total:4619
elapsed:4.822
score:  0.00850

seed:  11
W: 638 H: 839 T: 66 D: 6
p:0.0219 q:0.0762
status: survived
obtain:1363 total:3107
elapsed:4.062
score:  0.04451

seed:  12
W: 725 H: 641 T: 80 D:11
p:0.0660 q:0.0889
status: survived
obtain:328 total:6993
elapsed:6.047
score:  0.02677

seed:  13
W: 463 H: 500 T: 43 D: 4
p:0.0704 q:0.0733
status: survived
obtain:767 total:2683
elapsed:3.332
score:  0.07230

seed:  14
W: 132 H: 914 T: 48 D:10
p:0.0730 q:0.0902
status: survived
obtain:122 total:836
elapsed:0.509
score:  0.07692

seed:  15
W: 809 H: 452 T: 15 D:16
p:0.0519 q:0.0020
status: smushed
obtain:37 total:92
elapsed:1.040
score:  0.00000

seed:  16
W: 747 H: 633 T: 45 D:17
p:0.0068 q:0.0384
status: survived
obtain:1237 total:2513
elapsed:3.045
score:  0.04049

seed:  17
W: 453 H: 758 T: 91 D: 3
p:0.0908 q:0.0630
status: smushed
obtain:138 total:849
elapsed:0.525
score:  0.00000

seed:  18
W: 394 H: 981 T: 15 D:10
p:0.0151 q:0.0031
status: survived
obtain:110 total:131
elapsed:2.248
score:  0.09048

seed:  19
W: 424 H: 739 T: 90 D:10
p:0.0049 q:0.0845
status: survived
obtain:587 total:778
elapsed:0.244
score:  0.02490

seed:  20
W: 586 H: 993 T: 47 D: 7
p:0.0449 q:0.0258
status: survived
obtain:221 total:1520
elapsed:4.260
score:  0.03464

seed:  21
W: 111 H: 217 T: 82 D:14
p:0.0937 q:0.0210
status: survived
obtain:8 total:49
elapsed:0.048
score:  0.13885

seed:  22
W: 652 H: 257 T: 51 D:19
p:0.0396 q:0.0795
status: survived
obtain:337 total:1724
elapsed:0.518
score:  0.09796

seed:  23
W: 386 H: 204 T: 89 D:13
p:0.0785 q:0.0134
status: survived
obtain:28 total:183
elapsed:0.200
score:  0.10360

seed:  24
W: 747 H: 304 T: 98 D: 4
p:0.0826 q:0.0294
status: survived
obtain:70 total:400
elapsed:1.136
score:  0.04697

seed:  25
W: 599 H: 206 T: 66 D:12
p:0.0898 q:0.0407
status: survived
obtain:302 total:1484
elapsed:2.834
score:  0.18423

seed:  26
W: 595 H: 409 T: 17 D:17
p:0.0892 q:0.0382
status: smushed
obtain:224 total:1935
elapsed:1.615
score:  0.00000

seed:  27
W: 916 H: 693 T: 90 D:14
p:0.0211 q:0.0980
status: survived
obtain:955 total:9930
elapsed:4.342
score:  0.02107

seed:  28
W: 418 H: 792 T: 64 D: 6
p:0.0942 q:0.0163
status: survived
obtain:253 total:1062
elapsed:3.875
score:  0.11614

seed:  29
W: 526 H: 296 T: 98 D: 6
p:0.0483 q:0.0842
status: survived
obtain:139 total:727
elapsed:0.511
score:  0.04291

seed:  30
W: 989 H: 781 T: 25 D: 5
p:0.0453 q:0.0475
status: survived
obtain:503 total:3934
elapsed:3.597
score:  0.02289

seed:  31
W: 889 H: 805 T: 57 D:19
p:0.0361 q:0.0274
status: time limit exceeded
obtain:197 total:1352
elapsed:6.242
score:  0.00000

seed:  32
W: 119 H: 470 T: 20 D:15
p:0.0428 q:0.0328
status: survived
obtain:13 total:61
elapsed:0.013
score:  0.10555

seed:  33
W: 780 H: 381 T: 49 D: 0
p:0.0832 q:0.0263
status: survived
obtain:0 total:0
elapsed:0.000
score:  0.00000

seed:  34
W: 253 H: 397 T: 81 D: 4
p:0.0546 q:0.0905
status: survived
obtain:573 total:1428
elapsed:2.195
score:  0.07872

seed:  35
W: 971 H: 412 T: 69 D: 4
p:0.0496 q:0.0128
status: survived
obtain:89 total:460
elapsed:1.206
score:  0.03295

seed:  36
W: 915 H: 303 T: 64 D:17
p:0.0408 q:0.0372
status: survived
obtain:202 total:927
elapsed:1.496
score:  0.10044

seed:  37
W: 545 H: 385 T: 69 D: 3
p:0.0860 q:0.0268
status: survived
obtain:262 total:1268
elapsed:4.465
score:  0.05272

seed:  38
W: 944 H: 499 T: 95 D: 7
p:0.0221 q:0.0693
status: survived
obtain:437 total:1263
elapsed:1.587
score:  0.03789

seed:  39
W: 795 H: 900 T: 84 D:10
p:0.0957 q:0.0756
status: smushed
obtain:208 total:2814
elapsed:1.911
score:  0.00000

seed:  40
W: 661 H: 304 T: 68 D: 7
p:0.0982 q:0.0318
status: survived
obtain:252 total:1764
elapsed:3.093
score:  0.08390

seed:  41
W: 905 H: 368 T: 70 D:19
p:0.0180 q:0.0588
status: survived
obtain:1002 total:4747
elapsed:3.471
score:  0.05518

seed:  42
W: 793 H: 920 T: 33 D: 0
p:0.0186 q:0.0793
status: survived
obtain:1173 total:7662
elapsed:3.221
score:  0.00286

seed:  43
W: 635 H: 439 T: 73 D:13
p:0.0268 q:0.0262
status: survived
obtain:189 total:530
elapsed:0.586
score:  0.08391

seed:  44
W: 100 H: 740 T: 46 D:10
p:0.0611 q:0.0278
status: smushed
obtain:3 total:15
elapsed:0.007
score:  0.00000

seed:  45
W: 102 H: 537 T: 70 D:17
p:0.0278 q:0.0285
status: survived
obtain:32 total:104
elapsed:0.049
score:  0.11714

seed:  46
W: 855 H: 998 T: 15 D:17
p:0.0341 q:0.0205
status: smushed
obtain:10 total:1304
elapsed:0.097
score:  0.00000

seed:  47
W: 137 H: 248 T: 76 D:12
p:0.0472 q:0.0526
status: smushed
obtain:19 total:113
elapsed:0.034
score:  0.00000

seed:  48
W: 447 H: 792 T: 63 D: 6
p:0.0430 q:0.0313
status: smushed
obtain:239 total:736
elapsed:2.689
score:  0.00000

seed:  49
W: 182 H: 170 T: 24 D: 9
p:0.0182 q:0.0278
status: smushed
obtain:28 total:156
elapsed:0.010
score:  0.00000

seed:  50
W: 811 H: 611 T: 85 D: 6
p:0.0841 q:0.0535
status: survived
obtain:384 total:3333
elapsed:3.770
score:  0.04450

seed:  51
W: 531 H: 694 T: 70 D: 3
p:0.0498 q:0.0613
status: survived
obtain:285 total:2075
elapsed:0.884
score:  0.01845

seed:  52
W: 319 H: 658 T: 85 D:13
p:0.0930 q:0.0113
status: smushed
obtain:9 total:139
elapsed:0.140
score:  0.00000

seed:  53
W: 441 H: 352 T: 79 D: 2
p:0.0495 q:0.0648
status: survived
obtain:167 total:687
elapsed:0.379
score:  0.02532

seed:  54
W: 700 H: 160 T: 41 D: 6
p:0.0566 q:0.0996
status: smushed
obtain:384 total:2144
elapsed:0.461
score:  0.00000

seed:  55
W: 116 H: 649 T: 24 D:16
p:0.0596 q:0.0553
status: survived
obtain:41 total:287
elapsed:0.154
score:  0.09205

seed:  56
W: 957 H: 934 T: 42 D:19
p:0.0174 q:0.0984
status: survived
obtain:1053 total:4236
elapsed:5.710
score:  0.05292

seed:  57
W: 236 H: 860 T: 87 D:14
p:0.0763 q:0.0861
status: smushed
obtain:84 total:977
elapsed:0.475
score:  0.00000

seed:  58
W: 832 H: 247 T: 19 D: 1
p:0.0505 q:0.0533
status: survived
obtain:977 total:1069
elapsed:3.549
score:  0.06857

seed:  59
W: 972 H: 816 T: 32 D: 0
p:0.0739 q:0.0459
status: survived
obtain:139 total:4205
elapsed:3.547
score:  0.00244

seed:  60
W: 294 H: 275 T: 13 D:15
p:0.0021 q:0.0716
status: survived
obtain:894 total:913
elapsed:0.280
score:  0.02302

seed:  61
W: 255 H: 381 T: 14 D:10
p:0.0876 q:0.0401
status: survived
obtain:26 total:140
elapsed:0.078
score:  0.11494

seed:  62
W: 214 H: 469 T: 22 D: 6
p:0.0202 q:0.0755
status: survived
obtain:1499 total:1662
elapsed:1.197
score:  0.09979

seed:  63
W: 772 H: 134 T: 71 D: 3
p:0.0915 q:0.0089
status: survived
obtain:101 total:204
elapsed:1.655
score:  0.12526

seed:  64
W: 409 H: 348 T: 75 D: 3
p:0.0470 q:0.0432
status: survived
obtain:57 total:177
elapsed:0.146
score:  0.04184

seed:  65
W: 922 H: 760 T: 51 D: 1
p:0.0599 q:0.0414
status: survived
obtain:132 total:4472
elapsed:5.371
score:  0.00284

seed:  66
W: 687 H: 308 T: 89 D:17
p:0.0328 q:0.0971
status: survived
obtain:1389 total:4791
elapsed:3.567
score:  0.12163

seed:  67
W: 517 H: 303 T: 98 D: 4
p:0.0742 q:0.0908
status: smushed
obtain:558 total:2428
elapsed:0.735
score:  0.00000

seed:  68
W: 732 H: 582 T: 46 D: 4
p:0.0949 q:0.0324
status: survived
obtain:212 total:1526
elapsed:4.923
score:  0.04546

seed:  69
W: 944 H: 230 T: 41 D:11
p:0.0564 q:0.0803
status: survived
obtain:365 total:1612
elapsed:1.582
score:  0.09273

seed:  70
W: 158 H: 710 T: 70 D: 0
p:0.0433 q:0.0111
status: survived
obtain:41 total:44
elapsed:0.195
score:  0.04370

seed:  71
W: 891 H: 181 T: 98 D: 8
p:0.0317 q:0.0688
status: survived
obtain:333 total:1202
elapsed:0.471
score:  0.04790

seed:  72
W: 500 H: 244 T: 71 D:10
p:0.0590 q:0.0011
status: survived
obtain:16 total:39
elapsed:0.612
score:  0.26298

seed:  73
W: 171 H: 830 T: 49 D: 3
p:0.0395 q:0.0158
status: survived
obtain:18 total:56
elapsed:0.157
score:  0.03576

seed:  74
W: 883 H: 951 T: 96 D: 7
p:0.0055 q:0.0821
status: survived
obtain:1380 total:7497
elapsed:3.323
score:  0.00561

seed:  75
W: 932 H: 545 T: 34 D: 0
p:0.0166 q:0.0682
status: survived
obtain:1259 total:5630
elapsed:3.348
score:  0.00375

seed:  76
W: 840 H: 194 T: 59 D: 2
p:0.0692 q:0.0008
status: survived
obtain:28 total:28
elapsed:2.803
score:  0.12815

seed:  77
W: 974 H: 780 T: 51 D: 0
p:0.0651 q:0.0257
status: survived
obtain:98 total:2438
elapsed:4.746
score:  0.00258

seed:  78
W: 793 H: 370 T: 91 D:17
p:0.0263 q:0.0903
status: survived
obtain:1300 total:4556
elapsed:4.066
score:  0.09709

seed:  79
W: 284 H: 847 T: 39 D:15
p:0.0958 q:0.0481
status: smushed
obtain:10 total:130
elapsed:0.305
score:  0.00000

seed:  80
W: 845 H: 339 T: 24 D: 0
p:0.0906 q:0.0953
status: survived
obtain:716 total:4822
elapsed:3.332
score:  0.01362

seed:  81
W: 188 H: 915 T: 87 D:18
p:0.0853 q:0.0488
status: survived
obtain:20 total:412
elapsed:0.504
score:  0.04778

seed:  82
W: 407 H: 320 T: 57 D: 5
p:0.0962 q:0.0059
status: survived
obtain:61 total:143
elapsed:2.360
score:  0.18622

seed:  83
W: 100 H: 847 T: 32 D:16
p:0.0613 q:0.0481
status: smushed
obtain:19 total:155
elapsed:0.144
score:  0.00000

seed:  84
W: 232 H: 124 T: 92 D: 8
p:0.0481 q:0.0040
status: smushed
obtain:2 total:24
elapsed:0.025
score:  0.00000

seed:  85
W: 419 H: 232 T: 93 D:11
p:0.0221 q:0.0543
status: survived
obtain:434 total:1155
elapsed:0.975
score:  0.06819

seed:  86
W: 295 H: 766 T: 59 D:15
p:0.0850 q:0.0628
status: smushed
obtain:109 total:955
elapsed:0.418
score:  0.00000

seed:  87
W: 825 H: 160 T: 11 D:17
p:0.0316 q:0.0937
status: survived
obtain:170 total:604
elapsed:0.257
score:  0.09769

seed:  88
W: 717 H: 225 T: 77 D: 6
p:0.0559 q:0.0990
status: survived
obtain:696 total:2628
elapsed:2.239
score:  0.07248

seed:  89
W: 654 H: 334 T: 68 D:16
p:0.0072 q:0.0695
status: survived
obtain:1176 total:1388
elapsed:1.084
score:  0.06200

seed:  90
W: 162 H: 483 T: 39 D: 1
p:0.0519 q:0.0462
status: smushed
obtain:100 total:188
elapsed:0.068
score:  0.00000

seed:  91
W: 384 H: 275 T: 24 D:14
p:0.0791 q:0.0633
status: smushed
obtain:292 total:1462
elapsed:0.740
score:  0.00000

seed:  92
W: 200 H: 845 T: 38 D: 3
p:0.0964 q:0.0785
status: smushed
obtain:249 total:881
elapsed:1.474
score:  0.00000

seed:  93
W: 689 H: 208 T: 46 D: 5
p:0.0396 q:0.0980
status: survived
obtain:809 total:1996
elapsed:0.797
score:  0.06179

seed:  94
W: 884 H: 495 T: 56 D: 2
p:0.0739 q:0.0892
status: survived
obtain:662 total:3425
elapsed:4.484
score:  0.03141

seed:  95
W: 471 H: 406 T: 77 D:18
p:0.0097 q:0.0229
status: survived
obtain:528 total:793
elapsed:1.177
score:  0.09034

seed:  96
W: 159 H: 617 T: 79 D:15
p:0.0377 q:0.0922
status: survived
obtain:84 total:442
elapsed:0.066
score:  0.06597

seed:  97
W: 978 H: 557 T: 28 D:14
p:0.0508 q:0.0778
status: survived
obtain:0 total:7577
elapsed:0.349
score:  0.00000

seed:  98
W: 669 H: 470 T: 88 D:13
p:0.0051 q:0.0503
status: survived
obtain:1005 total:2062
elapsed:1.215
score:  0.02258

seed:  99
W: 462 H: 953 T: 62 D:19
p:0.0990 q:0.0056
status: survived
obtain:37 total:328
elapsed:4.122
score:  0.15177

seed: 100
W: 290 H: 672 T: 76 D:13
p:0.0272 q:0.0391
status: smushed
obtain:107 total:507
elapsed:0.098
score:  0.00000

ave:0.04802143677871332
