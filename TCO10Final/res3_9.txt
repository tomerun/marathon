seed:   1
W: 100 H: 100 T: 10 D:15
p:0.0000 q:0.1000
status: survived
obtain:255 total:255
elapsed:0.278
score:  0.11263

seed:   2
W: 362 H: 210 T: 24 D:15
p:0.0553 q:0.0878
status: survived
obtain:365 total:1294
elapsed:0.816
score:  0.16929

seed:   3
W: 681 H: 323 T: 83 D:18
p:0.0549 q:0.0832
status: smushed
obtain:274 total:3740
elapsed:0.893
score:  0.00000

seed:   4
W: 651 H: 986 T: 79 D: 2
p:0.0586 q:0.0200
status: smushed
obtain:85 total:328
elapsed:0.745
score:  0.00000

seed:   5
W: 203 H: 677 T: 36 D:17
p:0.0797 q:0.0949
status: smushed
obtain:52 total:835
elapsed:0.127
score:  0.00000

seed:   6
W: 159 H: 812 T: 65 D: 5
p:0.0334 q:0.0452
status: survived
obtain:100 total:198
elapsed:0.117
score:  0.07183

seed:   7
W: 992 H: 470 T: 46 D: 6
p:0.0555 q:0.0680
status: survived
obtain:318 total:6113
elapsed:3.384
score:  0.01441

seed:   8
W: 290 H: 141 T: 69 D: 8
p:0.0208 q:0.0176
status: smushed
obtain:25 total:91
elapsed:0.025
score:  0.00000

seed:   9
W: 352 H: 366 T: 83 D:12
p:0.0889 q:0.0243
status: survived
obtain:50 total:242
elapsed:0.682
score:  0.15166

seed:  10
W: 612 H: 713 T: 84 D: 1
p:0.0853 q:0.0839
status: survived
obtain:149 total:4619
elapsed:4.963
score:  0.00451

seed:  11
W: 638 H: 839 T: 66 D: 6
p:0.0219 q:0.0762
status: survived
obtain:1040 total:3107
elapsed:4.250
score:  0.03396

seed:  12
W: 725 H: 641 T: 80 D:11
p:0.0660 q:0.0889
status: survived
obtain:249 total:6993
elapsed:4.394
score:  0.02032

seed:  13
W: 463 H: 500 T: 43 D: 4
p:0.0704 q:0.0733
status: survived
obtain:477 total:2682
elapsed:3.371
score:  0.04497

seed:  14
W: 132 H: 914 T: 48 D:10
p:0.0730 q:0.0902
status: survived
obtain:208 total:836
elapsed:0.513
score:  0.13114

seed:  15
W: 809 H: 452 T: 15 D:16
p:0.0519 q:0.0020
status: survived
obtain:59 total:92
elapsed:3.743
score:  0.41744

seed:  16
W: 747 H: 633 T: 45 D:17
p:0.0068 q:0.0384
status: smushed
obtain:77 total:2513
elapsed:0.489
score:  0.00000

seed:  17
W: 453 H: 758 T: 91 D: 3
p:0.0908 q:0.0630
status: smushed
obtain:94 total:849
elapsed:0.514
score:  0.00000

seed:  18
W: 394 H: 981 T: 15 D:10
p:0.0151 q:0.0031
status: survived
obtain:112 total:131
elapsed:2.498
score:  0.09212

seed:  19
W: 424 H: 739 T: 90 D:10
p:0.0049 q:0.0845
status: survived
obtain:717 total:778
elapsed:0.375
score:  0.03041

seed:  20
W: 586 H: 993 T: 47 D: 7
p:0.0449 q:0.0258
status: smushed
obtain:30 total:1520
elapsed:0.337
score:  0.00000

seed:  21
W: 111 H: 217 T: 82 D:14
p:0.0937 q:0.0210
status: survived
obtain:10 total:49
elapsed:0.055
score:  0.17356

seed:  22
W: 652 H: 257 T: 51 D:19
p:0.0396 q:0.0795
status: smushed
obtain:378 total:1724
elapsed:0.600
score:  0.00000

seed:  23
W: 386 H: 204 T: 89 D:13
p:0.0785 q:0.0134
status: survived
obtain:43 total:183
elapsed:0.374
score:  0.15910

seed:  24
W: 747 H: 304 T: 98 D: 4
p:0.0826 q:0.0294
status: survived
obtain:45 total:400
elapsed:0.922
score:  0.03019

seed:  25
W: 599 H: 206 T: 66 D:12
p:0.0898 q:0.0407
status: survived
obtain:337 total:1484
elapsed:4.156
score:  0.20558

seed:  26
W: 595 H: 409 T: 17 D:17
p:0.0892 q:0.0382
status: smushed
obtain:20 total:1935
elapsed:0.197
score:  0.00000

seed:  27
W: 916 H: 693 T: 90 D:14
p:0.0211 q:0.0980
status: survived
obtain:564 total:9930
elapsed:4.104
score:  0.01244

seed:  28
W: 418 H: 792 T: 64 D: 6
p:0.0942 q:0.0163
status: survived
obtain:152 total:1062
elapsed:3.705
score:  0.06978

seed:  29
W: 526 H: 296 T: 98 D: 6
p:0.0483 q:0.0842
status: survived
obtain:104 total:727
elapsed:0.365
score:  0.03211

seed:  30
W: 989 H: 781 T: 25 D: 5
p:0.0453 q:0.0475
status: survived
obtain:269 total:3934
elapsed:4.045
score:  0.01224

seed:  31
W: 889 H: 805 T: 57 D:19
p:0.0361 q:0.0274
status: time limit exceeded
obtain:193 total:1352
elapsed:6.276
score:  0.00000

seed:  32
W: 119 H: 470 T: 20 D:15
p:0.0428 q:0.0328
status: survived
obtain:13 total:61
elapsed:0.022
score:  0.10555

seed:  33
W: 780 H: 381 T: 49 D: 0
p:0.0832 q:0.0263
status: survived
obtain:216 total:567
elapsed:3.719
score:  0.03183

seed:  34
W: 253 H: 397 T: 81 D: 4
p:0.0546 q:0.0905
status: survived
obtain:562 total:1428
elapsed:2.228
score:  0.07721

seed:  35
W: 971 H: 412 T: 69 D: 4
p:0.0496 q:0.0128
status: survived
obtain:134 total:460
elapsed:3.490
score:  0.04961

seed:  36
W: 915 H: 303 T: 64 D:17
p:0.0408 q:0.0372
status: survived
obtain:52 total:927
elapsed:1.247
score:  0.02586

seed:  37
W: 545 H: 385 T: 69 D: 3
p:0.0860 q:0.0268
status: survived
obtain:230 total:1268
elapsed:3.872
score:  0.04628

seed:  38
W: 944 H: 499 T: 95 D: 7
p:0.0221 q:0.0693
status: survived
obtain:524 total:1263
elapsed:1.973
score:  0.04543

seed:  39
W: 795 H: 900 T: 84 D:10
p:0.0957 q:0.0756
status: survived
obtain:167 total:2814
elapsed:3.945
score:  0.03941

seed:  40
W: 661 H: 304 T: 68 D: 7
p:0.0982 q:0.0318
status: survived
obtain:175 total:1764
elapsed:4.556
score:  0.05826

seed:  41
W: 905 H: 368 T: 70 D:19
p:0.0180 q:0.0588
status: survived
obtain:597 total:4747
elapsed:3.007
score:  0.03288

seed:  42
W: 793 H: 920 T: 33 D: 0
p:0.0186 q:0.0793
status: survived
obtain:584 total:7662
elapsed:3.346
score:  0.00142

seed:  43
W: 635 H: 439 T: 73 D:13
p:0.0268 q:0.0262
status: survived
obtain:220 total:530
elapsed:1.549
score:  0.09767

seed:  44
W: 100 H: 740 T: 46 D:10
p:0.0611 q:0.0278
status: smushed
obtain:3 total:15
elapsed:0.032
score:  0.00000

seed:  45
W: 102 H: 537 T: 70 D:17
p:0.0278 q:0.0285
status: smushed
obtain:41 total:104
elapsed:0.045
score:  0.00000

seed:  46
W: 855 H: 998 T: 15 D:17
p:0.0341 q:0.0205
status: survived
obtain:295 total:1304
elapsed:5.054
score:  0.08271

seed:  47
W: 137 H: 248 T: 76 D:12
p:0.0472 q:0.0526
status: survived
obtain:17 total:113
elapsed:0.009
score:  0.06499

seed:  48
W: 447 H: 792 T: 63 D: 6
p:0.0430 q:0.0313
status: smushed
obtain:241 total:736
elapsed:3.441
score:  0.00000

seed:  49
W: 182 H: 170 T: 24 D: 9
p:0.0182 q:0.0278
status: survived
obtain:116 total:156
elapsed:0.065
score:  0.09381

seed:  50
W: 811 H: 611 T: 85 D: 6
p:0.0841 q:0.0535
status: survived
obtain:338 total:3333
elapsed:4.508
score:  0.03917

seed:  51
W: 531 H: 694 T: 70 D: 3
p:0.0498 q:0.0613
status: survived
obtain:445 total:2075
elapsed:3.526
score:  0.02880

seed:  52
W: 319 H: 658 T: 85 D:13
p:0.0930 q:0.0113
status: survived
obtain:19 total:139
elapsed:1.069
score:  0.11930

seed:  53
W: 441 H: 352 T: 79 D: 2
p:0.0495 q:0.0648
status: survived
obtain:252 total:687
elapsed:0.600
score:  0.03821

seed:  54
W: 700 H: 160 T: 41 D: 6
p:0.0566 q:0.0996
status: survived
obtain:818 total:2143
elapsed:4.021
score:  0.10262

seed:  55
W: 116 H: 649 T: 24 D:16
p:0.0596 q:0.0553
status: survived
obtain:61 total:287
elapsed:0.149
score:  0.13695

seed:  56
W: 957 H: 934 T: 42 D:19
p:0.0174 q:0.0984
status: survived
obtain:717 total:4236
elapsed:3.853
score:  0.03604

seed:  57
W: 236 H: 860 T: 87 D:14
p:0.0763 q:0.0861
status: smushed
obtain:109 total:977
elapsed:0.643
score:  0.00000

seed:  58
W: 832 H: 247 T: 19 D: 1
p:0.0505 q:0.0533
status: survived
obtain:751 total:1069
elapsed:3.170
score:  0.05271

seed:  59
W: 972 H: 816 T: 32 D: 0
p:0.0739 q:0.0459
status: survived
obtain:145 total:4205
elapsed:3.654
score:  0.00255

seed:  60
W: 294 H: 275 T: 13 D:15
p:0.0021 q:0.0716
status: survived
obtain:888 total:912
elapsed:0.308
score:  0.02284

seed:  61
W: 255 H: 381 T: 14 D:10
p:0.0876 q:0.0401
status: smushed
obtain:14 total:140
elapsed:0.018
score:  0.00000

seed:  62
W: 214 H: 469 T: 22 D: 6
p:0.0202 q:0.0755
status: survived
obtain:1511 total:1662
elapsed:1.552
score:  0.10059

seed:  63
W: 772 H: 134 T: 71 D: 3
p:0.0915 q:0.0089
status: smushed
obtain:77 total:204
elapsed:1.312
score:  0.00000

seed:  64
W: 409 H: 348 T: 75 D: 3
p:0.0470 q:0.0432
status: survived
obtain:73 total:177
elapsed:0.186
score:  0.05359

seed:  65
W: 922 H: 760 T: 51 D: 1
p:0.0599 q:0.0414
status: survived
obtain:99 total:4472
elapsed:4.542
score:  0.00213

seed:  66
W: 687 H: 308 T: 89 D:17
p:0.0328 q:0.0971
status: smushed
obtain:1157 total:4791
elapsed:3.641
score:  0.00000

seed:  67
W: 517 H: 303 T: 98 D: 4
p:0.0742 q:0.0908
status: survived
obtain:788 total:2427
elapsed:3.636
score:  0.08468

seed:  68
W: 732 H: 582 T: 46 D: 4
p:0.0949 q:0.0324
status: survived
obtain:138 total:1525
elapsed:5.589
score:  0.02959

seed:  69
W: 944 H: 230 T: 41 D:11
p:0.0564 q:0.0803
status: survived
obtain:342 total:1612
elapsed:2.187
score:  0.08689

seed:  70
W: 158 H: 710 T: 70 D: 0
p:0.0433 q:0.0111
status: survived
obtain:40 total:44
elapsed:0.319
score:  0.04263

seed:  71
W: 891 H: 181 T: 98 D: 8
p:0.0317 q:0.0688
status: smushed
obtain:389 total:1202
elapsed:0.511
score:  0.00000

seed:  72
W: 500 H: 244 T: 71 D:10
p:0.0590 q:0.0011
status: survived
obtain:18 total:39
elapsed:1.385
score:  0.29585

seed:  73
W: 171 H: 830 T: 49 D: 3
p:0.0395 q:0.0158
status: survived
obtain:31 total:56
elapsed:0.241
score:  0.06158

seed:  74
W: 883 H: 951 T: 96 D: 7
p:0.0055 q:0.0821
status: survived
obtain:886 total:7497
elapsed:3.121
score:  0.00360

seed:  75
W: 932 H: 545 T: 34 D: 0
p:0.0166 q:0.0682
status: survived
obtain:857 total:5630
elapsed:3.334
score:  0.00255

seed:  76
W: 840 H: 194 T: 59 D: 2
p:0.0692 q:0.0008
status: survived
obtain:28 total:28
elapsed:3.369
score:  0.12815

seed:  77
W: 974 H: 780 T: 51 D: 0
p:0.0651 q:0.0257
status: survived
obtain:106 total:2438
elapsed:4.188
score:  0.00279

seed:  78
W: 793 H: 370 T: 91 D:17
p:0.0263 q:0.0903
status: smushed
obtain:736 total:4556
elapsed:2.746
score:  0.00000

seed:  79
W: 284 H: 847 T: 39 D:15
p:0.0958 q:0.0481
status: survived
obtain:15 total:130
elapsed:0.265
score:  0.09889

seed:  80
W: 845 H: 339 T: 24 D: 0
p:0.0906 q:0.0953
status: survived
obtain:555 total:4822
elapsed:3.731
score:  0.01056

seed:  81
W: 188 H: 915 T: 87 D:18
p:0.0853 q:0.0488
status: survived
obtain:24 total:411
elapsed:0.873
score:  0.05734

seed:  82
W: 407 H: 320 T: 57 D: 5
p:0.0962 q:0.0059
status: smushed
obtain:19 total:143
elapsed:0.196
score:  0.00000

seed:  83
W: 100 H: 847 T: 32 D:16
p:0.0613 q:0.0481
status: survived
obtain:33 total:155
elapsed:0.153
score:  0.14171

seed:  84
W: 232 H: 124 T: 92 D: 8
p:0.0481 q:0.0040
status: survived
obtain:0 total:24
elapsed:0.006
score:  0.00000

seed:  85
W: 419 H: 232 T: 93 D:11
p:0.0221 q:0.0543
status: survived
obtain:698 total:1155
elapsed:1.094
score:  0.10968

seed:  86
W: 295 H: 766 T: 59 D:15
p:0.0850 q:0.0628
status: smushed
obtain:138 total:955
elapsed:1.618
score:  0.00000

seed:  87
W: 825 H: 160 T: 11 D:17
p:0.0316 q:0.0937
status: survived
obtain:154 total:604
elapsed:0.304
score:  0.08850

seed:  88
W: 717 H: 225 T: 77 D: 6
p:0.0559 q:0.0990
status: smushed
obtain:542 total:2628
elapsed:1.517
score:  0.00000

seed:  89
W: 654 H: 334 T: 68 D:16
p:0.0072 q:0.0695
status: survived
obtain:793 total:1388
elapsed:0.497
score:  0.04181

seed:  90
W: 162 H: 483 T: 39 D: 1
p:0.0519 q:0.0462
status: survived
obtain:160 total:188
elapsed:0.295
score:  0.08411

seed:  91
W: 384 H: 275 T: 24 D:14
p:0.0791 q:0.0633
status: smushed
obtain:204 total:1461
elapsed:0.640
score:  0.00000

seed:  92
W: 200 H: 845 T: 38 D: 3
p:0.0964 q:0.0785
status: smushed
obtain:227 total:881
elapsed:0.967
score:  0.00000

seed:  93
W: 689 H: 208 T: 46 D: 5
p:0.0396 q:0.0980
status: survived
obtain:836 total:1996
elapsed:1.738
score:  0.06386

seed:  94
W: 884 H: 495 T: 56 D: 2
p:0.0739 q:0.0892
status: survived
obtain:472 total:3425
elapsed:3.994
score:  0.02239

seed:  95
W: 471 H: 406 T: 77 D:18
p:0.0097 q:0.0229
status: smushed
obtain:605 total:793
elapsed:1.562
score:  0.00000

seed:  96
W: 159 H: 617 T: 79 D:15
p:0.0377 q:0.0922
status: survived
obtain:75 total:442
elapsed:0.159
score:  0.05890

seed:  97
W: 978 H: 557 T: 28 D:14
p:0.0508 q:0.0778
status: survived
obtain:0 total:7577
elapsed:0.628
score:  0.00000

seed:  98
W: 669 H: 470 T: 88 D:13
p:0.0051 q:0.0503
status: survived
obtain:1574 total:2062
elapsed:3.211
score:  0.03537

seed:  99
W: 462 H: 953 T: 62 D:19
p:0.0990 q:0.0056
status: survived
obtain:34 total:328
elapsed:3.713
score:  0.13946

seed: 100
W: 290 H: 672 T: 76 D:13
p:0.0272 q:0.0391
status: survived
obtain:215 total:507
elapsed:0.954
score:  0.09805

ave:0.05367064345334224
