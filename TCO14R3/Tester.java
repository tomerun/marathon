import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.*;
import java.util.List;

public class Tester {
	public static boolean vis = false;
	public static String targetFolder = "testdata/300px";
	public static String sourceFolder = "testdata/100px";

	class Image {
		int H, W;
		int[][] pixels;

		void init(int H, int W) {
			this.H = H;
			this.W = W;
			this.pixels = new int[H][W];
		}

		public Image(int H, int W) {
			init(H, W);
		}

		public Image(String folder, String fileName) throws Exception {
			BufferedImage img;
			try {
				img = ImageIO.read(new File(folder, fileName));
			} catch (IOException e) {
				throw new Exception("Unable to read image from folder " + folder + " and file " + fileName + ".");
			}
			init(img.getHeight(), img.getWidth());
			int[] raster = img.getData().getPixels(0, 0, W, H, new int[H * W]);
			int pos = 0;
			for (int r = 0; r < H; r++) {
				for (int c = 0; c < W; c++) {
					pixels[r][c] = raster[pos++];
				}
			}
		}

		int intersect(int a, int b, int c, int d) {
			int from = Math.max(a, c);
			int to = Math.min(b, d);
			return from < to ? to - from : 0;
		}

		Image scale(int newH, int newW) {
			List<Integer> origRList = new ArrayList<Integer>();
			List<Integer> newRList = new ArrayList<Integer>();
			List<Integer> intrRList = new ArrayList<Integer>();
			List<Integer> origCList = new ArrayList<Integer>();
			List<Integer> newCList = new ArrayList<Integer>();
			List<Integer> intrCList = new ArrayList<Integer>();

			for (int origR = 0; origR < H; origR++) {
				int r1 = origR * newH, r2 = r1 + newH;
				for (int newR = 0; newR < newH; newR++) {
					int r3 = newR * H, r4 = r3 + H;
					int intr = intersect(r1, r2, r3, r4);
					if (intr > 0) {
						origRList.add(origR);
						newRList.add(newR);
						intrRList.add(intr);
					}
				}
			}
			for (int origC = 0; origC < W; origC++) {
				int c1 = origC * newW, c2 = c1 + newW;
				for (int newC = 0; newC < newW; newC++) {
					int c3 = newC * W, c4 = c3 + W;
					int intr = intersect(c1, c2, c3, c4);
					if (intr > 0) {
						origCList.add(origC);
						newCList.add(newC);
						intrCList.add(intr);
					}
				}
			}

			Image res = new Image(newH, newW);

			for (int i = 0; i < origRList.size(); i++) {
				int origR = origRList.get(i);
				int newR = newRList.get(i);
				int intrR = intrRList.get(i);

				for (int j = 0; j < origCList.size(); j++) {
					int origC = origCList.get(j);
					int newC = newCList.get(j);
					int intrC = intrCList.get(j);

					res.pixels[newR][newC] += intrR * intrC * pixels[origR][origC];
				}
			}

			for (int r = 0; r < newH; r++) {
				for (int c = 0; c < newW; c++) {
					res.pixels[r][c] = (2 * res.pixels[r][c] + H * W) / (2 * H * W);
				}
			}

			return res;
		}
	}

	class TestCase {
		public static final int TOTAL_IMAGE_COUNT = 1000;
		public static final int SOURCE_IMAGE_COUNT = 200;
		SecureRandom rnd = null;
		Set<Integer> IDs = new HashSet<Integer>();
		Image target;
		Image[] source;

		private int getFreeID() {
			while (true) {
				int id = rnd.nextInt(TOTAL_IMAGE_COUNT);
				if (!IDs.contains(id)) {
					IDs.add(id);
					return id;
				}
			}
		}

		public TestCase(long seed) throws Exception {
			try {
				rnd = SecureRandom.getInstance("SHA1PRNG");
			} catch (Exception e) {
				System.err.println("ERROR: unable to generate test case.");
				System.exit(1);
			}
			rnd.setSeed(seed);
			target = new Image(targetFolder, (seed % TOTAL_IMAGE_COUNT) + ".png");
			IDs.add((int) (seed % TOTAL_IMAGE_COUNT));
			source = new Image[SOURCE_IMAGE_COUNT];
			for (int i = 0; i < SOURCE_IMAGE_COUNT; i++) {
				source[i] = new Image(sourceFolder, getFreeID() + ".png");
			}
		}
	}

	class Drawer extends JFrame {
		class DrawerWindowListener extends WindowAdapter {
			public void windowClosing(WindowEvent event) {
				System.exit(0);
			}
		}

		class DrawerPanel extends JPanel {
			public void paint(Graphics g) {
				int w = (int) (target.getWidth() * SCALE);
				int h = (int) (target.getHeight() * SCALE);
				g.drawImage(target, GAP, GAP, w, h, null);
				g.drawImage(collage, GAP + w + GAP, GAP, w, h, null);
				g.drawImage(bndCollage, GAP, GAP + h + GAP, w, h, null);
				g.drawImage(diff, GAP + w + GAP, GAP + h + GAP, w, h, null);
			}
		}

		DrawerPanel panel;
		int width, height;
		BufferedImage target, collage;
		BufferedImage bndCollage, diff;

		static final double SCALE = 2;
		static final int GAP = 5;

		public Drawer(Image target, Image collage, int[] ans) {
			this.target = new BufferedImage(target.W, target.H, BufferedImage.TYPE_INT_RGB);
			this.collage = new BufferedImage(target.W, target.H, BufferedImage.TYPE_INT_RGB);
			this.bndCollage = new BufferedImage(target.W, target.H, BufferedImage.TYPE_INT_RGB);
			this.diff = new BufferedImage(target.W, target.H, BufferedImage.TYPE_INT_RGB);

			for (int r = 0; r < target.H; r++) {
				for (int c = 0; c < target.W; c++) {
					int vt = target.pixels[r][c];
					this.target.setRGB(c, r, new Color(vt, vt, vt).getRGB());

					int vc = collage.pixels[r][c];
					this.collage.setRGB(c, r, new Color(vc, vc, vc).getRGB());
					this.bndCollage.setRGB(c, r, new Color(vt, vt, vt).getRGB());

					if (vt <= vc) {
						int d = vc - vt;
						this.diff.setRGB(c, r, new Color(255, 255 - d, 255 - d).getRGB());
					} else {
						int d = vt - vc;
						this.diff.setRGB(c, r, new Color(255 - d, 255 - d, 255).getRGB());
					}
				}
			}

			for (int i = 0; i < ans.length / 4; i++) {
				int fromR = ans[4 * i], fromC = ans[4 * i + 1];
				int toR = ans[4 * i + 2], toC = ans[4 * i + 3];
				if (fromR == -1) {
					continue;
				}
				for (int r = fromR; r <= toR; r++) {
					for (int c = fromC; c <= toC; c++) {
						if (r == fromR || r == toR || c == fromC || c == toC) {
							this.bndCollage.setRGB(c, r, new Color(255, 0, 0).getRGB());
						}
					}
				}
			}

			panel = new DrawerPanel();
			getContentPane().add(panel);
			addWindowListener(new DrawerWindowListener());
			width = GAP * 3 + 2 * (int) (SCALE * target.W);
			height = GAP * 3 + 2 * (int) (SCALE * target.H);
			setSize(width, height + 20);
			setTitle("TCO'14 Marathon Round 3");
			setResizable(false);
			setVisible(true);
		}
	}

	Result runTest(long seed) throws Exception {
		Result res = new Result();
		res.seed = seed;
		TestCase tc = new TestCase(seed);
		int size = 0;
		size += tc.target.H * tc.target.W + 2;
		for (Image img : tc.source) {
			size += img.H * img.W + 2;
		}
		int[] ar = new int[size];
		int pos = 0;
		ar[pos++] = tc.target.H;
		ar[pos++] = tc.target.W;
		for (int i = 0; i < tc.target.H; ++i) {
			for (int j = 0; j < tc.target.W; ++j) {
				ar[pos++] = tc.target.pixels[i][j];
			}
		}
		for (Image img : tc.source) {
			ar[pos++] = img.H;
			ar[pos++] = img.W;
			for (int i = 0; i < img.H; ++i) {
				for (int j = 0; j < img.W; ++j) {
					ar[pos++] = img.pixels[i][j];
				}
			}
		}
		long before = System.currentTimeMillis();
		int[] ans = new CollageMaker().compose(ar);
		res.elapsed = System.currentTimeMillis() - before;

		Image collage = new Image(tc.target.H, tc.target.W);
		for (int i = 0; i < tc.target.H; i++) {
			Arrays.fill(collage.pixels[i], -1);
		}

		for (int i = 0; i < TestCase.SOURCE_IMAGE_COUNT; i++) {
			int fromR = ans[4 * i], fromC = ans[4 * i + 1];
			int toR = ans[4 * i + 2], toC = ans[4 * i + 3];
			if (fromR == -1 && fromC == -1 && toR == -1 && toC == -1) {
				continue;
			}
			if (fromR < 0 || fromR >= tc.target.H) {
				System.err.println("ERROR: top row for " + i + "-th source image (0-based) must be from 0" + " to "
						+ (tc.target.H - 1) + ", inclusive. In your return value it is equal to " + fromR + ".");
				return res;
			}
			if (toR < 0 || toR >= tc.target.H) {
				System.err.println("ERROR: bottom row for " + i + "-th source image (0-based) must be from 0" + " to "
						+ (tc.target.H - 1) + ", inclusive. In your return value it is equal to " + toR + ".");
				return res;
			}
			if (fromC < 0 || fromC >= tc.target.W) {
				System.err.println("ERROR: left column for " + i + "-th source image (0-based) must be from 0" + " to "
						+ (tc.target.W - 1) + ", inclusive. In your return value it is equal to " + fromC + ".");
				return res;
			}
			if (toC < 0 || toC >= tc.target.W) {
				System.err.println("ERROR: right column for " + i + "-th source image (0-based) must be from 0" + " to "
						+ (tc.target.W - 1) + ", inclusive. In your return value it is equal to " + toC + ".");
				return res;
			}
			if (fromR > toR) {
				System.err.println("ERROR: top row for " + i + "-th source image (0-based) can't be larger than"
						+ " bottom row. In your return value we have " + fromR + " > " + toR + ".");
				return res;
			}
			if (fromC > toC) {
				System.err.println("ERROR: left column for " + i + "-th source image (0-based) can't be larger than"
						+ " right column. In your return value we have " + fromC + " > " + toC + ".");
				return res;
			}
			int newH = toR - fromR + 1;
			if (newH > tc.source[i].H) {
				System.err.println("ERROR: scaled height for " + i + "-th source image (0-based) can't be larger than"
						+ " its original height. In your return value we have " + newH + " > " + tc.source[i].H + ".");
				return res;
			}
			int newW = toC - fromC + 1;
			if (newW > tc.source[i].W) {
				System.err.println("ERROR: scaled width for " + i + "-th source image (0-based) can't be larger than"
						+ " its original width. In your return value we have " + newW + " > " + tc.source[i].W + ".");
				return res;
			}

			Image scaled = tc.source[i].scale(newH, newW);
			for (int r = fromR; r <= toR; r++) {
				for (int c = fromC; c <= toC; c++) {
					if (collage.pixels[r][c] != -1) {
						System.err.println("ERROR: pixel at row " + r + ", column " + c + " (0-based) in target"
								+ " image is covered by at least 2 source images.");
						return res;
					}
					collage.pixels[r][c] = scaled.pixels[r - fromR][c - fromC];
				}
			}
		}

		double score = 0.0;
		for (int r = 0; r < collage.H; r++) {
			for (int c = 0; c < collage.W; c++) {
				if (collage.pixels[r][c] == -1) {
					System.err.println("ERROR: pixel at row " + r + ", column " + c + " (0-based) in target image"
							+ " is not covered by any source images.");
					return res;
				}
				int diff = tc.target.pixels[r][c] - collage.pixels[r][c];
				score += diff * diff;
			}
		}

		if (vis) {
			new Drawer(tc.target, collage, ans);
		}
		res.score = Math.sqrt(score / (tc.target.H * tc.target.W));
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = -1;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-b")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-e")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-vis")) {
				vis = true;
			} else if (args[i].equals("-target")) {
				targetFolder = args[++i];
			} else if (args[i].equals("-source")) {
				sourceFolder = args[++i];
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}

		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		double score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("score:%.4f", (double) score);
			return ret;
		}
	}

}
