#!/usr/bin/ruby
if ARGV.length < 2
  p "need 2 arguments"
  exit
end

def get_scores(filename)
  scores = []
  seed = 0;
  IO.foreach(filename){ |line|
    index = line.index("seed")
    if index == 0
      seed = line[index+5..8].to_i
    end
    index = line.index("score")
    if index == 0
      scores[seed] = line[index+6..-1].to_f
    end
  }
  return scores
end

from = get_scores(ARGV[0])
to = get_scores(ARGV[1])
seed_begin = ARGV[2] ? ARGV[2].to_i : nil
seed_end = ARGV[3] ? ARGV[3].to_i : nil

sum = 0
sumScoreDiff = 0
c = 0
win = 0
lose = 0
len = from.length < to.length ? from.length : to.length
width = 4
if len < 10
  width = 1
elsif len < 100
  width = 2
elsif len < 1000
  width = 3
end 

(seed_begin || 1).upto(seed_end || from.length - 1) do |seed|
  next if !from[seed] || !to[seed]
  c += 1;
  # dif = from[seed] - to[seed]
  dif = to[seed] == 0 ? -1.0 : 1.0 * from[seed] / to[seed]
  puts sprintf("%#{width}d % 5f", seed, dif)
  # sumScoreDiff += dif;
  if from[seed] < to[seed]
    lose += 1
    sumScoreDiff += dif - 1.0 # assume from[seed] is relative score 1.0
  elsif from[seed] > to[seed]
    win += 1
    sumScoreDiff += 1.0 - to[seed] / from[seed] # assume to[seed] is relative score 1.0
  end
  sum += dif
end

puts
puts "ave: " + (sum / c).to_s
puts "diff:" + (sumScoreDiff / c).to_s
puts "win: " + win.to_s
puts "lose:" + lose.to_s
puts "tie: " + (c - win - lose).to_s
