import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

public final class CollageMaker {

	private static final boolean DEBUG = 1 == 0;
	private static final boolean MEASURE_TIME = 1 == 0;
	private static final double INF = 1e99;
	private static final long TL = 42000;
	private static final long TL2 = 47000;
	private static final int N = 200;
	final long startTime = System.currentTimeMillis();
	XorShift rnd = new XorShift();
//	Timer timer = new Timer();
	int TH, TW;
	int[][] targetData;
	Image[] source = new Image[N];
	int RP, CP;
	int[] rp, cp;
	int[] ret = new int[N * 4];
	int[][] tgSum1;
	int turn;
	Rect[][][][] wholeRects;

	public int[] compose(int[] data) {
//		timer.start(0);
		init(data);
//		timer.stop(0);
		solve();
//		timer.print();
		return ret;
	}

	double ave(int t, int b, int l, int r) {
		double ret = tgSum1[b][r] - tgSum1[b][l] - tgSum1[t][r] + tgSum1[t][l];
		return ret / ((b - t) * (r - l));
	}

	double ave(Rect r) {
		return ave(r.t, r.b, r.l, r.r);
	}

	void init(int[] data) {
		int pos = 0;
		TH = data[pos++];
		TW = data[pos++];
		targetData = new int[TH][TW];
		for (int i = 0; i < TH; ++i) {
			for (int j = 0; j < TW; ++j) {
				targetData[i][j] = data[pos++];
			}
		}
		tgSum1 = new int[TH + 1][TW + 1];
		for (int i = 0; i < TH; ++i) {
			for (int j = 0; j < TW; ++j) {
				tgSum1[i + 1][j + 1] = tgSum1[i][j + 1] + tgSum1[i + 1][j] - tgSum1[i][j] + targetData[i][j];
			}
		}
		for (int i = 0; i < N; ++i) {
			source[i] = new Image(data, pos);
			source[i].idx = i;
			pos += source[i].W * source[i].H + 2;
		}
		RP = 10;
		CP = 10;
		rp = new int[RP + 1];
		cp = new int[CP + 1];
		for (int i = 0; i <= RP; ++i) {
			rp[i] = TH * i / RP;
		}
		for (int i = 0; i <= CP; ++i) {
			cp[i] = TW * i / CP;
		}
		wholeRects = new Rect[RP][RP][CP][CP];
		for (int top = 0; top < RP; ++top) {
			int t = rp[top];
			for (int bottom = top + 1; bottom <= RP; ++bottom) {
				int b = rp[bottom];
				if (b - t > 100) break;
				for (int left = 0; left < CP; ++left) {
					int l = cp[left];
					for (int right = left + 1; right <= CP; ++right) {
						int r = cp[right];
						if (r - l > 100) break;
						wholeRects[top][bottom - 1][left][right - 1] = new Rect(t, b, l, r);
					}
				}
			}
		}
	}

	static class Assign implements Comparable<Assign> {
		int si, ti, bi, li, ri;
		int difSum;
		double val;

		public Assign(int si, int ti, int bi, int li, int ri) {
			this.si = si;
			this.ti = ti;
			this.bi = bi;
			this.li = li;
			this.ri = ri;
		}

		public int compareTo(Assign o) {
			return Double.compare(this.val, o.val);
		}
	}

	double solve() {
		ArrayList<Assign> assigns = new ArrayList<>();
//		timer.start(1);
		for (int i = 0; i < N; ++i) {
			for (int top = 0; top < RP; ++top) {
				int t = rp[top];
				for (int bottom = top + 1; bottom <= RP; ++bottom) {
					int b = rp[bottom];
					if (b - t > source[i].H) break;
					for (int left = 0; left < CP; ++left) {
						int l = cp[left];
						for (int right = left + 1; right <= CP; ++right) {
							int r = cp[right];
							if (r - l > source[i].W) continue;
							Rect rect = wholeRects[top][bottom - 1][left][right - 1];
							Assign asg = new Assign(i, top, bottom, left, right);
							asg.difSum = source[i].calcStrict(rect, bottom - top == 1 && right - left == 1);
							if (asg.difSum == Integer.MAX_VALUE) continue;
							int area = (b - t) * (r - l);
							asg.val = asg.difSum / Math.pow(area, 1.2);
							assigns.add(asg);
						}
					}
				}
			}
		}
//		timer.stop(1);
		debug("assign size:" + assigns.size());
//		timer.start(2);
		Collections.sort(assigns);
//		timer.stop(2);
		ArrayList<Rect> rects = getRectCombination(assigns);
		double score = calcScore(rects);
		debug("initial Score:" + score);
		score = optimize(ret.clone(), TL * 4 / 5);
		debug("optimize Score:" + score);
		score = optimize2(ret.clone());
		debug("optimize2 Score:" + score);
		return score;
	}

	ArrayList<Rect> getRectCombination(ArrayList<Assign> assigns) {
//		timer.start(3);
		ArrayList<Rect> rects = new ArrayList<>();
		int[] usedPos = new int[RP];
		boolean[] usedSrc = new boolean[N];
		int uc = 0;
		OUT: for (int i = 0; i < assigns.size(); ++i) {
			Assign as = assigns.get(i);
			if (usedSrc[as.si]) continue;
			Rect rect = wholeRects[as.ti][as.bi - 1][as.li][as.ri - 1];
			int mask = (1 << (as.ri - as.li)) - 1;
			mask <<= as.li;
			for (int j = as.ti; j < as.bi; ++j) {
				if ((usedPos[j] & mask) != 0) {
					continue OUT;
				}
			}
			for (int j = as.ti; j < as.bi; ++j) {
				usedPos[j] |= mask;
			}
			uc += (as.bi - as.ti) * (as.ri - as.li);
			usedSrc[as.si] = true;
			rects.add(rect);
			if (uc == RP * CP) break;
		}
//		timer.stop(3);
		return rects;
	}

	static class Edge {
		int t, b, l, r, si;

		Edge() {}

		Edge(int t, int b, int l, int r, int si) {
			this.t = t;
			this.b = b;
			this.l = l;
			this.r = r;
			this.si = si;
		}

		Edge(Edge o) {
			this.t = o.t;
			this.b = o.b;
			this.l = o.l;
			this.r = o.r;
			this.si = o.si;
		}

		void set(Edge o) {
			this.t = o.t;
			this.b = o.b;
			this.l = o.l;
			this.r = o.r;
			this.si = o.si;
		}

		void set(int t, int b, int l, int r) {
			this.t = t;
			this.b = b;
			this.l = l;
			this.r = r;
			this.si = -1;
		}
	}

	ArrayList<Edge> retToEdges(int[] ret) {
		ArrayList<Edge> edges = new ArrayList<>();
		for (int i = 0; i < N; ++i) {
			if (ret[i * 4] == -1) continue;
			int t = 0;
			int b = 0;
			int l = 0;
			int r = 0;
			for (int j = 0; j <= RP; ++j) {
				if (ret[i * 4] == rp[j]) t = j;
				if (ret[i * 4 + 2] + 1 == rp[j]) b = j;
			}
			for (int j = 0; j <= CP; ++j) {
				if (ret[i * 4 + 1] == cp[j]) l = j;
				if (ret[i * 4 + 3] + 1 == cp[j]) r = j;
			}
			edges.add(new Edge(t, b, l, r, i));
		}
		return edges;
	}

	double optimize(int[] bestRet, long tl) {
		int MUL = 3;
		RP *= MUL;
		CP *= MUL;
		rp = new int[RP + 1];
		cp = new int[CP + 1];
		for (int i = 0; i < rp.length; ++i) {
			rp[i] = i * TH / RP;
		}
		for (int i = 0; i < cp.length; ++i) {
			cp[i] = i * TW / CP;
		}
		ArrayList<Edge> edges = retToEdges(bestRet);
		BitSet used = new BitSet(N);
		int[] es = new int[N];
		int score = 0;
		ArrayList<Edge> bestEdges = new ArrayList<>();
		for (int i = 0; i < edges.size(); ++i) {
			Edge e = edges.get(i);
			es[i] = source[e.si].calcStrict(rp[e.t], rp[e.b], cp[e.l], cp[e.r]);
			score += es[i];
			used.set(e.si);
			bestEdges.add(new Edge(e));
		}

		int bestScore = score;
		int[] nextScore = new int[N];
		transRatio = 600;
		long before = System.currentTimeMillis() - startTime;
		long rest = tl - before;
		int DIV = 8;
		long stableTime = rest / DIV + before;
		int mask = (1 << (DIV - 2)) - 1;
		Edge[] next = new Edge[N];
		Edge[] born = new Edge[N];
		for (int i = 0; i < N; ++i) {
			next[i] = new Edge();
			born[i] = new Edge();
		}
		BitSet nextUsed = new BitSet(N);
		int stableCount = 0;
		for (turn = 1;; ++turn) {
			if ((turn & 0x7F) == 0) {
				long elapsed = System.currentTimeMillis() - startTime;
				if (elapsed > tl) {
					debug("turn:" + turn);
					break;
				}
				if (elapsed > stableTime) {
					transRatio *= 1.2;
					debug("stable:" + transRatio + " " + edges.size() + " " + Math.sqrt(bestScore / (TH * TW)));
					stableTime += rest / DIV;
					mask /= 2;
					++stableCount;
					if (stableCount * 2 == DIV) {
						RP *= 2;
						CP *= 2;
						rp = new int[RP + 1];
						cp = new int[CP + 1];
						for (int i = 0; i < rp.length; ++i) {
							rp[i] = i * TH / RP;
						}
						for (int i = 0; i < cp.length; ++i) {
							cp[i] = i * TW / CP;
						}
						for (Edge e : edges) {
							e.t *= 2;
							e.b *= 2;
							e.l *= 2;
							e.r *= 2;
						}
						for (Edge e : bestEdges) {
							e.t *= 2;
							e.b *= 2;
							e.l *= 2;
							e.r *= 2;
						}
					}
				}
			}
			double thresholdScore = 5 * score / transRatio;
			if ((turn & mask) == 0) {
				int nextI = 0;
				int bornI = 0;
				nextUsed.clear();
				int moveEdgeI = rnd.nextInt(edges.size() * 4);
				int dir = moveEdgeI & 3;
				moveEdgeI >>= 2;
				Edge me = edges.get(moveEdgeI);
				int diffSum = 0;
				thresholdScore *= 2;
				boolean tooLargeDiff = false;
				if (dir == 0) {
					if (me.t == 0 || rp[me.b] - rp[me.t - 1] > source[me.si].H) {
						continue;
					}
					for (int i = 0; i < edges.size(); ++i) {
						Edge e = edges.get(i);
						if (e == me) {
							int ns = source[e.si].calcStrict(rp[e.t - 1], rp[e.b], cp[e.l], cp[e.r]);
							nextScore[nextI] = ns;
							next[nextI].set(e);
							next[nextI].t--;
							++nextI;
							diffSum += ns - es[i];
							if (diffSum > thresholdScore) {
								tooLargeDiff = true;
								break;
							}
							nextUsed.set(e.si);
						} else if (e.b == me.t && !(e.r <= me.l || me.r <= e.l)) {
							if (e.t != me.t - 1) {
								int ns = source[e.si].calcStrict(rp[e.t], rp[e.b - 1], cp[e.l], cp[e.r]);
								nextScore[nextI] = ns;
								next[nextI].set(e);
								next[nextI].b--;
								++nextI;
								diffSum += ns - es[i];
								if (diffSum > thresholdScore) {
									tooLargeDiff = true;
									break;
								}
								nextUsed.set(e.si);
							} else {
								diffSum -= es[i];
							}
							if (e.l < me.l) {
								born[bornI++].set(me.t - 1, me.t, e.l, me.l);
							}
							if (me.r < e.r) {
								born[bornI++].set(me.t - 1, me.t, me.r, e.r);
							}
						} else {
							nextScore[nextI] = es[i];
							next[nextI++].set(e);
							nextUsed.set(e.si);
						}
					}
				} else if (dir == 1) {
					if (me.l == 0 || cp[me.r] - cp[me.l - 1] > source[me.si].W) {
						continue;
					}
					for (int i = 0; i < edges.size(); ++i) {
						Edge e = edges.get(i);
						if (e == me) {
							int ns = source[e.si].calcStrict(rp[e.t], rp[e.b], cp[e.l - 1], cp[e.r]);
							nextScore[nextI] = ns;
							next[nextI].set(e);
							next[nextI].l--;
							++nextI;
							diffSum += ns - es[i];
							if (diffSum > thresholdScore) {
								tooLargeDiff = true;
								break;
							}
							nextUsed.set(e.si);
						} else if (e.r == me.l && !(e.b <= me.t || me.b <= e.t)) {
							if (e.l != me.l - 1) {
								int ns = source[e.si].calcStrict(rp[e.t], rp[e.b], cp[e.l], cp[e.r - 1]);
								nextScore[nextI] = ns;
								next[nextI].set(e);
								next[nextI].r--;
								++nextI;
								diffSum += ns - es[i];
								if (diffSum > thresholdScore) {
									tooLargeDiff = true;
									break;
								}
								nextUsed.set(e.si);
							} else {
								diffSum -= es[i];
							}
							if (e.t < me.t) {
								born[bornI++].set(e.t, me.t, me.l - 1, me.l);
							}
							if (me.b < e.b) {
								born[bornI++].set(me.b, e.b, me.l - 1, me.l);
							}
						} else {
							nextScore[nextI] = es[i];
							next[nextI++].set(e);
							nextUsed.set(e.si);
						}
					}
				} else if (dir == 2) {
					if (me.b == RP || rp[me.b + 1] - rp[me.t] > source[me.si].H) {
						continue;
					}
					for (int i = 0; i < edges.size(); ++i) {
						Edge e = edges.get(i);
						if (e == me) {
							int ns = source[e.si].calcStrict(rp[e.t], rp[e.b + 1], cp[e.l], cp[e.r]);
							nextScore[nextI] = ns;
							next[nextI].set(e);
							next[nextI].b++;
							++nextI;
							diffSum += ns - es[i];
							if (diffSum > thresholdScore) {
								tooLargeDiff = true;
								break;
							}
							nextUsed.set(e.si);
						} else if (e.t == me.b && !(e.r <= me.l || me.r <= e.l)) {
							if (e.b != me.b + 1) {
								int ns = source[e.si].calcStrict(rp[e.t + 1], rp[e.b], cp[e.l], cp[e.r]);
								nextScore[nextI] = ns;
								next[nextI].set(e);
								next[nextI].t++;
								++nextI;
								diffSum += ns - es[i];
								if (diffSum > thresholdScore) {
									tooLargeDiff = true;
									break;
								}
								nextUsed.set(e.si);
							} else {
								diffSum -= es[i];
							}
							if (e.l < me.l) {
								born[bornI++].set(me.b, me.b + 1, e.l, me.l);
							}
							if (me.r < e.r) {
								born[bornI++].set(me.b, me.b + 1, me.r, e.r);
							}
						} else {
							nextScore[nextI] = es[i];
							next[nextI++].set(e);
							nextUsed.set(e.si);
						}
					}
				} else {
					if (me.r == CP || cp[me.r + 1] - cp[me.l] > source[me.si].W) {
						continue;
					}
					for (int i = 0; i < edges.size(); ++i) {
						Edge e = edges.get(i);
						if (e == me) {
							int ns = source[e.si].calcStrict(rp[e.t], rp[e.b], cp[e.l], cp[e.r + 1]);
							nextScore[nextI] = ns;
							next[nextI].set(e);
							next[nextI].r++;
							++nextI;
							diffSum += ns - es[i];
							if (diffSum > thresholdScore) {
								tooLargeDiff = true;
								break;
							}
							nextUsed.set(e.si);
						} else if (e.l == me.r && !(e.b <= me.t || me.b <= e.t)) {
							if (e.r != me.r + 1) {
								int ns = source[e.si].calcStrict(rp[e.t], rp[e.b], cp[e.l + 1], cp[e.r]);
								nextScore[nextI] = ns;
								next[nextI].set(e);
								next[nextI].l++;
								++nextI;
								diffSum += ns - es[i];
								if (diffSum > thresholdScore) {
									tooLargeDiff = true;
									break;
								}
								nextUsed.set(e.si);
							} else {
								diffSum -= es[i];
							}
							if (e.t < me.t) {
								born[bornI++].set(e.t, me.t, me.r, me.r + 1);
							}
							if (me.b < e.b) {
								born[bornI++].set(me.b, e.b, me.r, me.r + 1);
							}
						} else {
							nextScore[nextI] = es[i];
							next[nextI++].set(e);
							nextUsed.set(e.si);
						}
					}
				}
				if (tooLargeDiff) continue;
				if (nextI + bornI > N) continue;
				boolean ok = true;
				for (int i = 0; i < bornI; ++i) {
					Edge e = born[i];
					int minI = -1;
					int minV = (int) (thresholdScore / 2 - diffSum);
					if (minV < 0) {
						ok = false;
						break;
					}
					for (int j = 0; j < N; ++j) {
						if (nextUsed.get(j)) continue;
						int v = source[j].calcStrict(rp[e.t], rp[e.b], cp[e.l], cp[e.r], minV);
						if (v < minV) {
							minV = v;
							minI = j;
						}
					}
					if (minI == -1) {
						ok = false;
						break;
					}
					e.si = minI;
					nextScore[nextI] = minV;
					next[nextI++].set(e);
					diffSum += minV;
					nextUsed.set(minI);
				}
				if (!ok) continue;

				if (transit(score, diffSum)) {
					edges.clear();
					for (int i = 0; i < nextI; ++i) {
						edges.add(new Edge(next[i]));
					}
					score += diffSum;
					BitSet tmp = used;
					used = nextUsed;
					nextUsed = tmp;
					for (int i = 0; i < edges.size(); ++i) {
						es[i] = nextScore[i];
					}
					if (score < bestScore) {
						bestScore = score;
						bestEdges.clear();
						for (int i = 0; i < edges.size(); ++i) {
							bestEdges.add(new Edge(edges.get(i)));
						}
						//						debug(edges.size(), Math.sqrt(1.0 * bestScore / (TH * TW)));
					}
				}
			} else {
				int mi = rnd.nextInt(edges.size());
				Edge me = edges.get(mi);
				int i1 = me.si;
				int i2 = rnd.nextInt(N - 1);
				if (i2 >= i1) ++i2;
				if (used.get(i2)) {
					int mi2 = -1;
					for (int i = 0; i < edges.size(); ++i) {
						if (edges.get(i).si == i2) {
							mi2 = i;
							break;
						}
					}
					Edge me2 = edges.get(mi2);
					int diff = -es[mi] - es[mi2];
					int v1 = source[i1].calcStrict(rp[me2.t], rp[me2.b], cp[me2.l], cp[me2.r], (int) (thresholdScore) - diff);
					if (v1 == Integer.MAX_VALUE) continue;
					diff += v1;
					int v2 = source[i2].calcStrict(rp[me.t], rp[me.b], cp[me.l], cp[me.r], (int) thresholdScore - diff);
					if (v2 == Integer.MAX_VALUE) continue;
					diff += v2;
					if (transit(score, diff)) {
						score += diff;
						edges.set(mi, new Edge(me2.t, me2.b, me2.l, me2.r, i1));
						edges.set(mi2, new Edge(me.t, me.b, me.l, me.r, i2));
						es[mi] = v1;
						es[mi2] = v2;
						if (score < bestScore) {
							bestScore = score;
							bestEdges.clear();
							for (int i = 0; i < edges.size(); ++i) {
								bestEdges.add(new Edge(edges.get(i)));
							}
							//							debug(edges.size(), Math.sqrt(bestScore / (TH * TW)));
						}
					}

				} else {
					int diff = -es[mi];
					int v = source[i2].calcStrict(rp[me.t], rp[me.b], cp[me.l], cp[me.r], (int) thresholdScore - diff);
					diff += v;
					if (transit(score, diff)) {
						score += diff;
						used.clear(i1);
						used.set(i2);
						edges.set(mi, new Edge(me.t, me.b, me.l, me.r, i2));
						es[mi] = v;
						if (score < bestScore) {
							bestScore = score;
							bestEdges.clear();
							for (int i = 0; i < edges.size(); ++i) {
								bestEdges.add(new Edge(edges.get(i)));
							}
							//							debug(edges.size(), Math.sqrt(bestScore / (TH * TW)));
						}
					}
				}
			}
		}

		Arrays.fill(ret, -1);
		for (Edge e : bestEdges) {
			ret[source[e.si].idx * 4 + 0] = rp[e.t];
			ret[source[e.si].idx * 4 + 1] = cp[e.l];
			ret[source[e.si].idx * 4 + 2] = rp[e.b] - 1;
			ret[source[e.si].idx * 4 + 3] = cp[e.r] - 1;
		}

//		timer.start(7);
		ArrayList<Rect> rects = new ArrayList<>();
		for (Edge e : bestEdges) {
			rects.add(new Rect(rp[e.t], rp[e.b], cp[e.l], cp[e.r]));
		}
		double finalScore = calcScore(rects);
//		timer.stop(7);
		return finalScore == INF ? bestScore : finalScore;
	}

	double optimize2(int[] bestRet) {
		ArrayList<Edge> initEdges = retToEdges(bestRet);
		int M = initEdges.size();
		Edge[] edges = new Edge[M];
		int[] es = new int[M];
		int score = 0;
		for (int i = 0; i < M; ++i) {
			edges[i] = initEdges.get(i);
			es[i] = source[edges[i].si].calcStrict(rp[edges[i].t], rp[edges[i].b], cp[edges[i].l], cp[edges[i].r]);
			score += es[i];
		}

		int[] rpIdx, cpIdx;
		{
			BitSet re = new BitSet(RP + 1);
			for (int i = 0; i < M; ++i) {
				re.set(edges[i].t);
				re.set(edges[i].b);
			}
			rpIdx = new int[re.cardinality() - 2];
			int rpP = 0;
			for (int i = 1; i < RP; ++i) {
				if (re.get(i)) rpIdx[rpP++] = i;
			}
			BitSet ce = new BitSet(CP + 1);
			for (int i = 0; i < M; ++i) {
				ce.set(edges[i].l);
				ce.set(edges[i].r);
			}
			cpIdx = new int[ce.cardinality() - 2];
			int cpP = 0;
			for (int i = 1; i < CP; ++i) {
				if (ce.get(i)) cpIdx[cpP++] = i;
			}
			debug(rpIdx.length, cpIdx.length);
		}
		int bestScore = score;
		int[] bestRp = rp.clone();
		int[] bestCp = cp.clone();
		int[] diff = new int[M];
		transRatio = 1000;
		long before = System.currentTimeMillis() - startTime;
		long rest = TL - before;
		int DIV = 8;
		long stableTime = rest / DIV + before;
		final int MAX_MOVE = 4;
		for (turn = 1;; ++turn) {
			if ((turn & 63) == 0) {
				long elapsed = System.currentTimeMillis() - startTime;
				if (elapsed > TL) {
					debug("turn:" + turn);
					break;
				}
				if (elapsed > stableTime) {
					transRatio *= 1.2;
					debug("stable:" + transRatio + " " + Math.sqrt(1. * bestScore / (TH * TW)));
					stableTime += rest / DIV;
				}
			}
			{
				int pos = rpIdx[rnd.nextInt(rpIdx.length)];
				int moveMin = Math.max(-MAX_MOVE, rp[pos - 1] - rp[pos] + 1);
				int moveMax = Math.min(MAX_MOVE, rp[pos + 1] - rp[pos] - 1);
				if (moveMax == moveMin) {
					continue;
				}
				int move = rnd.nextInt(moveMax - moveMin) + moveMin;
				if (move >= 0) move++;
				rp[pos] += move;
				int diffSum = 0;
				boolean ok = true;
				for (int i = 0; i < M; ++i) {
					Edge e = edges[i];
					if (e.t == pos || e.b == pos) {
						if (rp[e.b] - rp[e.t] > source[e.si].H) {
							ok = false;
							break;
						}
						int nc = source[e.si].calcStrict(rp[e.t], rp[e.b], cp[e.l], cp[e.r]);
						diff[i] = nc - es[i];
						diffSum += diff[i];
					} else {
						diff[i] = 0;
					}
				}
				if (ok && transit(score, diffSum)) {
					score += diffSum;
					for (int i = 0; i < M; ++i) {
						es[i] += diff[i];
					}
					if (score < bestScore) {
						bestScore = score;
						bestRp = rp.clone();
						bestCp = cp.clone();
						//						debug(Math.sqrt(1. * bestScore / (TH * TW)));
					}
				} else {
					rp[pos] -= move;
				}
			}
			{
				int pos = cpIdx[rnd.nextInt(cpIdx.length)];
				int moveMin = Math.max(-MAX_MOVE, cp[pos - 1] - cp[pos] + 1);
				int moveMax = Math.min(MAX_MOVE, cp[pos + 1] - cp[pos] - 1);
				if (moveMax == moveMin) {
					continue;
				}
				int move = rnd.nextInt(moveMax - moveMin) + moveMin;
				if (move >= 0) move++;
				cp[pos] += move;
				int diffSum = 0;
				boolean ok = true;
				for (int i = 0; i < M; ++i) {
					Edge e = edges[i];
					if (e.l == pos || e.r == pos) {
						if (cp[e.r] - cp[e.l] > source[e.si].W) {
							ok = false;
							break;
						}
						int nc = source[e.si].calcStrict(rp[e.t], rp[e.b], cp[e.l], cp[e.r]);
						diff[i] = nc - es[i];
						diffSum += diff[i];
					} else {
						diff[i] = 0;
					}
				}
				if (ok && transit(score, diffSum)) {
					score += diffSum;
					for (int i = 0; i < M; ++i) {
						es[i] += diff[i];
					}
					if (score < bestScore) {
						bestScore = score;
						bestRp = rp.clone();
						bestCp = cp.clone();
						//						debug(Math.sqrt(1. * bestScore / (TH * TW)));
					}
				} else {
					cp[pos] -= move;
				}
			}
		}

		Arrays.fill(ret, -1);
		for (int i = 0; i < M; ++i) {
			Edge e = edges[i];
			ret[source[e.si].idx * 4 + 0] = bestRp[e.t];
			ret[source[e.si].idx * 4 + 1] = bestCp[e.l];
			ret[source[e.si].idx * 4 + 2] = bestRp[e.b] - 1;
			ret[source[e.si].idx * 4 + 3] = bestCp[e.r] - 1;
		}

//		timer.start(8);
		ArrayList<Rect> rects = new ArrayList<>();
		int[] threshold = new int[M];
		int[] sis = new int[M];
		for (int i = 0; i < M; ++i) {
			Edge e = edges[i];
			rects.add(new Rect(bestRp[e.t], bestRp[e.b], bestCp[e.l], bestCp[e.r]));
			threshold[i] = source[e.si].calcStrict(rects.get(rects.size() - 1), true);
			sis[i] = e.si;
		}
		debug("elapsed before final calc:" + (System.currentTimeMillis() - startTime));
		double finalScore = calcScore(rects, threshold, sis);
//		timer.stop(8);
		return finalScore == INF ? Math.sqrt(bestScore / (TH * TW)) : finalScore;
	}

	double transRatio = 1000;

	boolean transit(int cur, int diff) {
		if (diff <= 0) return true;
		return rnd.nextDouble() < Math.exp(-transRatio * diff / cur);
	}

	double calcScore(ArrayList<Rect> list, int[] threshold, int[] si) {
		MinCostFlow mcf = constructMCF(list, threshold, si);
		if (mcf == null) {
			return INF;
		}
//		timer.start(5);
		int cost = mcf.calc(list.size());
//		timer.stop(5);
		setRetWithMCF(mcf, list);
		double score = Math.sqrt(cost / (TH * TW));
		return score;
	}

	double calcScore(ArrayList<Rect> list) {
		if (list.size() > N) {
			debug("used rects size over:" + list.size());
			return INF;
		}
		MinCostFlow mcf = constructMCF(list);
//		timer.start(5);
		int cost = mcf.calc(list.size());
//		timer.stop(5);
		setRetWithMCF(mcf, list);
		double score = Math.sqrt(cost / (TH * TW));
		return score;
	}

	MinCostFlow constructMCF(ArrayList<Rect> list, int[] threshold, int[] si) {
//		timer.start(4);
		MinCostFlow mcf = new MinCostFlow(N + list.size() + 2);
		for (int i = 0; i < N; ++i) {
			mcf.addEdge(0, i + 1, 0);
			for (int j = 0; j < list.size(); ++j) {
				if (si[j] == i) {
					mcf.addEdge(i + 1, N + 1 + j, threshold[j]);
					continue;
				}
				Rect r = list.get(j);
				if (r.h() > source[i].H || r.w() > source[i].W) {
					continue;
				}
				int diff = source[i].calcStrict(r, threshold[j]);
				if (diff == Integer.MAX_VALUE) {
					continue;
				}
				mcf.addEdge(i + 1, N + 1 + j, diff);
			}
			if (System.currentTimeMillis() - startTime > TL2) {
				debug("escape! " + i);
				return null;
			}
			source[i].cachedDataStrict = null; // avoid MLE
		}
		for (int i = 0; i < list.size(); ++i) {
			mcf.addEdge(N + 1 + i, N + 1 + list.size(), 0);
		}
//		timer.stop(4);
		return mcf;
	}

	MinCostFlow constructMCF(ArrayList<Rect> list) {
//		timer.start(4);
		MinCostFlow mcf = new MinCostFlow(N + list.size() + 2);
		for (int i = 0; i < N; ++i) {
			mcf.addEdge(0, i + 1, 0);
			for (int j = 0; j < list.size(); ++j) {
				Rect r = list.get(j);
				if (r.h() > source[i].H || r.w() > source[i].W) {
					continue;
				}
				int diff = source[i].calcStrict(r, true);
				if (diff == Integer.MAX_VALUE) {
					continue;
				}
				mcf.addEdge(i + 1, N + 1 + j, diff);
			}
		}
		for (int i = 0; i < list.size(); ++i) {
			mcf.addEdge(N + 1 + i, N + 1 + list.size(), 0);
		}
//		timer.stop(4);
		return mcf;
	}

	void setRetWithMCF(MinCostFlow mcf, ArrayList<Rect> list) {
		for (int i = 0; i < N; ++i) {
			int ri = -1;
			if (!mcf.cap[0].get(i + 1)) {
				for (int j = 0; j < list.size(); ++j) {
					if (mcf.cap[N + 1 + j].get(i + 1)) {
						ri = j;
						break;
					}
				}
			}
			if (ri == -1) {
				ret[4 * source[i].idx + 0] = -1;
				ret[4 * source[i].idx + 1] = -1;
				ret[4 * source[i].idx + 2] = -1;
				ret[4 * source[i].idx + 3] = -1;
			} else {
				ret[4 * source[i].idx + 0] = list.get(ri).t;
				ret[4 * source[i].idx + 1] = list.get(ri).l;
				ret[4 * source[i].idx + 2] = list.get(ri).b - 1;
				ret[4 * source[i].idx + 3] = list.get(ri).r - 1;
			}
		}
	}

	class Rect {
		int t, b, l, r;
		int bestDiff = Integer.MAX_VALUE / 4;

		Rect(int t, int b, int l, int r) {
			this.t = t;
			this.b = b;
			this.l = l;
			this.r = r;
		}

		int h() {
			return this.b - this.t;
		}

		int w() {
			return this.r - this.l;
		}
	}

	int[] colsBuf = new int[128];
	int[] xl1sBuf = new int[128];
	int[] xl2sBuf = new int[128];

	class Image {
		int W, H, idx;
		int[][] rowSum;
		byte[][] data;
		double ave;
		byte[][][] cachedDataStrict;

		Image(int[] stream, int pos) {
			boolean target = pos == 0;
			this.H = stream[pos++];
			this.W = stream[pos++];
			this.data = new byte[H][W + 1];
			this.rowSum = new int[H][W + 1];
			if (!target) {
				this.cachedDataStrict = new byte[H * W][][];
			}
			for (int i = 0; i < H; ++i) {
				for (int j = 0; j < W; ++j) {
					data[i][j] = (byte) stream[pos++];
					rowSum[i][j + 1] = rowSum[i][j] + (data[i][j] & 0xFF);
				}
				ave += rowSum[i][W];
			}
			ave /= (H * W);
		}

		int calcStrict(int t, int b, int l, int r, int threshold) {
			if (b - t > H || r - l > W) return Integer.MAX_VALUE;
			int h = b - t;
			int w = r - l;
			double aveDiff = Math.abs(ave(t, b, l, r) - this.ave);
			if (Math.ceil(sq(aveDiff)) * h * w > threshold) return Integer.MAX_VALUE;
			int key = (h - 1) * W + (w - 1);
			int diff = 0;
			if (cachedDataStrict[key] != null) {
//				timer.start(9);
				for (int i = t; i < b; ++i) {
					byte[] d = cachedDataStrict[key][i - t];
					for (int j = l; j < r; ++j) {
						diff += sq(targetData[i][j] - (d[j - l] & 0xFF));
					}
				}
//				timer.stop(9);
			} else {
//				timer.start(10);
				cachedDataStrict[key] = new byte[h][w];
				int total = this.H * this.W;
				for (int i = 1; i <= w; ++i) {
					colsBuf[i] = i * this.W / w;
					xl1sBuf[i - 1] = (colsBuf[i - 1] + 1) * w - (i - 1) * this.W;
					xl2sBuf[i - 1] = i * this.W - colsBuf[i] * w;
				}
				for (int i = 0; i < h; ++i) {
					int ts = i * this.H;
					int bs = ts + this.H;
					int row1 = ts / h;
					for (int j = 0; j < w; ++j) {
						int v = 0;
						int y = ts;
						int row = row1;
						int yl = (row + 1) * h - ts;
						int col = colsBuf[j];
						int col2 = colsBuf[j + 1];
						int xl = xl1sBuf[j];
						int xl2 = xl2sBuf[j];
						while (true) {
							int pre = (data[row][col] & 0xFF) * xl;
							int post = (data[row][col2] & 0xFF) * xl2;
							v += (pre + post + (rowSum[row][col2] - rowSum[row][col + 1]) * w) * yl;
							y += yl;
							if (y == bs) {
								break;
							} else if (y + h > bs) {
								yl = bs - y;
							} else {
								yl = h;
							}
							++row;
						}
						int pix = (v + total / 2) / total;
						diff += sq(targetData[i + t][j + l] - pix);
						cachedDataStrict[key][i][j] = (byte) (pix);
					}
				}
//				timer.stop(10);
			}
			return diff;
		}

		int calcStrict(int t, int b, int l, int r) {
			return calcStrict(t, b, l, r, Integer.MAX_VALUE);
		}

		int calcStrict(Rect rect, boolean force) {
			double aveDiff = Math.abs(ave(rect) - this.ave);
			if (!force && sq(aveDiff) * rect.h() * rect.w() > rect.bestDiff * 1) return Integer.MAX_VALUE;
			int diff = 0;
			byte[][] data = scaleStrict(rect.b - rect.t, rect.r - rect.l);
			for (int i = rect.t; i < rect.b; ++i) {
				for (int j = rect.l; j < rect.r; ++j) {
					diff += sq(targetData[i][j] - (data[i - rect.t][j - rect.l] & 0xFF));
				}
			}
			if (!force && diff > rect.bestDiff * 1.1) {
				return Integer.MAX_VALUE;
			}
			rect.bestDiff = Math.min(rect.bestDiff, diff);
			return diff;
		}

		int calcStrict(Rect rect, int threshold) {
			double aveDiff = Math.abs(ave(rect) - this.ave);
			if (sq(aveDiff) * rect.h() * rect.w() > threshold * 1.03) return Integer.MAX_VALUE;
			int diff = 0;
			byte[][] data = scaleStrict(rect.b - rect.t, rect.r - rect.l);
			for (int i = rect.t; i < rect.b; ++i) {
				for (int j = rect.l; j < rect.r; ++j) {
					diff += sq(targetData[i][j] - (data[i - rect.t][j - rect.l] & 0xFF));
				}
				if (diff > threshold * 1.1) {
					return Integer.MAX_VALUE;
				}
			}
			//			cachedDiffStrict.put(rect, diff);
			return diff;
		}

		byte[][] scaleStrict(int h, int w) {
			int size = (h - 1) * W + (w - 1);
			if (cachedDataStrict[size] != null) {
				return cachedDataStrict[size];
			}
			cachedDataStrict[size] = new byte[h][w];
			int total = this.H * this.W;
			for (int i = 1; i <= w; ++i) {
				colsBuf[i] = i * this.W / w;
				xl1sBuf[i - 1] = (colsBuf[i - 1] + 1) * w - (i - 1) * this.W;
				xl2sBuf[i - 1] = i * this.W - colsBuf[i] * w;
			}
			for (int i = 0; i < h; ++i) {
				int ts = i * this.H;
				int bs = ts + this.H;
				byte[] d = cachedDataStrict[size][i];
				for (int j = 0; j < w; ++j) {
					int v = 0;
					int y = ts;
					int row = y / h;
					int yl = (row + 1) * h - ts;
					int col = colsBuf[j];
					int col2 = colsBuf[j + 1];
					int xl = xl1sBuf[j];
					int xl2 = xl2sBuf[j];
					while (y < bs) {
						int pre = (data[row][col] & 0xFF) * xl;
						int post = (xl2 == 0 ? 0 : (data[row][col2] & 0xFF) * xl2);
						v += (pre + post + (rowSum[row][col2] - rowSum[row][col + 1]) * w) * yl;
						y += yl;
						yl = y + h > bs ? bs - y : h;
						++row;
					}
					d[j] = (byte) ((v + total / 2) / total);
				}
			}
			return cachedDataStrict[size];
		}
	}

	static class MinCostFlow {
		int size;
		int[][] cost;
		BitSet[] cap;

		MinCostFlow(int size) {
			this.size = size;
			cost = new int[size][size];
			cap = new BitSet[size];
			for (int i = 0; i < size; ++i) {
				cap[i] = new BitSet(size);
			}
		}

		void addEdge(int from, int to, int c) {
			cost[from][to] = c;
			cap[from].set(to);
			cost[to][from] = -c;
		}

		int calc(int flow) {
			final int INF = Integer.MAX_VALUE;
			int total = 0;
			int[] h = new int[size];
			int[] prev = new int[size];
			int[] dist = new int[size];
			PriorityQueue<State> q = new PriorityQueue<State>();
			while (flow > 0) {
				Arrays.fill(dist, INF);
				dist[0] = 0;
				q.clear();
				q.add(new State(0, dist[0]));
				BitSet used = new BitSet(size);
				int count = 0;
				while (!q.isEmpty()) {
					State st = q.poll();
					if (used.get(st.v)) continue;
					used.set(st.v);
					++count;
					for (int i = cap[st.v].nextSetBit(0); i >= 0; i = cap[st.v].nextSetBit(i + 1)) {
						int nCost = dist[st.v] + cost[st.v][i] + h[st.v] - h[i];
						if (dist[i] > nCost) {
							dist[i] = nCost;
							q.add(new State(i, nCost));
							prev[i] = st.v;
						}
					}
					if (count == size) {
						break;
					}
				}
				if (dist[size - 1] == INF) {
					debug("cannot flow all:" + flow);
					return 1 << 30;
				}
				for (int i = 0; i < size; ++i) {
					h[i] += dist[i];
				}
				for (int pos = size - 1; pos != 0; pos = prev[pos]) {
					cap[prev[pos]].clear(pos);
					cap[pos].set(prev[pos]);
				}
				total += h[size - 1];
				flow -= 1;
			}
			return total;
		}

		static class State implements Comparable<State> {
			int v;
			int cost;

			public State(int v, int cost) {
				this.v = v;
				this.cost = cost;
			}

			public int compareTo(State o) {
				return this.cost - o.cost;
			}
		}
	}

	static int sq(int v) {
		return v * v;
	}

	static double sq(double v) {
		return v * v;
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;
		static final double TO_DOUBLE = 1.0 / (1L << 31);

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		boolean nextBoolean() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return (w & 8) == 0;
		}

		double nextDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return Math.abs(w * TO_DOUBLE);
		}

		double nextSDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w * TO_DOUBLE;
		}
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}
	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int len = sc.nextInt();
			int[] data = new int[len];
			for (int i = 0; i < len; ++i) {
				data[i] = sc.nextInt();
			}
			int[] ret = new CollageMaker().compose(data);
			for (int i = 0; i < ret.length; ++i) {
				System.out.println(ret[i]);
			}
			System.out.flush();
		}
	}
}