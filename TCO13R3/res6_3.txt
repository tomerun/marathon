seed:   1  N:206
totarea:2.1909
elapsed:3.005
score:  10.86391979530793

seed:   2  N:137
totarea:0.9795
elapsed:3.001
score:  2.28592695234821

seed:   3  N:351
totarea:1.4454
elapsed:3.001
score:  10.963660021538972

seed:   4  N:392
totarea:1.6589
elapsed:3.001
score:  15.504394779378487

seed:   5  N:134
totarea:1.0797
elapsed:3.001
score:  3.5598561564024624

seed:   6  N:486
totarea:3.4797
elapsed:3.001
score:  45.522159326579306

seed:   7  N:109
totarea:2.4838
elapsed:3.001
score:  8.444237588256067

seed:   8  N:125
totarea:3.0321
elapsed:3.001
score:  12.573927789273915

seed:   9  N:256
totarea:4.2324
elapsed:3.001
score:  28.882448809403012

seed:  10  N:83
totarea:0.9591
elapsed:3.001
score:  2.088166154534965

seed:  11  N:334
totarea:2.1529
elapsed:3.001
score:  15.987897017526175

seed:  12  N:390
totarea:2.1738
elapsed:3.001
score:  19.14815298255096

seed:  13  N:214
totarea:4.4407
elapsed:3.001
score:  30.24116706620906

seed:  14  N:183
totarea:4.4338
elapsed:3.001
score:  21.337656112835095

seed:  15  N:171
totarea:3.1020
elapsed:3.001
score:  15.087025478873363

seed:  16  N:414
totarea:2.1718
elapsed:3.001
score:  22.809760595824805

seed:  17  N:95
totarea:4.8832
elapsed:3.001
score:  10.016686609254533

seed:  18  N:51
totarea:2.8690
elapsed:3.001
score:  5.873803026741085

seed:  19  N:240
totarea:3.0184
elapsed:3.001
score:  21.29602532682695

seed:  20  N:242
totarea:2.6495
elapsed:3.001
score:  16.490601036812507

seed:  21  N:62
totarea:3.5702
elapsed:3.001
score:  6.13238344454292

seed:  22  N:226
totarea:2.4289
elapsed:3.001
score:  12.449074963354056

seed:  23  N:328
totarea:2.9485
elapsed:3.001
score:  22.872678041785978

seed:  24  N:52
totarea:2.7151
elapsed:3.001
score:  3.258156846318852

seed:  25  N:435
totarea:1.1621
elapsed:3.001
score:  8.361027914478683

seed:  26  N:325
totarea:2.2609
elapsed:3.001
score:  20.439311932729357

seed:  27  N:143
totarea:4.7558
elapsed:3.001
score:  18.024426098203882

seed:  28  N:50
totarea:5.3482
elapsed:3.001
score:  7.2393915260610395

seed:  29  N:420
totarea:2.9985
elapsed:3.001
score:  32.80764566085446

seed:  30  N:237
totarea:3.6526
elapsed:3.001
score:  24.11515858783341

seed:  31  N:145
totarea:1.6628
elapsed:3.001
score:  5.749198954340731

seed:  32  N:58
totarea:3.3770
elapsed:3.001
score:  4.8210592299075605

seed:  33  N:59
totarea:3.4322
elapsed:3.001
score:  6.414373353168509

seed:  34  N:158
totarea:2.7154
elapsed:3.001
score:  13.986836708642198

seed:  35  N:118
totarea:2.9640
elapsed:3.001
score:  9.21833587392191

seed:  36  N:104
totarea:1.9845
elapsed:3.001
score:  5.624297103940395

seed:  37  N:50
totarea:4.9938
elapsed:3.001
score:  7.401682126113921

seed:  38  N:146
totarea:5.9137
elapsed:3.001
score:  25.010204907037988

seed:  39  N:94
totarea:3.0035
elapsed:3.001
score:  7.864458818820271

seed:  40  N:57
totarea:3.8221
elapsed:3.001
score:  5.684761869506857

seed:  41  N:205
totarea:1.4738
elapsed:3.001
score:  7.565008487268665

seed:  42  N:362
totarea:1.6081
elapsed:3.001
score:  14.90509700138054

seed:  43  N:146
totarea:4.7215
elapsed:3.001
score:  15.19795843313146

seed:  44  N:353
totarea:2.8105
elapsed:3.001
score:  26.024316000296235

seed:  45  N:85
totarea:3.2044
elapsed:3.001
score:  7.085863877233712

seed:  46  N:489
totarea:2.4531
elapsed:3.001
score:  30.573319864404528

seed:  47  N:120
totarea:2.4736
elapsed:3.001
score:  9.257262325785897

seed:  48  N:50
totarea:3.7120
elapsed:3.001
score:  5.898714205497926

seed:  49  N:388
totarea:2.6864
elapsed:3.001
score:  24.37747715963475

seed:  50  N:356
totarea:5.3193
elapsed:3.001
score:  50.407373843328664

seed:  51  N:99
totarea:2.7002
elapsed:3.005
score:  7.939544431183247

seed:  52  N:60
totarea:2.5196
elapsed:3.001
score:  5.040984641829469

seed:  53  N:56
totarea:3.4311
elapsed:3.001
score:  6.413097347269315

seed:  54  N:69
totarea:4.8608
elapsed:3.001
score:  9.588434260208116

seed:  55  N:224
totarea:2.0191
elapsed:3.001
score:  10.04074886627154

seed:  56  N:182
totarea:2.7157
elapsed:3.001
score:  11.595047071014188

seed:  57  N:56
totarea:7.2571
elapsed:3.001
score:  13.335940529177465

seed:  58  N:255
totarea:5.0326
elapsed:3.001
score:  32.30960979883855

seed:  59  N:450
totarea:3.5381
elapsed:3.001
score:  44.93416186834568

seed:  60  N:88
totarea:2.5441
elapsed:3.001
score:  7.005458078830253

seed:  61  N:375
totarea:4.7083
elapsed:3.001
score:  43.34089248435597

seed:  62  N:175
totarea:2.1710
elapsed:3.001
score:  9.716599711103528

seed:  63  N:323
totarea:3.2725
elapsed:3.001
score:  26.528944178140375

seed:  64  N:52
totarea:2.8528
elapsed:3.001
score:  4.435359865464523

seed:  65  N:54
totarea:1.4014
elapsed:3.001
score:  1.4882285232992711

seed:  66  N:89
totarea:2.6503
elapsed:3.001
score:  5.820289081547146

seed:  67  N:69
totarea:5.9198
elapsed:3.001
score:  13.49514811893167

seed:  68  N:109
totarea:3.0145
elapsed:3.001
score:  8.182611236720764

seed:  69  N:62
totarea:5.0934
elapsed:3.001
score:  10.633780523661903

seed:  70  N:83
totarea:1.0793
elapsed:3.001
score:  2.4410408643798935

seed:  71  N:109
totarea:1.6114
elapsed:3.001
score:  5.043161546373689

seed:  72  N:275
totarea:2.1586
elapsed:3.001
score:  13.286588835218401

seed:  73  N:66
totarea:1.3790
elapsed:3.001
score:  2.487670739186747

seed:  74  N:288
totarea:1.3316
elapsed:3.001
score:  8.740015191765329

seed:  75  N:142
totarea:1.5019
elapsed:3.001
score:  4.550983449002296

seed:  76  N:120
totarea:2.0498
elapsed:3.001
score:  5.5897379935941816

seed:  77  N:445
totarea:5.0971
elapsed:3.001
score:  58.23513189038824

seed:  78  N:183
totarea:1.1962
elapsed:3.001
score:  4.375037740210873

seed:  79  N:66
totarea:1.5332
elapsed:3.001
score:  2.1877930670912704

seed:  80  N:261
totarea:1.6144
elapsed:3.001
score:  8.174610930395248

seed:  81  N:273
totarea:4.9120
elapsed:3.001
score:  37.15901424163304

seed:  82  N:483
totarea:4.7354
elapsed:3.001
score:  57.44070563777372

seed:  83  N:50
totarea:3.0838
elapsed:3.001
score:  4.711579189079117

seed:  84  N:76
totarea:5.3282
elapsed:3.001
score:  9.50996460779893

seed:  85  N:78
totarea:2.5924
elapsed:3.001
score:  6.030559025012874

seed:  86  N:160
totarea:5.1409
elapsed:3.001
score:  26.540837809721634

seed:  87  N:428
totarea:4.0850
elapsed:3.001
score:  48.062531755387795

seed:  88  N:424
totarea:2.6727
elapsed:3.001
score:  30.89612621513063

seed:  89  N:58
totarea:1.6781
elapsed:3.001
score:  2.389573706978903

seed:  90  N:77
totarea:3.1239
elapsed:3.001
score:  6.629099344153259

seed:  91  N:50
totarea:2.9055
elapsed:3.001
score:  4.312266392184446

seed:  92  N:469
totarea:4.7136
elapsed:3.001
score:  59.93446865324271

seed:  93  N:67
totarea:1.8114
elapsed:3.001
score:  3.4409927455080425

seed:  94  N:51
totarea:3.1163
elapsed:3.001
score:  4.769406503062585

seed:  95  N:68
totarea:2.6917
elapsed:3.001
score:  4.8773750974727355

seed:  96  N:440
totarea:2.0942
elapsed:3.001
score:  23.731379140472633

seed:  97  N:56
totarea:1.5833
elapsed:3.001
score:  2.6606374911686297

seed:  98  N:339
totarea:1.2728
elapsed:3.001
score:  9.43463403475804

seed:  99  N:224
totarea:3.6253
elapsed:3.001
score:  21.76205112265259

seed: 100  N:133
totarea:1.3381
elapsed:3.001
score:  3.9570509157750355

ave:15.129512342787695
