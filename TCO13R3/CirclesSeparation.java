import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public final class CirclesSeparation {
	private static final boolean SUBMIT = false;
	private static final long TIMELIMIT = SUBMIT ? 9940 : 3000;
	private static final long TIMELIMIT2 = SUBMIT ? 9940 : 3000;
	private static final boolean DEBUG = 1 == 0;
	private static final boolean MEASURE_TIME = 1 == 0;
	private static final double EPS = 1e-8;
	private static final double PI2 = 2 * Math.PI;
	private final XorShift rnd = new XorShift();
	private final long starttime = System.currentTimeMillis();
	int N;
	double[] ret;
	double[] x, y, ox, oy, r, m;
	double[] bestX, bestY;
	double cx, cy;
	int tryCount;
	double bestScore;
	double HEAT = 300;

	double[] minimumWork(double[] x, double[] y, double[] r, double[] m) {
		N = x.length;
		this.x = x.clone();
		this.y = y.clone();
		this.ox = x.clone();
		this.oy = y.clone();
		this.r = r.clone();
		this.m = m.clone();
		ret = new double[2 * N];
		bestX = new double[N];
		bestY = new double[N];
		posConsider = new int[N];
		for (int i = 0; i < N; ++i) {
			cx += x[i];
			cy += y[i];
		}
		cx /= N;
		cy /= N;
		solve();
		//if (SUBMIT) submarine();
		for (int i = 0; i < N; ++i) {
			ret[2 * i] = this.x[i];
			ret[2 * i + 1] = this.y[i];
		}
		return ret;
	}

	void solve() {
		bestScore = initial();
		doKinetics();
//		if (DEBUG) log("initial", bestScore, System.currentTimeMillis() - starttime);
//		annealing();
		//        globalTransform();
	}

	void doKinetics() {
		double exMin = 0.01;
		double exRatio = 0.1;
		for (int i = 0; i < N; ++i) {
			x[i] += (x[i] < cx ? -exMin : exMin) + (x[i] - cx) * exRatio;
			y[i] += (y[i] < cy ? -exMin : exMin) + (y[i] - cy) * exRatio;
		}
		double[] vx = new double[N];
		double[] vy = new double[N];
		double[] dvx = new double[N];
		double[] dvy = new double[N];
		double collisionRatio = 0.99;
		double kSpring = 1;
		final long kinStart = System.currentTimeMillis();
		final long TL = (TIMELIMIT - (kinStart - starttime)) * 4 / 5;
		double timespan = 0.001;
		for (int i = 0; i < N; ++i) {
			System.out.println(x[i] + " " + y[i]);
		}
		for (int turn = 1;; ++turn) {
			if ((turn & ((1 << 9) - 1)) == 0) {
				long elapsed = System.currentTimeMillis() - kinStart;
				if (elapsed > TL) {
					log("bestScore", bestScore);
					log("turn", turn);
					break;
				}
				for (int i = 0; i < N; ++i) {
					System.out.println(x[i] + " " + y[i]);
				}
				//                log(turn, timespan);
				//                for (int i = 0; i < N; ++i) {
				//                    dvx[i] += rnd.nextSDouble() * rndRatio;
				//                    dvy[i] += rnd.nextSDouble() * rndRatio;
				//                }
				//                timespan *= 0.99;
				//                collisionRatio *= 0.9999;
				//                kSpring *= 0.99;
				//                rndRatio *= 0.99;
				//                for (int i = 0; i < N; ++i) {
				//                    x[i] += (x[i] < cx ? -exMin : exMin) + (x[i] - cx) * exRatio;
				//                    y[i] += (y[i] < cy ? -exMin : exMin) + (y[i] - cy) * exRatio;
				//                }
				//                exMin *= 0.9;
				//                exRatio *= 0.9;
				if (elapsed > TL - TL / 16) {
					//                    rndRatio = 0.;
					//                    timespan *= 0.5;
					//                    collisionRatio *= 2;
				}
			}
			for (int i = 0; i < N; ++i) {
				vx[i] += dvx[i] / 2;
				vy[i] += dvy[i] / 2;
				x[i] += vx[i] * timespan;
				y[i] += vy[i] * timespan;
				vx[i] += dvx[i] / 2;
				vy[i] += dvy[i] / 2;
				vx[i] *= 0.9999;
				vy[i] *= 0.9999;
				double dx = ox[i] - x[i];
				double dy = oy[i] - y[i];
				double norm = dx * dx + dy * dy;
				if (norm < EPS) continue;
				norm = 1.0 / Math.sqrt(norm) * timespan * 0.01;
				dvx[i] = dx * norm;
				dvy[i] = dy * norm;
				//                dvx[i] += rnd.nextSDouble() * rndRatio;
				//                dvy[i] += rnd.nextSDouble() * rndRatio;
			}
			for (int i = 0; i < N; ++i) {
				for (int j = 0; j < i; ++j) {
					double dx = x[j] - x[i];
					double dy = y[j] - y[i];
					double d2 = dx * dx + dy * dy;
					if (d2 > sq(r[i] + r[j] + EPS)) continue;
					double d1 = Math.sqrt(d2);
					double norm = 1.0 / d1;
					dx *= norm;
					dy *= norm;
					double rv1 = vx[i] * dx + vy[i] * dy;
					double rv2 = vx[j] * dx + vy[j] * dy;
					double dv = rv1 - rv2;
					double len = r[i] + r[j] - d1;
					double nv1 = -len * kSpring / m[i];
					double nv2 = len * kSpring / m[j];
					if (dv > 0) {
						double mul = 2.0 / (m[i] + m[j]) * dv * collisionRatio;
						nv1 += -mul * m[j];
						nv2 += mul * m[i];
					}
					//                    if (dv < 0) continue;
					//                    nv1 = -dv / 2;
					//                    nv2 = dv / 2;
					dvx[i] += dx * nv1;
					dvy[i] += dy * nv1;
					dvx[j] += dx * nv2;
					dvy[j] += dy * nv2;
				}
			}
		}
	}

	int[] order;

	double solveBeam() {
		order = new int[N];
		Circle[] cs = new Circle[N];
		for (int i = 0; i < N; ++i) {
			cs[i] = new Circle(i);
			x[i] = ox[i];
			y[i] = oy[i];
		}
		Arrays.sort(cs);
		for (int i = 0; i < N; ++i) {
			order[i] = cs[i].i;
		}
		final int BEAMWIDTH = 1000;
		ArrayList<World> worlds = new ArrayList<World>();
		World start = new World();
		start.set(0, ox[order[0]], oy[order[0]]);
		worlds.add(start);
		for (int turn = 1; turn < N; ++turn) {
			ArrayList<Move> next = new ArrayList<Move>();
			for (World w : worlds) {
				w.next(turn, next);
			}

			if (next.size() > BEAMWIDTH) Collections.sort(next);
			if (next.size() > BEAMWIDTH) {
				for (int i = 0; i < BEAMWIDTH / 3; ++i) {
					int from = rnd.nextInt(BEAMWIDTH - BEAMWIDTH / 16) + BEAMWIDTH / 16;
					int to = rnd.nextInt(Math.min(BEAMWIDTH * 5, next.size() - BEAMWIDTH)) + BEAMWIDTH;
					Move tmp = next.get(from);
					next.set(from, next.get(to));
					next.set(to, tmp);
				}
			}
			worlds.clear();
			for (int i = 0; i < BEAMWIDTH && i < next.size(); ++i) {
				worlds.add(next.get(i).parent.getNext(turn, next.get(i)));
			}
			log(turn, worlds.get(0).score);
		}
		for (int i = 0; i < N; ++i) {
			int idx = order[i];
			bestX[idx] = x[idx] = worlds.get(0).x[i];
			bestY[idx] = y[idx] = worlds.get(0).y[i];
		}
		return worlds.get(0).score;
	}

	class World implements Comparable<World> {
		double[] x, y;
		double score;

		World() {
			x = new double[N];
			y = new double[N];
		}

		World(World o) {
			x = o.x.clone();
			y = o.y.clone();
			score = o.score;
		}

		void next(int turn, ArrayList<Move> next) {
			int idx = order[turn];
			ArrayList<Pos> pos = new ArrayList<Pos>();
			boolean any = false;
			for (int i = 0; i < turn; ++i) {
				int oi = order[i];
				double dist = dist(this.x[i], this.y[i], ox[idx], oy[idx]);
				if (dist > r[oi] + r[idx]) continue;
				any = true;
				double dx = ox[idx] - this.x[i];
				double dy = oy[idx] - this.y[i];
				double norm = Math.sqrt(dx * dx + dy * dy);
				double px = this.x[i] + dx * (r[oi] + r[idx] + EPS) / norm;
				double py = this.y[i] + dy * (r[oi] + r[idx] + EPS) / norm;
				boolean ok = true;
				for (int j = 0; j < turn; ++j) {
					if (j == i) continue;
					if (dist2(px, py, this.x[j], this.y[j]) < sq(r[order[j]] + r[idx] + EPS)) {
						ok = false;
						break;
					}
				}
				if (ok) {
					pos.add(new Pos(px, py, r[oi] + r[idx] + EPS - dist));
				}
			}
			if (!any) {
				next.add(new Move(ox[idx], oy[idx], this.score, this));
				return;
			}
			for (int i = 0; i < turn; ++i) {
				int ii = order[i];
				for (int j = i + 1; j < turn; ++j) {
					int ji = order[j];
					double l = dist(this.x[i], this.y[i], this.x[j], this.y[j]);
					double r1 = r[ii] + r[idx];
					double r2 = r[ji] + r[idx];
					if (l >= r1 + r2) continue;
					double dx = this.x[j] - this.x[i];
					double dy = this.y[j] - this.y[i];
					double norm = Math.sqrt(dx * dx + dy * dy);
					dx /= norm;
					dy /= norm;
					double g1 = (l * l + r1 * r1 - r2 * r2) / (2 * l);
					double g2 = Math.sqrt(r1 * r1 - g1 * g1) + 0.0001;
					double px1 = this.x[i] + dx * g1 + dy * g2;
					double py1 = this.y[i] + dy * g1 - dx * g2;
					double px2 = this.x[i] + dx * g1 - dy * g2;
					double py2 = this.y[i] + dy * g1 + dx * g2;
					{
						boolean ok = true;
						for (int k = 0; k < turn; ++k) {
							if (k == j || k == i) continue;
							if (dist2(px1, py1, this.x[k], this.y[k]) < sq(r[order[k]] + r[idx] + EPS)) {
								ok = false;
								break;
							}
						}
						if (ok) {
							pos.add(new Pos(px1, py1, dist(px1, py1, ox[idx], oy[idx])));
						}
					}
					{
						boolean ok = true;
						for (int k = 0; k < turn; ++k) {
							if (k == j || k == i) continue;
							if (dist2(px2, py2, this.x[k], this.y[k]) < sq(r[order[k]] + r[idx] + EPS)) {
								ok = false;
								break;
							}
						}
						if (ok) {
							pos.add(new Pos(px2, py2, dist(px2, py2, ox[idx], oy[idx])));
						}
					}
				}
			}

			Collections.sort(pos);
			for (int i = 0; i < Math.min(3, pos.size()); ++i) {
				Pos p = pos.get(i);
				double score = p.len * m[idx];
				next.add(new Move(p.x, p.y, score + this.score, this));
			}
		}

		World getNext(int turn, Move move) {
			World ret = new World(this);
			ret.x[turn] = move.x;
			ret.y[turn] = move.y;
			ret.score = move.score;
			return ret;
		}

		void set(int idx, double xv, double yv) {
			x[idx] = xv;
			y[idx] = yv;
			score += dist(xv, yv, ox[order[idx]], oy[order[idx]]);
		}

		public int compareTo(World o) {
			return Double.compare(this.score, o.score);
		}

	}

	static class Move implements Comparable<Move> {
		double x, y, score;
		World parent;

		public Move(double x, double y, double score, World parent) {
			this.x = x;
			this.y = y;
			this.score = score;
			this.parent = parent;
		}

		public int compareTo(Move o) {
			return Double.compare(this.score, o.score);
		}
	}

	static class Pos implements Comparable<Pos> {
		double x, y, len;

		public Pos(double x, double y, double len) {
			this.x = x;
			this.y = y;
			this.len = len;
		}

		public int compareTo(Pos o) {
			return Double.compare(this.len, o.len);
		}
	}

	void annealing() {
		double[] cost = new double[N];
		double tcost = 0;
		for (int i = 0; i < N; ++i) {
			cost[i] = dist(x[i], y[i], ox[i], oy[i]) * m[i];
			tcost += cost[i];
		}
		bestScore = tcost;
		log("bestScore SA start", tcost);
		double moveW = 0.005;
		long shrink = TIMELIMIT / 16;
		double margin = 0.1;
		int nextConsider = (int) (margin / moveW * N / 1.5);
		int[][] consider = new int[N][N];
		calcConsider(consider, margin);
		int p = 0;
		final long annealStart = System.currentTimeMillis();
		final long TL = TIMELIMIT - (annealStart - starttime);
		for (int turn = 1;; ++turn, ++p) {
			//            if ((turn & ((1 << 16) - 1)) == 0) {
			//                for (int i = 0; i < N; ++i) {
			//                    System.out.println(bestX[i] + " " + bestY[i]);
			//                }
			//            }
			if ((turn & ((1 << 12) - 1)) == 0) {
				long elapsed = System.currentTimeMillis() - annealStart;
				if (elapsed > TL) {
					log("bestScore", bestScore);
					log("turn", turn);
					break;
				}
				if (elapsed > shrink) {
					HEAT *= 1.2;
					if (elapsed >= TL - TL / 16) {
						HEAT *= 100;
					}
					//                    tcost = 0;
					//                    for (int i = 0; i < N; ++i) {
					//                        x[i] += (x[i] < cx ? -exMin : exMin) + (x[i] - cx) * exRatio;
					//                        y[i] += (y[i] < cy ? -exMin : exMin) + (y[i] - cy) * exRatio;
					//                        cost[i] = dist(x[i], y[i], ox[i], oy[i]) * m[i];
					//                        tcost += cost[i];
					//                    }
					//                    exMin *= 0.5;
					//                    exRatio *= 0.5;
					moveW *= 0.97;
					shrink += TL / 16;
					log("turn", turn, "move", moveW, "heat", HEAT, "score", bestScore, tcost);
				}
			}
			if (turn == nextConsider) {
				nextConsider += (int) (margin / moveW * N / 1.5);
				calcConsider(consider, margin);
			}
			if (p == N) p = 0;
			double nx = x[p] + rnd.nextSDouble() * moveW;
			double ny = y[p] + rnd.nextSDouble() * moveW;
			boolean ok = true;
			for (int j = 0; consider[p][j] != -1; ++j) {
				if (overlap(consider[p][j], nx, ny, r[p])) {
					ok = false;
					break;
				}
			}
			if (!ok) continue;
			double c = dist(ox[p], oy[p], nx, ny) * m[p];
			double diff = c - cost[p];
			if (transit(diff)) {
				tcost += diff;
				cost[p] = c;
				x[p] = nx;
				y[p] = ny;
				if (tcost < bestScore) {
					bestScore = tcost;
					//                    log("bestScore", bestScore);
					for (int i = 0; i < N; ++i) {
						bestX[i] = x[i];
						bestY[i] = y[i];
					}
				}
			}
		}
		x = bestX.clone();
		y = bestY.clone();
	}

	int[] posConsider;

	void calcConsider(int[][] idx, double margin) {
		Arrays.fill(posConsider, 0);
		for (int i = 0; i < N; ++i) {
			double left = x[i] - r[i] - margin;
			double right = x[i] + r[i] + margin;
			double top = y[i] - r[i] - margin;
			double bottom = y[i] + r[i] + margin;
			for (int j = i + 1; j < N; ++j) {
				if (x[j] + r[j] > left && right > x[j] - r[j] && y[j] + r[j] > top && bottom > y[j] - r[j]) {
					idx[i][posConsider[i]++] = j;
					idx[j][posConsider[j]++] = i;
				}
			}
		}
		for (int i = 0; i < N; ++i) {
			idx[i][posConsider[i]] = -1;
		}
	}

	static class Range implements Comparable<Range> {
		double s, e;

		public Range(double s, double e) {
			this.s = s;
			this.e = e;
		}

		public int compareTo(Range o) {
			if (this.s != o.s) return Double.compare(this.s, o.s);
			return -Double.compare(this.e, o.e);
		}
	}

	double initial() {
		Circle[] cs = new Circle[N];
		for (int i = 0; i < N; ++i) {
			cs[i] = new Circle(i);
			x[i] = ox[i];
			y[i] = oy[i];
		}

		double paramMA = 5;
		double paramOA = -3;
		double paramCA = -15;
		double paramRA = N < 75 ? -10 : N < 140 ? -40 : -100;
		final double paramMV = 5;
		final double paramOV = 2;
		final double paramCV = 8;
		final double paramRV = paramRA / 2.5;
		double paramM = paramMA;
		double paramO = paramOA;
		double paramC = paramCA;
		double paramR = paramRA;
		for (int i = 0; i < N; ++i) {
			cs[i].setPriority(paramM, paramO, paramC, paramR);
		}
		Arrays.sort(cs);
		int[] ord = new int[N];
		for (int i = 0; i < N; ++i) {
			ord[i] = cs[i].i;
		}
		double best = initial_(ord, Double.MAX_VALUE, 0);
		for (int i = 0; i < N; ++i) {
			bestX[i] = x[i];
			bestY[i] = y[i];
		}
		log("init score", best);
		Random rng = new Random(42);
		for (int turn = 1;; ++turn) {
			if (System.currentTimeMillis() - starttime > TIMELIMIT / 2) {
				log("init count", turn);
				break;
			}
			paramM = rng.nextGaussian() * paramMV + paramMA;
			paramM = Math.max(paramM, 1);
			paramO = rng.nextGaussian() * paramOV + paramOA;
			paramC = rng.nextGaussian() * paramCV + paramCA;
			paramR = rng.nextGaussian() * paramRV + paramRA;
			for (int i = 0; i < N; ++i) {
				cs[i].setPriority(paramM, paramO, paramC, paramR);
			}
			Arrays.sort(cs);
			for (int i = 0; i < N; ++i) {
				ord[i] = cs[i].i;
			}

			//            int p = rnd.nextInt(N);
			//            int q = rnd.nextInt(N - 1);
			//            if (q >= p) ++q;
			//            {
			//                int tmp = ord[p];
			//                ord[p] = ord[q];
			//                ord[q] = tmp;
			//            }
			double score = initial_(ord, best, 0);
			//            log(score);
			if (score < best) {
				//                paramMA = paramM;
				//                paramOA = paramO;
				//                paramCA = paramC;
				//                paramRA = paramR;
				best = score;
				log("init score", turn, best);
				for (int j = 0; j < N; ++j) {
					bestX[j] = x[j];
					bestY[j] = y[j];
				}
				//            } else {
				//                int tmp = ord[p];
				//                ord[p] = ord[q];
				//                ord[q] = tmp;
			}
		}
		for (int i = 0; i < N; ++i) {
			x[i] = bestX[i];
			y[i] = bestY[i];
		}
		return best;
	}

	double initial_(int[] ord, double best, int init) {
		double totalCost = 0;
		double[] ls = new double[N];
		double[] angles = new double[N];
		for (int i = 0; i < init; ++i) {
			x[ord[i]] = bestX[ord[i]];
			y[ord[i]] = bestY[ord[i]];
			totalCost += dist(ox[ord[i]], oy[ord[i]], x[ord[i]], y[ord[i]]) * m[ord[i]];
		}
		for (int i = init; i < N; ++i) {
			final int idx = ord[i];
			double upper = 0;
			for (int j = 0; j < i; ++j) {
				upper = Math.max(upper, (r[idx] + r[ord[j]] + EPS) - dist(ox[idx], oy[idx], x[ord[j]], y[ord[j]]));
			}
			if (upper == 0) {
				x[ord[i]] = ox[ord[i]];
				y[ord[i]] = oy[ord[i]];
				continue;
			}

			for (int j = 0; j < i; ++j) {
				int ji = ord[j];
				ls[j] = dist(ox[idx], oy[idx], x[ji], y[ji]);
				angles[j] = Math.atan2(y[ji] - oy[idx], x[ji] - ox[idx]);
				if (angles[j] < 0) angles[j] += PI2;
			}
			for (double r1 = upper + 0.001;; r1 += 0.01) {
				ArrayList<Range> ranges = new ArrayList<Range>();
				for (int j = 0; j < i; ++j) {
					int ji = ord[j];
					double l = ls[j];
					double r2 = r[idx] + r[ji] + EPS;
					if (l > r1 + r2) continue;
					if (r1 > l + r2) continue;
					double theta = Math.acos((l * l + r1 * r1 - r2 * r2) / (2 * l * r1));
					double angle = angles[j];
					double start = angle - theta;
					double end = angle + theta;
					if (start < 0) {
						ranges.add(new Range(0.0, end));
						ranges.add(new Range(start + PI2, PI2));
					} else if (end > PI2) {
						ranges.add(new Range(0, end - PI2));
						ranges.add(new Range(start, PI2));
					} else {
						ranges.add(new Range(start, end));
					}
				}
				Collections.sort(ranges);
				double pos = -1;
				if (ranges.isEmpty()) {
					pos = 0;
				} else if (ranges.get(0).s != 0) {
					pos = ranges.get(0).s / 2;
				} else {
					double cur = 0;
					for (int j = 0; j < ranges.size(); ++j) {
						if (cur < ranges.get(j).s) {
							pos = (cur + ranges.get(j).s) / 2;
							break;
						}
						cur = Math.max(cur, ranges.get(j).e);
					}
				}
				if (pos == -1) continue;
				//                log((int)Math.round((r1 - upper - 0.001) * 100));
				double dx = r1 * Math.cos(pos);
				double dy = r1 * Math.sin(pos);
				x[idx] = ox[idx] + dx;
				y[idx] = oy[idx] + dy;
				for (int j = 0; j < 100; ++j) {
					double mx = rnd.nextSDouble() * 0.001;
					double my = rnd.nextSDouble() * 0.001;
					if ((ox[idx] - x[idx]) * mx < 0) mx *= -1;
					if ((oy[idx] - y[idx]) * my < 0) my *= -1;
					boolean ok = true;
					for (int k = 0; k < i; ++k) {
						if (overlap(ord[k], x[idx] + mx, y[idx] + my, r[idx])) {
							ok = false;
							break;
						}
					}
					if (ok) {
						x[idx] += mx;
						y[idx] += my;
					}
				}
				totalCost += dist(ox[idx], oy[idx], x[idx], y[idx]) * m[idx];
				if (totalCost > best) return totalCost;
				break;
			}
		}
		return totalCost;
	}

	boolean transit(double diff) {
		if (diff <= 0) return true;
		return rnd.nextDouble() < exp(-diff * HEAT);
	}

	double exp(double p) {
		return 1.0 / (1 - p + p * p * 0.5 - p * p * p * 0.15);
	}

	void globalTransform() {
		double gdx = 0;
		double gdy = 0;
		double MOVE = 0.01;
		for (int turn = 1;; ++turn) {
			if ((turn & ((1 << 4) - 1)) == 0 && System.currentTimeMillis() - starttime > TIMELIMIT2) {
				log("turn2", turn);
				break;
			}
			double dx = (rnd.nextDouble() - 0.5) * MOVE;
			double dy = (rnd.nextDouble() - 0.5) * MOVE;
			double score = 0;
			for (int i = 0; i < N; ++i) {
				score += dist(x[i] + gdx + dx, y[i] + gdy + dy, ox[i], oy[i]) * m[i];
			}
			if (score < bestScore) {
				bestScore = score;
				gdx += dx;
				gdy += dy;
				log("bestScore", bestScore);
			}
		}
		for (int i = 0; i < N; ++i) {
			x[i] += gdx;
			y[i] += gdy;
		}
	}

	// make score 5% worse
	void submarine() {
		double target = bestScore * 1.05;
		double r = 2;
		double l = 0;
		while (true) {
			double mid = (r + l) / 2;
			double score = 0;
			for (int i = 0; i < N; ++i) {
				score += dist(x[i] + mid, y[i], ox[i], oy[i]) * m[i];
			}
			double diff = score - target;
			if (Math.abs(diff) < 1e-5 || r - l < 1e-9) {
				log("move", mid);
				log("target", target);
				log("adjusted score", score);
				for (int i = 0; i < N; ++i) {
					x[i] += mid;
				}
				break;
			}
			if (diff < 0) {
				l = mid;
			} else {
				r = mid;
			}
		}
	}

	boolean overlap(int i, int j) {
		return dist2(x[i], y[i], x[j], y[j]) < sq(r[i] + r[j] + EPS);
	}

	boolean overlap(int i, double xp, double yp, double rp) {
		return dist2(x[i], y[i], xp, yp) < sq(r[i] + rp + EPS);
	}

	boolean overlap(int i, double xp, double yp, double rp, double margin) {
		return dist2(x[i], y[i], xp, yp) < sq(r[i] + rp + EPS + margin);
	}

	double dist(double x1, double y1, double x2, double y2) {
		return Math.sqrt(dist2(x1, y1, x2, y2));
	}

	double dist2(double x1, double y1, double x2, double y2) {
		return sq(x1 - x2) + sq(y1 - y2);
	}

	static double sq(double v) {
		return v * v;
	}

	final class Circle implements Comparable<Circle> {
		int i;
		double priority;
		double oc;

		public Circle(int i) {
			this.i = i;
			for (int j = 0; j < N; ++j) {
				if (j == i) continue;
				double len = r[i] + r[j] - dist(x[i], y[i], x[j], y[j]);
				if (len > 0) oc += len * m[j];
			}
		}

		void setPriority(double paramM, double paramO, double paramC, double paramR) {
			this.priority = paramM * m[i] + paramO * oc + paramC * dist(x[i], y[i], cx, cy) + paramR * Math.pow(r[i], 2);
		}

		public int compareTo(Circle o) {
			return -Double.compare(this.priority, o.priority);
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		double[] x = new double[N];
		double[] y = new double[N];
		double[] r = new double[N];
		double[] m = new double[N];
		for (int i = 0; i < N; ++i) {
			x[i] = sc.nextDouble();
		}
		for (int i = 0; i < N; ++i) {
			y[i] = sc.nextDouble();
		}
		for (int i = 0; i < N; ++i) {
			r[i] = sc.nextDouble();
		}
		for (int i = 0; i < N; ++i) {
			m[i] = sc.nextDouble();
		}
		double[] ret = new CirclesSeparation().minimumWork(x, y, r, m);
		for (int i = 0; i < N * 2; ++i) {
			System.out.println(ret[i]);
		}
		System.out.flush();
	}

	static void log(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void log(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}

	}

	static final class Counter {
		ArrayList<Integer> count = new ArrayList<Integer>();

		void add(int i) {
			if (DEBUG) {
				while (count.size() <= i) {
					count.add(0);
				}
				count.set(i, count.get(i) + 1);
			}
		}

		void print() {
			if (DEBUG) {
				System.err.print("[");
				for (int i = 0; i < count.size(); ++i) {
					System.err.print(count.get(i) + ", ");
				}
				System.err.println("]");
			}
		}

	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;
		static final double TO_DOUBLE = 1.0 / (1L << 31);

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		boolean nextBoolean() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return (w & 8) == 0;
		}

		double nextDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return Math.abs(w * TO_DOUBLE);
		}

		double nextSDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w * TO_DOUBLE;
		}

	}

}