seed:   1  N:206
totarea:2.1909
elapsed:1.501
score:  11.109847084252525

seed:   2  N:137
totarea:0.9795
elapsed:1.501
score:  2.300937027354186

seed:   3  N:351
totarea:1.4454
elapsed:1.501
score:  11.227277608365807

seed:   4  N:392
totarea:1.6589
elapsed:1.501
score:  15.757223757185173

seed:   5  N:134
totarea:1.0797
elapsed:1.501
score:  3.661563038178678

seed:   6  N:486
totarea:3.4797
elapsed:1.501
score:  46.15869637869955

seed:   7  N:109
totarea:2.4838
elapsed:1.501
score:  8.46285087916692

seed:   8  N:125
totarea:3.0321
elapsed:1.501
score:  12.912920537836824

seed:   9  N:256
totarea:4.2324
elapsed:1.501
score:  28.887133542066028

seed:  10  N:83
totarea:0.9591
elapsed:1.501
score:  2.311166209396574

seed:  11  N:334
totarea:2.1529
elapsed:1.501
score:  16.313467782690882

seed:  12  N:390
totarea:2.1738
elapsed:1.501
score:  19.468087444306068

seed:  13  N:214
totarea:4.4407
elapsed:1.501
score:  30.382952827861125

seed:  14  N:183
totarea:4.4338
elapsed:1.501
score:  21.446862778437552

seed:  15  N:171
totarea:3.1020
elapsed:1.501
score:  15.304335258462915

seed:  16  N:414
totarea:2.1718
elapsed:1.501
score:  23.12369547685286

seed:  17  N:95
totarea:4.8832
elapsed:1.501
score:  9.913813337275055

seed:  18  N:51
totarea:2.8690
elapsed:1.501
score:  6.040860841664185

seed:  19  N:240
totarea:3.0184
elapsed:1.501
score:  21.524384243390003

seed:  20  N:242
totarea:2.6495
elapsed:1.501
score:  16.737684181667674

seed:  21  N:62
totarea:3.5702
elapsed:1.501
score:  6.1504112397005875

seed:  22  N:226
totarea:2.4289
elapsed:1.501
score:  12.630434848173005

seed:  23  N:328
totarea:2.9485
elapsed:1.501
score:  23.4059202319097

seed:  24  N:52
totarea:2.7151
elapsed:1.501
score:  3.278247664612876

seed:  25  N:435
totarea:1.1621
elapsed:1.501
score:  8.449145990364817

seed:  26  N:325
totarea:2.2609
elapsed:1.501
score:  20.684912300783655

seed:  27  N:143
totarea:4.7558
elapsed:1.501
score:  18.291861262090737

seed:  28  N:50
totarea:5.3482
elapsed:1.501
score:  7.286756897007042

seed:  29  N:420
totarea:2.9985
elapsed:1.501
score:  33.34684186360541

seed:  30  N:237
totarea:3.6526
elapsed:1.501
score:  24.35437583929317

seed:  31  N:145
totarea:1.6628
elapsed:1.501
score:  5.712615783639067

seed:  32  N:58
totarea:3.3770
elapsed:1.501
score:  4.960509139362387

seed:  33  N:59
totarea:3.4322
elapsed:1.501
score:  6.446619449903281

seed:  34  N:158
totarea:2.7154
elapsed:1.501
score:  13.937335710790347

seed:  35  N:118
totarea:2.9640
elapsed:1.501
score:  9.253134888395945

seed:  36  N:104
totarea:1.9845
elapsed:1.501
score:  5.741575203869094

seed:  37  N:50
totarea:4.9938
elapsed:1.501
score:  7.322937010654549

seed:  38  N:146
totarea:5.9137
elapsed:1.501
score:  25.411261918623744

seed:  39  N:94
totarea:3.0035
elapsed:1.501
score:  8.119939621612303

seed:  40  N:57
totarea:3.8221
elapsed:1.501
score:  5.667322373948481

seed:  41  N:205
totarea:1.4738
elapsed:1.501
score:  7.760125106801692

seed:  42  N:362
totarea:1.6081
elapsed:1.501
score:  15.160881828879708

seed:  43  N:146
totarea:4.7215
elapsed:1.501
score:  15.488063641593204

seed:  44  N:353
totarea:2.8105
elapsed:1.501
score:  26.36022252219857

seed:  45  N:85
totarea:3.2044
elapsed:1.501
score:  7.100282857691931

seed:  46  N:489
totarea:2.4531
elapsed:1.501
score:  31.16483743297629

seed:  47  N:120
totarea:2.4736
elapsed:1.501
score:  9.750004312052365

seed:  48  N:50
totarea:3.7120
elapsed:1.501
score:  6.125692977286123

seed:  49  N:388
totarea:2.6864
elapsed:1.501
score:  24.47883044364648

seed:  50  N:356
totarea:5.3193
elapsed:1.501
score:  50.79436023730702

seed:  51  N:99
totarea:2.7002
elapsed:1.505
score:  8.007213343082375

seed:  52  N:60
totarea:2.5196
elapsed:1.501
score:  5.082044130017986

seed:  53  N:56
totarea:3.4311
elapsed:1.501
score:  6.482479888833312

seed:  54  N:69
totarea:4.8608
elapsed:1.501
score:  9.766221413119629

seed:  55  N:224
totarea:2.0191
elapsed:1.501
score:  10.400291598195153

seed:  56  N:182
totarea:2.7157
elapsed:1.501
score:  11.7825658961442

seed:  57  N:56
totarea:7.2571
elapsed:1.501
score:  13.727999779695212

seed:  58  N:255
totarea:5.0326
elapsed:1.501
score:  32.78838288858082

seed:  59  N:450
totarea:3.5381
elapsed:1.501
score:  45.17676262122065

seed:  60  N:88
totarea:2.5441
elapsed:1.501
score:  7.154313595938697

seed:  61  N:375
totarea:4.7083
elapsed:1.501
score:  43.98061186410588

seed:  62  N:175
totarea:2.1710
elapsed:1.501
score:  10.128600810777366

seed:  63  N:323
totarea:3.2725
elapsed:1.501
score:  26.733397432166548

seed:  64  N:52
totarea:2.8528
elapsed:1.501
score:  4.540484986186515

seed:  65  N:54
totarea:1.4014
elapsed:1.501
score:  1.5003803621918728

seed:  66  N:89
totarea:2.6503
elapsed:1.501
score:  5.840763533795114

seed:  67  N:69
totarea:5.9198
elapsed:1.501
score:  13.793126512941116

seed:  68  N:109
totarea:3.0145
elapsed:1.501
score:  8.203954261486091

seed:  69  N:62
totarea:5.0934
elapsed:1.501
score:  10.650500433116715

seed:  70  N:83
totarea:1.0793
elapsed:1.501
score:  2.6353196795997333

seed:  71  N:109
totarea:1.6114
elapsed:1.501
score:  5.141461975246546

seed:  72  N:275
totarea:2.1586
elapsed:1.501
score:  13.3455081668878

seed:  73  N:66
totarea:1.3790
elapsed:1.501
score:  2.5027691433725945

seed:  74  N:288
totarea:1.3316
elapsed:1.501
score:  8.886120334836813

seed:  75  N:142
totarea:1.5019
elapsed:1.501
score:  4.703868104370227

seed:  76  N:120
totarea:2.0498
elapsed:1.501
score:  5.850965914514698

seed:  77  N:445
totarea:5.0971
elapsed:1.501
score:  58.407201366122756

seed:  78  N:183
totarea:1.1962
elapsed:1.501
score:  4.536915456391259

seed:  79  N:66
totarea:1.5332
elapsed:1.501
score:  2.3352825218952415

seed:  80  N:261
totarea:1.6144
elapsed:1.501
score:  8.29798285734943

seed:  81  N:273
totarea:4.9120
elapsed:1.501
score:  37.34609742501198

seed:  82  N:483
totarea:4.7354
elapsed:1.501
score:  57.87677085761859

seed:  83  N:50
totarea:3.0838
elapsed:1.501
score:  4.771886210110237

seed:  84  N:76
totarea:5.3282
elapsed:1.501
score:  9.850815584536328

seed:  85  N:78
totarea:2.5924
elapsed:1.501
score:  6.032841747407572

seed:  86  N:160
totarea:5.1409
elapsed:1.501
score:  26.71484701172173

seed:  87  N:428
totarea:4.0850
elapsed:1.501
score:  48.446043812558294

seed:  88  N:424
totarea:2.6727
elapsed:1.501
score:  31.81693330820349

seed:  89  N:58
totarea:1.6781
elapsed:1.501
score:  2.3915057305238823

seed:  90  N:77
totarea:3.1239
elapsed:1.501
score:  7.010033273405306

seed:  91  N:50
totarea:2.9055
elapsed:1.501
score:  4.341787160563969

seed:  92  N:469
totarea:4.7136
elapsed:1.501
score:  60.55906441998738

seed:  93  N:67
totarea:1.8114
elapsed:1.501
score:  3.430974046106893

seed:  94  N:51
totarea:3.1163
elapsed:1.501
score:  4.837014734790174

seed:  95  N:68
totarea:2.6917
elapsed:1.501
score:  5.162667897566661

seed:  96  N:440
totarea:2.0942
elapsed:1.501
score:  24.44475498867692

seed:  97  N:56
totarea:1.5833
elapsed:1.501
score:  2.6779594072370014

seed:  98  N:339
totarea:1.2728
elapsed:1.501
score:  9.413219740826133

seed:  99  N:224
totarea:3.6253
elapsed:1.501
score:  22.107116681689718

seed: 100  N:133
totarea:1.3381
elapsed:1.501
score:  4.035215428718126

ave:15.333322611733273
