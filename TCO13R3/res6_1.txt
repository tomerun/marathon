seed:   1  N:206
totarea:2.1909
elapsed:3.001
score:  11.009143209928677

seed:   2  N:137
totarea:0.9795
elapsed:3.001
score:  2.290432021743874

seed:   3  N:351
totarea:1.4454
elapsed:3.001
score:  11.091157039599452

seed:   4  N:392
totarea:1.6589
elapsed:3.005
score:  15.674921262439089

seed:   5  N:134
totarea:1.0797
elapsed:3.001
score:  3.583981650712162

seed:   6  N:486
totarea:3.4797
elapsed:3.001
score:  45.69143389657003

seed:   7  N:109
totarea:2.4838
elapsed:3.001
score:  8.496074513976163

seed:   8  N:125
totarea:3.0321
elapsed:3.001
score:  12.71700396076673

seed:   9  N:256
totarea:4.2324
elapsed:3.001
score:  28.843516000068295

seed:  10  N:83
totarea:0.9591
elapsed:3.001
score:  2.096046976449394

seed:  11  N:334
totarea:2.1529
elapsed:3.001
score:  16.172415397635604

seed:  12  N:390
totarea:2.1738
elapsed:3.001
score:  19.2302139460858

seed:  13  N:214
totarea:4.4407
elapsed:3.001
score:  30.325677387220342

seed:  14  N:183
totarea:4.4338
elapsed:3.001
score:  21.361809230172238

seed:  15  N:171
totarea:3.1020
elapsed:3.001
score:  15.11510166834047

seed:  16  N:414
totarea:2.1718
elapsed:3.001
score:  23.028891938315876

seed:  17  N:95
totarea:4.8832
elapsed:3.001
score:  10.03494223609739

seed:  18  N:51
totarea:2.8690
elapsed:3.001
score:  5.919151788473701

seed:  19  N:240
totarea:3.0184
elapsed:3.001
score:  21.362667396350044

seed:  20  N:242
totarea:2.6495
elapsed:3.001
score:  16.773063320290703

seed:  21  N:62
totarea:3.5702
elapsed:3.001
score:  6.144855974445471

seed:  22  N:226
totarea:2.4289
elapsed:3.001
score:  12.5305812878673

seed:  23  N:328
totarea:2.9485
elapsed:3.001
score:  23.232036009520904

seed:  24  N:52
totarea:2.7151
elapsed:3.001
score:  3.271264386725386

seed:  25  N:435
totarea:1.1621
elapsed:3.001
score:  8.41305030413971

seed:  26  N:325
totarea:2.2609
elapsed:3.001
score:  20.586620804895034

seed:  27  N:143
totarea:4.7558
elapsed:3.001
score:  18.039280969399577

seed:  28  N:50
totarea:5.3482
elapsed:3.001
score:  7.149069485641536

seed:  29  N:420
totarea:2.9985
elapsed:3.001
score:  32.90714878231903

seed:  30  N:237
totarea:3.6526
elapsed:3.001
score:  24.110079120162997

seed:  31  N:145
totarea:1.6628
elapsed:3.001
score:  5.791977699074762

seed:  32  N:58
totarea:3.3770
elapsed:3.001
score:  4.869012691104002

seed:  33  N:59
totarea:3.4322
elapsed:3.001
score:  6.441816610475987

seed:  34  N:158
totarea:2.7154
elapsed:3.001
score:  13.968718245889683

seed:  35  N:118
totarea:2.9640
elapsed:3.001
score:  9.24965499591331

seed:  36  N:104
totarea:1.9845
elapsed:3.001
score:  5.596507252092366

seed:  37  N:50
totarea:4.9938
elapsed:3.001
score:  7.413386919784285

seed:  38  N:146
totarea:5.9137
elapsed:3.001
score:  25.13902483695109

seed:  39  N:94
totarea:3.0035
elapsed:3.001
score:  7.920207975755907

seed:  40  N:57
totarea:3.8221
elapsed:3.001
score:  5.677502687425774

seed:  41  N:205
totarea:1.4738
elapsed:3.001
score:  7.516892214602491

seed:  42  N:362
totarea:1.6081
elapsed:3.001
score:  15.016495660018515

seed:  43  N:146
totarea:4.7215
elapsed:3.001
score:  15.25453835280047

seed:  44  N:353
totarea:2.8105
elapsed:3.001
score:  26.14723173556503

seed:  45  N:85
totarea:3.2044
elapsed:3.001
score:  7.108109471286479

seed:  46  N:489
totarea:2.4531
elapsed:3.001
score:  30.723284491371142

seed:  47  N:120
totarea:2.4736
elapsed:3.001
score:  9.551652419168768

seed:  48  N:50
totarea:3.7120
elapsed:3.001
score:  5.9070414465615455

seed:  49  N:388
totarea:2.6864
elapsed:3.001
score:  24.42492636083867

seed:  50  N:356
totarea:5.3193
elapsed:3.001
score:  50.27078275771982

seed:  51  N:99
totarea:2.7002
elapsed:3.003
score:  7.952081223908792

seed:  52  N:60
totarea:2.5196
elapsed:3.001
score:  5.090721938784867

seed:  53  N:56
totarea:3.4311
elapsed:3.001
score:  6.543103382407403

seed:  54  N:69
totarea:4.8608
elapsed:3.001
score:  9.561457240020275

seed:  55  N:224
totarea:2.0191
elapsed:3.001
score:  10.247313395666664

seed:  56  N:182
totarea:2.7157
elapsed:3.001
score:  11.726551797119974

seed:  57  N:56
totarea:7.2571
elapsed:3.001
score:  13.323034888623067

seed:  58  N:255
totarea:5.0326
elapsed:3.001
score:  32.59014816275093

seed:  59  N:450
totarea:3.5381
elapsed:3.001
score:  44.89922740789715

seed:  60  N:88
totarea:2.5441
elapsed:3.001
score:  7.185000287024273

seed:  61  N:375
totarea:4.7083
elapsed:3.001
score:  43.93649420185759

seed:  62  N:175
totarea:2.1710
elapsed:3.001
score:  9.761266683766785

seed:  63  N:323
totarea:3.2725
elapsed:3.001
score:  26.666309639140717

seed:  64  N:52
totarea:2.8528
elapsed:3.001
score:  4.439140306084688

seed:  65  N:54
totarea:1.4014
elapsed:3.001
score:  1.4997980275690534

seed:  66  N:89
totarea:2.6503
elapsed:3.001
score:  5.769924809580614

seed:  67  N:69
totarea:5.9198
elapsed:3.001
score:  13.502588038162532

seed:  68  N:109
totarea:3.0145
elapsed:3.001
score:  8.21747864333643

seed:  69  N:62
totarea:5.0934
elapsed:3.001
score:  10.541095026370906

seed:  70  N:83
totarea:1.0793
elapsed:3.001
score:  2.46486766954851

seed:  71  N:109
totarea:1.6114
elapsed:3.001
score:  5.080962107056497

seed:  72  N:275
totarea:2.1586
elapsed:3.001
score:  13.346521414243112

seed:  73  N:66
totarea:1.3790
elapsed:3.001
score:  2.502167696175578

seed:  74  N:288
totarea:1.3316
elapsed:3.001
score:  8.824267317951207

seed:  75  N:142
totarea:1.5019
elapsed:3.001
score:  4.680909541807702

seed:  76  N:120
totarea:2.0498
elapsed:3.001
score:  5.611523509628968

seed:  77  N:445
totarea:5.0971
elapsed:3.001
score:  58.14785619214345

seed:  78  N:183
totarea:1.1962
elapsed:3.001
score:  4.402137175781158

seed:  79  N:66
totarea:1.5332
elapsed:3.001
score:  2.295512081040385

seed:  80  N:261
totarea:1.6144
elapsed:3.001
score:  8.226643962856262

seed:  81  N:273
totarea:4.9120
elapsed:3.001
score:  37.182695960888786

seed:  82  N:483
totarea:4.7354
elapsed:3.001
score:  57.53759595675759

seed:  83  N:50
totarea:3.0838
elapsed:3.001
score:  4.720006337467138

seed:  84  N:76
totarea:5.3282
elapsed:3.001
score:  9.510695018896321

seed:  85  N:78
totarea:2.5924
elapsed:3.001
score:  6.014862147497263

seed:  86  N:160
totarea:5.1409
elapsed:3.001
score:  26.669564359798922

seed:  87  N:428
totarea:4.0850
elapsed:3.001
score:  48.25680521348448

seed:  88  N:424
totarea:2.6727
elapsed:3.001
score:  31.230977113665787

seed:  89  N:58
totarea:1.6781
elapsed:3.001
score:  2.396540812979244

seed:  90  N:77
totarea:3.1239
elapsed:3.001
score:  6.895429705011866

seed:  91  N:50
totarea:2.9055
elapsed:3.001
score:  4.331228056261026

seed:  92  N:469
totarea:4.7136
elapsed:3.001
score:  59.93762634745439

seed:  93  N:67
totarea:1.8114
elapsed:3.001
score:  3.453894903985332

seed:  94  N:51
totarea:3.1163
elapsed:3.001
score:  4.835940912837068

seed:  95  N:68
totarea:2.6917
elapsed:3.001
score:  4.87698005866931

seed:  96  N:440
totarea:2.0942
elapsed:3.001
score:  24.24142975460992

seed:  97  N:56
totarea:1.5833
elapsed:3.001
score:  2.677399544138148

seed:  98  N:339
totarea:1.2728
elapsed:3.001
score:  9.354353021369317

seed:  99  N:224
totarea:3.6253
elapsed:3.001
score:  21.878803554067595

seed: 100  N:133
totarea:1.3381
elapsed:3.001
score:  3.9930963006763602

ave:15.20222425639575
