import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Tester {
	public static boolean vis = false;
	public static boolean saveImage = false;
	public static int size = 860;

	public static final int MIN_N = 50;
	public static final int MAX_N = 500;
	public static final double MIN_R_FACTOR = 1.0;
	public static final double MAX_R_FACTOR = 5.0;
	public static final double MAX_COORD = 100.0;
	public static final double INVALID = 1000.0;

	DrawerFrame drawer;
	int N;
	double[] x, y, r, m;
	double[] fx, fy;

	class DrawerFrame extends JFrame {
		boolean[] drawTrajectory = new boolean[N];

		class DrawerKeyListener extends KeyAdapter {
			DrawerFrame parent;

			public DrawerKeyListener(DrawerFrame parent) {
				this.parent = parent;
			}

			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == ' ') {
					boolean anyHidden = false;
					for (int i = 0; i < N; ++i) {
						if (!drawTrajectory[i]) {
							anyHidden = true;
							break;
						}
					}
					if (anyHidden) {
						Arrays.fill(drawTrajectory, true);
					} else {
						Arrays.fill(drawTrajectory, false);
					}
					parent.repaint();
				}
			}
		}

		class DrawerMouseListener extends MouseAdapter {
			DrawerFrame parent;

			public DrawerMouseListener(DrawerFrame parent) {
				this.parent = parent;
			}

			public void mouseClicked(MouseEvent e) {
				double x = (e.getX() - size / 2.0) / drawer.scale + (drawer.maxX + drawer.minX) / 2;
				double y = -(e.getY() - size / 2.0) / drawer.scale + (drawer.maxY + drawer.minY) / 2;
				for (int i = 0; i < N; ++i) {
					if (Math.sqrt((x - fx[i]) * (x - fx[i]) + (y - fy[i]) * (y - fy[i])) <= r[i]) {
						drawTrajectory[i] = !drawTrajectory[i];
						parent.repaint();
						break;
					}
				}
			}

		}

		class DrawerPanel extends JPanel {
			public void paint(Graphics g) {
				drawer.paint(g, drawTrajectory);
			}
		}

		Drawer drawer;
		DrawerPanel panel;

		public DrawerFrame() {
			this(INVALID);
		}

		public DrawerFrame(double margin) {
			panel = new DrawerPanel();
			getContentPane().add(panel);
			setSize(size, size);
			setTitle("Visualizer tool for problem CirclesSeparation");
			addKeyListener(new DrawerKeyListener(this));
			panel.addMouseListener(new DrawerMouseListener(this));
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setResizable(false);
			setVisible(true);
			drawer = new Drawer(margin);
		}

	}

	class Drawer {
		public static final int PADDING = 10;
		double minX, maxX, minY, maxY, scale;
		final Color trajColor = new Color(0xFF, 0x33, 0x99);

		Drawer() {
			this(INVALID);
		}

		Drawer(double margin) {
			if (margin == INVALID) {
				minX = 0.0;
				maxX = 1.0;
				minY = 0.0;
				maxY = 1.0;
				for (int i = 0; i < fx.length; i++) {
					if (fx[i] == INVALID) continue;
					minX = Math.min(minX, fx[i] - r[i]);
					maxX = Math.max(maxX, fx[i] + r[i]);
					minY = Math.min(minY, fy[i] - r[i]);
					maxY = Math.max(maxY, fy[i] + r[i]);
				}
			} else {
				minX = -margin;
				maxX = 1.0 + margin;
				minY = -margin;
				maxY = 1.0 + margin;
			}
			scale = (size - 2 * PADDING) / Math.max(maxX - minX, maxY - minY);
		}

		int getX(double x) {
			return (int) Math.round((x - (maxX + minX) / 2) * scale + size / 2.0);
		}

		int getY(double y) {
			return (int) Math.round(((maxY + minY) / 2 - y) * scale + size / 2.0);
		}

		int getL(double len) {
			return (int) Math.round(len * scale);
		}

		public void paint(Graphics g, boolean[] drawTrajectory) {
			for (int i = 0; i < fx.length; i++) {
				if (fx[i] == INVALID) continue;
				int col = (int) Math.round(248 - 150 * m[i]);
				g.setColor(new Color(col, col, 255));
				g.fillOval(getX(fx[i] - r[i]), getY(fy[i] + r[i]), getL(2 * r[i]) + 1, getL(2 * r[i]) + 1);
				g.setColor(Color.BLUE);
				g.drawOval(getX(fx[i] - r[i]), getY(fy[i] + r[i]), getL(2 * r[i]) + 1, getL(2 * r[i]) + 1);
			}

			for (int i = 0; i < fx.length; i++) {
				if (fx[i] == INVALID) continue;
				if (drawTrajectory[i]) {
					g.setColor(trajColor);
					g.drawLine(getX(x[i]), getY(y[i]), getX(fx[i]), getY(fy[i]));
					g.fillOval(getX(x[i]) - 1, getY(y[i]) - 1, 3, 3);
					g.fillOval(getX(fx[i]) - 1, getY(fy[i]) - 1, 3, 3);
				}
			}

			g.setColor(new Color(0, 128, 0));
			g.drawLine(getX(0), getY(0), getX(1), getY(0));
			g.drawLine(getX(1), getY(0), getX(1), getY(1));
			g.drawLine(getX(1), getY(1), getX(0), getY(1));
			g.drawLine(getX(0), getY(1), getX(0), getY(0));
		}

		BufferedImage createImage() {
			BufferedImage im = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
			boolean[] traj = new boolean[N];
			Arrays.fill(traj, true);
			Graphics g = im.getGraphics();
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, size, size);
			paint(g, traj);
			return im;
		}
	}

	void generate(long seed) {
		Random rnd = null;
		try {
			rnd = SecureRandom.getInstance("SHA1PRNG");
		} catch (Exception e) {
			System.err.println("ERROR: unable to generate test case.");
			System.exit(1);
		}
		rnd.setSeed(seed);

		double t = rnd.nextDouble();
		if (1001 <= seed && seed <= 2000) {
			t = 1.0 * (seed - 1001) / 1000;
		}
		N = MIN_N + (int) Math.floor((MAX_N - MIN_N + 1) * t * t);
		double from = Math.sqrt(MIN_R_FACTOR / (double) N);
		double to = Math.sqrt(MAX_R_FACTOR / (double) N);
		double maxR = from + rnd.nextDouble() * (to - from);

		x = new double[N];
		y = new double[N];
		r = new double[N];
		m = new double[N];
		for (int i = 0; i < N; i++) {
			x[i] = rnd.nextDouble();
			y[i] = rnd.nextDouble();
			r[i] = maxR * rnd.nextDouble();
			m[i] = rnd.nextDouble();
		}
	}

	void anime(long seed, int delay) {
		generate(seed);
		fx = new double[N];
		fy = new double[N];
		Scanner sc = new Scanner(System.in);
		double[][] xs = new double[1000][N];
		double[][] ys = new double[1000][N];
		int frame = 0;
		for (; frame < 1000 && sc.hasNext(); ++frame) {
			for (int i = 0; i < N; ++i) {
				xs[frame][i] = sc.nextDouble();
				ys[frame][i] = sc.nextDouble();
			}
		}
		for (int i = 0; i < N; ++i) {
			fx[i] = xs[0][i];
			fy[i] = ys[0][i];
		}
		DrawerFrame d = new DrawerFrame(0.3);
		try {
			Thread.sleep(10000);
			for (int i = 1; i < frame; ++i) {
				Thread.sleep(delay);
				for (int j = 0; j < N; ++j) {
					fx[j] = xs[i][j];
					fy[j] = ys[i][j];
				}
				d.repaint();
			}
		} catch (InterruptedException e) {}
	}

	public Result runTest(long seed) {
		generate(seed);
		Result res = new Result();
		res.seed = seed;
		res.N = N;
		for (int i = 0; i < N; i++) {
			res.totalArea += Math.PI * r[i] * r[i];
		}
		fx = new double[N];
		fy = new double[N];
		long before = System.currentTimeMillis();
		double[] ret = new CirclesSeparation().minimumWork(x, y, r, m);
		res.elapsed = System.currentTimeMillis() - before;
		for (int i = 0; i < N; i++) {
			fx[i] = ret[2 * i];
			fy[i] = ret[2 * i + 1];
		}

		for (int i = 0; i < N; i++) {
			if (fx[i] < -MAX_COORD || fx[i] > MAX_COORD) {
				System.err.println("ERROR: the final X coordinate of " + i
						+ "-th circle (0-based) must be in -100..100. Your return value = " + fx[i] + ".");
				//				return res;
			}
			if (fy[i] < -MAX_COORD || fy[i] > MAX_COORD) {
				System.err.println("ERROR: the final Y coordinate of " + i
						+ "-th circle (0-based) must be in -100..100. Your return value = " + fy[i] + ".");
				//				return res;
			}
		}
		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				if ((fx[i] - fx[j]) * (fx[i] - fx[j]) + (fy[i] - fy[j]) * (fy[i] - fy[j]) < (r[i] + r[j]) * (r[i] + r[j])) {
					System.err.print("ERROR: in your solution, circles " + i + " and " + j + " (0-based) overlap with length ");
					System.err.println((r[i] + r[j]) * (r[i] + r[j]) - (fx[i] - fx[j]) * (fx[i] - fx[j]) + (fy[i] - fy[j])
							* (fy[i] - fy[j]));
					//					return res;
				}
			}
		}
		for (int i = 0; i < N; i++) {
			res.score += m[i] * Math.sqrt((x[i] - fx[i]) * (x[i] - fx[i]) + (y[i] - fy[i]) * (y[i] - fy[i]));
		}
		if (vis) {
			drawer = new DrawerFrame();
		}
		if (saveImage) {
			try {
				Drawer d = new Drawer();
				BufferedImage image = d.createImage();
				String filename = String.format("%04d.png", seed);
				ImageIO.write(image, "png", new File(filename));
			} catch (IOException e) {
				System.err.println(e);
			}
		}
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) {
		long seed = 1;
		long begin = -1;
		long end = -1;
		boolean anime = false;
		int delay = 1000;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-begin")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-end")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-vis")) {
				vis = true;
			} else if (args[i].equals("-im")) {
				saveImage = true;
			} else if (args[i].equals("-sz")) {
				size = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-anime")) {
				anime = true;
			} else if (args[i].equals("-delay")) {
				delay = Integer.parseInt(args[++i]);
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}

		if (anime) {
			Tester tester = new Tester();
			tester.anime(seed, delay);
			return;
		}

		try {
			if (begin != -1 && end != -1) {
				ArrayList<Long> seeds = new ArrayList<Long>();
				for (long i = begin; i <= end; ++i) {
					seeds.add(i);
				}
				int len = seeds.size();
				Result[] results = new Result[len];
				TestThread[] threads = new TestThread[THREAD_COUNT];
				int prev = 0;
				for (int i = 0; i < THREAD_COUNT; ++i) {
					int next = len * (i + 1) / THREAD_COUNT;
					threads[i] = new TestThread(prev, next - 1, seeds, results);
					prev = next;
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].start();
				}
				for (int i = 0; i < THREAD_COUNT; ++i) {
					threads[i].join();
				}
				double sum = 0;
				for (int i = 0; i < results.length; ++i) {
					System.out.println(results[i]);
					System.out.println();
					sum += results[i].score;
				}
				System.out.println("ave:" + (sum / results.length));
			} else {
				Tester tester = new Tester();
				Result res = tester.runTest(seed);
				System.err.println(res);
			}
		} catch (Exception e) {
			System.err.println("ERROR: Unexpected error while running your test case.");
			e.printStackTrace();
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int N;
		double score, totalArea;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d  N:%d\n", seed, N);
			ret += String.format("totarea:%.4f\n", totalArea);
			return ret + "elapsed:" + this.elapsed / 1000.0 + "\nscore:  " + this.score;
		}
	}

}
