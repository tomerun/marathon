#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <iostream>
#include <array>
#include <numeric>
#include <unordered_set>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 5000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9300;
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

constexpr array<int, 41> ROT_PENA = {0, 0, 1, 2, 5, 8, 11, 14, 18, 22, 27, 31, 36, 41, 46, 52, 58, 64, 70, 76, 82, 89, 96, 103, 110, 117, 125, 132, 140, 148, 156, 164, 172, 181, 189, 198, 207, 216, 225, 234, 243};
constexpr array<const char*, 11> solution_type = { // 0 -> beam, 1 -> constructive, 2 -> both
// N=   5    10   15   20   25   30   35   40
  "00000000000000000000000000000000000000000", // P=
  "00000000000000000000000000000000000000000", // 1
  "00000000000000000000000000000000000000000", // 2
  "00000000000000001122222222222222222222222", // 3
  "00000000000000211111111111111111111111111", // 4
  "00000000000002211111111111111111111111111", // 5
  "00000000000002211111111111111111111111111", // 6
  "00000000000022211111111111111111111111111", // 7
  "00000000000022111111111111111111111111111", // 8
  "00000000000022111111111111111111111111111", // 9
  "00000000000022111111111111111111111111111", // 10
};
constexpr int NON_CONSTRUCT_SIZE = 4;

int N, P;
array<array<uint64_t, 40>, 40> hash_seed;
using BeamHand = uint64_t;

struct RandomOrder {

  vi idx;
  int pos, rot;

  RandomOrder(int size) : idx(size), pos(0), rot(0) {
    iota(idx.begin(), idx.end(), 0);
    shuffle();
  }

  int next() {
    if (pos == idx.size()) {
      pos = 0;
      rot++;
      if ((rot & 0x3) == 0) {
        shuffle();
      }
    }
    return idx[pos++];
  }

  void shuffle() {
    for (int i = 0; i < idx.size() - 1; ++i) {
      swap(idx[i], idx[rnd.nextUInt(idx.size() - 1 - i) + i]);
    }
  }
};

struct Rot {
  int r, c, s;
  bool cw;
};

struct Rots {
  vector<Rot> rots;
  vi rots_size_pos;

  void init(int grid_size, int rot_size) {
    rots_size_pos = {0, 0, 0};
    for (int s = 2; s <= rot_size; ++s) {
      for (int r = 0; r + s <= grid_size; ++r) {
        for (int c = 0; c + s <= grid_size; ++c) {
          rots.push_back({r, c, s, true});
          rots.push_back({r, c, s, false});
        }
      }
      rots_size_pos.push_back(rots.size());
    }
  }

  void set_rots_idx(int min_size, int max_size, vi& idx) {
    int min_rot_idx = rots_size_pos[min_size];
    int max_rot_idx = rots_size_pos[max_size + 1];
    idx.resize(max_rot_idx - min_rot_idx);
    iota(idx.begin(), idx.end(), min_rot_idx);
  }
};

Rots all_rots, small_all_rots;

template<class T>
void apply_rot(T& grid, const Rot& rot) {
  if (rot.cw) {
    for (int i = 0; i < rot.s / 2; ++i) {
      for (int j = 0; j < (rot.s + 1) / 2; ++j) {
        int first = grid[rot.r + i][rot.c + j];
        grid[rot.r + i][rot.c + j] = grid[rot.r + rot.s - 1 - j][rot.c + i];
        grid[rot.r + rot.s - 1 - j][rot.c + i] = grid[rot.r + rot.s - 1 - i][rot.c + rot.s - 1 - j];
        grid[rot.r + rot.s - 1 - i][rot.c + rot.s - 1 - j] = grid[rot.r + j][rot.c + rot.s - 1 - i];
        grid[rot.r + j][rot.c + rot.s - 1 - i] = first;
      }
    }
  } else {
    for (int i = 0; i < rot.s / 2; ++i) {
      for (int j = 0; j < (rot.s + 1) / 2; ++j) {
        int first = grid[rot.r + i][rot.c + j];
        grid[rot.r + i][rot.c + j] = grid[rot.r + j][rot.c + rot.s - 1 - i];
        grid[rot.r + j][rot.c + rot.s - 1 - i] = grid[rot.r + rot.s - 1 - i][rot.c + rot.s - 1 - j];
        grid[rot.r + rot.s - 1 - i][rot.c + rot.s - 1 - j] = grid[rot.r + rot.s - 1 - j][rot.c + i];
        grid[rot.r + rot.s - 1 - j][rot.c + i] = first;
      }
    }
  }
}

template<class T>
uint64_t hash_diff(const T& grid, const Rot& rot) {
  uint64_t ret = 0;
  if (rot.cw) {
    for (int i = 0; i < rot.s; ++i) {
      for (int j = 0; j < rot.s; ++j) {
        int cur = grid[rot.r + i][rot.c + j];
        int next = grid[rot.r + rot.s - 1 - j][rot.c + i];
        ret ^= hash_seed[rot.r + i][rot.c + j] * cur;
        ret ^= hash_seed[rot.r + i][rot.c + j] * next;
      }
    }
  } else {
    for (int i = 0; i < rot.s; ++i) {
      for (int j = 0; j < rot.s; ++j) {
        int cur = grid[rot.r + i][rot.c + j];
        int next = grid[rot.r + j][rot.c + rot.s - 1 - i];
        ret ^= hash_seed[rot.r + i][rot.c + j] * cur;
        ret ^= hash_seed[rot.r + i][rot.c + j] * next;
      }
    }
  }
  return ret;
}

template<class T>
int calc_loc_pena(const T& grid, int size) {
  int pena = 0;
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      int row = grid[i][j] >> 8;
      int col = grid[i][j] & 0xFF;
      pena += row > i ? row - i : i - row;
      pena += col > j ? col - j : j - col;
    }
  }
  return pena * P;
}

template<class T>
int calc_loc_pena_l2(const T& grid, int size) {
  int pena = 0;
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      int row = grid[i][j] >> 8;
      int col = grid[i][j] & 0xFF;
      pena += sq(row - i) + sq(col - j);
    }
  }
  return pena * P;
}

template<class T>
int calc_loc_pena_diff_2cw_l1(const T& grid, int r, int c) {
  int ret = 0;
  int cur = grid[r][c];
  int pos = cur & 0xFF;
  ret += abs(c + 1 - pos) - abs(c - pos);
  cur = grid[r][c + 1];
  pos = cur >> 8;
  ret += abs(r + 1 - pos) - abs(r - pos);
  cur = grid[r + 1][c + 1];
  pos = cur & 0xFF;
  ret += abs(c - pos) - abs(c + 1 - pos);
  cur = grid[r + 1][c];
  pos = cur >> 8;
  ret += abs(r - pos) - abs(r + 1 - pos);
  return ret * P;
}

template<class T>
int calc_loc_pena_diff_2ccw_l1(const T& grid, int r, int c) {
  int ret = 0;
  int cur = grid[r][c];
  int pos = cur >> 8;
  ret += abs(r + 1 - pos) - abs(r - pos);
  cur = grid[r][c + 1];
  pos = cur & 0xFF;
  ret += abs(c - pos) - abs(c + 1 - pos);
  cur = grid[r + 1][c + 1];
  pos = cur >> 8;
  ret += abs(r - pos) - abs(r + 1 - pos);
  cur = grid[r + 1][c];
  pos = cur & 0xFF;
  ret += abs(c + 1 - pos) - abs(c - pos);
  return ret * P;
}

template<class T>
int calc_loc_pena_diff(const T& grid, const Rot& rot) {
  ADD_COUNTER(rot.s);
  if (rot.s == 2) {
    if (rot.cw) {
      return calc_loc_pena_diff_2cw_l1(grid, rot.r, rot.c);
    } else {
      return calc_loc_pena_diff_2ccw_l1(grid, rot.r, rot.c);
    }
  // } else if (rot.s == 3) {
  //   if (rot.cw) {
  //     return calc_loc_pena_diff_3cw_l2(grid, rot.r, rot.c);
  //   } else {
  //     return calc_loc_pena_diff_3ccw_l2(grid, rot.r, rot.c);
  //   }
  } else {
    int ret = 0;
    if (rot.cw) {
      for (int i = 0; i < rot.s; ++i) {
        for (int j = 0; j < rot.s; ++j) {
          int cur = grid[rot.r + i][rot.c + j];
          int next = grid[rot.r + rot.s - 1 - j][rot.c + i];
          int cr = cur >> 8;
          int cc = cur & 0xFF;
          int nr = next >> 8;
          int nc = next & 0xFF;
          ret += abs(nr - rot.r - i) + abs(nc - rot.c - j) - abs(cr - rot.r - i) - abs(cc - rot.c - j);
        }
      }
    } else {
      for (int i = 0; i < rot.s; ++i) {
        for (int j = 0; j < rot.s; ++j) {
          int cur = grid[rot.r + i][rot.c + j];
          int next = grid[rot.r + j][rot.c + rot.s - 1 - i];
          int cr = cur >> 8;
          int cc = cur & 0xFF;
          int nr = next >> 8;
          int nc = next & 0xFF;
          ret += abs(nr - rot.r - i) + abs(nc - rot.c - j) - abs(cr - rot.r - i) - abs(cc - rot.c - j);
        }
      }
    }
    return ret * P;
  }
}

template<class T>
int calc_loc_pena_diff_2cw_l2(const T& grid, int r, int c) {
  int ret = 0;
  int cur = grid[r][c];
  int pos = cur & 0xFF;
  ret += sq(c + 1 - pos) - sq(c - pos);
  cur = grid[r][c + 1];
  pos = cur >> 8;
  ret += sq(r + 1 - pos) - sq(r - pos);
  cur = grid[r + 1][c + 1];
  pos = cur & 0xFF;
  ret += sq(c - pos) - sq(c + 1 - pos);
  cur = grid[r + 1][c];
  pos = cur >> 8;
  ret += sq(r - pos) - sq(r + 1 - pos);
  return ret * P;
}

template<class T>
int calc_loc_pena_diff_2ccw_l2(const T& grid, int r, int c) {
  int ret = 0;
  int cur = grid[r][c];
  int pos = cur >> 8;
  ret += sq(r + 1 - pos) - sq(r - pos);
  cur = grid[r][c + 1];
  pos = cur & 0xFF;
  ret += sq(c - pos) - sq(c + 1 - pos);
  cur = grid[r + 1][c + 1];
  pos = cur >> 8;
  ret += sq(r - pos) - sq(r + 1 - pos);
  cur = grid[r + 1][c];
  pos = cur & 0xFF;
  ret += sq(c + 1 - pos) - sq(c - pos);
  return ret * P;
}

template<class T>
int calc_loc_pena_diff_3cw_l2(const T& grid, int r, int c) {
  int ret = 0;
  int cur = grid[r][c];
  int pos = cur & 0xFF;
  ret += sq(c + 2 - pos) - sq(c - pos);

  cur = grid[r][c + 1];
  pos = cur >> 8;
  ret += sq(r + 1 - pos) - sq(r - pos);
  pos = cur & 0xFF;
  ret += sq(c + 2 - pos) - sq(c + 1 - pos);

  cur = grid[r][c + 2];
  pos = cur >> 8;
  ret += sq(r + 2 - pos) - sq(r - pos);

  cur = grid[r + 1][c];
  pos = cur >> 8;
  ret += sq(r - pos) - sq(r + 1 - pos);
  pos = cur & 0xFF;
  ret += sq(c + 1 - pos) - sq(c - pos);

  cur = grid[r + 1][c + 2];
  pos = cur >> 8;
  ret += sq(r + 2 - pos) - sq(r + 1 - pos);
  pos = cur & 0xFF;
  ret += sq(c + 1 - pos) - sq(c + 2 - pos);

  cur = grid[r + 2][c];
  pos = cur >> 8;
  ret += sq(r - pos) - sq(r + 2 - pos);

  cur = grid[r + 2][c + 1];
  pos = cur >> 8;
  ret += sq(r + 1 - pos) - sq(r + 2 - pos);
  pos = cur & 0xFF;
  ret += sq(c - pos) - sq(c + 1 - pos);

  cur = grid[r + 2][c + 2];
  pos = cur & 0xFF;
  ret += sq(c - pos) - sq(c + 2 - pos);
  return ret * P;
}

template<class T>
int calc_loc_pena_diff_3ccw_l2(const T& grid, int r, int c) {
  int ret = 0;
  int cur = grid[r][c];
  int pos = cur >> 8;
  ret += sq(r + 2 - pos) - sq(r - pos);

  cur = grid[r][c + 1];
  pos = cur >> 8;
  ret += sq(r + 1 - pos) - sq(r - pos);
  pos = cur & 0xFF;
  ret += sq(c - pos) - sq(c + 1 - pos);

  cur = grid[r][c + 2];
  pos = cur & 0xFF;
  ret += sq(c - pos) - sq(c + 2 - pos);

  cur = grid[r + 1][c];
  pos = cur >> 8;
  ret += sq(r + 2 - pos) - sq(r + 1 - pos);
  pos = cur & 0xFF;
  ret += sq(c + 1 - pos) - sq(c - pos);

  cur = grid[r + 1][c + 2];
  pos = cur >> 8;
  ret += sq(r - pos) - sq(r + 1 - pos);
  pos = cur & 0xFF;
  ret += sq(c + 1 - pos) - sq(c + 2 - pos);

  cur = grid[r + 2][c];
  pos = cur & 0xFF;
  ret += sq(c + 2 - pos) - sq(c - pos);

  cur = grid[r + 2][c + 1];
  pos = cur >> 8;
  ret += sq(r + 1 - pos) - sq(r + 2 - pos);
  pos = cur & 0xFF;
  ret += sq(c + 2 - pos) - sq(c + 1 - pos);

  cur = grid[r + 2][c + 2];
  pos = cur >> 8;
  ret += sq(r - pos) - sq(r + 2 - pos);
  return ret * P;
}

template<class T>
int calc_loc_pena_diff_l2(const T& grid, const Rot& rot) {
  ADD_COUNTER(rot.s);
  if (rot.s == 2) {
    if (rot.cw) {
      return calc_loc_pena_diff_2cw_l2(grid, rot.r, rot.c);
    } else {
      return calc_loc_pena_diff_2ccw_l2(grid, rot.r, rot.c);
    }
  } else if (rot.s == 3) {
    if (rot.cw) {
      return calc_loc_pena_diff_3cw_l2(grid, rot.r, rot.c);
    } else {
      return calc_loc_pena_diff_3ccw_l2(grid, rot.r, rot.c);
    }
  } else {
    int ret = 0;
    if (rot.cw) {
      for (int i = 0; i < rot.s; ++i) {
        for (int j = 0; j < rot.s; ++j) {
          int cur = grid[rot.r + i][rot.c + j];
          int next = grid[rot.r + rot.s - 1 - j][rot.c + i];
          int cr = cur >> 8;
          int cc = cur & 0xFF;
          int nr = next >> 8;
          int nc = next & 0xFF;
          ret += sq(nr - rot.r - i) + sq(nc - rot.c - j) - sq(cr - rot.r - i) - sq(cc - rot.c - j);
        }
      }
    } else {
      for (int i = 0; i < rot.s; ++i) {
        for (int j = 0; j < rot.s; ++j) {
          int cur = grid[rot.r + i][rot.c + j];
          int next = grid[rot.r + j][rot.c + rot.s - 1 - i];
          int cr = cur >> 8;
          int cc = cur & 0xFF;
          int nr = next >> 8;
          int nc = next & 0xFF;
          ret += sq(nr - rot.r - i) + sq(nc - rot.c - j) - sq(cr - rot.r - i) - sq(cc - rot.c - j);
        }
      }
    }
    return ret * P;
  }
}

struct State {
  array<array<int, 40>, 40> grid;

  int calc_loc_pena(int size) const {
    return ::calc_loc_pena(grid, size);
  }

  int calc_loc_pena_l2(int size) const {
    return ::calc_loc_pena_l2(grid, size);
  }

  int calc_loc_pena_diff(const Rot& rot) const {
    return ::calc_loc_pena_diff(grid, rot);
  }

  void apply(const Rot& rot) {
    apply_rot(grid, rot);
  }

  void rot_2_cw(int r, int c) {
    int first = grid[r][c];
    grid[r][c] = grid[r + 1][c];
    grid[r + 1][c] = grid[r + 1][c + 1];
    grid[r + 1][c + 1] = grid[r][c + 1];
    grid[r][c + 1] = first;
  }

  void rot_2_ccw(int r, int c) {
    int first = grid[r][c];
    grid[r][c] = grid[r][c + 1];
    grid[r][c + 1] = grid[r + 1][c + 1];
    grid[r + 1][c + 1] = grid[r + 1][c];
    grid[r + 1][c] = first;
  }

  int eval_2_cw(int r, int c) const {
    int ret = 0;

    int cur = grid[r][c];
    int pos = cur & 0xFF;
    ret += c < pos ? -1 : 1;

    cur = grid[r][c + 1];
    pos = cur >> 8;
    ret += r < pos ? -1 : 1;

    cur = grid[r + 1][c + 1];
    pos = cur & 0xFF;
    ret += c < pos ? 1 : -1;

    cur = grid[r + 1][c];
    pos = cur >> 8;
    ret += r < pos ? 1 : -1;

    return ret;
  }

  int eval_2_ccw(int r, int c) const {
    int ret = 0;

    int cur = grid[r][c];
    int pos = cur >> 8;
    ret += r < pos ? -1 : 1;

    cur = grid[r + 1][c];
    pos = cur & 0xFF;
    ret += c < pos ? -1 : 1;

    cur = grid[r + 1][c + 1];
    pos = cur >> 8;
    ret += r < pos ? 1 : -1;

    cur = grid[r][c + 1];
    pos = cur & 0xFF;
    ret += c < pos ? 1 : -1;

    return ret;
  }
};

struct Ans {
  vector<Rot> rots;
  int loc_pena, move_pena;
};

struct BeamState {
  Rot rot;
  int prev_idx;
  int move_pena, loc_pena;
  uint64_t hash;
};

vector<vector<BeamState>> beam;
unordered_set<uint64_t> used_hash;

struct BeamSolver {
  vvi initial_grid;
  Ans ans;
  int size, beam_width, orig_select_count;
  int min_rot_size, max_rot_size;
  vi rots_idx;
  Rots& use_rots;

  BeamSolver(const State& s, int sz, int bw, int sc, int min_rs, int max_rs, Rots& rots)
   : size(sz), beam_width(bw), orig_select_count(sc), min_rot_size(min_rs), max_rot_size(max_rs), use_rots(rots) {
    initial_grid.resize(sz);
    for (int i = 0; i < sz; ++i) {
      initial_grid[i].assign(s.grid[i].begin(), s.grid[i].begin() + sz);
    }
    for (int i = 0; i < beam.size(); ++i) {
      beam[i].clear();
      beam[i].reserve(bw);
    }
    if (beam.empty()) {
      beam.resize(1);
    }
    BeamState initial_state;
    initial_state.prev_idx = -1;
    initial_state.move_pena = 0;
    initial_state.loc_pena = s.calc_loc_pena_l2(sz);
    initial_state.hash = 0;
    beam[0].push_back(initial_state);
    used_hash.insert(initial_state.hash);
    use_rots.set_rots_idx(min_rot_size, max_rot_size, rots_idx);
  }

  void update_rots_idx(int turn) {
    if (max_rot_size == 3) return;
    if ((turn & 0xFF) == 0) {
      max_rot_size--;
      if (min_rot_size > 2) min_rot_size--;
      use_rots.set_rots_idx(min_rot_size, max_rot_size, rots_idx);
    }
  }

  void solve(int max_turn, int best_score) {
    int min_turn = 0;
    int min_idx = 0;
    int min_loc_pena = beam[0][0].loc_pena;
    int min_move_pena = 0;
    vector<vvi> prev_grids(beam_width, vvi(size, vi(size)));
    vector<vvi> next_grids(beam_width, vvi(size, vi(size)));
    vector<vvi> prev_loc_diff_2cw(beam_width, vvi(size - 1, vi(size - 1)));
    vector<vvi> prev_loc_diff_2ccw(beam_width, vvi(size - 1, vi(size - 1)));
    vector<vvi> prev_loc_diff_3cw(beam_width, vvi(size - 2, vi(size - 2)));
    vector<vvi> prev_loc_diff_3ccw(beam_width, vvi(size - 2, vi(size - 2)));
    vector<vvi> next_loc_diff_2cw(beam_width, vvi(size - 1, vi(size - 1)));
    vector<vvi> next_loc_diff_2ccw(beam_width, vvi(size - 1, vi(size - 1)));
    vector<vvi> next_loc_diff_3cw(beam_width, vvi(size - 2, vi(size - 2)));
    vector<vvi> next_loc_diff_3ccw(beam_width, vvi(size - 2, vi(size - 2)));
    for (int i = 0; i < size; ++i) {
      copy(initial_grid[i].begin(), initial_grid[i].end(), prev_grids[0][i].begin());
    }
    for (int i = 0; i < size - 1; ++i) {
      for (int j = 0; j < size - 1; ++j) {
        prev_loc_diff_2cw[0][i][j] = calc_loc_pena_diff_2cw_l2(prev_grids[0], i, j);
        prev_loc_diff_2ccw[0][i][j] = calc_loc_pena_diff_2ccw_l2(prev_grids[0], i, j);
      }
    }
    for (int i = 0; i < size - 2; ++i) {
      for (int j = 0; j < size - 2; ++j) {
        prev_loc_diff_3cw[0][i][j] = calc_loc_pena_diff_3cw_l2(prev_grids[0], i, j);
        prev_loc_diff_3ccw[0][i][j] = calc_loc_pena_diff_3ccw_l2(prev_grids[0], i, j);
      }
    }

    vvi rot2cw_pos_idx(size - 1, vi(size - 1));
    vvi rot2ccw_pos_idx(size - 1, vi(size - 1));
    vvi rot3cw_pos_idx(size - 2, vi(size - 2));
    vvi rot3ccw_pos_idx(size - 2, vi(size - 2));
    for (int i = 0; i < use_rots.rots.size(); ++i) {
      const Rot& rot = use_rots.rots[i];
      if (rot.s == 2) {
        if (rot.cw) {
          rot2cw_pos_idx[rot.r][rot.c] = i;
        } else {
          rot2ccw_pos_idx[rot.r][rot.c] = i;
        }
      } else if (rot.s == 3) {
        if (rot.cw) {
          rot3cw_pos_idx[rot.r][rot.c] = i;
        } else {
          rot3ccw_pos_idx[rot.r][rot.c] = i;
        }
      }
    }
    vector<BeamHand> next_state;
    int turn = 1;
    // use L2 norm
    for (; turn < min_turn + 10 && turn < max_turn && min_loc_pena > min(500, 2 * N * N) * P; ++turn) {
      next_state.clear();
      if ((turn & 0x1) == 0 && beam_width > 3 && get_elapsed_msec() >= TL) {
        debug("reach time limit at turn %d\n", turn);
        beam_width = 3;
        if (beam[turn - 1].size() > beam_width) {
          beam[turn - 1].resize(beam_width);
        }
        orig_select_count = 3;
      }
      update_rots_idx(turn);
      int select_count = orig_select_count;
      if (select_count > rots_idx.size()) {
        select_count = rots_idx.size();
      } else if (select_count > rots_idx.size() * 3 / 4) {
        select_count = rots_idx.size() * 3 / 4;
      }
      START_TIMER(4);
      for (int i = 0; i < beam[turn - 1].size(); ++i) {
        const BeamState& prev_state = beam[turn - 1][i];
        if (turn > 1 && N >= 18 && select_count > 100 && select_count < rots_idx.size()) {
          int prev_t = prev_state.rot.r;
          int prev_l = prev_state.rot.c;
          int prev_b = prev_state.rot.r + prev_state.rot.s - 1;
          int prev_r = prev_state.rot.c + prev_state.rot.s - 1;
          for (int r = max(0, prev_t - 2); r <= min(size - 2, prev_b); ++r) {
            for (int c = max(0, prev_l - 2); c <= min(size - 2, prev_r); ++c) {
              if (r > prev_t - 2 && c > prev_l - 2) {
                int loc_pena_diff = prev_loc_diff_2cw[i][r][c];
                if (ROT_PENA[2] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[2] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot2cw_pos_idx[r][c] << 16) | i);
                }
                loc_pena_diff = prev_loc_diff_2ccw[i][r][c];
                if (ROT_PENA[2] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[2] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot2ccw_pos_idx[r][c] << 16) | i);
                }
              }
              if (r < size - 2 && c < size - 2) {
                int loc_pena_diff = prev_loc_diff_3cw[i][r][c];
                if (ROT_PENA[3] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[3] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot3cw_pos_idx[r][c] << 16) | i);
                }
                loc_pena_diff = prev_loc_diff_3ccw[i][r][c];
                if (ROT_PENA[3] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[3] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot3ccw_pos_idx[r][c] << 16) | i);
                }
              }
            }
          }
        }
        for (int j = 0; j < select_count; ++j) {
          int rot_pos = rnd.nextUInt(rots_idx.size() - j) + j;
          swap(rots_idx[j], rots_idx[rot_pos]);
          const Rot& rot = use_rots.rots[rots_idx[j]];
          int loc_pena_diff;
          if (rot.s == 2) {
            loc_pena_diff = rot.cw ? prev_loc_diff_2cw[i][rot.r][rot.c] : prev_loc_diff_2ccw[i][rot.r][rot.c];
          } else if (rot.s == 3) {
            loc_pena_diff = rot.cw ? prev_loc_diff_3cw[i][rot.r][rot.c] : prev_loc_diff_3ccw[i][rot.r][rot.c];
          } else {
            loc_pena_diff = calc_loc_pena_diff_l2(prev_grids[i], rot);
          }
          if (ROT_PENA[rot.s] + loc_pena_diff >= 1 + 3 * P) continue;
          uint64_t next_pena = prev_state.move_pena + ROT_PENA[rot.s] + prev_state.loc_pena + loc_pena_diff;
          next_state.push_back((next_pena << 40) | (((uint64_t)(j & 0xFF) << 32)) | (rots_idx[j] << 16) | i);
        }
      }
      STOP_TIMER(4);
      START_TIMER(5);
      if (next_state.size() > beam_width * 2) {
        partial_sort(next_state.begin(), next_state.begin() + beam_width * 2, next_state.end());
      } else {
        sort(next_state.begin(), next_state.end());
      }
      STOP_TIMER(5);
      if (beam.size() <= turn) {
        beam.resize(turn + 1);
      }
      START_TIMER(6);
      for (int i = 0; i < min(beam_width * 2, (int)next_state.size()) && beam[turn].size() < beam_width; ++i) {
        const BeamHand hand = next_state[i];
        const Rot& rot = use_rots.rots[(hand >> 16) & 0xFFFF];
        const int pi = hand & 0xFFFF;
        const uint64_t hash = beam[turn - 1][pi].hash ^ hash_diff(prev_grids[pi], rot);
        if (used_hash.find(hash) != used_hash.end()) continue;
        used_hash.insert(hash);
        const int ni = beam[turn].size();
        for (int j = 0; j < size; ++j) {
          copy(prev_grids[pi][j].begin(), prev_grids[pi][j].end(), next_grids[ni][j].begin());
        }
        for (int j = 0; j < size - 2; ++j) {
          copy(prev_loc_diff_2cw[pi][j].begin(), prev_loc_diff_2cw[pi][j].end(), next_loc_diff_2cw[ni][j].begin());
          copy(prev_loc_diff_2ccw[pi][j].begin(), prev_loc_diff_2ccw[pi][j].end(), next_loc_diff_2ccw[ni][j].begin());
          copy(prev_loc_diff_3cw[pi][j].begin(), prev_loc_diff_3cw[pi][j].end(), next_loc_diff_3cw[ni][j].begin());
          copy(prev_loc_diff_3ccw[pi][j].begin(), prev_loc_diff_3ccw[pi][j].end(), next_loc_diff_3ccw[ni][j].begin());
        }
        copy(prev_loc_diff_2cw[pi][size - 2].begin(), prev_loc_diff_2cw[pi][size - 2].end(), next_loc_diff_2cw[ni][size - 2].begin());
        copy(prev_loc_diff_2ccw[pi][size - 2].begin(), prev_loc_diff_2ccw[pi][size - 2].end(), next_loc_diff_2ccw[ni][size - 2].begin());
        apply_rot(next_grids[ni], rot);
        for (int y = max(0, rot.r - 1); y < min(size - 1, rot.r + rot.s); ++y) {
          for (int x = max(0, rot.c - 1); x < min(size - 1, rot.c + rot.s); ++x) {
            next_loc_diff_2cw[ni][y][x] = calc_loc_pena_diff_2cw_l2(next_grids[ni], y, x);
            next_loc_diff_2ccw[ni][y][x] = calc_loc_pena_diff_2ccw_l2(next_grids[ni], y, x);
          }
        }
        for (int y = max(0, rot.r - 2); y < min(size - 2, rot.r + rot.s); ++y) {
          for (int x = max(0, rot.c - 2); x < min(size - 2, rot.c + rot.s); ++x) {
            next_loc_diff_3cw[ni][y][x] = calc_loc_pena_diff_3cw_l2(next_grids[ni], y, x);
            next_loc_diff_3ccw[ni][y][x] = calc_loc_pena_diff_3ccw_l2(next_grids[ni], y, x);
          }
        }
        const int next_pena = (int)(hand >> 40);
        const int move_pena = beam[turn - 1][pi].move_pena + ROT_PENA[rot.s];
        beam[turn].push_back({rot, pi, move_pena, next_pena - move_pena, hash});
        if (next_pena < min_loc_pena + min_move_pena) {
          min_loc_pena = next_pena - move_pena;
          min_move_pena = move_pena;
          min_turn = turn;
          min_idx = ni;
        }
      }
      STOP_TIMER(6);
      if (beam[turn].empty()) {
        break;
      }
      // debug("turn:%d beam_witdh:%lu\n", turn, beam[turn].size());
      swap(prev_grids, next_grids);
      swap(prev_loc_diff_2cw, next_loc_diff_2cw);
      swap(prev_loc_diff_2ccw, next_loc_diff_2ccw);
      swap(prev_loc_diff_3cw, next_loc_diff_3cw);
      swap(prev_loc_diff_3ccw, next_loc_diff_3ccw);
    }
    debug("beam_pena:%d loc:%d move:%d turn:%d hash_size:%lu\n", min_loc_pena + min_move_pena, min_loc_pena, min_move_pena, min_turn, used_hash.size());

    // use L1 norm
    min_loc_pena = 1 << 30;
    min_move_pena = 0;
    min_turn = turn - 1;
    for (int i = 0; i < beam[turn - 1].size(); ++i) {
      BeamState& bs = beam[turn - 1][i];
      bs.loc_pena = calc_loc_pena(prev_grids[i], size);
      if (bs.loc_pena + bs.move_pena < min_loc_pena + min_move_pena) {
        min_loc_pena = bs.loc_pena;
        min_move_pena = bs.move_pena;
        min_idx = i;
      }
    }
    for (; turn < min_turn + 10 && turn < max_turn; ++turn) {
      next_state.clear();
      if (beam_width > 3 && get_elapsed_msec() >= TL) {
        debug("reach time limit at turn %d\n", turn);
        beam_width = 3;
        if (beam[turn - 1].size() > beam_width) {
          beam[turn - 1].resize(beam_width);
        }
        orig_select_count = 3;
      }
      update_rots_idx(turn);
      int select_count = orig_select_count;
      if (select_count > rots_idx.size()) {
        select_count = rots_idx.size();
      } else if (select_count > rots_idx.size() * 3 / 4) {
        select_count = rots_idx.size() * 3 / 4;
      }
      // debug("select_count:%d rots_idx.size:%d\n", select_count, (int)rots_idx.size());
      for (int i = 0; i < beam[turn - 1].size(); ++i) {
        const BeamState& prev_state = beam[turn - 1][i];
        if (turn > 1 && N >= 18 && select_count > 100 && select_count < rots_idx.size()) {
          int prev_t = prev_state.rot.r;
          int prev_l = prev_state.rot.c;
          int prev_b = prev_state.rot.r + prev_state.rot.s - 1;
          int prev_r = prev_state.rot.c + prev_state.rot.s - 1;
          for (int r = max(0, prev_t - 2); r <= min(size - 2, prev_b); ++r) {
            for (int c = max(0, prev_l - 2); c <= min(size - 2, prev_r); ++c) {
              if (r > prev_t - 2 && c > prev_l - 2) {
                int loc_pena_diff = calc_loc_pena_diff(prev_grids[i], use_rots.rots[rot2cw_pos_idx[r][c]]);
                if (ROT_PENA[2] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[2] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot2cw_pos_idx[r][c] << 16) | i);
                }
                loc_pena_diff = calc_loc_pena_diff(prev_grids[i], use_rots.rots[rot2ccw_pos_idx[r][c]]);
                if (ROT_PENA[2] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[2] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot2ccw_pos_idx[r][c] << 16) | i);
                }
              }
              if (r < size - 2 && c < size - 2) {
                int loc_pena_diff = calc_loc_pena_diff(prev_grids[i], use_rots.rots[rot3cw_pos_idx[r][c]]);
                if (ROT_PENA[3] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[3] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot3cw_pos_idx[r][c] << 16) | i);
                }
                loc_pena_diff = calc_loc_pena_diff(prev_grids[i], use_rots.rots[rot3ccw_pos_idx[r][c]]);
                if (ROT_PENA[3] + loc_pena_diff < 1 + 3 * P) {
                  uint64_t next_pena = prev_state.move_pena + ROT_PENA[3] + prev_state.loc_pena + loc_pena_diff;
                  next_state.push_back((next_pena << 40) | (rot3ccw_pos_idx[r][c] << 16) | i);
                }
              }
            }
          }
        }

        for (int j = 0; j < select_count; ++j) {
          int rot_pos = rnd.nextUInt(rots_idx.size() - j) + j;
          swap(rots_idx[j], rots_idx[rot_pos]);
          const Rot& rot = use_rots.rots[rots_idx[j]];
          int loc_pena_diff = calc_loc_pena_diff(prev_grids[i], rot);
          if (ROT_PENA[rot.s] + loc_pena_diff >= 1 + 3 * P) continue;
          if (ROT_PENA[rot.s] + prev_state.move_pena >= best_score) continue;
          uint64_t next_pena = prev_state.move_pena + ROT_PENA[rot.s] + prev_state.loc_pena + loc_pena_diff;
          next_state.push_back((next_pena << 40) | (((uint64_t)(j & 0xFF) << 32)) | (rots_idx[j] << 16) | i);
        }
      }
      if (next_state.size() > beam_width * 2) {
        partial_sort(next_state.begin(), next_state.begin() + beam_width * 2, next_state.end());
      } else {
        sort(next_state.begin(), next_state.end());
      }
      if (beam.size() <= turn) {
        beam.resize(turn + 1);
      }
      for (int i = 0; i < min(beam_width * 2, (int)next_state.size()) && beam[turn].size() < beam_width; ++i) {
        const BeamHand hand = next_state[i];
        const Rot& rot = use_rots.rots[(hand >> 16) & 0xFFFF];
        const int pi = hand & 0xFFFF;
        const uint64_t hash = beam[turn - 1][pi].hash ^ hash_diff(prev_grids[pi], rot);
        if (used_hash.find(hash) != used_hash.end()) continue;
        used_hash.insert(hash);
        int ni = beam[turn].size();
        for (int j = 0; j < size; ++j) {
          copy(prev_grids[pi][j].begin(), prev_grids[pi][j].end(), next_grids[ni][j].begin());
        }
        apply_rot(next_grids[ni], rot);
        const int next_pena = (int)(hand >> 40);
        const int move_pena = beam[turn - 1][pi].move_pena + ROT_PENA[rot.s];
        beam[turn].push_back({rot, pi, move_pena, next_pena - move_pena, hash});
        if (next_pena < min_loc_pena + min_move_pena) {
          min_loc_pena = next_pena - move_pena;
          min_move_pena = move_pena;
          min_turn = turn;
          min_idx = ni;
          best_score = min(best_score, min_loc_pena + min_move_pena);
        }
      }
      // debug("turn:%d beam_witdh:%lu\n", turn, beam[turn].size());
      swap(prev_grids, next_grids);
    }
    debug("beam_pena:%d loc:%d move:%d turn:%d hash_size:%lu\n", min_loc_pena + min_move_pena, min_loc_pena, min_move_pena, min_turn, used_hash.size());

    ans.loc_pena = beam[min_turn][min_idx].loc_pena;
    ans.move_pena = beam[min_turn][min_idx].move_pena;
    ans.rots.resize(min_turn);
    while (min_turn > 0) {
      ans.rots[min_turn - 1] = beam[min_turn][min_idx].rot;
      min_idx = beam[min_turn][min_idx].prev_idx;
      min_turn--;
    }
    used_hash.clear();
  }
};

struct ConstructState {
  vector<Rot> rots;
  int move_pena;
  int prev_idx;
};

vector<vector<ConstructState>> c_beam;

struct ConstructiveSolver {
  State state;
  Ans ans;
  ll time_limit;
  vector<vector<int>> rev_pos;
  vector<vector<bool>> fixed;

  ConstructiveSolver(const State& s, ll tl) : state(s), time_limit(tl) {
    ans.loc_pena = ans.move_pena = 0;
    rev_pos.assign(N, vi(N));
    fixed.assign(N, vector<bool>(N));
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        int r = state.grid[i][j] >> 8;
        int c = state.grid[i][j] & 0xFF;
        rev_pos[r][c] = (i << 8) | j;
      }
    }
  }

  void update_revpos(int r, int c) {
    const int num = state.grid[r][c];
    rev_pos[num >> 8][num & 0xFF] = (r << 8) | c;
  }

  void update_revpos_2x2(int r, int c) {
    update_revpos(r, c);
    update_revpos(r, c + 1);
    update_revpos(r + 1, c + 1);
    update_revpos(r + 1, c);
  }

  void rot_2_cw(int r, int c) {
    state.rot_2_cw(r, c);
    ans.rots.push_back({r, c, 2, true});
    ans.move_pena += ROT_PENA[2];
    update_revpos_2x2(r, c);
  }

  void rot_2_ccw(int r, int c) {
    state.rot_2_ccw(r, c);
    ans.rots.push_back({r, c, 2, false});
    ans.move_pena += ROT_PENA[2];
    update_revpos_2x2(r, c);
  }

  void move_up(int& r, int c) {
    assert(r > 0);
    bool ok_r = c != N - 1 && !fixed[r - 1][c + 1] && !fixed[r][c + 1];
    bool ok_l = c != 0 && !fixed[r - 1][c - 1] && !fixed[r][c - 1];
    if (ok_r && ok_l) {
      int v_r = state.eval_2_cw(r - 1, c);
      int v_l = state.eval_2_ccw(r - 1, c - 1);
      if (v_r < v_l) {
        rot_2_cw(r - 1, c);
      } else {
        rot_2_ccw(r - 1, c - 1);
      }
    } else if (ok_r) {
      rot_2_cw(r - 1, c);
    } else {
      assert(ok_l);
      rot_2_ccw(r - 1, c - 1);
    }
    r--;
  }

  void move_down(int& r, int c) {
    assert(r < N - 1);
    bool ok_r = c != N - 1 && !fixed[r + 1][c + 1] && !fixed[r][c + 1];
    bool ok_l = c != 0 && !fixed[r + 1][c - 1] && !fixed[r][c - 1];
    if (ok_r && ok_l) {
      int v_r = state.eval_2_ccw(r, c);
      int v_l = state.eval_2_cw(r, c - 1);
      if (v_r < v_l) {
        rot_2_ccw(r, c);
      } else {
        rot_2_cw(r, c - 1);
      }
    } else if (ok_r) {
      rot_2_ccw(r, c);
    } else {
      assert(ok_l);
      rot_2_cw(r, c - 1);
    }
    r++;
  }

  void move_left(int r, int& c) {
    assert(c > 0);
    bool ok_b = r != N - 1 && !fixed[r + 1][c - 1] && !fixed[r + 1][c];
    bool ok_t = r != 0 && !fixed[r - 1][c - 1] && !fixed[r - 1][c];
    if (ok_b && ok_t) {
      int v_b = state.eval_2_ccw(r, c - 1);
      int v_t = state.eval_2_cw(r - 1, c - 1);
      if (v_b < v_t) {
        rot_2_ccw(r, c - 1);
      } else {
        rot_2_cw(r - 1, c - 1);
      }
    } else if (ok_b) {
      rot_2_ccw(r, c - 1);
    } else {
      assert(ok_t);
      rot_2_cw(r - 1, c - 1);
    }
    c--;
  }

  void move_right(int r, int& c) {
    assert(c < N - 1);
    bool ok_b = r != N - 1 && !fixed[r + 1][c + 1] && !fixed[r + 1][c];
    bool ok_t = r != 0 && !fixed[r - 1][c + 1] && !fixed[r - 1][c];
    if (ok_b && ok_t) {
      int v_b = state.eval_2_cw(r, c);
      int v_t = state.eval_2_ccw(r - 1, c);
      if (v_b < v_t) {
        rot_2_cw(r, c);
      } else {
        rot_2_ccw(r - 1, c);
      }
    } else if (ok_b) {
      rot_2_cw(r, c);
    } else {
      assert(ok_t);
      rot_2_ccw(r - 1, c);
    }
    c++;
  }

  void apply(const Rot& rot) {
    state.apply(rot);
    ans.rots.push_back(rot);
    ans.move_pena += ROT_PENA[rot.s];
    for (int i = 0; i < rot.s; ++i) {
      for (int j = 0; j < rot.s; ++j) {
        update_revpos(rot.r + i, rot.c + j);
      }
    }
  }

  int sep_pos(int len) {
    if (len < 4) return 0;
    int min_pos = max(2, (len + 1) / 2 - 3);
    int max_pos = min(len - 2, (len + 1) / 2 + 3);
    return rnd.nextUInt(max_pos - min_pos + 1) + min_pos;
  }

  int next_size(int rest) {
    if (rest <= 3) return rest;
    int max_size = min(3, rest);
    while (true) {
      int ret = rnd.nextUInt(max_size) + 1;
      if (ret != rest - 1) return ret;
    }
  }

  void solve_one(int i) {
    int bottom_sep = sep_pos(i + 1);
    // debug("bottom_sep:%d\n", bottom_sep);
    // bottom-right
    int cur = i + 1;
    while (cur > bottom_sep) {
      if (cur - bottom_sep > 2 && state.grid[i][cur - 1] == ((i << 8) | (cur - 1))) {
        cur--;
        fixed[i][cur] = true;
        continue;
      }
      int size = next_size(cur - bottom_sep);
      int col = cur - size;
      // debug("right: %d %d\n", cur, size);
      for (int j = 0; j < size; ++j) {
        int target_r = i - j;
        int pos = rev_pos[i][cur - j - 1];
        int nr = pos >> 8;
        int nc = pos & 0xFF;
        if (cur == N && size == 2 && j == 1 && nr == i && nc == cur - 1) { // edge case
          if (cur - bottom_sep > 2) {
            rot_2_ccw(i - 1, i - 1);
            fixed[i][i - 1] = false;
            cur -= 1;
          } else {
            cur -= 2;
          }
          size = 0;
          fixed[i][col + 1] = true;
          break;
        }
        while (nr > target_r) {
          if (nr == i && nc == col + 1 && size == 2) { // edge case
            rot_2_cw(nr - 1, nc);
            nr--;
          } else {
            move_up(nr, nc);
          }
        }
        if (nr == 0) {
          move_down(nr, nc);
        }
        while (nc > col) {
          move_left(nr, nc);
        }
        while (nc < col) {
          move_right(nr, nc);
        }
        while (nr < target_r) {
          move_down(nr, nc);
        }
        if (pos == ((i << 8) | (col + 1)) && size == 2 && j == 1) { // edge case
          rot_2_ccw(i - 1, col + 1);
        }
        fixed[target_r][col] = true;
      }
      if (size > 1) {
        Rot rot = {i - size + 1, col, size, false};
        apply(rot);
        for (int j = 1; j < size; ++j) {
          fixed[i][col + j] = true;
          fixed[i - j][col] = false;
        }
      }
      cur -= size;
    }

    // bottom-left
    cur = 0;
    while (cur < bottom_sep) {
      if (bottom_sep - cur > 2 && state.grid[i][cur] == ((i << 8) | cur)) {
        fixed[i][cur] = true;
        cur++;
        continue;
      }
      int size = next_size(bottom_sep - cur);
      int col = cur + size - 1;
     // debug("left: %d %d\n", cur, size);
      for (int j = 0; j < size; ++j) {
        int target_r = i - j;
        int pos = rev_pos[i][cur + j];
        int nr = pos >> 8;
        int nc = pos & 0xFF;
        if (cur == 0 && size == 2 && j == 1 && nr == i && nc == 0) { // edge case
          if (bottom_sep - cur > 2) {
            rot_2_cw(i - 1, 0);
            fixed[i][1] = false;
            cur += 1;
          } else {
            cur += 2;
          }
          size = 0;
          fixed[i][col - 1] = true;
          break;
        }
        while (nr > target_r) {
          if (nr == i && nc == col - 1 && size == 2) { // edge case
            rot_2_ccw(nr - 1, nc - 1);
            nr--;
          } else {
            move_up(nr, nc);
          }
        }
        if (nr == 0) {
          move_down(nr, nc);
        }
        while (nc > col) {
          move_left(nr, nc);
        }
        while (nc < col) {
          move_right(nr, nc);
        }
        while (nr < target_r) {
          move_down(nr, nc);
        }
        if (pos == ((i << 8) | (col - 1)) && size == 2 && j == 1) { // edge case
          rot_2_cw(i - 1, col - 2);
        }
        fixed[target_r][col] = true;
      }
      if (size > 1) {
        Rot rot = {i - size + 1, cur, size, true};
        apply(rot);
        for (int j = 1; j < size; ++j) {
          fixed[i][col - j] = true;
          fixed[i - j][col] = false;
        }
      }
      cur += size;
    }

    int right_sep = sep_pos(i);
    // debug("right_sep:%d\n", right_sep);
    // right-bottom
    cur = i;
    while (cur > right_sep) {
      if (cur - right_sep > 2 && state.grid[cur - 1][i] == (((cur - 1) << 8) | i)) {
        cur--;
        fixed[cur][i] = true;
        continue;
      }
      int size = next_size(cur - right_sep);
      int row = cur - size;
      // debug("bottom: %d %d\n", cur, size);
      for (int j = 0; j < size; ++j) {
        int target_c = i - j;
        int pos = rev_pos[cur - j - 1][i];
        int nr = pos >> 8;
        int nc = pos & 0xFF;
        while (nc > target_c) {
          if (nc == i && nr == row + 1 && size == 2) { // edge case
            rot_2_ccw(nr, nc - 1);
            nc--;
          } else {
            move_left(nr, nc);
          }
        }
        if (nc == 0) {
          move_right(nr, nc);
        }
        while (nr > row) {
          move_up(nr, nc);
        }
        while (nr < row) {
          move_down(nr, nc);
        }
        while (nc < target_c) {
          move_right(nr, nc);
        }
        if (pos == (((row + 1) << 8) | i) && size == 2 && j == 1) { // edge case
          rot_2_cw(row + 1, i - 1);
        }
        fixed[row][target_c] = true;
      }
      if (size > 1) {
        Rot rot = {row, i - size + 1, size, true};
        apply(rot);      
        for (int j = 1; j < size; ++j) {
          fixed[row + j][i] = true;
          fixed[row][i - j] = false;
        }
      }
      cur -= size;
    }

    // right-top
    cur = 0;
    while (cur < right_sep) {
      if (right_sep - cur > 2 && state.grid[cur][i] == ((cur << 8) | i)) {
        fixed[cur][i] = true;
        cur++;
        continue;
      }
      int size = next_size(right_sep - cur);
      int row = cur + size - 1;
      // debug("top: %d %d\n", cur, size);
      for (int j = 0; j < size; ++j) {
        int target_c = i - j;
        int pos = rev_pos[cur + j][i];
        int nr = pos >> 8;
        int nc = pos & 0xFF;
        if (cur == 0 && size == 2 && j == 1 && nr == 0 && nc == i) { // edge case
          if (right_sep - cur > 2) {
            rot_2_ccw(0, i - 1);
            fixed[1][i] = false;
            cur += 1;
          } else {
            cur += 2;
          }
          size = 0;
          fixed[row - 1][i] = true;
          break;
        }
        while (nc > target_c) {
          if (nc == i && nr == row - 1 && size == 2) { // edge case
            rot_2_cw(nr - 1, nc - 1);
            nc--;
          } else {
            move_left(nr, nc);
          }
        }
        if (nc == 0) {
          move_right(nr, nc);
        }
        while (nr > row) {
          move_up(nr, nc);
        }
        while (nr < row) {
          move_down(nr, nc);
        }
        while (nc < target_c) {
          move_right(nr, nc);
        }
        if (pos == (((row - 1) << 8) | i) && size == 2 && j == 1) { // edge case
          rot_2_ccw(row - 2, i - 1);
        }
        fixed[row][target_c] = true;
      }
      if (size > 1) {
        Rot rot = {cur, i - size + 1, size, false};
        apply(rot);
        for (int j = 1; j < size; ++j) {
          fixed[row - j][i] = true;
          fixed[row][i - j] = false;
        }
      }
      cur += size;
    }

    // for (int y = 0; y < N; ++y) {
    //   for (int x = 0; x < N; ++x) {
    //     assert((y < i && x < i) != fixed[y][x]);
    //   }
    // }
  }

  void solve(int beam_width, int try_count) {
    debug("try_count:%d\n", try_count);
    if (c_beam.empty()) {
      c_beam.assign(N + 1, vector<ConstructState>(beam_width));
      c_beam[N][0].move_pena = 0;
    }
    vector<vvi> prev_grid(beam_width, vvi(N, vi(N)));
    vector<vvi> prev_rev_pos(beam_width, vvi(N, vi(N)));
    vector<vvi> next_grid(beam_width, vvi(N, vi(N)));
    vector<vvi> next_rev_pos(beam_width, vvi(N, vi(N)));
    for (int r = 0; r < N; ++r) {
      for (int c = 0; c < N; ++c) {
        prev_grid[0][r][c] = state.grid[r][c];
        prev_rev_pos[0][r][c] = rev_pos[r][c];
      }
    }

    vector<ConstructState> next_state;
    for (int i = N - 1; i >= NON_CONSTRUCT_SIZE; --i) {
      next_state.clear();
      c_beam[i].clear();
      START_TIMER(10);
      for (int j = 0; j < c_beam[i + 1].size(); ++j) {
        if (j >= 2 && get_elapsed_msec() > time_limit) {
          debug("reach_time_limit at %d\n", i);
          beam_width = 2;
          try_count = 2;
          break;
        }
        const ConstructState& prev_state = c_beam[i + 1][j];
        for (int k = 0; k < try_count; ++k) {
          for (int r = 0; r <= i; ++r) {
            copy(prev_grid[j][r].begin(), prev_grid[j][r].begin() + i + 1, state.grid[r].begin());
            copy(prev_rev_pos[j][r].begin(), prev_rev_pos[j][r].begin() + i + 1, rev_pos[r].begin());
            fixed[r][i] = fixed[i][r] = false;
          }
          START_TIMER(18);
          solve_one(i);
          STOP_TIMER(18);
          if (state.grid[i][0] != i << 8) ans.move_pena += 2 * P;
          if (state.grid[0][i] != i) ans.move_pena += 2 * P;
          next_state.push_back({ans.rots, prev_state.move_pena + ans.move_pena, j});
          ans.rots.clear();
          ans.move_pena = 0;
        }
      }
      STOP_TIMER(10);
      START_TIMER(11);
      vector<int> next_state_p(next_state.size());
      for (int j = 0; j < next_state.size(); ++j) {
        next_state_p[j] = (next_state[j].move_pena << 16) | j;
      }
      nth_element(next_state_p.begin(), next_state_p.begin() + beam_width, next_state_p.end());
      STOP_TIMER(11);
      START_TIMER(12);
      for (int j = 0; j < min(beam_width, (int)next_state.size()); ++j) {
        const ConstructState& st = next_state[next_state_p[j] & 0xFFFF];
        const int pi = st.prev_idx;
        c_beam[i].push_back(st);
        for (int r = 0; r <= i; ++r) {
          copy(prev_grid[pi][r].begin(), prev_grid[pi][r].begin() + i + 1, next_grid[j][r].begin());
        }
        for (const Rot& rot : st.rots) {
          apply_rot(next_grid[j], rot);
        }
        for (int r = 0; r <= i; ++r) {
          for (int c = 0; c <= i; ++c) {
            int rv = next_grid[j][r][c] >> 8;
            int cv = next_grid[j][r][c] & 0xFF;
            next_rev_pos[j][rv][cv] = (r << 8) | c;
          }
        }
      }
      STOP_TIMER(12);
      swap(prev_grid, next_grid);
      swap(prev_rev_pos, next_rev_pos);
    }
    int best_idx = 0;
    int min_pena = 1 << 30;
    for (int i = 0; i < c_beam[NON_CONSTRUCT_SIZE].size(); ++i) {
      if (c_beam[NON_CONSTRUCT_SIZE][i].move_pena < min_pena) {
        best_idx = i;
        min_pena = c_beam[NON_CONSTRUCT_SIZE][i].move_pena;
      }
    }
    debug("min_move:%d\n", min_pena);
    for (int r = 0; r < NON_CONSTRUCT_SIZE; ++r) {
      copy(prev_grid[best_idx][r].begin(), prev_grid[best_idx][r].begin() + NON_CONSTRUCT_SIZE, state.grid[r].begin());
    }
    for (int i = NON_CONSTRUCT_SIZE; i < N; ++i) {
      ans.rots.insert(ans.rots.end(), c_beam[i][best_idx].rots.rbegin(), c_beam[i][best_idx].rots.rend());
      best_idx = c_beam[i][best_idx].prev_idx;
    }
    reverse(ans.rots.begin(), ans.rots.end());
    ans.move_pena = min_pena;

    BeamSolver beam_solver(state, NON_CONSTRUCT_SIZE, 100, 30, 2, NON_CONSTRUCT_SIZE, small_all_rots);
    beam_solver.solve(100, 1 << 30);
    for (const Rot& rot : beam_solver.ans.rots) {
      state.apply(rot);
    }
    ans.rots.insert(ans.rots.end(), beam_solver.ans.rots.begin(), beam_solver.ans.rots.end());
    ans.move_pena += beam_solver.ans.move_pena;
    ans.loc_pena = state.calc_loc_pena(NON_CONSTRUCT_SIZE);
  }
};

struct RotatingNumbers {
  vector<string> findSolution(int, int, vector<int>&);
};

Ans solve_beam(State initial_state) {
  int initial_beam_width = 200000 / (N * N * N);
  auto before_time = get_time();
  int try_count = N >= 15 ? 600 : 400;
  int MAX_ROT = 3;
  BeamSolver beam_solver(initial_state, N, initial_beam_width, try_count, 2, min(N, MAX_ROT), all_rots);
  beam_solver.solve(10000, 1 << 30);
  Ans best_ans = beam_solver.ans;
  auto elapsed_initial_beam = get_time() - before_time + 1;

  auto time_left = TL - get_elapsed_msec();
  int beam_width = max(5, min(10000, (int)(initial_beam_width * time_left * 19 / (elapsed_initial_beam * 20))));
  const int main_beam_width = beam_width;
  debug("score:%d width:%d elapsed:%lld\n", best_ans.loc_pena + best_ans.move_pena, initial_beam_width, elapsed_initial_beam);
  auto max_beam_time = elapsed_initial_beam;
  for (int turn = 0; ; ++turn) {
    auto current_time = get_elapsed_msec();
    if (current_time + max_beam_time / 8 > TL) {
      debug("turn:%d score:%d\n", turn, best_ans.loc_pena + best_ans.move_pena);
      break;
    }
    if (TL - current_time < max_beam_time) {
      beam_width = (int)(main_beam_width * (TL - current_time) / max_beam_time);
      if (beam_width <= 2) break;
    }
    BeamSolver beam_solver(initial_state, N, beam_width, try_count, 2, min(N, MAX_ROT), all_rots);
    START_TIMER(3);
    beam_solver.solve(10000, best_ans.loc_pena + best_ans.move_pena);
    STOP_TIMER(3);
    auto elapsed = get_elapsed_msec() - current_time;
    max_beam_time = max(max_beam_time, elapsed);
    debug("score:%d width:%d elapsed:%lld\n", beam_solver.ans.loc_pena + beam_solver.ans.move_pena, beam_width, elapsed);
    if (beam_solver.ans.loc_pena + beam_solver.ans.move_pena < best_ans.loc_pena + best_ans.move_pena) {
      best_ans = beam_solver.ans;
    }
  }
  return best_ans;
}

Ans solve_constructive(State initial_state, int loop, ll time_limit) {
  START_TIMER(1);
  int MAX_ROT = N >= 36 ? 4 : 3;
  BeamSolver pre_solver(initial_state, N, 16000 / (N * N), 1000, 2, min(N, MAX_ROT), all_rots);
  pre_solver.solve(N * N * N / 13, 1 << 30);
  for (const Rot& rot : pre_solver.ans.rots) {
    initial_state.apply(rot);
  }
  debug("beam score:%d elapsed:%lld\n", pre_solver.ans.loc_pena + pre_solver.ans.move_pena, get_elapsed_msec());
  Ans ans = pre_solver.ans
  STOP_TIMER(1);

  Ans best_ans;
  best_ans.loc_pena = 1 << 30;
  best_ans.move_pena = 0;
  for (int turn = 0; ; ++turn) {
    if (get_elapsed_msec() > time_limit) {
      debug("total turn:%d score:%d\n", turn, best_ans.loc_pena + best_ans.move_pena);
      break;
    }
    START_TIMER(2);
    int beam_width = 30;
    int try_count = loop / (beam_width * N * N);
    ConstructiveSolver solver(initial_state, time_limit);
    solver.solve(beam_width, try_count);
    STOP_TIMER(2);
    if (solver.ans.loc_pena + solver.ans.move_pena < best_ans.loc_pena + best_ans.move_pena) {
      swap(best_ans, solver.ans);
      debug("turn:%d score:%d\n", turn, best_ans.loc_pena + best_ans.move_pena);
    }
  }

  ans.rots.insert(ans.rots.end(), best_ans.rots.begin(), best_ans.rots.end());
  ans.move_pena += best_ans.move_pena;
  ans.loc_pena = best_ans.loc_pena;
  return ans;
}

vector<string> RotatingNumbers::findSolution(int N_, int P_, vector<int>& grid_) {
  N = N_;
  P = P_;
  State initial_state = {};
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      initial_state.grid[i][j] = grid_[i * N + j];
    }
  }
  debug("initial_pena:%d\n", initial_state.calc_loc_pena(N));
  Ans ans;
  if (solution_type[P][N] == '0') {
    ans = solve_beam(initial_state);
  } else if (solution_type[P][N] == '1') {
    ans = solve_constructive(initial_state, 5000000, TL);
  } else {
    Ans ans_c = solve_constructive(initial_state, 1000000, 2000);
    Ans ans_b = solve_beam(initial_state);
    debug("score_cons:%d score_beam:%d\n", ans_c.loc_pena + ans_c.move_pena, ans_b.loc_pena + ans_b.move_pena);
    if (ans_c.loc_pena + ans_c.move_pena < ans_b.loc_pena + ans_b.move_pena) {
      ans = ans_c;
    } else {
      ans = ans_b;
    }
  }

  debug("loc_pena:%d move_pena:%d\n", ans.loc_pena, ans.move_pena);
  vector<string> ret(ans.rots.size());
  for (int i = 0; i < ans.rots.size(); ++i) {
    const Rot& rot = ans.rots[i];
    ret[i] = to_string(rot.r) + " " + to_string(rot.c) + " " + to_string(rot.s) + " " + (rot.cw ? "R" : "L");
  }
  return ret;
}

int main() {
  start_time = get_time();
  RotatingNumbers prog;
  int N;
  int P;
  scanf("%d %d", &N, &P);
  vi grid(N * N);
  for (int i = 0; i < N * N; i++) {
    scanf("%d", &grid[i]);
    int row = (grid[i] - 1) / N;
    int col = (grid[i] - 1) % N;
    grid[i] = (row << 8) | col;
    hash_seed[i / N][i % N] = ((uint64_t)rnd.nextUInt() << 32) | rnd.nextUInt();
  }
  all_rots.init(N, N);
  small_all_rots.init(NON_CONSTRUCT_SIZE, NON_CONSTRUCT_SIZE);
  
  vector<string> ret = prog.findSolution(N, P, grid);
  printf("%d\n", (int)ret.size());
  for (int i = 0; i < (int)ret.size(); ++i) {
    printf("%s\n", ret[i].c_str());
  }
  fflush(stdout);
  PRINT_COUNTER();
  PRINT_TIMER();
}