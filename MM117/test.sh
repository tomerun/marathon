#!/bin/sh -exu

for i in 2 4 5; do
  g++ -Wall -Wno-sign-compare -std=gnu++11 -O3 -DLOCAL -DCUTOFF=${i} -o main RotatingNumbers.cpp
  java Tester -b 10000 -e 11999 > result/all31_cutoff_$i.txt
done