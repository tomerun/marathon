import java.io.*;
import java.security.SecureRandom;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

class TestCase {
	public static final int minN = 4, maxN = 40;
	public static final int minP = 1, maxP = 10;
	public static final int minSize = 2;
	public static final int MAX_MOVES = 100000;
	public int N;
	public int P;
	public int[][] Grid;
	public long seed;
	public int MovePenalty = 0;
	public int LocationPenalty = 0;

	public TestCase(Scanner sc) {
		this.seed = sc.nextLong();
		this.N = sc.nextInt();
		this.P = sc.nextInt();
		this.Grid = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				this.Grid[i][j] = sc.nextInt();
			}
		}
	}

	public TestCase(long seed, int n, int p) throws Exception {
		SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
		this.seed = seed;
		rnd.setSeed(seed);

		if (n == 0) {
			N = rnd.nextInt(maxN - minN + 1) + minN;
			if (seed == 1) N = minN;
			if (seed == 2) N = maxN;
			if (1000 <= seed && seed < 1500) {
				N = (int)((seed - 1000) * (maxN - minN + 1) / 500 + minN);
			}
		} else {
			N = Math.min(Math.max(n, minN), maxN);
		}
		if (p == 0) {
			P = rnd.nextInt(maxP - minP + 1) + minP;
		} else {
			P = Math.min(Math.max(p, minP), maxP);
		}

		int[] ind = new int[N * N];
		for (int i = 0; i < ind.length; i++) {
			ind[i] = i;
		}
		for (int i = 0; i < ind.length; i++) {
			int k = (int) (rnd.nextDouble() * (ind.length - i) + i);
			int temp = ind[i];
			ind[i] = ind[k];
			ind[k] = temp;
		}

		Grid = new int[N][N];
		for (int i = 0; i < N * N; i++)
			Grid[ind[i] / N][ind[i] % N] = (i + 1);
	}

	public void makeMove(int r, int c, int size, boolean clockwise) {
		int[][] temp = new int[size][size];
		MovePenalty += (int) Math.pow(size - 1, 1.5);
		if (clockwise) {
			for (int r2 = 0; r2 < size; r2++)
				for (int c2 = 0; c2 < size; c2++)
					temp[r2][c2] = Grid[r + size - 1 - c2][c + r2];
		} else {
			for (int r2 = 0; r2 < size; r2++)
				for (int c2 = 0; c2 < size; c2++)
					temp[r2][c2] = Grid[r + c2][c + size - 1 - r2];
		}

		for (int r2 = 0; r2 < size; r2++)
			for (int c2 = 0; c2 < size; c2++)
				Grid[r + r2][c + c2] = temp[r2][c2];
	}

	void recalcLocationPenalty() {
		this.LocationPenalty = 0;
		for (int r2 = 0; r2 < this.N; r2++) {
			for (int c2 = 0; c2 < this.N; c2++) {
				int requiredR = (this.Grid[r2][c2] - 1) / this.N;
				int requiredC = (this.Grid[r2][c2] - 1) % this.N;
				this.LocationPenalty += this.P * (Math.abs(r2 - requiredR) + Math.abs(c2 - requiredC));
			}
		}
	}

}

class Move {
	int r;
	int c;
	int size;
	boolean clockwise;

	public Move(int R, int C, int S, char D) {
		r = R;
		c = C;
		size = S;
		clockwise = D == 'R';
	}
}

public class Tester {
	public long seed;
	public static int N = 0;
	public static int P = 0;
	public Process solution;
	PrintWriter logWriter;

	Tester(long seed) {
		this.seed = seed;
	}

	public Result runTest() throws Exception {
		solution = Runtime.getRuntime().exec("./main");
		BufferedReader reader = new BufferedReader(new InputStreamReader(solution.getInputStream()));
		PrintWriter writer = new PrintWriter(solution.getOutputStream());
		ErrorReader output = new ErrorReader(solution.getErrorStream());
		output.start();

		Result res = new Result();
		res.seed = this.seed;
		TestCase tc = new TestCase(seed, N, P);
		long startTime = System.currentTimeMillis();
		writer.println(tc.N);
		writer.println(tc.P);
		for (int i = 0; i < tc.N; i++)
			for (int k = 0; k < tc.N; k++)
				writer.println(tc.Grid[i][k]);
		writer.flush();

		int numMoves = Integer.parseInt(reader.readLine());
		if (numMoves > TestCase.MAX_MOVES) {
			throw new RuntimeException("ERROR: Return array is too large.");
		}
		String[] allMoves = new String[numMoves];
		for (int i = 0; i < numMoves; i++) {
			allMoves[i] = reader.readLine();
		}
		res.elapsed = System.currentTimeMillis() - startTime;
		res.N = tc.N;
		res.P = tc.P;
		if (logWriter != null) {
			logWriter.println(seed + " " + tc.N + " " + tc.P);
			for (int i = 0; i < tc.N; i++) {
				for (int j = 0; j < tc.N; j++) {
					logWriter.print(tc.Grid[i][j] + (j == tc.N - 1 ? "\n" : " "));
				}
			}
			logWriter.println(numMoves);
		}
		try {
			for (int i = 0; i < numMoves; i++) {
				String smove = allMoves[i];
				if (logWriter != null) {
					logWriter.println(smove);
				}
				String[] s = smove.split(" ");
				int r = Integer.parseInt(s[0]);
				int c = Integer.parseInt(s[1]);
				int size = Integer.parseInt(s[2]);
				if (size < TestCase.minSize || size > tc.N) {
					throw new RuntimeException(
							"ERROR: size must be between " + TestCase.minSize + " and " + tc.N + ", but " + size + " was given");
				}
				if (r + size > tc.N || c + size > tc.N) {
					throw new RuntimeException("ERROR: move " + smove + " is out of bounds.");
				}
				char dir = s[3].charAt(0);
				if (!(dir == 'R' || dir == 'L')) {
					throw new RuntimeException("ERROR: direction " + s[3] + " is illegal.");
				}
				tc.makeMove(r, c, size, dir == 'R');
			}
			tc.recalcLocationPenalty();
			res.locPena = tc.LocationPenalty;
			res.movePena = tc.MovePenalty;
			res.moves = numMoves;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stopSolution();
		}
		return res;
	}

	public void stopSolution() {
		if (solution != null) {
			solution.destroy();
		}
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 0, begin = -1, end = -1;
		String logFileName = null;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-N")) N = Integer.parseInt(args[++i]);
			if (args[i].equals("-P")) P = Integer.parseInt(args[++i]);
			if (args[i].equals("-log")) logFileName = args[++i];
		}
		if (begin != -1 && end != -1) {
			BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
			BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int) (end - begin + 1));
			for (long i = begin; i <= end; ++i) {
				q.add(i);
			}
			Result[] results = new Result[(int) (end - begin + 1)];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i] = new TestThread(q, receiver);
				threads[i].start();
			}
			int printed = 0;
			try {
				for (int i = 0; i < (int) (end - begin + 1); i++) {
					Result res = receiver.poll(160, TimeUnit.SECONDS);
					results[(int) (res.seed - begin)] = res;
					for (; printed < results.length && results[printed] != null; printed++) {
						System.out.println(results[printed]);
						System.out.println();
					}
				}
			} catch (InterruptedException e) {
				for (int i = printed; i < results.length; i++) {
					System.out.println(results[i]);
					System.out.println();
				}
				e.printStackTrace();
			}

			double sum = 0;
			for (Result result : results) {
				sum += result.locPena + result.movePena;
			}
			System.out.println("ave:" + (sum / (end - begin + 1)));
		} else {
			Tester tester = new Tester(seed);
			if (logFileName != null) {
				tester.logWriter = new PrintWriter(new FileWriter(logFileName));
			}
			Result res = tester.runTest();
			if (logFileName != null) {
				tester.logWriter.flush();
				tester.logWriter.close();
			}
			System.out.println(res);
		}
	}

	private static class TestThread extends Thread {
		BlockingQueue<Result> results;
		BlockingQueue<Long> q;

		TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
			this.q = q;
			this.results = results;
		}

		public void run() {
			while (true) {
				Long seed = q.poll();
				if (seed == null) break;
				try {
					Tester f = new Tester(seed);
					Result res = f.runTest();
					results.add(res);
				} catch (Exception e) {
					e.printStackTrace();
					Result res = new Result();
					res.seed = seed;
					results.add(res);
				}
			}
		}
	}
}

class Result {
	long seed;
	int N, P, locPena, movePena, moves;
	long elapsed;

	public String toString() {
		String ret = String.format("seed:%4d\n", seed);
		ret += String.format("N:%2d P:%2d locPena:%5d movePena:%5d moves:%4d\n", N, P, locPena, movePena, moves);
		ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
		ret += String.format("score:%5d", locPena + movePena);
		return ret;
	}
}

class ErrorReader extends Thread {
	InputStream error;
	StringBuilder sb = new StringBuilder();

	public ErrorReader(InputStream is) {
		error = is;
	}

	public void run() {
		try {
			byte[] ch = new byte[50000];
			int read;
			while ((read = error.read(ch)) > 0) {
				String s = new String(ch, 0, read);
				System.err.print(s);
				sb.append(s);
				System.err.flush();
			}
		} catch (Exception e) {
		}
	}
}
