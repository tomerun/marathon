#!/bin/sh -exu

SEED_START=$(expr $AWS_BATCH_JOB_ARRAY_INDEX \* $RANGE + 1)
SEED_END=$(expr $AWS_BATCH_JOB_ARRAY_INDEX \* $RANGE + $RANGE)

javac Tester.java
g++ RotatingNumbers.cpp -Wall -Wno-sign-compare -std=gnu++11 -O3 -DLOCAL -o main
java Tester -b $SEED_START -e $SEED_END > log.txt 2> /dev/null
aws s3 cp log.txt s3://marathon-tester/$RESULT_PATH/$SEED_START.txt
