#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:N, :P, :score)
CompareResult = Struct.new(:win, :lose, :tie, :diff, :win_rate)

def get_scores(filename)
  scores = []
  seed = 0
  n = 0
  c = 0
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      seed = $1.to_i
      scores[seed] = TestCase.new
    elsif line =~ /N: *(\d+) P: *(\d+)/
      scores[seed].N = $1.to_i
      scores[seed].P = $2.to_i
    elsif line =~ /score: *([0-9]+)/
      scores[seed].score = $1.to_i
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    next unless filter.call(pair[0], pair[1])
    count += 1
    s1 = pair[0].score
    s2 = pair[1].score
    if s1 < s2
      lose += 1
      sum_score_diff += 1.0 * s1 / s2 - 1.0
    elsif s1 > s2
      win += 1
      sum_score_diff += 1.0 - 1.0 * s2 / s1
    else
      #
    end
  end
  # puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  # puts sprintf("  diff:%.6f", 1000000.0 * sum_score_diff / count)
  tie = count - win - lose
  return CompareResult.new(win, lose, tie, sum_score_diff / count, (win * 2.0 + tie) / (count * 2.0) )
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])
  ["win_rate", "diff"].each do |key|
    puts "#{key}:"
    print ' N\P |'
    1.upto(10) do |p|
      print sprintf(' %3d', p)
    end
    puts
    puts '-' * 46
    4.step(40, 2) do |n|
      n_upper = n + 1
      print sprintf('%2d-%2d|', n, n_upper)
      1.upto(10) do |p|
        res = calc(from, to) { |from, to| from.N.between?(n, n_upper) && from.P == p }
        print sprintf(' %3d', (res[key] * 100).to_i)
      end
      puts
    end
  end
end

main
