import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.util.Scanner;

public class Visualizer extends JFrame {

	static final int EXTRA_WIDTH = 200;
	static final int EXTRA_HEIGHT = 50;
	static final int startX = 10;
	static final int startY = 10;
	int SZ;
	TestCase tc;
	DrawerPanel panel;
	int turn;
	Move[] moves;
	Frame[] frames;
	int highlightPos = -1;

	static class Frame {
		int[][] grid;
		int locPena, movePena;

		Frame(TestCase tc) {
			this.grid = new int[tc.N][tc.N];
			for (int i = 0; i < tc.N; i++) {
				System.arraycopy(tc.Grid[i], 0, this.grid[i], 0, tc.N);
			}
			this.locPena = tc.LocationPenalty;
			this.movePena = tc.MovePenalty;
		}
	}

	class DrawerKeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					if (turn > 0) {
						if (e.isMetaDown()) {
							move(-frames.length);
						} else if (e.isShiftDown()) {
							move(-10);
						} else {
							move(-1);
						}
						repaint();
					}
					break;
				case KeyEvent.VK_RIGHT:
					if (turn < frames.length - 1) {
						if (e.isMetaDown()) {
							move(frames.length);
						} else if (e.isShiftDown()) {
							move(10);
						} else {
							move(1);
						}
						repaint();
					}
					break;
				case KeyEvent.VK_UP:
					if (highlightPos >= 0) {
						highlightPos--;
					} else {
						highlightPos = tc.N - 1;
					}
					repaint();
					break;
				case KeyEvent.VK_DOWN:
					if (highlightPos < tc.N - 1) {
						highlightPos++;
					} else {
						highlightPos = -1;
					}
					repaint();
					break;
			}
		}
	}

	private void move(int diff) {
		turn += diff;
		if (turn < 0) turn = 0;
		if (turn >= frames.length) turn = frames.length - 1;
		repaint();
	}

	public void repaint(int r, int c, int s) {
		panel.repaint(startX + SZ * c - SZ * s / 2, startY + SZ * r - SZ * s / 2, SZ * s * 2, SZ * s * 2);
	}

	class DrawerPanel extends JPanel {
		Font fontGrid;
		Color[] colors;
		Color highlightColor = new Color(255, 128, 255);

		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

			// gray background
			g.setColor(new Color(0xDDDDDD));
			g.fillRect(0, 0, 2000, 2000);
			// white rectangle
			g.setColor(Color.WHITE);
			g.fillRect(startX, startY, tc.N * SZ, tc.N * SZ);
			//paint thin lines between cells
			g.setColor(Color.BLACK);
			for (int i = 0; i <= tc.N; i++)
				g.drawLine(startX, startY + i * SZ, startX + tc.N * SZ, startY + i * SZ);
			for (int i = 0; i <= tc.N; i++)
				g.drawLine(startX + i * SZ, startY, startX + i * SZ, startY + tc.N * SZ);

			if (fontGrid == null) {
				int fontSize = 4;
				int limit = SZ - 2;
				if (SZ > 20) limit -= SZ / 10;
				while (fontSize < 50) {
					g.setFont(new Font("Arial", Font.PLAIN, fontSize));
					FontMetrics fm = g2.getFontMetrics();
					Rectangle2D rc = fm.getStringBounds("8888", g2);
					if (rc.getWidth() >= limit) {
						fontSize--;
						break;
					}
					fontSize++;
				}
				fontGrid = new Font("Arial", Font.PLAIN, fontSize);
			}
			g.setFont(fontGrid);
			FontMetrics fm = g2.getFontMetrics();

			if (colors == null) {
				colors = new Color[2 * tc.N - 1];
				colors[0] = new Color(128, 128, 255);
				for (int i = 1; i < colors.length; i++) {
					int alpha = 255 - (int) Math.round(255 * (1 - Math.min(1, (i - 1) * 4.0 / (2 * (tc.N - 1)))));
					colors[i] = new Color(alpha, 255, alpha);
				}
			}

			Frame frame = frames[turn];
			Rectangle clip = g.getClipBounds();
			for (int step = 0; step <= 0; step++) {
				for (int r = 0; r < tc.N; r++) {
					if (startY + r * SZ > clip.getMaxY()) break;
					if (startY + r * SZ + SZ < clip.getMinY()) continue;
					for (int c = 0; c < tc.N; c++) {
						if (startX + c * SZ > clip.getMaxX()) break;
						if (startX + c * SZ + SZ < clip.getMinX()) continue;
						//paint squares green based on how close they are to the target location
						int requiredR = (frame.grid[r][c] - 1) / tc.N;
						int requiredC = (frame.grid[r][c] - 1) % tc.N;
						int dist = Math.abs(r - requiredR) + Math.abs(c - requiredC);
						g.setColor(colors[dist]);
						if (Math.max(requiredR, requiredC) == highlightPos) {
							g.setColor(highlightColor);
						}
						g.fillRect(startX + SZ * c + 1, startY + SZ * r + 1, SZ - 1, SZ - 1);
						//draw numbers
						g.setColor(Color.BLACK);
						String str = String.format("%02d%02d", requiredR, requiredC);
						Rectangle2D rc = fm.getStringBounds(str, g2);
						int x = (SZ - (int) rc.getWidth()) / 2;
						int y = (SZ - (int) rc.getHeight()) / 2 + fm.getAscent();
						g.drawString(str, startX + SZ * c + x, startY + SZ * r + y);
					}
				}
			}

			if (turn > 0) {
				// draw last move
				Move move = moves[turn - 1];
				Stroke oldStroke = g2.getStroke();
				g2.setStroke(new BasicStroke(3));
				g.setColor(move.clockwise ? Color.BLUE : Color.RED);
				g.drawRect(startX + move.c * SZ, startY + move.r * SZ, SZ * move.size, SZ * move.size);
				g2.setStroke(oldStroke);
			}

			g.setColor(Color.BLACK);
			g.setFont(new Font("Arial", Font.BOLD, 16));
			g.drawString("N: " + tc.N, SZ * tc.N + 25, 50);
			g.drawString("P: " + tc.P, SZ * tc.N + 25, 80);
			g.drawString("Moves: " + turn + "/" + moves.length, SZ * tc.N + 25, 110);
			g.drawString("Move Penalty: " + frame.movePena, SZ * tc.N + 25, 140);
			g.drawString("Loc Penalty: " + frame.locPena, SZ * tc.N + 25, 170);
			g.drawString("SCORE: " + (frame.movePena + frame.locPena), SZ * tc.N + 25, 200);
		}
	}

	class DrawerWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			System.exit(0);
		}
	}

	public Visualizer(TestCase tc_, Move[] moves, int SZ) {
		this.tc = tc_;
		tc.recalcLocationPenalty();
		this.moves = moves;
		this.frames = new Frame[moves.length + 1];
		this.frames[0] = new Frame(tc);
		for (int i = 0; i < moves.length; i++) {
			Move m = moves[i];
			tc.makeMove(m.r, m.c, m.size, m.clockwise);
			tc.recalcLocationPenalty();
			this.frames[i + 1] = new Frame(tc);
		}
		panel = new DrawerPanel();
		getContentPane().add(panel);
		addWindowListener(new DrawerWindowListener());
		addKeyListener(new DrawerKeyListener());
		if (SZ == 0) {
			Insets frameInsets = getInsets();
			int fw = frameInsets.left + frameInsets.right;
			int fh = frameInsets.top + frameInsets.bottom;
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Dimension screenSize = toolkit.getScreenSize();
			Insets screenInsets = toolkit.getScreenInsets(getGraphicsConfiguration());
			screenSize.width -= screenInsets.left + screenInsets.right;
			screenSize.height -= screenInsets.top + screenInsets.bottom;
			SZ = Math.min((screenSize.width - fw - EXTRA_WIDTH) / tc.N, (screenSize.height - fh - EXTRA_HEIGHT) / tc.N);
			setSize(tc.N * SZ + EXTRA_WIDTH + fw, Math.max(420, tc.N * SZ + EXTRA_HEIGHT + fh));
		} else {
			setSize(tc.N * SZ + EXTRA_WIDTH, tc.N * SZ + EXTRA_HEIGHT);
		}
		this.SZ = SZ;
		setTitle("Visualizer for RotatingNumbers, seed = " + tc.seed);
		setResizable(false);
		setVisible(true);
	}

	public static void main(String[] args) {
		int size = 0;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-sz")) size = Integer.parseInt(args[++i]);
		}
		TestCase tc;
		Move[] moves;
		try (Scanner sc = new Scanner(System.in)) {
			tc = new TestCase(sc);
			int numMove = sc.nextInt();
			moves = new Move[numMove];
			sc.nextLine();
			for (int i = 0; i < numMove; i++) {
				String[] elems = sc.nextLine().split(" ");
				int r = Integer.parseInt(elems[0]);
				int c = Integer.parseInt(elems[1]);
				int len = Integer.parseInt(elems[2]);
				moves[i] = new Move(r, c, len, elems[3].charAt(0));
			}
		}
		new Visualizer(tc, moves, size);
	}
}
