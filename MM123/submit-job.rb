range = 100
array_size = 6
contest_id = 123
solver_version = ARGV[0] || "00"

args = [
	'batch', 'submit-job',
	'--job-name', 'marathon_tester',
	'--job-queue', 'marathon_tester',
	'--job-definition', 'marathon_tester',
]

if array_size > 1
	args << '--array-properties' << "size=#{array_size}"
end
args << '--container-overrides'

solver_path = "#{contest_id}/#{solver_version}"

result_path = "#{solver_path}/00"
envs = "environment=[{name=RANGE,value=#{range}},{name=SUBMISSION_ID,value=#{solver_path}},{name=RESULT_PATH,value=#{result_path}}]"
system('aws', *args, envs)

# [10, 14, 18, 22, 26, 30].each do |i|
# 	result_path = sprintf("#{solver_path}/%s", i)
# 	envs = "environment=[{name=RANGE,value=#{range}},{name=SUBMISSION_ID,value=#{solver_path}},{name=RESULT_PATH,value=#{result_path}}, {name=NC,value=#{i}}]"
# 	system('aws', *args, envs)
# end
