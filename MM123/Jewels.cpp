#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
#ifndef NDEBUG
// #define MEASURE_TIME
// #define DEBUG
#endif
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 3.126448e9;
constexpr ll TL = 4000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 9000;
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
constexpr inline T sq(T v) { return v * v; }

void debug_vec(const vi& vec) {
  for (int i = 0; i < vec.size(); ++i) {
    debug("%d ", vec[i]);
  }
  debugln();
}

XorShift rnd;
Timer timer;
Counter counter;

constexpr double PI = 3.141592653589793238;
constexpr int INF = 1 << 28;

//////// end of template ////////

struct Swap {
  int r1, c1, r2, c2;
};

bool operator<(const Swap& o1, const Swap& o2) {
  return o1.r1 < o2.r1;
}

constexpr int T = 1000;
int N, C;
array<array<int, 16>, 16> grid, grid_tmp;
array<array<array<int, 10>, 16>, 16> point_score;
vector<Swap> plan;
int time_used;
int turn;
vector<vvi> COMBO_PATTERN = {
  { // N=8
    {14,13,13,12,12,11,11,10},
    {-1,14,14,13,13,12,-1,10},
    { 5, 6, 6, 7, 7, 8, 8, 9},
    { 5,-1, 7, 8, 8, 9, 9,10},
    { 3, 3, 1, 1, 2, 1, 3, 3},
    { 4, 4, 3, 3, 2, 1, 2, 2},
    { 5,-1, 4, 4, 3, 3, 4, 4},
    { 6,-1,-1,-1, 4, 4,-1,11},
},
  { // N=9
    {17,16,16,15,15,14,13,13,12},
    {-1,17,17,16,16,15,14,14,12},
    { 6, 7, 7, 8, 8, 9,10,10,11},
    { 6,-1, 8, 9, 9,10,11,11,12},
    { 3, 3, 1, 1, 2, 1, 3, 3,12},
    { 4, 4, 3, 3, 2, 1, 2, 2, 4},
    { 5, 5, 4, 4, 3, 3, 4, 4, 5},
    { 6,-1, 5, 5, 4, 4, 5, 5,13},
    { 7,-1,-1,-1, 5, 5,-1,-1,-1},
  },
  { // N=10
    {20,19,19,18,17,17,16,15,15,14},
    {-1,20,20,19,18,18,17,16,16,14},
    { 7, 8, 8, 9,10,10,11,12,12,13},
    { 7, 9, 9,10,11,11,12,13,13,14},
    { 3, 3, 1, 1, 2, 1, 3, 3,-1,14},
    { 4, 4, 3, 3, 2, 1, 2, 2, 4, 4},
    { 5, 5, 4, 4, 3, 3, 4, 4, 5, 5},
    { 7, 6, 5, 5, 4, 4, 5, 5, 6, 6},
    { 8,-1, 6, 6, 5, 5, 6, 6,-1,15},
    {-1,-1,-1,-1, 6, 6,-1,-1,-1,-1},
  },
  { // N=11
    {22,21,21,20,20,19,18,18,17,17,16},
    {-1,22,22,21,21,20,19,19,18,18,15},
    { 8, 9, 9,10,10,11,11,12,12,13,14},
    { 8,-1,10,11,11,12,12,13,13,14,15},
    { 3, 3, 1, 1, 2, 1, 3, 3,14,-1,15},
    { 4, 4, 3, 3, 2, 1, 2, 2, 4, 4,16},
    { 5, 5, 4, 4, 3, 3, 4, 4, 5, 5,16},
    { 6, 6, 5, 5, 4, 4, 5, 5, 6, 6,17},
    { 7, 7, 6, 6, 5, 5, 6, 6, 7, 7,-1},
    { 8,-1, 7, 7, 6, 6, 7, 7,-1,-1,-1},
    { 9,-1,-1,-1, 7, 7,-1,-1,-1,-1,-1},
  },
  { // N=12
    {22,21,21,20,19,19,18,17,17,16,16,15},
    {-1,22,22,21,20,20,19,18,18,17,17,15},
    { 8, 9, 9,10,10,11,11,12,13,13,14,14},
    { 8,10,10,11,11,12,12,13,14,14,-1,15},
    { 3, 1, 1, 2, 1, 1, 3, 2, 1, 2, 2, 3},
    { 4, 3, 3, 1, 3, 3, 4, 3, 2, 3, 3, 4},
    { 5, 4, 4, 1, 4, 4, 5, 4, 2, 4, 4, 5},
    { 6, 5, 5, 3, 5, 5, 6, 5, 3, 5, 5, 6},
    { 7, 6, 6, 4, 6, 6, 7, 6, 4, 6, 6, 7},
    { 8, 7, 7, 5, 7, 7,-1, 7, 5, 7, 7,16},
    { 9,-1,-1, 6,-1,-1,-1,-1, 6,-1,-1,-1},
    {-1,-1,-1, 7,-1,-1,-1,-1, 7,-1,-1,-1},
  },
  { // N=13
    {24,23,23,22,21,21,20,19,19,18,17,17,16},
    {-1,24,24,23,22,22,21,20,20,19,18,18,16},
    { 9,10,10,11,11,12,12,13,13,14,14,15,15},
    { 9,11,11,12,12,13,13,14,14,15,15,-1,16},
    { 3, 1, 1, 2, 1, 1, 3, 2, 2, 1, 2, 2, 3},
    { 4, 3, 3, 1, 3, 3, 4, 3, 3, 2, 3, 3, 4},
    { 5, 4, 4, 1, 4, 4, 5, 4, 4, 2, 4, 4, 5},
    { 6, 5, 5, 3, 5, 5, 6, 5, 5, 3, 5, 5, 6},
    { 7, 6, 6, 4, 6, 6, 7, 6, 6, 4, 6, 6, 7},
    { 8, 7, 7, 5, 7, 7, 8, 7, 7, 5, 7, 7, 8},
    { 9, 8, 8, 6, 8, 8,-1, 8, 8, 6, 8, 8,17},
    {10,-1,-1, 7,-1,-1,-1,-1,-1, 7,-1,-1,-1},
    {-1,-1,-1, 8,-1,-1,-1,-1,-1, 8,-1,-1,-1},
  },
  { // N=14
    {26,27,27,28,28,29,30,30,31,32,32,33,33,34},
    {26,-1,28,29,29,30,31,31,32,33,33,34,34,-1},
    {25,25,24,24,23,22,22,21,20,20,19,18,18,17},
    {26,-1,25,25,24,23,23,22,21,21,20,19,19,17},
    { 8, 9, 9,10,11,11,12,13,13,14,15,15,16,16},
    { 8,10,10,11,12,12,13,14,14,15,16,16,-1,17},
    { 3, 3, 1, 1, 2, 1, 1, 3, 2, 2, 1, 2, 2, 3},
    { 4, 4, 3, 3, 1, 3, 3, 4, 3, 3, 2, 3, 3, 4},
    { 5, 5, 4, 4, 1, 4, 4, 5, 4, 4, 2, 4, 4, 5},
    { 6, 6, 5, 5, 3, 5, 5, 6, 5, 5, 3, 5, 5, 6},
    { 7, 7, 6, 6, 4, 6, 6, 7, 6, 6, 4, 6, 6, 7},
    { 8,-1, 7, 7, 5, 7, 7,-1, 7, 7, 5, 7, 7,18},
    { 9,-1,-1,-1, 6,-1,-1,-1,-1,-1, 6,-1,-1,-1},
    {27,-1,-1,-1, 7,-1,-1,-1,-1,-1, 7,-1,-1,-1},
  },
  { // N=15
    {29,30,30,31,31,32,33,33,34,35,35,36,37,37,38},
    {29,-1,31,32,32,33,34,34,35,36,36,37,38,38,-1},
    {28,27,27,26,26,25,24,24,23,22,22,21,20,20,19},
    {29,28,28,27,27,26,25,25,24,23,23,22,21,21,19},
    { 9,10,10,11,12,12,13,14,14,15,16,16,17,17,18},
    { 9,11,11,12,13,13,14,15,15,16,17,17,18,18,19},
    { 3, 3, 1, 1, 2, 1, 1, 3, 2, 2, 1, 2, 2, 3, 3},
    { 4, 4, 3, 3, 1, 3, 3, 4, 3, 3, 2, 3, 3, 4, 4},
    { 5, 5, 4, 4, 1, 4, 4, 5, 4, 4, 2, 4, 4, 5, 5},
    { 6, 6, 5, 5, 3, 5, 5, 6, 5, 5, 3, 5, 5, 6, 6},
    { 7, 7, 6, 6, 4, 6, 6, 7, 6, 6, 4, 6, 6, 7, 7},
    { 8, 8, 7, 7, 5, 7, 7, 8, 7, 7, 5, 7, 7, 8, 8},
    { 9,-1, 8, 8, 6, 8, 8,-1, 8, 8, 6, 8, 8,-1,20},
    {10,-1,-1,-1, 7,-1,-1,-1,-1,-1, 7,-1,-1,-1,-1},
    {30,-1,-1,-1, 8,-1,-1,-1,-1,-1, 8,-1,-1,-1,-1},
  },
  { // N=16
    {32,33,33,34,34,35,36,36,37,38,38,39,40,40,41,41},
    {32,-1,34,35,35,36,37,37,38,39,39,40,41,41,-1,-1},
    {31,30,30,29,28,28,27,26,26,25,24,24,23,22,22,21},
    {32,31,31,30,29,29,28,27,27,26,25,25,24,23,23,21},
    {10,11,11,12,13,13,14,15,15,16,17,17,18,19,19,20},
    {10,12,12,13,14,14,15,16,16,17,18,18,19,20,20,21},
    { 3, 3, 1, 1, 2, 1, 1, 3, 3, 2, 2, 1, 2, 2, 3, 3},
    { 4, 4, 3, 3, 1, 3, 3, 4, 4, 3, 3, 2, 3, 3, 4, 4},
    { 5, 5, 4, 4, 1, 4, 4, 5, 5, 4, 4, 2, 4, 4, 5, 5},
    { 6, 6, 5, 5, 3, 5, 5, 6, 6, 5, 5, 3, 5, 5, 6, 6},
    { 7, 7, 6, 6, 4, 6, 6, 7, 7, 6, 6, 4, 6, 6, 7, 7},
    { 8, 8, 7, 7, 5, 7, 7, 8, 8, 7, 7, 5, 7, 7, 8, 8},
    { 9, 9, 8, 8, 6, 8, 8, 9, 9, 8, 8, 6, 8, 8, 9, 9},
    {10,-1, 9, 9, 7, 9, 9,-1,-1, 9, 9, 7, 9, 9,-1,22},
    {11,-1,-1,-1, 8,-1,-1,-1,-1,-1,-1, 8,-1,-1,-1,-1},
    {33,-1,-1,-1, 9,-1,-1,-1,-1,-1,-1, 9,-1,-1,-1,-1},
  },
};
const vector<array<int, 4>> COMBO_START = {
  {4, 4, 5, 5},  // N=8
  {4, 4, 5, 5},  // N=9
  {4, 4, 5, 5},  // N=10
  {4, 4, 5, 5},  // N=11
  {4, 3, 4, 8},  // N=12
  {4, 3, 4, 9},  // N=13
  {6, 4, 6,10},  // N=14
  {6, 4, 6,10},  // N=15
  {6, 4, 6,11},  // N=16
};

struct ComboPreset {
  vvi pattern;
  vi count;
  vector<vector<bool>> graph;
  vector<vvi> disallow;
  vector<vector<pair<int, int>>> rev_pos;
  int sr1, sc1, sr2, sc2;

  int size() const {
    return count.size();
  }

  void init() {
    pattern = COMBO_PATTERN[N - 8];
    sr1 = COMBO_START[N - 8][0];
    sc1 = COMBO_START[N - 8][1];
    sr2 = COMBO_START[N - 8][2];
    sc2 = COMBO_START[N - 8][3];
    count.clear();
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        const int order = pattern[i][j];
        if (order == -1) {
          pattern[i][j] = -i;
          continue;
        }
        if (count.size() <= order) {
          count.resize(order + 1);
          rev_pos.resize(order + 1);
        }
        count[order]++;
        rev_pos[order].push_back({i, j});
      }
    }
    graph.assign(count.size(), vector<bool>(count.size()));
    disallow.assign(N, vvi(N));
    for (int i = N - 3; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        if (pattern[i][j] <= 0 && pattern[i - 1][j] > 0) {
          int start = pattern[i - 1][j];
          int row = i - 1;
          while (row >= 0 && pattern[row][j] > 0 && pattern[row][j] <= start) {
            row -= 1;
          }
          if (row == -1) continue;
          int below = pattern[row][j];
          if (below <= 0) continue;
          disallow[i][j].push_back(below);
        }
      }
    }
    if (pattern[1][1] <= 0 && pattern[1][2] > 0) {
      disallow[1][1].push_back(pattern[1][2]);
    }
    if (pattern[1][N - 2] <= 0 && pattern[1][N - 3] > 0) {
      disallow[1][N - 2].push_back(pattern[1][N - 3]);
    }

    graph[pattern[sr1][sc1]][pattern[sr2][sc2]] = graph[pattern[sr2][sc2]][pattern[sr1][sc1]] = true;
    swap(pattern[sr1][sc1], pattern[sr2][sc2]);
    while (true) {
      vector<vector<bool>> mark(N, vector<bool>(N));
      bool any = false;
      for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
          const int cur = pattern[i][j];
          if (cur <= 0) continue;
          if (j < N - 2 && cur == pattern[i][j + 1] && cur == pattern[i][j + 2]) {
            mark[i][j] = mark[i][j + 1] = mark[i][j + 2] = true;
            any = true;
          }
          if (i < N - 2 && cur == pattern[i + 1][j] && cur == pattern[i + 2][j]) {
            mark[i][j] = mark[i + 1][j] = mark[i + 2][j] = true;
            any = true;
          }
        }
      }
      if (!any) break;
      for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
          const int cur = pattern[i][j];
          if (cur <= 0) continue;
          // horz
          if (j < N - 1 && cur == pattern[i][j + 1]) {
            if (j > 0) {
              const int adj = pattern[i][j - 1];
              if (adj > 0 && adj != cur) {
                graph[cur][adj] = graph[adj][cur] = true;
              } else if (adj < 0 && adj != -INF) {
                disallow[-adj][j - 1].push_back(cur);
              }
            }
            if (j < N - 2) {
              const int adj = pattern[i][j + 2];
              if (adj > 0 && adj != cur) {
                graph[cur][adj] = graph[adj][cur] = true;
              } else if (adj < 0 && adj != -INF) {
                disallow[-adj][j + 2].push_back(cur);
              }
            }
          }
          if (j < N - 2 && cur == pattern[i][j + 2]) {
            const int adj = pattern[i][j + 1];
            if (adj > 0 && adj != cur) {
              graph[cur][adj] = graph[adj][cur] = true;
            } else if (adj < 0 && adj != -INF) {
              disallow[-adj][j + 1].push_back(cur);
            }
          }
          // vert
          if (i < N - 1 && cur == pattern[i + 1][j]) {
            if (i > 0) {
              const int adj = pattern[i - 1][j];
              if (adj > 0 && adj != cur) {
                graph[cur][adj] = graph[adj][cur] = true;
              } else if (adj < 0 && adj != -INF) {
                disallow[-adj][j].push_back(cur);
              }
            }
            if (i < N - 2) {
              const int adj = pattern[i + 2][j];
              if (adj > 0 && adj != cur) {
                graph[cur][adj] = graph[adj][cur] = true;
              } else if (adj < 0 && adj != -INF) {
                disallow[-adj][j].push_back(cur);
              }
            }
          }
          if (i < N - 2 && cur == pattern[i + 2][j]) {
            const int adj = pattern[i + 1][j];
            if (adj > 0 && adj != cur) {
              graph[cur][adj] = graph[adj][cur] = true;
            } else if (adj < 0 && adj != -INF) {
              disallow[-adj][j].push_back(cur);
            }
          }
        }
      }
      for (int i = 0; i < N; ++i) {
        int to = 0;
        for (int j = 0; j < N; ++j) {
          if (mark[j][i]) continue;
          pattern[to][i] = pattern[j][i];
          to++;
        }
        for (; to < N; ++to) {
          pattern[to][i] = -INF;
        }
      }
    }
    // debugStr("disallow:\n");
    for (int i = N - 1; i >= 0; --i) {
      for (int j = 0; j < N; ++j) {
        auto& dis = disallow[i][j];
        sort(dis.begin(), dis.end());
        dis.erase(unique(dis.begin(), dis.end()), dis.end());
        // debug("(%d %d) ", i, j);
        // for (int c : dis) {
        //   debug("%d ", c);
        // }
        // debugln();
      }
    }
    pattern = COMBO_PATTERN[N - 8];
  }
} combo_preset;

int full_sim() {
  int score = 0;
  int combo = 0;
  static array<array<bool, 16>, 16> mark = {};
  while (true) {
    bool any = false;
    for (int i = 0; i < N; ++i) {
      int start = 0;
      int len = 0;
      for (int j = 1; j < N; ++j) {
        if (grid_tmp[i][j] != grid_tmp[i][j - 1] || grid_tmp[i][j] == -1) {
          len = j - start;
          if (len >= 3) {
            score += sq(len - 2);
            for (int k = start; k < j; ++k) {
              mark[i][k] = true;
            }
            any = true;
          }
          start = j;
        }
      }
      len = N - start;
      if (len >= 3) {
        score += sq(len - 2);
        for (int k = start; k < N; ++k) {
          mark[i][k] = true;
        }
        any = true;
      }

      start = 0;
      for (int j = 1; j < N; ++j) {
        if (grid_tmp[j][i] != grid_tmp[j - 1][i] || grid_tmp[j][i] == -1) {
          len = j - start;
          if (len >= 3) {
            score += sq(len - 2);
            for (int k = start; k < j; ++k) {
              mark[k][i] = true;
            }
            any = true;
          }
          start = j;
        }
      }
      len = N - start;
      if (len >= 3) {
        score += sq(len - 2);
        for (int k = start; k < N; ++k) {
          mark[k][i] = true;
        }
        any = true;
      }
    }
    if (!any) break;
    combo++;
    for (int col = 0; col < N; ++col) {
      int to = 0;
      for (int row = 0; row < N; ++row) {
        if (!mark[row][col]) {
          grid_tmp[to][col] = grid_tmp[row][col];
          to++;
        }
      }
      for (int row = to; row < N; ++row) {
        grid_tmp[row][col] = -1;
      }
    }
    for (int i = 0; i < N; ++i) {
      fill(mark[i].begin(), mark[i].begin() + N, false);
    }
  }
  return score * combo;
  // int ret = (score * combo) << 16;
  // for (int i = 0; i < N; ++i) {
  //   for (int j = 0; j < N; ++j) {
  //     int cur = grid_tmp[i][j];
  //     if (cur == -1) continue;
  //     if (j != N - 1) {
  //       // horz 2 match
  //       if (grid_tmp[i][j + 1] == cur) {
  //         ret += 1;
  //         if (j > 0) {
  //           if (i < N - 1 && grid_tmp[i + 1][j - 1] == cur) {
  //             ret += 4;
  //           }
  //           if (i < N - 3 && grid_tmp[i + 3][j - 1] == cur) {
  //             ret += 1;
  //           }
  //           if (i > 0 && grid_tmp[i - 1][j - 1] == cur) {
  //             ret += 4;
  //           }
  //         }
  //         if (j < N - 2) {
  //           if (i < N - 1 && grid_tmp[i + 1][j + 2] == cur) {
  //             ret += 4;
  //           }
  //           if (i < N - 3 && grid_tmp[i + 3][j + 2] == cur) {
  //             ret += 1;
  //           }
  //           if (i > 0 && grid_tmp[i - 1][j + 2] == cur) {
  //             ret += 4;
  //           }
  //         }
  //       } else if (j != N - 2 && grid_tmp[i][j + 2] == cur) {
  //         ret += 1;
  //         if (i < N - 3 && grid_tmp[i - 3][j + 1] == cur) {
  //           ret += 1;
  //         }
  //       }
  //     }
  //     if (i != N - 1) {
  //       // vert 2 match
  //       if (grid_tmp[i + 1][j] == cur) {
  //         ret += 1;
  //         if (i > 1 && grid_tmp[i - 2][j] == cur) {
  //           ret += 2;
  //         }
  //         if (i < N - 3 && grid_tmp[i + 3][j] == cur) {
  //           ret += 2;
  //         }
  //       }
  //     }
  //   }
  // }
  // return ret;
}

int dfs_rep;
vi dfs_result;
constexpr int DFS_CUTOFF = 10000;

bool coloring_dfs(int depth, vi& j_cnt, vi& color) {
  dfs_rep++;
  if (depth == combo_preset.size()) {
    dfs_result = color;
    return true;
  }
  if (dfs_rep > DFS_CUTOFF) return false;
  int conflict = 0;
  for (int i = 0; i < depth; ++i) {
    if (combo_preset.graph[depth][i]) {
      conflict |= 1 << color[i];
    }
  }
  vi cand;
  static vi histo(C);
  fill(histo.begin(), histo.end(), 0);
  for (pair<int, int> p : combo_preset.rev_pos[depth]) {
    histo[grid[p.first][p.second]]++;
  }
  for (int i = 0; i < C; ++i) {
    if (j_cnt[i] < combo_preset.count[depth]) continue;
    if ((conflict & (1 << i)) == 0) {
      cand.push_back(((100 - histo[i] - rnd.nextUInt(3)) << 8) | i);
    }
  }
  if (cand.empty()) return false;
  sort(cand.begin(), cand.end());
  // for (int i = 0; i < cand.size() - 1; ++i) {
  //   int pos = rnd.nextUInt(cand.size() - i) + i;
  //   swap(cand[i], cand[pos]);
  // }
  for (int i = 0; i < cand.size(); ++i) {
    int c = cand[i] & 0xFF;
    color[depth] = c;
    j_cnt[c] -= combo_preset.count[depth];
    if (coloring_dfs(depth + 1, j_cnt, color)) {
      return true;
    }
    j_cnt[c] += combo_preset.count[depth];
  }
  return false;
}

vi coloring() {
  vi j_cnt(C);
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      j_cnt[grid[i][j]]++;
    }
  }
  vi color(combo_preset.size());
  dfs_result.clear();
  for (int i = 0; i < 10; ++i) {
    dfs_rep = 0;
    bool success = coloring_dfs(combo_preset.count[0] ? 0 : 1, j_cnt, color);
    if (success) {
      // debug("coloring_dfs found after:%d %d\n", i, dfs_rep);
      break;
    }
  }
  return dfs_result;
}

template<class T>
bool matched(const T& field, int r, int c) {
  int top = r;
  int bottom = r;
  int left = c;
  int right = c;
  const int cur = field[r][c];
  while (bottom > 0 && field[bottom - 1][c] == cur) {
    bottom--;
  }
  while (top < N - 1 && field[top + 1][c] == cur) {
    top++;
  }
  while (left > 0 && field[r][left - 1] == cur) {
    left--;
  }
  while (right < N - 1 && field[r][right + 1] == cur) {
    right++;
  }
  return top - bottom >= 2 || right - left >= 2;
}

pair<int, vector<Swap>> create_plan(const vi& color) {
  static vvi dis(N, vi(N));
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      if (combo_preset.pattern[i][j] < 0) {
        dis[i][j] = 0;
        for (int c : combo_preset.disallow[i][j]) {
          dis[i][j] |= (1 << color[c]);
        }
      } else {
        dis[i][j] = (1 << C) - 1;
        dis[i][j] -= 1 << color[combo_preset.pattern[i][j]];
      }
    }
  }
  for (int i = 0; i < N; ++i) {
    copy(grid[i].begin(), grid[i].begin() + N, grid_tmp[i].begin());
  }
  // debugStr("target:\n");
  // for (int i = N - 1; i >= 0; --i) {
  //   for (int j = 0; j < N; ++j) {
  //     if (combo_preset.pattern[i][j] >= 0) {
  //       debug("%d ", color[combo_preset.pattern[i][j]]);
  //     } else {
  //       debugStr("# ");
  //     }
  //   }
  //   debugln();
  // }
  // debugln();
  // debugStr("dis:\n");
  // for (int i = N - 1; i >= 0; --i) {
  //   for (int j = 0; j < N; ++j) {
  //     debug("%3x ", dis[i][j]);
  //   }
  //   debugln();
  // }
  // debugln();
  // debugStr("field:\n");
  // for (int i = N - 1; i >= 0; --i) {
  //   for (int j = 0; j < N; ++j) {
  //     debug("%d ", grid_tmp[i][j]);
  //   }
  //   debugln();
  // }
  // debugln();
  vector<Swap> cur_plan;
  bool left = false;
  for (int loop = 0; loop < 10; ++loop) {
    left = false;
    vi cands;
    for (int r = 0; r < N; ++r) {
      for (int c = 0; c < N; ++c) {
        if (combo_preset.pattern[r][c] >= 0 && color[combo_preset.pattern[r][c]] == grid_tmp[r][c]) {
          continue;
        }
        cands.push_back((r << 8) | c);
      }
    }

    // search dual matching swap first
    for (int p1 : cands) {
      const int r1 = p1 >> 8;
      const int c1 = p1 & 0xFF;
      if (combo_preset.pattern[r1][c1] < 0) continue;
      const int expect = color[combo_preset.pattern[r1][c1]];
      const int cur = grid_tmp[r1][c1];
      if (expect == cur) continue;
      for (int p2 : cands) {
        const int r2 = p2 >> 8;
        const int c2 = p2 & 0xFF;
        if (combo_preset.pattern[r2][c2] < 0) continue;
        const int oc = grid_tmp[r2][c2];
        if (cur == oc) continue;
        if (dis[r2][c2] & (1 << cur)) continue;
        if (dis[r1][c1] & (1 << oc)) continue;
        swap(grid_tmp[r1][c1], grid_tmp[r2][c2]);
        if (matched(grid_tmp, r1, c1) || matched(grid_tmp, r2, c2)) {
          swap(grid_tmp[r1][c1], grid_tmp[r2][c2]);
        } else {
          cur_plan.push_back({r1, c1, r2, c2});
          break;
        }
      }
    }

    for (int p1 : cands) {
      const int r1 = p1 >> 8;
      const int c1 = p1 & 0xFF;
      const int cur = grid_tmp[r1][c1];
      if ((dis[r1][c1] & (1 << cur)) == 0) continue;
      int used = 1 << cur;
      int gain = -1;
      int fr = r1;
      int fc = c1;
      while (true) {
        int sr = 0;
        int sc = 0;
        for (int p2 : cands) {
          const int r2 = p2 >> 8;
          const int c2 = p2 & 0xFF;
          const int oc = grid_tmp[r2][c2];
          if (cur == oc) continue;
          if (combo_preset.pattern[r2][c2] >= 0 && color[combo_preset.pattern[r2][c2]] == oc) continue;
          if (dis[fr][fc] & (1 << oc)) continue;
          int cur_gain = 1 + ((dis[r2][c2] & (1 << oc)) != 0) - ((dis[r2][c2] & (1 << cur)) != 0);
          if (cur_gain <= gain) continue;
          if (cur_gain != 2 && (used & (1 << oc))) continue;
          swap(grid_tmp[fr][fc], grid_tmp[r2][c2]);
          if (!matched(grid_tmp, fr, fc) && !matched(grid_tmp, r2, c2)) {
            gain = cur_gain;
            sr = r2;
            sc = c2;
          }
          swap(grid_tmp[fr][fc], grid_tmp[r2][c2]);
          if (gain == 2) break;
        }
        if (gain == -1) {
          left = true;
          break;
        } else {
          // debug("swap:(%d %d) (%d %d)\n", fr, fc, sr, sc);
          swap(grid_tmp[fr][fc], grid_tmp[sr][sc]);
          cur_plan.push_back({fr, fc, sr, sc});
          if ((dis[sr][sc] & (1 << cur)) == 0) {
            break;
          }
          used |= (1 << grid_tmp[fr][fc]);
          fr = sr;
          fc = sc;
          gain = -1;
        }
      }
    }
    if (!left) break;
  }
  if (left) {
    // debug("create_plan fail:%lu\n", cur_plan.size());
    // debugStr("field:\n");
    // for (int i = N - 1; i >= 0; --i) {
    //   for (int j = 0; j < N; ++j) {
    //     debug("%d ", grid_tmp[i][j]);
    //   }
    //   debugln();
    // }
    // debugln();
    return {0, cur_plan};
  }
  cur_plan.push_back({combo_preset.sr1, combo_preset.sc1, combo_preset.sr2, combo_preset.sc2});
  swap(grid_tmp[combo_preset.sr1][combo_preset.sc1], grid_tmp[combo_preset.sr2][combo_preset.sc2]);
  reverse(cur_plan.begin(), cur_plan.end());
  int top_pena_count = 0;
  for (int i = 0; i < N; ++i) {
    if (combo_preset.pattern[0][i] == -1 && combo_preset.pattern[1][i] == -1 &&
        combo_preset.pattern[2][i] != -1 && grid_tmp[0][i] == grid_tmp[1][i]) {
      top_pena_count++;
    }
  }
  // debug("create_plan success:%lu\n", cur_plan.size());
  int estimated_score = full_sim();
  estimated_score *= pow(0.9, top_pena_count);
  return {estimated_score, cur_plan};
}

int calc_point_score(int r, int c, int color) {
  int left = c;
  int right = c;
  int top = r;
  int bottom = r;
  while (left > 0 && grid[r][left - 1] == color) {
    left--;
  }
  while (right < N - 1 && grid[r][right + 1] == color) {
    right++;
  }
  while (bottom > 0 && grid[bottom - 1][c] == color) {
    bottom--;
  }
  while (top < N - 1 && grid[top + 1][c] == color) {
    top++;
  }
  int hlen = right - left + 1;
  int vlen = top - bottom + 1;
  int ret = N - r;
  if (hlen >= 3) {
    ret += sq(hlen - 2) * 10;
  } else {
    ret += hlen;
  }
  if (vlen >= 3) {
    ret += sq(vlen - 2) * 10;
    ret += r - bottom;
  } else {
    ret += vlen;
  }
  return ret;
}

int swap_score(int r1, int c1, int r2, int c2) {
  if (grid[r1][c1] == grid[r2][c2]) return 0;
  swap(grid[r1][c1], grid[r2][c2]);
  int s = calc_point_score(r1, c1, grid[r1][c1]) + calc_point_score(r2, c2, grid[r2][c2]);
  swap(grid[r1][c1], grid[r2][c2]);
  return s;
}

Swap greedy() {
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      for (int k = 0; k < C; ++k) {
        point_score[i][j][k] = calc_point_score(i, j, k);
      }
    }
  }

  static vector<uint32_t> cands;
  cands.clear();
  for (int r1 = 0; r1 < N; ++r1) {
    for (int c1 = 0; c1 < N; ++c1) {
      for (int r2 = r1; r2 < N; ++r2) {
        for (int c2 = r2 == r1 ? c1 + 1 : 0; c2 < N; ++c2) {
          uint32_t s;
          if (r1 == r2 || c1 == c2) {
            s = swap_score(r1, c1, r2, c2);
          } else {
            s = point_score[r1][c1][grid[r2][c2]] + point_score[r2][c2][grid[r1][c1]];
          }
          if (s >= (1 << 16)) s = 0xFFFF;
          uint32_t v = (s << 16) | (r1 << 12) | (c1 << 8) | (r2 << 4) | (c2);
          cands.push_back(v);
        }
      }
    }
  }
  // const int sz = min((int)cands.size(), 2500);
  // nth_element(cands.begin(), cands.end() - sz, cands.end());
  sort(cands.begin(), cands.end());
  Swap ret;
  int best_score = 0;
  int time_limit = (TL - time_used) / (T - turn);
  for (int i = 0; i < cands.size(); ++i) {
    if (i > 0 && (i & 0x7F) == 0) {
      auto elapsed = get_elapsed_msec();
      if (elapsed > time_limit) {
        // debug("t:%d turn:%d\n", t, i);
        break;
      }
    }
    for (int j = 0; j < N; ++j) {
      copy(grid[j].begin(), grid[j].begin() + N, grid_tmp[j].begin());
    }
    int v = cands[cands.size() - 1 - i];
    int r1 = (v >> 12) & 0xF;
    int c1 = (v >> 8) & 0xF;
    int r2 = (v >> 4) & 0xF;
    int c2 = (v >> 0) & 0xF;
    swap(grid_tmp[r1][c1], grid_tmp[r2][c2]);
    int s = full_sim();
    if (s > best_score) {
      best_score = s;
      ret.r1 = r1;
      ret.c1 = c1;
      ret.r2 = r2;
      ret.c2 = c2;
    }
  }
  return ret;
}

vi plan_sizes;
bool final_phase = false;

Swap solve() {
  if (!plan.empty()) {
    Swap ret = plan.back();
    plan.pop_back();
    return ret;
  }
  if (final_phase) {
    return greedy();
  }
  debug("\nconstruct turn:%d elapsed:%d\n", turn, time_used);
  double best_ef = 0.0;
  for (int trial = 0; ; ++trial) {
    if (trial == 50 && best_ef == 0.0) {
      break;
    }
    if (trial >= 50) {
      const auto elapsed = get_elapsed_msec();
      if (elapsed + time_used > TL * (turn + plan.size()) / T) {
        debug("construct trial:%d\n", trial);
        if (!plan.empty()) {
          plan_sizes.push_back(plan.size());
          sort(plan_sizes.begin(), plan_sizes.end());
          // min_plan_size = min(min_plan_size, (int)plan.size());
        }
        break;
      }
    }
    const auto colors = coloring();
    if (colors.size() < combo_preset.size()) {
      continue;
    }
    auto cur_plan = create_plan(colors);
    if (cur_plan.first == 0 || cur_plan.second.size() > T - turn) continue;
    const double ef = cur_plan.first / cur_plan.second.size();
    if (ef > best_ef) {
      debug("trial:%d score:%d len:%lu ef:%.2f\n", trial, cur_plan.first, cur_plan.second.size(), ef);
      best_ef = ef;
      swap(plan, cur_plan.second);
    }
  }
  if (plan.empty() && !plan_sizes.empty() && turn + plan_sizes[plan_sizes.size() / 2] + 3 > T) {
    debugStr("enter final phase\n");
    final_phase = true;
    int max_pat = combo_preset.size() - 1;
    for (int i = max_pat; i > 10; --i) {
      for (int j = 0; j < N; ++j) {
        for (int k = 0; k < N; ++k) {
          if (COMBO_PATTERN[N - 8][j][k] == i) {
            COMBO_PATTERN[N - 8][j][k] = -1;
          }
        }
      }
      combo_preset.init();
      for (int trial = 0; trial < 100; ++trial) {
        if (trial == 50 && plan.empty()) break;
        const auto colors = coloring();
        if (colors.size() < combo_preset.size()) {
          continue;
        }
        auto cur_plan = create_plan(colors);
        if (cur_plan.first == 0 || cur_plan.second.size() > T - turn) continue;
        const double ef = cur_plan.first / cur_plan.second.size();
        if (ef > best_ef) {
          debug("trial:%d score:%d len:%lu ef:%.2f\n", trial, cur_plan.first, cur_plan.second.size(), ef);
          best_ef = ef;
          swap(plan, cur_plan.second);
        }
      }
      if (!plan.empty()) {
        debug("found at max_pat:%d\n", i);
        break;
      }
    }
  }
  if (plan.empty()) {
    // fallback
    return greedy();
  } else {
    Swap ret = plan.back();
    plan.pop_back();
    return ret;
  }
}

int main() {
  scanf("%d %d", &N, &C);
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; ++j) {
      scanf("%d", &grid[i][j]);
      grid[i][j]--;
    }
  }
  combo_preset.init();
  // for (int i = 0; i < combo_preset.count.size(); ++i) {
  //   for (int j = 0; j < combo_preset.count.size(); ++j) {
  //     debug("%d", (int)combo_preset.graph[i][j]);
  //   }
  //   debugln();
  // }
  // debugln();
  start_time = get_time();
  for (turn = 0; turn < 1000; turn++) {
    Swap s = solve();
    printf("%d %d %d %d\n", s.r1, s.c1, s.r2, s.c2);
    fflush(stdout);
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < N; ++j) {
        scanf("%d", &grid[i][j]);
        grid[i][j]--;
      }
    }
    scanf("%d", &time_used);
    start_time = get_time();
  }
}
