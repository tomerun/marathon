# MM123

新しい方が上。


## 2021-02-03

あと8時間。重要度順にやれることをやっていく

### 連鎖を何パターンか生成してシミュレーションしてスコア効率が最大になるやつを選ぶ

=> やった。想像以上によくなっている。微妙に制限時間超えちゃってるけど
C=5もgreedy解に比べて5%減くらいまで来てわからない感じになってきた

```
C = 5, N = 8, Seed = 1, Score = 44317.0, RunTime = 4051 ms
C = 10, N = 16, Seed = 2, Score = 348909.0, RunTime = 4493 ms
C = 5, N = 12, Seed = 3, Score = 145747.0, RunTime = 4082 ms
C = 5, N = 11, Seed = 4, Score = 94812.0, RunTime = 4154 ms
C = 8, N = 13, Seed = 5, Score = 157029.0, RunTime = 4206 ms
C = 8, N = 14, Seed = 6, Score = 221886.0, RunTime = 4253 ms
C = 7, N = 14, Seed = 7, Score = 198587.0, RunTime = 4121 ms
C = 5, N = 12, Seed = 8, Score = 140929.0, RunTime = 4197 ms
C = 8, N = 9, Seed = 9, Score = 59860.0, RunTime = 4096 ms
C = 9, N = 12, Seed = 10, Score = 127399.0, RunTime = 4254 ms
```

### 終了間際は間に合う範囲でやる

=> やった。seed 1-10 ではなんか悪化して見えるがたまたまだと思う…たぶん…

600件で0.5%くらいしか変わってないな…なんでだろう。まあ良くはなってるからとりあえずこれで

実行時間は安定するようになって Max. Run Time: 4033 ms だ

### パターンに色を割り当てるのをgreedyにやってswap回数少なくできるようにする

=> やった。ムムッ思いのほか影響大きいぞ。C=5もgreedyを逆転しちゃった
総合でも2割伸びた。

```
C = 5, N = 8, Seed = 1, Score = 51467.0, RunTime = 3922 ms
C = 10, N = 16, Seed = 2, Score = 451256.0, RunTime = 4002 ms
C = 5, N = 12, Seed = 3, Score = 146247.0, RunTime = 4001 ms
C = 5, N = 11, Seed = 4, Score = 107701.0, RunTime = 4001 ms
C = 8, N = 13, Seed = 5, Score = 197550.0, RunTime = 4001 ms
C = 8, N = 14, Seed = 6, Score = 260481.0, RunTime = 4001 ms
C = 7, N = 14, Seed = 7, Score = 258704.0, RunTime = 4009 ms
C = 5, N = 12, Seed = 8, Score = 158770.0, RunTime = 4001 ms
C = 8, N = 9, Seed = 9, Score = 60690.0, RunTime = 3952 ms
C = 9, N = 12, Seed = 10, Score = 152454.0, RunTime = 4001 ms
```

### 暴発率下げ：コンボ数を少々減らしてでも盤面外から降ってくるもので暴発しにくいようパターンを微調整

seed=3 がやたら変わってる
```
C = 5, N = 8, Seed = 1, Score = 51902.0, RunTime = 3969 ms
C = 10, N = 16, Seed = 2, Score = 447929.0, RunTime = 4002 ms
C = 5, N = 12, Seed = 3, Score = 213808.0, RunTime = 4000 ms
C = 5, N = 11, Seed = 4, Score = 124882.0, RunTime = 4001 ms
C = 8, N = 13, Seed = 5, Score = 165855.0, RunTime = 4001 ms
C = 8, N = 14, Seed = 6, Score = 210637.0, RunTime = 4001 ms
C = 7, N = 14, Seed = 7, Score = 224192.0, RunTime = 3831 ms
C = 5, N = 12, Seed = 8, Score = 200964.0, RunTime = 4001 ms
C = 8, N = 9, Seed = 9, Score = 60046.0, RunTime = 3953 ms
C = 9, N = 12, Seed = 10, Score = 135980.0, RunTime = 4001 ms
```

N=11,12,14,15をいじったが
* 11 微増
* 12 1割up
* 14,15 微減

という感じ。Nによってだいぶ様相が違う

N=13 もよくなった。コンボ2減ってるのだがスコアは6%増。

### 暴発率下げ：連鎖に絡まないジュエルで降ってきたものと加えて暴発する可能性がある箇所をつぶしていく

やったが意味あるかないかわからないくらいの差

### 過去の平均よりも1割以上悪い結果しか得られないなら適当にどこか消して次ターンでやり直し

やってみたが良くない。

### swapを構築する戦略を改善して回数少なくできるようにする

軽くやったら5%上がった！　やっぱりここ重要か

```
C = 5, N = 8, Seed = 1, Score = 57338.0, RunTime = 3991 ms
C = 10, N = 16, Seed = 2, Score = 413259.0, RunTime = 4001 ms
C = 5, N = 12, Seed = 3, Score = 198663.0, RunTime = 4001 ms
C = 5, N = 11, Seed = 4, Score = 124951.0, RunTime = 4001 ms
C = 8, N = 13, Seed = 5, Score = 218219.0, RunTime = 4013 ms
C = 8, N = 14, Seed = 6, Score = 266559.0, RunTime = 4049 ms
C = 7, N = 14, Seed = 7, Score = 257828.0, RunTime = 3859 ms
C = 5, N = 12, Seed = 8, Score = 203728.0, RunTime = 4001 ms
C = 8, N = 9, Seed = 9, Score = 65817.0, RunTime = 3902 ms
C = 9, N = 12, Seed = 10, Score = 156698.0, RunTime = 4004 ms
```

もう少し詰めよう

swap位置を探すの、グリッドを順に一つずつ見ていくのだったが、
1箇所swapしてswap先がまだ条件を満たしていなかったら再帰的にその位置から探すようにした

0.5%up


### 連鎖パターンの左右反転版も試してみる

やってみたが良くない。



実行時間を 4s -> 9s にすると 1% くらいは上がるっぽい

これけっこう実行のたびスコア安定しないのにwleiteさんが99.9近く取ってるのすごいんだよなあ、
自己対戦内のベストが95なんだけど…　順位表のバグのせいかも？


できることがなくなってきたのでビジュアライザを眺めて過ごす

=> やっぱり上から降ってくるものでの暴発がたまにあるので同じ列の上2個が連鎖に関係ない位置だったら、その2個が同じ色になりづらいような評価を入れてみる


あと1時間くらいなので一度2000件でテスト流しておく

=> fail は0件。制限2秒のところ 2660ms かかっているケースがある。
終了間際の処理をちょっと攻めすぎた感あるので安全寄りにする。

と言ってたけど最後高速化を入れられそうだったので入れてしまった。ドキドキサブミット

exampleが正常に動いたのを確認して一安心。

```
seed=1   58362
seed=2  437297
seed=3  223773
seed=4  123311
seed=5  213032
seed=6  286437
seed=7  277949
seed=8  186925
seed=9   70323
seed=10 155972
```

ビジュアライザの動画撮ろう

=> 97.64 で暫定2位 （まあ順位表バグってるのでよくわからんけど…）
=> 順位表更新されて88.65になって暫定3位（えー）



## 2021-02-02

なんとか各Nでのパターンを埋め込んだ。大変。ちゃんとスコアは良くなっている

パターン作ってて気付いたけど、長さNのラインは1つだけじゃなくて何列か作ってmove score増やした方がスコアよくなる。よく調整された問題だなあ

動かしてみてわかったが、暴発して連鎖が途切れさせられるのをどうやって防ぐのかが本質の問題ですね…やっとスタートラインに立った感。

Cが小さい方がかえって難しい

多少スコア効率を悪くしてでも暴発しない形にするのが必要になってきそう

```
C = 5, N = 8, Seed = 1, Score = 28684.0, RunTime = 681 ms
C = 10, N = 16, Seed = 2, Score = 165127.0, RunTime = 1458 ms
C = 5, N = 12, Seed = 3, Score = 91674.0, RunTime = 678 ms
C = 5, N = 11, Seed = 4, Score = 62539.0, RunTime = 619 ms
C = 8, N = 13, Seed = 5, Score = 100377.0, RunTime = 182 ms
C = 8, N = 14, Seed = 6, Score = 115444.0, RunTime = 2471 ms
C = 7, N = 14, Seed = 7, Score = 89648.0, RunTime = 771 ms
C = 5, N = 12, Seed = 8, Score = 84366.0, RunTime = 446 ms
C = 8, N = 9, Seed = 9, Score = 43333.0, RunTime = 106 ms
C = 9, N = 12, Seed = 10, Score = 75863.0, RunTime = 647 ms
```

C=5ではだいぶ悪化している。まあこれは積み込みしないことになるが

時間は特に気にしていなかったけど600ケース中一番時間がかかっているケースで1秒強。

時間管理して暴発しないケースをできるだけ探すようなことをやるべき

順位表的には今からあと倍くらいにしないといけないのかな…？

やっとパズルフェーズが終わってマラソンっぽくなってきた

ほんのちょっとした暴発率下げをやっただけで8%くらいよくなった。なるほどなあ


## 2021-02-01

頑張ってN=8,9で大連鎖積み込みがそれなりに動くようにした。

いろいろ雑で連鎖中の暴発もよくあってまだまだという感じだがとりあえずは。

上から降ってくる分で暴発しちゃうのはある程度どうしようもないけど自分でシミュレーションしたらわかる分は最低限避けなければ

やること

* N=16まで全部パターン作る
* 積み込みのswap回数最小化
* パターンの改善
  * 連鎖密度や暴発のしづらさ
* C=小 では既存の毎ターンgreedyベースのも使うのでそれの改善

全然時間足りんな


## 2021-01-31

積み込んで画面全体連鎖にするのが基本な気がしてきたな。

で、長さ3で組むよりも4で組むべきっぽい

N=8で64マス中60マス使って連鎖させるとすると（実際ここまで高密度にするのは無理だろうけど）、

* 長さ3: 20連鎖、move score = 20 * 1 -> スコア400
* 長さ4: 15連鎖、move score = 15 * 4 -> スコア900

と倍以上違う。長さ5はちょっと連鎖させるのが難しいので考えない

いや、しっかり積み込んだら長さ=Nのラインを作る事もできるな。多少コンボ数を減らしてでもmove scoreを倍くらいにできるので重要。

N=16で長さ16を作ってそれから連鎖をつなげたとすると、
それを作るのに30マスくらい使うので残り226セルのうち200箇所くらいを連鎖に使えるとして、

* 基本3で作る: 70連鎖、 move score = 216 + 70 * 1 = 286 -> スコア20000くらい
* 基本4で作る: 50連鎖、 move score = 216 + 50 * 4 = 416 -> スコア20000くらい

move scoreの216は最初の長さ16を作るときの分

そんなに変わらんかった。けどNが小さくなるにつれて4で作った方が強くなるだろうから、基本4で作れるならそうしたほうがよい

長さ16を作らずにN=16で全部短いラインにすると、

* 長さ3: 80連鎖、move score = 80 * 1 -> スコア6400
* 長さ4: 60連鎖、move score = 60 * 4 -> スコア14400

なので、Nが大きい範囲では長さNのラインを作った方が良い。

N=12の場合

* N=12+長さ3で連鎖: 34連鎖、 move score = 120 + 34 * 1 = 154 -> スコア5100くらい
* N=12+長さ4で連鎖: 25連鎖、 move score = 120 + 25 * 4 = 220 -> スコア5500くらい
* 全部長さ3で連鎖: 40連鎖、 move score = 40 * 1 = 40 -> スコア1600くらい
* 全部長さ4で連鎖: 30連鎖、 move score = 30 * 4 = 120 -> スコア3600くらい

N=8の場合

* N=8+長さ3で連鎖: 14連鎖、 move score = 38 + 14 * 1 = 52 -> スコア728くらい
* N=8+長さ4で連鎖: 10連鎖、 move score = 38 + 10 * 4 = 78 -> スコア780くらい
* 全部長さ3で連鎖: 20連鎖、 move score = 20 * 1 = 20 -> スコア400くらい
* 全部長さ4で連鎖: 15連鎖、 move score = 15 * 4 = 60 -> スコア900くらい


これ全サイズでパターン手書きして埋め込んだ方が良さそうだな…。
密度高く連鎖を構築する方法がサイズによってだいぶ違う

とりあえずNが小さいところからパターンを手書きしてみたので実装してみよう（結局今日は実装までできなかった）

最終的には、次に帰着されそう

* パターンの賢さ
* どこまで素早く構築して回数を回すか

ただしC=5では積み込みより毎ターン消しまくった方が良さそう。C=6はどっちか微妙


## 2021-01-30

テスターで

```
Grid[c].get(r)==Grid[c-1].get(r)
```

とIntegerインスタンスを `==` で比較しているところあるが、バグでは
でも絶対値が小さい範囲のIntegerはキャッシュされて常に同じオブジェクトになるから実際問題になることは無さそう
=> 絶対値が小さい範囲で同じ参照を指すようにしろというのはJavaの言語仕様で決まってるのか https://docs.oracle.com/javase/specs/jls/se8/html/jls-5.html#jls-5.1.7

たくさん消えるケースでテスターの上から落とす処理が遅いから高速化しよう。よく消えるケースで2秒くらいテスターのオーバーヘッドがある
いや全然消えないケースでもそうだ。IOの分かなあ


方針、一番大きい分かれ目は、積み込んで大連鎖狙うか、毎ターンそこそこ消すか、だ

現状の解を計測してみると、

* 最難ケース:seed=1500,N=8,C=10
    * コンボ数平均 1.435
    * コンボ数2乗平均 2.457
    * コンボ数中央値 1
    * コンボ数最大 4
* 最易ケース:seed=1099,N=16,C=5
    * コンボ数平均 6.517
    * コンボ数2乗平均 46.779
    * コンボ数中央値 6
    * コンボ数最大 20

スコアがコンボ数の2乗になるとすると、毎ターン2コンボするのと6ターンごとに5コンボするのが同じくらいか。
難しいケースでは積み込んだ方がいい説あるな

毎ターン方式の現状の解を改善してどれくらい良くなるかだが

とりあえず単純にリーチになってる位置の個数くらいで評価してみた

若干しか上がらない。C=5では悪化してるし

考えてみたら積み込むのそこまで難しくないかもしんないね。明日実装しよう


## 2021-01-28

問題を読んだ。インタラクティブか。
各ターン処理後の盤面しか与えられないから自分でスコアわからないのね。

テストケースは `C` の値の順に作る

連鎖ゲーなんだけど、積み込んで大連鎖みたいなのを狙うべきかと言われると、どうなんだろう？

落ちてくるので偶然連鎖ができる分もデカそうだし、どんどん消して回転数上げるのが重要そうだしなあ

あと積み込んでも落ちてくる分で不意に消えて連鎖が壊されるとかもありそう。無視できる確率かもだが

ビジュアライザのカスタマイズをやった。

* 全ターンの状態を覚えておいて巻き戻し可能に
* 画像じゃなくて数字で表現


とりあえず全通りのswap試して一番消える解のを選ぶ解を書いた。連鎖のシミュレーションまではやらない。
600ケースで Max. Run Time = 1414 ms だった

```
C = 5, N = 8, Seed = 1, Score = 21127.0, RunTime = 619 ms
C = 10, N = 16, Seed = 2, Score = 7061.0, RunTime = 792 ms
C = 5, N = 12, Seed = 3, Score = 44600.0, RunTime = 335 ms
C = 5, N = 11, Seed = 4, Score = 34559.0, RunTime = 250 ms
C = 8, N = 13, Seed = 5, Score = 9842.0, RunTime = 389 ms
C = 8, N = 14, Seed = 6, Score = 10228.0, RunTime = 510 ms
C = 7, N = 14, Seed = 7, Score = 14570.0, RunTime = 521 ms
C = 5, N = 12, Seed = 8, Score = 39766.0, RunTime = 333 ms
C = 8, N = 9, Seed = 9, Score = 6671.0, RunTime = 103 ms
C = 9, N = 12, Seed = 10, Score = 6809.0, RunTime = 295 ms
```

盤面の下の方をちょっと優先するようにしてみた。上と4%くらい違う。意外と差が大きい
```
C = 5, N = 8, Seed = 1, Score = 22409.0, RunTime = 136 ms
C = 10, N = 16, Seed = 2, Score = 6714.0, RunTime = 918 ms
C = 5, N = 12, Seed = 3, Score = 44309.0, RunTime = 374 ms
C = 5, N = 11, Seed = 4, Score = 39618.0, RunTime = 282 ms
C = 8, N = 13, Seed = 5, Score = 9808.0, RunTime = 436 ms
C = 8, N = 14, Seed = 6, Score = 10867.0, RunTime = 568 ms
C = 7, N = 14, Seed = 7, Score = 17235.0, RunTime = 592 ms
C = 5, N = 12, Seed = 8, Score = 44828.0, RunTime = 372 ms
C = 8, N = 9, Seed = 9, Score = 6287.0, RunTime = 117 ms
C = 9, N = 12, Seed = 10, Score = 6888.0, RunTime = 354 ms
```

各位置の評価結果をキャッシュしたら最大200msくらいになった。


さて連鎖を評価したいのだが

swapする位置が相互に影響することもあるから、位置を独立にやることはできんなあ

候補をピックアップして単純に連鎖でのスコアをシミュレーションした。
盤面外から落ちてくるやつは、消えないブロックが落ちてくるとする。

```
C = 5, N = 8, Seed = 1, Score = 39908.0, RunTime = 452 ms
C = 10, N = 16, Seed = 2, Score = 11053.0, RunTime = 786 ms
C = 5, N = 12, Seed = 3, Score = 109229.0, RunTime = 383 ms
C = 5, N = 11, Seed = 4, Score = 86198.0, RunTime = 289 ms
C = 8, N = 13, Seed = 5, Score = 17493.0, RunTime = 417 ms
C = 8, N = 14, Seed = 6, Score = 19142.0, RunTime = 532 ms
C = 7, N = 14, Seed = 7, Score = 30783.0, RunTime = 554 ms
C = 5, N = 12, Seed = 8, Score = 100884.0, RunTime = 381 ms
C = 8, N = 9, Seed = 9, Score = 9236.0, RunTime = 121 ms
C = 9, N = 12, Seed = 10, Score = 10674.0, RunTime = 315 ms
```

8割増しくらいになった

submit1 => 99.92526

サンプルそのままと思われるスコアが 3.26761 -> 1.27042

順位表のスコアの動きがわけわからなさすぎてウケる
これは見てはダメだ


さてどうやって評価するかだがあり得る方向性としては

* 良い形を検出する評価関数

```
x11
1yz
```

と並んでる、みたいなリーチっぽい形がある盤面を評価上げる

* ある程度候補を絞って乱択シミュレーションで期待値計算


* 期待値計算を頑張る

真面目に計算しなくても、消えるラインの候補となる列が何か所あるかを数える、くらいでもよさそう


