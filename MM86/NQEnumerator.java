import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

public class NQEnumerator {
	static final int DFS_LIMIT = 100000;
	static final int TIMELIMIT = 11 * 60 * 60 * 1000;
	Random rnd = new Random();
	PrintWriter writer;
	int dfsCount;
	int N;
	int[] colN;
	BitSet col;
	boolean[] diag1, diag2;
	int[] coords;
	AnsRepo repo;
	int[][] dist;
	int[] minDistSum;

	static class EnumerateThread extends Thread {
		int begin, end, id;

		EnumerateThread(int begin, int end, int id) {
			this.begin = begin;
			this.end = end;
			this.id = id;
		}

		public void run() {
			try {
				try (PrintWriter writer = new PrintWriter("embed" + id + ".txt")) {
					long startTime = System.currentTimeMillis();
					long timespan = TIMELIMIT / (end - begin + 1);
					for (int i = begin; i <= end; ++i) {
						NQEnumerator exec = new NQEnumerator(i);
						exec.writer = writer;
						exec.solveNormal2(startTime + timespan * (i - begin + 1));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) throws Exception {
		NQEnumerator exec = new NQEnumerator(13);
		exec.writer = new PrintWriter(System.out);
		exec.solveSpecial();
		exec.writer.flush();
		//		ArrayList<EnumerateThread> threads = new ArrayList<>();
		//		threads.add(new EnumerateThread(25, 38, 91));
		//		threads.add(new EnumerateThread(39, 50, 92));
		//		for (Thread th : threads) {
		//			th.start();
		//		}
		//		for (Thread th : threads) {
		//			th.join();
		//		}
	}

	NQEnumerator(int N) {
		this.N = N;
		initDist();
	}

	void initDist() {
		dist = new int[N][N];
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				dist[i][j] = Math.max(distFromCenter(i), distFromCenter(j));
			}
		}
		minDistSum = new int[N + 1];
		for (int i = N - 1; i >= 0; --i) {
			minDistSum[i] = minDistSum[i + 1] + dist[N / 2][i];
		}
	}

	void solveNormal() {
		repo = new AnsRepo(N);
		colN = new int[N];
		col = new BitSet(N);
		diag1 = new boolean[2 * N];
		diag2 = new boolean[2 * N];
		recNormal(0, 0);
		//		repo.print(writer);
		repo.printMetrics(writer);
	}

	void solveNormal2(long timelimit) {
		repo = new AnsRepo(N);
		colN = new int[N];
		col = new BitSet(N);
		diag1 = new boolean[2 * N];
		diag2 = new boolean[2 * N];
		coords = new int[N];
		for (int i = 0; i < N; ++i) {
			coords[i] = i;
		}
		for (int i = 0;; ++i) {
			if (System.currentTimeMillis() > timelimit) break;
			MovingNQueens.shuffle(coords, rnd);
			dfsCount = 0;
			col.clear();
			Arrays.fill(diag1, false);
			Arrays.fill(diag2, false);
			recNormal2(0, 0);
		}
		repo.print(writer);
		for (int i = repo.minMetrics; i < repo.answers.size() && i <= repo.upper; ++i) {
			writer.println("metrics:" + i + " count:" + repo.answers.get(i).size());
		}
	}

	void solveSpecial() {
		colN = new int[N];
		col = new BitSet(N);
		diag1 = new boolean[N];
		diag2 = new boolean[N];
		recSpecial(0);
	}

	void recNormal(int y, int metrics) {
		if (metrics + minDistSum[y] >= repo.upper) return;
		if (y == N) {
			repo.add(colN);
			return;
		}
		for (int x = col.nextClearBit(0); x < (y == 0 ? (N + 1) / 2 : N); x = col.nextClearBit(x + 1)) {
			if (!diag1[x + y] && !diag2[x - y + N - 1]) {
				colN[y] = x;
				col.set(x);
				diag1[x + y] = true;
				diag2[x - y + N - 1] = true;
				recNormal(y + 1, metrics + dist[y][x]);
				col.clear(x);
				diag1[x + y] = false;
				diag2[x - y + N - 1] = false;
			}
		}
	}

	void recNormal2(int y, int metrics) {
		if (metrics + minDistSum[y] >= repo.upper) return;
		if (y == N) {
			repo.add(colN);
			return;
		}
		++dfsCount;
		if (dfsCount >= DFS_LIMIT) return;
		for (int i = col.nextClearBit(0); i < N; i = col.nextClearBit(i + 1)) {
			int x = coords[i];
			if (!diag1[x + y] && !diag2[x - y + N - 1]) {
				colN[y] = x;
				col.set(i);
				diag1[x + y] = true;
				diag2[x - y + N - 1] = true;
				recNormal2(y + 1, metrics + dist[y][x]);
				col.clear(i);
				diag1[x + y] = false;
				diag2[x - y + N - 1] = false;
			}
		}
	}

	void recSpecial(int y) {
		if (y == N) {
			new Answer(colN).print(writer);
//			System.out.print("{");
//			for (int i = 0; i < N; ++i) {
//				System.out.print(colN[i] + ",");
//			}
//			System.out.println("},");
			return;
		}
		int d = y;
		int e = N - 1 - y;
		for (int x = 0; x < N; ++x) {
			if (!col.get(x) && !diag1[d] && !diag2[e]) {
				colN[y] = x;
				col.set(x);
				diag1[d] = true;
				diag2[e] = true;
				recSpecial(y + 1);
				col.clear(x);
				diag1[d] = false;
				diag2[e] = false;
			}
			++d;
			if (d == N) d = 0;
			++e;
			if (e == N) e = 0;
		}
	}

	int distFromCenter(int pos) {
		return pos <= (N - 1) / 2 ? (N - 1) / 2 - pos : pos - N / 2;
	}

}
