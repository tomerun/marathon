import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

public class Tester {
	static boolean debug;
	static final int MIN_NQ = 8, MAX_NQ = 100;
	static int reqNQ = -1, reqSZ = -1;
	int NQ; // number of queens
	int[] QR, QC; // coordinates of queens (initial -> current -> final)

	void generate(long seed, Result res) throws Exception {
		SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
		rnd.setSeed(seed);
		res.seed = seed;
		if (seed == 1) {
			NQ = MIN_NQ;
		} else {
			NQ = rnd.nextInt(MAX_NQ - MIN_NQ + 1) + MIN_NQ;
		}
		if (1001 <= seed && seed <= 2000) {
			NQ = (int) (seed - 1001) * (MAX_NQ - MIN_NQ + 1) / (2000 - 1000) + MIN_NQ;
		}
		if (reqNQ != -1) NQ = reqNQ;
		if (debug) {
			System.out.println("Number of queens = " + NQ);
		}

		// queens are placed randomly within a SZ x SZ square
		int min_sz = (int) Math.sqrt(NQ) + 1;
		int max_sz = min_sz * 2;
		int sz = rnd.nextInt(max_sz - min_sz + 1) + min_sz;
		if (reqSZ != -1) sz = reqSZ;
		if (debug) {
			System.out.println("Initial arrangement size = " + sz);
		}
		res.N = NQ;
		res.SZ = sz;

		QR = new int[NQ];
		QC = new int[NQ];
		char[][] queenPlaced = new char[sz][sz];
		for (int i = 0; i < sz; ++i) {
			Arrays.fill(queenPlaced[i], '.');
		}

		int nPlaced = 0;
		while (nPlaced < NQ) {
			int r = rnd.nextInt(sz);
			int c = rnd.nextInt(sz);
			if (queenPlaced[r][c] == '.') {
				queenPlaced[r][c] = 'Q';
				QR[nPlaced] = r;
				QC[nPlaced] = c;
				++nPlaced;
			}
		}
		if (debug) {
			for (int i = 0; i < sz; ++i) {
				System.out.println(new String(queenPlaced[i]));
			}
			System.out.println();
		}
	}

	boolean isValidMove(int dR, int dC) {
		return dR == 0 || dC == 0 || dR == dC || dR == -dC;
	}

	public Result runTest(long seed) throws Exception {
		Result res = new Result();
		generate(seed, res);
		int rawScore = 0;
		int maxMoves = NQ * 8;
		long startTime = System.currentTimeMillis();
		String[] moves = new MovingNQueens().rearrange(QR, QC);
		res.elapsed = System.currentTimeMillis() - startTime;
		res.ansLen = moves.length;
		if (moves.length > maxMoves) {
			addFatalError("Your return can contain at most " + maxMoves + " elements (contains " + moves.length + ").");
			return res;
		}

		for (int i = 0; i < moves.length; ++i) {
			String[] s = moves[i].split(" ");
			if (s.length != 3) {
				addFatalError("Each element of your return must be formatted as \"INDEX ROW COL\" (element " + i + " is \""
						+ moves[i] + "\").");
				return res;
			}
			int ind, newRow, newCol;
			try {
				ind = Integer.parseInt(s[0]);
				newRow = Integer.parseInt(s[1]);
				newCol = Integer.parseInt(s[2]);
			} catch (Exception e) {
				addFatalError("Each element of your return must be formatted as \"INDEX ROW COL\" (element " + i + " is \""
						+ moves[i] + "\").");
				return res;
			}
			if (ind < 0 || ind >= NQ) {
				addFatalError("Element " + i + ": invalid queen index " + ind + ".");
				return res;
			}
			// valid move direction: horizontal, vertical or diagonal
			int dRow = newRow - QR[ind];
			int dCol = newCol - QC[ind];
			if (dRow == 0 && dCol == 0) {
				addFatalError("Element " + i + ": a queen must move at least one square (queen " + ind + " stayed at ("
						+ newRow + "," + newCol + ")).");
				return res;
			}
			if (!isValidMove(dRow, dCol)) {
				addFatalError("Element " + i
						+ ": a queen can move in a straight line vertically, horizontally or diagonally only (queen " + ind
						+ " moved by (" + dRow + "," + dCol + ")).");
				return res;
			}
			// extract move distance and move direction
			int dist = Math.max(Math.abs(dRow), Math.abs(dCol));
			dRow /= dist;
			dCol /= dist;
			// valid move: over unoccupied squares only, to an unoccupied cell
			for (int j = 0; j < NQ; ++j) {
				if (ind == j) {
					continue;
				}
				// check that queen j is not in the way of the moving one
				int dRowJ = QR[j] - QR[ind];
				int dColJ = QC[j] - QC[ind];
				if (!isValidMove(dRowJ, dColJ)) {
					continue;
				}
				int distJ = Math.max(Math.abs(dRowJ), Math.abs(dColJ));
				dRowJ /= distJ;
				dColJ /= distJ;
				if (dRow == dRowJ && dCol == dColJ && distJ <= dist) {
					addFatalError("Element " + i + ": a queen can move only over unoccupied squares (queen " + ind
							+ " passed queen " + j + " at (" + QR[j] + "," + QC[j] + ")).");
					return res;
				}
			}

			// valid move - do it and update score (Chebyshev length of the move)
			rawScore += Math.max(Math.abs(newRow - QR[ind]), Math.abs(newCol - QC[ind]));
			QR[ind] = newRow;
			QC[ind] = newCol;
		}

		// now all moves are done - check that the result is valid
		for (int i = 0; i < NQ; ++i)
			for (int j = i + 1; j < NQ; ++j) {
				if (isValidMove(QR[i] - QR[j], QC[i] - QC[j])) {
					addFatalError("After all moves queens " + i + " and " + j + " threaten each other.");
					return res;
				}
			}

		res.score = rawScore;
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) throws Exception {
		long seed = 1;
		long begin = -1;
		long end = -1;
		debug = false;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-N")) reqNQ = Integer.parseInt(args[++i]);
			if (args[i].equals("-SZ")) reqSZ = Integer.parseInt(args[++i]);
			if (args[i].equals("-debug")) debug = true;
		}

		if (begin != -1 && end != -1) {
			ArrayList<Long> seeds = new ArrayList<Long>();
			for (long i = begin; i <= end; ++i) {
				seeds.add(i);
			}
			int len = seeds.size();
			Result[] results = new Result[len];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			int prev = 0;
			for (int i = 0; i < THREAD_COUNT; ++i) {
				int next = len * (i + 1) / THREAD_COUNT;
				threads[i] = new TestThread(prev, next - 1, seeds, results);
				prev = next;
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].start();
			}
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i].join();
			}
			double sum = 0;
			for (int i = 0; i < results.length; ++i) {
				System.out.println(results[i]);
				System.out.println();
				sum += results[i].score;
			}
			System.out.println("ave:" + (sum / results.length));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	void addFatalError(String message) {
		System.out.println(message);
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
				}
			}
		}
	}

	static class Result {
		long seed;
		int N, SZ;
		int score;
		long elapsed;
		int ansLen;

		public String toString() {
			String ret = String.format("seed:%4d\n", seed);
			ret += String.format("N:%3d SZ:%2d\n", N, SZ);
			ret += String.format("length:%3d\n", ansLen);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("score:%4d", score);
			return ret;
		}
	}

}
