import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class SASolver {

	static int N;
	static XorShift rnd = new XorShift();
	static AnsRepo repo;
	static double diffRatio;
	static final double[] pows = { 0.25, 0.25, 0.28, 0.37, 0.45, 0.51, 0.57, 0.6, 0.65, 0.7, 0.7, 0.7 };

	public static void main(String[] args) {
		for (N = 19; N <= 19; ++N) {
			repo = new AnsRepo(N);
			generate();
		}
	}

	static void generate() {
		double pow = (pows[N / 10] * (10 - N % 10) + pows[N / 10 + 1] * (N % 10)) / 10;
		diffRatio = Math.pow(N, pow);
		final long TIMELIMIT = 5000;
		long startTime = System.currentTimeMillis();
		for (int i = 0;; ++i) {
			if (System.currentTimeMillis() - startTime > TIMELIMIT) {
				break;
			}
			int[] ans = solveSA();
			if (ans == null) continue;
			repo.add(ans);
		}
		repo.print();
	}

	static int[] solveSA() {
		int[] cols = new int[N];
		int[] diag1 = new int[N * 2];
		int[] diag2 = new int[N * 2];
		for (int i = 0; i < N; ++i) {
			cols[i] = i;
		}
		shuffle(cols);
		Arrays.fill(diag1, 0);
		Arrays.fill(diag2, 0);
		int conflict = 0;
		for (int i = 0; i < N; ++i) {
			diag1[i + cols[i]]++;
			diag2[i - cols[i] + N - 1]++;
		}
		for (int i = 1; i < 2 * N - 2; ++i) {
			if (diag1[i] > 1) conflict += (diag1[i] - 1) * (diag1[i] - 1);
			if (diag2[i] > 1) conflict += (diag2[i] - 1) * (diag2[i] - 1);
		}
		int r1 = 0;
		for (int turn = 0; turn < 1_000_000_000; ++turn) {
			int r2 = rnd.nextInt(N - 1);
			if (r2 >= r1) ++r2;
			int d1o = r1 + cols[r1];
			int d2o = r2 + cols[r2];
			int d1n = r1 + cols[r2];
			int d2n = r2 + cols[r1];
			int e1o = r1 - cols[r1] + N - 1;
			int e2o = r2 - cols[r2] + N - 1;
			int e1n = r1 - cols[r2] + N - 1;
			int e2n = r2 - cols[r1] + N - 1;
			int diff = diffConflict(diag1, d1o, d2o, d1n, d2n) + diffConflict(diag2, e1o, e2o, e1n, e2n);
			int md = Math.max(Math.abs(r1 - N / 2), Math.abs(cols[r2] - N / 2));
			md += Math.max(Math.abs(r2 - N / 2), Math.abs(cols[r1] - N / 2));
			md -= Math.max(Math.abs(r1 - N / 2), Math.abs(cols[r1] - N / 2));
			md -= Math.max(Math.abs(r2 - N / 2), Math.abs(cols[r2] - N / 2));
			if (transit(diff * diffRatio + md)) {
				conflict += diff;
				//				System.out.println(conflict + " " + metrics);
				int tmp = cols[r1];
				cols[r1] = cols[r2];
				cols[r2] = tmp;
				--diag1[d1o];
				--diag1[d2o];
				++diag1[d1n];
				++diag1[d2n];
				--diag2[e1o];
				--diag2[e2o];
				++diag2[e1n];
				++diag2[e2n];
				if (conflict == 0) break;
			}
		}
		if (conflict != 0) return null;
		return cols;
	}

	static boolean transit(double diff) {
		if (diff <= 0) return true;
		return rnd.nextDouble() < Math.exp(-diff / (diffRatio / 4));
	}

	static int diffConflict(int[] diag, int v1o, int v2o, int v1n, int v2n) {
		int ret = 0;
		if (diag[v1o] >= 2) ret -= 2 * diag[v1o] - 3;
		--diag[v1o];
		if (diag[v2o] >= 2) ret -= 2 * diag[v2o] - 3;
		--diag[v2o];
		if (diag[v1n] >= 1) ret += 2 * diag[v1n] - 1;
		++diag[v1n];
		if (diag[v2n] >= 1) ret += 2 * diag[v2n] - 1;

		--diag[v1n];
		++diag[v2o];
		++diag[v1o];

		return ret;
	}

	static void shuffle(int[] a) {
		for (int i = 0; i < a.length; ++i) {
			int pos = rnd.nextInt(a.length - i) + i;
			int tmp = a[i];
			a[i] = a[pos];
			a[pos] = tmp;
		}
	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;
		static final double TO_DOUBLE = 1.0 / (1L << 31);

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		boolean nextBoolean() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return (w & 8) == 0;
		}

		double nextDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return Math.abs(w * TO_DOUBLE);
		}

		double nextSDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w * TO_DOUBLE;
		}
	}
}

class Answer {
	int[] cols;

	Answer(int[] cols) {
		this.cols = cols.clone();
		int[] tmp = cols;
		for (int i = 0; i < 2; ++i) {
			tmp = MovingNQueens.flipNQ(tmp);
			for (int j = 0; j < 4; ++j) {
				if (compare(this.cols, tmp) < 0) this.cols = tmp;
				tmp = MovingNQueens.rotNQ(tmp);
			}
		}
	}

	void print() {
		try (PrintWriter writer = new PrintWriter(System.out)) {
			print(writer);
		}
	}

	void print(PrintWriter writer) {
		if (cols.length <= 95) {
			for (int i = 0; i < cols.length; ++i) {
				char ch = (char) (cols[i] + (cols[i] < 95 ? ' ' : 161 - 95));
				if (ch == '"' || ch == '\\') {
					writer.print('\\');
				}
				writer.print(ch);
			}
		} else {
			for (int i = 0; i < cols.length; ++i) {
				writer.print((char) ('0' + cols[i] / 10));
				writer.print((char) ('0' + cols[i] % 10));
			}
		}
	}

	static int compare(int[] a1, int[] a2) {
		for (int i = 0; i < a1.length; ++i) {
			if (a1[i] < a2[i]) return -1;
			if (a1[i] > a2[i]) return 1;
		}
		return 0;
	}

	public int hashCode() {
		return Arrays.hashCode(cols);
	}

	public boolean equals(Object obj) {
		if (this == obj) return true;
		Answer other = (Answer) obj;
		return Arrays.equals(cols, other.cols);
	}
}

class AnsRepo {
	int N;
	int COUNT = 300;
	int minMetrics = 9999;
	int upper = 9999;
	ArrayList<HashSet<Answer>> answers = new ArrayList<>();

	AnsRepo(int N) {
		this.N = N;
	}

	void add(int[] ans) {
		int metrics = 0;
		for (int j = 0; j < N; ++j) {
			metrics += Math.max(j <= (N - 1) / 2 ? (N - 1) / 2 - j : j - N / 2, ans[j] <= (N - 1) / 2 ? (N - 1) / 2 - ans[j]
					: ans[j] - N / 2);
		}
		if (metrics >= upper) return;
		minMetrics = Math.min(minMetrics, metrics);
		while (answers.size() <= metrics) {
			answers.add(new HashSet<>());
		}
		int sum = 0;
		for (int j = minMetrics; j <= metrics; ++j) {
			sum += answers.get(j).size();
			if (sum >= COUNT) {
				upper = j;
				return;
			}
		}
		answers.get(metrics).add(new Answer(ans));
	}

	void print() {
		try (PrintWriter writer = new PrintWriter(System.out)) {
			print(writer);
		}
	}

	void print(PrintWriter writer) {
		int count = 0;
		writer.print("embed[" + N + "]=\"");
		for (int i = minMetrics; i < answers.size() && count < COUNT; ++i) {
			for (Answer answer : answers.get(i)) {
				answer.print(writer);
				++count;
				if (count == COUNT) break;
			}
		}
		writer.println("\";");
	}

	void printMetrics(PrintWriter writer) {
		for (int i = minMetrics; i < answers.size() && i <= upper; ++i) {
			writer.println("metrics:" + i + " count:" + answers.get(i).size());
		}
	}

}
