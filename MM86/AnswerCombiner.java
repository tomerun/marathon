import java.io.PrintWriter;

public class AnswerCombiner {

	static PrintWriter writer = new PrintWriter(System.out);
	static MovingNQueens obj = new MovingNQueens();

	public static void main(String[] args) {
		obj.init(new int[] { 0 }, new int[] { 0 });
		for (int i = 51; i <= 100; ++i) {
			combine(i);
		}
		writer.flush();
	}

	static void combine(int N) {
		AnsRepo repo = new AnsRepo(N);
		repo.COUNT = 100;
		int[] ans = new int[N];
		for (int pos = 0; pos < CombineSolver.embed[N].length(); pos += N) {
			decodeLegacy(ans, CombineSolver.embed[N], pos);
			repo.add(ans);
		}
		for (int pos = 0; pos < obj.embed[N].length(); pos += N <= 95 ? N : 2 * N) {
			MovingNQueens.decode(ans, obj.embed[N], pos);
			repo.add(ans);
		}
		repo.print(writer);
		repo.printMetrics(writer);
	}

	static void decodeLegacy(int[] nq, String str, int pos) {
		for (int i = 0; i < nq.length; ++i) {
			char ch = str.charAt(pos + i);
			nq[i] = ch < 127 ? ch - ' ' : ch + 95 - 161;
		}
	}

}
