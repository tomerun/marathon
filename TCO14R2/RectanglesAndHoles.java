import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RectanglesAndHoles {
	private static final boolean DEBUG = 1 == 1;
	private static final boolean MEASURE_TIME = 1 == 0;
	private static final boolean REPEAT_HOLE = 1 == 1;
	private static final long TL = 4000;
	static final int[] DX = new int[] { -1, 1, 0, 0 };
	static final int[] DY = new int[] { 0, 0, -1, 1 };
	static double smallTopRatio = 1.1;
	static double smallRatio = 0.54;
	long startTime = System.currentTimeMillis();
	XorShift rnd = new XorShift();
	Timer timer = new Timer();
	ScoreCaluculator calc = new ScoreCaluculator();
	int N, smallCount;
	int[] ret;
	Rect[] origRect;
	int smallTop;
	int bestTouchCount;

	public int[] place(int[] A, int[] B) {
		N = A.length;
		ret = new int[3 * N];
		origRect = new Rect[N];
		for (int i = 0; i < N; ++i) {
			origRect[i] = new Rect(A[i], B[i], i);
		}
		smallCount = (int) (N * smallRatio);
		//		int smallTopBase = (int) Math.sqrt(cutoff * 1.8);
		int smallTopBase = (int) (Math.sqrt(smallCount) * smallTopRatio);
		int div = Math.min(smallTopBase * 2, (int) (2500 / Math.pow(N, 1.1)));
		//		div = 1;
		//		smallTopBase -= div / 2;
		debug("div:" + div + " smallTopBase:" + smallTopBase + " smallBase:" + smallCount);
		Rect[] bestAns = new Rect[N];
		Rect[] ans = new Rect[N];
		double limit = 1.0 * TL / (div + 1);
		smallTop = smallTopBase;
		double bestScore = solve(bestAns, limit * 2);
		for (int i = 1; i < div; ++i) {
			limit = (i + 2) * TL / (div + 1);
			//			smallTop = smallTopBase + i;
			--smallCount;
			double score = solve(ans, limit);
			debug("score:" + score);
			if (score > bestScore) {
				bestScore = score;
				for (int j = 0; j < N; ++j) {
					bestAns[j] = new Rect(ans[j]);
				}
			}
		}
		for (int i = 0; i < N; ++i) {
			Rect r = bestAns[i];
			ret[r.idx * 3] = r.x;
			ret[r.idx * 3 + 1] = r.y;
			ret[r.idx * 3 + 2] = r.rot;
		}
		timer.print();
		return ret;
	}

	double solve(Rect[] bestRects, double limit) {
		timer.start(0);
		Rect[] rects = new Rect[N];
		for (int i = 0; i < N; ++i) {
			rects[i] = new Rect(origRect[i]);
		}
		Arrays.sort(rects, new Comparator<Rect>() {
			public int compare(Rect o1, Rect o2) {
				return Integer.compare(o2.w * 2 + o2.h, o1.w * 2 + o1.h);
				//				return Integer.compare(o2.w * o2.w + o2.h * o2.h, o1.w * o1.w + o1.h * o1.h);
			}
		});

		//		for (int i = (int) (System.currentTimeMillis() & 0xFF); i >= 0; --i) {
		//			rnd.nextInt();
		//		}
		//		for (int i = 0; i < (N - cutoff) / 16; ++i) {
		//			int i1 = rnd.nextInt(N - cutoff);
		//			int i2 = rnd.nextInt(N - cutoff);
		//			Rect tmp = rects[i1];
		//			rects[i1] = rects[i2];
		//			rects[i2] = tmp;
		//		}
		Chain[] chains = distributeChains(rects);
		Rect[] smalls = new Rect[smallCount];
		for (int i = 0; i < smallCount; ++i) {
			smalls[i] = rects[N - smallCount + i];
		}
		Arrays.sort(smalls, new Comparator<Rect>() {
			public int compare(Rect a, Rect b) {
				return Long.compare(sq(a.w) + sq(a.h), sq(b.w) + sq(b.h));
				//				return Integer.compare(a.w, b.w);
			}
		});
		createLargeHole(chains);
		Circle circle = formCircle(chains);
		timer.stop(0);
		calc.init(circle, smalls);
		long before = System.currentTimeMillis();
		bestTouchCount = locateSmallRects(circle, smalls);
		long bestScore = calc.calcScore(rects);
		long after = System.currentTimeMillis();
		long worstTime = 0;
		before = after;
		debug("bestScore:" + bestScore);
		for (int i = 0; i < N; ++i) {
			bestRects[i] = new Rect(rects[i]);
		}

		// optimize by exchange rects order in smalls
		int[] swapPos = new int[smalls.length / 20 * 2];
		int swapDist = smalls.length / 10;
		for (int turn = 0;; ++turn) {
			if (!REPEAT_HOLE) break;
			if (System.currentTimeMillis() - startTime + worstTime * 1.05 > limit) {
				debug("turn:" + turn);
				break;
			}
			for (int i = 0; i < swapPos.length; i += 2) {
				swapPos[i] = rnd.nextInt(smalls.length - swapDist);
				swapPos[i + 1] = swapPos[i] + rnd.nextInt(swapDist) + 1;
				Rect tmp = smalls[swapPos[i]];
				smalls[swapPos[i]] = smalls[swapPos[i + 1]];
				smalls[swapPos[i + 1]] = tmp;
			}
			int touchCount = locateSmallRects(circle, smalls);
			long curScore = touchCount > bestTouchCount - 2 ? calc.calcScore(rects) : 0;
			if (curScore > bestScore) {
				bestScore = curScore;
				bestTouchCount = touchCount;
				debug("bestScore:" + bestScore + " at turn " + turn);
				for (int i = N - smalls.length; i < N; ++i) {
					bestRects[i] = new Rect(rects[i]);
				}
			}
			for (int i = swapPos.length - 2; i >= 0; i -= 2) {
				Rect tmp = smalls[swapPos[i]];
				smalls[swapPos[i]] = smalls[swapPos[i + 1]];
				smalls[swapPos[i + 1]] = tmp;
			}
			after = System.currentTimeMillis();
			if (turn > 5) worstTime = Math.max(worstTime, after - before);
			before = after;
		}

		return bestScore;
	}

	Chain[] distributeChains(Rect[] rects) {
		Rect[] cr = new Rect[N - smallCount];
		int totL = 0;
		int totW = 0;
		for (int i = 0; i < N - smallCount; ++i) {
			cr[i] = rects[i];
			totL += cr[i].w;
			totW += cr[i].h;
		}
		Chain[] chains = new Chain[4];
		for (int i = 0; i < 4; ++i) {
			chains[i] = new Chain();
		}

		Arrays.sort(cr, rectCompTop);
		for (int i = 0; i < smallTop; ++i) {
			chains[3].add(cr[i]);
		}

		Rect[] crr = new Rect[cr.length - smallTop];
		for (int i = 0; i < crr.length; ++i) {
			crr[i] = cr[smallTop + i];
		}
		Arrays.sort(crr, new Comparator<Rect>() {
			public int compare(Rect r1, Rect r2) {
				return Integer.compare(r2.w, r1.w);
			}
		});
		for (int i = 0; i < crr.length; ++i) {
			int mi = 0;
			int ml = chains[0].len;
			for (int j = 1; j < 4; ++j) {
				if (chains[j].len < ml) {
					mi = j;
					ml = chains[j].len;
				}
			}
			chains[mi].add(crr[i]);
		}

		double bestScore = 0;
		for (int i = 0; i < 4; ++i) {
			bestScore += sq(chains[i].len * 4 - totL);
			bestScore += sq(chains[i].wide * 4 - totW);
		}
		for (int i = 0; i < 100000; ++i) {
			int c1 = i % 3;
			int c2 = rnd.nextInt(2);
			if (c2 >= c1) ++c2;
			int i1 = rnd.nextInt(chains[c1].size());
			int i2 = rnd.nextInt(chains[c2].size());
			Rect r1 = chains[c1].rects.get(i1);
			Rect r2 = chains[c2].rects.get(i2);
			chains[c1].len += r2.w - r1.w;
			chains[c2].len += r1.w - r2.w;
			chains[c1].wide += r2.h - r1.h;
			chains[c2].wide += r1.h - r2.h;
			double score = 0;
			for (int j = 0; j < 4; ++j) {
				score += sq(chains[j].len * 4 - totL);
				score += sq(chains[j].wide * 4 - totW);
			}
			if (transit(score, bestScore)) {
				bestScore = score;
				//				debug("bestScore:" + bestScore);
				chains[c1].rects.set(i1, r2);
				chains[c2].rects.set(i2, r1);
			} else {
				chains[c1].len -= r2.w - r1.w;
				chains[c2].len -= r1.w - r2.w;
				chains[c1].wide -= r2.h - r1.h;
				chains[c2].wide -= r1.h - r2.h;
			}
		}
		return chains;
	}

	boolean transit(double cur, double best) {
		return cur < best;
		//		if (cur <= best) return true;
		//		return rnd.nextDouble() < Math.exp((best - cur) * 0.01);
	}

	void createLargeHole(Chain[] chains) {
		Chain[] tmpChains = { chains[0], chains[1], chains[2] };
		Arrays.sort(tmpChains);
		chains[0] = tmpChains[0];
		chains[1] = chains[3];
		chains[2] = tmpChains[1];
		chains[3] = tmpChains[2];
		for (int i = 0; i < 4; ++i) {
			chains[i].idx = i;
		}
		chains[0].placeLine(chains[2].len - chains[3].len);
		chains[1].placeLine(0);
		chains[2].placeLine(0);
		chains[3].placeLine(chains[0].len - chains[1].len);
		int x = chains[0].len;
		int y = chains[2].len;
		for (int i = 0; i < chains[2].rects.size(); ++i) { // right
			Rect r = chains[2].rects.get(i);
			int toR = r.y + r.h;
			r.y = r.x;
			r.x = x - toR;
			r.rot();
		}
		for (int i = 0; i < chains[1].rects.size(); ++i) { // top
			Rect r = chains[1].rects.get(i);
			r.x = x - r.x - r.w;
			r.y = y - r.y - r.h;
		}
		for (int i = 0; i < chains[3].rects.size(); ++i) { // left
			Rect r = chains[3].rects.get(i);
			int toL = r.y;
			r.y = y - r.x - r.w;
			r.x = toL;
			r.rot();
		}
	}

	Circle formCircle(Chain[] chains) {
		Circle circle = new Circle();
		circle.rects = new Rect[chains[0].size() + chains[1].size() + chains[2].size() + chains[3].size()];
		int ci = 0;
		for (int i = 0; i < chains[0].size() - 1; ++i) {
			if (chains[0].rects.get(i).y < chains[0].rects.get(i + 1).y) {
				circle.bi = i;
				break;
			}
		}
		for (int i = circle.bi; i < chains[0].size(); ++i) {
			circle.rects[ci++] = chains[0].rects.get(i);
		}
		for (int i = 0; i < chains[2].size() - 1; ++i) {
			if (chains[2].rects.get(i).r() > chains[2].rects.get(i + 1).r()) {
				circle.ri = chains[0].size() + i - circle.bi;
				break;
			}
		}
		for (int i = 0; i < chains[2].size(); ++i) {
			circle.rects[ci++] = chains[2].rects.get(i);
		}
		for (int i = 0; i < chains[1].size() - 1; ++i) {
			if (chains[1].rects.get(i).t() > chains[1].rects.get(i + 1).t()) {
				circle.ti = chains[0].size() + chains[2].size() + i - circle.bi;
				break;
			}
		}
		for (int i = 0; i < chains[1].size(); ++i) {
			circle.rects[ci++] = chains[1].rects.get(i);
		}
		for (int i = 0; i < chains[3].size() - 1; ++i) {
			if (chains[3].rects.get(i).x < chains[3].rects.get(i + 1).x) {
				circle.li = chains[0].size() + chains[2].size() + chains[1].size() + i - circle.bi;
				break;
			}
		}
		for (int i = 0; i < chains[3].size(); ++i) {
			circle.rects[ci++] = chains[3].rects.get(i);
		}
		for (int i = 0; i < circle.bi; ++i) {
			circle.rects[ci++] = chains[0].rects.get(i);
		}
		circle.bri = chains[0].size() - circle.bi;
		circle.tri = circle.bri + chains[2].size();
		circle.tli = circle.tri + chains[1].size();
		circle.bli = circle.tli + chains[3].size();
		circle.bi = 0;
		centerX = circle.rects[circle.ti].x + circle.rects[circle.ti].w / 2;
		centerY = circle.rects[circle.bi].y;
		return circle;
	}

	int centerX, centerY;
	long candDist;

	boolean near(int curX, int curY) {
		long curDist = calcDist(curX, curY);
		return curDist > candDist;
	}

	long calcDist(int curX, int curY) {
		return sq(curX - centerX) + sq(curY - centerY);
	}

	int locateSmallRects(Circle circle, Rect[] smalls) {
		timer.start(1);
		ArrayList<Rect> cands = new ArrayList<Rect>();
		int candWidth = (int) (Math.sqrt(smalls.length) * 1);
		int candSi = Math.max(circle.tri + 1, circle.ti - candWidth);
		int candEi = Math.min(circle.tli - 1, circle.ti + candWidth);
		int countTR = circle.ti - candSi + 1;
		for (int i = candSi; i <= candEi; ++i) {
			cands.add(circle.rects[i]);
		}
		int origCandSize = cands.size();

		for (int i = 0; i < Math.sqrt(smallCount * 2); ++i) {
			Rect r = smalls[smallCount - 1 - i];
			if (r.w < r.h) r.rot();
		}
		int maxLen = 0;
		for (Rect small : smalls) {
			maxLen = Math.max(maxLen, Math.max(small.w, small.h));
		}
		ArrayList<Rect> obsts = new ArrayList<Rect>();
		for (int i = 0; i < circle.rects.length; ++i) {
			obsts.add(circle.rects[i]);
		}
		ArrayList<ArrayList<Rect>> considerR = new ArrayList<>();
		ArrayList<ArrayList<Rect>> considerL = new ArrayList<>();
		for (int i = 0; i < cands.size(); ++i) {
			ArrayList<Rect> listR = new ArrayList<>();
			ArrayList<Rect> listL = new ArrayList<>();
			if (i != 0) {
				listR.add(cands.get(i - 1));
			}
			if (i < cands.size() - 1) {
				listL.add(cands.get(i + 1));
			}
			considerR.add(listR);
			considerL.add(listL);
		}
		timer.stop(1);

		timer.start(2);
		int touchCount = 0;
		for (int si = 0; si < smalls.length; ++si) {
			Rect small = smalls[si];
			int candX = centerX;
			int candY = centerY;
			candDist = 0;
			boolean candTouch = false;
			int candRot = 0;
			for (int i = 0; i < 2; ++i, small.rot()) {
				if (i == 1 && candTouch) {
					small.rot();
					break;
				}
				for (int ci = 0; ci < cands.size(); ++ci) {
					Rect cand = cands.get(ci);

					// right-side
					if (ci >= countTR) {
						int top = cand.t();
						int bottom = cand.y - small.h;
						Rect topR = null;
						Rect bottomR = null;
						OUT: for (int j = 0; j < considerR.get(ci).size(); ++j) {
							Rect loc = considerR.get(ci).get(j);
							if (!overlap(cand.r(), cand.r() + small.w, loc.x, loc.r())) continue;
							if (loc.t() - bottom < top - (loc.y - small.h)) {
								if (loc.t() >= bottom) {
									bottomR = loc;
									bottom = loc.t();
									if (top < bottom) break OUT;
								}
							} else {
								if (loc.y - small.h <= top) {
									topR = loc;
									top = loc.y - small.h;
									if (top < bottom) break OUT;
								}
							}
						}
						if (bottom <= top) {
							boolean touchBottom = bottomR != null && (bottomR.t() < cand.y || bottomR.x > cand.r());
							boolean touchTop = topR != null && (topR.y > cand.t() || topR.x > cand.r());
							if (touchBottom && (!candTouch || near(cand.r(), bottom))) {
								candX = cand.r();
								candY = bottom;
								candDist = calcDist(candX, candY);
								candTouch = true;
								candRot = i;
							}
							if (touchTop && (!candTouch || near(cand.r(), top))) {
								candX = cand.r();
								candY = top;
								candDist = calcDist(candX, candY);
								candTouch = true;
								candRot = i;
							}
							if (!candTouch && !touchBottom && !touchTop) {
								if (top + small.h > cand.t() && near(cand.r(), top)) {
									candX = cand.r();
									candY = top;
									candDist = calcDist(candX, candY);
									candRot = i;
								} else if (bottom < cand.y && near(cand.r(), bottom)) {
									candX = cand.r();
									candY = bottom;
									candDist = calcDist(candX, candY);
									candRot = i;
								} else {
									int pos = bottom != top ? top - 1 : top;
									if (near(cand.r(), pos)) {
										candX = cand.r();
										candY = pos;
										candDist = calcDist(candX, candY);
										candRot = i;
									}
								}
							}
						}
					}

					// left-side
					if (ci >= origCandSize || ci < countTR - 1) {
						int top = cand.t();
						int bottom = cand.y - small.h;
						Rect topR = null;
						Rect bottomR = null;
						OUT: for (int j = 0; j < considerL.get(ci).size(); ++j) {
							Rect loc = considerL.get(ci).get(j);
							if (!overlap(cand.x - small.w, cand.x, loc.x, loc.r())) continue;
							if (loc.t() - bottom < top - (loc.y - small.h)) {
								if (loc.t() >= bottom) {
									bottomR = loc;
									bottom = loc.t();
									if (top < bottom) break OUT;
								}
							} else {
								if (loc.y - small.h <= top) {
									topR = loc;
									top = loc.y - small.h;
									if (top < bottom) break OUT;
								}
							}
						}
						if (bottom <= top) {
							boolean touchBottom = bottomR != null && (bottomR.t() < cand.y || bottomR.r() < cand.x);
							boolean touchTop = topR != null && (topR.y > cand.t() || topR.r() < cand.x);
							if (touchBottom && (!candTouch || near(cand.x - small.w, bottom))) {
								candX = cand.x - small.w;
								candY = bottom;
								candDist = calcDist(candX, candY);
								candTouch = true;
								candRot = i;
							}
							if (touchTop && (!candTouch || near(cand.x - small.w, top))) {
								candX = cand.x - small.w;
								candY = top;
								candDist = calcDist(candX, candY);
								candTouch = true;
								candRot = i;
							}
							if (!candTouch && !touchBottom && !touchTop) {
								if (top + small.h > cand.t() && near(cand.x - small.w, top)) {
									candX = cand.x - small.w;
									candY = top;
									candDist = calcDist(candX, candY);
									candRot = i;
								} else if (bottom < cand.y && near(cand.x - small.w, bottom)) {
									candX = cand.x - small.w;
									candY = bottom;
									candDist = calcDist(candX, candY);
									candRot = i;
								} else {
									int pos = bottom != top ? top - 1 : top;
									if (near(cand.x - small.w, pos)) {
										candX = cand.x - small.w;
										candY = pos;
										candDist = calcDist(candX, candY);
										candRot = i;
									}
								}
							}
						}
					}

				}
			}
			small.x = candX;
			small.y = candY;
			if (candRot == 1) {
				small.rot();
			}
			for (int i = 0; i < cands.size(); ++i) {
				Rect cand = cands.get(i);
				if (overlap(cand.r(), cand.r() + maxLen, small.x, small.r())
						&& overlap(cand.y - maxLen, cand.t() + maxLen, small.y, small.t())) {
					considerR.get(i).add(small);
				}
				if (overlap(cand.x - maxLen, cand.x, small.x, small.r())
						&& overlap(cand.y - maxLen, cand.t() + maxLen, small.y, small.t())) {
					considerL.get(i).add(small);
				}
			}

			{
				ArrayList<Rect> listR = new ArrayList<>();
				ArrayList<Rect> listL = new ArrayList<>();
				for (int i = 0; i < obsts.size(); ++i) {
					Rect obst = obsts.get(i);
					if (overlap(small.r(), small.r() + maxLen, obst.x, obst.r())
							&& overlap(small.y - maxLen, small.t() + maxLen, obst.y, obst.t())) {
						listR.add(obst);
					}
					if (overlap(small.x - maxLen, small.x, obst.x, obst.r())
							&& overlap(small.y - maxLen, small.t() + maxLen, obst.y, obst.t())) {
						listL.add(obst);
					}
				}
				considerR.add(listR);
				considerL.add(listL);
			}
			cands.add(small);
			obsts.add(small);
			if (candTouch) {
				++touchCount;
			} else {
				if (touchCount + smalls.length - 1 - si <= bestTouchCount - 1) break;
			}
		}
		timer.stop(2);
		return touchCount;
	}

	static boolean overlap(Rect r1, Rect r2) {
		return overlap(r1.x, r1.r(), r2.x, r2.r()) && overlap(r1.y, r1.t(), r2.y, r2.t());
	}

	static boolean overlap(int aMin, int aMax, int bMin, int bMax) {
		if (aMax <= bMin) return false;
		if (bMax <= aMin) return false;
		return true;
	}

	static long dist2(int x1, int x2, int y1, int y2) {
		return sq(x2 - x1) + sq(y2 - y1);
	}

	static long sq(long v) {
		return v * v;
	}

	Comparator<Rect> rectCompTop = new Comparator<Rect>() {
		public int compare(Rect r1, Rect r2) {
			//			return Integer.compare(r1.w + r1.h, r2.w + r2.h);
			//			return Integer.compare(r1.w * r1.w + r1.h * r1.h, r2.w * r2.w + r2.h * r2.h);
			return Integer.compare(r1.w * 3 + r1.h, r2.w * 3 + r2.h);
		}
	};

	class Chain implements Comparable<Chain> {
		int len, wide, idx;
		ArrayList<Rect> rects = new ArrayList<>();

		void add(Rect r) {
			this.rects.add(r);
			this.len += r.w;
			this.wide += r.h;
		}

		int size() {
			return this.rects.size();
		}

		void swap(int i, int j) {
			Rect tmp = rects.get(i);
			rects.set(i, rects.get(j));
			rects.set(j, tmp);
		}

		void placeLine(int startY) {
			sort();
			int left = 0;
			int right = len;
			int ly = startY;
			int ry = 0;
			ArrayList<Rect> first = new ArrayList<>();
			ArrayList<Rect> second = new ArrayList<>();
			for (int i = 0; i < rects.size() - 1; ++i) {
				Rect r = rects.get(i);
				if (r.w < r.h) r.rot();
				boolean addFirst = ry <= ly;
				if (addFirst) {
					r.x = left;
					r.y = ly - r.h;
					left += r.w;
					ly -= r.h;
					first.add(r);
				} else {
					r.x = right - r.w;
					r.y = ry - r.h;
					right -= r.w;
					ry -= r.h;
					second.add(r);
				}
			}
			Rect r = rects.get(rects.size() - 1);
			if (r.w < r.h) r.rot();
			r.x = left;
			r.y = Math.max(ry, ly) - r.h;
			first.add(r);

			for (int i = 0; i < first.size(); ++i) {
				rects.set(i, first.get(i));
			}
			for (int i = 0; i < second.size(); ++i) {
				rects.set(i + first.size(), second.get(second.size() - 1 - i));
			}
		}

		void sort() {
			Comparator<Rect> compRatio = new Comparator<Rect>() {
				public int compare(Rect o1, Rect o2) {
					double v1 = 1.0 * o1.h / o1.w;
					double v2 = 1.0 * o2.h / o2.w;
					//					if (idx == 1) {
					//						v1 += o1.w / DIV;
					//						v2 += o2.w / DIV;
					//					}
					return Double.compare(v2, v1);
				}
			};
			if (idx == 1) {
				Collections.sort(rects, rectCompTop);
				Rect[] ord = new Rect[rects.size() - smallTop];
				for (int i = 0; i < ord.length; ++i) {
					ord[i] = rects.get(smallTop + i);
				}
				Rect[] ord2 = new Rect[smallTop];
				for (int i = 0; i < smallTop; ++i) {
					ord2[i] = rects.get(i);
				}
				Arrays.sort(ord, compRatio);
				Arrays.sort(ord2, compRatio);
				for (int i = 0; i < ord.length; ++i) {
					rects.set(i, ord[i]);
				}
				for (int i = 0; i < smallTop; ++i) {
					rects.set(ord.length + i, ord2[i]);
				}
			} else {
				Collections.sort(rects, compRatio);
			}
		}

		public int compareTo(Chain o) {
			return Integer.compare(this.len, o.len);
		}
	}

	static class Rect {
		int w, h, rot, idx, x, y;

		Rect(int w, int h, int idx) {
			if (w < h) {
				this.w = h;
				this.h = w;
				rot = 1;
			} else {
				this.w = w;
				this.h = h;
				rot = 0;
			}
			this.idx = idx;
		}

		Rect(Rect o) {
			this.w = o.w;
			this.h = o.h;
			this.idx = o.idx;
			this.rot = o.rot;
			this.x = o.x;
			this.y = o.y;
		}

		int r() {
			return x + w;
		}

		int t() {
			return y + h;
		}

		void rot() {
			rot ^= 1;
			int tmp = w;
			w = h;
			h = tmp;
		}
	}

	static class Circle {
		Rect[] rects;
		int bi, ri, ti, li, bri, bli, tri, tli;
	}

	class ScoreCaluculator {

		int[] QX = new int[4100000];
		int[] QY = new int[4100000];
		long area;

		void init(Circle circle, Rect[] smalls) {
			timer.start(3);
			int N = circle.rects.length;
			int[] xs = new int[N * 2];
			int[] ys = new int[N * 2];
			for (int i = 0; i < N; ++i) {
				xs[i * 2] = circle.rects[i].x;
				xs[i * 2 + 1] = circle.rects[i].r();
				ys[i * 2] = circle.rects[i].y;
				ys[i * 2 + 1] = circle.rects[i].t();
			}
			int[] xp = enumerateCoordinates(xs);
			int[] yp = enumerateCoordinates(ys);
			int[][] map = new int[xp.length][yp.length];
			for (int i = 0; i < N; i++) {
				int fromX = Arrays.binarySearch(xp, circle.rects[i].x);
				int toX = Arrays.binarySearch(xp, circle.rects[i].r());
				int fromY = Arrays.binarySearch(yp, circle.rects[i].y);
				int toY = Arrays.binarySearch(yp, circle.rects[i].t());
				for (int x = fromX; x < toX; x++) {
					for (int y = fromY; y < toY; y++) {
						map[x][y] = -1;
					}
				}
			}
			Arrays.fill(map[0], -1);
			Arrays.fill(map[xp.length - 1], -1);
			for (int i = 0; i < xp.length; ++i) {
				map[i][0] = map[i][yp.length - 1] = -1;
			}

			int compCnt = 1;
			int qBeg = 0, qEnd = 0;
			area = 0;
			for (int x = 1; x < map.length - 2; x++) {
				for (int y = 1; y < map[0].length - 2; y++) {
					if (map[x][y] != 0) continue;
					QX[qBeg] = x;
					QY[qBeg++] = y;
					map[x][y] = compCnt;
					if (compCnt > 1) {
						area += (long) (xp[x + 1] - xp[x]) * (yp[y + 1] - yp[y]);
					}
					while (qEnd < qBeg) {
						int curX = QX[qEnd];
						int curY = QY[qEnd++];
						for (int d = 0; d < 4; d++) {
							int nx = curX + DX[d];
							int ny = curY + DY[d];
							if (map[nx][ny] != 0) continue;
							QX[qBeg] = nx;
							QY[qBeg++] = ny;
							map[nx][ny] = compCnt;
							if (compCnt > 1) {
								area += (long) (xp[nx + 1] - xp[nx]) * (yp[ny + 1] - yp[ny]);
							}
						}
					}
					compCnt++;
				}
			}
			int minus = 0;
			for (Rect small : smalls) {
				minus += small.w * small.h;
			}
			area -= minus;
			timer.stop(3);
			debug(String.format("%.4e, %.4e", 1.0 * area, 1.0 * minus));
			//			double theo = area * (cutoff - 1) * (cutoff - 1);
			//			debug(String.format("cutoff:%d theoretical_score:%.4e", cutoff, theo));
		}

		long calcScore(Rect[] rects) {
			timer.start(4);
			int N = rects.length;
			int[] xs = new int[N * 2];
			int[] ys = new int[N * 2];
			for (int i = 0; i < N; ++i) {
				xs[i * 2] = rects[i].x;
				xs[i * 2 + 1] = rects[i].r();
				ys[i * 2] = rects[i].y;
				ys[i * 2 + 1] = rects[i].t();
			}
			int[] xp = enumerateCoordinates(xs);
			int[] yp = enumerateCoordinates(ys);
			int[][] map = new int[xp.length][yp.length];
			for (int i = 0; i < N; i++) {
				int fromX = Arrays.binarySearch(xp, rects[i].x);
				int toX = Arrays.binarySearch(xp, rects[i].r());
				int fromY = Arrays.binarySearch(yp, rects[i].y);
				int toY = Arrays.binarySearch(yp, rects[i].t());
				for (int x = fromX; x < toX; x++) {
					for (int y = fromY; y < toY; y++) {
						map[x][y] = -1;
					}
				}
			}
			Arrays.fill(map[0], -1);
			Arrays.fill(map[xp.length - 1], -1);
			for (int i = 0; i < xp.length; ++i) {
				map[i][0] = map[i][yp.length - 1] = -1;
			}

			int compCnt = 1;
			int qBeg = 0, qEnd = 0;
			for (int x = 1; x < map.length - 2; x++) {
				for (int y = 1; y < map[0].length - 2; y++) {
					if (map[x][y] != 0) continue;
					QX[qBeg] = x;
					QY[qBeg++] = y;
					map[x][y] = compCnt;
					while (qEnd < qBeg) {
						int curX = QX[qEnd];
						int curY = QY[qEnd++];
						for (int d = 0; d < 4; d++) {
							int nx = curX + DX[d];
							int ny = curY + DY[d];
							if (map[nx][ny] != 0) continue;
							QX[qBeg] = nx;
							QY[qBeg++] = ny;
							map[nx][ny] = compCnt;
						}
					}
					compCnt++;
				}
			}
			timer.stop(4);
			compCnt -= 2;
			return area * compCnt * compCnt;
		}

		int[] enumerateCoordinates(int[] ar) {
			int min = Integer.MAX_VALUE;
			int max = Integer.MIN_VALUE;
			Set<Integer> coords = new HashSet<Integer>();
			for (int elm : ar) {
				min = Math.min(min, elm);
				max = Math.max(max, elm);
				coords.add(elm);
			}
			coords.add(min - 100);
			coords.add(min - 50);
			coords.add(max + 50);
			coords.add(max + 100);
			int cnt = coords.size();
			int[] res = new int[cnt];
			int pos = 0;
			for (int elm : coords) {
				res[pos++] = elm;
			}
			Arrays.sort(res);
			return res;
		}
	}

	static void debug(String str) {
		if (DEBUG) System.err.println(str);
	}

	static void debug(Object... obj) {
		if (DEBUG) System.err.println(Arrays.deepToString(obj));
	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;
		static final double TO_DOUBLE = 1.0 / (1L << 31);

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w;
		}

		boolean nextBoolean() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return (w & 8) == 0;
		}

		double nextDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return Math.abs(w * TO_DOUBLE);
		}

		double nextSDouble() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w * TO_DOUBLE;
		}
	}

	static final class Timer {
		ArrayList<Long> sum = new ArrayList<Long>();
		ArrayList<Long> start = new ArrayList<Long>();

		void start(int i) {
			if (MEASURE_TIME) {
				while (sum.size() <= i) {
					sum.add(0L);
					start.add(0L);
				}
				start.set(i, System.currentTimeMillis());
			}
		}

		void stop(int i) {
			if (MEASURE_TIME) {
				sum.set(i, sum.get(i) + System.currentTimeMillis() - start.get(i));
			}
		}

		void print() {
			if (MEASURE_TIME && !sum.isEmpty()) {
				System.err.print("[");
				for (int i = 0; i < sum.size(); ++i) {
					System.err.print(sum.get(i) + ", ");
				}
				System.err.println("]");
			}
		}
	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int N = sc.nextInt();
			int[] A = new int[N];
			int[] B = new int[N];
			for (int i = 0; i < N; ++i) {
				A[i] = sc.nextInt();
			}
			for (int i = 0; i < N; ++i) {
				B[i] = sc.nextInt();
			}
			int[] ret = new RectanglesAndHoles().place(A, B);
			for (int i = 0; i < 3 * N; ++i) {
				System.out.println(ret[i]);
			}
			System.out.flush();
		}
	}
}
