import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Tester {
	public static final int MIN_N = 100;
	public static final int MAX_N = 1000;
	public static final int MAX_DIM = 1000;
	public static final int MAX_COORD = 1000000;
	public static final int UNKNOWN = -1;
	public static final int OBSTACLE = -2;

	static boolean outputLog = false;
	int N;
	int[] LX, LY, RX, RY;
	int[] XS, YS;
	int[][] map;

	boolean overlap(int A, int B, int C, int D) {
		return Math.max(A, C) < Math.min(B, D);
	}

	static int[] enumerateCoordinates(int[] A, int[] B) {
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		Set<Integer> coords = new HashSet<Integer>();
		for (int elm : A) {
			min = Math.min(min, elm);
			max = Math.max(max, elm);
			coords.add(elm);
		}
		for (int elm : B) {
			min = Math.min(min, elm);
			max = Math.max(max, elm);
			coords.add(elm);
		}
		coords.add(min - 100);
		coords.add(max + 100);

		int cnt = coords.size();
		int[] res = new int[cnt];
		int pos = 0;
		for (int elm : coords) {
			res[pos++] = elm;
		}
		Arrays.sort(res);
		return res;
	}

	public Result runTest(long seed) throws Exception {
		Random rnd = SecureRandom.getInstance("SHA1PRNG");
		rnd.setSeed(seed);
		N = MIN_N + rnd.nextInt(MAX_N - MIN_N + 1);
		if (1001 <= seed && seed <= 2000) {
			N = (int) (seed - 1001) / 100 * 100 + 100;
		}
		int[] A = new int[N];
		int[] B = new int[N];
		for (int i = 0; i < N; i++) {
			A[i] = rnd.nextInt(MAX_DIM) + 1;
			B[i] = rnd.nextInt(MAX_DIM) + 1;
		}
		LX = new int[N];
		LY = new int[N];
		RX = new int[N];
		RY = new int[N];
		int[] kind = new int[N];
		Result res = new Result();
		res.N = N;
		res.seed = seed;
		long starttime = System.currentTimeMillis();
		int[] ret = new RectanglesAndHoles().place(A, B);
		res.elapsed = System.currentTimeMillis() - starttime;
		for (int i = 0; i < N; i++) {
			LX[i] = ret[3 * i];
			LY[i] = ret[3 * i + 1];
			kind[i] = ret[3 * i + 2];
		}

		for (int i = 0; i < N; i++) {
			RX[i] = LX[i] + (kind[i] == 0 ? A[i] : B[i]);
			RY[i] = LY[i] + (kind[i] == 0 ? B[i] : A[i]);
		}
		if (outputLog) {
			String fileName = String.format("log/%04d.log", seed);
			try (PrintWriter writer = new PrintWriter(fileName)) {
				writer.println(N);
				for (int i = 0; i < N; ++i) {
					writer.println(LX[i] + " " + LY[i] + " " + RX[i] + " " + RY[i]);
				}
			}
		}
		for (int i = 0; i < N; i++) {
			if (LX[i] < -MAX_COORD || LX[i] > MAX_COORD) {
				throw new RuntimeException("ERROR: the left X coordinate of " + i
						+ "-th rectangle (0-based) must be in -1,000,000 .. 1,000,000. Your return value = " + LX[i] + ".");
			}
			if (LY[i] < -MAX_COORD || LY[i] > MAX_COORD) {
				throw new RuntimeException("ERROR: the bottom Y coordinate of " + i
						+ "-th rectangle (0-based) must be in -1,000,000 .. 1,000,000. Your return value = " + LY[i] + ".");
			}
			if (kind[i] != 0 && kind[i] != 1) {
				throw new RuntimeException("ERROR: element " + (3 * i + 2)
						+ " of your return value must be 0 or 1, but it is equal to " + kind[i] + ".");
			}
		}

		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				if (overlap(LX[i], RX[i], LX[j], RX[j]) && overlap(LY[i], RY[i], LY[j], RY[j])) {
					throw new RuntimeException("ERROR: rectangles " + i + " (0-based) and " + j
							+ " (0-based) in your solution overlap. Rectangle " + i + " is (" + LX[i] + ", " + LY[i] + ") - ("
							+ RX[i] + ", " + RY[i] + "). Rectangle " + j + " is (" + LX[j] + ", " + LY[j] + ") - (" + RX[j] + ", "
							+ RY[j] + ").");
				}
			}
		}

		XS = enumerateCoordinates(LX, RX);
		YS = enumerateCoordinates(LY, RY);
		map = new int[XS.length - 1][YS.length - 1];
		for (int[] elm : map) {
			Arrays.fill(elm, UNKNOWN);
		}
		for (int i = 0; i < N; i++) {
			int fromX = Arrays.binarySearch(XS, LX[i]);
			int toX = Arrays.binarySearch(XS, RX[i]);
			int fromY = Arrays.binarySearch(YS, LY[i]);
			int toY = Arrays.binarySearch(YS, RY[i]);
			for (int x = fromX; x < toX; x++) {
				for (int y = fromY; y < toY; y++) {
					map[x][y] = OBSTACLE;
				}
			}
		}

		int cells = map.length * map[0].length;
		int compCnt = 0;
		int[] QX = new int[cells];
		int[] QY = new int[cells];
		int qBeg = 0, qEnd = 0;
		final int[] DX = new int[] { -1, 1, 0, 0 };
		final int[] DY = new int[] { 0, 0, -1, 1 };

		for (int x = 0; x < map.length; x++) {
			for (int y = 0; y < map[0].length; y++) {
				if (map[x][y] == UNKNOWN) {
					QX[qBeg] = x;
					QY[qBeg++] = y;
					map[x][y] = compCnt;
					while (qEnd < qBeg) {
						int curX = QX[qEnd];
						int curY = QY[qEnd++];
						for (int d = 0; d < DX.length; d++) {
							int nextX = curX + DX[d];
							int nextY = curY + DY[d];
							if (nextX >= 0 && nextX < map.length && nextY >= 0 && nextY < map[0].length
									&& map[nextX][nextY] == UNKNOWN) {
								QX[qBeg] = nextX;
								QY[qBeg++] = nextY;
								map[nextX][nextY] = compCnt;
							}
						}
					}
					compCnt++;
				}
			}
		}

		long totArea = 0;
		for (int x = 0; x < map.length; x++) {
			for (int y = 0; y < map[0].length; y++) {
				if (map[x][y] > 0) {
					totArea += (long) (XS[x + 1] - XS[x]) * (YS[y + 1] - YS[y]);
				}
			}
		}
		compCnt--;
		res.hole = compCnt;
		res.area = totArea;
		res.score = totArea * compCnt * compCnt;
		return res;
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) {
		long seed = 1;
		long begin = -1;
		long end = -1;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) {
				seed = Long.parseLong(args[++i]);
			} else if (args[i].equals("-b")) {
				begin = Long.parseLong(args[++i]);
			} else if (args[i].equals("-e")) {
				end = Long.parseLong(args[++i]);
			} else if (args[i].equals("-log")) {
				outputLog = true;
			} else {
				System.out.println("WARNING: unknown argument " + args[i] + ".");
			}
		}

//		RectanglesAndHoles.smallRatio = 0.5;
		for (int turn = 0; turn < 1; ++turn) {
//			System.out.println("ratio:" + RectanglesAndHoles.smallRatio);
			try {
				if (begin != -1 && end != -1) {
					ArrayList<Long> seeds = new ArrayList<Long>();
					for (long i = begin; i <= end; ++i) {
						seeds.add(i);
					}
					int len = seeds.size();
					Result[] results = new Result[len];
					TestThread[] threads = new TestThread[THREAD_COUNT];
					int prev = 0;
					for (int i = 0; i < THREAD_COUNT; ++i) {
						int next = len * (i + 1) / THREAD_COUNT;
						threads[i] = new TestThread(prev, next - 1, seeds, results);
						prev = next;
					}
					for (int i = 0; i < THREAD_COUNT; ++i) {
						threads[i].start();
					}
					for (int i = 0; i < THREAD_COUNT; ++i) {
						threads[i].join();
					}
					double sum = 0;
					for (int i = 0; i < results.length; ++i) {
						System.out.println(results[i]);
						System.out.println();
						sum += results[i].score;
					}
					System.out.println("ave:" + (sum / results.length));
				} else {
					Tester tester = new Tester();
					Result res = tester.runTest(seed);
					System.out.println(res);
				}
			} catch (Exception e) {
				System.err.println("ERROR: Unexpected error while running your test case.");
				e.printStackTrace();
			}
			System.out.println();
			System.out.flush();
			RectanglesAndHoles.smallRatio += 0.01;
		}
	}

	static class TestThread extends Thread {
		int begin, end;
		ArrayList<Long> seeds;
		Result[] results;

		TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
			this.begin = begin;
			this.end = end;
			this.seeds = seeds;
			this.results = results;
		}

		public void run() {
			for (int i = begin; i <= end; ++i) {
				Tester f = new Tester();
				try {
					Result res = f.runTest(seeds.get(i));
					results[i] = res;
				} catch (Exception e) {
					e.printStackTrace();
					results[i] = new Result();
					results[i].seed = seeds.get(i);
					results[i].N = f.N;
				}
			}
		}
	}

	static class Result {
		long seed;
		int N;
		int hole;
		long area;
		long score;
		long elapsed;

		public String toString() {
			String ret = String.format("seed:%4d  N:%2d\n", seed, N);
			ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
			ret += String.format("hole:%3d area:%.4e\n", hole, (double) area);
			ret += String.format("score:%.4e", (double) score);
			return ret;
		}
	}

}
