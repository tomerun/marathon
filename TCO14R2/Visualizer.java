import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

class Visualizer extends JFrame {
	static final int size = 880;
	int N;
	int[] LX, LY, RX, RY, XS, YS;
	double scale, centerX, centerY;
	int titleHeight;

	class DrawerKeyListener extends KeyAdapter {

		public void keyPressed(KeyEvent e) {
			double move = size / 40.0 / scale;
			if (e.isShiftDown()) move *= 10;
			switch (e.getKeyCode()) {
			case KeyEvent.VK_UP:
				centerY += move;
				Visualizer.this.repaint();
				break;
			case KeyEvent.VK_DOWN:
				centerY -= move;
				Visualizer.this.repaint();
				break;
			case KeyEvent.VK_LEFT:
				centerX -= move;
				Visualizer.this.repaint();
				break;
			case KeyEvent.VK_RIGHT:
				centerX += move;
				Visualizer.this.repaint();
				break;
			}
			if (e.getKeyChar() == '+') {
				scale *= 2;
				Visualizer.this.repaint();
			} else if (e.getKeyChar() == '-') {
				scale *= 0.5;
				Visualizer.this.repaint();
			}
		}
	}

	class DrawerMouseListener extends MouseAdapter {
		boolean drag = false;
		int dragStartX, dragStartY;

		public void mouseClicked(MouseEvent e) {
			int x = e.getX();
			int y = e.getY() - titleHeight;
			double clickX = getRealX(x);
			double clickY = getRealY(y);
			if (e.isShiftDown()) {
				scale *= 0.5;
			} else {
				scale *= 2;
				scale = Math.min(scale, size / 200.0);
			}
			centerX = clickX + (size / 2.0 - x) / scale;
			centerY = clickY - (size / 2.0 - y) / scale;
			Visualizer.this.repaint();
		}

		public void mouseDragged(MouseEvent e) {
			if (!drag) {
				dragStartX = e.getX();
				dragStartY = e.getY() - titleHeight;
				drag = true;
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (!drag) return;
			drag = false;
			int dragEndX = e.getX();
			int dragEndY = e.getY() - titleHeight;
			int left = getRealX(Math.min(dragStartX, dragEndX));
			int right = getRealX(Math.max(dragStartX, dragEndX));
			int bottom = getRealY(Math.max(dragStartY, dragEndY));
			int top = getRealY(Math.min(dragStartY, dragEndY));
			scale = 1.0 * size / Math.max(right - left, top - bottom);
			scale = Math.min(scale, size / 200.0);
			centerX = (left + right) / 2;
			centerY = (top + bottom) / 2;
			Visualizer.this.repaint();
		}
	}

	class DrawerPanel extends JPanel {
		void drawRectangles(Graphics g) {
			for (int i = 0; i < N; i++) {
				int left = getWindowX(LX[i]);
				int right = getWindowX(RX[i]);
				int bottom = getWindowY(LY[i]);
				int top = getWindowY(RY[i]);
				g.setColor(new Color(127, 127, 127));
				g.fillRect(left, top, right - left + 1, bottom - top + 1);
				g.setColor(Color.BLACK);
				g.drawRect(left, top, right - left, bottom - top);
			}
		}

		void drawHoles(Graphics g) {
			g.setColor(Color.RED);
			//			for (int i = 0; i < map.length; i++) {
			//				for (int j = 0; j < map[0].length; j++) {
			//					if (map[i][j] > 0) {
			//						g.setColor(new Color(255, 127, 127));
			//						g.fillRect(getX(XS[i]), getY(YS[j + 1]), getX(XS[i + 1]) - getX(XS[i]) + 1, getY(YS[j]) - getY(YS[j + 1])
			//								+ 1);
			//					}
			//				}
			//			}
		}

		void drawMeasure(Graphics g) {
			int measure = (int) (100 / scale);
			if (measure < 10) {
				measure = 10;
			} else if (measure < 100) {
				measure = (measure + 5) / 10 * 10;
			} else if (measure < 1000) {
				measure = (measure + 50) / 100 * 100;
			} else if (measure < 10000) {
				measure = (measure + 500) / 1000 * 1000;
			} else {
				measure = (measure + 5000) / 10000 * 10000;
			}
			g.setColor(Color.BLACK);
			int y = size - 50;
			int l = 50;
			int r = l + (int) (measure * scale);
			g.drawLine(l, y, r, y);
			g.drawLine(l, y, l, y - 5);
			g.drawLine(r, y, r, y - 5);
			g.drawString("" + measure, r - 10, y + 12);
		}

		public void paint(Graphics g) {
			drawHoles(g);
			drawRectangles(g);
			drawMeasure(g);
		}
	}

	int getWindowX(double x) {
		return (int) Math.round((x - centerX) * scale + size / 2.0);
	}

	int getWindowY(double y) {
		return (int) Math.round((centerY - y) * scale + size / 2.0);
	}

	int getRealX(int x) {
		return (int) ((x - size / 2.0) / scale + centerX);
	}

	int getRealY(int y) {
		return (int) (centerY - (y - size / 2.0) / scale);
	}

	public Visualizer(int[] t, int[] b, int[] l, int[] r) {
		N = t.length;
		LX = l;
		LY = b;
		RX = r;
		RY = t;
		XS = Tester.enumerateCoordinates(LX, RX);
		YS = Tester.enumerateCoordinates(LY, RY);
		double minX = XS[0];
		double maxX = XS[XS.length - 1];
		double minY = YS[0];
		double maxY = YS[YS.length - 1];
		centerX = (maxX + minX) / 2;
		centerY = (maxY + minY) / 2;
		scale = size / Math.max(maxX - minX, maxY - minY);

		DrawerPanel panel = new DrawerPanel();
		panel.setPreferredSize(new Dimension(size, size));
		getContentPane().add(panel);
		setSize(size, size);
		pack();
		titleHeight = getInsets().top;
		addKeyListener(new DrawerKeyListener());
		DrawerMouseListener mouseListener = new DrawerMouseListener();
		addMouseListener(mouseListener);
		addMouseMotionListener(mouseListener);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int N = sc.nextInt();
			int[] t = new int[N];
			int[] b = new int[N];
			int[] l = new int[N];
			int[] r = new int[N];
			for (int i = 0; i < N; ++i) {
				l[i] = sc.nextInt();
				b[i] = sc.nextInt();
				r[i] = sc.nextInt();
				t[i] = sc.nextInt();
			}
			new Visualizer(t, b, l, r);
		}
	}

}
