#!/bin/sh

java Tester -seed $1 -log
logfile=`printf "%04d.log" $1`
java Visualizer < log/$logfile
