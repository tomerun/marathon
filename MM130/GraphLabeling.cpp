#include <algorithm>
#include <utility>
#include <vector>
#include <iostream>
#include <array>
#include <set>
#include <numeric>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifdef LOCAL
#ifndef NDEBUG
// #define MEASURE_TIME
#define DEBUG
#endif
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
constexpr double CLOCK_PER_SEC = 3.126448e9;
constexpr ll TL = 5000;
#else
constexpr double CLOCK_PER_SEC = 2.5e9;
constexpr ll TL = 9000;
#endif

ll start_time; // msec

inline ll get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
  return result;
}

inline ll get_elapsed_msec() {
  return get_time() - start_time;
}

inline bool reach_time_limit() {
  return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
  uint32_t x,y,z,w;
  static const double TO_DOUBLE;

  XorShift() {
    x = 123456789;
    y = 362436069;
    z = 521288629;
    w = 88675123;
  }

  uint32_t nextUInt(uint32_t n) {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    return w % n;
  }

  uint32_t nextUInt() {
    uint32_t t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
  }

  double nextDouble() {
    return nextUInt() * TO_DOUBLE;
  }
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct IndexSet {

  vector<int> que;
  vector<int> pos;

  IndexSet(int n) : pos(n, -1) {}

  void add(int v) {
    assert(pos[v] == -1);
    pos[v] = que.size();
    que.push_back(v);
  }

  void remove(int v) {
    assert(pos[v] != -1);
    int p = pos[v];
    int b = que.back();
    que[p] = b;
    que.pop_back();
    pos[b] = p;
    pos[v] = -1;
  }

  int pop_random(XorShift& rnd) {
    assert(size() > 0);
    int i = rnd.nextUInt(que.size());
    int ret = que[i];
    remove(ret);
    return ret;
  }

  void clear() {
    for (int v : que) {
      pos[v] = -1;
    }
    que.clear();
  }

  bool exists(int v) const {
    return pos[v] != -1;
  }

  int size() const {
    return que.size();
  }
};

struct Counter {
  vector<ull> cnt;

  void add(int i) {
    if (i >= cnt.size()) {
      cnt.resize(i+1);
    }
    ++cnt[i];
  }

  void print() {
    cerr << "counter:[";
    for (int i = 0; i < cnt.size(); ++i) {
      cerr << cnt[i] << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

struct Timer {
  vector<ull> at;
  vector<ull> sum;

  void start(int i) {
    if (i >= at.size()) {
      at.resize(i+1);
      sum.resize(i+1);
    }
    at[i] = get_tsc();
  }

  void stop(int i) {
    sum[i] += (get_tsc() - at[i]);
  }

  void print() {
    cerr << "timer:[";
    for (int i = 0; i < at.size(); ++i) {
      cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
      if (i % 10 == 9) cerr << endl;
    }
    cerr << "]" << endl;
  }
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
constexpr inline T sq(T v) { return v * v; }

void debug_vec(const vi& vec) {
  debugStr("[");
  for (int i = 0; i < vec.size(); ++i) {
    debug("%d ", vec[i]);
  }
  debugStr("]");
}

template<class T>
void erase_vec(vector<T>& vec, const T& value) {
  vec.erase(remove(vec.begin(), vec.end(), value), vec.end());
}

XorShift rnd;
Timer timer;
Counter counter;

constexpr double PI = 3.141592653589793238;
constexpr int INF = 1 << 28;
constexpr ull MAX_ULL = 0xFFFFFFFFFFFFFFFFull;

//////// end of template ////////

int N, E;
double C;
vvi G;
int est_ans;
constexpr double INITIAL_COOLER = 0.1;
constexpr double FINAL_COOLER = 3.0;
double cooler;

struct BitSet {
  vector<ull> bits;

  BitSet(int size) : bits((size + 63) >> 6) {}

  void set(int pos) {
    bits[pos >> 6] |= (1ull << (pos & 0x3F));
  }

  void clear(int pos) {
    bits[pos >> 6] &= ~(1ull << (pos & 0x3F));
  }

  bool get(int pos) const {
    return (bits[pos >> 6] & (1ull << (pos & 0x3F))) != 0;
  }

  int size() const {
    return bits.size() << 6;
  }
};

struct Result {
  vi value;
  int score;

  bool verify() const {
    set<int> len;
    for (int i = 0; i < N; ++i) {
      for (int a : G[i]) {
        if (i < a) {
          int l = abs(value[i] - value[a]);
          if (len.find(l) != len.end()) {
            debug("fail %d\n", l);
            return false;
          }
          len.insert(l);
        }
      }
    }
    return true;
  }
};

vi random_order() {
  vi order;
  vector<bool> unable_use(N);
  vi que = {(int)rnd.nextUInt(N)};
  while (!que.empty()) {
    int pos = rnd.nextUInt(que.size());
    swap(que[pos], que.back());
    int use = que.back();
    que.pop_back();
    if (unable_use[use]) {
      continue;
    }
    unable_use[use] = true;
    order.push_back(use);
    for (int a : G[use]) {
      if (!unable_use[a]) {
        que.push_back(a);
      }
    }
  }
  return order;
}

vi sparse_order() {
  // vi order;
  // for (int i = 0; i < N; ++i) {
  //   order.push_back(i);
  // }
  // for (int i = 0; i < N - 1; ++i) {
  //   int pos = rnd.nextUInt(N - i) + i;
  //   swap(order[i], order[pos]);
  // }

  vi see;
  for (int i = 0; i < N; ++i) {
    see.push_back(i);
  }
  for (int i = 0; i < N - 1; ++i) {
    int pos = rnd.nextUInt(N - i) + i;
    swap(see[i], see[pos]);
  }
  vi order;
  vi adj_cnt(N);
  for (int i = 0; i < N; ++i) {
    int min = N;
    int mi = -1;
    for (int j = 0; j < N; ++j) {
      if (adj_cnt[see[j]] < min) {
        min = adj_cnt[see[j]];
        mi = see[j];
      }
    }
    order.push_back(mi);
    adj_cnt[mi] += N;
    for (int adj : G[mi]) {
      adj_cnt[adj]++;
    }
  }

  // vi order;
  // vi adj_cnt(N);
  // vi que(N);
  // que[rnd.nextUInt(N)] = 1;
  // for (int i = 0; i < N; ++i) {
  //   int min = N;
  //   int mi = -1;
  //   for (int j = 0; j < N; ++j) {
  //     if (que[j] == 1 && adj_cnt[j] < min) {
  //       min = adj_cnt[j];
  //       mi = j;
  //     }
  //   }
  //   order.push_back(mi);
  //   que[mi] = 2;
  //   for (int adj : G[mi]) {
  //     if (que[adj] == 0) {
  //       que[adj] = 1;
  //     }
  //     adj_cnt[adj]++;
  //   }
  // }
  return order;
}

void set_used_edge(BitSet& used_edge_f, BitSet& used_edge_b, int len) {
  used_edge_f.set(len);
  const ull mod = (len - 1) & 0x3F;
  used_edge_b.bits[(len - 1) >> 6] |= 1ull << (63 - mod);
}

void clear_used_edge(BitSet& used_edge_f, BitSet& used_edge_b, int len) {
  used_edge_f.clear(len);
  const ull mod = (len - 1) & 0x3F;
  used_edge_b.bits[(len - 1) >> 6] &= ~(1ull << (63 - mod));
}

bool ok_adj(const vi& adj_vs, int nv) {
  int left = 0;
  int right = adj_vs.size() - 1;
  while (left < right && adj_vs[left] < nv) {
    int upper = nv + (nv - adj_vs[left]);
    while (upper < adj_vs[right]) {
      right--;
    }
    if (upper == adj_vs[right]) {
      return false;
    }
    left++;
  }
  return true;
}

int select_nv(const BitSet& unable_use, const vi& adj_vs, int best, double skip_prob) {
  for (int i = 0; i < unable_use.bits.size(); ++i) {
    ull b64 = ~unable_use.bits[i];
    while (b64 != 0) {
      int nv = (i << 6) + __builtin_ctzll(b64);
      if (nv >= best) {
        return -1;
      }
      b64 &= b64 - 1;
      if (rnd.nextDouble() < skip_prob) continue;
      if (!ok_adj(adj_vs, nv)) continue;
      return nv;
    }
  }
  return -1;
}

vi select_destroy(vi& ans, vi& ans_back, int start) {
  static IndexSet que(N);
  que.clear();
  vi removed;
  // int pos = rnd.nextUInt(N);
  que.add(start);
  // int cnt = rnd.nextUInt((int)sqrt(N) * 2 + 1) + 1;
  int cnt = rnd.nextUInt(min(80, N / 5)) + 1;
  for (int i = 0; i < cnt; ++i) {
    int v = que.pop_random(rnd);
    removed.push_back(v);
    ans_back[v] = ans[v];
    ans[v] = -1;
    for (int adj : G[v]) {
      if (ans[adj] != -1 && !que.exists(adj)) {
        que.add(adj);
      }
    }
  }
  return removed;
}

void set_unable(BitSet& unable_use, BitSet& used_edge_f, BitSet& used_edge_b, int center, int max_len) {
  const int shift = center >> 6;
  if ((center & 0x3F) == 0) {
    for (int j = 0; j < shift; ++j) {
      unable_use.bits[shift + j] |= used_edge_f.bits[j];
      unable_use.bits[shift - j - 1] |= used_edge_b.bits[j];
    }
    for (int j = shift; j <= (max_len >> 6); ++j) {
      unable_use.bits[j + shift] |= used_edge_f.bits[j];
    }
  } else {
    const int mod = center & 0x3F;
    for (int j = 0; j < shift; ++j) {
      unable_use.bits[shift + j] |= used_edge_f.bits[j] << mod;
      unable_use.bits[shift + j + 1] |= used_edge_f.bits[j] >> (64 - mod);
      unable_use.bits[shift - j] |= used_edge_b.bits[j] >> (64 - mod);
      unable_use.bits[shift - j - 1] |= used_edge_b.bits[j] << mod;
    }
    unable_use.bits[shift + shift] |= used_edge_f.bits[shift] << mod;
    unable_use.bits[shift + shift + 1] |= used_edge_f.bits[shift] >> (64 - mod);
    unable_use.bits[0] |= used_edge_b.bits[shift] >> (64 - mod);
    for (int j = shift + 1; j <= (max_len >> 6); ++j) {
      unable_use.bits[j + shift] |= used_edge_f.bits[j] << mod;
      unable_use.bits[j + shift + 1] |= used_edge_f.bits[j] >> (64 - mod);
    }
  }
}

Result solve_single(int best, const vi& order) {
  BitSet unable_use(est_ans * 15);
  vi ans(N, -1);
  ans[order[0]] = 0;
  BitSet used_edge(unable_use.size());
  BitSet used_edge_rev(unable_use.size());
  int max_len = 0;
  vi adj_vs;
  double skip_prob = rnd.nextDouble() * 0.05;
  for (int i = 1; i < N; ++i) {
    int cur = order[i];
    int max_pos = 0;
    adj_vs.clear();
    for (int adj : G[cur]) {
      if (ans[adj] != -1) {
        adj_vs.push_back(ans[adj]);
        set_unable(unable_use, used_edge, used_edge_rev, ans[adj], max_len);
        max_pos = max(max_pos, ans[adj] + max_len);
      }
    }
    sort(adj_vs.begin(), adj_vs.end());
    for (int j = 0; j < i; ++j) {
      unable_use.set(ans[order[j]]);
    }
    ans[cur] = select_nv(unable_use, adj_vs, best, skip_prob);
    if (ans[cur] == -1) {
      Result res = {ans, INF};
      return res;
    }
    for (int adj : G[cur]) {
      if (ans[adj] != -1) {
        int len = abs(ans[cur] - ans[adj]);
        set_used_edge(used_edge, used_edge_rev, len);
        max_len = max(max_len, len);
      }
    }
    fill(unable_use.bits.begin(), unable_use.bits.begin() + (max_pos >> 6) + 1, 0ull);
  }
  Result res = {ans, *max_element(ans.begin(), ans.end())};
  return res;
}

Result improve(Result& res, ll timelimit) {
  vi ans = res.value;
  BitSet unable_use(est_ans * 15);
  BitSet used_edge(unable_use.size());
  BitSet used_edge_rev(unable_use.size());
  auto minmax = minmax_element(ans.begin(), ans.end());
  int best_score = *minmax.second - *minmax.first;
  int max_idx = minmax.second - ans.begin();
  vi best_ans = ans;
  vi ans_back(N);
  vi adj_vs;
  int max_len = 0;
  for (int i = 0; i < N; ++i) {
    for (int adj : G[i]) {
      if (i < adj) {
        const int len = abs(ans[i] - ans[adj]);
        set_used_edge(used_edge, used_edge_rev, len);
        max_len = max(max_len, len);
      }
    }
  }
  debug("score:%d\n", best_score);
  for (int turn = 0; ; ++turn) {
    ll elapsed = get_elapsed_msec();
    if (elapsed > timelimit) {
      debug("total turn:%d\n", turn);
      break;
    }
    vi destroy = select_destroy(ans, ans_back, max_idx);
    for (int rmi : destroy) {
      ans[rmi] = ans_back[rmi];
    }
    const int max_v = ans[max_idx];
    for (int rmi : destroy) {
      for (int adj : G[rmi]) {
        if (ans[adj] == -1) continue;
        const int len = abs(ans[rmi] - ans[adj]);
        clear_used_edge(used_edge, used_edge_rev, len);
      }
      ans[rmi] = -1;
    }
    bool ok = true;
    for (int i = 0; i < destroy.size(); ++i) {
      const int cur = destroy[i];
      int max_pos = 0;
      adj_vs.clear();
      for (int adj : G[cur]) {
        if (ans[adj] != -1) {
          adj_vs.push_back(ans[adj]);
          set_unable(unable_use, used_edge, used_edge_rev, ans[adj], max_len);
          max_pos = max(max_pos, ans[adj] + max_len);
        }
      }
      sort(adj_vs.begin(), adj_vs.end());
      for (int j = 0; j < N; ++j) {
        if (ans[j] != -1) {
          unable_use.set(ans[j]);
        }
      }
      ans[cur] = select_nv(unable_use, adj_vs, max_v + 1, 0.1);
      // if (ans[cur] == -1) {
      //   ok = false;
      //   break;
      // }
      if (ans[cur] == -1 || ans[cur] > max_v) {
        fill(unable_use.bits.begin(), unable_use.bits.begin() + (max_pos >> 6) + 1, 0ull);
        ok = false;
        break;
      }
      for (int adj : G[cur]) {
        if (ans[adj] != -1) {
          int len = abs(ans[cur] - ans[adj]);
          set_used_edge(used_edge, used_edge_rev, len);
          max_len = max(max_len, len);
        }
      }
      fill(unable_use.bits.begin(), unable_use.bits.begin() + (max_pos >> 6) + 1, 0ull);
    }
    int score = INF;
    if (ok) {
      minmax = minmax_element(ans.begin(), ans.end());
      score = *minmax.second - *minmax.first;
      // if (score > best_score) {
      //   ok = false;
      // }
      // ok = score <= best_score ? true : rnd.nextDouble() < exp((best_score - score) * 0.1);
    }
    if (ok) {
      if (score < best_score) {
        debug("score:%d at turn %d destroy size:%lu\n", score, turn, destroy.size());
        best_score = score;
        best_ans = ans;
      }
        // debug("score:%d at turn %d destroy size:%lu\n", score, turn, destroy.size());
      max_idx = minmax.second - ans.begin();
    } else {
      for (int rmi : destroy) {
        if (ans[rmi] == -1) break;
        for (int adj : G[rmi]) {
          if (ans[adj] == -1) continue;
          const int len = abs(ans[rmi] - ans[adj]);
          clear_used_edge(used_edge, used_edge_rev, len);
        }
        ans[rmi] = -1;
      }
      for (int rmi : destroy) {
        ans[rmi] = ans_back[rmi];
        for (int adj : G[rmi]) {
          if (ans[adj] == -1) continue;
          const int len = abs(ans[rmi] - ans[adj]);
          set_used_edge(used_edge, used_edge_rev, len);
        }
      }
    }
  }
  int min = *min_element(best_ans.begin(), best_ans.end());
  for (int i = 0; i < N; ++i) {
    best_ans[i] -= min;
  }
  Result ret = {best_ans, best_score};
  return ret;
}

Result improve_iter(Result& res, ll timelimit) {
  vi ans = res.value;
  debug_vec(ans);
  debugln();
  IndexSet cands(res.score);
  for (int i = 0; i < res.score; ++i) {
    cands.add(i);
  }
  for (int v : ans) {
    if (v != res.score) {
      cands.remove(v);
    }
  }
  vi used_edges(res.score + 1);
  for (int i = 0; i < N; ++i) {
    for (int adj : G[i]) {
      if (i < adj) {
        const int len = abs(ans[i] - ans[adj]);
        used_edges[len]++;
      }
    }
  }
  IndexSet que(N);
  vi from(N, -1);
  vi ans_back(N);
  IndexSet changed(N);
  vi removed;
  int max_idx = max_element(ans.begin(), ans.end()) - ans.begin();
  for (int turn = 0; ; ++turn) {
    if (cands.size() == 0) {
      break;
    }
    ll elapsed = get_elapsed_msec();
    if (elapsed > timelimit) {
      debug("total turn:%d\n", turn);
      break;
    }
    // debugStr("cands:");
    // debug_vec(cands.que);
    // debugln();
    // debugStr("ans:");
    // debug_vec(ans);
    // debugln();
    // debugStr("used_edges:");
    // debug_vec(used_edges);
    // debugln();
    assert(cands.size() + N == ans[max_idx] + 1);
    for (int i = 0; i < N; ++i) {
      assert(i == max_idx || !cands.exists(ans[i]));
      for (int adj : G[i]) {
        const int len = abs(ans[i] - ans[adj]);
        assert(used_edges[len] == 1);
      }
    }
    que.clear();
    changed.clear();
    changed.add(max_idx);
    ans_back = ans;
    ans[max_idx] = cands.pop_random(rnd);
    for (int adj : G[max_idx]) {
      used_edges[abs(ans_back[max_idx] - ans[adj])]--;
    }
    for (int adj : G[max_idx]) {
      const int len = abs(ans[max_idx] - ans[adj]);
      used_edges[len]++;
      if (used_edges[len] > 1 && from[adj] == -1) {
        que.add(adj);
        from[adj] = max_idx;
      }
    }
    debug("turn:%d que.size:%d\n", turn, que.size());
    for (int loop = 0; loop < 1000 && que.size() > 0; ++loop) {
      const int cur = que.pop_random(rnd);
      const int f = from[cur];
      const int ov = ans[cur];
      // debug("loop:%d cur:%d\n", loop, cur);
      bool skip = true;
      for (int adj : G[cur]) {
        if (used_edges[abs(ans[cur] - ans[adj])] != 1) {
          skip = false;
          break;
        }
      }
      if (skip) {
        from[cur] = -1;
        continue;
      }
      removed.clear();
      int best_size = N;
      int best_nv = -1;
      for (int i = 0; i < 100 && cands.size() > 0; ++i) {
        int nv = cands.pop_random(rnd);
        removed.push_back(nv);
        if (used_edges[abs(nv - ans[f])] != 0) {
          continue;
        }
        for (int adj : G[cur]) {
          used_edges[abs(ans[cur] - ans[adj])]--;
        }
        int invalid_cnt = 0;
        for (int adj : G[cur]) {
          const int len = abs(nv - ans[adj]);
          used_edges[len]++;
          if (used_edges[len] > 1) {
            invalid_cnt++;
          }
        }
        if (invalid_cnt < best_size) {
          best_size = invalid_cnt;
          best_nv = nv;
        }
        for (int adj : G[cur]) {
          used_edges[abs(ans[cur] - ans[adj])]++;
          used_edges[abs(nv - ans[adj])]--;
        }
      }
      // debug("best_size:%d\n", best_size);
      if (best_nv == -1) {
        que.add(cur);
      } else {
        // debug("nv:%d\n", best_nv);
        if (!changed.exists(cur)) {
          changed.add(cur);
        }
        for (int adj : G[cur]) {
          used_edges[abs(ans[cur] - ans[adj])]--;
        }
        for (int adj : G[cur]) {
          const int len = abs(best_nv - ans[adj]);
          used_edges[len]++;
          if (!que.exists(adj) && used_edges[len] > 1) {
            que.add(adj);
            from[adj] = cur;
          }
        }
        ans[cur] = best_nv;
        cands.add(ov);
        from[cur] = -1;
      }

      // debugStr("removed:");
      // debug_vec(removed);
      // debugln();
      for (int rm : removed) {
        if (rm != best_nv) {
          cands.add(rm);
        }
      }
    }
    debug("que.size:%d\n", que.size());
    if (que.size() == 0) {
      max_idx = max_element(ans.begin(), ans.end()) - ans.begin();
      debug("score:%d at turn %d\n", ans[max_idx], turn);
      for (int i = res.score - 1; i >= ans[max_idx]; --i) {
        if (cands.exists(i)) {
          cands.remove(i);
        }
      }
    } else {
      // debugStr("reverts:");
      // debug_vec(cands.que);
      // debugln();
      // debug_vec(changed.que);
      // debugln();
      // debug_vec(ans);
      // debugln();
      // debug_vec(ans_back);
      // debugln();
      for (int ch : changed.que) {
        cands.add(ans[ch]);
        for (int adj : G[ch]) {
          if (ans[adj] != -1) {
            used_edges[abs(ans[ch] - ans[adj])]--;
          }
        }
        ans[ch] = -1;
      }
      for (int ch : changed.que) {
        ans[ch] = ans_back[ch];
        if (ch != max_idx) {
          cands.remove(ans[ch]);
        }
        for (int adj : G[ch]) {
          if (ans[adj] != -1) {
            used_edges[abs(ans[ch] - ans[adj])]++;
            assert(used_edges[abs(ans[ch] - ans[adj])] == 1);
          }
        }
      }
      fill(from.begin(), from.end(), -1);
    }
    changed.clear();
  }
  int min = *min_element(ans.begin(), ans.end());
  for (int i = 0; i < N; ++i) {
    ans[i] -= min;
  }
  Result ret = {ans, ans[max_idx]};
  return ret;
}

Result solve_random(int best) {
  best = min(best, est_ans * 20);
  vi order = sparse_order();
  vi cands(best - 1);
  iota(cands.begin(), cands.end(), 1);
  vi ans(N, -1);
  ans[order[0]] = 0;
  BitSet used_edge(best);
  vi adj_vs;
  vi discard;
  for (int i = 1; i < N; ++i) {
    int cur = order[i];
    adj_vs.clear();
    for (int adj : G[cur]) {
      if (ans[adj] == -1) continue;
      adj_vs.push_back(ans[adj]);
    }
    sort(adj_vs.begin(), adj_vs.end());
    discard.clear();
    while (!cands.empty()) {
      int pos = rnd.nextUInt(cands.size());
      swap(cands[pos], cands.back());
      int nv = cands.back();
      cands.pop_back();
      bool ok = true;
      for (int adj : G[cur]) {
        if (ans[adj] == -1) continue;
        int diff = abs(ans[adj] - nv);
        if (used_edge.get(diff)) {
          ok = false;
          break;
        }
      }
      if (ok) {
        ok = ok_adj(adj_vs, nv);
      }
      if (ok) {
        for (int adj : G[cur]) {
          if (ans[adj] == -1) continue;
          int diff = abs(ans[adj] - nv);
          used_edge.set(diff);
        }
        ans[cur] = nv;
        break;
      } else {
        discard.push_back(nv);
      }
    }
    if (ans[cur] == -1) {
      // debug("fail at %d\n", i);
      ans[cur] = INF;
      break;
    }
    cands.insert(cands.end(), discard.begin(), discard.end());
  }
  Result res = {ans, *max_element(ans.begin(), ans.end())};
  return res;
}

Result solve_greedy(int best) {
  vi order(N);
  iota(order.begin(), order.end(), 0);
  for (int i = 0; i < N - 1; ++i) {
    int pos = rnd.nextUInt(N - i) + i;
    swap(order[i], order[pos]);
  }
  vi ans(N, -1);
  ans[order.back()] = 0;
  order.pop_back();
  BitSet used_edge(est_ans * 15);
  const int REP_CNT = min(N, (est_ans > 1000000 ? 7000000 : 9000000) / E);
  for (int i = 1; !order.empty(); ++i) {
    if (i + order.size() >= best) {
      Result res = {ans, INF};
      return res;
    }
    if (N > 200 && get_elapsed_msec() > TL - 1800) {
      break;
    }
    for (int j = 0; j < min((int)order.size(), REP_CNT); ++j) {
      // int swp = rnd.nextUInt(order.size() - j) + j;
      // swap(order[j], order[swp]);
      int cur = order[j];
      bool ok = true;
      for (int adj : G[cur]) {
        if (ans[adj] == -1) continue;
        const int diff = i - ans[adj];
        if (used_edge.get(diff)) {
          ok = false;
          break;
        }
      }
      if (!ok) continue;
      for (int adj : G[cur]) {
        if (ans[adj] == -1) continue;
        const int diff = i - ans[adj];
        used_edge.set(diff);
      }
      ans[cur] = i;
      swap(order[j], order.back());
      order.pop_back();
      break;
    }
  }
  if (order.empty()) {
    Result res = {ans, *max_element(ans.begin(), ans.end())};
    return res;
  }
  debug("end greedy:%lu/%d\n", N - order.size(), N);
  vi used_v;
  BitSet unable_use(est_ans * 15);
  BitSet used_edge_rev(unable_use.size());
  int max_len = 1;
  for (int i = 0; i < N; ++i) {
    if (ans[i] == -1) continue;
    used_v.push_back(ans[i]);
    for (int adj : G[i]) {
      if (adj < i || ans[adj] == -1) continue;
      const int len = abs(ans[i] - ans[adj]);
      set_used_edge(used_edge, used_edge_rev, len);
      max_len = max(max_len, len);
    }
  }
  vi adj_vs;
  double skip_prob = rnd.nextDouble() * 0.05;
  for (int i = 0; i < order.size(); ++i) {
    int cur = order[i];
    int max_pos = 0;
    adj_vs.clear();
    for (int adj : G[cur]) {
      if (ans[adj] != -1) {
        adj_vs.push_back(ans[adj]);
        set_unable(unable_use, used_edge, used_edge_rev, ans[adj], max_len);
        max_pos = max(max_pos, ans[adj] + max_len);
      }
    }
    sort(adj_vs.begin(), adj_vs.end());
    for (int v : used_v) {
      unable_use.set(v);
    }
    ans[cur] = select_nv(unable_use, adj_vs, best, skip_prob);
    if (ans[cur] == -1) {
      Result res = {ans, INF};
      return res;
    }
    used_v.push_back(ans[cur]);
    for (int adj : G[cur]) {
      if (ans[adj] != -1) {
        int len = abs(ans[cur] - ans[adj]);
        set_used_edge(used_edge, used_edge_rev, len);
        max_len = max(max_len, len);
      }
    }
    fill(unable_use.bits.begin(), unable_use.bits.begin() + (max_pos >> 6) + 1, 0ull);
  }

  if (find(ans.begin(), ans.end(), -1) != ans.end()) {
    Result res = {ans, INF};
    return res;
  }
  Result res = {ans, *max_element(ans.begin(), ans.end())};
  return res;
}

Result improve_mini(const Result& res, ll timelimit) {
  const int MARGIN = min(1000, res.score / 1000);
  vi cands;
  vi ans_tmp(res.value);
  sort(ans_tmp.begin(), ans_tmp.end());
  int ati = 1;
  for (int i = 1; i <= res.score + MARGIN; ++i) {
    if (ati < ans_tmp.size() && i == ans_tmp[ati]) {
      ati++;
      continue;
    }
    cands.push_back(i);
  }
  vi ans = res.value;
  BitSet used_edge(res.score + MARGIN + 1);
  for (int i = 0; i < N; ++i) {
    for (int adj : G[i]) {
      used_edge.set(abs(ans[i] - ans[adj]));
    }
  }
  vi best_ans(ans);
  int best_score = res.score;
  int max_v = res.score;
  int min_v = 0;
  vi adj_vs;
  for (int turn = 0;; ++turn) {
    if ((turn & 0xF) == 0) {
      auto elapsed = get_elapsed_msec();
      if (elapsed > timelimit) {
        debug("improve turn:%d\n", turn);
        break;
      }
    }
    const int cur = rnd.nextUInt(N);
    adj_vs.clear();
    for (int adj : G[cur]) {
      adj_vs.push_back(ans[adj]);
      used_edge.clear(abs(ans[cur] - ans[adj]));
    }
    sort(adj_vs.begin(), adj_vs.end());
    const int prev_v = ans[cur];
    ans[cur] = -1;
    for (int i = 0; i < min((int)cands.size(), 5); ++i) {
      int pos = rnd.nextUInt(cands.size() - i);
      int nv = cands[pos];
      assert(nv != prev_v);
      swap(cands[pos], cands[cands.size() - 1 - i]);
      if (nv < min_v - MARGIN || nv > max_v + MARGIN) {
        // debug("erase %d %d %d %d\n", *(cands.end() - i - 1), max_v, min_v, nv);
        cands.erase(cands.end() - i - 1);
        continue;
      }
      bool ok = true;
      for (int adj_v : adj_vs) {
        int diff = abs(adj_v - nv);
        if (used_edge.get(diff)) {
          ok = false;
          break;
        }
      }
      if (ok) {
        ok = ok_adj(adj_vs, nv);
      }
      if (ok) {
        for (int adj_v : adj_vs) {
          int diff = abs(adj_v - nv);
          used_edge.set(diff);
        }
        ans[cur] = nv;
        cands[cands.size() - 1 - i] = prev_v;
        break;
      }
    }
    if (ans[cur] == -1) {
      // revert
      ans[cur] = prev_v;
      for (int adj_v : adj_vs) {
        int diff = abs(adj_v - ans[cur]);
        used_edge.set(diff);
      }
    } else {
      auto minmax = minmax_element(ans.begin(), ans.end());
      max_v = *minmax.second;
      min_v = *minmax.first;
      int score = max_v - min_v;
      if (score < best_score) {
        debug("%d -> %d (%d) at turn %d\n", prev_v, ans[cur], score, turn);
        best_score = score;
        best_ans = ans;
      }
    }
  }
  min_v = *min_element(best_ans.begin(), best_ans.end());
  for (int i = 0; i < N; ++i) {
    best_ans[i] -= min_v;
  }
  assert(best_score == *max_element(best_ans.begin(), best_ans.end()));
  Result ret = {best_ans, best_score};
  return ret;
}

bool accept(double old_e, double new_e) {
  if (new_e <= old_e) return true;
  double v = cooler * (old_e - new_e);
  if (v < -10) return false;
  return rnd.nextDouble() < exp(v);
}

Result improve_with_invalid(const Result& res, ll timelimit) {
  const int MARGIN = min(1000, res.score / 1000);
  cooler = INITIAL_COOLER;
  vi cands;
  vi ans_tmp(res.value);
  sort(ans_tmp.begin(), ans_tmp.end());
  int ati = 1;
  for (int i = 1; i <= res.score + MARGIN; ++i) {
    if (ati < ans_tmp.size() && i == ans_tmp[ati]) {
      ati++;
      continue;
    }
    cands.push_back(i);
  }
  vi ans = res.value;
  vi used_edge(res.score + MARGIN + 1);
  for (int i = 0; i < N; ++i) {
    for (int adj : G[i]) {
      if (i < adj) {
        used_edge[abs(ans[i] - ans[adj])]++;
      }
    }
  }
  vi best_ans(ans);
  int score = res.score;
  int best_score = res.score;
  int edge_pena = 0;
  int max_v = res.score;
  int min_v = 0;
  double ratio_pena = 4.0 * score / E;
  int accept_cnt = 0;
  ll begin_time = get_elapsed_msec();
  for (int turn = 0;; ++turn) {
    if ((turn & 0xFFF) == 0) {
      auto elapsed = get_elapsed_msec();
      if (elapsed > timelimit) {
        debug("improve turn:%d\n", turn);
        break;
      }
      ratio_pena *= 1.001;
      double ratio_elapsed = 1.0 * (elapsed - begin_time) / (timelimit - begin_time);
      cooler = exp(log(INITIAL_COOLER) * (1.0 - ratio_elapsed) + log(FINAL_COOLER) * ratio_elapsed);
      debug("turn:%d ratio_pena:%.4f cooler:%.4f score:%d pena:%d accept:%d\n", turn, ratio_pena, cooler, score, edge_pena, accept_cnt);
    }
    const int cur = rnd.nextUInt(N);
    const int prev_v = ans[cur];
    const int cand_pos = rnd.nextUInt(cands.size());
    const int nv = cands[cand_pos];
    ans[cur] = nv;
    int next_pena = edge_pena;
    for (int adj : G[cur]) {
      const int diff_old = abs(prev_v - ans[adj]);
      const int diff_new = abs(nv - ans[adj]);
      next_pena -= used_edge[diff_old] == 1 ? 0 : 2 * used_edge[diff_old] - 3;
      used_edge[diff_old]--;
      used_edge[diff_new]++;
      next_pena += used_edge[diff_new] == 1 ? 0 : 2 * used_edge[diff_new] - 3;
    }
    int next_max_v, next_min_v;
    if (prev_v == max_v || prev_v == min_v) {
      auto minmax = minmax_element(ans.begin(), ans.end());
      next_max_v = *minmax.second;
      next_min_v = *minmax.first;
    } else {
      next_max_v = max(max_v, nv);
      next_min_v = min(min_v, nv);
    }
    int next_score = next_max_v - next_min_v;
    const double old_e = score + edge_pena * ratio_pena;
    const double new_e = next_score + next_pena * ratio_pena;
    bool force = edge_pena == 0 && next_score < score && (rnd.nextUInt() & 0x3FF) == 0;
    if (force || accept(old_e, new_e)) {
      score = next_score;
      edge_pena = next_pena;
      if (edge_pena == 0 && score < best_score) {
        best_score = score;
        best_ans = ans;
        debug("score:%d at turn %d\n", score, turn);
      }
      cands[cand_pos] = prev_v;
      max_v = next_max_v;
      min_v = next_min_v;
      accept_cnt++;
    } else {
      ans[cur] = prev_v;
      for (int adj : G[cur]) {
        const int diff_old = abs(prev_v - ans[adj]);
        const int diff_new = abs(nv - ans[adj]);
        used_edge[diff_old]++;
        used_edge[diff_new]--;
      }
    }
  }
  min_v = *min_element(best_ans.begin(), best_ans.end());
  for (int i = 0; i < N; ++i) {
    best_ans[i] -= min_v;
  }
  assert(best_score == *max_element(best_ans.begin(), best_ans.end()));
  Result ret = {best_ans, best_score};
  return ret;
}

vi solve() {
  ll begin_time = get_elapsed_msec();
  Result best_result = solve_greedy(max(8, (int)(est_ans * 1.5)));
  ll cur_time = get_elapsed_msec();
  ll max_time = cur_time - begin_time;
  begin_time = cur_time;
  debug("score:%d at turn 0\n", best_result.score);
  for (int turn = 1; ; turn++) {
    if (get_elapsed_msec() + max_time > TL * 8 / 10) {
      debug("turn:%d\n", turn);
      break;
    }
    Result res = est_ans < 200 && (turn & 1) == 0 ? solve_random(best_result.score) : solve_greedy(best_result.score);
    if (res.score < INF) {
      debug("score:%d at turn %d\n", res.score, turn);
    }
    if (res.score < best_result.score) {
      swap(best_result.value, res.value);
      best_result.score = res.score;
    }
    cur_time = get_elapsed_msec();
    max_time = max(max_time, cur_time - begin_time);
    begin_time = cur_time;
  }
  best_result = improve(best_result, TL);
  return best_result.value;
}

int main() {
  start_time = get_time();
  scanf("%d %d", &N, &E);
  C = E * 2.0 / (N * (N - 1));
  est_ans = int(pow(N, 2.6) * pow(C, 1.5) * 0.2);
  debug("N=%d E=%d C=%.3f est=%d\n", N, E, C, est_ans);
  G.resize(N);
  for (int i = 0; i < E; ++i) {
    int U, V;
    scanf("%d %d", &U, &V);
    G[U].push_back(V);
    G[V].push_back(U);
  }
  vi ans = solve();
  // sort(ans.begin(), ans.end());
  debug_vec(ans);
  debugln();
  for (int i = 0; i < ans.size(); ++i) {
    printf("%d%s", ans[i], i == ans.size() - 1 ? "\n" : " ");
  }
  printf("\n");
}
