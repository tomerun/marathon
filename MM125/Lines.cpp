// C++11
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
#include <set>
#include <string>

using namespace std;

constexpr array<int, 4> DR = {1, 0, -1, 0};
constexpr array<int, 4> DC = {0, 1, 0, -1};
constexpr int EMPTY = 0;

struct Hand {
  int r1, c1, r2, c2;
};

int N, C, elapsed_time;
array<array<int, 11>, 11> grid;
array<int, 3> next_balls;

inline bool inside(int p) {
  return 0 <= p && p < N;
}

Hand solve() {
  for (int r=0; r<N; r++) {
    for (int c=0; c<N; c++) {
      if (grid[r][c] == EMPTY) continue;
      for (int m=0; m<4; m++) {
        int r2 = r + DR[m];
        int c2 = c + DC[m];
        if (inside(r2) && inside(c2) && grid[r2][c2]==EMPTY) {
          return Hand{r, c, r2, c2};
        }
      }
    }   
  }
  return Hand{0, 0, 0, 0};
}

int main() {
  scanf("%d %d", &N, &C);
  for (int i = 0; i < 1000; ++i) {
    for (int j = 0; j < N; ++j) {
      for (int k = 0; k < N; ++k) {
        scanf("%d", &grid[j][k]);
      }
    }
    for (int j = 0; j < 3; ++j) {
      scanf("%d", &next_balls[j]);
    }
    scanf("%d", &elapsed_time);
    Hand hand = solve();
    printf("%d %d %d %d\n", hand.r1, hand.c1, hand.r2, hand.c2);
    fflush(stdout);
  }
}