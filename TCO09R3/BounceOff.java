import java.awt.Point;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BounceOff {

	private static final boolean DEBUG = false;
	private static final double TIMER = 9;
	private static final long GIGA = 1000 * 1000 * 1000;
	private static final double EPS = 1e-9;
	private static final double DELAY_STEP = 0.2;
	private static final double DELAY_HIT = 0.1;
	private static final double NOOBST_BONUS = 1.8;
	private static final double A = 25;
	private static final int END_REP_TIME = 259;

	int initX;
	Point[] tar;
	int R;
	int N;
	int[] value;

	public String[] placeObstacles(int[] objectX, int[] objectY, int R) {
		long startTime = System.nanoTime();
		this.N = objectX.length - 1;
		this.initX = objectX[0];
		tar = new Point[N];
		value = new int[N];
		for (int i = 0; i < N; ++i) {
			tar[i] = new Point(objectX[i + 1], objectY[i + 1]);
			value[i] = objectY[i + 1] / 100 + 1;
			if (objectY[i + 1] > 400) {
				value[i] += 5;
			}
			if (objectY[i + 1] > 440) {
				value[i] += objectY[i + 1];
			}
		}
		this.R = R;
		double STEP1 = N <= 20 ? 2 : 10;
		double STEP2 = N <= 20 ? 2 : 3;
		//		double DELAY_STEP = N <= 20 ? 0.1 : 0.2;
		ArrayList<MyObstacle> best = new ArrayList<MyObstacle>();
		double bestScore = 0;
		double bestStep = 0;

		for (double STEP = 10; STEP <= Math.max(20, 50 - N); STEP += STEP1) {
			Config cf = new Config();
			cf.visited = new boolean[N];
			cf.diff = new boolean[N];
			cf.obsts = new ArrayList<MyObstacle>();
			cf.v = new Point2D.Double(0, 0);
			cf.p = new Point2D.Double(initX, 490);
			Config next;
			double cur = 0;
			while (cur < END_REP_TIME && cf.hitN < N) {
				next = advance(cf, DELAY_HIT);
				cur += next.t;
				for (int i = 0; i < N; ++i) {
					next.visited[i] = cf.visited[i] || next.diff[i];
					next.diff[i] = false;
				}
				cf = next;
				Config bestCf = advance(cf, STEP);
				int bestVal = (int) (eval(bestCf.diff) * NOOBST_BONUS);
				MyObstacle add = null;
				for (double ad = 0; ad < 8; ad += DELAY_STEP) {
					Config config = advance(cf, ad);
					int left = (int) config.p.x;
					int bottom = (int) Math.ceil(config.p.y) - 1;
					if (left == 0 || left == 499 || bottom == 0) {
						continue;
					}

					ArrayList<MyObstacle> added = new ArrayList<MyObstacle>();
					if (config.v.x != 0) {
						double vx = config.v.x;
						double vy = config.v.y;
						double x0 = config.p.x;
						double y0 = config.p.y;
						int y1 = (int) Math
								.floor(-5 / vx / vx * left * left + (vy / vx + 10 * x0 / vx / vx) * left + y0 - vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						int y2 = (int) Math.floor(-5 / vx / vx * (left + 1) * (left + 1) + (vy / vx + 10 * x0 / vx / vx) * (left + 1) + y0
								- vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						if (y1 == bottom) {
							added.add(new MyObstacle(left, y1, left, y1 + 1));
						}
						if (y2 == bottom) {
							added.add(new MyObstacle(left + 1, y2, left + 1, y2 + 1));
						}
					}
					added.add(new MyObstacle(left, bottom, left + 1, bottom));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom + 1));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 1));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 2));
					added.add(new MyObstacle(left, bottom + 2, left + 1, bottom));
					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 1, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 2, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 2, bottom));
					}

					for (MyObstacle ob : added) {
						cf.obsts.add(ob);
						Config result = advance(cf, STEP);
						if (result != null) {
							int val = eval(result.diff);
							if (val > bestVal || (val == bestVal && result.t < bestCf.t)) {
								bestVal = val;
								bestCf = result;
								add = ob;
							}
						}
						cf.obsts.remove(cf.obsts.size() - 1);
					}

				}

				if (add != null) {
					cf.obsts.add(add);
				}
				cf.p = bestCf.p;
				cf.v = bestCf.v;
				cf.hitN = bestCf.hitN;
				for (int i = 0; i < N; ++i) {
					cf.visited[i] = cf.visited[i] || bestCf.diff[i];
					cf.diff[i] = false;
				}
				cur += bestCf.t;
			}
			double score = calcScore(cf.obsts);
			if (score > bestScore) {
				bestScore = score;
				best.clear();
				best.addAll(cf.obsts);
				bestStep = STEP;
			}
		}

		for (double STEP = 10; STEP <= Math.max(20, 50 - N); STEP += STEP1) {
			Config cf = new Config();
			cf.visited = new boolean[N];
			cf.diff = new boolean[N];
			cf.obsts = new ArrayList<MyObstacle>();
			cf.v = new Point2D.Double(0, 0);
			cf.p = new Point2D.Double(initX, 490);
			Config next;
			double cur = 0;
			while (cur < END_REP_TIME && cf.hitN < N) {
				next = advance(cf, DELAY_HIT);
				cur += next.t;
				for (int i = 0; i < N; ++i) {
					next.visited[i] = cf.visited[i] || next.diff[i];
					next.diff[i] = false;
				}
				cf = next;
				Config bestCf = advance(cf, STEP);
				int bestVal = (int) (eval(bestCf.diff) * NOOBST_BONUS);
				MyObstacle add = null;
				for (double ad = 0; ad < 8; ad += DELAY_STEP) {
					Config config = advance(cf, ad);
					int left = (int) config.p.x;
					int bottom = (int) Math.ceil(config.p.y) - 1;
					if (left == 0 || left == 499 || bottom == 0) {
						continue;
					}

					ArrayList<MyObstacle> added = new ArrayList<MyObstacle>();
					if (config.v.x != 0) {
						double vx = config.v.x;
						double vy = config.v.y;
						double x0 = config.p.x;
						double y0 = config.p.y;
						int y1 = (int) Math
								.floor(-5 / vx / vx * left * left + (vy / vx + 10 * x0 / vx / vx) * left + y0 - vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						int y2 = (int) Math.floor(-5 / vx / vx * (left + 1) * (left + 1) + (vy / vx + 10 * x0 / vx / vx) * (left + 1) + y0
								- vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						if (y1 == bottom) {
							added.add(new MyObstacle(left, y1, left, y1 + 1));
						}
						if (y2 == bottom) {
							added.add(new MyObstacle(left + 1, y2, left + 1, y2 + 1));
						}
					}
					added.add(new MyObstacle(left, bottom, left + 1, bottom));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom + 1));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 1));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 2));
					added.add(new MyObstacle(left, bottom + 2, left + 1, bottom));
					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 1, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 2, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 2, bottom));
					}

					int j = 0;// = bottom == 1 ? 0 : -1;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 3 + j));
					added.add(new MyObstacle(left, bottom + 3 + j, left + 1, bottom + j));
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 2, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 3, bottom));
					}
					for (MyObstacle ob : added) {
						cf.obsts.add(ob);
						Config result = advance(cf, STEP);
						if (result != null) {
							int val = eval(result.diff);
							if (val > bestVal || (val == bestVal && result.t < bestCf.t)) {
								bestVal = val;
								bestCf = result;
								add = ob;
							}
						}
						cf.obsts.remove(cf.obsts.size() - 1);
					}

				}

				if (add != null) {
					cf.obsts.add(add);
				}
				cf.p = bestCf.p;
				cf.v = bestCf.v;
				cf.hitN = bestCf.hitN;
				for (int i = 0; i < N; ++i) {
					cf.visited[i] = cf.visited[i] || bestCf.diff[i];
					cf.diff[i] = false;
				}
				cur += bestCf.t;
			}
			double score = calcScore(cf.obsts);
			if (score > bestScore) {
				bestScore = score;
				best.clear();
				best.addAll(cf.obsts);
				bestStep = STEP;
			}
		}

		for (double STEP = 10; STEP <= 80
				&& (STEP <= Math.max(30, 60 - N) || (System.nanoTime() - startTime) < TIMER * 0.15 * GIGA); STEP += STEP1) {
			Config cf = new Config();
			cf.visited = new boolean[N];
			cf.diff = new boolean[N];
			cf.obsts = new ArrayList<MyObstacle>();
			cf.v = new Point2D.Double(0, 0);
			cf.p = new Point2D.Double(initX, 490);
			Config next;
			double cur = 0;
			while (cur < END_REP_TIME && cf.hitN < N) {
				next = advance(cf, DELAY_HIT);
				cur += next.t;
				for (int i = 0; i < N; ++i) {
					next.visited[i] = cf.visited[i] || next.diff[i];
					next.diff[i] = false;
				}
				cf = next;
				Config bestCf = advance(cf, STEP);
				int bestVal = (int) (eval(bestCf.diff) * NOOBST_BONUS);
				MyObstacle add = null;
				for (double ad = 0; ad < 8; ad += DELAY_STEP) {
					Config config = advance(cf, ad);
					int left = (int) config.p.x;
					int bottom = (int) Math.ceil(config.p.y) - 1;
					if (left == 0 || left == 499 || bottom == 0) {
						continue;
					}

					ArrayList<MyObstacle> added = new ArrayList<MyObstacle>();
					if (config.v.x != 0) {
						double vx = config.v.x;
						double vy = config.v.y;
						double x0 = config.p.x;
						double y0 = config.p.y;
						int y1 = (int) Math
								.floor(-5 / vx / vx * left * left + (vy / vx + 10 * x0 / vx / vx) * left + y0 - vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						int y2 = (int) Math.floor(-5 / vx / vx * (left + 1) * (left + 1) + (vy / vx + 10 * x0 / vx / vx) * (left + 1) + y0
								- vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						if (y1 == bottom) {
							added.add(new MyObstacle(left, y1, left, y1 + 1));
						}
						if (y2 == bottom) {
							added.add(new MyObstacle(left + 1, y2, left + 1, y2 + 1));
						}
					}
					added.add(new MyObstacle(left, bottom, left + 1, bottom));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom + 1));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 1));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 2));
					added.add(new MyObstacle(left, bottom + 2, left + 1, bottom));
					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 1, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 2, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 2, bottom));
					}

					int j = 0;// = bottom == 1 ? 0 : -1;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 3 + j));
					added.add(new MyObstacle(left, bottom + 3 + j, left + 1, bottom + j));
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 2, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 3, bottom));
					}

					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom + j, left + 1, bottom + 3 + j));
						added.add(new MyObstacle(left - 1, bottom + 3 + j, left + 1, bottom + j));
					} else {
						added.add(new MyObstacle(left, bottom + j, left + 2, bottom + 3 + j));
						added.add(new MyObstacle(left, bottom + 3 + j, left + 2, bottom + j));
					}
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 2));
						added.add(new MyObstacle(left - 2, bottom + 2, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 2));
						added.add(new MyObstacle(left, bottom + 2, left + 3, bottom));
					}

					for (MyObstacle ob : added) {
						cf.obsts.add(ob);
						Config result = advance(cf, STEP);
						if (result != null) {
							int val = eval(result.diff);
							if (val > bestVal || (val == bestVal && result.t < bestCf.t)) {
								bestVal = val;
								bestCf = result;
								add = ob;
							}
						}
						cf.obsts.remove(cf.obsts.size() - 1);
					}

				}

				if (add != null) {
					cf.obsts.add(add);
				}
				cf.p = bestCf.p;
				cf.v = bestCf.v;
				cf.hitN = bestCf.hitN;
				for (int i = 0; i < N; ++i) {
					cf.visited[i] = cf.visited[i] || bestCf.diff[i];
					cf.diff[i] = false;
				}
				cur += bestCf.t;
			}
			double score = calcScore(cf.obsts);
			if (score > bestScore) {
				bestScore = score;
				best.clear();
				best.addAll(cf.obsts);
				bestStep = STEP;
			}
		}

		for (double STEP = 8; STEP <= 80 && (System.nanoTime() - startTime) < TIMER * 0.45 * GIGA; STEP += STEP2) {
			Config cf = new Config();
			cf.visited = new boolean[N];
			cf.diff = new boolean[N];
			cf.obsts = new ArrayList<MyObstacle>();
			cf.v = new Point2D.Double(0, 0);
			cf.p = new Point2D.Double(initX, 490);
			Config next;
			double cur = 0;
			while (cur < END_REP_TIME && cf.hitN < N) {
				next = advance(cf, DELAY_HIT);
				cur += next.t;
				for (int i = 0; i < N; ++i) {
					next.visited[i] = cf.visited[i] || next.diff[i];
					next.diff[i] = false;
				}
				cf = next;
				Config bestCf = advance(cf, STEP);
				int bestVal = (int) (eval(bestCf.diff) * NOOBST_BONUS);
				MyObstacle add = null;
				for (double ad = 0; ad < 8; ad += DELAY_STEP) {
					Config config = advance(cf, ad);
					int left = (int) config.p.x;
					int bottom = (int) Math.ceil(config.p.y) - 1;
					if (left == 0 || left == 499 || bottom == 0) {
						continue;
					}

					ArrayList<MyObstacle> added = new ArrayList<MyObstacle>();
					if (config.v.x != 0) {
						double vx = config.v.x;
						double vy = config.v.y;
						double x0 = config.p.x;
						double y0 = config.p.y;
						int y1 = (int) Math
								.floor(-5 / vx / vx * left * left + (vy / vx + 10 * x0 / vx / vx) * left + y0 - vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						int y2 = (int) Math.floor(-5 / vx / vx * (left + 1) * (left + 1) + (vy / vx + 10 * x0 / vx / vx) * (left + 1) + y0
								- vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						if (y1 == bottom) {
							added.add(new MyObstacle(left, y1, left, y1 + 1));
						}
						if (y2 == bottom) {
							added.add(new MyObstacle(left + 1, y2, left + 1, y2 + 1));
						}
					}
					added.add(new MyObstacle(left, bottom, left + 1, bottom));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom + 1));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 1));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 2));
					added.add(new MyObstacle(left, bottom + 2, left + 1, bottom));
					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 1, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 2, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 2, bottom));
					}

					int j = 0;// = bottom == 1 ? 0 : -1;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 3 + j));
					added.add(new MyObstacle(left, bottom + 3 + j, left + 1, bottom + j));
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 2, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 3, bottom));
					}

					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom + j, left + 1, bottom + 3 + j));
						added.add(new MyObstacle(left - 1, bottom + 3 + j, left + 1, bottom + j));
					} else {
						added.add(new MyObstacle(left, bottom + j, left + 2, bottom + 3 + j));
						added.add(new MyObstacle(left, bottom + 3 + j, left + 2, bottom + j));
					}
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 2));
						added.add(new MyObstacle(left - 2, bottom + 2, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 2));
						added.add(new MyObstacle(left, bottom + 2, left + 3, bottom));
					}

					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 4 + j));
					added.add(new MyObstacle(left, bottom + 4 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496) {
						added.add(new MyObstacle(left - 3, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 3, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 4, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 4, bottom));
					}

					for (MyObstacle ob : added) {
						cf.obsts.add(ob);
						Config result = advance(cf, STEP);
						if (result != null) {
							int val = eval(result.diff);
							if (val > bestVal || (val == bestVal && result.t < bestCf.t)) {
								bestVal = val;
								bestCf = result;
								add = ob;
							}
						}
						cf.obsts.remove(cf.obsts.size() - 1);
					}

				}

				if (add != null) {
					cf.obsts.add(add);
				}
				cf.p = bestCf.p;
				cf.v = bestCf.v;
				cf.hitN = bestCf.hitN;
				for (int i = 0; i < N; ++i) {
					cf.visited[i] = cf.visited[i] || bestCf.diff[i];
					cf.diff[i] = false;
				}
				cur += bestCf.t;
			}
			double score = calcScore(cf.obsts);
			if (score > bestScore) {
				bestScore = score;
				best.clear();
				best.addAll(cf.obsts);
				bestStep = STEP;
			}
		}

		for (double STEP = 8; STEP <= 80 && (System.nanoTime() - startTime) < TIMER * 0.85 * GIGA; STEP += STEP2) {
			Config cf = new Config();
			cf.visited = new boolean[N];
			cf.diff = new boolean[N];
			cf.obsts = new ArrayList<MyObstacle>();
			cf.v = new Point2D.Double(0, 0);
			cf.p = new Point2D.Double(initX, 490);
			Config next;
			double cur = 0;
			while (cur < END_REP_TIME && cf.hitN < N) {
				next = advance(cf, DELAY_HIT);
				cur += next.t;
				for (int i = 0; i < N; ++i) {
					next.visited[i] = cf.visited[i] || next.diff[i];
					next.diff[i] = false;
				}
				cf = next;
				Config bestCf = advance(cf, STEP);
				int bestVal = (int) (eval(bestCf.diff) * NOOBST_BONUS);
				MyObstacle add = null;
				for (double ad = 0; ad < 8; ad += DELAY_STEP) {
					Config config = advance(cf, ad);
					int left = (int) config.p.x;
					int bottom = (int) Math.ceil(config.p.y) - 1;
					if (left == 0 || left == 499 || bottom == 0) {
						continue;
					}

					ArrayList<MyObstacle> added = new ArrayList<MyObstacle>();
					if (config.v.x != 0) {
						double vx = config.v.x;
						double vy = config.v.y;
						double x0 = config.p.x;
						double y0 = config.p.y;
						int y1 = (int) Math
								.floor(-5 / vx / vx * left * left + (vy / vx + 10 * x0 / vx / vx) * left + y0 - vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						int y2 = (int) Math.floor(-5 / vx / vx * (left + 1) * (left + 1) + (vy / vx + 10 * x0 / vx / vx) * (left + 1) + y0
								- vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						if (y1 == bottom) {
							added.add(new MyObstacle(left, y1, left, y1 + 1));
						}
						if (y2 == bottom) {
							added.add(new MyObstacle(left + 1, y2, left + 1, y2 + 1));
						}
					}
					added.add(new MyObstacle(left, bottom, left + 1, bottom));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom + 1));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 1));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 2));
					added.add(new MyObstacle(left, bottom + 2, left + 1, bottom));
					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 1, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 2, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 2, bottom));
					}

					int j = 0;// = bottom == 1 ? 0 : -1;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 3 + j));
					added.add(new MyObstacle(left, bottom + 3 + j, left + 1, bottom + j));
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 2, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 3, bottom));
					}

					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom + j, left + 1, bottom + 3 + j));
						added.add(new MyObstacle(left - 1, bottom + 3 + j, left + 1, bottom + j));
					} else {
						added.add(new MyObstacle(left, bottom + j, left + 2, bottom + 3 + j));
						added.add(new MyObstacle(left, bottom + 3 + j, left + 2, bottom + j));
					}
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 2));
						added.add(new MyObstacle(left - 2, bottom + 2, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 2));
						added.add(new MyObstacle(left, bottom + 2, left + 3, bottom));
					}

					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 4 + j));
					added.add(new MyObstacle(left, bottom + 4 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496) {
						added.add(new MyObstacle(left - 3, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 3, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 4, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 4, bottom));
					}

					//j = bottom == 1 ? 0 : bottom == 2 ? -1 : -2;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 5 + j));
					added.add(new MyObstacle(left, bottom + 5 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496 || left == 495) {
						added.add(new MyObstacle(left - 4, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 4, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 5, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 5, bottom));
					}

					for (MyObstacle ob : added) {
						cf.obsts.add(ob);
						Config result = advance(cf, STEP);
						if (result != null) {
							int val = eval(result.diff);
							if (val > bestVal || (val == bestVal && result.t < bestCf.t)) {
								bestVal = val;
								bestCf = result;
								add = ob;
							}
						}
						cf.obsts.remove(cf.obsts.size() - 1);
					}

				}

				if (add != null) {
					cf.obsts.add(add);
				}
				cf.p = bestCf.p;
				cf.v = bestCf.v;
				cf.hitN = bestCf.hitN;
				for (int i = 0; i < N; ++i) {
					cf.visited[i] = cf.visited[i] || bestCf.diff[i];
					cf.diff[i] = false;
				}
				cur += bestCf.t;
			}
			double score = calcScore(cf.obsts);
			if (score > bestScore) {
				bestScore = score;
				best.clear();
				best.addAll(cf.obsts);
				bestStep = STEP;
			}
		}

		for (double STEP = 10; STEP <= 80 && (System.nanoTime() - startTime) < TIMER * 0.9 * GIGA; STEP += STEP1) {
			Config cf = new Config();
			cf.visited = new boolean[N];
			cf.diff = new boolean[N];
			cf.obsts = new ArrayList<MyObstacle>();
			cf.v = new Point2D.Double(0, 0);
			cf.p = new Point2D.Double(initX, 490);
			Config next;
			double cur = 0;
			while (cur < END_REP_TIME && cf.hitN < N) {
				next = advance(cf, DELAY_HIT);
				cur += next.t;
				for (int i = 0; i < N; ++i) {
					next.visited[i] = cf.visited[i] || next.diff[i];
					next.diff[i] = false;
				}
				cf = next;
				Config bestCf = advance(cf, STEP);
				int bestVal = (int) (eval(bestCf.diff) * NOOBST_BONUS);
				MyObstacle add = null;
				for (double ad = 0; ad < 8; ad += DELAY_STEP) {
					Config config = advance(cf, ad);
					int left = (int) config.p.x;
					int bottom = (int) Math.ceil(config.p.y) - 1;
					if (left == 0 || left == 499 || bottom == 0) {
						continue;
					}

					ArrayList<MyObstacle> added = new ArrayList<MyObstacle>();
					if (config.v.x != 0) {
						double vx = config.v.x;
						double vy = config.v.y;
						double x0 = config.p.x;
						double y0 = config.p.y;
						int y1 = (int) Math
								.floor(-5 / vx / vx * left * left + (vy / vx + 10 * x0 / vx / vx) * left + y0 - vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						int y2 = (int) Math.floor(-5 / vx / vx * (left + 1) * (left + 1) + (vy / vx + 10 * x0 / vx / vx) * (left + 1) + y0
								- vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						if (y1 == bottom) {
							added.add(new MyObstacle(left, y1, left, y1 + 1));
						}
						if (y2 == bottom) {
							added.add(new MyObstacle(left + 1, y2, left + 1, y2 + 1));
						}
					}
					added.add(new MyObstacle(left, bottom, left + 1, bottom));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom + 1));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 1));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 2));
					added.add(new MyObstacle(left, bottom + 2, left + 1, bottom));
					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 1, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 2, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 2, bottom));
					}

					int j = 0;// = bottom == 1 ? 0 : -1;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 3 + j));
					added.add(new MyObstacle(left, bottom + 3 + j, left + 1, bottom + j));
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 2, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 3, bottom));
					}

					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom + j, left + 1, bottom + 3 + j));
						added.add(new MyObstacle(left - 1, bottom + 3 + j, left + 1, bottom + j));
					} else {
						added.add(new MyObstacle(left, bottom + j, left + 2, bottom + 3 + j));
						added.add(new MyObstacle(left, bottom + 3 + j, left + 2, bottom + j));
					}
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 2));
						added.add(new MyObstacle(left - 2, bottom + 2, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 2));
						added.add(new MyObstacle(left, bottom + 2, left + 3, bottom));
					}

					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 4 + j));
					added.add(new MyObstacle(left, bottom + 4 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496) {
						added.add(new MyObstacle(left - 3, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 3, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 4, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 4, bottom));
					}

					//j = bottom == 1 ? 0 : bottom == 2 ? -1 : -2;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 5 + j));
					added.add(new MyObstacle(left, bottom + 5 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496 || left == 495) {
						added.add(new MyObstacle(left - 4, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 4, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 5, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 5, bottom));
					}

					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 6 + j));
					added.add(new MyObstacle(left, bottom + 6 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496 || left == 495 || left == 494) {
						added.add(new MyObstacle(left - 5, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 5, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 6, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 6, bottom));
					}

					for (MyObstacle ob : added) {
						cf.obsts.add(ob);
						Config result = advance(cf, STEP);
						if (result != null) {
							int val = eval(result.diff);
							if (val > bestVal || (val == bestVal && result.t < bestCf.t)) {
								bestVal = val;
								bestCf = result;
								add = ob;
							}
						}
						cf.obsts.remove(cf.obsts.size() - 1);
					}

				}

				if (add != null) {
					cf.obsts.add(add);
				}
				cf.p = bestCf.p;
				cf.v = bestCf.v;
				cf.hitN = bestCf.hitN;
				for (int i = 0; i < N; ++i) {
					cf.visited[i] = cf.visited[i] || bestCf.diff[i];
					cf.diff[i] = false;
				}
				cur += bestCf.t;
			}
			double score = calcScore(cf.obsts);
			if (score > bestScore) {
				bestScore = score;
				best.clear();
				best.addAll(cf.obsts);
				bestStep = STEP;
			}
		}

		for (double STEP = 10; (System.nanoTime() - startTime) < TIMER * 0.9 * GIGA; STEP += STEP1) {
			Config cf = new Config();
			cf.visited = new boolean[N];
			cf.diff = new boolean[N];
			cf.obsts = new ArrayList<MyObstacle>();
			cf.v = new Point2D.Double(0, 0);
			cf.p = new Point2D.Double(initX, 490);
			Config next;
			double cur = 0;
			while (cur < END_REP_TIME && cf.hitN < N) {
				next = advance(cf, DELAY_HIT);
				cur += next.t;
				for (int i = 0; i < N; ++i) {
					next.visited[i] = cf.visited[i] || next.diff[i];
					next.diff[i] = false;
				}
				cf = next;
				Config bestCf = advance(cf, STEP);
				int bestVal = (int) (eval(bestCf.diff) * NOOBST_BONUS);
				MyObstacle add = null;
				for (double ad = 0; ad < 8; ad += DELAY_STEP) {
					Config config = advance(cf, ad);
					int left = (int) config.p.x;
					int bottom = (int) Math.ceil(config.p.y) - 1;
					if (left == 0 || left == 499 || bottom == 0) {
						continue;
					}

					ArrayList<MyObstacle> added = new ArrayList<MyObstacle>();
					if (config.v.x != 0) {
						double vx = config.v.x;
						double vy = config.v.y;
						double x0 = config.p.x;
						double y0 = config.p.y;
						int y1 = (int) Math
								.floor(-5 / vx / vx * left * left + (vy / vx + 10 * x0 / vx / vx) * left + y0 - vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						int y2 = (int) Math.floor(-5 / vx / vx * (left + 1) * (left + 1) + (vy / vx + 10 * x0 / vx / vx) * (left + 1) + y0
								- vy * x0 / vx - 5 * x0 * x0 / vx / vx);
						if (y1 == bottom) {
							added.add(new MyObstacle(left, y1, left, y1 + 1));
						}
						if (y2 == bottom) {
							added.add(new MyObstacle(left + 1, y2, left + 1, y2 + 1));
						}
					}
					added.add(new MyObstacle(left, bottom, left + 1, bottom));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom + 1));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 1));
					added.add(new MyObstacle(left, bottom + 1, left + 1, bottom));

					added.add(new MyObstacle(left, bottom, left + 1, bottom + 2));
					added.add(new MyObstacle(left, bottom + 2, left + 1, bottom));
					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 1, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 2, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 2, bottom));
					}

					int j = 0;// = bottom == 1 ? 0 : -1;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 3 + j));
					added.add(new MyObstacle(left, bottom + 3 + j, left + 1, bottom + j));
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 2, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 3, bottom));
					}

					if (left == 498) {
						added.add(new MyObstacle(left - 1, bottom + j, left + 1, bottom + 3 + j));
						added.add(new MyObstacle(left - 1, bottom + 3 + j, left + 1, bottom + j));
					} else {
						added.add(new MyObstacle(left, bottom + j, left + 2, bottom + 3 + j));
						added.add(new MyObstacle(left, bottom + 3 + j, left + 2, bottom + j));
					}
					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom, left + 1, bottom + 2));
						added.add(new MyObstacle(left - 2, bottom + 2, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 3, bottom + 2));
						added.add(new MyObstacle(left, bottom + 2, left + 3, bottom));
					}

					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 4 + j));
					added.add(new MyObstacle(left, bottom + 4 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496) {
						added.add(new MyObstacle(left - 3, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 3, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 4, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 4, bottom));
					}

					//j = bottom == 1 ? 0 : bottom == 2 ? -1 : -2;
					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 5 + j));
					added.add(new MyObstacle(left, bottom + 5 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496 || left == 495) {
						added.add(new MyObstacle(left - 4, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 4, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 5, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 5, bottom));
					}

					added.add(new MyObstacle(left, bottom + j, left + 1, bottom + 6 + j));
					added.add(new MyObstacle(left, bottom + 6 + j, left + 1, bottom + j));
					if (left == 498 || left == 497 || left == 496 || left == 495 || left == 494) {
						added.add(new MyObstacle(left - 5, bottom, left + 1, bottom + 1));
						added.add(new MyObstacle(left - 5, bottom + 1, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 6, bottom + 1));
						added.add(new MyObstacle(left, bottom + 1, left + 6, bottom));
					}

					if (left == 498 || left == 497) {
						added.add(new MyObstacle(left - 2, bottom + j, left + 1, bottom + 4 + j));
						added.add(new MyObstacle(left - 2, bottom + 4 + j, left + 1, bottom + j));
					} else {
						added.add(new MyObstacle(left, bottom + j, left + 3, bottom + 4 + j));
						added.add(new MyObstacle(left, bottom + 4 + j, left + 3, bottom + j));
					}
					if (left == 498 || left == 497 || left == 496) {
						added.add(new MyObstacle(left - 3, bottom, left + 1, bottom + 3));
						added.add(new MyObstacle(left - 3, bottom + 3, left + 1, bottom));
					} else {
						added.add(new MyObstacle(left, bottom, left + 4, bottom + 3));
						added.add(new MyObstacle(left, bottom + 3, left + 4, bottom));
					}

					for (MyObstacle ob : added) {
						cf.obsts.add(ob);
						Config result = advance(cf, STEP);
						if (result != null) {
							int val = eval(result.diff);
							if (val > bestVal || (val == bestVal && result.t < bestCf.t)) {
								bestVal = val;
								bestCf = result;
								add = ob;
							}
						}
						cf.obsts.remove(cf.obsts.size() - 1);
					}

				}

				if (add != null) {
					cf.obsts.add(add);
				}
				cf.p = bestCf.p;
				cf.v = bestCf.v;
				cf.hitN = bestCf.hitN;
				for (int i = 0; i < N; ++i) {
					cf.visited[i] = cf.visited[i] || bestCf.diff[i];
					cf.diff[i] = false;
				}
				cur += bestCf.t;
			}
			double score = calcScore(cf.obsts);
			if (score > bestScore) {
				bestScore = score;
				best.clear();
				best.addAll(cf.obsts);
				bestStep = STEP;
			}
		}

		ArrayList<MyObstacle> obs = new ArrayList<MyObstacle>();
		int count = 0;
		long worstRepTime = 0;
		long repEndTime = 0;
		do {
			long repStartTime = System.nanoTime();
			int x1 = Math.max(1, initX - (int) (Math.random() * 50) - 1);
			int y1 = (int) (Math.random() * 481) + 1;
			int x2 = Math.min(499, initX + (int) (Math.random() * 50));
			int y2 = Math.min(481, Math.max(1, y1 + (int) (Math.random() * 50) - 3));
			if (y1 == y2) {
				if (initX > 250) {
					++y2;
				} else {
					--y2;
				}
			}
			MyObstacle o = new MyObstacle(x1, y1, x2, y2);
			obs.add(o);
			double sc = calcScore(obs);
			if (sc > bestScore) {
				bestScore = sc;
				best.clear();
				best.addAll(obs);
			}
			obs.clear();
			++count;
			repEndTime = System.nanoTime();
			worstRepTime = Math.max(worstRepTime, repEndTime - repStartTime);
		} while ((repEndTime - startTime + worstRepTime) / GIGA < TIMER);

		String[] res = new String[best.size()];
		for (int i = 0; i < res.length; ++i) {
			res[i] = best.get(i).toString();
		}
		debug("worstRepTime:" + 1. * worstRepTime / GIGA);
		debug("count:" + count);
		debug("bestScore:" + bestScore);
		debug(Arrays.toString(res));
		return res;
	}

	/////////////////////////
	// debug
	/////////////////////////

	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(System.in);
			int N = sc.nextInt();
			int[] x = new int[N];
			int[] y = new int[N];
			for (int i = 0; i < N; i++) {
				x[i] = sc.nextInt();
			}
			for (int i = 0; i < N; i++) {
				y[i] = sc.nextInt();
			}
			int R = sc.nextInt();
			BounceOff obj = new BounceOff();
			String[] res = obj.placeObstacles(x, y, R);
			System.out.println(res.length);
			for (int i = 0; i < res.length; i++) {
				System.out.println(res[i]);
			}
			System.out.flush();
		} catch (Throwable e) {
			e.printStackTrace(System.err);
		}
	}

	static DecimalFormat dformat = new DecimalFormat("000.000");
	static DecimalFormat iformat = new DecimalFormat("00;-#0");

	static void debug(double[] a) {
		if (DEBUG) {
			for (int i = 0; i < a.length; ++i) {
				System.err.print(dformat.format(a[i]) + ", ");
			}
			System.err.println();
		}
	}

	static void debug(int[] a) {
		if (DEBUG) {
			for (int i = 0; i < a.length; ++i) {
				System.err.print(a[i] + ", ");
			}
			System.err.println();
		}
	}

	static void debug(String s) {
		if (DEBUG) {
			System.out.println(s);
		}
	}

	static void debug(Object... o) {
		if (DEBUG) {
			System.out.println(Arrays.deepToString(o));
		}
	}

	/////////////////////////
	// calc
	/////////////////////////

	double calcScore(ArrayList<MyObstacle> input) {
		if (input.size() > 100) {
			return 0.;
		}
		MyObstacle[] obst = new MyObstacle[input.size() + 3];
		for (int i = 0; i < input.size(); ++i) {
			obst[i] = input.get(i);
		}
		obst[obst.length - 3] = MyObstacle.BOTTOM_WALL;
		obst[obst.length - 2] = MyObstacle.LEFT_WALL;
		obst[obst.length - 1] = MyObstacle.RIGHT_WALL;
		for (int i = 1; i < obst.length - 3; i++)
			for (int j = 0; j < i; j++) {
				if (obst[i].intersect(obst[j])) {
					return 0.;
				}
			}

		//---------- the actual physics ------------------
		Point2D.Double b = new Point2D.Double(initX, 490);
		Point2D.Double ve = new Point2D.Double(0, 0); //ball's velocity
		Point2D.Double vn, vt; //normal and tangential components of velocity
		Point2D.Double hitp;
		double hitt;
		double time = 0;
		int hi;
		boolean[] visited = new boolean[N];
		int nhits = 0;
		int prev = -1;
		int bounces = 0;
		int MAXTIME = 500;
		while (time < MAXTIME && bounces < 5000) {
			hitp = null;
			hitt = MAXTIME - time;
			hi = -1;
			for (int i = 0; i < obst.length; i++) {
				double A = obst[i].p1.y - obst[i].p2.y;
				double B = obst[i].p2.x - obst[i].p1.x;
				double C = A * obst[i].p1.x + B * obst[i].p1.y;
				//segment is A x + B y = C
				double qa = -5 * B;
				double qb = A * ve.x + B * ve.y;
				double qc = A * b.x + B * b.y - C;
				//solve: qa t^2 + qb t + qc == 0
				double det = qb * qb - 4 * qa * qc;
				if (det < 0) { // no solution, doesn't get there
					if (i == prev) {
						//this can only happen when numerics break down
						//just bounced off this surface, should be able to hit line again
						time = MAXTIME;
						break;
					}
					continue;
				}
				double t1, t2, apogee;
				if (qa == 0) {//vertical line, solve qb t = -qc
					if (i == prev) continue;
					t1 = t2 = -qc / qb;
					apogee = 0;
				} else {
					double sq = Math.sqrt(det);
					t1 = (-qb - sq) / 2 / qa;
					t2 = (-qb + sq) / 2 / qa;
					if (t2 < t1) {
						double tmp = t2;
						t2 = t1;
						t1 = tmp;
					}
					apogee = -qb / 2 / qa;
				}

				if (t1 > 0 && i != prev && t1 < hitt) {
					double x1 = b.x + t1 * ve.x;
					double y1 = b.y + t1 * ve.y - 5 * t1 * t1;
					Point2D.Double h1 = new Point2D.Double(x1, y1);
					if (obst[i].dist(h1) < EPS) {
						hitp = h1;
						hitt = t1;
						hi = i;
					}
				}
				if (t2 > 0 && (i != prev || apogee > 0) && t2 < hitt) {
					double x2 = b.x + t2 * ve.x;
					double y2 = b.y + t2 * ve.y - 5 * t2 * t2;
					Point2D.Double h2 = new Point2D.Double(x2, y2);
					if (obst[i].dist(h2) < EPS) {
						hitp = h2;
						hitt = t2;
						hi = i;
					}
				}
			}
			if (time + hitt > MAXTIME) {
				hitp = null;
				hitt = MAXTIME - time;
			}
			double maxHit = 0;
			for (int i = 0; i < N; i++) {
				if (visited[i]) continue;
				if (b.x < tar[i].x - R && ve.x < 0) {
					continue;
				}
				if (b.x > tar[i].x + R && ve.x > 0) {
					continue;
				}
				if (b.y < tar[i].y - R && ve.y < 0) {
					continue;
				}
				double artime;
				if (ve.x == 0 || (tar[i].x - R <= b.x && b.x <= tar[i].x + R)) {
					if (b.y < tar[i].y - R && ve.y <= 0) {
						continue;
					}
					artime = 0;
				} else {
					if (b.x < tar[i].x) {
						artime = (tar[i].x - R - b.x) / ve.x;
					} else {
						artime = (tar[i].x + R - b.x) / ve.x;
					}
					if (artime > hitt) {
						continue;
					}
					final double vyt = ve.y - 10 * artime;
					final double byt = b.y + ve.y * artime - 5 * artime * artime;
					if (byt < tar[i].y - R && vyt <= 0) {
						continue;
					}
					final double dptime = artime + 2 * R / Math.abs(ve.x);
					final double vyt2 = ve.y - 10 * dptime;
					final double byt2 = b.y + ve.y * dptime - 5 * dptime * dptime;
					if (vyt <= 0 && vyt2 <= 0) {
						if (byt <= tar[i].y - R || byt2 >= tar[i].y + R) {
							continue;
						}
					} else if (vyt >= 0 && vyt2 >= 0) {
						if (byt >= tar[i].y + R || byt2 <= tar[i].y - R) {
							continue;
						}
					}
				}
				final double B = -10 * ve.y;
				final double C = ve.x * ve.x + ve.y * ve.y - 10 * (b.y - tar[i].y);
				final double D = 2 * ve.x * (b.x - tar[i].x) + 2 * ve.y * (b.y - tar[i].y);
				final double E = (b.x - tar[i].x) * (b.x - tar[i].x) + (b.y - tar[i].y) * (b.y - tar[i].y) - R * R;
				double t2;
				for (double tt = 0; tt <= artime + 1; tt++) {
					t2 = tt;
					double d = 0, dd = 0;
					for (int j = 0; j < 35; j++) {
						d = A * t2 * t2 * t2 * t2 + B * t2 * t2 * t2 + C * t2 * t2 + D * t2 + E;
						dd = 4 * A * t2 * t2 * t2 + 3 * B * t2 * t2 + 2 * C * t2 + D;
						t2 -= d / dd;
					}
					if (d > -EPS && d < EPS && t2 < hitt && t2 > 0) {
						maxHit = Math.max(maxHit, t2);
						visited[i] = true;
						++nhits;
						break;
					}
				}
			}
			if (nhits == N) {
				time += maxHit;
				break;
			}
			b.x += ve.x * hitt;
			b.y += ve.y * hitt - 5 * hitt * hitt;
			ve.y -= 10 * hitt;
			time += hitt;
			if (hitp != null) {
				bounces++;
				prev = hi;
				vt = MyG2D.mult(obst[hi].vect, MyG2D.dot(ve, obst[hi].vect) / MyG2D.dot(obst[hi].vect, obst[hi].vect));
				vn = MyG2D.substr(ve, vt);
				vn = MyG2D.mult(vn, -.99);
				ve = MyG2D.add(vn, vt);
			} else {
				prev = -1;
			}
			if (MyG2D.norm(ve) < 1e-6) {
				time = MAXTIME;
				break;
			}
		}
		if (bounces >= 5000) time = MAXTIME;

		return (nhits == N ? 2. : nhits * 1. / N) * Math.pow(0.995, time) * Math.pow(0.9, obst.length - 3);
	}

	Config advance(Config cf, double step) {
		MyObstacle[] obst = new MyObstacle[cf.obsts.size() + 3];
		for (int i = 0; i < cf.obsts.size(); ++i) {
			obst[i] = cf.obsts.get(i);
		}
		obst[obst.length - 3] = MyObstacle.BOTTOM_WALL;
		obst[obst.length - 2] = MyObstacle.LEFT_WALL;
		obst[obst.length - 1] = MyObstacle.RIGHT_WALL;
		for (int i = 0; i < obst.length - 4; i++)
			if (obst[i].intersect(obst[obst.length - 4])) return null;

		Point2D.Double b = new Point2D.Double(cf.p.x, cf.p.y);
		Point2D.Double ve = new Point2D.Double(cf.v.x, cf.v.y); //ball's velocity
		Point2D.Double vn, vt; //normal and tangential components of velocity
		Point2D.Double hitp;
		double hitt;
		double time = cf.t;
		int hi;
		boolean[] visited = new boolean[N];
		int nhits = 0;
		for (int i = 0; i < N; ++i) {
			if (cf.visited[i]) ++nhits;
		}
		int prev = -1;
		int bounces = 0;
		double MAXTIME = step;
		double lastHitt = 0;
		Point2D.Double lastHitp = new Point2D.Double(0, 0);
		Point2D.Double lastHitv = new Point2D.Double(0, 0);
		while (time < MAXTIME && bounces < 500) {
			hitp = null;
			hitt = MAXTIME - time;
			hi = -1;
			for (int i = 0; i < obst.length; i++) {
				double A = obst[i].p1.y - obst[i].p2.y;
				double B = obst[i].p2.x - obst[i].p1.x;
				double C = A * obst[i].p1.x + B * obst[i].p1.y;
				double qa = -5 * B;
				double qb = A * ve.x + B * ve.y;
				double qc = A * b.x + B * b.y - C;
				double det = qb * qb - 4 * qa * qc;
				if (det < 0) {
					if (i == prev) {
						time = MAXTIME;
						break;
					}
					continue;
				}
				double t1, t2, apogee;
				if (qa == 0) {
					if (i == prev) continue;
					t1 = t2 = -qc / qb;
					apogee = 0;
				} else {
					double sq = Math.sqrt(det);
					t1 = (-qb - sq) / 2 / qa;
					t2 = (-qb + sq) / 2 / qa;
					if (t2 < t1) {
						double tmp = t2;
						t2 = t1;
						t1 = tmp;
					}
					apogee = -qb / 2 / qa;
				}

				if (t1 > 0 && i != prev && t1 < hitt) {
					double x1 = b.x + t1 * ve.x;
					double y1 = b.y + t1 * ve.y - 5 * t1 * t1;
					Point2D.Double h1 = new Point2D.Double(x1, y1);
					if (obst[i].dist(h1) < EPS) {
						hitp = h1;
						hitt = t1;
						hi = i;
					}
				}
				if (t2 > 0 && (i != prev || apogee > 0) && t2 < hitt) {
					double x2 = b.x + t2 * ve.x;
					double y2 = b.y + t2 * ve.y - 5 * t2 * t2;
					Point2D.Double h2 = new Point2D.Double(x2, y2);
					if (obst[i].dist(h2) < EPS) {
						hitp = h2;
						hitt = t2;
						hi = i;
					}
				}
			}
			if (time + hitt > MAXTIME) {
				hitp = null;
				hitt = MAXTIME - time;
			}
			double maxHit = 0;
			for (int i = 0; i < N; i++) {
				if (cf.visited[i] || visited[i]) continue;
				if (b.x < tar[i].x - R && ve.x <= 0) {
					continue;
				}
				if (b.x > tar[i].x + R && ve.x >= 0) {
					continue;
				}
				if (b.y < tar[i].y - R && ve.y <= 0) {
					continue;
				}
				final double artime;
				if (ve.x == 0 || (tar[i].x - R <= b.x && b.x <= tar[i].x + R)) {
					if (b.y < tar[i].y - R && ve.y <= 0) {
						continue;
					}
					artime = 0;
				} else {
					if (b.x < tar[i].x) {
						artime = (tar[i].x - R - b.x) / ve.x;
					} else {
						artime = (tar[i].x + R - b.x) / ve.x;
					}
					if (artime > hitt) {
						continue;
					}
					final double vyt = ve.y - 10 * artime;
					final double byt = b.y + ve.y * artime - 5 * artime * artime;
					if (byt < tar[i].y - R && vyt <= 0) {
						continue;
					}
					final double dptime = artime + 2 * R / Math.abs(ve.x);
					final double vyt2 = ve.y - 10 * dptime;
					final double byt2 = b.y + ve.y * dptime - 5 * dptime * dptime;
					if (vyt <= 0 && vyt2 <= 0) {
						if (byt <= tar[i].y - R || byt2 >= tar[i].y + R) {
							continue;
						}
					} else if (vyt >= 0 && vyt2 >= 0) {
						if (byt >= tar[i].y + R || byt2 <= tar[i].y - R) {
							continue;
						}
					}

				}
				final double B = -10 * ve.y;
				final double C = ve.x * ve.x + ve.y * ve.y - 10 * (b.y - tar[i].y);
				final double D = 2 * ve.x * (b.x - tar[i].x) + 2 * ve.y * (b.y - tar[i].y);
				final double E = (b.x - tar[i].x) * (b.x - tar[i].x) + (b.y - tar[i].y) * (b.y - tar[i].y) - R * R;
				double t2;
				for (double tt = 0; tt <= artime + 1; tt++) {
					t2 = tt;
					double d = 0, dd = 0;
					for (int j = 0; j < 25; j++) {
						d = A * t2 * t2 * t2 * t2 + B * t2 * t2 * t2 + C * t2 * t2 + D * t2 + E;
						dd = 4 * A * t2 * t2 * t2 + 3 * B * t2 * t2 + 2 * C * t2 + D;
						t2 -= d / dd;
					}
					if (d > -EPS && d < EPS && t2 < hitt && t2 > 0) {
						maxHit = Math.max(maxHit, t2);
						visited[i] = true;
						++nhits;
						break;
					}
				}
			}
			if (nhits == N) {
				time += maxHit;
				break;
			}
			if (maxHit > 0) {
				lastHitp.x = b.x + ve.x * maxHit;
				lastHitp.y = b.y + ve.y * maxHit - 5 * maxHit * maxHit;
				lastHitv.x = ve.x;
				lastHitv.y = ve.y - 10 * maxHit;
				lastHitt = time + maxHit;
			}
			b.x += ve.x * hitt;
			b.y += ve.y * hitt - 5 * hitt * hitt;
			ve.y -= 10 * hitt;
			time += hitt;
			if (hitp != null) {
				bounces++;
				prev = hi;
				vt = MyG2D.mult(obst[hi].vect, MyG2D.dot(ve, obst[hi].vect) / MyG2D.dot(obst[hi].vect, obst[hi].vect));
				vn = MyG2D.substr(ve, vt);
				vn = MyG2D.mult(vn, -.99);
				ve = MyG2D.add(vn, vt);
			} else {
				prev = -1;
			}
			if (MyG2D.norm(ve) < 1e-6) {
				time = MAXTIME;
				break;
			}
		}
		if (bounces >= 500) {
			time = MAXTIME;
		}
		Config ret = new Config();
		if (lastHitt == 0) {
			ret.p = b;
			ret.v = ve;
			ret.t += time;
		} else {
			ret.p = lastHitp;
			ret.v = lastHitv;
			ret.t += lastHitt;
		}
		ret.diff = visited;
		ret.obsts = cf.obsts;
		ret.visited = cf.visited;
		ret.hitN = nhits;
		return ret;
	}

	int eval(boolean[] visited) {
		int ret = 0;
		for (int i = 0; i < N; ++i) {
			if (visited[i]) {
				ret += value[i];
			}
		}
		return ret;
	}

}

class Config {
	boolean[] visited;
	boolean[] diff;
	ArrayList<MyObstacle> obsts;
	Point2D.Double v;
	Point2D.Double p;
	double t;
	int hitN;
}

class MyG2D {
	public static Point2D.Double mult(Point2D.Double p, double w) {
		return new Point2D.Double(p.getX() * w, p.getY() * w);
	}

	public static Point2D.Double add(Point2D.Double p1, Point2D.Double p2) {
		return new Point2D.Double(p1.getX() + p2.getX(), p1.getY() + p2.getY());
	}

	public static Point2D.Double substr(Point2D.Double p1, Point2D.Double p2) {
		return new Point2D.Double(p1.getX() - p2.getX(), p1.getY() - p2.getY());
	}

	public static double norm(Point2D.Double p) {
		return Math.sqrt(p.getX() * p.getX() + p.getY() * p.getY());
	}

	public static double dot(Point2D.Double p1, Point2D.Double p2) {
		return p1.getX() * p2.getX() + p1.getY() * p2.getY();
	}
}

class MyObstacle {
	static final MyObstacle RIGHT_WALL = new MyObstacle(500, 500, 500, 0);;
	static final MyObstacle LEFT_WALL = new MyObstacle(0, 0, 0, 500);
	static final MyObstacle BOTTOM_WALL = new MyObstacle(0, 0, 500, 0);

	public Point2D.Double p1, p2, vect;
	public double norm;

	public MyObstacle(int x1, int y1, int x2, int y2) {
		p1 = new Point2D.Double(x1, y1);
		p2 = new Point2D.Double(x2, y2);
		vect = MyG2D.substr(p2, p1);
		norm = MyG2D.norm(vect);
	}

	public boolean intersect(MyObstacle other) {
		if (Math.min(p1.getX(), p2.getX()) > Math.max(other.p1.getX(), other.p2.getX())) return false;
		if (Math.max(p1.getX(), p2.getX()) < Math.min(other.p1.getX(), other.p2.getX())) return false;
		if (Math.min(p1.getY(), p2.getY()) > Math.max(other.p1.getY(), other.p2.getY())) return false;
		if (Math.max(p1.getY(), p2.getY()) < Math.min(other.p1.getY(), other.p2.getY())) return false;

		double den = other.vect.getY() * vect.getX() - other.vect.getX() * vect.getY();
		double num1 = other.vect.getX() * (p1.getY() - other.p1.getY()) - other.vect.getY() * (p1.getX() - other.p1.getX());
		double num2 = vect.getX() * (p1.getY() - other.p1.getY()) - vect.getY() * (p1.getX() - other.p1.getX());

		if (den == 0) {
			return Math.min(other.dist2(this), dist2(other)) == 0;
		}
		double u1 = num1 / den;
		double u2 = num2 / den;
		if (u1 < 0 || u1 > 1 || u2 < 0 || u2 > 1) return false;
		return true;
	}

	public double dist(Point2D.Double p) {
		if (MyG2D.dot(vect, MyG2D.substr(p, p1)) <= 0) return MyG2D.norm(MyG2D.substr(p, p1)); //from p to p1
		if (MyG2D.dot(vect, MyG2D.substr(p, p2)) >= 0) return MyG2D.norm(MyG2D.substr(p, p2)); //from p to p2
		return Math.abs(-vect.getY() * p.getX() + vect.getX() * p.getY() + p1.getX() * p2.getY() - p1.getY() * p2.getX()) / norm;
	}

	public double dist2(MyObstacle other) {
		return Math.min(dist(other.p1), dist(other.p2));
	}

	public String toString() {
		return (int) p1.x + " " + (int) p1.y + " " + (int) p2.x + " " + (int) p2.y;
	}

}