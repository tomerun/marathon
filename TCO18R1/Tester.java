import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;

class Pnt {
  int x, y;

  Pnt(int x1, int y1) {
    x = x1;
    y = y1;
  }

  int dist2(Pnt other) {
    return (x - other.x) * (x - other.x) + (y - other.y) * (y - other.y);
  }

  public String toString() {
    return "(" + x + "," + y + ")";
  }
}

public class Tester {
  private static final int minS = 100, maxS = 1000;
  private static final int minNC = 10, maxNC = 100;
  private static final double minJC = 0, maxJC = 10;
  private static final double minProb = 0, maxProb = 0.4;

  private int S;                  // size of the area
  private double jCost;           // cost of building a junction
  private double jFailProb;       // probability of a junction construction failing
  private int NC;                 // number of cities
  private Pnt[] cities;           // coordinates of cities
  private int NJ;                 // number of junctions
  private Pnt[] junct;            // coordinates of newly built junctions
  private HashSet<Integer> cUsed; // coordinates of cities and junctions as a set
  private int[] junctOk;          // indicates whether the junction has been built successfully and can be used
  private Pnt[] vertices;         // cities and junctions stored together
  private SecureRandom rnd;
  private int NR;                 // number of roads built
  private int[] roadS, roadE;     // start and end cities/junctions of newly built roads
  private double evaluation;

  private boolean isInside(int x, int y) {
    return (x >= 0 && x <= S && y >= 0 && y <= S);
  }

  private int cityToInt(Pnt p) {
    return p.x * (S + 1) + p.y;
  }

  private int roadToInt(int start, int end) {
    return start * (NC + NJ) + end;
  }

  private void generate(long seed) throws Exception {
    rnd = SecureRandom.getInstance("SHA1PRNG");
    rnd.setSeed(seed);
    S = rnd.nextInt(maxS - minS + 1) + minS;
    NC = rnd.nextInt(maxNC - minNC + 1) + minNC;
    jCost = rnd.nextDouble() * (maxJC - minJC) + minJC;
    jFailProb = rnd.nextDouble() * (maxProb - minProb) + minProb;
    if (1000 <= seed && seed <= 1999) {
      jFailProb = minProb + (maxProb - minProb)
          * (seed - 1000 + 1) / 1001;
    }

    cities = new Pnt[NC];
    cUsed = new HashSet<>();
    for (int i = 0; i < NC; ++i) {
      while (true) {
        cities[i] = new Pnt(rnd.nextInt(S + 1), rnd.nextInt(S + 1));
        Integer point = cityToInt(cities[i]);
        if (cUsed.contains(point)) continue;
        cUsed.add(point);
        break;
      }
    }
  }

  private Result runTest(long seed) throws Exception {
    Result res = new Result();
    long beforeTime = 0;
    try {
      generate(seed);
      res.seed = seed;
      res.S = S;
      res.NC = NC;
      res.cost = jCost;
      res.P = jFailProb;

      int[] citiesArg = new int[2 * NC];
      for (int i = 0; i < NC; ++i) {
        citiesArg[2 * i] = cities[i].x;
        citiesArg[2 * i + 1] = cities[i].y;
      }

      beforeTime = System.currentTimeMillis();
      int[] junctionsRet = buildJunctions(S, citiesArg, jCost, jFailProb);
      res.elapsed += System.currentTimeMillis() - beforeTime;
      if (junctionsRet.length % 2 == 1) {
        throw new RuntimeException("Your return from buildJunctions contained odd number of elements.");
      }
      res.NJ = NJ = junctionsRet.length / 2;
      if (NJ > 2 * NC) {
        throw new RuntimeException("You can build at most " + 2 * NC + " new junctions.");
      }
      junct = new Pnt[NJ];
      for (int i = 0; i < NJ; ++i) {
        if (!isInside(junctionsRet[2 * i], junctionsRet[2 * i + 1])) {
          throw new RuntimeException("You can only build junctions inside the area.");
        }
        junct[i] = new Pnt(junctionsRet[2 * i], junctionsRet[2 * i + 1]);
        Integer point = cityToInt(junct[i]);
        if (cUsed.contains(point)) {
          throw new RuntimeException("You can only build junctions on places not occupied by cities or other junctions.");
        }
        cUsed.add(point);
      }
      junctOk = new int[NJ];
      for (int i = 0; i < NJ; ++i) {
        junctOk[i] = (rnd.nextDouble() >= jFailProb) ? 1 : 0;
      }

      beforeTime = System.currentTimeMillis();
      int[] roadsRet = buildRoads(junctOk);
      res.elapsed += System.currentTimeMillis() - beforeTime;
      if (roadsRet.length % 2 == 1) {
        throw new RuntimeException("Your return from buildRoads contained odd number of elements.");
      }
      NR = roadsRet.length / 2;
      int maxR = (NC + NJ) * (NC + NJ - 1) / 2;
      if (NR > maxR) {
        throw new RuntimeException("You can build at most " + maxR + " roads.");
      }

      HashSet<Integer> rUsed = new HashSet<>();
      roadS = new int[NR];
      roadE = new int[NR];
      for (int i = 0; i < NR; ++i) {
        if (roadsRet[2 * i] < 0 || roadsRet[2 * i] >= NC + NJ ||
            roadsRet[2 * i + 1] < 0 || roadsRet[2 * i + 1] >= NC + NJ) {
          throw new RuntimeException("You can only build roads between pairs of existing cities/junctions.");
        }
        if (roadsRet[2 * i] == roadsRet[2 * i + 1]) {
          throw new RuntimeException("You can only build roads between distinct points.");
        }
        if (roadsRet[2 * i] >= NC && junctOk[roadsRet[2 * i] - NC] == 0 ||
            roadsRet[2 * i + 1] >= NC && junctOk[roadsRet[2 * i + 1] - NC] == 0) {
          throw new RuntimeException("You can not build a road to a dysfunctional junction.");
        }
        roadS[i] = Math.min(roadsRet[2 * i], roadsRet[2 * i + 1]);
        roadE[i] = Math.max(roadsRet[2 * i], roadsRet[2 * i + 1]);
        Integer road = roadToInt(roadS[i], roadE[i]);
        if (rUsed.contains(road)) {
          throw new RuntimeException("You can only build one road between each pair of points.");
        }
        rUsed.add(road);
      }

      vertices = new Pnt[NC + NJ];
      System.arraycopy(cities, 0, vertices, 0, NC);
      System.arraycopy(junct, 0, vertices, NC, NJ);

      if (vis) draw();

      ArrayList<ArrayList<Integer>> adj = new ArrayList<>(NC + NJ);
      for (int i = 0; i < NC + NJ; ++i)
        adj.add(new ArrayList<>());
      for (int i = 0; i < NR; ++i) {
        adj.get(roadS[i]).add(roadE[i]);
        adj.get(roadE[i]).add(roadS[i]);
      }
      boolean[] used = new boolean[NC + NJ];
      ArrayList<Integer> next = new ArrayList<>();
      next.add(0);
      while (!next.isEmpty()) {
        int vertexInd = next.get(0);
        next.remove(0);
        for (int i = 0; i < adj.get(vertexInd).size(); ++i) {
          int adjInd = adj.get(vertexInd).get(i);
          if (!used[adjInd]) {
            used[adjInd] = true;
            next.add(adjInd);
          }
        }
      }
      for (int i = 0; i < NC + NJ; ++i)
        if (!used[i] && (i < NC || i >= NC && junctOk[i - NC] == 1)) {
          throw new RuntimeException((i < NC ? ("City " + i) : ("Junction " + (i - NC))) + " not connected to the network.");
        }

      for (int i = 0; i < NR; ++i) {
        res.dist += Math.sqrt(vertices[roadS[i]].dist2(vertices[roadE[i]]));
      }
      res.score = NJ == 0 ? res.dist : calcScoreExpectation() + NJ * jCost;
      res.evaluation = evaluation;
      return res;
    } finally {
      if (res.elapsed == 0) {
        res.elapsed = System.currentTimeMillis() - beforeTime;
      }
      proc.destroy();
    }
  }

  private double calcScoreExpectation() {
    double[][] distGraph = new double[NC + NJ][NC + NJ];
    for (int i = 0; i < NC + NJ; i++) {
      for (int j = 0; j < i; j++) {
        distGraph[i][j] = distGraph[j][i] = Math.sqrt(vertices[i].dist2(vertices[j]));
      }
    }
    boolean[] constructOk = new boolean[NJ];
    double score = 0;
    if (NJ < 14) {
      for (int i = 0; i < (1 << NJ); i++) {
        double p = 1.0;
        for (int j = 0; j < NJ; j++) {
          constructOk[j] = (i & (1 << j)) != 0;
          p *= constructOk[j] ? (1 - jFailProb) : jFailProb;
        }
        score += p * mst(constructOk, distGraph);
      }
    } else {
      final int REP = 10000;
      for (int i = 0; i < REP; ++i) {
        for (int j = 0; j < NJ; j++) {
          constructOk[j] = rnd.nextDouble() >= jFailProb;
        }
        score += mst(constructOk, distGraph);
      }
      score /= REP;
    }

    return score;
  }

  private double mst(boolean[] constructOk, double[][] distGraph) {
    double ret = 0;
    boolean[] used = new boolean[NC + NJ];
    double[] min = new double[NC + NJ];
    for (int i = 0; i < NJ; i++) {
      if (!constructOk[i]) used[NC + i] = true;
    }
    for (int i = 1; i < NC + NJ; ++i) {
      min[i] = distGraph[0][i];
    }
    while (true) {
      int n = 0;
      double min_d = Double.MAX_VALUE;
      for (int j = 1; j < NC + NJ; ++j) {
        if (used[j]) continue;
        if (min[j] < min_d) {
          min_d = min[j];
          n = j;
        }
      }
      if (min_d == Double.MAX_VALUE) break;
      used[n] = true;
      ret += min_d;
      for (int j = 1; j < NC + NJ; ++j) {
        if (used[j]) continue;
        double d = distGraph[n][j];
        if (d < min[j]) {
          min[j] = d;
        }
      }
    }
    return ret;
  }

  private static String fileName;
  private static boolean vis;
  private Process proc;
  private OutputStream os;
  private BufferedReader br;

  private int[] buildJunctions(int S, int[] cities, double junctionCost, double failProb) throws IOException {
    StringBuilder sb = new StringBuilder();
    sb.append(S).append("\n");
    sb.append(cities.length).append("\n");
    for (int i = 0; i < cities.length; ++i) {
      sb.append(cities[i]).append("\n");
    }
    sb.append(junctionCost).append("\n");
    sb.append(failProb).append("\n");
    os.write(sb.toString().getBytes());
    os.flush();
    int N = Integer.parseInt(br.readLine());
    int[] ret = new int[N];
    for (int i = 0; i < N; i++)
      ret[i] = Integer.parseInt(br.readLine());
    return ret;
  }

  private int[] buildRoads(int[] junctionBuilt) throws IOException {
    StringBuilder sb = new StringBuilder();
    sb.append(junctionBuilt.length).append("\n");
    for (int i = 0; i < junctionBuilt.length; ++i) {
      sb.append(junctionBuilt[i]).append("\n");
    }
    os.write(sb.toString().getBytes());
    os.flush();
    int N = Integer.parseInt(br.readLine());
    int[] ret = new int[N];
    for (int i = 0; i < N; i++)
      ret[i] = Integer.parseInt(br.readLine());
    evaluation = Double.parseDouble(br.readLine());
    return ret;
  }

  private void draw() throws Exception {
    int SZX = S * 2 + 10;
    int SZY = S + 1;
    BufferedImage bi = new BufferedImage(SZX, SZY, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) bi.getGraphics();
    g2.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
    g2.setColor(Color.WHITE);
    g2.fillRect(0, 0, SZX, SZY);
    g2.setColor(Color.BLACK);
    for (int i = 0; i < NR; ++i) {
      g2.drawLine(vertices[roadS[i]].x, vertices[roadS[i]].y, vertices[roadE[i]].x, vertices[roadE[i]].y);
    }
    g2.setColor(Color.BLACK);
    for (int i = 0; i < NC; ++i) {
      g2.fillOval(cities[i].x - 3, cities[i].y - 3, 7, 7);
    }
    for (int i = 0; i < NJ; ++i) {
      g2.setColor((junctOk[i] == 0 ? Color.RED : Color.GREEN));
      g2.fillOval(junct[i].x - 2, junct[i].y - 2, 5, 5);
    }

    g2.translate(S + 5, 0);
    g2.setColor(Color.BLACK);
    int[] start = new int[NC - 1];
    int[] end = new int[NC - 1];
    boolean[] used = new boolean[NC];
    int[] min = new int[NC];
    int[] from = new int[NC];
    for (int i = 1; i < NC; ++i) {
      min[i] = vertices[0].dist2(vertices[i]);
      from[i] = 0;
    }
    for (int i = 0; i < NC - 1; ++i) {
      int n = 0;
      int min_d = Integer.MAX_VALUE;
      for (int j = 1; j < NC; ++j) {
        if (used[j]) continue;
        if (min[j] < min_d) {
          min_d = min[j];
          n = j;
        }
      }
      start[i] = from[n];
      end[i] = n;
      used[n] = true;
      for (int j = 1; j < NC; ++j) {
        if (used[j]) continue;
        int d = vertices[n].dist2(vertices[j]);
        if (d < min[j]) {
          min[j] = d;
          from[j] = n;
        }
      }
    }
    for (int i = 0; i < NC - 1; ++i) {
      g2.drawLine(vertices[start[i]].x, vertices[start[i]].y, vertices[end[i]].x, vertices[end[i]].y);
    }
    g2.setColor(Color.BLACK);
    for (int i = 0; i < NC; ++i) {
      g2.fillOval(cities[i].x - 3, cities[i].y - 3, 7, 7);
    }
    ImageIO.write(bi, "png", new File(fileName + ".png"));
  }

  private Tester() throws Exception {
    Runtime rt = Runtime.getRuntime();
    proc = rt.exec("./tester");
    os = proc.getOutputStream();
    InputStream is = proc.getInputStream();
    br = new BufferedReader(new InputStreamReader(is));
    new ErrorReader(proc.getErrorStream()).start();
  }

  private static final int THREAD_COUNT = 15;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-vis")) vis = true;
    }
    if (vis) fileName = seed + "";
    if (begin != -1 && end != -1) {
      vis = false;
      BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
      for (long i = begin; i <= end; ++i) {
        q.add(i);
      }
      Result[] results = new Result[(int) (end - begin + 1)];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i] = new TestThread(q, results, begin);
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / (end - begin + 1)));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    long seedMin;
    Result[] results;
    BlockingQueue<Long> q;

    TestThread(BlockingQueue<Long> q, Result[] results, long seedMin) {
      this.q = q;
      this.results = results;
      this.seedMin = seedMin;
    }

    public void run() {
      while (true) {
        Long seed = q.poll();
        if (seed == null) break;
        try {
          Tester f = new Tester();
          Result res = f.runTest(seed);
          results[(int) (seed - seedMin)] = res;
        } catch (Exception e) {
          e.printStackTrace();
          Result res = new Result();
          res.seed = seed;
          res.dist = 999999;
          results[(int) (seed - seedMin)] = res;
        }
      }
    }
  }
}

class Result {
  long seed;
  int S, NC, NJ;
  double cost, P;
  double dist, score, evaluation;
  long elapsed;

  public String toString() {
    String ret = String.format("seed:%4d\n", seed);
    ret += String.format("S:%4d NC:%3d cost:%.3f P:%.3f\n", S, NC, cost, P);
    ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
    ret += String.format("NJ:%3d dist:%10.4f eval:%10.4f\n", NJ, dist, evaluation);
    ret += String.format("score:%10.4f", score);
    return ret;
  }
}


class ErrorReader extends Thread {
  private InputStream error;

  ErrorReader(InputStream is) {
    error = is;
  }

  public void run() {
    try {
      byte[] ch = new byte[50000];
      int read;
      while ((read = error.read(ch)) > 0) {
        String s = new String(ch, 0, read);
        System.err.print(s);
        System.err.flush();
      }
    } catch (Exception e) {
      // do nothing
    }
  }
}
