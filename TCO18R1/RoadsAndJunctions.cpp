#include <algorithm>
#include <utility>
#include <vector>
#include <unordered_set>
#include <bitset>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
// #define DEBUG
#define NDEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;
using pi=pair<int, int>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 7500;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9800;
#endif

ll start_time; // msec

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
	ull ret;
	__asm__ volatile ("rdtsc" : "=A" (ret));
	return ret;
#else
	ull high,low;
	__asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
	return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

struct Point {
	int x, y, idx;

	bool operator<(const Point& o) const {
		if (x != o.x) return x < o.x;
		return y < o.y;
	}
};

int ccw(const Point& p1, const Point& p2, const Point& p3) {
	int dx1 = p2.x - p1.x;
	int dy1 = p2.y - p1.y;
	int dx2 = p3.x - p2.x;
	int dy2 = p3.y - p2.y;
	return dx1 * dy2 - dx2 * dy1;
}

int dist2(const Point& p1, const Point& p2) {
	return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}

double dist(int x1, int y1, int x2, int y2) {
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

const uint8_t EMPTY = 0xFF;
const uint8_t CANDIDATE = 0xFE;
const int INF = 1 << 29;
const double INITIAL_COOLER = 0.39;
const int COOLER_UPDATE_COUNT = 1626;
const double COOLER_UPDATE_RATIO = 1.096;
const int INITIAL_MOVE_RANGE = 4;
const int FINAL_MOVE_RANGE = 3;
int MAX_EVALUATE_FAILUER_SIZE = 3;
const int MST_DIFF_MASK = (1 << 2) - 1;
const int TYPE_MASK = (1 << 4) - 1;
int S, NC, NJ;
double C, P;
int miny, maxy, minx, maxx;
array<Point, 200> points;
vector<vector<uint8_t>> pos_vertex;
array<array<int, 30>, 200> graph, cur_graph;
array<int, 200> graph_size, cur_graph_size;
array<array<double, 200>, 200> graph_dist;
array<double, 200> graph_dist_buf;
vector<bool> eval_used(200);
array<double, 200> eval_min_dist;
array<int, 200> eval_min_from;
array<int, 200> gen_buffer;
double sa_cooler;
int move_range;
double best_score;

inline void move_inside(int& p) {
	if (p < 0) p = 0;
	if (p > S) p = S;
}

inline bool accept(double prev_score, double cur_score) {
	if (cur_score <= prev_score) return true;
	return rnd.nextDouble() < exp((prev_score - cur_score) * sa_cooler);
}

double mst() {
	START_TIMER(3);
	fill(graph_size.begin(), graph_size.begin() + NC + NJ, 0);
	int n = 0;
	double min_d = INF;
	for (int i = 1; i < NC + NJ; ++i) {
		eval_min_dist[i] = graph_dist[0][i];
		eval_min_from[i] = 0;
		if (eval_min_dist[i] < min_d) {
			min_d = eval_min_dist[i];
			n = i;
		}
	}
	double sum = 0;
	if (NC + NJ > 193) {
		eval_used.assign(NC + NJ, false);
		for (int i = 0; i < NC + NJ - 1; ++i) {
			eval_used[n] = true;
			int from = eval_min_from[n];
			graph[from][graph_size[from]++] = n;
			graph[n][graph_size[n]++] = from;
			sum += min_d;
			min_d = INF;
			int nn = 0;
			for (int j = 1; j < NC + NJ; ++j) {
				if (eval_used[j]) continue;
				double d = graph_dist[n][j];
				if (d < eval_min_dist[j]) {
					eval_min_dist[j] = d;
					eval_min_from[j] = n;
				}
				if (eval_min_dist[j] < min_d) {
					min_d = eval_min_dist[j];
					nn = j;
				}
			}
			n = nn;
		}
	} else if(NC + NJ > 129) {
		array<ull, 3> rest = {(ull)-1, (ull)-1, NC + NJ == 193 ? -1 : (1ull << (NC + NJ - 129)) - 1};
		rest[(n - 1) >> 6] ^= 1ull << ((n - 1) & 63);
		while (rest[0] || rest[1] || rest[2]) {
			int from = eval_min_from[n];
			graph[from][graph_size[from]++] = n;
			graph[n][graph_size[n]++] = from;
			sum += min_d;
			min_d = INF;
			int nn = 0;
			for (int i = 0; i < 3; ++i) {
				ull cur_rest = rest[i];
				while (cur_rest != 0) {
					int j = __builtin_ctzll(cur_rest) + 1 + 64 * i;
					double d = graph_dist[n][j];
					if (d < eval_min_dist[j]) {
						eval_min_dist[j] = d;
						eval_min_from[j] = n;
					}
					if (eval_min_dist[j] < min_d) {
						min_d = eval_min_dist[j];
						nn = j;
					}
					cur_rest &= cur_rest - 1;
				}
			}
			n = nn;
			rest[(n - 1) >> 6] ^= 1ull << ((n - 1) & 63);
		}
		int from = eval_min_from[n];
		graph[from][graph_size[from]++] = n;
		graph[n][graph_size[n]++] = from;
		sum += min_d;
	} else if(NC + NJ > 65) {
		array<ull, 2> rest = {(ull)-1, NC + NJ == 129 ? -1 : (1ull << (NC + NJ - 65)) - 1};
		rest[(n - 1) >> 6] ^= 1ull << ((n - 1) & 63);
		while (rest[0] || rest[1]) {
			int from = eval_min_from[n];
			graph[from][graph_size[from]++] = n;
			graph[n][graph_size[n]++] = from;
			sum += min_d;
			min_d = INF;
			int nn = 0;
			ull cur_rest = rest[0];
			while (cur_rest != 0) {
				int j = __builtin_ctzll(cur_rest) + 1;
				double d = graph_dist[n][j];
				if (d < eval_min_dist[j]) {
					eval_min_dist[j] = d;
					eval_min_from[j] = n;
				}
				if (eval_min_dist[j] < min_d) {
					min_d = eval_min_dist[j];
					nn = j;
				}
				cur_rest &= cur_rest - 1;
			}
			cur_rest = rest[1];
			while (cur_rest != 0) {
				int j = __builtin_ctzll(cur_rest) + 65;
				double d = graph_dist[n][j];
				if (d < eval_min_dist[j]) {
					eval_min_dist[j] = d;
					eval_min_from[j] = n;
				}
				if (eval_min_dist[j] < min_d) {
					min_d = eval_min_dist[j];
					nn = j;
				}
				cur_rest &= cur_rest - 1;
			}
			n = nn;
			rest[(n - 1) >> 6] ^= 1ull << ((n - 1) & 63);
		}
		int from = eval_min_from[n];
		graph[from][graph_size[from]++] = n;
		graph[n][graph_size[n]++] = from;
		sum += min_d;
	} else {
		ull rest = NC + NJ == 65 ? -1 : (1ull << (NC + NJ - 1)) - 1;
		rest ^= 1ull << (n - 1);
		while (rest) {
			int from = eval_min_from[n];
			graph[from][graph_size[from]++] = n;
			graph[n][graph_size[n]++] = from;
			sum += min_d;
			min_d = INF;
			int nn = 0;
			ull cur_rest = rest;
			while (cur_rest) {
				int j = __builtin_ctzll(cur_rest) + 1;
				double d = graph_dist[n][j];
				if (d < eval_min_dist[j]) {
					eval_min_dist[j] = d;
					eval_min_from[j] = n;
				}
				if (eval_min_dist[j] < min_d) {
					min_d = eval_min_dist[j];
					nn = j;
				}
				cur_rest &= cur_rest - 1;
			}
			n = nn;
			rest ^= 1ull << (n - 1);
		}
		int from = eval_min_from[n];
		graph[from][graph_size[from]++] = n;
		graph[n][graph_size[n]++] = from;
		sum += min_d;
	}
	STOP_TIMER(3);
	return sum;
}

double mst_diff(int base) {
	START_TIMER(6);
	double ret = 0;
	int bi = 1;
	gen_buffer[0] = base;
	for (int i = 0; i < NC + NJ; ++i) {
		if (i == base) continue;
		graph_size[i] = 0;
		for (int j = 0; j < cur_graph_size[i]; ++j) {
			if (cur_graph[i][j] != base) {
				graph[i][graph_size[i]++] = cur_graph[i][j];
				ret += graph_dist[i][cur_graph[i][j]];
			} else {
				gen_buffer[bi++] = i;
			}
		}
	}
	ret /= 2;
	int n = 0;
	double min_d = INF;
	for (int i = 1; i < bi; ++i) {
		eval_min_dist[i] = graph_dist[gen_buffer[0]][gen_buffer[i]];
		eval_min_from[i] = 0;
		if (eval_min_dist[i] < min_d) {
			min_d = eval_min_dist[i];
			n = i;
		}
	}
	ull rest = (1ull << (bi - 1)) - 1;
	rest ^= 1ull << (n - 1);
	while (rest != 0) {
		ret += min_d;
		int from = gen_buffer[eval_min_from[n]];
		int to = gen_buffer[n];
		graph[from][graph_size[from]++] = to;
		graph[to][graph_size[to]++] = from;
		min_d = INF;
		int nn = 0;
		ull cur_rest = rest;
		while (cur_rest != 0) {
			int j = __builtin_ctzll(cur_rest) + 1;
			double d = graph_dist[gen_buffer[n]][gen_buffer[j]];
			if (d < eval_min_dist[j]) {
				eval_min_dist[j] = d;
				eval_min_from[j] = n;
			}
			if (eval_min_dist[j] < min_d) {
				min_d = eval_min_dist[j];
				nn = j;
			}
			cur_rest &= cur_rest - 1;
		}
		n = nn;
		rest ^= 1ull << (n - 1);
	}
	ret += min_d;
	int from = gen_buffer[eval_min_from[n]];
	int to = gen_buffer[n];
	graph[from][graph_size[from]++] = to;
	graph[to][graph_size[to]++] = from;
	STOP_TIMER(6);
	return ret;
}

double intercept_x(const Point& p1, const Point& p2, int y) {
	if (min(p1.y, p2.y) > y) return -1;
	if (max(p1.y, p2.y) < y) return -1;
	if (p1.y == p2.y) return -1;
	return p1.x + 1.0 * (p2.x - p1.x) / (p2.y - p1.y) * (y - p1.y);
}

const double cos60 = 0.5;
const double sin60 = sqrt(3) * 0.5;
vector<Point> fps;

void fermat_point(int v1, int v2, int v3, vi& candidate_points) {
	int dx1 = points[v2].x - points[v1].x;
	int dy1 = points[v2].y - points[v1].y;
	int dx2 = points[v3].x - points[v1].x;
	int dy2 = points[v3].y - points[v1].y;
	if (dx1 * dy2 - dx2 * dy1 < 0) {
		swap(v2, v3);
		swap(dx1, dx2);
		swap(dy1, dy2);
	}
	double x11 = points[v1].x + dx1 * cos60 + dy1 * sin60;
	double y11 = points[v1].y - dx1 * sin60 + dy1 * cos60;
	double x12 = points[v3].x;
	double y12 = points[v3].y;
	double x21 = points[v1].x + dx2 * cos60 - dy2 * sin60;
	double y21 = points[v1].y + dx2 * sin60 + dy2 * cos60;
	double x22 = points[v2].x;
	double y22 = points[v2].y;
	double div = (x12 - x11) * (y22 - y21) - (y12 - y11) * (x22 - x21);
	double p = ((y22 - y21) * (x21 - x11) - (x22 - x21) * (y21 - y11)) / div;
	double cx = x11 + p * (x12 - x11);
	double cy = y11 + p * (y12 - y11);
	// debug("(%d %d) (%d %d) (%d %d) -> (%f %f)\n",
	//  points[v1].x, points[v1].y, points[v2].x, points[v2].y, points[v3].x, points[v3].y, cx, cy);
	bool used = false;
	for (int mx = 0; mx <= 1; ++mx) {
		for (int my = 0; my <= 1; ++my) {
			if (pos_vertex[(int)cy][(int)cx] != CANDIDATE) continue;
			int px = (int)cx + mx;
			int py = (int)cy + my;
			double diff = C + dist(px, py, points[v1].x, points[v1].y)
			              + dist(px, py, points[v2].x, points[v2].y)
			              + dist(px, py, points[v3].x, points[v3].y)
			              - graph_dist[v1][v2] - graph_dist[v1][v3];
			if (!used && diff <= 0) {
				Point pnt = {px, py};
				fps.push_back(pnt);
				used = true;
			}
			int pv = (py << 16) | px;
			if (diff > 0) {
				candidate_points.push_back(pv);
			} else {
				for (int i = 0; i < 4; ++i) {
					candidate_points.push_back(pv);
				}
			}
		}
	}
}

vi collect_candidate() {
	vector<Point> convex;
	vector<Point> ps(points.begin(), points.begin() + NC);
	sort(ps.begin(), ps.end());
	for (int i = 0; i < NC; i++) {
		while (convex.size() > 1 && ccw(convex[convex.size() - 2], convex.back(), ps[i]) <= 0) {
			convex.pop_back();
		}
		convex.push_back(ps[i]);
	}
	const int lower_size = convex.size();
	for (int i = NC - 2; i >= 0; i--) {
		while (convex.size() > lower_size && ccw(convex[convex.size() - 2], convex.back(), ps[i]) <= 0) {
			convex.pop_back();
		}
		convex.push_back(ps[i]);
	}
	convex.push_back(convex[0]);
	for (int y = miny; y <= maxy; ++y) {
		double left = S;
		double right = 0;
		for (int i = 0; i < convex.size() - 1; ++i) {
			double x = intercept_x(convex[i], convex[i + 1], y);
			if (x != -1) {
				left = min(left, x);
				right = max(right, x);
			}
		}
		for (int c = (int)ceil(left); c < (int)right; ++c) {
			if (pos_vertex[y][c] == EMPTY) pos_vertex[y][c] = CANDIDATE;
		}
	}

	vector<int> empty_points;
	// for (int i = 0; i < NC; ++i) {
	// 	for (int j = 0; j < graph_size[i]; ++j) {
	// 		int u = graph[i][j];
	// 		for (int k = 0; k < j; ++k) {
	// 			int v = graph[i][k];
	// 			int d1 = graph_dist[i][u];
	// 			int d2 = graph_dist[i][v];
	// 			int d3 = graph_dist[u][v];
	// 			double cosine = (d1*d1 + d2*d2 - d3*d3) / (2.0 * d1 * d2);
	// 			if (cosine < -0.5) continue;
	// 			cosine = (d1*d1 - d2*d2 + d3*d3) / (2.0 * d1 * d3);
	// 			if (cosine < -0.5) continue;
	// 			cosine = (-d1*d1 + d2*d2 + d3*d3) / (2.0 * d2 * d3);
	// 			if (cosine < -0.5) continue;
	// 			fermat_point(i, u, v, empty_points);
	// 		}
	// 	}
	// }
	for (int y = miny; y <= maxy; ++y) {
		for (int x = minx; x <= maxx; ++x) {
			if (pos_vertex[y][x] == CANDIDATE) {
				pos_vertex[y][x] = EMPTY;
				empty_points.push_back((y << 16) | x);
			}
		}
	}
	return empty_points;
}

template <class T>
double mini_mst(const T& vs, int size) {
	START_TIMER(4);
	if (size == 1) return 0;
	double ret = 0;
	int n = 0;
	double min_d = INF;
	for (int i = 1; i < size; ++i) {
		eval_min_dist[i] = graph_dist[vs[0]][vs[i]];
		if (eval_min_dist[i] < min_d) {
			min_d = eval_min_dist[i];
			n = i;
		}
	}
	ull rest = (1ull << (size - 1)) - 1;
	rest ^= 1ull << (n - 1);
	while (rest != 0) {
		ret += min_d;
		min_d = INF;
		int nn = 0;
		ull cur_rest = rest;
		while (cur_rest != 0) {
			int j = __builtin_ctzll(cur_rest) + 1;
			double d = graph_dist[vs[n]][vs[j]];
			if (d < eval_min_dist[j]) {
				eval_min_dist[j] = d;
			}
			if (eval_min_dist[j] < min_d) {
				min_d = eval_min_dist[j];
				nn = j;
			}
			cur_rest &= cur_rest - 1;
		}
		n = nn;
		rest ^= 1ull << (n - 1);
	}
	ret += min_d;
	STOP_TIMER(4);
	return ret;
}

double evaluate_failure(int junction_size) {
	ADD_COUNTER(junction_size);
	double ret = 0;
	if (junction_size == 1) {
		START_TIMER(10);
		int center = gen_buffer[0];
		for (int i = 0; i < graph_size[center]; ++i) {
			ret -= graph_dist[center][graph[center][i]];
		}
		ret += mini_mst(graph[center], graph_size[center]);
		STOP_TIMER(10);
		return ret * P;
	} else if (junction_size == 2) {
		START_TIMER(11);
		double dist_sum = 0;
		for (int i = 0; i < 2; ++i) {
			int center = gen_buffer[i];
			double diff = 0;
			for (int j = 0; j < graph_size[center]; ++j) {
				diff -= graph_dist[center][graph[center][j]];
			}
			dist_sum -= diff;
			diff += mini_mst(graph[center], graph_size[center]);
			ret += diff * P * (1 - P);
		}
		int v0 = gen_buffer[0];
		int v1 = gen_buffer[1];
		dist_sum -= graph_dist[v0][v1];
		int bi = 0;
		for (int i = 0; i < graph_size[v0]; ++i) {
			if (graph[v0][i] != v1) gen_buffer[bi++] = graph[v0][i];
		}
		for (int i = 0; i < graph_size[v1]; ++i) {
			if (graph[v1][i] != v0) gen_buffer[bi++] = graph[v1][i];
		}
		double diff = -dist_sum + mini_mst(gen_buffer, bi);
		ret += diff * P * P;
		STOP_TIMER(11);
		return ret;
	} else if (junction_size == 3) {
		START_TIMER(12);
		array<int, 3> vs = {gen_buffer[0], gen_buffer[1], gen_buffer[2]};
		array<double, 3> single_dist_sum = {};
		for (int i = 0; i < 3; ++i) {
			int center = vs[i];
			double diff = 0;
			for (int j = 0; j < graph_size[center]; ++j) {
				diff -= graph_dist[center][graph[center][j]];
			}
			single_dist_sum[i] = -diff;
			diff += mini_mst(graph[center], graph_size[center]);
			ret += diff * P * (1 - P) * (1 - P);
		}
		for (int i = 0; i < 3; ++i) {
			int va = vs[i];
			int vb = vs[(i + 1) % 3];
			int vc = vs[(i + 2) % 3];
			double diff = -single_dist_sum[i] - single_dist_sum[(i + 1) % 3];
			int bi = 0;
			for (int j = 0; j < graph_size[va]; ++j) {
				if (graph[va][j] < NC) gen_buffer[bi++] = graph[va][j];
			}
			for (int j = 0; j < graph_size[vb]; ++j) {
				if (graph[vb][j] == va) diff += graph_dist[vb][va];
				if (graph[vb][j] < NC) gen_buffer[bi++] = graph[vb][j];
			}
			gen_buffer[bi++] = vc;
			diff += mini_mst(gen_buffer, bi);
			ret += diff * P * P * (1 - P);
		}
		double diff = -single_dist_sum[0] - single_dist_sum[1] - single_dist_sum[2];
		int bi = 0;
		for (int j = 0; j < graph_size[vs[0]]; ++j) {
			if (graph[vs[0]][j] < NC) gen_buffer[bi++] = graph[vs[0]][j];
		}
		for (int j = 0; j < graph_size[vs[1]]; ++j) {
			if (graph[vs[1]][j] == vs[0]) diff += graph_dist[vs[1]][vs[0]];
			if (graph[vs[1]][j] < NC) gen_buffer[bi++] = graph[vs[1]][j];
		}
		for (int j = 0; j < graph_size[vs[2]]; ++j) {
			if (graph[vs[2]][j] == vs[0]) diff += graph_dist[vs[2]][vs[0]];
			if (graph[vs[2]][j] == vs[1]) diff += graph_dist[vs[2]][vs[1]];
			if (graph[vs[2]][j] < NC) gen_buffer[bi++] = graph[vs[2]][j];
		}
		diff += mini_mst(gen_buffer, bi);
		ret += diff * P * P * P;
		STOP_TIMER(12);
		return ret;
	} else if (junction_size <= MAX_EVALUATE_FAILUER_SIZE) {
		START_TIMER(13);
		vi vs(gen_buffer.begin(), gen_buffer.begin() + junction_size);
		// array<int, 5> vs = {gen_buffer[0], gen_buffer[1], gen_buffer[2], gen_buffer[3], gen_buffer[4]};
		double all_inner_edges = 0;
		for (int i = 0 ; i < junction_size; ++i) {
			for (int j = 0; j < graph_size[vs[i]]; ++j) {
				int adj = graph[vs[i]][j];
				if (NC <= adj && adj < vs[i]) all_inner_edges += graph_dist[vs[i]][adj];
			}
		}
		for (int i = 1; i < (1 << junction_size); ++i) {
			int bi = 0;
			double diff = -all_inner_edges;
			double prob = 1.0;
			for (int j = 0; j < junction_size; ++j) {
				if (i & (1 << j)) {
					prob *= P;
					for (int k = 0; k < graph_size[vs[j]]; ++k) {
						int adj = graph[vs[j]][k];
						if (adj < NC) {
							gen_buffer[bi++] = adj;
							diff -= graph_dist[vs[j]][adj];
						}
					}
				} else {
					prob *= (1 - P);
					gen_buffer[bi++] = vs[j];
				}
			}
			diff += mini_mst(gen_buffer, bi);
			ret += diff * prob;
		}
		STOP_TIMER(13);
		return ret;
 	}
 	START_TIMER(14);
	vi city_size(junction_size);
	vector<double> dist_sum(junction_size);
	for (int i = 0; i < junction_size; ++i) {
		int center = gen_buffer[i];
		int head = 0;
		int tail = graph_size[center] - 1;
		while (true) {
			while (head < graph_size[center] && graph[center][head] < NC) {
				++head;
			}
			while (tail >= 0 && graph[center][tail] >= NC) {
				--tail;
			}
			if (head >= tail) break;
			swap(graph[center][head], graph[center][tail]);
			++head;
			--tail;
		}
		city_size[i] = head;
	}
	double all_prob = pow(P, junction_size);
	double single_prob = P * pow(1 - P, junction_size - 1);
	double dual_prob = (1.0 - all_prob - single_prob * junction_size) / (junction_size * (junction_size - 1) / 2);
	// single
	for (int i = 0; i < junction_size; ++i) {
		int center = gen_buffer[i];
		for (int j = 0; j < graph_size[center]; ++j) {
			int n = graph[center][j];
			dist_sum[i] += graph_dist[center][n];
		}
		double diff = -dist_sum[i] + mini_mst(graph[center], graph_size[center]);
		ret += diff * single_prob;
	}
	// dual
	vi adj;
	for (int i = 0; i < junction_size; ++i) {
		int center = gen_buffer[i];
		for (int j = city_size[i]; j < graph_size[center]; ++j) {
			int n = graph[center][j];
			if (n < center) continue;
			int ni = 0;
			while (true) {
				if (gen_buffer[ni] == n) break;
				++ni;
			}
			double diff = -dist_sum[i] - dist_sum[ni] + graph_dist[center][n];
			adj.clear();
			for (int k = 0; k < graph_size[center]; ++k) {
				if (k != j) {
					adj.push_back(graph[center][k]);
				}
			}
			for (int k = 0; k < graph_size[n]; ++k) {
				if (graph[n][k] != center) {
					adj.push_back(graph[n][k]);
				}
			}
			double mst_dist = mini_mst(adj, adj.size());
			diff += mst_dist;
			ret += diff * dual_prob;
		}
	}
	// all
	adj.clear();
	double all_cost = 0;
	for (int i = 0; i < junction_size; ++i) {
		int center = gen_buffer[i];
		adj.insert(adj.end(), graph[center].begin(), graph[center].begin() + city_size[i]);
		all_cost -= dist_sum[i];
		for (int j = city_size[i]; j < graph_size[center]; ++j) {
			int n = graph[center][j];
			if (center < n) {
				all_cost += graph_dist[center][n];
			}
		}
	}
	all_cost += mini_mst(adj, adj.size());
	ret += all_cost * all_prob;
	STOP_TIMER(14);
	return ret;
}

double evaluate(int move_base) {
	START_TIMER(2);
	double base_score = (move_base == EMPTY ? mst() : mst_diff(move_base)) + NJ * C;
	double diff = 0;
	vector<bool> visited(NJ);
	for (int i = 0; i < NJ; ++i) {
		if (visited[i]) continue;
		visited[i] = true;
		int buf_size = 1;
		gen_buffer[0] = NC + i;
		for (int j = 0; j < buf_size; ++j) {
			int cur = gen_buffer[j];
			for (int k = 0; k < graph_size[cur]; ++k) {
				int n = graph[cur][k];
				if (n >= NC && !visited[n - NC]) {
					gen_buffer[buf_size++] = n;
					visited[n - NC] = true;
				}
			}
		}
		// if (buf_size > MAX_EVALUATE_FAILUER_SIZE) {
		// 	return INF;
		// }
		START_TIMER(5);
		diff += evaluate_failure(buf_size);
		STOP_TIMER(5);
	}
	STOP_TIMER(2);
	return base_score + diff;
}

void move_rnd(Point& p) {
	pos_vertex[p.y][p.x] = EMPTY;
	do {
		p.y += rnd.nextUInt(move_range * 2 + 1) - move_range;
		p.x += rnd.nextUInt(move_range * 2 + 1) - move_range;
		move_inside(p.y);
		move_inside(p.x);
	} while (pos_vertex[p.y][p.x] != EMPTY);
}

void copy_cur_graph() {
	copy(graph_size.begin(), graph_size.begin() + NC + NJ, cur_graph_size.begin());
	for (int i = 0; i < NC + NJ; ++i) {
		copy(graph[i].begin(), graph[i].begin() + graph_size[i], cur_graph[i].begin());
	}
}

void solve() {
	// set NJ, points[NC...NC+NJ]
	NJ = 0;
	vi empty_points = collect_candidate();
	debug("empty_points size:%d\n", (int)empty_points.size());
	best_score = mst();
	copy_cur_graph();
	vector<Point> best_junctions;
	double cur_score = best_score;
	const int max_nj = min(200 - NC, NC * 2);
	sa_cooler = INITIAL_COOLER;
	move_range = INITIAL_MOVE_RANGE;
	int update_count = 0;
	int ji = -1;
	int move_pos = 0;
	debug("base score:%.5f\n", best_score);
	for (int turn = 1; ; ++turn) {
		if ((turn & 0xFF) == 0) {
			ll elapsed = get_elapsed_msec();
			if (elapsed > TL) {
				debug("turn:%d best_score:%.4f NJ:%d\n", turn, best_score, (int)best_junctions.size());
				break;
			}
			if (elapsed > TL - 500) {
				if (move_range != FINAL_MOVE_RANGE) {
					sa_cooler = 10000;
					move_range = FINAL_MOVE_RANGE;
					debug("sa_cooler:%.4f score:%.4f->%.4f NJ:%d at turn %d\n", sa_cooler, cur_score, best_score, NJ, turn);
				}
			}
		}

		int type = move_range == FINAL_MOVE_RANGE ? INF : (rnd.nextUInt() & TYPE_MASK);
		if (type == 0 && NJ > 0) {
			// remove
			ji = rnd.nextUInt(NJ);
			pos_vertex[points[NC + ji].y][points[NC + ji].x] = EMPTY;
			swap(points[NC + ji], points[NC + NJ - 1]);
			for (int i = 0; i < NC + ji; ++i) {
				swap(graph_dist[NC + ji][i], graph_dist[NC + NJ - 1][i]);
				swap(graph_dist[i][NC + ji], graph_dist[i][NC + NJ - 1]);
			}
			for (int i = NC + ji + 1; i < NC + NJ - 1; ++i) {
				swap(graph_dist[NC + ji][i], graph_dist[NC + NJ - 1][i]);
				swap(graph_dist[i][NC + ji], graph_dist[i][NC + NJ - 1]);
			}
			NJ--;
		} else if (NJ == 0 || (type == 1 && NJ < max_nj)) {
			// add
			type = 1;
			int pos = empty_points[rnd.nextUInt(empty_points.size())];
			points[NC + NJ].y = pos >> 16;
			points[NC + NJ].x = pos & 0xFFFF;
			while (pos_vertex[points[NC + NJ].y][points[NC + NJ].x] != EMPTY) {
				pos = empty_points[rnd.nextUInt(empty_points.size())];
				points[NC + NJ].y = pos >> 16;
				points[NC + NJ].x = pos & 0xFFFF;
			}
			pos_vertex[points[NC + NJ].y][points[NC + NJ].x] = NC;
			for (int i = 0; i < NC + NJ; ++i) {
				graph_dist[NC + NJ][i] = graph_dist[i][NC + NJ] = sqrt(dist2(points[i], points[NC + NJ]));
			}
			NJ++;
		// } else if (type == 2 && NJ < max_nj) {
		// 	// duplicate
		// 	type = 2;
		// 	ji = rnd.nextUInt(NJ);
		// 	int dx = rnd.nextUInt(4);
		// 	int dy = rnd.nextUInt(4);
		// 	while (dx == 0 && dy == 0) {
		// 		dx = rnd.nextUInt(4);
		// 		dy = rnd.nextUInt(4);
		// 	}
		// 	int ny = points[NC + ji].y + dy;
		// 	int nx = points[NC + ji].x + dx;
		// 	move_inside(ny);
		// 	move_inside(nx);
		// 	if (pos_vertex[ny][nx] != EMPTY) continue;
		// 	int ny2 = points[NC + ji].y - dy;
		// 	int nx2 = points[NC + ji].x - dx;
		// 	move_inside(ny2);
		// 	move_inside(nx2);
		// 	if (pos_vertex[ny2][nx2] != EMPTY) continue;
		// 	move_pos = (points[NC + ji].y << 16) | points[NC + ji].x;
		// 	pos_vertex[points[NC + ji].y][points[NC + ji].x] = EMPTY;
		// 	pos_vertex[ny][nx] = pos_vertex[ny2][nx2] = NC;
		// 	points[NC + ji].y = ny;
		// 	points[NC + ji].x = nx;
		// 	points[NC + NJ].y = ny2;
		// 	points[NC + NJ].x = nx2;
		// 	++NJ;
		} else {
			// move
			type = 3;
			ji = rnd.nextUInt(NJ);
			move_pos = (points[NC + ji].y << 16) | points[NC + ji].x;
			move_rnd(points[NC + ji]);
			pos_vertex[points[NC + ji].y][points[NC + ji].x] = NC;
			for (int i = 0; i < NC + NJ; ++i) {
				graph_dist_buf[i] = graph_dist[NC + ji][i];
				graph_dist[NC + ji][i] = graph_dist[i][NC + ji] = sqrt(dist2(points[i], points[NC + ji]));
			}
		}
		int move_base = (type == 3 && (turn & MST_DIFF_MASK) != 0) ? ji : EMPTY;
		double score = evaluate(move_base);
		if (accept(cur_score, score)) {
			ADD_COUNTER(20);
			cur_score = score;
			if (move_base != EMPTY) mst();
			copy_cur_graph();
			if (score < best_score) {
				// debug("best_score:%.5f NJ:%d at turn %d\n", score, NJ, turn);
				best_score = score;
				best_junctions.assign(points.begin() + NC, points.begin() + NC + NJ);
			}
			update_count++;
			if (update_count == (int)COOLER_UPDATE_COUNT) {
				update_count = 0;
				sa_cooler *= COOLER_UPDATE_RATIO;
				debug("sa_cooler:%.4f score:%.4f NJ:%d at turn %d\n", sa_cooler, score, NJ, turn);
			}
		} else {
			ADD_COUNTER(21);
			if (type == 0) {
				// remove
				pos_vertex[points[NC + NJ].y][points[NC + NJ].x] = NC;
				swap(points[NC + ji], points[NC + NJ]);
				for (int i = 0; i < NC + ji; ++i) {
					swap(graph_dist[NC + ji][i], graph_dist[NC + NJ][i]);
					swap(graph_dist[i][NC + ji], graph_dist[i][NC + NJ]);
				}
				for (int i = NC + ji + 1; i < NC + NJ; ++i) {
					swap(graph_dist[NC + ji][i], graph_dist[NC + NJ][i]);
					swap(graph_dist[i][NC + ji], graph_dist[i][NC + NJ]);
				}
				NJ++;
			} else if (type == 1) {
				// add
				NJ--;
				pos_vertex[points[NC + NJ].y][points[NC + NJ].x] = EMPTY;
			// } else if (type == 2) {
			// 	// duplicate
			// 	NJ--;
			// 	pos_vertex[points[NC + NJ].y][points[NC + NJ].x] = EMPTY;
			// 	pos_vertex[points[NC + ji].y][points[NC + ji].x] = EMPTY;
			// 	points[NC + ji].y = move_pos >> 16;
			// 	points[NC + ji].x = move_pos & 0xFFFF;
			// 	pos_vertex[points[NC + ji].y][points[NC + ji].x] = NC;
			} else {
				// move
				pos_vertex[points[NC + ji].y][points[NC + ji].x] = EMPTY;
				points[NC + ji].y = move_pos >> 16;
				points[NC + ji].x = move_pos & 0xFFFF;
				pos_vertex[points[NC + ji].y][points[NC + ji].x] = NC;
				for (int i = 0; i < NC + NJ; ++i) {
					graph_dist[NC + ji][i] = graph_dist[i][NC + ji] = graph_dist_buf[i];
				}
			}
		}
	}
	copy(best_junctions.begin(), best_junctions.end(), points.begin() + NC);
	NJ = best_junctions.size();
	for (int i = 0; i < NC + NJ; ++i) {
		for (int j = 0; j < i; ++j) {
			graph_dist[i][j] = graph_dist[j][i] = sqrt(dist2(points[i], points[j]));
		}
	}
	for (int i = 0; i < NJ; ++i) {
		swap(points[NC + i], points[NC + NJ - 1]);
		for (int j = 0; j < NC + i; ++j) {
			swap(graph_dist[NC + i][j], graph_dist[NC + NJ - 1][j]);
			swap(graph_dist[j][NC + i], graph_dist[j][NC + NJ - 1]);
		}
		for (int j = NC + i + 1; j < NC + NJ - 1; ++j) {
			swap(graph_dist[NC + i][j], graph_dist[NC + NJ - 1][j]);
			swap(graph_dist[j][NC + i], graph_dist[j][NC + NJ - 1]);
		}
		--NJ;
		double score = evaluate(EMPTY);
		if (score < best_score) {
			best_score = score;
			debug("remove update: best score:%f\n", score);
			--i;
		} else {
			++NJ;
		}
	}
}

class RoadsAndJunctions {
public:
	vi buildJunctions(int size, const vi& cities, double jc, double fp);

	vi buildRoads(const vi& status) {
		int LJ = 0;
		for (int i = 0; i < NJ; ++i) {
			if (status[i] == 1) {
				points[NC + LJ] = points[NC + i];
				++LJ;
			}
		}
		debug("%d out of %d failed\n", NJ - LJ, NJ);
		NJ = LJ;
		for (int i = 0; i < NC + NJ; ++i) {
			for (int j = 0; j < i; ++j) {
				graph_dist[i][j] = graph_dist[j][i] = sqrt(dist2(points[i], points[j]));
			}
		}
		mst();
		vi ret;
		for (int i = 0; i < NC + NJ; ++i) {
			const Point& from = points[i];
			for (int j = 0; j < graph_size[i]; ++j) {
				const Point& to = points[graph[i][j]];
				if (from.idx > to.idx) continue;
				ret.push_back(from.idx);
				ret.push_back(to.idx);
			}
		}
		return ret;
	}
};

vi RoadsAndJunctions::buildJunctions(int size, const vi& cities, double jc, double fp) {
	start_time = get_time();
	S = size;
	NC = cities.size() / 2;
	C = jc;
	P = fp;
	MAX_EVALUATE_FAILUER_SIZE = NC <= 50 ? 10 : 3;
	pos_vertex.assign(S + 1, vector<uint8_t>(S + 1, EMPTY));
	miny = minx = S;
	maxy = maxx = S;
	for (int i = 0; i < NC; ++i) {
		points[i].x = cities[i * 2];
		points[i].y = cities[i * 2 + 1];
		points[i].idx = i;
		pos_vertex[points[i].y][points[i].x] = i;
		miny = min(miny, points[i].y);
		maxy = max(maxy, points[i].y);
		minx = min(minx, points[i].x);
		maxx = max(maxx, points[i].x);
	}
	for (int i = 0; i < NC; ++i) {
		for (int j = 0; j < i; ++j) {
			graph_dist[i][j] = graph_dist[j][i] = sqrt(dist2(points[i], points[j]));
		}
	}
	solve();
	vi ret(NJ * 2);
	for (int i = 0; i < NJ; ++i) {
		debug("(%d, %d) ", points[NC + i].y, points[NC + i].x);
		points[NC + i].idx = NC + i;
		ret[i * 2] = points[NC + i].x;
		ret[i * 2 + 1] = points[NC + i].y;
	}
	debugln();
	return ret;
}

#ifdef LOCAL
int main() {
	RoadsAndJunctions rj;
	int S, C, J;
	cin >> S >> C;
	vector<int> cities(C);
	for (int i = 0; i < C; ++i) {
		cin >> cities[i];
	}
	double junctionCost, failureProbability;
	cin >> junctionCost >> failureProbability;
	vi ret = rj.buildJunctions(S, cities, junctionCost, failureProbability);
	cout << ret.size() << endl;
	for (int i = 0; i < (int)ret.size(); ++i) {
		cout << ret[i] << endl;
	}
	
	cin >> J;
	vector<int> junctionStatus(J);
	for (int i = 0; i < J; ++i) {
		cin >> junctionStatus[i];
	}
	ret = rj.buildRoads(junctionStatus);
	cout << ret.size() << endl;
	for (int i = 0; i < (int)ret.size(); ++i) {
		cout << setprecision(10) << ret[i] << endl;
	}
	cout << best_score << endl;
	PRINT_COUNTER();
	PRINT_TIMER();
}
#endif
