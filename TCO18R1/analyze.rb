#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:S, :NC, :cost, :P, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /S: *(\d+) NC: *(\d+) cost: *([0-9.]+) P: *([0-9.]+)/
      testcase.S = $1.to_i
      testcase.NC = $2.to_i
      testcase.cost = $3.to_f
      testcase.P = $4.to_f
    elsif line =~ /score: *([0-9.]+)/
      testcase.score = $1.to_f
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0.0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    count += 1
    if s1 > s2
      sum_score_diff += 1.0 - (1.0 * s2 / s1) ** 2
      win += 1
    elsif s1 < s2
      sum_score_diff += (1.0 * s1 / s2) ** 2 - 1.0
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff * 1000000.0 / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  100.step(999, 100) do |param|
    upper = param == 900 ? param + 100 : param + 99
    puts "S:#{param}-#{upper}"
    calc(from, to) { |from, to| from.S.between?(param, upper) }
  end
  puts "---------------------"
  10.step(90, 20) do |param|
    upper = param + 19
    puts "NC:#{param}-#{upper}"
    calc(from, to) { |from, to| from.NC.between?(param, upper) }
  end
  puts "---------------------"
  0.step(9, 2) do |param|
    upper = param + 1
    puts "cost:#{param}-#{upper}"
    calc(from, to) { |from, to| from.cost.between?(param, upper) }
  end
  puts "---------------------"
  0.step(0.39, 0.05) do |param|
    upper = param + 0.05
    puts "P:#{param}-#{upper}"
    calc(from, to) { |from, to| from.P.between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
