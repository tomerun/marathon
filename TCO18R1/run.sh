#!/bin/sh -x

for i in 3 4 5 6
do
	date
	g++48 -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL -DTYPE_MASK_BIT="$i" -pipe -mmmx -msse -msse2 -msse3 -o tester RoadsAndJunctions.cpp
	java Tester -b 1000 -e 1999 > log/log14_typemask_"$i".txt
done
