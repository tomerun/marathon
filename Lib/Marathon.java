        static final class Timer {
                long[] sum = new long[9];
                long[] start = new long[sum.length];

                void start(int i) {
                        if (MEASURE_TIME) {
                                start[i] = System.currentTimeMillis();
                        }
                }

                void end(int i) {
                        if (MEASURE_TIME) {
                                sum[i] += System.currentTimeMillis() - start[i];
                        }
                }
        }

        static final class XorShift {
                int x = 123456789;
                int y = 362436069;
                int z = 521288629;
                int w = 88675123;

                int nextInt(int n) {
                        final int t = x ^ (x << 11);
                        x = y;
                        y = z;
                        z = w;
                        w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
                        final int r = w % n;
                        return r < 0 ? r + n : r;
                }

        }

        private static final int THREAD_COUNT = 4;

        public static void main(String[] args) throws Exception {
                long seed = 1;
                long begin = -1;
                long end = -1;
                vis = false;
                manual = false;
                debug = false;
                for (int i = 0; i < args.length; i++) {
                        if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
                        if (args[i].equals("-begin")) begin = Long.parseLong(args[++i]);
                        if (args[i].equals("-end")) end = Long.parseLong(args[++i]);
                        if (args[i].equals("-vis")) vis = true;
                        if (args[i].equals("-manual")) manual = true;
                        if (args[i].equals("-info")) debug = true;
                        if (args[i].equals("-delay")) del = Integer.parseInt(args[++i]);
                        if (args[i].equals("-side")) side = Integer.parseInt(args[++i]);
                }
                if (manual) vis = true;
                if (begin != -1 && end != -1) {
                        vis = false;
                        manual = false;
                        ArrayList<Long> seeds = new ArrayList<Long>();
                        for (long i = begin; i <= end; ++i) {
                                seeds.add(i);
                        }
                        int len = seeds.size();
                        Result[] results = new Result[len];
                        TestThread[] threads = new TestThread[THREAD_COUNT];
                        int prev = 0;
                        for (int i = 0; i < THREAD_COUNT; ++i) {
                                int next = len * (i + 1) / THREAD_COUNT;
                                threads[i] = new TestThread(prev, next - 1, seeds, results);
                                prev = next;
                        }
                        for (int i = 0; i < THREAD_COUNT; ++i) {
                                threads[i].start();
                        }
                        for (int i = 0; i < THREAD_COUNT; ++i) {
                                threads[i].join();
                        }
                        double sum = 0;
                        for (int i = 0; i < results.length; ++i) {
                                System.out.println(results[i]);
                                System.out.println();
                                sum += results[i].score;
                        }
                        System.out.println("ave:" + (sum / results.length));
                } else {
                        Visualizer tester = new Visualizer();
                        System.out.println(tester.runTest(seed));
                }
        }

        static class TestThread extends Thread {
                int begin, end;
                ArrayList<Long> seeds;
                Result[] results;

                TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
                        this.begin = begin;
                        this.end = end;
                        this.seeds = seeds;
                        this.results = results;
                }

                public void run() {
                        for (int i = begin; i <= end; ++i) {
                                Visualizer f = new Visualizer();
                                try {
                                        Result res = f.runTest(seeds.get(i));
                                        results[i] = res;
                                } catch (Exception e) {
                                        e.printStackTrace();
                                        results[i] = new Result();
                                        results[i].seed = seeds.get(i);
                                }
                        }
                }
        }

        static class Result {
                static final Result NULL_RESULT = new Result();
                long seed;
                int SZ;
                int whiteCount;
                double score;
                long elapsed;
                boolean isStripe;

                public String toString() {
                        return "seed " + this.seed + "\nSZ " + this.SZ + "\nelapsed " + this.elapsed / 1000.0 + "\nscore " + this.score;
                }

        }