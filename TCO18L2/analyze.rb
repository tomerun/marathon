#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:H, :W, :P, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /H: *(\d+) W: *(\d+) P:(0\.\d*)/
      testcase.H = $1.to_i
      testcase.W = $2.to_i
      testcase.P = $3.to_f
    elsif line =~ /score:(.+)$/
      testcase.score = $1.to_i
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    count += 1
    if s1 < s2
      sum_score_diff += 1.0 - (1.0 * s1 / s2) ** 2
      win += 1
    elsif s1 > s2
      sum_score_diff -= 1.0 - (1.0 * s2 / s1) ** 2
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff * 1000000.0 / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  [100, 1000, 1800, 3200, 5000, 10001].each_cons(2) do |lower, upper|
    puts "S:#{lower}-#{upper - 1}"
    calc(from, to) { |from, to| (from.H * from.W).between?(lower, upper - 1) }
  end
  puts "---------------------"
  (0.0).step(0.55, 0.1) do |param|
    upper = param + 0.1
    puts "P:#{sprintf('%.2f', param)}-#{sprintf('%.2f', upper)}"
    calc(from, to) { |from, to| from.P.between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
