import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Tester {
  private static final int maxS = 100, minS = 10;
  private static int[] dr = {1, 1, -1, -1, 2, 2, -2, -2};
  private static int[] dc = {2, -2, 2, -2, 1, -1, 1, -1};
  private int H, W;                       // board size
  private double P;
  private int PC;
  private volatile char[][] board;        // board (P for a pawn, K for a knight . for empty space)
  private volatile int K;
  private volatile int[][] attacks;       // number of other pieces attacked by this knight

  private void generate(long seed) throws Exception {
    SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
    r1.setSeed(seed);
    H = r1.nextInt(maxS - minS + 1) + minS;
    W = r1.nextInt(maxS - minS + 1) + minS;
    if (seed == 1) {
      H = 5;
      W = 10;
    } else if (seed == 2) {
      W = H = (minS + maxS) / 2;
    } else if (seed == 3) {
      W = H = maxS;
    }
    P = r1.nextDouble() * 0.6;   // probability of a pawn being placed in a cell
    if (1000 <= seed && seed < 2000) {
      P = (seed - 1000 + 0.5) * 0.6 / 1000;
    }
    board = new char[H][W];
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c) {
        if (r1.nextDouble() < P) {
          board[r][c] = 'P';
          ++PC;
        } else {
          board[r][c] = '.';
        }
      }
  }

  private void updateCell(int r, int c) {
    attacks[r][c] = 0;
    if (board[r][c] != 'K')
      return;
    for (int k = 0; k < 8; ++k) {
      int r1 = r + dr[k];
      int c1 = c + dc[k];
      if (r1 >= 0 && c1 >= 0 && r1 < H && c1 < W && board[r1][c1] != '.')
        attacks[r][c]++;
    }
  }

  private void updateBoard() {
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c)
        updateCell(r, c);
  }

  private boolean isValidBoard() {
    updateBoard();
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c)
        if (board[r][c] == 'K' && attacks[r][c] != 2)
          return false;
    return true;
  }

  private Result runTest(long seed) throws Exception {
    generate(seed);
    K = 0;
    attacks = new int[H][W];
    if (vis) {
      jf.setTitle("Seed " + seed);
      jf.setVisible(true);
      Insets frameInsets = jf.getInsets();
      int fw = frameInsets.left + frameInsets.right + 8;
      int fh = frameInsets.top + frameInsets.bottom + 8;
      Toolkit toolkit = Toolkit.getDefaultToolkit();
      Dimension screenSize = toolkit.getScreenSize();
      Insets screenInsets = toolkit.getScreenInsets(jf.getGraphicsConfiguration());
      screenSize.width -= screenInsets.left + screenInsets.right;
      screenSize.height -= screenInsets.top + screenInsets.bottom;
      if (SZ == 0) {
        SZ = Math.min(Math.min((screenSize.width - fw - 120) / W, (screenSize.height - fh) / H), 40);
        if (!plain && SZ < 12) plain = true;
      }
      Dimension dim = v.getVisDimension();
      v.setPreferredSize(dim);
      jf.setSize(Math.min(dim.width + fw, screenSize.width), Math.min(dim.height + fh, screenSize.height));
      manualReady = false;
      draw();
    }

    Result res = new Result();
    res.seed = seed;
    res.H = H;
    res.W = W;
    res.P = 1.0 * PC / (H * W);
    try {
      String[] boardArg = new String[H];
      for (int i = 0; i < H; ++i) {
        boardArg[i] = new String(board[i]);
      }
      long beforeTime = System.currentTimeMillis();
      String[] placement = placeKnights(boardArg);
      res.elapsed = System.currentTimeMillis() - beforeTime;
      if (placement.length != H) {
        throw new RuntimeException("Your return must contain " + H + " strings, but it contained " + placement.length + ".");
      }
      for (int i = 0; i < H; ++i) {
        if (placement[i].length() != W) {
          throw new RuntimeException("element " + i + " had length " + placement[i].length() + ".");
        }
        for (int j = 0; j < W; ++j) {
          char c = placement[i].charAt(j);
          if (c != '.' && c != 'K') {
            throw new RuntimeException("element " + i + " contained character '" + c + "'.");
          }
          if (c == 'K') {
            if (board[i][j] == 'P') {
              throw new RuntimeException("put a knight on a cell (" + i + ", " + j + ") which already contains a pawn.");
            }
            board[i][j] = 'K';
            K++;
          }
        }
      }
      if (vis) draw();
      if (manual) {
        System.err.println("Manual play on");
        // wait till player finishes (possibly on top of automated return)
        while (!manualReady) {
          try {
            Thread.sleep(50);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      if (!isValidBoard()) {
        throw new RuntimeException("Not all knights attack exactly 2 pieces.");
      }
    } catch (Exception e) {
      e.printStackTrace();
      res.score = -1000000;
      return res;
    } finally {
      proc.destroy();
    }
    res.score = K;
    return res;
  }

  private static long seed;
  private JFrame jf;
  private Vis v;
  private static String exec = "./tester";
  private static boolean vis, manual, plain, save;
  private Process proc;
  private static int SZ;
  private volatile boolean manualReady;

  private String[] placeKnights(String[] board) throws IOException {
    try (OutputStream os = proc.getOutputStream();
         InputStream is = proc.getInputStream();
         BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
      StringBuilder sb = new StringBuilder();
      sb.append(board.length).append('\n');
      for (int i = 0; i < board.length; ++i)
        sb.append(board[i]).append('\n');
      os.write(sb.toString().getBytes());
      os.flush();
      int nret = Integer.parseInt(br.readLine());
      String[] ret = new String[nret];
      for (int i = 0; i < nret; ++i)
        ret[i] = br.readLine();
      return ret;
    }
  }

  private void draw() {
    if (!vis) return;
    v.repaint();
  }

  private class Vis extends JPanel implements MouseListener {
    public void paint(Graphics g) {
      super.paint(g);
      Dimension dim = getVisDimension();
      BufferedImage bi = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      // background
      g2.setColor(Color.LIGHT_GRAY);
      g2.fillRect(0, 0, dim.width, dim.height);
      // board
      g2.setColor(Color.WHITE);
      g2.fillRect(0, 0, W * SZ, H * SZ);
      g2.setBackground(Color.WHITE);

      g2.setColor(Color.BLACK);
      for (int i = 0; i <= H; i++)
        g2.drawLine(0, i * SZ, W * SZ, i * SZ);
      for (int i = 0; i <= W; i++)
        g2.drawLine(i * SZ, 0, i * SZ, H * SZ);

      updateBoard();

      g2.setFont(new Font("Segoe UI Symbol", Font.BOLD, SZ - 3));
      FontMetrics fm = g2.getFontMetrics();
      int h = fm.getHeight() / 2;
      for (int i = 0; i < H; ++i)
        for (int j = 0; j < W; ++j) {
          if (board[i][j] == 'K') {
            if (attacks[i][j] != 2) {
              // red for knights which attack incorrect number of pieces
              g2.setColor(Color.RED);
              g2.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
              if (plain)
                continue;
            }
          }
          // draw the pawn/knight
          if (board[i][j] != '.') {
            g2.setColor(board[i][j] == 'K' ? Color.BLACK : Color.GRAY);
            if (plain) {
              g2.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
            } else {
              char[] ch = {board[i][j] == 'K' ? '\u265E' : '\u265F'};
              g2.drawChars(ch, 0, ch.length, j * SZ + SZ / 2 - fm.charWidth(ch[0]) / 2, i * SZ + SZ / 2 + h - 2);
            }
          }
        }
      for (int i = 0; i < H; ++i)
        for (int j = 0; j < W; ++j) {
          if (board[i][j] == 'K') {
            for (int k = 0; k < 8; k++) {
              int nr = i + dr[k];
              int nc = j + dc[k];
              if (0 <= nr && nr < H && 0 <= nc && nc < W && board[nr][nc] != '.') {
                g2.setColor(Color.LIGHT_GRAY);
                g2.drawLine(j * SZ + SZ / 2, i * SZ + SZ / 2, nc * SZ + SZ / 2, nr * SZ + SZ / 2);
              }
            }
          }
        }
      // "buttons" to control visualization options
      g2.setFont(new Font("Arial", Font.BOLD, 13));
      g2.setStroke(new BasicStroke(1f));
      g2.setColor(Color.BLACK);
      int xText = SZ * W + 10;
      int wText = 100;
      int yText = 10;
      int hButton = 30;
      int hText = 20;
      int vGap = 10;

      if (manualReady)
        g2.clearRect(xText, yText, wText, hButton);
      drawString(g2, "READY", xText, yText, wText, hButton, 0);
      g2.drawRect(xText, yText, wText, hButton);
      yText += hButton + vGap;

      if (plain)
        g2.clearRect(xText, yText, wText, hButton);
      drawString(g2, "PLAIN", xText, yText, wText, hButton, 0);
      g2.drawRect(xText, yText, wText, hButton);
      yText += hButton + vGap;

      // current score
      drawString(g2, "SCORE", xText, yText, wText, hText, 0);
      yText += hText;
      boolean valid = isValidBoard();
      g2.setColor(valid ? Color.BLACK : Color.RED);
      drawString(g2, String.format("%d", valid ? K : -1), xText, yText, wText, hText, 0);

      if (save) {
        try {
          ImageIO.write(bi, "png", new File(seed + ".png"));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      g.drawImage(bi, 0, 0, null);
    }

    void drawString(Graphics2D g2, String text, int x, int y, int w, int h, int align) {
      FontMetrics metrics = g2.getFontMetrics();
      Rectangle2D rect = metrics.getStringBounds(text, g2);
      int th = (int) (rect.getHeight());
      int tw = (int) (rect.getWidth());
      if (align == 0) x = x + (w - tw) / 2;
      else if (align > 0) x = (x + w) - tw;
      y = y + (h - th) / 2 + metrics.getAscent();
      g2.drawString(text, x, y);
    }

    Vis() {
      addMouseListener(this);
      jf.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
          if (proc != null)
            try {
              proc.destroy();
            } catch (Exception ex) {
              ex.printStackTrace();
            }
          System.exit(0);
        }
      });
    }

    Dimension getVisDimension() {
      return new Dimension(W * SZ + 126, H * SZ + 1);
    }

    public void mousePressed(MouseEvent e) {
      // Treat "plain" button
      int x = e.getX() - SZ * W - 10, y = e.getY() - 10;
      if (x >= 0 && x <= 100 && y >= 40 && y <= 70) {
        plain = !plain;
        repaint();
        return;
      }

      // for manual play
      if (!manual || manualReady) return;

      // "ready" button submits current state of the board
      if (x >= 0 && x <= 100 && y >= 0 && y <= 30) {
        manualReady = true;
        repaint();
        return;
      }

      int row = e.getY() / SZ, col = e.getX() / SZ;
      // convert to args only clicks with valid coordinates
      if (row < 0 || row >= H || col < 0 || col >= W)
        return;

      // ignore clicks on pawns
      if (board[row][col] == 'P')
        return;

      // a click toggles the state of the knight
      if (board[row][col] == '.') {
        board[row][col] = 'K';
        K++;
      } else {
        board[row][col] = '.';
        K--;
      }
      repaint();
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
  }

  private Tester() throws Exception {
    if (vis) {
      jf = new JFrame();
      v = new Vis();
      JScrollPane sp = new JScrollPane(v);
      jf.getContentPane().add(sp);
    }
    Runtime rt = Runtime.getRuntime();
    proc = rt.exec(exec);
    new ErrorReader(proc.getErrorStream()).start();
  }

  private static final int THREAD_COUNT = 2;

  public static void main(String[] args) throws Exception {
    seed = 1;
    long begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-exec")) exec = args[++i];
      if (args[i].equals("-vis")) vis = true;
      if (args[i].equals("-manual")) manual = true;
      if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
      if (args[i].equals("-plain")) plain = true;
      if (args[i].equals("-save")) save = true;
    }
    if (manual) vis = true;

    if (begin != -1 && end != -1) {
      vis = false;
      BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
      for (long i = begin; i <= end; ++i) {
        q.add(i);
      }
      Result[] results = new Result[(int) (end - begin + 1)];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i] = new TestThread(q, results, begin);
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / (end - begin + 1)));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    long seedMin;
    Result[] results;
    BlockingQueue<Long> q;

    TestThread(BlockingQueue<Long> q, Result[] results, long seedMin) {
      this.q = q;
      this.results = results;
      this.seedMin = seedMin;
    }

    public void run() {
      while (true) {
        Long seed = q.poll();
        if (seed == null) break;
        try {
          Tester f = new Tester();
          Result res = f.runTest(seed);
          results[(int) (seed - seedMin)] = res;
        } catch (Exception e) {
          e.printStackTrace();
          Result res = new Result();
          res.seed = seed;
          results[(int) (seed - seedMin)] = res;
        }
      }
    }
  }
}

class ErrorReader extends Thread {
  private InputStream error;

  ErrorReader(InputStream is) {
    error = is;
  }

  public void run() {
    try {
      byte[] ch = new byte[50000];
      int read;
      while ((read = error.read(ch)) > 0) {
        String s = new String(ch, 0, read);
        System.err.print(s);
        System.err.flush();
      }
    } catch (Exception e) {
      //
    }
  }
}

class Result {
  long seed;
  int H, W;
  double P;
  int score;
  long elapsed;

  public String toString() {
    String ret = String.format("seed:%4d\n", seed);
    ret += String.format("H:%3d W:%3d P:%.4f\n", H, W, P);
    ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
    ret += String.format("score:%d", score);
    return ret;
  }
}

