#include <algorithm>
#include <utility>
#include <vector>
#include <unordered_set>
#include <bitset>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
// #define DEBUG
#define NDEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;
using pi=pair<int, int>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 7000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9700;
#endif

ll start_time; // msec

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
	ull ret;
	__asm__ volatile ("rdtsc" : "=A" (ret));
	return ret;
#else
	ull high,low;
	__asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
	return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

struct Board {
	vector<bool> f;
};

const int DR[8] = {-1, -2, -2, -1, 1, 2, 2, 1};
const int DC[8] = {-2, -1, 1, 2, 2, 1, -1, -2};
const int DM[8] = {-130, -257, -255, -126, 130, 257, 255, 126};
const double INITIAL_COOLER = 1.1;
const double FINAL_COOLER = 5.5;
// const array<int, 9> PENALTY = {2, 1, 0, 2, 5, 8, 12, 16, 22};
// const double INITIAL_PENA_RATIO = 1.3;
// const double FINAL_PENA_RATIO = 6.0;

int H, W;
Board orig_board, board, best_board;
int ac[128 * 128], kc[128 * 128];
int buffer[20000];
int buf_dir[8];
int dir_list[8] = {0, 1, 2, 3, 4, 5, 6, 7};
double cooler = INITIAL_COOLER;
// double pena_ratio = INITIAL_PENA_RATIO;

inline bool accept(int diff) {
	if (diff >= 0) return true;
	return rnd.nextDouble() < exp(diff * cooler);
}

inline int encode(int r, int c) {
	return (r << 7) | c;
}

struct PositionSet {
	vi pos;
	vi idx;

	void init() {
		idx.assign((H + 4) * 128, -1);
		pos.clear();
	}

	int size() const {
		return pos.size();
	}

	bool contains(int p) const {
		return idx[p] != -1;
	}

	bool contains(int r, int c) const {
		return contains(encode(r, c));
	}

	void add(int p) {
		assert(idx[p] == -1);
		idx[p] = pos.size();
		pos.push_back(p);
	}

	void add(int r, int c) {
		add(encode(r, c));
	}

	void remove(int p) {
		int i = idx[p];
		assert(pos[i] == p);
		swap(pos[i], pos.back());
		idx[pos[i]] = i;
		idx[p] = -1;
		pos.pop_back();
	}

	void remove(int r, int c) {
		remove(encode(r, c));
	}

	int pull() {
		int pi = rnd.nextUInt(size());
		int ret = pos[pi];
		swap(pos[pi], pos.back());
		idx[pos[pi]] = pi;
		idx[ret] = -1;
		pos.pop_back();
		return ret;
	}

	int get_random() {
		int p = rnd.nextUInt(size());
		return pos[p];
	}

	int get_and_move_random(int skip) {
		int p = rnd.nextUInt(size() - skip) + skip;
		swap(pos[skip], pos[p]);
		idx[pos[skip]] = skip;
		idx[pos[p]] = p;
		return pos[skip];
	}
};
PositionSet all_ps, add_ps, knight_ps;

void update_add_ps_remove(int rem_count, int* buf) {
	for (int i = 0; i < rem_count; ++i) {
		const int p = buf[i];
		if (!add_ps.contains(p)) {
			add_ps.add(p);
		}
		for (int j = 0; j < 8; ++j) {
			int np = p + DM[j];
			if (!add_ps.contains(np) && !board.f[np] && kc[np] == 0 && ac[np] <= 2) {
				add_ps.add(np);
			}
		}
	}
}

template<bool update_knight_ps>
void update_add_ps_add(int add_count, int* buf) {
	for (int i = 0; i < add_count; ++i) {
		const int p = buf[i];
		if (add_ps.contains(p)) {
			add_ps.remove(p);
		}
		for (int j = 0; j < 8; ++j) {
			int np = p + DM[j];
			if (add_ps.contains(np)) {
				add_ps.remove(np);
			}
			if (update_knight_ps) ++kc[np];
		}
		if (update_knight_ps) knight_ps.add(p);
	}
}

bool validate() {
	bool result = true;
	for (int i = 2; i < H + 2; ++i) {
		for (int j = 2; j < W + 2; ++j) {
			int pos = encode(i, j);
			int count_knight = 0;
			int count_some = 0;
			for (int k = 0; k < 8; ++k) {
				if (board.f[pos + DM[k]]) {
					++count_knight;
				}
				if (2 <= i + DR[k] && i + DR[k] < 2 + H && 2 <= j + DC[k] && j + DC[k] < 2 + W && orig_board.f[pos + DM[k]]) {
					++count_some;
				}
			}
			if (count_knight != kc[pos]) {
				debug("ck: %d %d %d %d\n", i, j, count_knight, kc[pos]);
				result = false;
			}
			if (!orig_board.f[pos] && count_knight + count_some != ac[pos]) {
				debug("cs: %d %d %d %d %d\n", i, j, count_knight, count_some, ac[pos]);
				result = false;
			}
			if (board.f[pos] && count_knight + count_some != 2) {
				debug("n: %d %d %d %d\n", i, j, count_knight, count_some);
				result = false;
			}
		}
	}
	return result;
}

int add_chain_both(int sp, int* buf) {
	buf[0] = sp;
	int qs = 1;
	board.f[sp] = true;
	for (int i = 0; i < 8; ++i) {
		ac[sp + DM[i]]++;
	}
	for (int i = 0; i < qs; ++i) {
		if (ac[buf[i]] == 2) continue;
		int dc = 0;
		for (int j = 0; j < 8; ++j) {
			int np = buf[i] + DM[j];
			if (!add_ps.contains(np) || board.f[np] || ac[np] > 2) continue;
			bool ok = true;
			for (int k = 0; k < 8; ++k) {
				int nnp = np + DM[k];
				if (board.f[nnp] && ac[nnp] >= 2) {
					ok = false;
					break;
				}
			}
			if (ok) buf_dir[dc++] = j;
		}
		if (dc < 2 - ac[buf[i]]) {
			// fail
			// debug("fail:%lu\n", add_ps.size());
			for (int j = 0; j < qs; ++j) {
				int p = buf[j];
				board.f[p] = false;
				for (int k = 0; k < 8; ++k) {
					ac[p + DM[k]]--;
				}
			}
			return 0;
		}
		if (dc > 1) {
			swap(buf_dir[0], buf_dir[rnd.nextUInt(dc)]);
		}
		int np = buf[i] + DM[buf_dir[0]];
		board.f[np] = true;
		for (int j = 0; j < 8; ++j) {
			ac[np + DM[j]]++;
		}
		buf[qs++] = np;
		if (ac[buf[i]] == 1) {
			if (dc > 2) {
				swap(buf_dir[1], buf_dir[rnd.nextUInt(dc - 1) + 1]);
			}
			np = buf[i] + DM[buf_dir[1]];
			board.f[np] = true;
			for (int j = 0; j < 8; ++j) {
				ac[np + DM[j]]++;
			}
			buf[qs++] = np;
		}
	}
	return qs;
}

int add_chain_from(int sp, int* buf) {
	// debug("add_chain_from:%d %d\n", sr, sc);
	board.f[sp] = true;
	for (int i = 0; i < 8; ++i) {
		ac[sp + DM[i]]++;
	}
	buf[0] = sp;
	int qs = 1;
	for (int i = 0; i < qs; ++i) {
		// debug("cr:%d cc:%d ac:%d\n", cr, cc, ac[cr][cc]);
		if (ac[buf[i]] == 2) {
			for (int j = 0; j < 8; ++j) {
				kc[buf[i] + DM[j]]++;
			}
			break;
		}
		START_TIMER(10);
		bool ok = false;
		swap(dir_list[0], dir_list[rnd.nextUInt() & 7]);
		swap(dir_list[1], dir_list[rnd.nextUInt() & 7]);
		for (int j = 0; j < 8; ++j) {
			int np = buf[i] + DM[dir_list[j]];
			if (board.f[np] || kc[np] > 0 || ac[np] > 2) continue;
			board.f[np] = true;
			for (int k = 0; k < 8; ++k) {
				ac[np + DM[k]]++;
				kc[buf[i] + DM[k]]++;
			}
			buf[qs++] = np;
			ok = true;
			break;
		}
		STOP_TIMER(10);
		if (!ok) {
			START_TIMER(11);
			// debug("fail:%d\n", qs);
			for (int j = 0; j < qs - 1; ++j) {
				int p = buf[j];
				board.f[p] = false;
				for (int k = 0; k < 8; ++k) {
					ac[p + DM[k]]--;
					kc[p + DM[k]]--;
				}
			}
			board.f[buf[i]] = false;
			for (int j = 0; j < 8; ++j) {
				ac[buf[i] + DM[j]]--;
			}
			STOP_TIMER(11);
			return 0;
		}
	}
	// debug("success:%d\n", qs);
	return qs;
}

int add_chain(int sp, int* buf) {
	int dc = 0;
	for (int i = 0; i < 8; ++i) {
		int np = sp + DM[i];
		if (board.f[np] || kc[np] > 0 || ac[np] >= 2) continue;
		buf_dir[dc++] = i;
	}
	if (dc < 2 - ac[sp]) {
		return 0;
	}
	buf[0] = sp;
	board.f[sp] = true;
	for (int i = 0; i < 8; ++i) {
		ac[sp + DM[i]]++;
	}
	bool fail = false;
	bool satisfied_first = false;
	int add_count = 0;
	while (ac[sp] < 2) {
		if (dc > 1) {
			swap(buf_dir[dc - 1], buf_dir[rnd.nextUInt(dc)]);
		}
		int fp = sp + DM[buf_dir[dc - 1]];
		--dc;
		if (kc[fp] > 0 || ac[fp] > 2) {
			if (dc == 0) {
				fail = true;
				break;
			}
			continue;
		}
		if (ac[sp] == 1) {
			satisfied_first = true;
			for (int i = 0; i < 8; ++i) {
				kc[sp + DM[i]]++;
			}
		}
		START_TIMER(5);
		int count = add_chain_from(fp, &buf[1 + add_count]);
		STOP_TIMER(5);
		if (count == 0) {
			fail = true;
			break;
		}
		add_count += count;
	}
	if (fail) {
		START_TIMER(13);
		board.f[sp] = false;
		if (satisfied_first) {
			for (int i = 0; i < 8; ++i) {
				ac[sp + DM[i]]--;
				kc[sp + DM[i]]--;
			}
		} else {
			for (int i = 0; i < 8; ++i) {
				ac[sp + DM[i]]--;
			}
		}
		for (int j = 1; j <= add_count; ++j) {
			int p = buf[j];
			board.f[p] = false;
			for (int k = 0; k < 8; ++k) {
				ac[p + DM[k]]--;
				kc[p + DM[k]]--;
			}
		}
		STOP_TIMER(13);
		return 0;
	}
	if (!satisfied_first) {
		for (int i = 0; i < 8; ++i) {
			kc[sp + DM[i]]++;
		}
	}
	return add_count + 1;
}

void add() {
	for (int i = 0; i < add_ps.size(); ++i) {
		START_TIMER(1);
		const int ap = add_ps.get_and_move_random(i);
		STOP_TIMER(1);
		START_TIMER(2);
		int add_count = add_chain_both(ap, &buffer[0]);
		STOP_TIMER(2);
		START_TIMER(3);
		update_add_ps_add<true>(add_count, &buffer[0]);
		STOP_TIMER(3);
	}
}

void remove_one() {
	buffer[0] = knight_ps.pull();
	int qs = 1;
	const int sp = buffer[0];
	board.f[sp] = false;
	if (!add_ps.contains(sp)) {
		add_ps.add(sp);
	}
	for (int i = 0; i < qs; ++i) {
		for (int j = 0; j < 8; ++j) {
			int np = buffer[i] + DM[j];
			ac[np]--;
			kc[np]--;
			if (board.f[np]) {
				buffer[qs++] = np;
				board.f[np] = false;
				knight_ps.remove(np);
			}
			if (!add_ps.contains(np) && !board.f[np] && ac[np] <= 2 && kc[np] == 0) {
				add_ps.add(np);
			}
		}
	}
	// debug("remove:%d\n", k);
}

int remove(int sp, int* buf) {
	assert(board.f[sp]);
	buf[0] = sp;
	int qs = 1;
	board.f[sp] = false;
	for (int i = 0; i < qs; ++i) {
		for (int j = 0; j < 8; ++j) {
			int np = buf[i] + DM[j];
			ac[np]--;
			kc[np]--;
			if (board.f[np]) {
				buf[qs++] = np;
				board.f[np] = false;
			}
		}
	}
	return qs;
}

void clear() {
	for (int i = 2; i < H + 2; ++i) {
		for (int j = 2; j < W + 2; ++j) {
			int pos = encode(i, j);
			if (board.f[pos]) {
				board.f[pos] = false;
				knight_ps.remove(pos);
				for (int k = 0; k < 8; ++k) {
					ac[pos + DM[k]]--;
					kc[pos + DM[k]]--;
				}
			}
		}
	}
	for (int i = 2; i < H + 2; ++i) {
		for (int j = 2; j < W + 2; ++j) {
			int pos = encode(i, j);
			if (!board.f[pos] && ac[pos] <= 2 && !add_ps.contains(pos)) {
				add_ps.add(pos);
			}
		}
	}
}

Board solve() {
	knight_ps.init();
	int best_score = 0;
	best_board = board;
	int best_turn = 0;
	for (int turn = 1; ; ++turn) {
		if ((turn & 0xFFF) == 0) {
			auto elapsed = get_elapsed_msec();
			if (elapsed > TL) {
				debug("turn:%d score:%d\n", turn, best_score);
				break;
			}
			if (turn - best_turn > 100000) {
				clear();
				best_turn = turn;
			}
		}
		add();
		if (knight_ps.size() > best_score) {
			best_score = knight_ps.size();
			best_board = board;
			best_turn = turn;
			debug("score:%d at turn %d\n", knight_ps.size(), turn);
		}
		START_TIMER(0);
		if (knight_ps.size() > 0) {
			remove_one();
		}
		STOP_TIMER(0);
	}
	return best_board;
}

template<bool whole>
Board solve_iter() {
	int best_score = 0;
	best_board = board;
	int score = 0;
	const ll begin_time = get_elapsed_msec();
	for (int turn = 1; ; ++turn) {
		if ((turn & 0xFFF) == 0) {
			auto elapsed = get_elapsed_msec();
			if (elapsed > TL) {
				debug("turn:%d score:%d\n", turn, best_score);
				break;
			}
			if ((turn & 0x3FFF) == 0) {
				double ratio = 1.0 * (elapsed - begin_time) / (TL - begin_time);
				double c1 = log(INITIAL_COOLER);
				double c2 = log(FINAL_COOLER);
				cooler = elapsed > TL - 50 ? 10 : exp(c1 + (c2 - c1) * ratio);
				debug("cooler:%.4f score:%d\n", cooler, score);
			}
		}
		int pos = all_ps.get_random();
		bool remove_only = false;
		int rem_count = 0;
		int add_count = 0;
		if (board.f[pos]) {
			START_TIMER(0);
			remove_only = true;
			rem_count = remove(pos, &buffer[0]);
			STOP_TIMER(0);
		} else {
			START_TIMER(1);
			for (int i = 0; i < 8; ++i) {
				if (board.f[pos + DM[i]]) {
					rem_count += remove(pos + DM[i], &buffer[rem_count]);
				}
			}
			update_add_ps_remove(rem_count, &buffer[0]);
			STOP_TIMER(1);
			START_TIMER(2);
			if (whole) {
				for (int j = 0; j < add_ps.size(); ++j) {
					int ap = add_ps.get_and_move_random(j);
					if (board.f[ap] || kc[ap] > 0) continue;
					add_count += add_chain(ap, &buffer[rem_count + add_count]);
				}
				// if (!validate()) {
				// 	debugStr("invalid\n");
				// 	return board;
				// }
			} else {
				add_count = add_chain(pos, &buffer[rem_count]);
			}
			STOP_TIMER(2);
		}
		// debug("add_c:%d rem_c:%d\n", add_count, rem_count);
		if (accept(add_count - rem_count)) {
			START_TIMER(3);
			score += add_count - rem_count;
			if (score > best_score) {
				best_score = score;
				best_board = board;
				// debug("score:%d at turn %d\n", score, turn);
			}
			if (remove_only) {
				update_add_ps_remove(rem_count, &buffer[0]);
			}
			update_add_ps_add<false>(add_count, &buffer[rem_count]);
			STOP_TIMER(3);
		} else {
			START_TIMER(4);
			for (int i = 0; i < add_count; ++i) {
				int ap = buffer[rem_count + i];
				assert(board.f[ap]);
				board.f[ap] = false;
				for (int j = 0; j < 8; ++j) {
					ac[ap + DM[j]]--;
					kc[ap + DM[j]]--;
				}
			}
			for (int i = 0; i < rem_count; ++i) {
				int rp = buffer[i];
				assert(!board.f[rp]);
				board.f[rp] = true;
				if (add_ps.contains(rp)) {
					add_ps.remove(rp);
				}
				for (int j = 0; j < 8; ++j) {
					ac[rp + DM[j]]++;
					kc[rp + DM[j]]++;
					if (add_ps.contains(rp + DM[j])) {
						add_ps.remove(rp + DM[j]);
					}
				}
			}
			STOP_TIMER(4);
		}
	}
	return best_board;
}

// Board solve_sa() {
// 	int best_score = 0;
// 	best_board = board;
// 	int score = 0;
// 	int cur_pena = 0;
// 	const ll begin_time = get_elapsed_msec();
// 	for (int turn = 1; ; ++turn) {
// 		if ((turn & 0xFFF) == 0) {
// 			auto elapsed = get_elapsed_msec();
// 			if (elapsed > TL) {
// 				debug("turn:%d score:%d\n", turn, best_score);
// 				break;
// 			}
// 			if ((turn & 0xFFFFF) == 0) {
// 				double ratio = 1.0 * (elapsed - begin_time) / (TL - begin_time);
// 				double c1 = log(INITIAL_COOLER);
// 				double c2 = log(FINAL_COOLER);
// 				cooler = exp(c1 + (c2 - c1) * ratio);
// 				c1 = log(INITIAL_PENA_RATIO);
// 				c2 = log(FINAL_PENA_RATIO);
// 				pena_ratio = exp(c1 + (c2 - c1) * ratio);
// 				if (elapsed > TL - 100) {
// 					cooler = 10;
// 					pena_ratio = 100;
// 				}
// 				debug("cooler:%.4f pena_ratio:%.4f pena:%d score:%d\n", cooler, pena_ratio, cur_pena, score);
// 			}
// 		}
// 		int pos = all_ps.get_random();
// 		int r = pos >> 8;
// 		int c = pos & 0xFF;
// 		int prev_pena = 0;
// 		int next_pena = 0;
// 		int diff_ac;
// 		if (board.f[r][c]) {
// 			prev_pena += PENALTY[ac[r][c]];
// 			diff_ac = -1;
// 		} else {
// 			next_pena += PENALTY[ac[r][c]];
// 			diff_ac = 1;
// 		}
// 		for (int i = 0; i < 8; ++i) {
// 			int nr = r + DR[i];
// 			int nc = c + DC[i];
// 			if (board.f[nr][nc]) {
// 				prev_pena += PENALTY[ac[nr][nc]];
// 				next_pena += PENALTY[ac[nr][nc] + diff_ac];
// 			}
// 		}
// 		double diff = (prev_pena - next_pena) * pena_ratio + diff_ac;
// 		if (accept(diff)) {
// 			cur_pena += next_pena - prev_pena;
// 			board.f[r][c] = !board.f[r][c];
// 			score += diff_ac;
// 			for (int i = 0; i < 8; ++i) {
// 				ac[r + DR[i]][c + DC[i]] += diff_ac;
// 			}
// 			if (cur_pena == 0 && score > best_score) {
// 				best_score = score;
// 				best_board = board;
// 				debug("score:%d at turn %d\n", score, turn);
// 			}
// 		}
// 	}
// 	return best_board;
// }

class KnightsAndPawns {
public:
	vector<string> placeKnights(const vector<string>& initial_board);
};

vector<string> KnightsAndPawns::placeKnights(const vector<string>& initial_board) {
	start_time = get_elapsed_msec();
	H = initial_board.size();
	W = initial_board[0].size();
	orig_board.f.resize((H + 4) * 128);
	board.f.resize((H + 4) * 128);
	all_ps.init();
	add_ps.init();
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			if (initial_board[i][j] == 'P') {
				int pos = encode(i + 2, j + 2);
				orig_board.f[pos] = true;
				ac[pos] = 8; // hack
				for (int k = 0; k < 8; ++k) {
					ac[pos + DM[k]]++;
				}
			}
		}
		orig_board.f[encode(i + 2, 0)] = orig_board.f[encode(i + 2, 1)] = orig_board.f[encode(i + 2, W + 2)] = orig_board.f[encode(i + 2, W + 3)] = true;
		ac[encode(i + 2, 0)] = ac[encode(i + 2, 1)] = ac[encode(i + 2, W + 2)] = ac[encode(i + 2, W + 3)] = 8;
	}
	for (int i = 0; i < W + 4; ++i) {
		orig_board.f[encode(0, i)] = orig_board.f[encode(1, i)] = orig_board.f[encode(H + 2, i)] = orig_board.f[encode(H + 3, i)] = true;
		ac[encode(0, i)] = ac[encode(1, i)] = ac[encode(H + 2, i)] = ac[encode(H + 3, i)] = 8;
	}
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			int pos = encode(i + 2, j + 2);
			if (!orig_board.f[pos] && ac[pos] <= 2) {
				all_ps.add(pos);
				add_ps.add(pos);
			}
		}
	}
	// Board res_board = solve();
	Board res_board = solve_iter<true>();
	vector<string> ans(H, string(W, '.'));
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			if (res_board.f[encode(i + 2, j + 2)]) {
				ans[i][j] = 'K';
			}
		}
	}
	return ans;
}


#ifdef LOCAL
int main() {
	KnightsAndPawns cl;
	int H;
	cin >> H;
	vector<string> board(H);
	for (int i = 0; i < H; ++i) {
		cin >> board[i];
	}
	vector<string> ret = cl.placeKnights(board);
	cout << ret.size() << endl;
	for (int i = 0; i < (int)ret.size(); ++i) {
		cout << ret[i] << endl;
	}
	PRINT_COUNTER();
	PRINT_TIMER();
}
#endif
