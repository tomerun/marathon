#!/bin/sh -x

for i in 0.9 1.0 1.1 1.2; do
	for j in 5.5 6.5 7.5; do
		g++48 -DINITIAL_COOLER="$i" -DFINAL_COOLER="$j" -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL -s -pipe -mmmx -msse -msse2 -msse3 KnightsAndPawns.cpp -o tester
		java Tester -b 1000 -e 1999 > log06_"$i"_"$j".txt
	done
done
