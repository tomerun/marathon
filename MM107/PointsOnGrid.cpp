#include <algorithm>
#include <utility>
#include <vector>
#include <bitset>
#include <string>
#include <queue>
#include <unordered_map>
#include <set>
#include <iostream>
#include <array>
#include <tuple>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
#define NDEBUG
#define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 8500;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 8000;
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

const int DR[] = {-1, 0, 1, 0, -1, 0, 1, 0};
const int DC[] = {0, 1, 0, -1, 0, 1, 0, -1};
const double INITIAL_COOLER = 0.8 / 256.0;
const double FINAL_COOLER = 8.0 / 256.0;
const double INITIAL_PENA_RATIO = 4.0 * 256;
const double FINAL_PENA_RATIO = 13.0 * 256;
const double PENA_RATIO_DIFF = 0.7 * 256;
const double INITIAL_SWAP_RATIO = 0.5;
const double FINAL_SWAP_RATIO = 0.8;
const int MULTI_START = 9;
double sa_cooler;
double initial_pena_ratio = INITIAL_PENA_RATIO;
double pena_ratio;
int swap_ratio;
int H, W, P, Q, KMAX, KMIN;
array<array<int, 50>, 50> grid;
vector<int> pena_count;
vector<vector<int>> pena_diff_add, pena_diff_sub;
vector<int> pena_change_add, pena_change_sub;
int optimal_score;
array<uint8_t, 1024> swap_pos;

struct Answer {
	int score;
	vector<bitset<50>> cells;

	Answer() : score(0), cells(H) {}

	void transpose() {
		int h = cells.size();
		int w = H + W - h;
		vector<bitset<50>> transposed(w);
		for (int i = 0; i < h; ++i) {
			for (int j = 0; j < w; ++j) {
				transposed[j][i] = cells[i][j];
			}
		}
		swap(cells, transposed);
	}
};

inline bool accept(int diff) {
	if (diff >= 0) return true;
	diff *= sa_cooler;
	return diff > -10 && rnd.nextUInt() < (1u << (int)(31.5 + diff));
}

void transpose() {
	for (int i = 0; i < max(H, W); ++i) {
		for (int j = 0; j < i; ++j) {
			swap(grid[i][j], grid[j][i]);
		}
	}
	swap(H, W);
	swap(P, Q);
}

bool optimal_check() {
	for (int i = 0; i <= H - P; ++i) {
		for (int j = 0; j <= W - Q; ++j) {
			int count = 0;
			for (int k = 0; k < P; ++k) {
				for (int l = 0; l < Q; ++l) {
					if (grid[i + k][j + l] != 0) count++;
				}
			}
			if (count < KMIN || KMAX < count) return false;
		}
	}
	debugStr("simple optimal\n");
	return true;
}

Answer create_initial_solution() {
	vvi sum(H, vi(Q));
	Answer ans;
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			sum[i][j % Q] += grid[i][j];
		}
		for (int j = 0; j < Q; ++j) {
			sum[i][j] = (sum[i][j] << 8) | j;
		}
		sort(sum[i].rbegin(), sum[i].rend());
	}
	vvi dp(P + 1, vi(KMAX + 1, -1));
	vvi prev(P + 1, vi(KMAX + 1));
	vi acc(Q + 1);
	dp[0][0] = 0;
	for (int i = 0; i < P; ++i) {
		for (int j = 0; j < Q; ++j) {
			acc[j + 1] = acc[j];
			for (int k = i; k < H; k += P) {
				acc[j + 1] += sum[k][j] >> 8;
			}
		}
		for (int j = 0; j <= KMAX; ++j) {
			if (dp[i][j] == -1) continue;
			for (int k = 0; k <= Q && j + k <= KMAX; ++k) {
				if (dp[i + 1][j + k] < dp[i][j] + acc[k]) {
					dp[i + 1][j + k] = dp[i][j] + acc[k];
					prev[i + 1][j + k] = k;
				}
			}
		}
	}
	int rest = KMAX;
	for (int i = P; i > 0; --i) {
		int p = prev[i][rest];
		for (int j = i - 1; j < H; j += P) {
			for (int k = 0; k < p; ++k) {
				int c = sum[j][k] & 0xFF;
				for (int l = c; l < W; l += Q) {
					ans.cells[j][l] = true;
				}
			}
		}
		rest -= p;
	}
	ans.score = dp[P][KMAX];
	debug("initial score:%d\n", dp[P][KMAX] >> 8);
	return ans;
}

void update_pena_count() {
	for (int i = 0; i < KMIN; ++i) {
		pena_count[i] = (int)((i - KMIN) * pena_ratio);
	}
	for (int i = KMAX + 1; i <= P * Q + 1; ++i) {
		pena_count[i] = (int)((KMAX - i) * pena_ratio);
	}

	for  (int i = 0; i <= P * Q; ++i) {
		pena_change_add[i] = pena_count[i + 1] - pena_count[i];
		pena_change_sub[i] = i == 0 ? 0 : pena_count[i - 1] - pena_count[i];
	}
}

template<bool difficult, int SWAP_RANGE>
Answer solve_sa(ll time_limit, const Answer& ans) {
	sa_cooler = INITIAL_COOLER;
	pena_ratio = initial_pena_ratio;
	swap_ratio = (int)(INITIAL_SWAP_RATIO * 256);
	Answer ret = ans;
	vector<bitset<50>> cur;
	for (int i = 0; i < H; ++i) {
		cur.push_back(ret.cells[i]);
	}
	vvi counts(H - P + 1, vi(W - Q + 1));
	update_pena_count();
	for (int i = 0; i < H - P + 1; ++i) {
		for (int j = 0; j < W - Q + 1; ++j) {
			for (int k = 0; k < P; ++k) {
				for (int l = 0; l < Q; ++l) {
					if (cur[i + k][j + l]) counts[i][j]++;
				}
			}
			pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
			pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
		}
	}
	int score = 0;
	int penalty = 0;
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			if (cur[i][j]) score += grid[i][j];
		}
	}
	const ll begin_time = get_elapsed_msec();
	for (int turn = 1; ; ++turn) {
		if ((turn & 0x3FFFF) == 0) {
			START_TIMER(9);
			ll elapsed = get_elapsed_msec();
			if (elapsed > time_limit) {
				debug("turn:%d score:%d initial_pena_ratio:%.2f\n", turn, ret.score >> 8, initial_pena_ratio);
				break;
			}
			// if ((turn & 0x3FFFF) == 0) {
				const double ratio = 1.0 * (elapsed - begin_time) / (time_limit - begin_time);
				double c0 = log(INITIAL_COOLER);
				double c1 = log(FINAL_COOLER);
				sa_cooler = exp(c0 * (1 - ratio) + c1 * ratio);
				c0 = log(initial_pena_ratio);
				c1 = log(FINAL_PENA_RATIO);
				pena_ratio = exp(c0 * (1 - ratio) + c1 * ratio);
				c0 = log(INITIAL_SWAP_RATIO);
				c1 = log(FINAL_SWAP_RATIO);
				swap_ratio = (int)(exp(c0 * (1 - ratio) + c1 * ratio) * 256);

				update_pena_count();
				penalty = 0;
				for (int i = 0; i < H - P + 1; ++i) {
					for (int j = 0; j < W - Q + 1; ++j) {
						pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
						pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						penalty += pena_count[counts[i][j]];
					}
				}
				// debug("turn:%d cooler:%.4f pena_ratio:%.4f penalty:%.4f score:%d\n", turn, sa_cooler, pena_ratio, penalty, score);
			// }
			STOP_TIMER(9);
		}
		int chr = rnd.nextUInt(H);
		int chc = turn % W;
		int chr2 = -1;
		int chc2 = -1;
		uint32_t rnd_v = rnd.nextUInt();
		if ((rnd_v & 0xFF) < swap_ratio) {
			START_TIMER(0);
			const int center_r = chr < SWAP_RANGE ? SWAP_RANGE : chr > H - 1 - SWAP_RANGE ? H - 1 - SWAP_RANGE : chr;
			const int center_c = chc < SWAP_RANGE ? SWAP_RANGE : chc > W - 1 - SWAP_RANGE ? W - 1 - SWAP_RANGE : chc;
			if (difficult) {
				for (int i = 0; i < 10; ++i) {
					int sp = rnd.nextUInt() & (swap_pos.size() - 1);
					int cr = center_r - SWAP_RANGE + (swap_pos[sp] >> 4);
					int cc = center_c - SWAP_RANGE + (swap_pos[sp] & 0xF);
					if (cur[chr][chc] != cur[cr][cc]) {
						chr2 = cr;
						chc2 = cc;
						break;
					}
				}
			} else {
				rnd_v >>= 8;
				int sp = rnd_v & (swap_pos.size() - 1);
				int cr = center_r - SWAP_RANGE + (swap_pos[sp] >> 4);
				int cc = center_c - SWAP_RANGE + (swap_pos[sp] & 0xF);
				if (cur[chr][chc] == cur[cr][cc]) {
					rnd_v >>= 10;
					sp = rnd_v & (swap_pos.size() - 1);
					cr = center_r - SWAP_RANGE + (swap_pos[sp] >> 4);
					cc = center_c - SWAP_RANGE + (swap_pos[sp] & 0xF);
				}
				if (cur[chr][chc] != cur[cr][cc]) {
					chr2 = cr;
					chc2 = cc;
				}
			}
			STOP_TIMER(0);
		}
		int diff_score;
		int diff_pena = 0;
		if (chr2 == -1) {
			START_TIMER(1);
			const int top = max(0, chr - P + 1);
			const int bottom = min(H - P, chr);
			const int left = max(0, chc - Q + 1);
			const int right = min(W - Q, chc);
			if (cur[chr][chc]) {
				diff_score = -grid[chr][chc];
				for (int i = top; i <= bottom; ++i) {
					diff_pena += pena_diff_sub[right + 1][i] - pena_diff_sub[left][i];
				}
			} else {
				diff_score = grid[chr][chc];
				for (int i = top; i <= bottom; ++i) {
					diff_pena += pena_diff_add[right + 1][i] - pena_diff_add[left][i];
				}
			}
			STOP_TIMER(1);
		} else {
			START_TIMER(2);
			if (chr > chr2 || (chr == chr2 && chc > chc2)) {
				swap(chr, chr2);
				swap(chc, chc2);
			}
			const int change = cur[chr][chc] ? 1 : -1;
			diff_score = (grid[chr2][chc2] - grid[chr][chc]) * change;
			int top = max(0, chr - P + 1);
			int bottom = min(chr, chr2 - P);
			{
				const auto& pena_diff = cur[chr][chc] ? pena_diff_sub : pena_diff_add;
				const int left = max(0, chc - Q + 1);
				const int right = min(W - Q, chc);
				for (int i = top; i <= bottom; ++i) {
					diff_pena += pena_diff[right + 1][i] - pena_diff[left][i];
				}
			}
			{
				top = max(0, chr2 - P + 1);
				bottom = min(chr, H - P);
				if (chc < chc2) {
					const auto& pena_diff = cur[chr][chc] ? pena_diff_sub : pena_diff_add;
					const auto& pena_diff2 = cur[chr][chc] ? pena_diff_add : pena_diff_sub;
					const int left1 = max(0, chc - Q + 1);
					const int right1 = max(left1 - 1, min(chc, chc2 - Q));
					const int right2 = min(chc2, W - Q);
					const int left2 = min(right2 + 1, max(chc + 1, chc2 - Q + 1));
					for (int i = top; i <= bottom; ++i) {
						diff_pena += pena_diff[right1 + 1][i] - pena_diff[left1][i];
						diff_pena += pena_diff2[right2 + 1][i] - pena_diff2[left2][i];
					}
				} else if (chc > chc2) {
					const auto& pena_diff = cur[chr][chc] ? pena_diff_add : pena_diff_sub;
					const auto& pena_diff2 = cur[chr][chc] ? pena_diff_sub : pena_diff_add;
					const int left1 = max(0, chc2 - Q + 1);
					const int right1 = max(left1 - 1, min(chc2, chc - Q));
					const int right2 = min(chc, W - Q);
					const int left2 = min(right2 + 1, max(chc2 + 1, chc - Q + 1));
					for (int i = top; i <= bottom; ++i) {
						diff_pena += pena_diff[right1 + 1][i] - pena_diff[left1][i];
						diff_pena += pena_diff2[right2 + 1][i] - pena_diff2[left2][i];
					}
				}
			}
			{
				top = max(chr2 - P + 1, bottom + 1);
				bottom = min(chr2, H - P);
				const auto& pena_diff = cur[chr][chc] ? pena_diff_add : pena_diff_sub;
				const int left = max(0, chc2 - Q + 1);
				const int right = min(W - Q, chc2);
				for (int i = top; i <= bottom; ++i) {
					diff_pena += pena_diff[right + 1][i] - pena_diff[left][i];
				}
			}
			STOP_TIMER(2);
		}
		ADD_COUNTER(0);
		if (accept(diff_score + diff_pena)) {
			if (chr2 == -1) {
				ADD_COUNTER(1);
				START_TIMER(3);
				const int top = max(0, chr - P + 1);
				const int bottom = min(H - P, chr);
				const int left = max(0, chc - Q + 1);
				const int right = min(W - Q, chc);
				if (cur[chr][chc]) {
					for (int i = top; i <= bottom; ++i) {
						for (int j = left; j <= right; ++j) {
							counts[i][j]--;
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
						for (int j = right + 1; j <= W - Q; ++j) {
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
					}
					cur[chr][chc] = false;
				} else {
					for (int i = top; i <= bottom; ++i) {
						for (int j = left; j <= right; ++j) {
							counts[i][j]++;
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
						for (int j = right + 1; j <= W - Q; ++j) {
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
					}
					cur[chr][chc] = true;
				}
				STOP_TIMER(3);
			} else {
				ADD_COUNTER(2);
				START_TIMER(4);
				const int change = cur[chr][chc] ? 1 : -1;
				{
					const int left = max(0, chc - Q + 1);
					const int right = min(W - Q, chc);
					for (int i = max(0, chr - P + 1); i <= min(chr, chr2 - P); ++i) {
						for (int j = left; j <= right; ++j) {
							counts[i][j] -= change;
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
						for (int j = right + 1; j <= W - Q; ++j) {
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
					}
				}
				{
					int chc_min, change1;
					if (chc < chc2) {
						chc_min = chc;
						change1 = change;
					} else {
						chc_min = chc2;
						change1 = -change;
					}
					int chc_max = chc + chc2 - chc_min;
					const int left1 = max(0, chc_min - Q + 1);
					const int right1 = min(chc_min, chc_max - Q);
					const int left2 = max(chc_min + 1, chc_max - Q + 1);
					const int right2 = min(chc_max, W - Q);
					for (int i = max(0, chr2 - P + 1); i <= min(chr, H - P); ++i) {
						for (int j = left1; j <= right1; ++j) {
							counts[i][j] -= change1;
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
						for (int j = max(0, right1 + 1); j < min(left2, W - Q + 1); ++j) {
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
						for (int j = left2; j <= right2; ++j) {
							counts[i][j] += change1;
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
						for (int j = max(0, right2 + 1); j <= W - Q; ++j) {
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
					}
				}
				{
					const int left = max(0, chc2 - Q + 1);
					const int right = min(W - Q, chc2);
					for (int i = max(chr + 1, chr2 - P + 1); i <= min(chr2, H - P); ++i) {
						for (int j = left; j <= right; ++j) {
							counts[i][j] += change;
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
						for (int j = right + 1; j <= W - Q; ++j) {
							pena_diff_add[j + 1][i] = pena_diff_add[j][i] + pena_change_add[counts[i][j]];
							pena_diff_sub[j + 1][i] = pena_diff_sub[j][i] + pena_change_sub[counts[i][j]];
						}
					}
				}
				bool tmp = cur[chr][chc];
				cur[chr].set(chc, cur[chr2][chc2]);
				cur[chr2].set(chc2, tmp);
				STOP_TIMER(4);
			}
			score += diff_score;
			penalty += diff_pena;
			// double p = 0;
			// for (int i = 0; i <= H - P; ++i) {
			// 	for (int j = 0; j <= W - Q; ++j) {
			// 		int c = 0;
			// 		for (int k = 0; k < P; ++k) {
			// 			for (int l = 0; l < Q; ++l) {
			// 				if (cur[i + k][j + l]) ++c;
			// 			}
			// 		}
			// 		p += pena_count[c];
			// 		if (counts[i][j] != c) {
			// 			debug("counts:%d %d %d %d\n", i, j, counts[i][j], c);
			// 		}
			// 		if (abs(pena_diff_add[i][j + 1] - pena_diff_add[i][j] - (pena_count[c + 1] - pena_count[c])) > 1e-4) {
			// 			debug("pena_diff_add:%d %d %f %f\n", i, j, pena_diff_add[i][j + 1] - pena_diff_add[i][j], (pena_count[c + 1] - pena_count[c]));
			// 		}
			// 		if (abs(pena_diff_sub[i][j + 1] - pena_diff_sub[i][j] - (c == 0 ? 0 : (pena_count[c - 1] - pena_count[c]))) > 1e-4) {
			// 			debug("pena_diff_sub:%d %d %f %f\n", i, j, pena_diff_sub[i][j + 1] - pena_diff_sub[i][j], c == 0 ? 0 : (pena_count[c - 1] - pena_count[c]));
			// 		}
			// 	}
			// }
			// if (abs(p - penalty) > 1e-4) {
			// 	debug("fail:%f %f\n", p, penalty);
			// 	exit(1);
			// }
			if (penalty == 0 && score > ret.score) {
				ADD_COUNTER(9);
				ret.score = score;
				for (int i = 0; i < H; ++i) {
					ret.cells[i] = cur[i];
				}
				if (ret.score == optimal_score) {
					debug("[reach optimal] turn:%d score:%d\n", turn, ret.score);
					break;
				}
			}
		}
	}
	return ret;
}

Answer solve() {
	if (optimal_check()) {
		Answer ans;
		ans.score = optimal_score;
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				if (grid[i][j] != 0) ans.cells[i][j] = true;
			}
		}
		return ans;
	}
	Answer ans_horz = create_initial_solution();
	transpose();
	Answer ans_vert = create_initial_solution();
	bool transposed = ans_horz.score < ans_vert.score;
	Answer& ans_init = transposed ? ans_vert : ans_horz;
	if (!transposed) {
		transpose();
	}

	int swap_range;
	if (min(H, W) <= 6) {
		swap_range = 2;
	} else if (min(H, W) <= 8 || KMAX - KMIN <= 7) {
		swap_range = 3;
	} else {
		swap_range = 4;
	}
	{
		int pos = 0;
		while (pos < swap_pos.size()) {
			for (int i = 0; i < swap_range * 2 + 1 && pos < swap_pos.size(); ++i) {
				for (int j = 0; j < swap_range * 2 + 1 && pos < swap_pos.size(); ++j) {
					swap_pos[pos++] =(i << 4) | j;
				}
			}
		}
	}

	pena_diff_add.assign(W - Q + 2, vector<int>(H - P + 1));
	pena_diff_sub.assign(W - Q + 2, vector<int>(H - P + 1));
	pena_change_add.resize(P * Q + 1);
	pena_change_sub.resize(P * Q + 1);
	Answer ans_sa;
	ans_sa.score = -1;
	if (ans_init.score == optimal_score) {
		debug("[reach optimal] turn:0 score:%d\n", ans_init.score >> 8);
		ans_sa = ans_init;
	} else {
		const bool difficult = KMAX - KMIN == 1;
		ll begin_time = get_elapsed_msec();
		for (int i = 0; i < MULTI_START; ++i) {
			ll time_limit = (TL - begin_time) * (i + 1) / MULTI_START + begin_time;
			Answer ans_sa_single;
			if (min(H, W) <= 6) {
				ans_sa_single = difficult ? solve_sa<true, 2>(time_limit, ans_init) : solve_sa<false, 2>(time_limit, ans_init);
			} else if (min(H, W) <= 8 || KMAX - KMIN <= 7) {
				ans_sa_single = difficult ? solve_sa<true, 3>(time_limit, ans_init) : solve_sa<false, 3>(time_limit, ans_init);
			} else {
				ans_sa_single = solve_sa<false, 4>(time_limit, ans_init);
			}
			if (ans_sa_single.score > ans_sa.score) {
				ans_sa = ans_sa_single;
				if (ans_sa_single.score == optimal_score) break;
			}
			if (ans_sa_single.score > ans_init.score) {
				initial_pena_ratio = max(0.5 * 256, initial_pena_ratio - PENA_RATIO_DIFF);
			} else {
				initial_pena_ratio += PENA_RATIO_DIFF;
			}
		}
	}
	if (transposed) {
		transpose();
		ans_sa.transpose();
	}
	return ans_sa;
}

struct PointsOnGrid {
	vector<string> findSolution(int H_, int W_, int h_, int w_, int Kmin_, int Kmax_, vector<string>& board_);
};

vector<string> PointsOnGrid::findSolution(
	int H_, int W_, int h_, int w_, int Kmin_, int Kmax_, vector<string>& board_){
	start_time = get_time();
	H = H_;
	W = W_;
	P = h_;
	Q = w_;
	KMIN = Kmin_;
	KMAX = Kmax_;
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			grid[i][j] = (board_[i][j] - '0') << 8;
			optimal_score += grid[i][j];
		}
	}
	pena_count.resize(P * Q + 2);
	Answer ans = solve();
	vector<string> ret;
	for (int i = 0; i < H; ++i) {
		string row;
		for (int j = 0; j < W; ++j) {
			row += ans.cells[i][j] ? 'x' : '.';
		}
		ret.push_back(row);
	}
	PRINT_COUNTER();
	PRINT_TIMER();
	return ret;
}

#if defined(LOCAL)
int main() {
	PointsOnGrid obj;
	int H, W, h, w, kmin, kmax;
	cin >> H >> W >> h >> w >> kmin >> kmax;
	vector<string> grid(H);
	for (int i = 0; i < H; ++i) {
		cin >> grid[i];
	}
  vector<string> ret = obj.findSolution(H, W, h, w, kmin, kmax, grid);
  for (int i = 0; i < H; ++i) {
  	cout << ret[i] << endl;
  }
}
#endif