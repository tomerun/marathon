import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.security.SecureRandom;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;


public class Tester {
	private static final int minN = 5, maxN = 50;
	private static final int minM = 2;
	private static final int minK1 = 1, minK2 = 0;
	private static final int minNum = 0, maxNum = 9;
	int H, W, P, Q, Kmin, Kmax;
	String[] numbers;

	void generate(long seed) throws Exception {
		SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
		r1.setSeed(seed);
		H = r1.nextInt(maxN - minN + 1) + minN;
		W = r1.nextInt(maxN - minN + 1) + minN;
		P = r1.nextInt((H - 1) - minM + 1) + minM;
		Q = r1.nextInt((W - 1) - minM + 1) + minM;
		Kmax = r1.nextInt((P * Q - 1) - minK1 + 1) + minK1;
		Kmin = r1.nextInt((Kmax - 1) - minK2 + 1) + minK2;
		if (seed == 1) {
			H = 8;
			W = 8;
			P = 3;
			Q = 4;
			Kmin = 0;
			Kmax = 5;
		}
		if (1000 <= seed && seed < 2000) {
			Kmax = (int) (((P * Q - 1) - minK1) * (seed - 1000) / (1999 - 1000) + minK1);
			Kmin = r1.nextInt((Kmax - 1) - minK2 + 1) + minK2;
		}
		numbers = new String[H];
		for (int i = 0; i < H; i++) {
			numbers[i] = "";
			for (int k = 0; k < W; k++)
				numbers[i] += (r1.nextInt(maxNum - minNum + 1) + minNum);
		}
	}

	int getScore() {
		int total = 0;
		for (int i = 0; i < H; ++i)
			for (int j = 0; j < W; ++j)
				if (board[i].charAt(j) == 'x')
					total += numbers[i].charAt(j) - '0';

		return total;
	}

	public Result runTest(long seed) {
		Result res = new Result();
		res.seed = seed;
		try {
			generate(seed);
			res.H = H;
			res.W = W;
			res.P = P;
			res.Q = Q;
			res.kmin = Kmin;
			res.kmax = Kmax;
			Runtime rt = Runtime.getRuntime();
			proc = rt.exec("./tester");
			Thread thread = new ErrorReader(proc.getErrorStream());
			thread.start();
			long beforeTime = System.currentTimeMillis();
			String[] grid = callSolution();
			res.elapsed = System.currentTimeMillis() - beforeTime;
			for (int r = 0; r < H; r++) {
				if (grid[r].length() != W) {
					throw new RuntimeException("Row " + r + " does not contain " + W + " elements.");
				}
				for (int c = 0; c < W; c++) {
					char a = grid[r].charAt(c);
					if (a != '.' && a != 'x') {
						throw new RuntimeException("Row " + r + " column " + c + " contains an illegal character");
					}
				}
			}

			for (int r1 = 0; r1 < H - (P - 1); r1++)
				for (int c1 = 0; c1 < W - (Q - 1); c1++) {
					int count = 0;
					for (int r2 = r1; r2 < r1 + P; r2++)
						for (int c2 = c1; c2 < c1 + Q; c2++)
							if (grid[r2].charAt(c2) == 'x') count++;

					if (count > Kmax) {
						throw new RuntimeException("Subgrid starting at (" + r1 + "," + c1 + ") contains too many painted cells");
					}
					if (count < Kmin) {
						throw new RuntimeException("Subgrid starting at (" + r1 + "," + c1 + ") contains not enough painted cells");
					}
				}

			board = grid;
			if (vis) {
				jf = new JFrame();
				v = new Vis();
				jf.getContentPane().add(v);
				jf.setSize((W + 3) * SZ + 50, H * SZ + 40);
				jf.setVisible(true);
				v.repaint();
			}
			res.score = getScore();
		} catch (Exception e) {
			e.printStackTrace();
			res.score = -1;
		} finally {
			if (proc != null) {
				proc.destroy();
			}
		}
		return res;
	}

	JFrame jf;
	Vis v;
	static String exec;
	static boolean vis;
	Process proc;
	static int SZ;
	String[] board;

	private String[] callSolution() throws IOException, NumberFormatException {
		StringBuilder sb = new StringBuilder(H + "\n" + W + "\n" + P + "\n" + Q + "\n" + Kmin + "\n" + Kmax + "\n");
		for (int i = 0; i < H; i++) {
			sb.append(numbers[i]).append("\n");
		}
		try (OutputStream os = proc.getOutputStream()) {
			os.write(sb.toString().getBytes());
			os.flush();
		}
		String[] grid = new String[H];
		try (InputStream is = proc.getInputStream();
				 BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
			for (int i = 0; i < grid.length; i++)
				grid[i] = br.readLine();
		}
		return grid;
	}

	public class Vis extends JPanel {
		public void paint(Graphics g) {
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			// background
			g.setColor(new Color(0xDDDDDD));
			g.fillRect(0, 0, W * SZ + 120, H * SZ + 40);
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, W * SZ, H * SZ);

			for (int i = 0; i < H; ++i)
				for (int j = 0; j < W; ++j) {
					if (board[i].charAt(j) == 'x') {
						g.setColor(Color.CYAN);
						g.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
					}
					g.setFont(new Font("Arial", Font.BOLD, 14));
					g.setColor(Color.BLACK);
					g.drawString("" + numbers[i].charAt(j), j * SZ + SZ / 2 - 3, i * SZ + SZ / 2 + 3);
				}

			// lines between cells
			g.setColor(Color.BLACK);
			for (int i = 0; i <= H; i++)
				g.drawLine(0, i * SZ, W * SZ, i * SZ);
			for (int i = 0; i <= W; i++)
				g.drawLine(i * SZ, 0, i * SZ, H * SZ);

			g.setFont(new Font("Arial", Font.BOLD, 14));
			g.drawString("SCORE", SZ * W + 25, 30);
			g.drawString(String.format("%d", getScore()), SZ * W + 25, 50);
		}

		public Vis() {
			jf.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
		}
	}

	private static final int THREAD_COUNT = 2;

	public static void main(String[] args) {
		long seed = 0, begin = -1, end = -1;
		SZ = 20;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
			if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
			if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
			if (args[i].equals("-exec")) exec = args[++i];
			if (args[i].equals("-vis")) vis = true;
			if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
		}
		if (seed == 1) SZ = 50;
		if (begin != -1 && end != -1) {
			BlockingQueue<Long> q = new ArrayBlockingQueue<>((int)(end - begin + 1));
			BlockingQueue<Result> receiver = new ArrayBlockingQueue<>((int)(end - begin + 1));
			for (long i = begin; i <= end; ++i) {	q.add(i);	}
			Result[] results = new Result[(int) (end - begin + 1)];
			TestThread[] threads = new TestThread[THREAD_COUNT];
			for (int i = 0; i < THREAD_COUNT; ++i) {
				threads[i] = new TestThread(q, receiver);
				threads[i].start();
			}
			int printed = 0;
			try {
				for (int i = 0; i < (int) (end - begin + 1); i++) {
					Result res = receiver.poll(160, TimeUnit.SECONDS);
					results[(int) (res.seed - begin)] = res;
					for (; printed < results.length && results[printed] != null; printed++) {
						System.out.println(results[printed]);
						System.out.println();
					}
				}
			} catch (InterruptedException e) {
				for (int i = printed; i < results.length; i++) {
					System.out.println(results[i]);
					System.out.println();
				}
				e.printStackTrace();
			}

			double sum = 0;
			for (Result result : results) {
				sum += result.score;
			}
			System.out.println("ave:" + (sum / (end - begin + 1)));
		} else {
			Tester tester = new Tester();
			Result res = tester.runTest(seed);
			System.out.println(res);
		}
	}

	private static class TestThread extends Thread {
		BlockingQueue<Result> results;
		BlockingQueue<Long> q;

		TestThread(BlockingQueue<Long> q, BlockingQueue<Result> results) {
			this.q = q;
			this.results = results;
		}

		public void run() {
			while (true) {
				Long seed = q.poll();
				if (seed == null) break;
				try {
					Tester f = new Tester();
					Result res = f.runTest(seed);
					results.add(res);
				} catch (Exception e) {
					e.printStackTrace();
					Result res = new Result();
					res.seed = seed;
					results.add(res);
				}
			}
		}
	}
}

class Result {
	long seed;
	int H, W, P, Q, kmin, kmax;
	int score;
	long elapsed;

	public String toString() {
		String ret = String.format("seed:%4d\n", seed);
		ret += String.format("H:%2d W:%2d P:%2d Q:%2d kmin:%4d kmax:%4d\n", H, W, P, Q, kmin, kmax);
		ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
		ret += String.format("score:%5d", score);
		return ret;
	}
}

class ErrorReader extends Thread {
	InputStream error;

	public ErrorReader(InputStream is) {
		error = is;
	}

	public void run() {
		try {
			byte[] ch = new byte[50000];
			int read;
			while ((read = error.read(ch)) > 0) {
				String s = new String(ch, 0, read);
				System.err.print(s);
				System.err.flush();
			}
		} catch (Exception e) {
			//
		}
	}
}
