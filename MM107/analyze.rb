#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:H, :W, :P, :Q, :kmin, :kmax, :score)

def get_scores(filename)
  scores = []
  seed = 0
  n = 0
  c = 0
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      seed = $1.to_i
      scores[seed] = TestCase.new
    elsif line =~ /H: *(\d+) W: *(\d+) P: *(\d+) Q: *(\d+) kmin: *(\d+) kmax: *(\d+)/
      scores[seed].H = $1.to_i
      scores[seed].W = $2.to_i
      scores[seed].P = $3.to_i
      scores[seed].Q = $4.to_i
      scores[seed].kmin = $5.to_i
      scores[seed].kmax = $6.to_i
    elsif line =~ /score: *([0-9]+)/
      scores[seed].score = $1.to_i
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    next unless filter.call(pair[0], pair[1])
    count += 1
    s1 = pair[0].score
    s2 = pair[1].score
    if s1 < s2
      win += 1
      sum_score_diff += 1.0 - 1.0 * s1 / s2
    elsif s1 > s2
      lose += 1
      sum_score_diff += 1.0 * s2 / s1 - 1.0
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", 1000000.0 * sum_score_diff / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  (0.0).step(0.999, 0.15) do |param|
    upper = param + 0.15
    puts "kmax/PQ : #{sprintf('%.2f', param)}-#{sprintf('%.2f', upper)}"
    calc(from, to) { |from, to| (1.0 * from.kmax / (from.P * from.Q)).between?(param, upper) }
  end
  puts "---------------------"
  (0.0).step(0.999, 0.15) do |param|
    upper = param + 0.15
    puts "PQ/HW : #{sprintf('%.2f', param)}-#{sprintf('%.2f', upper)}"
    calc(from, to) { |from, to| (1.0 * from.P * from.Q / (from.H * from.W)).between?(param, upper) }
  end
  puts "---------------------"
  (0).upto(10) do |pow|
    param = 2 ** pow
    upper = param * 2 - 1
    puts "kmax-kmin : #{sprintf('%d', param)}-#{sprintf('%d', upper)}"
    calc(from, to) { |from, to| (from.kmax - from.kmin).between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
