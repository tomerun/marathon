#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <iostream>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>

#ifndef LOCAL
#define NDEBUG
#else
#define MEASURE_TIME
#define DEBUG
#endif
#include <cassert>

using namespace std;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t i64;
typedef int64_t ll;
typedef uint64_t ull;
typedef vector<int> vi;
typedef vector<vi> vvi;

namespace {
const int DR[] = {0, 1, 0, -1};
const int DC[] = {-1, 0, 1, 0};

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.48e9;
const ll TL = 9000;
#else
#ifdef TEST
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 9000;
#else
const double CLOCK_PER_SEC = 3.6e9;
const ll TL = 9600;
#endif
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

struct Point {
	double x, y;
};

bool operator==(const Point& l, const Point& r) {
	return l.x == r.x && l.y == r.y;
}

ostream& operator<<(ostream& stm, const Point& p) {
	stm << "(" << p.x << "," << p.y << ")";
	return stm;
}

struct Rect {
	double t,b,l,r;

	Point valid_point(const Point& p) const {
		double x = p.x;
		double y = p.y;
		if (x < l) x = l;
		if (r < x) x = r;
		if (y < t) y = t;
		if (b < y) y = b;
		return {x, y};
	}
};

struct UsedTable {
	vector<vector<u64>> used;

	void resize(int d1, int d2) {
		used.resize(d1);
		for (auto& v : used) {
			v.resize((d2 + 63) / 64);
		}
	}

	void next() {
		for (auto& v : used) {
			fill(v.begin(), v.end(), 0ULL);
		}
	}

	inline bool query(int y, int x) const { return used[y][x >> 6] & (1ULL << (x & 63)); }
	inline void set(int y, int x) { used[y][x >> 6] |= (1ULL << (x & 63)); }
};

const double EPS = 1e-8;
const double BUF = 1e-3;
// const int PART = 8;
// const double CELL_SIZE = 1.0 / PART;
const int MUL_PENA = 100;
const int FLAG_VERT = 1 << 28;
const int FLAG_BACKWARD = 1 << 29;
int S,N,C;
int f[50][50];
vector<Point> locs;
vector<vvi> cell_to_loc;
vector<vector<uint16_t>> dists;
vvi dist_cands;
UsedTable dist_used_horz, dist_used_vert;
vvi path_prev_horz, path_prev_vert;
vvi dist_buf_horz, dist_buf_vert;

double dist(double x1, double y1, double x2, double y2) {
	return sqrt(sq(x2 - x1) + sq(y2 - y1));
} 

inline bool is_int(double v) {
	return abs((int)(v + 0.5) - v) <= EPS;
}

Point move_slightly_inner(Point p) {
	if (p.x == 0) p.x = EPS;
	if (p.x == S) p.x -= EPS;
	if (p.y == 0) p.y = EPS;
	if (p.y == S) p.y -= EPS;
	return p;
}

double quantize(double d) {
	int BACKET = 999;
	int coord = (int)(d * BACKET + 0.5);
	double ret = coord * 1.0 / BACKET;
	if (coord % BACKET == 0) {
		return ret + 1.0 / BACKET * ((d - (int)d < 0.5) ? 1 : -1);
	} else {
		return ret;
	}
}

Point quantize(const Point& p) {
	return {quantize(p.x), quantize(p.y)};
}

template <int PART_BITS>
void calc_dists() {
	constexpr int PART = 1 << PART_BITS;
	constexpr double CELL_SIZE = 1.0 / PART;
	constexpr int PART_MASK = 0x3F ^ (PART - 1);
	constexpr uint16_t INF = 0xFFFF;
	for (int i = 0; i < 2 * N + 1; ++i) {
		dists.emplace_back(2 * N + 1, INF);
	}
	dist_cands.resize(0x11000);
	dist_used_horz.resize(S + 1, S * PART);
	dist_used_vert.resize(S + 1, S * PART);
	array<array<double, PART>, PART> dist_table;
	array<double, PART> dist_table2;
	for (int i = 0; i < PART; ++i) {
		for (int j = 0; j < PART; ++j) {
			dist_table[i][j] = dist(0, 0, 2 * i + 1, 2 * j + 1) * CELL_SIZE / 2;
		}
		dist_table2[i] = dist(0, 0, i, PART) * CELL_SIZE;
	}

	for (int i = 0; i <= 2 * N; ++i) {
		dist_used_horz.next();
		dist_used_vert.next();
		int count = 0;
		int add_count = 0;
		if (i != 2 * N) {
			const int sy = (int)locs[i].y;
			const int sx = (int)locs[i].x;
			// debug("i:%d %d %d\n", i, sy, sx);
			const double st = f[sy][sx] == 0 ? 0.0001 : f[sy][sx];
			for (int loci : cell_to_loc[sy][sx]) {
				dists[i][loci] = (uint16_t)ceil(st * dist(locs[i].x, locs[i].y, locs[loci].x, locs[loci].y) * MUL_PENA);
				++count;
			}
			for (int coord = 0; coord < PART; ++coord) {
				if (sy != 0) {
					const double d = dist(locs[i].x, locs[i].y, (sx * PART + coord + 0.5) * CELL_SIZE, sy);
					const int nt = f[sy - 1][sx];
					const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
					dist_cands[nd].push_back(FLAG_BACKWARD | (sy << 16) | (sx * PART + coord));
					++add_count;
				}
				if (sy != S - 1) {
					const double d = dist(locs[i].x, locs[i].y, (sx * PART + coord + 0.5) * CELL_SIZE, sy + 1);
					const int nt = f[sy + 1][sx];
					const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
					dist_cands[nd].push_back(((sy + 1) << 16) | (sx * PART + coord));
					++add_count;
				}
				if (sx != 0) {
					const double d = dist(locs[i].x, locs[i].y, sx, (sy * PART + coord + 0.5) * CELL_SIZE);
					const int nt = f[sy][sx - 1];
					const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
					dist_cands[nd].push_back(FLAG_BACKWARD | FLAG_VERT | ((sy * PART + coord) << 16) | sx);
					++add_count;
				}
				if (sx != S - 1) {
					const double d = dist(locs[i].x, locs[i].y, sx + 1, (sy * PART + coord + 0.5) * CELL_SIZE);
					const int nt = f[sy][sx + 1];
					const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
					dist_cands[nd].push_back(FLAG_VERT | ((sy * PART + coord) << 16) | (sx + 1));
					++add_count;
				}
			}
		} else {
			for (int coord = 0; coord < S * PART; ++coord) {
				dist_cands[0].push_back((0 << 16) | coord);
				dist_cands[0].push_back(FLAG_BACKWARD | (S << 16) | coord);
				dist_cands[0].push_back(FLAG_VERT | (coord << 16) | 0);
				dist_cands[0].push_back(FLAG_BACKWARD | FLAG_VERT | (coord << 16) | S);
			}
			add_count += S * PART * 4;
		}
		const int search_count = i == 2 * N ? 2 * N : min(200, 2 * N);
		int stop_pena = count == search_count ? 0 : 0xFFFF;
		for (int j = 0; j < stop_pena; ++j) {
			for (int pi = 0; pi < dist_cands[j].size(); ++pi) {
				const int p = dist_cands[j][pi];
				const int cy = (p >> 16) & 0xFFF;
				const int cx = p & 0xFFFF;
				double coordY, coordX;
				int nCellY, nCellX;
				if (p & FLAG_VERT) {
					if (dist_used_vert.query(cx, cy)) continue;
					dist_used_vert.set(cx, cy);
					coordY = (cy + 0.5) * CELL_SIZE;
					coordX = cx;
					nCellY = cy / PART;
					nCellX = cx - ((p & FLAG_BACKWARD) ? 1 : 0);
				} else {
					if (dist_used_horz.query(cy, cx)) continue;
					dist_used_horz.set(cy, cx);
					coordX = (cx + 0.5) * CELL_SIZE;
					coordY = cy;
					nCellX = cx / PART;
					nCellY = cy - ((p & FLAG_BACKWARD) ? 1 : 0);
				}
				const double ct = f[nCellY][nCellX] == 0 ? 0.0001 : f[nCellY][nCellX];
				// debug("j:%d FLAG_VERT:%d FLAG_BACKWARD:%d cy:%d cx:%d nCellY:%d nCellX:%d\n", j, !!(p & FLAG_VERT), !!(p & FLAG_BACKWARD), cy, cx, nCellY, nCellX);
				for (int loci : cell_to_loc[nCellY][nCellX]) {
					// debug("loci:%d dist:%d\n", loci, dists[i][loci]);
					if (dists[i][loci] == INF) {
						++count;
						if (count == search_count) {
							stop_pena = min(stop_pena, j + MUL_PENA * 2);
						}
					}
					dists[i][loci] = min((int)dists[i][loci], j + (int)ceil(ct * dist(coordX, coordY, locs[loci].x, locs[loci].y) * MUL_PENA));
				}
				if (p & FLAG_VERT) {
					if (nCellY != 0) { // top
						if (!((cy % PART != 0) && (dist_used_vert.query(cx, cy - 1)))) {
							int used = dist_used_horz.used[nCellY][nCellX >> PART_BITS] >> ((nCellX * PART) & PART_MASK);
							used = (~used) & 0xFF;
							while (used != 0) {
								int coord = __builtin_ctz(used);
								const double d = dist_table[(p & FLAG_BACKWARD) ? PART - 1 - coord : coord][cy - nCellY * PART];
								const int nt = f[nCellY - 1][nCellX];
								const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
								dist_cands[nd].push_back(FLAG_BACKWARD | ((nCellY) << 16) | (nCellX * PART + coord));
								++add_count;
								used &= (used - 1);
							}
						}
					}
					if (nCellY != S - 1) { // bottom
						if (!((cy % PART != PART - 1) && (dist_used_vert.query(cx, cy + 1)))) {
							int used = dist_used_horz.used[nCellY + 1][nCellX >> PART_BITS] >> ((nCellX * PART) & PART_MASK);
							used = (~used) & 0xFF;
							while (used != 0) {
								int coord = __builtin_ctz(used);
								const double d = dist_table[(p & FLAG_BACKWARD) ? PART - 1 - coord : coord][(nCellY + 1) * PART - cy - 1];
								const int nt = f[nCellY + 1][nCellX];
								const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
								dist_cands[nd].push_back(((nCellY + 1) << 16) | (nCellX * PART + coord));
								++add_count;
								used &= (used - 1);
							}
						}
					}
					if (nCellX != 0 && (p & FLAG_BACKWARD)) { // left
						int used = dist_used_vert.used[nCellX][nCellY >> PART_BITS] >> ((nCellY * PART) & PART_MASK);
						used = (~used) & 0xFF;
						while (used != 0) {
							int coord = __builtin_ctz(used);
							const double d = dist_table2[abs((cy - nCellY * PART) - coord)];
							const int nt = f[nCellY][nCellX - 1];
							const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
							dist_cands[nd].push_back(FLAG_BACKWARD | FLAG_VERT | ((nCellY * PART + coord) << 16) | (nCellX));
							++add_count;
							used &= (used - 1);
						}
					}
					if (nCellX != S - 1 && !(p & FLAG_BACKWARD)) { // right
						int used = dist_used_vert.used[nCellX + 1][nCellY >> PART_BITS] >> ((nCellY * PART) & PART_MASK);
						used = (~used) & 0xFF;
						while (used != 0) {
							int coord = __builtin_ctz(used);
							const double d = dist_table2[abs((cy - nCellY * PART) - coord)];
							const int nt = f[nCellY][nCellX + 1];
							const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
							dist_cands[nd].push_back(FLAG_VERT | ((nCellY * PART + coord) << 16) | (nCellX + 1));
							++add_count;
							used &= (used - 1);
						}
					}
				} else {
					if (nCellY != 0 && (p & FLAG_BACKWARD)) { // top
						int used = dist_used_horz.used[nCellY][nCellX >> PART_BITS] >> ((nCellX * PART) & PART_MASK);
						used = (~used) & 0xFF;
						while (used != 0) {
							int coord = __builtin_ctz(used);
							const double d = dist_table2[abs((cx - nCellX * PART) - coord)];
							const int nt = f[nCellY - 1][nCellX];
							const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
							dist_cands[nd].push_back(FLAG_BACKWARD | ((nCellY) << 16) | (nCellX * PART + coord));
							++add_count;
							used &= (used - 1);
						}
					}
					if (nCellY != S - 1 && !(p & FLAG_BACKWARD)) { // bottom
						int used = dist_used_horz.used[nCellY + 1][nCellX >> PART_BITS] >> ((nCellX * PART) & PART_MASK);
						used = (~used) & 0xFF;
						while (used != 0) {
							int coord = __builtin_ctz(used);
							const double d = dist_table2[abs((cx - nCellX * PART) - coord)];
							const int nt = f[nCellY + 1][nCellX];
							const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
							dist_cands[nd].push_back(((nCellY + 1) << 16) | (nCellX * PART + coord));
							++add_count;
							used &= (used - 1);
						}
					}
					if (nCellX != 0) { // left
						if (!((cx % PART != 0) && (dist_used_horz.query(cy, cx - 1)))) {
							int used = dist_used_vert.used[nCellX][nCellY >> PART_BITS] >> ((nCellY * PART) & PART_MASK);
							used = (~used) & 0xFF;
							while (used != 0) {
								int coord = __builtin_ctz(used);
								const double d = dist_table[(p & FLAG_BACKWARD) ? PART - 1 - coord : coord][cx - nCellX * PART];
								const int nt = f[nCellY][nCellX - 1];
								const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
								dist_cands[nd].push_back(FLAG_BACKWARD | FLAG_VERT | ((nCellY * PART + coord) << 16) | (nCellX));
								++add_count;
								used &= (used - 1);
							}
						}
					}
					if (nCellX != S - 1) { // right
						if (!((cx % PART != PART - 1) && (dist_used_horz.query(cy, cx + 1)))) {
							int used = dist_used_vert.used[nCellX + 1][nCellY >> PART_BITS] >> ((nCellY * PART) & PART_MASK);
							used = (~used) & 0xFF;
							while (used != 0) {
								int coord = __builtin_ctz(used);
								const double d = dist_table[(p & FLAG_BACKWARD) ? PART - 1 - coord : coord][(nCellX + 1) * PART - cx - 1];
								const int nt = f[nCellY][nCellX + 1];
								const int nd = j + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
								dist_cands[nd].push_back(FLAG_VERT | ((nCellY * PART + coord) << 16) | (nCellX + 1));
								++add_count;
								used &= (used - 1);
							}
						}
					}
				}
			}
		}
		{
			int clear_count = 0;
			for (int k = 0; clear_count < add_count; ++k) {
				clear_count += dist_cands[k].size();
				dist_cands[k].clear();
			}
			// debug("max_dist:%d sum:%d\n", max_dist, sum);
		}
	}
	for (int i = 0; i < 2 * N; ++i) {
		dists[i][2 * N] = dists[2 * N][i];
		if (dists[i][2 * N] < MUL_PENA && f[(int)locs[i].y][(int)locs[i].x] == 0) {
			dists[i][2 * N] = dists[2 * N][i] = 0;
		}
	}
	START_TIMER(9);
	for (int i = 0; i < 2 * N; ++i) {
		for (int j = 0; j < 2 * N; ++j) {
			for (int k = 0; k < 2 * N; ++k) {
				dists[j][k] = (uint16_t)min((int)dists[j][k], dists[j][i] + dists[i][k]);
			}
		}
	}
	for (int i = 0; i < 2 * N; ++i) {
		for (int j = 0; j < 2 * N; ++j) {
			dists[i][j] = dists[j][i] = min(dists[i][j], dists[j][i]);
			if (dists[i][j] < MUL_PENA && f[(int)locs[i].y][(int)locs[i].x] == 0 && f[(int)locs[j].y][(int)locs[j].x] == 0) {
				dists[i][j] = dists[j][i] = 0;
			}
		}
	}
	STOP_TIMER(9);
	// for (int i = 0; i <= 2 * N; ++i) {
	// 	for (int j = 0; j <= 2 * N; ++j) {
	// 		debug("%3d ", dists[i][j]);
	// 	}
	// 	debugln();
	// }
}

vector<bool> dfs_used;
i64 dfs_best_score;
i64 dfs_count;
vi dfs_best_path;
bool dfs_timeout;
vvi dfs_order;
vi dfs_min_dist;
i64 dfs_rest_lowerbound;
i64 dfs_timelimit;

template<int SEARCH_COUNT>
void tsp_rec(vi& path, i64 score, int carry) {
	if (dfs_timeout) return;
	if (path.size() == 2 * N + 1) {
		if (score + dists[path.back()][2 * N] < dfs_best_score) {
			dfs_best_score = score + dists[path.back()][2 * N];
			dfs_best_path = path;
			debug("dfs_best_score:%lld at turn %lld\n", dfs_best_score, dfs_count);
		}
		return;
	}
	if ((dfs_count & 0xFFFF) == 0) {
		dfs_timeout = get_elapsed_msec() > dfs_timelimit;
		if (dfs_timeout) debugStr("dfs_timeout\n");
	}
	++dfs_count;
	int cur_count = 0;
	for (int i = 0; i < dfs_order[path.back()].size() && (SEARCH_COUNT == 0 || path.size() == 1 || cur_count < SEARCH_COUNT); ++i) {
		if (!dfs_timeout && path.size() <= 1) debug("%lu %d\n", path.size(), i);
		const int ni = dfs_order[path.back()][i];
		if (dfs_used[ni]) continue;
		if (carry == 0 && ni >= N) continue;
		if (carry == C && ni < N) continue;
		path.push_back(ni);
		dfs_used[ni] = true;
		dfs_rest_lowerbound -= dfs_min_dist[ni];
		const i64 next_score = score + dists[path[path.size() - 2]][ni];
		if (next_score + dfs_rest_lowerbound < dfs_best_score) {
			++cur_count;
			tsp_rec<SEARCH_COUNT>(path, next_score, carry + (ni < N ? 1 : -1));
		}
		dfs_rest_lowerbound += dfs_min_dist[ni];
		dfs_used[ni] = false;
		path.pop_back();
	}
}

vector<int> tsp_DFS() {
	dfs_used.resize(2 * N);
	dfs_best_score = 1 << 30;
	dfs_count = 1;
	dfs_timeout = false;
	dfs_order.resize(2 * N + 1);
	dfs_rest_lowerbound = 0;
	for (int i = 0; i <= 2 * N; ++i) {
		dfs_order[i].reserve(2 * N);
		for (int j = 0; j < 2 * N; ++j) {
			if (j != i) dfs_order[i].push_back(j);
		}
		sort(dfs_order[i].begin(), dfs_order[i].end(), [i](int l, int r){
			return dists[i][l] < dists[i][r];
		});
		dfs_min_dist.push_back(dists[i][dfs_order[i][0]]);
		dfs_rest_lowerbound += dfs_min_dist.back();
	}
	debug("dfs_rest_lowerbound:%lld\n", dfs_rest_lowerbound);
	vi path = {2 * N};
	if (N <= 11) {
		tsp_rec<0>(path, 0, 0);
	} else if (N <= 18) {
		tsp_rec<2>(path, 0, 0);
	} else {
		tsp_rec<2>(path, 0, 0);
	}
	dfs_best_path.push_back(2 * N);
	debug("dfs_best_score:%lld dfs_count:%lld\n", dfs_best_score, dfs_count);
	return dfs_best_path;
}

double HEAT_INV;
double diff_cutoff;

inline bool transit(i64 diff) {
	if (diff > diff_cutoff) return false;
	if (diff <= 0) return true;
	return rnd.nextDouble() < exp(-diff * HEAT_INV);
}

template<bool C_is_1>
vector<int> tsp_SA(const vector<int>& initial) {
	auto ret = initial;
	auto cur_path = initial;
	auto best_sum = dfs_best_score;
	auto sum = dfs_best_score;
	i64 last_update_turn = 0;
	i64 last_best_turn = 0;
	double ave_pena = 1.0 * best_sum / (2 * N + 2);
	HEAT_INV = 1.0 / (ave_pena / 3);
	const double DIFF_CUTOFF_COEF = 10;
	diff_cutoff = DIFF_CUTOFF_COEF / HEAT_INV;
	debug("initital sum %lld %.5f\n", best_sum, ave_pena);
	debug("HEAT_INV:%.6f at turn %lld\n", HEAT_INV, 0ll);
	vector<int> carries(2 * N + 2);
	for (int i = 1; i < 2 * N + 1; ++i) {
		carries[i] = carries[i-1] + (initial[i] < N ? 1 : -1);
	}

	for (i64 turn = 0; ; ++turn) {
		if ((turn & 0x3FFFF) == 0) {
			if ((turn & 0xFFFFF) == 0 && get_elapsed_msec() > TL - 1200) {
				if (HEAT_INV > 1000) {
					debug("best sum %lld after turn %lld\n", best_sum, turn);
					break;
				} else {
					HEAT_INV = 10000;
					diff_cutoff = DIFF_CUTOFF_COEF / HEAT_INV;
					debug("HEAT_INV:infinity at turn %lld\n", turn);
				}
			}
			if (last_best_turn + 100000000 < turn && HEAT_INV < 10000) {
				debug("purturb at turn:%lld score:%lld\n", turn, sum);
				last_update_turn = last_best_turn = turn;
				HEAT_INV = 1.0 / (ave_pena / 3);
				diff_cutoff = DIFF_CUTOFF_COEF / HEAT_INV;
				for (int i = 0; i < N; ++i) {
					int p1 = rnd.nextUInt(2 * N) + 1;
					int p2 = rnd.nextUInt(2 * N - 1) + 1;
					if (p1 <= p2) ++p2;
					if ((cur_path[p1] < N) != (cur_path[p2] < N)) {
						--i;
						continue;
					}
					swap(cur_path[p1], cur_path[p2]);
				}
				sum = dists[2 * N][cur_path[1]];
				for (int i = 1; i <= 2 * N; ++i) {
					carries[i] = carries[i - 1] + (cur_path[i] < N ? 1 : -1);
					sum += dists[cur_path[i]][cur_path[i + 1]];
				}
			}
			if (last_update_turn + 0x7FFFF < turn) {
				HEAT_INV *= 1.007;
				diff_cutoff = DIFF_CUTOFF_COEF / HEAT_INV;
				last_update_turn = turn;
				debug("HEAT_INV:%.6f at turn %lld\n", HEAT_INV, turn);
			}
		}
		int p1, p2;
		auto rp = rnd.nextUInt();
		if (C_is_1) {
			p1 = (rp & 0xFFFF) % N;
			p2 = (rp >> 16) % (N - 1);
			if (p1 <= p2) {
				p2 += 2;
			}
			p1 = p1 * 2 + 1;
			p2 = p2 * 2;
		} else {
			p1 = (rp & 0xFFFF) % (2 * N + 1);
			p2 = (rp >> 16)  % (2 * N + 1 - 3);
			if (p1 - 1 <= p2) {
				p2 += 3;
			}
		}

		i64 diff = - dists[cur_path[p1]][cur_path[p1 + 1]]
		           + dists[cur_path[p1]][cur_path[p2]]
		           + dists[cur_path[p2 + 1]][cur_path[p1 + 1]]
		           - dists[cur_path[p2 + 1]][cur_path[p2]];
		if (transit(diff)) {
			ADD_COUNTER(0);
			if (p2 < p1) {
				swap(p1, p2);
			}
			bool ok = true;
			if (!C_is_1 && carries[p1] + carries[p2] != C) {
				int cur_carry = carries[p1];
				for (int i = p2; i > p1; --i) {
					cur_carry += (cur_path[i] < N) ? 1 : -1;
					if (cur_carry == -1 || cur_carry == C + 1) {
						ok = false;
						break;
					}
				}
			}
			if (ok) {
				ADD_COUNTER(1);
				sum += diff;
				reverse(cur_path.begin() + p1 + 1, cur_path.begin() + p2 + 1);
				if (!C_is_1) {
					for (int i = p1 + 1; i <= p2; ++i) {
						carries[i] = carries[i-1] + (cur_path[i] < N ? 1 : -1);
					}
				}
				if (sum < best_sum) {
					ret = cur_path;
					best_sum = sum;
					last_update_turn = last_best_turn = turn;
					debug("best sum %lld at turn %lld\n", best_sum, turn);
					if (best_sum == 0) {
						break;
					}
				}
			}
		}
	}
	PRINT_COUNTER();
	return ret;
}

vector<int> tsp() {
	if (N < 12) {
		dfs_timelimit = TL - 2050;
	} else {
		dfs_timelimit = min(TL - 2000, get_elapsed_msec() + 5);
	}
	auto ret = tsp_DFS();
	if (!dfs_timeout) return ret;
	return C == 1 ? tsp_SA<true>(ret) : tsp_SA<false>(ret);
}

vector<Point> create_zero_path(int loc1, int loc2) {
	double cy = locs[loc1].y;
	double cx = locs[loc1].x;
	const int ey = (int)(locs[loc2].y);
	const int ex = (int)(locs[loc2].x);
	if (f[(int)cy][(int)cx] != 0 || f[ey][ex] != 0) return {};
	vector<Point> ret = {locs[loc1]};
	while ((int)cx != ex || (int)cy != ey) {
		if ((int)cx < ex) {
			cx = floor(cx + 1);
			cy = floor(cy) + 0.5;
		} else if (ex < (int)cx) {
			cx = ceil(cx - 1);
			cy = floor(cy) + 0.5;
		} else if ((int)cy < ey) {
			cx = floor(cx) + 0.5;
			cy = floor(cy + 1);
		} else {
			assert(ey < (int)cy);
			cx = floor(cx) + 0.5;
			cy = ceil(cy - 1);
		}
		if (f[(int)cy][(int)cx] != 0) return {};
		ret.push_back({cx, cy});
	}
	ret.push_back(locs[loc2]);
	return ret;
}

template <int PART_BITS>
vector<Point> create_path(int loc1, int loc2) {
	const int PART = 1 << PART_BITS;
	const double CELL_SIZE = 1.0 / PART;
	const int FOUND = 0xFFFFFFFE;
	const int sy = (int)(locs[loc1].y);
	const int sx = (int)(locs[loc1].x);
	const int ey = loc2 == 2 * N ? -1 : (int)(locs[loc2].y);
	const int ex = loc2 == 2 * N ? -1 : (int)(locs[loc2].x);
	// debug("sy:%d sx:%d ey:%d ex:%d\n", sy, sx, ey, ex);
	if (sy == ey && sx == ex) {
		return {locs[loc1], locs[loc2]};
	}
	if (loc2 == 2 * N) {
		if (sy == 0) {
			return {locs[loc1], {locs[loc1].x, 0.0}};
		} else if (sy == S - 1) {
			return {locs[loc1], {locs[loc1].x, (double)S}};
		} else if (sx == 0) {
			return {locs[loc1], {0.0, locs[loc1].y}};
		} else if (sx == S - 1) {
			return {locs[loc1], {(double)S, locs[loc1].y}};
		}
	}
	if (dists[loc1][loc2] == 0 && loc2 != 2 * N) {
		vector<Point> ret = create_zero_path(loc1, loc2);
		if (!ret.empty()) {
			return ret;
		}
	}
	dist_used_horz.next();
	dist_used_vert.next();
	int add_count = 0;
	const double st = f[sy][sx] == 0 ? 0.0001 : f[sy][sx];
	for (int coord = 0; coord < PART; ++coord) {
		if (sy != 0) {
			const double d = dist(locs[loc1].x, locs[loc1].y, (sx * PART + coord + 0.5) * CELL_SIZE, sy);
			const int nt = f[sy - 1][sx];
			const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
			dist_cands[nd].push_back(FLAG_BACKWARD | (sy << 16) | (sx * PART + coord));
			++add_count;
			dist_buf_horz[sy][sx * PART + coord] = nd;
			path_prev_horz[sy][sx * PART + coord] = FOUND;
			dist_used_horz.set(sy, sx * PART + coord);
		}
		if (sy != S - 1) {
			const double d = dist(locs[loc1].x, locs[loc1].y, (sx * PART + coord + 0.5) * CELL_SIZE, sy + 1);
			const int nt = f[sy + 1][sx];
			const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
			dist_cands[nd].push_back(((sy + 1) << 16) | (sx * PART + coord));
			++add_count;
			dist_buf_horz[sy + 1][sx * PART + coord] = nd;
			path_prev_horz[sy + 1][sx * PART + coord] = FOUND;
			dist_used_horz.set(sy + 1, sx * PART + coord);
		}
		if (sx != 0) {
			const double d = dist(locs[loc1].x, locs[loc1].y, sx, (sy * PART + coord + 0.5) * CELL_SIZE);
			const int nt = f[sy][sx - 1];
			const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
			dist_cands[nd].push_back(FLAG_BACKWARD | FLAG_VERT | ((sy * PART + coord) << 16) | sx);
			++add_count;
			dist_buf_vert[sx][sy * PART + coord] = nd;
			path_prev_vert[sx][sy * PART + coord] = FOUND;
			dist_used_vert.set(sx, sy * PART + coord);
		}
		if (sx != S - 1) {
			const double d = dist(locs[loc1].x, locs[loc1].y, sx + 1, (sy * PART + coord + 0.5) * CELL_SIZE);
			const int nt = f[sy][sx + 1];
			const int nd = (int)ceil((st * d + sq(nt - st)) * MUL_PENA);
			dist_cands[nd].push_back(FLAG_VERT | ((sy * PART + coord) << 16) | (sx + 1));
			++add_count;
			dist_buf_vert[sx + 1][sy * PART + coord] = nd;
			path_prev_vert[sx + 1][sy * PART + coord] = FOUND;
			dist_used_vert.set(sx + 1, sy * PART + coord);
		}
	}

	int goal_prev = -1;
	int goal_pena = 1 << 28;
	for (int pena = 0; pena < goal_pena; ++pena) {
		// if (pena % 1000 == 0) debug("p:%d\n", pena);
		for (int pi = 0; pi < dist_cands[pena].size(); ++pi) {
			const int p = dist_cands[pena][pi];
			const int cy = (p >> 16) & 0xFFF;
			const int cx = p & 0xFFFF;
			double coordY, coordX;
			int nCellY, nCellX;
			if (p & FLAG_VERT) {
				assert(dist_used_vert.query(cx, cy));
				if (dist_buf_vert[cx][cy] > pena) continue;
				coordY = (cy + 0.5) * CELL_SIZE;
				coordX = cx;
				nCellY = cy / PART;
				nCellX = cx - ((p & FLAG_BACKWARD) ? 1 : 0);
			} else {
				assert(dist_used_horz.query(cy, cx));
				if (dist_buf_horz[cy][cx] > pena) continue;
				coordX = (cx + 0.5) * CELL_SIZE;
				coordY = cy;
				nCellX = cx / PART;
				nCellY = cy - ((p & FLAG_BACKWARD) ? 1 : 0);
			}
			// debug("pena:%d %d %d %d %d %d %d\n", pena, cy, cx, nCellY, nCellX, !!(p & FLAG_BACKWARD), !!(p & FLAG_VERT));
			// found?
			if (loc2 == 2 * N && (coordX == 0 || coordX == S || coordY == 0 || coordY == S)) {
				goal_prev = p;
				goal_pena = pena;
				break;
			}
			const double ct = f[nCellY][nCellX] == 0 ? 0.0001 : f[nCellY][nCellX];
			if (nCellY == ey && nCellX == ex) {
				int gpena = pena + (int)ceil(dist(coordX, coordY, locs[loc2].x, locs[loc2].y) * ct * MUL_PENA);
				if (gpena < goal_pena) {
					goal_prev = p;
					goal_pena = gpena;
				}
			}
			if ((nCellY != 0 || loc2 == 2 * N) && ((p & FLAG_VERT) || (p & FLAG_BACKWARD))) { // top
				for (int coord = 0; coord < PART; ++coord) {
					const double d = dist(coordX, coordY, (nCellX * PART + coord + 0.5) * CELL_SIZE, nCellY);
					const int nt = nCellY == 0 ? ct : f[nCellY - 1][nCellX];
					const int nd = pena + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
					if (dist_used_horz.query(nCellY, nCellX * PART + coord) && dist_buf_horz[nCellY][nCellX * PART + coord] <= nd) continue;
					dist_used_horz.set(nCellY, nCellX * PART + coord);
					dist_buf_horz[nCellY][nCellX * PART + coord] = nd;
					path_prev_horz[nCellY][nCellX * PART + coord] = p;
					dist_cands[nd].push_back(FLAG_BACKWARD | ((nCellY) << 16) | (nCellX * PART + coord));
					++add_count;
					// debug("add top %d %d %d\n", nd, nCellY, nCellX * PART + coord);
				}
			}
			if ((nCellY != S - 1 || loc2 == 2 * N) && ((p & FLAG_VERT) || !(p & FLAG_BACKWARD))) { // bottom
				for (int coord = 0; coord < PART; ++coord) {
					const double d = dist(coordX, coordY, (nCellX * PART + coord + 0.5) * CELL_SIZE, nCellY + 1);
					const int nt = nCellY == S - 1 ? ct : f[nCellY + 1][nCellX];
					const int nd = pena + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
					if (dist_used_horz.query(nCellY + 1, nCellX * PART + coord) && dist_buf_horz[nCellY + 1][nCellX * PART + coord] <= nd) continue;
					dist_used_horz.set(nCellY + 1, nCellX * PART + coord);
					dist_buf_horz[nCellY + 1][nCellX * PART + coord] = nd;
					path_prev_horz[nCellY + 1][nCellX * PART + coord] = p;
					dist_cands[nd].push_back(((nCellY + 1) << 16) | (nCellX * PART + coord));
					++add_count;
					// debug("add bottom %d %d %d\n", nd, nCellY + 1, nCellX * PART + coord);
				}
			}
			if ((nCellX != 0 || loc2 == 2 * N) && (!(p & FLAG_VERT) || (p & FLAG_BACKWARD))) { // left
				for (int coord = 0; coord < PART; ++coord) {
					const double d = dist(coordX, coordY, nCellX, (nCellY * PART + coord + 0.5) * CELL_SIZE);
					const int nt = nCellX == 0 ? ct : f[nCellY][nCellX - 1];
					const int nd = pena + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
					if (dist_used_vert.query(nCellX, nCellY * PART + coord) && dist_buf_vert[nCellX][nCellY * PART + coord] <= nd) continue;
					dist_used_vert.set(nCellX, nCellY * PART + coord);
					dist_buf_vert[nCellX][nCellY * PART + coord] = nd;
					path_prev_vert[nCellX][nCellY * PART + coord] = p;
					dist_cands[nd].push_back(FLAG_BACKWARD | FLAG_VERT | ((nCellY * PART + coord) << 16) | (nCellX));
					++add_count;
					// debug("add left %d %d %d\n", nd, nCellY * PART + coord, nCellX);
				}
			}
			if ((nCellX != S - 1 || loc2 == 2 * N) && (!(p & FLAG_VERT) || !(p & FLAG_BACKWARD))) { // right
				for (int coord = 0; coord < PART; ++coord) {
					const double d = dist(coordX, coordY, nCellX + 1, (nCellY * PART + coord + 0.5) * CELL_SIZE);
					const int nt = nCellX == S - 1 ? ct : f[nCellY][nCellX + 1];
					const int nd = pena + (int)ceil((ct * d + sq(nt - ct)) * MUL_PENA);
					if (dist_used_vert.query(nCellX + 1, nCellY * PART + coord) && dist_buf_vert[nCellX + 1][nCellY * PART + coord] <= nd) continue;
					dist_used_vert.set(nCellX + 1, nCellY * PART + coord);
					dist_buf_vert[nCellX + 1][nCellY * PART + coord] = nd;
					path_prev_vert[nCellX + 1][nCellY * PART + coord] = p;
					dist_cands[nd].push_back(FLAG_VERT | ((nCellY * PART + coord) << 16) | (nCellX + 1));
					++add_count;
					// debug("add right %d %d %d\n", nd, nCellY * PART + coord, nCellX + 1);
				}
			}
		}
	}
	int clear_count = 0;
	for (int k = 0; clear_count < add_count; ++k) {
		clear_count += dist_cands[k].size();
		dist_cands[k].clear();
	}

	// retrieve
	assert(goal_prev != -1);
	vector<Point> ret;
	if (loc2 != 2 * N) {
		ret.push_back(locs[loc2]);
	}
	int pos = goal_prev;
	while (pos != FOUND) {
		const int cy = (pos >> 16) & 0xFFF;
		const int cx = pos & 0xFFFF;
		double coordY, coordX;
		if (pos & FLAG_VERT) {
			coordY = (cy + 0.5) * CELL_SIZE;
			coordX = cx;
			pos = path_prev_vert[cx][cy];
		} else {
			coordX = (cx + 0.5) * CELL_SIZE;
			coordY = cy;
			pos = path_prev_horz[cy][cx];
		}
		ret.push_back({coordX, coordY});
	}
	ret.push_back(locs[loc1]);
	reverse(ret.begin(), ret.end());
	return ret;
}

template<int PART_BIT>
vector<Point> create_whole_path(const vector<int>& loc_path) {
	assert(loc_path[0] == 2 * N);
	assert(loc_path.back() == 2 * N);
	assert(loc_path.size() == 2 * N + 2);

	auto ret = create_path<PART_BIT>(loc_path[1], loc_path[0]);
	reverse(ret.begin(), ret.end());
	for (int i = 1; i < loc_path.size() - 1; ++i) {
		auto path = create_path<PART_BIT>(loc_path[i], loc_path[i + 1]);
		assert(ret.back() == path[0]);
		ret.insert(ret.end(), path.begin() + 1, path.end());
	}
	// for (auto& p : ret) {
	// 	debug("%.9f %.9f\n", p.x, p.y);
	// }
	// debugln();
	return ret;
}

vector<Point> create_valid_path(const vector<Point>& point_path) {
	// leave from the edges
	vector<Point> separated = {move_slightly_inner(point_path[0])};
	for (int i = 1; i < point_path.size() - 1; ++i) {
		const Point& p = point_path[i];
		if (is_int(p.x)) {
			if (separated.back().x < p.x) {
				separated.push_back({p.x - BUF - EPS, p.y});
			} else {
				separated.push_back({p.x + BUF + EPS, p.y});
			}
		} else if (is_int(p.y)) {
			if (separated.back().y < p.y) {
				separated.push_back({p.x, p.y - BUF - EPS});
			} else {
				separated.push_back({p.x, p.y + BUF + EPS});
			}
		} else {
			separated.push_back(p);
		}
	}
	separated.push_back(move_slightly_inner(point_path.back()));

	// quantize
	vector<Point> ret = {separated[0]};
	vector<int> loc_index(separated.size(), -1);
	for (int i = 1; i < separated.size() - 1; ++i) {
		Point& p = separated[i];
		bool pos_target = false;
		for (int li : cell_to_loc[(int)p.y][(int)p.x]) {
			const double d = dist(p.x, p.y, locs[li].x, locs[li].y);
			if (d <= BUF) {
				loc_index[i] = li;
				if (d <= EPS) {
					pos_target = true;
				}
				break;
			}
		}
		if (!pos_target) {
			p = quantize(p);
			for (int li : cell_to_loc[(int)p.y][(int)p.x]) {
				const double d = dist(p.x, p.y, locs[li].x, locs[li].y);
				if (d <= BUF) {
					loc_index[i] = li;
					break;
				}
			}
		}
		if (i != 1 && dist(p.x, p.y, ret.back().x, ret.back().y) <= BUF + EPS) {
			debug("remove collision: %d %d %d %.8f %.8f %.8f %.8f\n", i, loc_index[i], loc_index[i-1], p.x, p.y, ret.back().x, ret.back().y);
			if (loc_index[i] != -1 && loc_index[i - 1] != -1) {
				assert(loc_index[i] == loc_index[i - 1]);
				if (dist(p.x, p.y, locs[loc_index[i]].x, locs[loc_index[i]].y) < dist(ret.back().x, ret.back().y, locs[loc_index[i]].x, locs[loc_index[i]].y)) {
					ret.back() = p;
				}
			} else if (loc_index[i] != -1) {
				ret.back() = p;
			}
			continue;
		}
		ret.push_back(p);
	}
	ret.push_back(separated.back());
	return ret;
}

Rect get_edge_rect(const Point& p) {
	if (p.x <= BUF) {
		return {(int)p.y + BUF + EPS, (int)p.y + 1 - BUF - EPS, EPS, BUF - EPS};
	} else if (S - BUF <= p.x) {
		return {(int)p.y + BUF + EPS, (int)p.y + 1 - BUF - EPS, S - BUF + EPS, S - EPS};
	} else if (p.y <= BUF) {
		return {EPS, BUF - EPS, (int)p.x + BUF + EPS, (int)p.x + 1 - BUF - EPS};
	} else {
		assert(S - BUF <= p.y);
		return {S - BUF + EPS, S - EPS, (int)p.x + BUF + EPS, (int)p.x + 1 - BUF - EPS};
	}
}

double cross_penalty(double v1, double v2, int t1, int t2) {
	double ret;
	if (v1 < v2) {
		ret = ((int)v2 - v1) * t1 + (v2 - (int)v2) * t2;
	} else {
		ret = ((int)v1 - v2) * t2 + (v1 - (int)v1) * t1;
	}
	ret /= abs(v2 - v1);
	return ret;
}

void hill_climb(vector<Point>& point_path) {
	vi loc_idx(point_path.size(), -1);
	vi types(point_path.size());
	for (int i = 0; i < point_path.size() - 1; ++i) {
		types[i] = f[(int)point_path[i].y][(int)point_path[i].x];
		for (int li : cell_to_loc[(int)point_path[i].y][(int)point_path[i].x]) {
			if (dist(point_path[i].x, point_path[i].y, locs[li].x, locs[li].y) <= BUF) {
				assert(loc_idx[i] == -1);
				loc_idx[i] = li;
			}
		}
	}
	types.back() = f[(int)point_path.back().y][(int)point_path.back().x];
	loc_idx[0] = loc_idx.back() = -1;

	vector<double> penas(point_path.size() - 1);
	double sum_pena = 0;
	for (int i = 0; i < point_path.size() - 1; ++i) {
		const double d = dist(point_path[i].x, point_path[i].y, point_path[i+1].x, point_path[i+1].y);
		if ((int)point_path[i].x != (int)point_path[i+1].x) {
			penas[i] = cross_penalty(point_path[i].x, point_path[i+1].x, types[i], types[i+1]) * d;
		} else if ((int)point_path[i].y != (int)point_path[i+1].y) {
			penas[i] = cross_penalty(point_path[i].y, point_path[i+1].y, types[i], types[i+1]) * d;
		} else {
			penas[i] = types[i] * d;
		}
		sum_pena += penas[i];
	}
	// TODO set accurate value to penas

	const Rect rect_first = get_edge_rect(point_path[0]);
	const Rect rect_last = get_edge_rect(point_path.back());
	debug("sum_pena:%.7f\n", sum_pena);
	double MOVE = 0.01;
	int pi = 0;
	int last_improve_turn = 0;
	for (int turn = 0; ; ++turn, ++pi) {
		if (pi == point_path.size()) pi = 0;
		if ((turn & 0x3FFFF) == 0) {
			if (sum_pena == 0) {
				break;
			}
			if (get_elapsed_msec() > TL - 100) {
				debug("sum_pena:%.7f after turn %d\n", sum_pena, turn);
				break;
			}
			if (turn > last_improve_turn + 0x3FFFF) {
				MOVE /= 2;
				last_improve_turn = turn;
				// debug("MOVE:%.7f at turn %d\n", MOVE, turn);
				if (MOVE < BUF / 50) {
					MOVE = BUF / 50;
				}
			}
		}
		const Point& p = point_path[pi];
		const double move = rnd.nextDouble() * MOVE * 2 - MOVE;
		double nx = p.x;
		double ny = p.y;
		if (pi == 0 || pi == point_path.size() - 1) {
			nx += move;
			ny += (rnd.nextDouble() * MOVE * 2 - MOVE);
			Point np = (pi == 0 ? rect_first : rect_last).valid_point({nx, ny});
			nx = np.x;
			ny = np.y;
		} else {
			nx += move;
			if ((int)nx != (int)p.x) continue;
			ny += (rnd.nextDouble() * MOVE * 2 - MOVE);
			if ((int)ny != (int)p.y) continue;
			if (nx - (int)nx <= BUF + EPS) nx = (int)nx + BUF + EPS;
			if (nx - (int)nx >= 1 - BUF - EPS) nx = (int)nx + 1 - BUF - EPS;
			if (ny - (int)ny <= BUF + EPS) ny = (int)ny + BUF + EPS;
			if (ny - (int)ny >= 1 - BUF - EPS) ny = (int)ny + 1 - BUF - EPS;
			if (loc_idx[pi] != -1 && dist(nx, ny, locs[loc_idx[pi]].x, locs[loc_idx[pi]].y) >= BUF - EPS) continue;
		}
		double d1 = 0;
		double d2 = 0;
		if (pi != 0) {
			double d = dist(point_path[pi - 1].x, point_path[pi - 1].y, nx, ny);
			if (d <= BUF + EPS) continue;
			if ((int)point_path[pi - 1].x != (int)nx) {
				d1 = d * cross_penalty(point_path[pi - 1].x, nx, types[pi - 1], types[pi]);
			} else if ((int)point_path[pi - 1].y != (int)ny) {
				d1 = d * cross_penalty(point_path[pi - 1].y, ny, types[pi - 1], types[pi]);
			} else {
				d1 = d * types[pi - 1];
			}
			d1 -= penas[pi - 1];
		}
		if (pi != point_path.size() - 1) {
			double d = dist(point_path[pi + 1].x, point_path[pi + 1].y, nx, ny);
			if (d <= BUF + EPS) continue;
			if ((int)point_path[pi + 1].x != (int)nx) {
				d2 = d * cross_penalty(point_path[pi + 1].x, nx, types[pi + 1], types[pi]);
			} else if ((int)point_path[pi + 1].y != (int)ny) {
				d2 = d * cross_penalty(point_path[pi + 1].y, ny, types[pi + 1], types[pi]);
			} else {
				d2 = d * types[pi];
			}
			d2 -= penas[pi];
		}
		// debug("%.8f %d %d, %.8f %.8f %.8f %.8f \n", diff, pi, nx, ny, point_path[pi].x, point_path[pi].y);
		if (d1 + d2 < 0) {
			point_path[pi].x = nx;
			point_path[pi].y = ny;
			if (pi != 0) penas[pi - 1] += d1;
			if (pi != point_path.size() - 1) penas[pi] += d2;
			sum_pena += d1 + d2;
			// debug("sum_pena:%.9f at turn %d\n", sum_pena, turn);
			last_improve_turn = turn;
		}
	}
	double total_pena = sum_pena;
	for (int i = 0; i < types.size() - 1; ++i) {
		total_pena += sq(types[i + 1] - types[i]);
	}
	debug("total pena:%.8f\n", total_pena);
}

void do_final_validation(vector<Point>& point_path, const vector<int> loc_path) {
	int lpi = 1;
	for (int i = 1; i < point_path.size() - 1; ++i) {
		const double x = point_path[i].x;
		const double y = point_path[i].y;
		int li = loc_path[lpi];
		if (dist(x, y, locs[li].x, locs[li].y) < BUF) {
			++lpi;
			continue;
		}
		bool valid = true;
		for (int j = lpi + 1; j < loc_path.size(); ++j) {
			int li = loc_path[j];
			if (dist(x, y, locs[li].x, locs[li].y) <= BUF + EPS) {
				valid = false;
				break;
			}
		}
		if (!valid) {
			while (true) {
				double nx = x + rnd.nextDouble() * 2 * BUF - BUF;
				double ny = y + rnd.nextDouble() * 2 * BUF - BUF;
				if ((int)x != (int)nx) continue;
				if ((int)y != (int)ny) continue;
				if (nx - (int)nx <= BUF + EPS) continue;
				if (ny - (int)ny <= BUF + EPS) continue;
				if ((int)nx + 1 - nx <= BUF + EPS) continue;
				if ((int)ny + 1 - ny <= BUF + EPS) continue;
				if (dist(nx, ny, point_path[i - 1].x, point_path[i - 1].y) <= BUF + EPS) continue;
				if (dist(nx, ny, point_path[i + 1].x, point_path[i + 1].y) <= BUF + EPS) continue;
				bool ok = true;
				for (int j = lpi + 1; j < loc_path.size(); ++j) {
					if (dist(nx, ny, locs[loc_path[j]].x, locs[loc_path[j]].y) <= BUF + EPS) {
						ok = false;
						break;
					}
				}
				if (ok) {
					debug("validate at idx %d : (%.6f %.6f) -> (%.6f %.6f)\n", i, x, y, nx, ny);
					point_path[i].x = nx;
					point_path[i].y = ny;
					break;
				}
			}
		}
	}
}

struct TerrainCrossing {
    vector<double> getPath(const vector<string>& field, const vector<double>& locations, int capacity);
};

vector<double> TerrainCrossing::getPath(const vector<string>& field, const vector<double>& locations, int capacity) {
	start_time = get_time();
	S = field.size();
	N = locations.size() / 4;
	cell_to_loc.resize(S, vvi(S));
	for (int i = 0; i < 2 * N; ++i) {
		locs.push_back({locations[2 * i], locations[2 * i + 1]});
		cell_to_loc[(int)locs[i].y][(int)locs[i].x].push_back(i);
	}
	C = capacity;
	for (int i = 0; i < S; ++i) {
		for (int j = 0; j < S; ++j) {
			f[i][j] = field[i][j] - '0';
		}
	}
	const int PART_BITS = 3;
	path_prev_horz.resize(S + 1);
	path_prev_vert.resize(S + 1);
	dist_buf_horz.resize(S + 1);
	dist_buf_vert.resize(S + 1);
	for (int i = 0; i <= S; ++i) {
		path_prev_horz[i].resize(S * (1 << PART_BITS));
		path_prev_vert[i].resize(S * (1 << PART_BITS));
		dist_buf_horz[i].resize(S * (1 << PART_BITS));
		dist_buf_vert[i].resize(S * (1 << PART_BITS));
	}
	START_TIMER(0);
	calc_dists<PART_BITS>();
	STOP_TIMER(0);
	START_TIMER(1);
	vector<int> loc_path = tsp();
	STOP_TIMER(1);
	// int max_ord = 0;
	// for (int i = 0; i < 2 * N + 2; ++i) {
	// 	int p = loc_path[i];
	// 	int ord = 0;
	// 	if (i != 0) {
	// 		for (int j = 0; j <= 2 * N; ++j) {
	// 			if (j == p) continue;
	// 			if (dists[j][p] < dists[loc_path[i - 1]][p]) ++ord;
	// 		}
	// 	}
	// 	max_ord = max(max_ord, ord);
	// 	debug("%d (%.8f %.8f) %.5f %d\n", p, locs[p].y, locs[p].x, i == 0 ? 0 : 1.0 * dists[loc_path[i - 1]][p] / MUL_PENA, ord);
	// }
	// debug("max_ord:%d\n", max_ord);
	START_TIMER(2);
	vector<Point> ans = create_whole_path<PART_BITS>(loc_path);
	STOP_TIMER(2);
	START_TIMER(3);
	ans = create_valid_path(ans);
	STOP_TIMER(3);
	START_TIMER(4);
	hill_climb(ans);
	STOP_TIMER(4);
	START_TIMER(5);
	do_final_validation(ans, loc_path);
	STOP_TIMER(5);
	debug("points size:%lu\n", ans.size());
	// for (auto& p : ans) {
	// 	debug("%.9f %.9f\n", p.x, p.y);
	// }
	// debugln();
	vector<double> ret;
	for (Point& p : ans) {
		ret.push_back(p.x);
		ret.push_back(p.y);
	}
	PRINT_TIMER();
	return ret;
}

#if defined(LOCAL) || defined(TEST)
int main() {
	int M, L, capacity;
	scanf("%d", &M);
	vector<string> f(M);
	for (int i = 0; i < M; ++i) {
		f[i].resize(M);
		scanf("%s", &f[i][0]);
	}
	scanf("%d", &L);
	vector<double> locations(L);
	for (int i = 0; i < L; ++i) {
		scanf("%lf", &locations[i]);
	}
	scanf("%d", &capacity);
	
	TerrainCrossing tc;
	vector<double> ret = tc.getPath(f, locations, capacity);
	printf("%d\n", (int)ret.size());
	for (int i = 0; i < ret.size(); ++i) {
		printf("%.9lf\n", ret[i]);
	}
	fflush(stdout);
}
#endif