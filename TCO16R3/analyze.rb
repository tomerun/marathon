#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:S, :N, :C, :T, :elapsed, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /S: *(\d+) N: *(\d+) C: *(\d+) T: *(\d+)/
      testcase.S = $1.to_i
      testcase.N = $2.to_i
      testcase.C = $3.to_i
      testcase.T = $4.to_i
    elsif line =~ /elapsed:(.+)/
      testcase.elapsed = $1.to_f
    elsif line =~ /score:(.+)/
      testcase.score = $1.to_f
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, filter)
  sum_score_diff = 0
  sum_score_diff_relative = 0
  sum_score = 0
  total = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    sum_score += s2
    sum_score_diff += s2 - s1
    total += 1
    if s1 > s2
      win += 1
      sum_score_diff_relative += s2 == 0 ? 1.0 : 1.0 - s2 / s1
    elsif s1 < s2
      lose += 1
      sum_score_diff_relative += s1 == 0 ? -1.0 : s1 / s2 - 1.0
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, total - win - lose)
  puts sprintf("  diff_abs:%.6f", sum_score_diff / total)
  puts sprintf("  diff_rel:%.6f", sum_score_diff_relative / total)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  1.upto(10) do |c|
    puts "C:#{c}"
    calc(from, to, proc { |tc| tc[0].C == c })
  end
  puts "---------------------"
  2.upto(10) do |t|
    puts "T:#{t}"
    calc(from, to, proc { |tc| tc[0].T == t })
  end
  puts "---------------------"
  ns = [4, 13, 27, 52, 97, 134, 251]
  0.upto(ns.size - 2) do |nsi|
    puts "N:[#{ns[nsi]}-#{ns[nsi+1] - 1}]"
    calc(from, to, proc { |tc| ns[nsi] <= tc[0].N && tc[0].N < ns[nsi+1] })
  end
  puts "---------------------"
  puts "total"
  calc(from, to, proc { |tc| true })
end

main
