#!/bin/bash

 i=1
 while [[ $i -le 100 ]];
 do
 java -jar Vis.jar -exec "java PolygonEstimation" -silent -delay 0 -novis -seed $i
 i=`expr $i + 1`
 done
