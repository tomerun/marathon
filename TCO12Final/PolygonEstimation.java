import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class PolygonEstimation {

    static final boolean LOCAL = 1 == 1;
    static final boolean DEBUG = 1 == 0;
    static final boolean MEASURE_TIME = 1 == 0;
    static final int MAX_COORD = 9999;
    static Scanner sc = new Scanner(System.in);
    static final int NA = Integer.MIN_VALUE;
    final int PART = 40;
    final int REC_DEPTH = 9;
    final double REC_NORM = 500;
    double D, P;
    int[] ans;
    int proveCount = 0;

    int[] estimate(double D, double P) {
        this.D = D;
        this.P = P;
        int[] top = new int[PART];
        int[] bottom = new int[PART];
        Arrays.fill(top, NA);
        Arrays.fill(bottom, NA);
        int[] xs = new int[PART];
        for (int i = 0; i < PART; ++i) {
            xs[i] = MAX_COORD * (2 * i + 1) / PART / 2;
        }
        boolean detected = false;
        for (int i = 0; i < PART; ++i) {
            double[] res = sendRequest(xs[i], 0, xs[i], MAX_COORD);
            top[i] = (int) (res.length == 0 ? NA : res[1]);
            if (top[i] == NA) {
                if (detected) {
                    xs[i] = (xs[i] + xs[i - 1]) / 2;
                    res = sendRequest(xs[i], 0, xs[i], MAX_COORD);
                    top[i] = (int) (res.length == 0 ? NA : res[1]);
                    if (top[i] != NA) {
                        res = sendRequest(xs[i], MAX_COORD, xs[i], 0);
                        bottom[i] = (int) (res.length == 0 ? NA : Math.ceil(res[1]));
                    }
                    break;
                }
                bottom[i] = NA;
            } else {
                if (!detected) {
                    if (i != 0) {
                        xs[i - 1] = (xs[i] + xs[i - 1]) / 2;
                        res = sendRequest(xs[i - 1], 0, xs[i - 1], MAX_COORD);
                        top[i - 1] = (int) (res.length == 0 ? NA : res[1]);
                        if (top[i - 1] != NA) {
                            res = sendRequest(xs[i - 1], MAX_COORD, xs[i - 1], 0);
                            bottom[i - 1] = (int) (res.length == 0 ? NA : Math.ceil(res[1]));
                        }
                    }
                    detected = true;
                }
                res = sendRequest(xs[i], MAX_COORD, xs[i], 0);
                bottom[i] = (int) (res.length == 0 ? NA : Math.ceil(res[1]));
            }
        }
        for (int i = 0; i < PART; ++i) {
            if (top[i] != NA && top[i] >= bottom[i]) {
                int tmp = top[i];
                top[i] = Math.max(0, bottom[i] - 1);
                bottom[i] = Math.min(MAX_COORD, tmp + 1);
            }
            if (top[i] != NA && top[i] < 0) top[i] = 0;
            if (top[i] > MAX_COORD) top[i] = MAX_COORD - 1;
            if (bottom[i] != NA && bottom[i] < 0) bottom[i] = 1;
            if (bottom[i] > MAX_COORD) bottom[i] = MAX_COORD;
        }
        explore(top, bottom, xs);
        return ans;
    }

    void explore(int[] top, int[] bottom, int[] xs) {
        final double threshold = Math.min(120, D * 4);
        int li = 0;
        int ri = top.length - 1;
        for (int i = 0; i < top.length; ++i) {
            if (top[i] != NA) {
                li = i;
                break;
            }
        }
        for (int i = top.length - 1; i >= 0; --i) {
            if (top[i] != NA) {
                ri = i;
                break;
            }
        }
        if (li == ri || li + 1 == ri) {
            createEasy(top, bottom, xs);
            return;
        }

        ArrayList<Seg> segs = new ArrayList<Seg>();
        {
            // leftmost
            Seg seg = new Seg(xs[li], bottom[li], xs[li], top[li]);
            seg.examine = Math.abs(seg.y2 - seg.y1) > 500;
            segs.add(seg);
        }
        // upside
        int start = li;
        double sum = top[li + 1] - top[li];
        for (int i = li + 2; i <= ri; ++i) {
            double slope = sum / (xs[i - 1] - xs[start]);
            double expY = top[start] + slope * (xs[i] - xs[start]);

            int prevX = segs.get(segs.size() - 1).x2;
            int prevY = segs.get(segs.size() - 1).y2;
            double slopeNext = (sum + top[i] - top[i - 1]) / (xs[i] - xs[start]);
            Seg segNext = new Seg(prevX, prevY, xs[i], place(prevY + slopeNext * (xs[i] - prevX)));

            if (Math.abs(top[i] - expY) > threshold || cross(segNext, segs)) {
                if (start + 1 == i - 1) {
                    Seg seg = new Seg(prevX, prevY, xs[i - 1], top[i - 1]);
                    seg.examine = true;
                    segs.add(seg);
                } else {
                    for (int j = start; j < i - 1; ++j) {
                        prevX = segs.get(segs.size() - 1).x2;
                        prevY = segs.get(segs.size() - 1).y2;
                        Seg seg = new Seg(prevX, prevY, xs[j + 1], top[j + 1]);
                        segs.add(seg);
                    }
                }
                start = i - 1;
                sum = 0;
            }
            sum += top[i] - top[i - 1];
        }
        {
            // last one
            if (start + 1 == ri) {
                int prevX = segs.get(segs.size() - 1).x2;
                int prevY = segs.get(segs.size() - 1).y2;
                Seg seg = new Seg(prevX, prevY, xs[ri], top[ri]);
                seg.examine = true;
                segs.add(seg);
            } else {
                for (int j = start; j < ri; ++j) {
                    int prevX = segs.get(segs.size() - 1).x2;
                    int prevY = segs.get(segs.size() - 1).y2;
                    Seg seg = new Seg(prevX, prevY, xs[j + 1], top[j + 1]);
                    segs.add(seg);
                }
            }
        }
        {
            // rightmost
            int prevY = segs.get(segs.size() - 1).y2;
            Seg seg = new Seg(xs[ri], prevY, xs[ri], bottom[ri]);
            seg.examine = Math.abs(seg.y2 - seg.y1) > 500;
            segs.add(seg);
        }
        // downside
        start = ri;
        sum = bottom[ri - 1] - bottom[ri];
        for (int i = ri - 2; i >= li; --i) {
            double slope = sum / (xs[i + 1] - xs[start]);
            double expY = bottom[start] + slope * (xs[i] - xs[start]);
            int prevX = segs.get(segs.size() - 1).x2;
            int prevY = segs.get(segs.size() - 1).y2;
            double slopeNext = (sum + bottom[i] - bottom[i + 1]) / (xs[i] - xs[start]);
            Seg segNext = new Seg(prevX, prevY, xs[i], place(prevY + slopeNext * (xs[i] - prevX)));
            if (Math.abs(bottom[i] - expY) > threshold || cross(segNext, segs)) {
                if (start - 1 == i + 1) {
                    Seg seg = new Seg(prevX, prevY, xs[i + 1], bottom[i + 1]);
                    seg.examine = true;
                    segs.add(seg);
                } else {
                    for (int j = start; j > i + 1; --j) {
                        prevX = segs.get(segs.size() - 1).x2;
                        prevY = segs.get(segs.size() - 1).y2;
                        Seg seg = new Seg(prevX, prevY, xs[j - 1], bottom[j - 1]);
                        segs.add(seg);
                    }
                }
                start = i + 1;
                sum = 0;
            }
            sum += bottom[i] - bottom[i + 1];
        }
        {
            // last one
            if (start - 1 == li) {
                int prevX = segs.get(segs.size() - 1).x2;
                int prevY = segs.get(segs.size() - 1).y2;
                Seg seg = new Seg(prevX, prevY, xs[li], bottom[li]);
                seg.examine = true;
                segs.add(seg);
            } else {
                for (int j = start; j > li; --j) {
                    int prevX = segs.get(segs.size() - 1).x2;
                    int prevY = segs.get(segs.size() - 1).y2;
                    Seg seg = new Seg(prevX, prevY, xs[j - 1], bottom[j - 1]);
                    segs.add(seg);
                }
            }
        }

        for (int i = 0; i < segs.size();) {
            Seg seg = segs.get(i);
            if (!seg.examine) {
                ++i;
                continue;
            }
            Seg[] replaced = dag(seg, 0);
            if (replaced.length == 0) {
                ++i;
                continue;
            }
            // check crossing with other segments
            boolean ok = true;
            for (int j = 0; j < segs.size() && ok; ++j) {
                if (j == i) continue;
                if ((j + 1) % segs.size() == i) {
                    for (int k = 1; k < replaced.length; ++k) {
                        if (Seg.intersectSegments(segs.get(j), replaced[k].ghost) != Seg.IntersectionResult.NOTHING) {
                            ok = false;
                            break;
                        }
                    }
                    if (Seg.intersectSegments(segs.get(j), replaced[0].ghost) == Seg.IntersectionResult.MANY_POINTS) {
                        ok = false;
                    }
                } else if (j == (i + 1) % segs.size()) {
                    for (int k = 0; k < replaced.length - 1; ++k) {
                        if (Seg.intersectSegments(segs.get(j), replaced[k].ghost) != Seg.IntersectionResult.NOTHING) {
                            ok = false;
                            break;
                        }
                    }
                    if (Seg.intersectSegments(segs.get(j), replaced[replaced.length - 1].ghost) == Seg.IntersectionResult.MANY_POINTS) {
                        ok = false;
                    }
                } else {
                    for (int k = 0; k < replaced.length; ++k) {
                        if (Seg.intersectSegments(segs.get(j), replaced[k].ghost) != Seg.IntersectionResult.NOTHING) {
                            ok = false;
                            break;
                        }
                    }
                }
            }
            for (int j = 0; j < segs.size() && ok; ++j) {
                if (j == i) continue;
                if ((j + 1) % segs.size() == i) {
                    for (int k = 1; k < replaced.length; ++k) {
                        if (Seg.intersectSegments(segs.get(j), replaced[k]) != Seg.IntersectionResult.NOTHING) {
                            ok = false;
                            break;
                        }
                    }
                    if (Seg.intersectSegments(segs.get(j), replaced[0]) == Seg.IntersectionResult.MANY_POINTS) {
                        ok = false;
                    }
                } else if (j == (i + 1) % segs.size()) {
                    for (int k = 0; k < replaced.length - 1; ++k) {
                        if (Seg.intersectSegments(segs.get(j), replaced[k]) != Seg.IntersectionResult.NOTHING) {
                            ok = false;
                            break;
                        }
                    }
                    if (Seg.intersectSegments(segs.get(j), replaced[replaced.length - 1]) == Seg.IntersectionResult.MANY_POINTS) {
                        ok = false;
                    }
                } else {
                    for (int k = 0; k < replaced.length; ++k) {
                        if (Seg.intersectSegments(segs.get(j), replaced[k]) != Seg.IntersectionResult.NOTHING) {
                            ok = false;
                            break;
                        }
                    }
                }
            }
            if (!ok) {
                ++i;
                continue;
            }
            for (int j = 0; j < replaced.length; ++j) {
                segs.add(i + j, replaced[j]);
            }
            segs.remove(i + replaced.length);
        }

        ans = new int[segs.size() * 2];
        for (int i = 0; i < segs.size(); ++i) {
            Seg seg = segs.get(i);
            ans[2 * i] = seg.x1;
            ans[2 * i + 1] = seg.y1;
        }
    }

    Seg[] dag(Seg seg, int depth) {
        if (proveCount == 1000) {
            return new Seg[0];
        }
        double MARGIN = 1.0 * MAX_COORD / PART;
        double cx = (seg.x1 + seg.x2) / 2.0;
        double cy = (seg.y1 + seg.y2) / 2.0;
        double ex = (seg.x2 - seg.x1) / seg.norm();
        double ey = (seg.y2 - seg.y1) / seg.norm();
        if (seg.parent != null) {
            double pcx = (seg.parent.x1 + seg.parent.x2) / 2.0;
            double pcy = (seg.parent.y1 + seg.parent.y2) / 2.0;
            double l1 = Seg.norm(seg.x1, seg.y1, pcx, pcy);
            double l2 = Seg.norm(seg.x2, seg.y2, pcx, pcy);
            double l3 = seg.norm();
            MARGIN = Math.max(MARGIN, Math.min(l1 * l3 / 2 / l2, l2 * l3 / 2 / l1) * 1.01);
        }
        double sx = cx + ey * MARGIN;
        double sy = cy - ex * MARGIN;
        int sxi = place(sx);
        int syi = place(sy);
        int gxi = place(cx);
        int gyi = place(cy);
        double[] prove = sendRequest(sxi, syi, gxi, gyi);
        if (prove.length == 0) {
            return new Seg[0];
        }
        // if new point is on the original segment, need not to recurse?
        if (seg.depth > 1 && Seg.norm(cx, cy, prove[0], prove[1]) < (P < 25 ? 3 * D : P < 50 ? 2 * D : D)) {
            return new Seg[0];
        }

        int nx = place(prove[0]);
        int ny = place(prove[1]);

        Seg[] ret = new Seg[2];
        ret[0] = new Seg(seg.x1, seg.y1, nx, ny);
        ret[1] = new Seg(nx, ny, seg.x2, seg.y2);
        ret[0].depth = seg.depth + 1;
        ret[1].depth = seg.depth + 1;
        boolean noGhost = seg.depth == 0 || (seg.depth == 1 && seg.parent != null);
        int ngx = (int) (nx - ey * D * (noGhost ? 0 : 2.0));
        int ngy = (int) (ny + ex * D * (noGhost ? 0 : 2.0));
        ret[0].ghost = new Seg(seg.x1, seg.y1, ngx, ngy);
        ret[1].ghost = new Seg(ngx, ngy, seg.x2, seg.y2);
        if (seg.depth < REC_DEPTH) {
            ret[0].examine = ret[0].norm() > REC_NORM;
            ret[1].examine = ret[1].norm() > REC_NORM;
            double dx1 = seg.x2 - seg.x1;
            double dy1 = seg.y2 - seg.y1;
            double dx2 = nx - seg.x1;
            double dy2 = ny - seg.y1;
            double rot = dx1 * dy2 - dx2 * dy1;
            if (rot > 0) {
                ret[0].parent = seg;
                ret[1].parent = seg;
            }
        }
        return ret;
    }

    boolean cross(Seg seg, ArrayList<Seg> segs) {
        for (int i = 0; i < segs.size() - 1; ++i) {
            if (Seg.intersectSegments(seg, segs.get(i)) != Seg.IntersectionResult.NOTHING) {
                return true;
            }
        }
        return false;
    }

    int place(double coord) {
        int v = (int) Math.floor(coord + 0.5);
        if (v < 0) v = 0;
        if (v > MAX_COORD) v = MAX_COORD;
        return v;
    }

    void createEasy(int[] top, int[] bottom, int[] xs) {
        ArrayList<Integer> ret = new ArrayList<Integer>();
        for (int i = 0; i < top.length; ++i) {
            if (top[i] == NA) continue;
            ret.add(xs[i]);
            ret.add(top[i]);
        }
        for (int i = top.length - 1; i >= 0; --i) {
            if (bottom[i] == NA) continue;
            ret.add(xs[i]);
            ret.add(bottom[i]);
        }
        ans = new int[ret.size()];
        for (int i = 0; i < ret.size(); ++i) {
            ans[i] = ret.get(i);
        }
    }

    static class Seg {
        int x1, y1, x2, y2;
        boolean examine;
        int depth = 0;
        Seg parent = null;
        Seg ghost = null;
        int A, B, C;

        Seg(int x1, int y1, int x2, int y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.A = y2 - y1;
            this.B = x1 - x2;
            this.C = -A * x1 - B * y1;
        }

        public int signOf(int x, int y) {
            int Z = x * A + y * B + C;
            if (Z > 0) return 1;
            if (Z < 0) return -1;
            return 0;
        }

        double norm() {
            return Math.sqrt(sq(this.x2 - this.x1) + sq(this.y2 - this.y1));
        }

        static double norm(double x1, double y1, double x2, double y2) {
            return Math.sqrt(sq(x2 - x1) + sq(y2 - y1));
        }

        public String toString() {
            return x1 + " " + y1 + " " + x2 + " " + y2 + " " + examine;
        }

        enum IntersectionResult {
            NOTHING, SINGLE_POINT, MANY_POINTS
        }

        private static IntersectionResult intersectLineSegments(int a, int b, int c, int d) {
            if (a > b) {
                int tmp = a;
                a = b;
                b = tmp;
            }
            if (c > d) {
                int tmp = c;
                c = d;
                d = tmp;
            }
            int left = Math.max(a, c);
            int right = Math.min(b, d);
            if (left < right) {
                return IntersectionResult.MANY_POINTS;
            }
            if (left == right) {
                return IntersectionResult.SINGLE_POINT;
            }
            return IntersectionResult.NOTHING;
        }

        static IntersectionResult intersectSegments(Seg a, Seg b) {
            int Z1 = b.signOf(a.x1, a.y1);
            int Z2 = b.signOf(a.x2, a.y2);
            int Z3 = a.signOf(b.x1, b.y1);
            int Z4 = a.signOf(b.x2, b.y2);

            if (Z1 == 0 && Z2 == 0) {
                if (b.x1 == b.x2) {
                    return intersectLineSegments(a.y1, a.y2, b.y1, b.y2);
                } else {
                    return intersectLineSegments(a.x1, a.x2, b.x1, b.x2);
                }
            } else {
                if (Z1 * Z2 <= 0 && Z3 * Z4 <= 0) {
                    return IntersectionResult.SINGLE_POINT;
                } else {
                    return IntersectionResult.NOTHING;
                }
            }
        }

    }

    static double sq(double v) {
        return v * v;
    }

    double[] sendRequest(int x1, int y1, int x2, int y2) {
        if (x1 == x2 && y1 == y2) {
            return new double[0];
        }
        ++proveCount;
        if (LOCAL) {
            System.out.println(1);
            System.out.println(x1);
            System.out.println(y1);
            System.out.println(x2);
            System.out.println(y2);
            System.out.flush();
            int res = sc.nextInt();
            if (res == 0) {
                return new double[0];
            }
            double x = sc.nextDouble();
            double y = sc.nextDouble();
            return new double[] { x, y };
        } else {
            return RayCaster.sendRequest(x1, y1, x2, y2);
        }
    }

    public static void main(String[] args) {
        double D = sc.nextDouble();
        double P = sc.nextDouble();
        int[] ret = new PolygonEstimation().estimate(D, P);
        System.out.println(2);
        System.out.println(ret.length / 2);
        for (int i = 0; i < ret.length; ++i) {
            System.out.println(ret[i]);
        }
        System.out.flush();
    }

    static void debug(Object... obj) {
        if (DEBUG) System.err.println(Arrays.deepToString(obj));
    }

    static final class Timer {
        long[] sum = new long[9];
        long[] start = new long[sum.length];

        void start(int i) {
            if (MEASURE_TIME) {
                start[i] = System.currentTimeMillis();
            }
        }

        void end(int i) {
            if (MEASURE_TIME) {
                sum[i] += System.currentTimeMillis() - start[i];
            }
        }
    }

    static final class XorShift {
        int x = 123456789;
        int y = 362436069;
        int z = 521288629;
        int w = 88675123;

        int nextInt(int n) {
            final int t = x ^ (x << 11);
            x = y;
            y = z;
            z = w;
            w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
            final int r = w % n;
            return r < 0 ? r + n : r;
        }

    }

}