#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:H, :W, :CL, :CM, :CO, :MM, :MO, :PO, :PC, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /H: *(\d+) W: *(\d+) CL: *(\d+) CM: *(\d+) CO: *(\d+) MM: *(\d+) MO: *(\d+) PO: *(\d+) PC: *(\d+)/
      testcase.H = $1.to_i
      testcase.W = $2.to_i
      testcase.CL = $3.to_i
      testcase.CM = $4.to_i
      testcase.CO = $5.to_i
      testcase.MM = $6.to_i
      testcase.MO = $7.to_i
      testcase.PO = $8.to_i
      testcase.PC = $9.to_i
    elsif line =~ /score:(.+)$/
      testcase.score = $1.to_i
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    count += 1
    sum_score_diff += s2 - s1
    if s1 < s2
      win += 1
    elsif s1 > s2
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff * 1.0 / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  [100, 1000, 1800, 3200, 5000, 10001].each_cons(2) do |lower, upper|
    puts "S:#{lower}-#{upper - 1}"
    calc(from, to) { |from, to| (from.H * from.W).between?(lower, upper - 1) }
  end
  puts "---------------------"
  1.step(10, 2) do |param|
    upper = param + 1
    puts "CL:#{param}-#{upper}"
    calc(from, to) { |from, to| from.CL.between?(param, upper) }
  end
  puts "---------------------"
  3.step(30, 7) do |param|
    upper = param + 6
    puts "CM:#{param}-#{upper}"
    calc(from, to) { |from, to| from.CM.between?(param, upper) }
  end
  puts "---------------------"
  2.step(20, 5) do |param|
    upper = param + 4
    puts "CO:#{param}-#{upper}"
    calc(from, to) { |from, to| from.CO.between?(param, upper) }
  end
  puts "---------------------"
  5.step(15, 4) do |param|
    upper = param + 3
    puts "PO:#{param}-#{upper}"
    calc(from, to) { |from, to| from.PO.between?(param, upper) }
  end
  puts "---------------------"
  15.step(25, 4) do |param|
    upper = param + 3
    puts "PC:#{param}-#{upper}"
    calc(from, to) { |from, to| from.PC.between?(param, upper) }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
