#include <algorithm>
#include <utility>
#include <vector>
#include <unordered_set>
#include <bitset>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
// #define DEBUG
#define NDEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;
using pi=pair<int, int>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 7000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9600;
#endif

ll start_time; // msec

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
	ull ret;
	__asm__ volatile ("rdtsc" : "=A" (ret));
	return ret;
#else
	ull high,low;
	__asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
	return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

struct Point {
	uint8_t r, c;
};

const int DR[4] = {-1, 0, 1, 0};
const int DC[4] = {0, 1, 0, -1};
const int INF = 1 << 28;
const uint8_t EMPTY = 0;
const uint8_t CRYSTAL = 0x07; // CRYSTAL: 0x01~0x06
const uint8_t OBST = 0x08;
const uint8_t LANTERN = 0x10; // LANTERN: 0x11,0x12,0x14
const uint8_t MIRROR_S = 0x20;
const uint8_t MIRROR_B = 0x40;
const uint8_t RESERVED = 0x80;
// const double COOLER_INI = COOLER_INI_V;
// const double COOLER_END = COOLER_END_V;
// const int INITIAL_SINGLE_VALUE = (int)INITIAL_SINGLE_VALUE_V;
// const int FIX_INVALID_RATIO_BITS = (int)FIX_INVALID_RATIO_BITS_V;
// const int REMOVE_LANTERN_RATIO_BITS = (int)REMOVE_LANTERN_RATIO_BITS_V;
const double COOLER_INI = 0.13;
const double COOLER_END = 0.72;
const int INITIAL_SINGLE_VALUE = 5;
const int FIX_INVALID_RATIO_BITS = 6;
const int REMOVE_LANTERN_RATIO_BITS = 2;
const int SA_COUNT_RATIO = 6000;
const int FIX_INVALID_RATIO = (1 << FIX_INVALID_RATIO_BITS) - 1;
const int REMOVE_LANTERN_RATIO = (1 << REMOVE_LANTERN_RATIO_BITS) - 1;
int H, W;
int CL, CM, CO;
int MM, MO;
uint8_t target[102][102];
uint8_t board[102][102], best_board[102][102];
uint16_t light[102][102]; // bit0-3:top, bit4-7:right bit8-11:bottom bit12-15:left
uint8_t dist[102][102][4];
double cooler;
int single_value;
int8_t evaluation[7][8];
int count_obst;
int count_mirror;

struct Terminal {
	uint8_t r, c, d;

	bool operator==(const Terminal& o) const {
		return r == o.r && c == o.c && d == o.d;
	}
};

Terminal trace(int r, int c, int dir) {
	int cr = r + DR[dir] * dist[r][c][dir];
	int cc = c + DC[dir] * dist[r][c][dir];
	for (int i = 0; i < 8; ++i) {
		if (board[cr][cc] == MIRROR_S) {
			dir ^= 1;
		} else if (board[cr][cc] == MIRROR_B) {
			dir ^= 3;
		} else {
			return {(uint8_t)cr, (uint8_t)cc, (uint8_t)dir};
		}
		cr += DR[dir] * dist[cr][cc][dir];
		cc += DC[dir] * dist[cr][cc][dir];
	}
	return {(uint8_t)0xFF, (uint8_t)0xFF, (uint8_t)0};
}

inline bool accept(int diff) {
	if (diff >= 0) return true;
	return rnd.nextDouble() < exp(diff * cooler);
}

inline bool is_crystal(int v) {
	return v != 0 && (v & CRYSTAL) == v;
}

inline bool from_both_side(int l) {
	return ((l & 0x000F) && (l & 0x0F00)) || ((l & 0x00F0) && (l & 0xF000));
}

inline int combined_light(int l) {
	l = l | (l >> 8);
	return (l | l >> 4) & 0xF;
}

inline int combined_light(int r, int c) {
	return combined_light(light[r][c]);
}

void set_evaluation() {
	for (int i = 1; i <= 6; ++i) {
		for (int j = 1; j <= 7; ++j) {
			if (i == j) {
				evaluation[i][j] = 10 * (__builtin_popcount(i) + 1);
			} else if ((i & j) == j) {
				evaluation[i][j] = single_value;
			} else {
				evaluation[i][j] = -10;
			}
		}
	}
}

struct Evaluator {

	array<uint32_t, 4> buf;
	int size;

	Evaluator() : size(0) { }

	void add(int r, int c, int ol, int nl) {
		assert(board[r][c] & CRYSTAL);
		for (int i = 0; i < size; ++i) {
			if ((buf[i] >> 16) == ((r << 8) | c)) {
				buf[i] ^= ol ^ nl;
				return;
			}
		}
		buf[size++] = (r << 24) | (c << 16) | (ol ^ nl);
	}

	int calc() {
		int ret = 0;
		for (int i = 0; i < size; ++i) {
			const int r = buf[i] >> 24;
			const int c = (buf[i] >> 16) & 0xFF;
			int l = light[r][c];
			const int old_color = combined_light(l);
			const int new_color = combined_light(l ^ buf[i]);
			const int ov = evaluation[board[r][c]][old_color];
			const int nv = evaluation[board[r][c]][new_color];
			ret += nv - ov;
		}
		return ret;
	}
};

struct EvaluatorAdd {

	array<uint32_t, 4> buf;
	int size;

	EvaluatorAdd() : size(0) { }

	void add(int r, int c) {
		assert(board[r][c] & CRYSTAL);
		buf[size++] = (r << 8) | c;
	}

	array<int, 3> calc() {
		array<int, 3> ret = {};
		for (int i = 0; i < size; ++i) {
			const int r = buf[i] >> 8;
			const int c = buf[i] & 0xFF;
			assert(board[r][c] & RESERVED);
			board[r][c] ^= RESERVED;
			const int old_color = combined_light(r, c);
			const int ov = evaluation[board[r][c]][old_color];
			ret[0] += evaluation[board[r][c]][old_color | 1] - ov;
			ret[1] += evaluation[board[r][c]][old_color | 2] - ov;
			ret[2] += evaluation[board[r][c]][old_color | 4] - ov;
		}
		return ret;
	}

	void clear() {
		for (int i = 0; i < size; ++i) {
			const int r = buf[i] >> 8;
			const int c = buf[i] & 0xFF;
			assert(board[r][c] & RESERVED);
			board[r][c] ^= RESERVED;
		}
	}
};

struct PositionSet {
	vector<Point> pos;
	vvi idx;

	void init() {
		idx.assign(H + 2, vi(W + 2, -1));
		pos.clear();
	}

	int size() const {
		return pos.size();
	}

	bool contains(int r, int c) const {
		return idx[r][c] != -1;
	}

	void add(int r, int c) {
		assert(idx[r][c] == -1);
		idx[r][c] = pos.size();
		pos.push_back({(uint8_t)r, (uint8_t)c});
	}

	void remove(int r, int c) {
		int i = idx[r][c];
		assert(pos[i].r == r && pos[i].c == c);
		swap(pos[i], pos.back());
		idx[pos[i].r][pos[i].c] = i;
		idx[r][c] = -1;
		pos.pop_back();
	}
};
PositionSet overcolor_ps;

void set_dist() {
	for (int i = 1; i <= H; ++i) {
		for (int j = 1; j <= W; ++j) {
			for (int k = 0; k < 4; ++k) {
				int cr = i + DR[k];
				int cc = j + DC[k];
				dist[i][j][k] = 1;
				while (board[cr][cc] == EMPTY) {
					cr += DR[k];
					cc += DC[k];
					dist[i][j][k]++;
				}
			}
		}
	}
}

void update_dist_add(int r, int c) {
	for (int i = 0; i < 4; ++i) {
		int cr = r - DR[i];
		int cc = c - DC[i];
		dist[cr][cc][i] -= dist[r][c][i];
		while (board[cr][cc] == EMPTY) {
			cr -= DR[i];
			cc -= DC[i];
			dist[cr][cc][i] -= dist[r][c][i];
		}
	}
}

void update_dist_remove(int r, int c) {
	for (int i = 0; i < 4; ++i) {
		int cr = r - DR[i];
		int cc = c - DC[i];
		dist[cr][cc][i] += dist[r][c][i];
		while (board[cr][cc] == EMPTY) {
			cr -= DR[i];
			cc -= DC[i];
			dist[cr][cc][i] += dist[r][c][i];
		}
	}
}

int evaluate_add_lantern(int r, int c, int* type) {
	// debug("evaluate add lantern:(%d %d)\n", r, c);
	EvaluatorAdd evaluator;
	int diff = 0;
	bool any = false;
	board[r][c] = LANTERN | 7;
	for (int i = 0; i < 4; ++i) {
		int cr = r + DR[i] * dist[r][c][i];
		int cc = c + DC[i] * dist[r][c][i];
		int cd = i;
		assert(board[cr][cc] != EMPTY);
		while (true) {
			if (board[cr][cc] == OBST || (board[cr][cc] & CRYSTAL)) {
				break;
			}
			if (board[cr][cc] == MIRROR_S) {
				cd ^= 1;
			} else if (board[cr][cc] == MIRROR_B) {
				cd ^= 3;
			}
			cr += DR[cd];
			cc += DC[cd];
		}
		if (board[cr][cc] & LANTERN) {
			board[cr][cc] = EMPTY;
			evaluator.clear();
			return -INF;
		}
		if ((board[cr][cc] & CRYSTAL) == 0) continue;
		if (board[cr][cc] & RESERVED) continue;
		any = true;
		evaluator.add(cr, cc);
		board[cr][cc] |= RESERVED;
	}
	board[r][c] = EMPTY;
	if (!any) {
		return -INF;
	}
	array<int, 3> color_v = evaluator.calc();
	const auto max_color_itr = max_element(color_v.begin(), color_v.end());
	diff += *max_color_itr;
	*type = LANTERN | (1 << (max_color_itr - color_v.begin()));
	return diff - CL;
}

int evaluate_remove_lantern(int r, int c) {
	assert(board[r][c] & LANTERN);
	int color = board[r][c] - LANTERN;
	// debug("evaluate remove lantern:(%d %d)\n", r, c);
	Evaluator evaluator;
	for (int i = 0; i < 4; ++i) {
		int cr = r + DR[i] * dist[r][c][i];
		int cc = c + DC[i] * dist[r][c][i];
		int cd = i;
		while (true) {
			assert(board[cr][cc] != EMPTY);
			if (board[cr][cc] == OBST || (board[cr][cc] & CRYSTAL)) {
				break;
			}
			cd ^= (board[cr][cc] == MIRROR_S ? 1 : 3);
			cr += DR[cd] * dist[cr][cc][cd];
			cc += DC[cd] * dist[cr][cc][cd];
		}
		if ((board[cr][cc] & CRYSTAL) == 0) continue;
		assert((board[cr][cc] & CRYSTAL) == board[cr][cc]);
		evaluator.add(cr, cc, color << (4 * cd), 0);
	}
	const int ret = evaluator.calc() + CL;
	return ret;
}

int evaluate_move_lantern(int r, int c, Point* move_to) {
	assert(board[r][c] & LANTERN);
	const int color = board[r][c] - LANTERN;
	const array<Terminal, 4> terms = {trace(r, c, 0), trace(r, c, 1), trace(r, c, 2), trace(r, c, 3)};
	array<int, 4> colors;
	colors[0] = (terms[0].r != 0xFF && (board[terms[0].r][terms[0].c] & CRYSTAL)) ? (color << (terms[0].d * 4)) : 0;
	colors[1] = (terms[1].r != 0xFF && (board[terms[1].r][terms[1].c] & CRYSTAL)) ? (color << (terms[1].d * 4)) : 0;
	colors[2] = (terms[2].r != 0xFF && (board[terms[2].r][terms[2].c] & CRYSTAL)) ? (color << (terms[2].d * 4)) : 0;
	colors[3] = (terms[3].r != 0xFF && (board[terms[3].r][terms[3].c] & CRYSTAL)) ? (color << (terms[3].d * 4)) : 0;
	int best_v = -INF;
	for (int d = 0; d < 4; ++d) {
		int cd = d;
		int cr = r + DR[cd];
		int cc = c + DC[cd];
		while (true) {
			if (board[cr][cc] == OBST) break;
			if (board[cr][cc] & CRYSTAL) break;
			if (board[cr][cc] == EMPTY) {
				if ((light[cr][cc] & ~(0xF << (4 * cd))) == 0) {
					Terminal t3 = trace(cr, cc, cd ^ 1);
					Terminal t4 = trace(cr, cc, cd ^ 3);
					if (t3.r != 0xFF && t4.r != 0xFF) {
						Evaluator evaluator;
						if (colors[d ^ 1] != 0) evaluator.add(terms[d ^ 1].r, terms[d ^ 1].c, colors[d ^ 1], 0);
						if (colors[d ^ 3] != 0) evaluator.add(terms[d ^ 3].r, terms[d ^ 3].c, colors[d ^ 3], 0);
						if (board[t3.r][t3.c] & CRYSTAL) evaluator.add(t3.r, t3.c, 0, color << (t3.d * 4));
						if (board[t4.r][t4.c] & CRYSTAL) evaluator.add(t4.r, t4.c, 0, color << (t4.d * 4));
						int diff = evaluator.calc();
						if (best_v < diff) {
							best_v = diff;
							move_to->r = cr;
							move_to->c = cc;
						}
					}
				}
			} else if (board[cr][cc] == MIRROR_S) {
				cd ^= 1;
			} else {
				cd ^= 3;
			}
			cr += DR[cd];
			cc += DC[cd];
		}
	}
	return best_v;
}

void add_lantern(int r, int c, int color) {
	board[r][c] = color | LANTERN;
	for (int dir = 0; dir < 4; ++dir) {
		int cd = dir;
		int col = color << (dir * 4);
		int cr = r + DR[dir];
		int cc = c + DC[dir];
		while (true) {
			// debug("add_lantern:%d %d %x\n", cr, cc, board[cr][cc]);
			assert((board[cr][cc] & LANTERN) == 0);
			assert((light[cr][cc] & col) == 0);
			light[cr][cc] ^= col;
			if (board[cr][cc] == OBST) break;
			if (board[cr][cc] & CRYSTAL) {
				if ((color & board[cr][cc]) == 0 && !overcolor_ps.contains(cr, cc)) {
					overcolor_ps.add(cr, cc);
				}
				break;
			}
			if (board[cr][cc] == EMPTY) {
			} else {
				cd ^= board[cr][cc] == MIRROR_S ? 1 : 3;
				col = color << (cd * 4);
			}
			cr += DR[cd];
			cc += DC[cd];
		}
	}
	update_dist_add(r, c);
}

void remove_lantern(int r, int c) {
	int color = board[r][c] - LANTERN;
	board[r][c] = EMPTY;
	for (int dir = 0; dir < 4; ++dir) {
		int cd = dir;
		int col = color << (dir * 4);
		int cr = r + DR[dir];
		int cc = c + DC[dir];
		while (true) {
			// debug("remove_lantern:%d %d %x\n", cr, cc, board[cr][cc]);
			assert((board[cr][cc] & LANTERN) == 0);
			assert((light[cr][cc] & col) == col);
			light[cr][cc] ^= col;
			if (board[cr][cc] == OBST) break;
			if (board[cr][cc] & CRYSTAL) {
				const int cl = combined_light(cr, cc);
				if ((cl & board[cr][cc]) == cl && overcolor_ps.contains(cr, cc)) {
					overcolor_ps.remove(cr, cc);
				}
				break;
			}
			if (board[cr][cc] == EMPTY) {
			} else {
				cd ^= board[cr][cc] == MIRROR_S ? 1 : 3;
				col = color << (cd * 4);
			}
			cr += DR[cd];
			cc += DC[cd];
		}
	}
	update_dist_remove(r, c);
}

template<bool add>
int evaluate_change_obstacle(int r, int c) {
	// debug("eval_change_obst:(%d %d)\n", r, c);
	assert(board[r][c] == (add ? EMPTY : OBST));
	board[r][c] = add ? OBST : EMPTY;
	Evaluator evaluator;
	for (int i = 0; i < 4; ++i) {
		int color = (light[r][c] >> (i * 4)) & 0xF;
		if (color == 0) continue;
		int cd = i;
		int cr = r + DR[i] * dist[r][c][cd];
		int cc = c + DC[i] * dist[r][c][cd];
		while (true) {
			if (board[cr][cc] == OBST || (board[cr][cc] & CRYSTAL)) {
				break;
			}
			if (board[cr][cc] == MIRROR_S) {
				cd ^= 1;
			} else if (board[cr][cc] == MIRROR_B) {
				cd ^= 3;
			}
			cr += DR[cd];
			cc += DC[cd];
		}
		if (!add) {
			if (board[cr][cc] & LANTERN) {
				board[r][c] = OBST;
				return -INF;
			}
		}
		if ((board[cr][cc] & CRYSTAL) == 0) continue;
		assert((board[cr][cc] & CRYSTAL) == board[cr][cc]);
		if (add) {
			evaluator.add(cr, cc, color << (4 * cd), 0);
		} else {
			evaluator.add(cr, cc, 0, color << (4 * cd));
		}
	}
	board[r][c] = add ? EMPTY : OBST;
	return evaluator.calc() + (add ? -CO : CO);
}

void add_obstacle(int r, int c) {
	// debug("add_obstacle:%d %d\n", r, c);
	assert(count_obst < MO);
	++count_obst;
	board[r][c] = OBST;
	int old_light = light[r][c];
	for (int dir = 0; dir < 4; ++dir) {
		int color = (old_light >> (dir * 4)) & 0xF;
		if (color == 0) continue;
		int cd = dir;
		int cr = r + DR[dir];
		int cc = c + DC[dir];
		while (true) {
			int col = color << (cd * 4);
			// debug("%d %d %d %d %x %x\n", cr, cc, cd, col, light[cr][cc], board[cr][cc]);
			assert((light[cr][cc] & col) == col);
			light[cr][cc] ^= col;
			if (board[cr][cc] == OBST) break;
			if (is_crystal(board[cr][cc])) {
				if (overcolor_ps.contains(cr, cc)) {
					int cl = combined_light(cr, cc);
					if ((cl & board[cr][cc]) == cl) {
						overcolor_ps.remove(cr, cc);
					}
				}
				break;
			}
			if (board[cr][cc] == MIRROR_S) {
				cd ^= 1;
			} else if (board[cr][cc] == MIRROR_B) {
				cd ^= 3;
			}
			cr += DR[cd];
			cc += DC[cd];
		}
	}
	update_dist_add(r, c);
}

void remove_obstacle(int r, int c) {
	// debug("remove_obstacle:(%d %d) %x\n", r, c, light[r][c]);
	--count_obst;
	board[r][c] = EMPTY;
	int old_light = light[r][c];
	for (int dir = 0; dir < 4; ++dir) {
		int color = (old_light >> (4 * dir)) & 0xF;
		if (color == 0) continue;
		int cd = dir;
		int cr = r + DR[dir];
		int cc = c + DC[dir];
		while (true) {
			// debug("%d %d %d %d %x\n", cr, cc, cd, color, light[cr][cc]);
			assert((light[cr][cc] & (color << (4 * cd))) == 0);
			light[cr][cc] ^= (color << (4 * cd));
			if (board[cr][cc] == OBST) break;
			if (is_crystal(board[cr][cc])) {
				if ((color & board[cr][cc]) == 0 && !overcolor_ps.contains(cr, cc)) {
					overcolor_ps.add(cr, cc);
				}
				break;
			}
			if (board[cr][cc] == MIRROR_S) {
				cd ^= 1;
			} else if (board[cr][cc] == MIRROR_B) {
				cd ^= 3;
			}
			cr += DR[cd];
			cc += DC[cd];
		}
	}
	update_dist_remove(r, c);
}

int evaluate_add_mirror(int r, int c, int* type) {
	assert(board[r][c] == EMPTY);
	const array<int, 2> types = {MIRROR_S, MIRROR_B};
	array<int, 2> eval = {};
	const int l = light[r][c];
	const array<bool, 4> has = {(l & 0xF) != 0, (l & 0xF0) != 0, (l & 0xF00) != 0, (l & 0xF000) != 0};
	// debug("evaluate_add_mirror:(%d %d) %x\n", r, c, l);
	for (int t = 0; t < 2; ++t) {
		const int rd = t * 2 + 1;
		if ((has[0] && has[rd ^ 2]) || (has[2] && has[rd])) {
			eval[t] = -INF;
			continue;
		}
		board[r][c] = types[t];
		Evaluator evaluator;
		for (int i = 0; i < 4; ++i) {
			const int ol = (light[r][c] >> ((i ^ rd) * 4)) & 0xF;
			const int nl = (light[r][c] >> (i * 4)) & 0xF;
			if (ol == nl) continue;
			int cd = i ^ rd;
			int cr = r + DR[cd] * dist[r][c][cd];
			int cc = c + DC[cd] * dist[r][c][cd];
			while (true) {
				if (cr == r && cc == c) {
					eval[t] = -INF;
					goto END;
				}
				if (board[cr][cc] == OBST || (board[cr][cc] & CRYSTAL)) {
					break;
				}
				if (board[cr][cc] == MIRROR_S) {
					cd ^= 1;
				} else if (board[cr][cc] == MIRROR_B) {
					cd ^= 3;
				}
				cr += DR[cd];
				cc += DC[cd];
			}
			if (!(board[cr][cc] & CRYSTAL)) continue;
			evaluator.add(cr, cc, ol << (4 * cd), nl << (4 * cd));
		}
		eval[t] = evaluator.calc() - CM;
		END: ;
	}
	board[r][c] = EMPTY;
	if (eval[0] > eval[1]) {
		*type = types[0];
		return eval[0];
	} else {
		*type = types[1];
		return eval[1];
	}
}

int evaluate_remove_mirror(int r, int c) {
	assert(board[r][c] == MIRROR_S || board[r][c] == MIRROR_B);
	const int rd = board[r][c] == MIRROR_S ? 1 : 3;
	const int old = board[r][c];
	board[r][c] = EMPTY;
	// debug("evaluate_remove_mirror:%d %d\n", r, c);
	Evaluator evaluator;
	for (int i = 0; i < 4; ++i) {
		const int ol = (light[r][c] >> ((i ^ rd) * 4)) & 0xF;
		const int nl = (light[r][c] >> (i * 4)) & 0xF;
		if (ol == nl) continue;
		int cd = i;
		int cr = r + DR[cd] * dist[r][c][cd];
		int cc = c + DC[cd] * dist[r][c][cd];
		while (true) {
			if (board[cr][cc] == OBST || (board[cr][cc] & CRYSTAL)) {
				break;
			}
			if (board[cr][cc] == MIRROR_S) {
				cd ^= 1;
			} else if (board[cr][cc] == MIRROR_B) {
				cd ^= 3;
			}
			cr += DR[cd];
			cc += DC[cd];
		}
		if (board[cr][cc] & LANTERN) {
			board[r][c] = old;
			return -INF;
		}
		if (!(board[cr][cc] & CRYSTAL)) continue;
		evaluator.add(cr, cc, ol << (4 * cd), nl << (4 * cd));
		// debug("eval_rm_mirror:%d %d %d %d %d %x %x %x\n", r, c, i, cr, cc, board[cr][cc], ol, nl);
	}
	board[r][c] = old;
	return evaluator.calc() + CM;
}

void add_mirror(int r, int c, int type) {
	// debug("add_mirror:(%d %d)\n", r, c);
	assert(count_mirror < MM);
	++count_mirror;
	board[r][c] = type;
	const int rd = type == MIRROR_S ? 1 : 3;
	for (int dir = 0; dir < 4; ++dir) {
		int cd = dir;
		int cr = r + DR[dir];
		int cc = c + DC[dir];
		int color = (light[r][c] >> ((dir ^ rd) * 4)) & 0xF;
		while (true) {
			const int mask = 0xF << (cd * 4);
			const int col = color << (cd * 4);
			// debug("%d %d %d %d %x %x\n", cr, cc, cd, color, light[cr][cc], board[cr][cc]);
			light[cr][cc] &= ~mask;
			light[cr][cc] |= col;
			if (board[cr][cc] == OBST) break;
			if (is_crystal(board[cr][cc])) {
				const int cl = combined_light(cr, cc);
				if (overcolor_ps.contains(cr, cc)) {
					if ((cl & board[cr][cc]) == cl) {
						overcolor_ps.remove(cr, cc);
					}
				} else {
					if ((cl & board[cr][cc]) != cl) {
						overcolor_ps.add(cr, cc);
					}
				}
				break;
			}
			if (board[cr][cc] & LANTERN) {
				break;
			}
			if (board[cr][cc] == EMPTY) {
			} else {
				if (board[cr][cc] == MIRROR_S) {
					cd ^= 1;
				} else if (board[cr][cc] == MIRROR_B) {
					cd ^= 3;
				}
				if (cr == r && cc == c && cd == dir) {
					if (color == 0) break;
					color = 0; // clear loop
				}
			}
			cr += DR[cd];
			cc += DC[cd];
		}
	}
	update_dist_add(r, c);
}

void remove_mirror(int r, int c) {
	// debug("remove_mirror:%d %d %x\n", r, c, light[r][c]);
	--count_mirror;
	board[r][c] = EMPTY;
	for (int dir = 0; dir < 4; ++dir) {
		int cd = dir;
		int cr = r + DR[dir];
		int cc = c + DC[dir];
		int color = (light[r][c] >> (4 * dir)) & 0xF;
		while (true) {
			// debug("%d %d %d %d %x\n", cr, cc, cd, color, light[cr][cc]);
			// assert((light[cr][cc] & (color << (4 * cd))) == 0);
			light[cr][cc] &= ~(0xF << (4 * cd));
			light[cr][cc] |= (color << (4 * cd));
			if (board[cr][cc] == OBST) break;
			if (is_crystal(board[cr][cc])) {
				const int cl = combined_light(cr, cc);
				if (overcolor_ps.contains(cr, cc)) {
					if ((cl & board[cr][cc]) == cl) {
						overcolor_ps.remove(cr, cc);
					}
				} else {
					if ((cl & board[cr][cc]) != cl) {
						overcolor_ps.add(cr, cc);
					}
				}
				break;
			}
			if (board[cr][cc] & LANTERN) {
				break;
			}
			if (board[cr][cc] == MIRROR_S) {
				cd ^= 1;
			} else if (board[cr][cc] == MIRROR_B) {
				cd ^= 3;
			}
			if (cr == r && cc == c && cd == dir) {
				if (color == 0) break;
				color = 0; // clear loop
			}
			cr += DR[cd];
			cc += DC[cd];
		}
	}
	update_dist_remove(r, c);
}

int count_single_secondery() {
	int ret = 0;
	for (int i = 1; i <= H; ++i) {
		for (int j = 1; j <= W; ++j) {
			if (!is_crystal(board[i][j])) continue;
			if (__builtin_popcount(board[i][j]) != 2) continue;
			int l = combined_light(i, j);
			if (__builtin_popcount(l) == 1 && (board[i][j] & l) == l) {
				ret++;
			}
		}
	}
	return ret;
}

void validate(int cur_score) {
	bool fail = false;
	for (int i = 1; i <= H; ++i) {
		for (int j = 1; j <= W; ++j) {
			if (board[i][j] & LANTERN) continue;
			for (int dir = 0; dir < 4; ++dir) {
				int cd = dir;
				int cr = i;
				int cc = j;
				int color = 0;
				for (int k = 0; ; ++k) {
					cr += DR[cd];
					cc += DC[cd];
					if (board[cr][cc] == OBST || is_crystal(board[cr][cc])) break;
					if (board[cr][cc] & LANTERN) {
						color = board[cr][cc] - LANTERN;
						break;
					}
					if (board[cr][cc] == MIRROR_S) {
						cd ^= 1;
					} else if (board[cr][cc] == MIRROR_B) {
						cd ^= 3;
					}
					if (k > 100000) break;
				}
				if (((light[i][j] >> (4 * (dir ^ 2))) & 0xF) != color) {
					debug("invalid light:%d %d %d %d %x\n", i, j, dir, color, light[i][j]);
					fail = true;
				}
			}
		}
	}
	if (fail) {
		for (int i = 1; i <= H; ++i) {
			for (int j = 1; j <= W; ++j) {
				debug("%4x ", light[i][j]);
			}
			debugln();
		}
		assert(false);
		return;
	}
	int ok_prim = 0;
	int ok_seco = 0;
	int wrong = 0;
	int wrong_over = 0;
	int count_lantern = 0;
	for (int i = 1; i <= H; ++i) {
		for (int j = 1; j <= W; ++j) {
			if ((board[i][j] & LANTERN) && light[i][j]) {
				debug("lantern illuminated:%d %d\n", i, j);
				return;
			}
			if (board[i][j] & LANTERN) {
				++count_lantern;
			}
			if (!is_crystal(board[i][j])) continue;
			int l = combined_light(i, j);
			if (l == board[i][j]) {
				if (__builtin_popcount(board[i][j]) == 1) {
					ok_prim++;
				} else {
					ok_seco++;
				}
			} else if (l != 0) {
				wrong++;
				if ((board[i][j] & l) != l) wrong_over++;
			}
		}
	}
	int sc = ok_prim * 20 + ok_seco * 30 - wrong_over * 10 + (wrong - wrong_over) * single_value - CL * count_lantern - CO * count_obst - CM * count_mirror;
	debug("ok_prim:%d ok_seco:%d wrong:%d lantern:%d score:%d oversize:%d obst:%d mirror:%d\n",
	      ok_prim, ok_seco, wrong, count_lantern, sc, wrong_over, count_obst, count_mirror);
	// if (score != sc) return;
	assert(cur_score == sc);
	assert(overcolor_ps.size() == wrong_over);
}

int fix_invalid_crystal(int score) {
	Point p = overcolor_ps.pos[rnd.nextUInt(overcolor_ps.size())];
	int cl = combined_light(p.r, p.c);
	vector<Point> pos;
	assert(is_crystal(board[p.r][p.c]));
	assert(cl != board[p.r][p.c]);
	if ((cl & board[p.r][p.c]) != cl) {
		// remove light
		for (int dir = 0; dir < 4; ++dir) {
			int cd = dir;
			int l = (light[p.r][p.c] >> (4 * cd)) & 0xF;
			if (l != 0 && (l & board[p.r][p.c]) == 0) {
				int cr = p.r - DR[cd];
				int cc = p.c - DC[cd];
				while (true) {
					if (board[cr][cc] == OBST) break;
					if (board[cr][cc] == EMPTY) {
						if (count_mirror < MM || count_obst < MO) pos.push_back({(uint8_t)cr, (uint8_t)cc});
					} else if (board[cr][cc] == MIRROR_S || board[cr][cc] == MIRROR_B) {
						if (!from_both_side(light[cr][cc])) pos.push_back({(uint8_t)cr, (uint8_t)cc});
					} else {
						pos.push_back({(uint8_t)cr, (uint8_t)cc});
					}
					if (board[cr][cc] & LANTERN) {
						if (H * W < 1000) {
							pos.push_back({(uint8_t)cr, (uint8_t)cc});
						}
						break;
					}
					assert(!(board[cr][cc] & CRYSTAL));
					if (board[cr][cc] == MIRROR_S) {
						cd ^= 1;
					} else if (board[cr][cc] == MIRROR_B) {
						cd ^= 3;
					}
					cr -= DR[cd];
					cc -= DC[cd];
				}
			}
		}
		if (pos.empty()) assert(false);
		Point rp = pos[rnd.nextUInt(pos.size())];
		int diff = -INF;
		int type;
		if (board[rp.r][rp.c] & LANTERN) {
			type = board[rp.r][rp.c];
			diff = evaluate_remove_lantern(rp.r, rp.c);
		} else if (board[rp.r][rp.c] == MIRROR_S || board[rp.r][rp.c] == MIRROR_B) {
			type = board[rp.r][rp.c];
			diff = evaluate_remove_mirror(rp.r, rp.c);
		} else {
			assert(board[rp.r][rp.c] == EMPTY);
			int diff_o = count_obst < MO ? evaluate_change_obstacle<true>(rp.r, rp.c) : -INF;
			diff = count_mirror < MM ? evaluate_add_mirror(rp.r, rp.c, &type) : -INF;
			if (diff < diff_o) {
				type = OBST;
				diff = diff_o;
			}
		}
		if (diff == -INF) return score;
		if (accept(diff)) {
			score += diff;
			if (type & LANTERN) {
				remove_lantern(rp.r, rp.c);
			} else if (type == OBST) {
				add_obstacle(rp.r, rp.c);
			} else {
				if (board[rp.r][rp.c] == EMPTY) {
					add_mirror(rp.r, rp.c, type);
				} else {
					remove_mirror(rp.r, rp.c);
				}
			}
		}
	} else {
		// add light
		// for (int dir = 0; dir < 4; ++dir) {
		// 	int cd = dir;
		// 	int l = (light[p.r][p.c] >> (4 * cd)) & 0xFF;
		// 	if (l == 0) {
		// 		int cr = p.r - DR[cd];
		// 		int cc = p.c - DC[cd];
		// 		while (true) {
		// 			assert(!(board[cr][cc] & LANTERN));
		// 			if (board[cr][cc] == OBST) break;
		// 			if (board[cr][cc] & CRYSTAL) break;
		// 			if (board[cr][cc] == EMPTY) {
		// 				if (light[cr][cc] == 0) {
		// 					pos.push_back({(uint8_t)cr, (uint8_t)cc});
		// 					// pos.push_back({(uint8_t)cr, (uint8_t)cc});
		// 				} else {
		// 					if (count_mirror < MM || count_obst < MO) pos.push_back({(uint8_t)cr, (uint8_t)cc});
		// 				}
		// 			}
		// 			if (board[cr][cc] == MIRROR_S) {
		// 				cd ^= 1;
		// 			} else if (board[cr][cc] == MIRROR_B) {
		// 				cd ^= 3;
		// 			}
		// 			cr -= DR[cd];
		// 			cc -= DC[cd];
		// 		}
		// 	}
		// }
		// if (pos.empty()) return score;
		// Point rp = pos[rnd.nextUInt(pos.size())];
		// if (light[rp.r][rp.c] == 0) {
		// 	int type = 0;
		// 	int diff = evaluate_add_lantern(rp.r, rp.c, &type);
		// 	if (diff != -INF && accept(diff)) {
		// 		score += diff;
		// 		add_lantern(rp.r, rp.c, type - LANTERN);
		// 	}
		// } else {
		// 	int dir;
		// 	if (light[rp.r][rp.c] & 0xF) {
		// 		dir = 0;
		// 	} else if (light[rp.r][rp.c] & 0xF0) {
		// 		dir = 1;
		// 	} else if (light[rp.r][rp.c] & 0xF00) {
		// 		dir = 2;
		// 	} else {
		// 		dir = 3;
		// 	}
		// 	int cd = dir;
		// 	if (rnd.nextUInt() & 1) {
		// 		int cr = rp.r - DR[cd] * dist[rp.r][rp.c][cd ^ 2];
		// 		int cc = rp.c - DC[cd] * dist[rp.r][rp.c][cd ^ 2];
		// 		while ((board[cr][cc] & LANTERN) == 0) {
		// 			if (board[cr][cc] == MIRROR_S) {
		// 				cd ^= 1;
		// 			} else {
		// 				cd ^= 3;
		// 			}
		// 			cr -= DR[cd] * dist[cr][cc][cd ^ 2];;
		// 			cc -= DC[cd] * dist[cr][cc][cd ^ 2];;
		// 		}
		// 		int diff = evaluate_remove_lantern(cr, cc);
		// 		int removed_color = board[cr][cc] - LANTERN;
		// 		remove_lantern(cr, cc);
		// 		int type;
		// 		int diff2 = evaluate_add_lantern(rp.r, rp.c, &type);
		// 		if (accept(diff + diff2)) {
		// 			score += diff + diff2;
		// 			add_lantern(rp.r, rp.c, type - LANTERN);
		// 		} else {
		// 			add_lantern(cr, cc, removed_color);
		// 		}
		// 	} else {
		// 		int cr = rp.r - DR[cd];
		// 		int cc = rp.c - DC[cd];
		// 		while (board[cr][cc] != EMPTY) {
		// 			if (board[cr][cc] & LANTERN) break;
		// 			if (board[cr][cc] == MIRROR_S) {
		// 				cd ^= 1;
		// 			} else {
		// 				cd ^= 3;
		// 			}
		// 			cr -= DR[cd];
		// 			cc -= DC[cd];
		// 		}
		// 		if (board[cr][cc] & LANTERN) {
		// 			int diff = evaluate_remove_lantern(cr, cc);
		// 			int removed_color = board[cr][cc] - LANTERN;
		// 			remove_lantern(cr, cc);
		// 			int type;
		// 			int diff2 = evaluate_add_lantern(rp.r, rp.c, &type);
		// 			if (accept(diff + diff2)) {
		// 				score += diff + diff2;
		// 				add_lantern(rp.r, rp.c, type - LANTERN);
		// 			} else {
		// 				add_lantern(cr, cc, removed_color);
		// 			}
		// 		} else {
		// 			int nl = light[cr][cc];
		// 			if (count_mirror == MM || (count_obst < MO && (((nl & 0x0F0F) && (nl & 0xF0F0)) || (rnd.nextUInt() & 1)))) {
		// 				int diff = evaluate_change_obstacle<true>(cr, cc);
		// 				add_obstacle(cr, cc);
		// 				int type;
		// 				int diff2 = evaluate_add_lantern(rp.r, rp.c, &type);
		// 				if (accept(diff + diff2)) {
		// 					score += diff + diff2;
		// 					add_lantern(rp.r, rp.c, type - LANTERN);
		// 				} else {
		// 					remove_obstacle(cr, cc);
		// 				}
		// 			} else {
		// 				int mirror_type;
		// 				int diff = evaluate_add_mirror(cr, cc, &mirror_type);
		// 				if (diff != -INF) {
		// 					add_mirror(cr, cc, mirror_type);
		// 					if (light[rp.r][rp.c] != 0) {
		// 						remove_mirror(cr, cc);
		// 					} else {
		// 						int type;
		// 						int diff2 = evaluate_add_lantern(rp.r, rp.c, &type);
		// 						if (accept(diff + diff2)) {
		// 							score += diff + diff2;
		// 							add_lantern(rp.r, rp.c, type - LANTERN);
		// 						} else {
		// 							remove_mirror(cr, cc);
		// 						}
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }
	}
	return score;
}

int solve_sa(ll time_limit) {
	int score = 0;
	int best_score = 0;
	// vector<Point> items;
	vector<Point> positions;
	for (int i = 1; i <= H; ++i) {
		for (int j = 1; j <= W; ++j) {
			if (board[i][j] == EMPTY) {
				positions.push_back({(uint8_t)i, (uint8_t)j});
			}
		}
	}
	Point move_to = {};
	const ll begin_time = get_elapsed_msec();
	for (int turn = 1; ; ++turn) {
		if ((turn & 0xFFFF) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed >= time_limit) {
				debug("best_score:%d turn:%d\n", best_score == 0 ? score : best_score, turn);
				break;
			}
			double ratio = 1.0 * (elapsed - begin_time) / (time_limit - begin_time);
			double c1 = log(COOLER_INI);
			double c2 = log(COOLER_END);
			cooler = elapsed > time_limit - 20 ? 10 : exp(c1 + (c2 - c1) * ratio);
			int next_single_value = (int)floor(INITIAL_SINGLE_VALUE + (-10 - INITIAL_SINGLE_VALUE) * ratio);
			if (next_single_value != single_value) {
				int single_secondery_count = count_single_secondery();
				score += single_secondery_count * (next_single_value - single_value);
				debug("cooler:%.5f score:%d at turn %d\n", cooler, score + single_secondery_count * (-10 - next_single_value), turn);
				single_value = next_single_value;
				set_evaluation();
			}
		}
		if (overcolor_ps.size() > 0 && (rnd.nextUInt() & FIX_INVALID_RATIO) == 0) {
			score = fix_invalid_crystal(score);
			if (single_value == -10 && score > best_score) {
				best_score = score;
				for (int i = 1; i <= H; ++i) {
					copy(&board[i][0], &board[i][W + 2], &best_board[i][0]);
				}
			}
			continue;
		}
		Point pos = positions[rnd.nextUInt(positions.size())];
		int type = board[pos.r][pos.c];
		int diff;
		if (type == EMPTY) {
			if (light[pos.r][pos.c] == 0) {
				ADD_COUNTER(0);
				START_TIMER(0);
				assert(board[pos.r][pos.c] == EMPTY);
				assert(light[pos.r][pos.c] == 0);
				diff = evaluate_add_lantern(pos.r, pos.c, &type);
				STOP_TIMER(0);
				if (diff == -INF) continue;
				ADD_COUNTER(10);
			} else {
				if (count_obst == MO && count_mirror == MM) {
					ADD_COUNTER(7);
					continue;
				}
				if (count_mirror == MM || (count_obst < MO && (rnd.nextUInt() & 1))) {
					type = OBST;
					START_TIMER(1);
					diff = evaluate_change_obstacle<true>(pos.r, pos.c);
					STOP_TIMER(1);
					ADD_COUNTER(11);
				} else {
					ADD_COUNTER(2);
					START_TIMER(2);
					diff = evaluate_add_mirror(pos.r, pos.c, &type);
					STOP_TIMER(2);
					if (diff == -INF) continue;
					ADD_COUNTER(12);
				}
			}
		} else if (type & LANTERN) {
			if (rnd.nextUInt() & REMOVE_LANTERN_RATIO) {
				START_TIMER(3);
				diff = evaluate_remove_lantern(pos.r, pos.c);
				move_to.r = 0;
				STOP_TIMER(3);
				ADD_COUNTER(13);
			} else {
				ADD_COUNTER(4);
				START_TIMER(4);
				diff = evaluate_move_lantern(pos.r, pos.c, &move_to);
				STOP_TIMER(4);
				if (diff == -INF) continue;
				ADD_COUNTER(14);
			}
		} else if (type == OBST) {
			if (from_both_side(light[pos.r][pos.c])) {
				ADD_COUNTER(8);
				continue;
			}
			ADD_COUNTER(5);
			START_TIMER(5);
			diff = evaluate_change_obstacle<false>(pos.r, pos.c);
			STOP_TIMER(5);
			if (diff == -INF) continue;
			ADD_COUNTER(15);
		} else {
			if (from_both_side(light[pos.r][pos.c])) {
				ADD_COUNTER(8);
				continue;
			}
			ADD_COUNTER(6);
			START_TIMER(6);
			diff = evaluate_remove_mirror(pos.r, pos.c);
			STOP_TIMER(6);
			if (diff == -INF) continue;
			ADD_COUNTER(16);
		}
		if (accept(diff)) {
			score += diff;
			// debug("transit:%2d %2d %2x %2d %d\n", pos.r, pos.c, type, diff, turn);
			ADD_COUNTER(9);
			if (board[pos.r][pos.c] == EMPTY) {
				if (type & LANTERN) {
					START_TIMER(10);
					add_lantern(pos.r, pos.c, type - LANTERN);
					STOP_TIMER(10);
				} else if (type == OBST) {
					START_TIMER(11);
					add_obstacle(pos.r, pos.c);
					STOP_TIMER(11);
				} else {
					START_TIMER(12);
					add_mirror(pos.r, pos.c, type);
					STOP_TIMER(12);
				}
			} else if (board[pos.r][pos.c] & LANTERN) {
				START_TIMER(13);
				remove_lantern(pos.r, pos.c);
				STOP_TIMER(13);
				if (move_to.r != 0) {
					START_TIMER(14);
					add_lantern(move_to.r, move_to.c, type - LANTERN);
					STOP_TIMER(14);
				}
			} else if (board[pos.r][pos.c] == OBST) {
				START_TIMER(15);
				remove_obstacle(pos.r, pos.c);
				STOP_TIMER(15);
			} else {
				START_TIMER(16);
				remove_mirror(pos.r, pos.c);
				STOP_TIMER(16);
			}
			if (single_value == -10 && score > best_score) {
				// debug("score:%d at turn %d\n", score, turn);
				best_score = score;
				for (int i = 1; i <= H; ++i) {
					copy(&board[i][0], &board[i][W + 2], &best_board[i][0]);
				}
				// validate(score);
			}
		}
	}
	if (best_score == 0 && score > 0) {
		best_score = score;
		for (int i = 1; i <= H; ++i) {
			copy(&board[i][0], &board[i][W + 2], &best_board[i][0]);
		}
	}
	return best_score;
}

void solve() {
	const int SA_COUNT = max(1, SA_COUNT_RATIO / (H * W));
	int best_score = 0;
	ll before_time = get_elapsed_msec();
	vvi result_board(H + 2, vi(W + 2));
	for (int t = 0; t < SA_COUNT; ++t) {
		for (int i = 0; i < H + 2; ++i) {
			copy(&target[i][0], &target[i][W + 2], &board[i][0]);
			fill(&light[i][0], &light[i][W + 2], 0);
		}
		overcolor_ps.init();
		cooler = COOLER_INI;
		single_value = INITIAL_SINGLE_VALUE;
		count_obst = count_mirror = 0;
		set_evaluation();
		set_dist();
		int score = solve_sa((TL - before_time) * (t + 1) / SA_COUNT);
		if (score > best_score) {
			best_score = score;
			for (int i = 1; i <= H; ++i) {
				copy(&best_board[i][0], &best_board[i][W + 2], result_board[i].begin());
			}
		}
	}
	debug("final_score:%d\n", best_score);
	for (int i = 1; i <= H; ++i) {
		copy(result_board[i].begin(), result_board[i].end(), &board[i][0]);
	}
}

class CrystalLighting {
public:
	vector<string> placeItems(const vector<string>& tBoard, int costL, int costM, int costO, int maxM, int maxO);
};

vector<string> CrystalLighting::placeItems(const vector<string>& tBoard, int costL, int costM, int costO, int maxM, int maxO) {
	start_time = get_elapsed_msec();
	H = tBoard.size();
	W = tBoard[0].size();
	CL = costL;
	CM = costM;
	CO = costO;
	MM = maxM;
	MO = maxO;
	fill(&target[0][0], &target[0][W + 2], OBST);
	fill(&target[H + 1][0], &target[H + 1][W + 2], OBST);
	for (int i = 0; i < H; ++i) {
		target[i + 1][0] = target[i + 1][W + 1] = OBST;
		for (int j = 0; j < W; ++j) {
			if (tBoard[i][j] == '.') {
				target[i + 1][j + 1] = EMPTY;
			} else if (tBoard[i][j] == 'X') {
				target[i + 1][j + 1] = OBST;
			} else {
				target[i + 1][j + 1] = tBoard[i][j] - '0';
			}
		}
	}
	solve();
	vector<string> ans;
	for (int i = 1; i <= H; ++i) {
		for (int j = 1; j <= W; ++j) {
			char type = 0;
			if (board[i][j] & LANTERN) {
				type = (char)('0' + (board[i][j] & 0xF));
			} else if (board[i][j] == MIRROR_S) {
				type = '/';
			} else if (board[i][j] == MIRROR_B) {
				type = '\\';
			} else if (board[i][j] == OBST && target[i][j] != OBST) {
				type = 'X';
			} else {
				continue;
			}
			stringstream stm;
			stm << (i - 1) << " " << (j - 1) << " " << type;
			ans.push_back(stm.str());
		}
	}
	return ans;
}


#ifdef LOCAL
int main() {
	CrystalLighting cl;
	int H;
	cin >> H;
	vector<string> targetBoard(H);
	for (int i = 0; i < H; ++i) {
		cin >> targetBoard[i];
	}
	int costLantern, costMirror, costObstacle, maxMirrors, maxObstacles;
	cin >> costLantern >> costMirror >> costObstacle >> maxMirrors >> maxObstacles;
	vector<string> ret = cl.placeItems(targetBoard, costLantern, costMirror, costObstacle, maxMirrors, maxObstacles);
	cout << ret.size() << endl;
	for (int i = 0; i < (int)ret.size(); ++i) {
		cout << ret[i] << endl;
	}
	PRINT_COUNTER();
	PRINT_TIMER();
}
#endif
