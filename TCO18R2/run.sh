#!/bin/sh -x

for i in 4 5 6 8
do
	date
	g++48 -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL -DINITIAL_SINGLE_VALUE_V="$i" -pipe -mmmx -msse -msse2 -msse3 -o tester CrystalLighting.cpp
	java Tester -b 1000 -e 1999 > log15_isv_"$i".txt
done
