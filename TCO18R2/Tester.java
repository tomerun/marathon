import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SplittableRandom;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

class Item {
  int r, c;
  char type;
  boolean valid;

  Item(int R, int C, char Type) {
    r = R;
    c = C;
    type = Type;
  }

  boolean isLantern() {
    return type == '1' || type == '2' || type == '4';
  }

  boolean isMirror() {
    return type == '\\' || type == '/';
  }

  boolean isObstacle() {
    return type == 'X';
  }

  int getColor() {
    if (isLantern()) return type - '0';
    return 0;
  }
}

class Ray {
  int r0, c0, r1, c1, dr, dc;
  Item source;

  Ray(int r0, int c0, int r1, int c1, int dr, int dc, Item source) {
    this.r0 = r0;
    this.c0 = c0;
    this.r1 = r1;
    this.c1 = c1;
    this.dr = dr;
    this.dc = dc;
    this.source = source;
  }
}

public class Tester {
  private static final char[] types = new char[]{'1', '2', '4', '\\', '/', 'X'};
  private static final int minS = 10, maxS = 100;
  private static final int minLanternCost = 1, maxLanternCost = 10;
  private static final int minMirrorCost = 3, maxMirrorCost = 30;
  private static final int minObstacleCost = 2, maxObstacleCost = 20;
  private static final int invalidScore = -1000000;

  static int myH, myW;
  private int H, W;
  private int costLantern, costMirror, costObstacle;
  private int maxMirrors, maxObstacles;
  private int numCrystals, numCorrectPrimary, numCorrectSecondary, numIncorrect;
  private int pObstacle, pCrystal;
  private volatile char[][] targetBoard;  // valid crystal colors are 1..6 (RYB color model), X is an obstacle
  private volatile List<Item> addedItems;
  private volatile List<Ray> rays;
  private volatile char[][] resultBoard;  // marks the results of placing the lanterns and lighting the crystals up

  private boolean isInside(int r, int c) {
    return (r >= 0 && r < H && c >= 0 && c < W);
  }

  private int getTypeIdx(char type) {
    for (int i = 0; i < types.length; i++) {
      if (types[i] == type) return i;
    }
    return -1;
  }

  private void generate() throws Exception {
    SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
    r1.setSeed(seed);
    H = r1.nextInt(maxS - minS + 1) + minS;
    W = r1.nextInt(maxS - minS + 1) + minS;
    if (seed == 1) W = H = minS;
    else if (seed == 2) W = H = (minS + maxS) / 2;
    else if (seed == 3) W = H = maxS;
    if (1000 <= seed && seed < 2000) {
      SplittableRandom sr = new SplittableRandom(42);
      int[] size = new int[1000];
      for (int i = 0; i < 1000; ++i) {
        int h = sr.nextInt(maxS - minS + 1) + minS;
        int w = sr.nextInt(maxS - minS + 1) + minS;
        size[i] = ((h * w) << 16) | (h << 8) | w;
      }
      Arrays.sort(size);
      H = (size[(int) seed - 1000] >> 8) & 0xFF;
      W = size[(int) seed - 1000] & 0xFF;
    }
    if (myH != 0) H = myH;
    if (myW != 0) W = myW;
    pObstacle = r1.nextInt(11) + 5;
    pCrystal = r1.nextInt(11) + 15;
    targetBoard = new char[H][W];
    numCrystals = 0;
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c) {
        int t = r1.nextInt(100);
        targetBoard[r][c] = (t < pCrystal ? (char) (r1.nextInt(6) + 1 + '0') : (t < pCrystal + pObstacle ? 'X' : '.'));
        if (t < pCrystal) numCrystals++;
      }
    costLantern = r1.nextInt(maxLanternCost - minLanternCost + 1) + minLanternCost;
    costMirror = r1.nextInt(maxMirrorCost - minMirrorCost + 1) + minMirrorCost;
    costObstacle = r1.nextInt(maxObstacleCost - minObstacleCost + 1) + minObstacleCost;
    maxMirrors = r1.nextInt(numCrystals / 8 + 1);
    maxObstacles = r1.nextInt(numCrystals / 16 + 1);
    if (seed == 1) maxMirrors = maxObstacles = 3;
  }

  private void calculateLighting() {
    // recalculate all lighting from scratch
    // mark obstacles and empty places (crystals start empty)
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c) {
        if (targetBoard[r][c] == '.' || targetBoard[r][c] == 'X')
          resultBoard[r][c] = targetBoard[r][c];
        else
          resultBoard[r][c] = '0';
      }
    //Place all added items
    rays.clear();
    for (Item item : addedItems) {
      resultBoard[item.r][item.c] = item.type;
      if (item.isLantern()) {
        item.valid = true;
        for (int dir = 0; dir < 4; dir++) {
          int dr = dir == 0 ? 1 : dir == 1 ? -1 : 0;
          int r1 = item.r + dr;
          int dc = dir == 2 ? 1 : dir == 3 ? -1 : 0;
          int c1 = item.c + dc;
          rays.add(new Ray(item.r, item.c, r1, c1, dr, dc, item));
        }
      }
    }
    //Expand rays, checking for obstacles, crystals or other added items
    //Here is checked if each lantern is valid (i.e. do not light another lantern)
    int currRay = 0;
    while (currRay < rays.size()) {
      Ray ray = rays.get(currRay++);
      if (!isInside(ray.r1, ray.c1)) continue;
      char res = resultBoard[ray.r1][ray.c1];
      char tgt = targetBoard[ray.r1][ray.c1];
      int dr = ray.dr;
      int dc = ray.dc;
      if (res == 'X') {
        continue;
      } else if (res == '/') {
        dr = -ray.dc;
        dc = -ray.dr;
      } else if (res == '\\') {
        dr = ray.dc;
        dc = ray.dr;
      } else if (tgt != '.') {
        resultBoard[ray.r1][ray.c1] = (char) (((res - '0') | (ray.source.type - '0')) + '0');
        continue;
      } else if (res != '.') {
        ray.source.valid = false;
        continue;
      }
      rays.add(new Ray(ray.r1, ray.c1, ray.r1 + dr, ray.c1 + dc, dr, dc, ray.source));
    }
  }

  private String removeOneItem(int R, int C) {
    if (!isInside(R, C)) {
      return "You can only remove items from within the board.";
    }
    for (int i = 0; i < addedItems.size(); ++i) {
      Item item = addedItems.get(i);
      if (item.r == R && item.c == C) {
        addedItems.remove(i);
        resultBoard[item.r][item.c] = '.';
        return "";
      }
    }
    return "You can only remove items already placed on the board.";
  }

  private void placeOneItem(int R, int C, char type) {
    if (!isInside(R, C)) {
      throw new RuntimeException("You can only place items within the board.");
    }
    if (getTypeIdx(type) < 0) {
      throw new RuntimeException("Invalid item type: " + type + ".");
    }
    if (targetBoard[R][C] != '.') {
      throw new RuntimeException("You can only place items on empty cells of the board.");
    }
    if (resultBoard[R][C] != '.') {
      throw new RuntimeException("You can not place two items on the same cell.");
    }
    resultBoard[R][C] = type;
    if (type == 'X') {
      int usedObstacles = 0;
      for (Item item : addedItems)
        if (item.isObstacle()) usedObstacles++;
      if (usedObstacles >= maxObstacles)
        throw new RuntimeException("You can place at most " + maxObstacles + " obstacles.");
    } else if (type == '/' || type == '\\') {
      int usedMirrors = 0;
      for (Item item : addedItems)
        if (item.isMirror()) usedMirrors++;
      if (usedMirrors >= maxMirrors)
        throw new RuntimeException("You can place at most " + maxMirrors + " mirrors.");
    }
    addedItems.add(new Item(R, C, type));
  }

  private int getScore(Result result) {
    for (Item item : addedItems)
      if (item.isLantern() && !item.valid)
        return invalidScore;

    numCorrectPrimary = numCorrectSecondary = numIncorrect = 0;
    int score = 0;
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c) {
        char tgt = targetBoard[r][c];
        char res = resultBoard[r][c];
        if (tgt == 'X') continue;
        if (tgt == '.') {
          if (res == '/' || res == '\\') {
            if (result != null) result.cm++;
            score -= costMirror;
          } else if (res == 'X') {
            if (result != null) result.co++;
            score -= costObstacle;
          } else if (res == '1' || res == '2' || res == '4') {
            if (result != null) result.cl++;
            score -= costLantern;
          }
          continue;
        }
        if (tgt == res) {
          if (tgt == '1' || tgt == '2' || tgt == '4') {
            score += 20;
            numCorrectPrimary++;
            if (result != null) result.ok1++;
          } else {
            score += 30;
            numCorrectSecondary++;
            if (result != null) result.ok2++;
          }
        } else {
          if (res != '0') {
            score -= 10;
            numIncorrect++;
            if (result != null) {
              int tn = tgt - '0';
              int rn = res - '0';
              if ((tn & rn) == rn) {
                result.ng1++;
              } else {
                result.ng2++;
              }
            }
          }
        }
      }
    if (result != null) {
      result.score = score;
      result.no = numCrystals - numCorrectPrimary - numCorrectSecondary - numIncorrect;
    }
    return score;
  }

  private Result runTest() {
    Result res = new Result();
    res.seed = this.seed;
    try {
      generate();
      res.H = H;
      res.W = W;
      res.CL = this.costLantern;
      res.CM = this.costMirror;
      res.CO = this.costObstacle;
      res.MM = this.maxMirrors;
      res.MO = this.maxObstacles;
      res.PC = this.pCrystal;
      res.PO = this.pObstacle;

      addedItems = new ArrayList<>();
      rays = new ArrayList<>();
      resultBoard = new char[H][W];
      calculateLighting();
      if (vis) {
        jf.setVisible(true);
        Insets frameInsets = jf.getInsets();
        int fw = frameInsets.left + frameInsets.right + 8;
        int fh = frameInsets.top + frameInsets.bottom + 8;
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        Insets screenInsets = toolkit.getScreenInsets(jf.getGraphicsConfiguration());
        screenSize.width -= screenInsets.left + screenInsets.right;
        screenSize.height -= screenInsets.top + screenInsets.bottom;
        if (SZ == 0) {
          SZ = Math.min((screenSize.width - fw - 120) / W, (screenSize.height - fh) / H);
          if (!plain && SZ < 20) plain = true;
        }
        Dimension dim = v.getVisDimension();
        v.setPreferredSize(dim);
        jf.setSize(Math.min(dim.width + fw, screenSize.width), Math.min(dim.height + fh, screenSize.height));
        manualReady = false;
        draw();
      }

      String[] targetBoardArg = new String[H];
      for (int i = 0; i < H; ++i) {
        targetBoardArg[i] = new String(targetBoard[i]);
      }

      long beforeTime = System.currentTimeMillis();
      String[] itemsRet = placeItems(targetBoardArg, costLantern, costMirror, costObstacle, maxMirrors, maxObstacles);
      res.elapsed = System.currentTimeMillis() - beforeTime;
      if (itemsRet.length > W * H) {
        throw new RuntimeException("Your return contained more than " + (W * H) + " elements.");
      }
      for (int i = 0; i < itemsRet.length; ++i) {
        String[] s = itemsRet[i].split(" ");
        if (s.length != 3) {
          throw new RuntimeException("Item " + i + ": Each element of your return must be formatted as \"ROW COL TYPE\"");
        }
        int R = Integer.parseInt(s[0]);
        int C = Integer.parseInt(s[1]);
        if (s[2].length() != 1) {
          throw new RuntimeException("Item " + i + ": Invalid item type: " + s[2] + ". Item type must be a single character.");
        }
        char type = s[2].charAt(0);
        placeOneItem(R, C, type);
      }
      calculateLighting();
      if (vis) draw();
      for (int i = 0; i < itemsRet.length; ++i) {
        Item item = addedItems.get(i);
        if (item.isLantern() && !item.valid) {
          throw new RuntimeException("Item " + i + ": A lantern should not be illuminated by any light ray.");
        }
      }
      if (manual) {
        // wait till player finishes (possibly on top of automated return)
        while (!manualReady) {
          try {
            Thread.sleep(50);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      getScore(res);
    } catch (Exception e) {
      e.printStackTrace();
      res.score = invalidScore;
    } finally {
      proc.destroy();
    }
    return res;
  }

  private long seed;
  private JFrame jf;
  private Vis v;
  private static boolean vis, manual, plain, mark, showRays, save;
  private Process proc;
  private static int SZ;
  private volatile boolean manualReady;

  private String[] placeItems(String[] targetBoard, int costL, int costM, int costO, int maxM, int maxO) throws IOException {
    OutputStream os = proc.getOutputStream();
    InputStream is = proc.getInputStream();
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();
    sb.append(H).append("\n");
    for (int i = 0; i < H; ++i) {
      sb.append(targetBoard[i]).append("\n");
    }
    sb.append(costL).append("\n");
    sb.append(costM).append("\n");
    sb.append(costO).append("\n");
    sb.append(maxM).append("\n");
    sb.append(maxO).append("\n");
    os.write(sb.toString().getBytes());
    os.flush();

    int N = Integer.parseInt(br.readLine());
    String[] ret = new String[N];
    for (int i = 0; i < N; i++)
      ret[i] = br.readLine();
    return ret;
  }

  private void draw() {
    if (!vis) return;
    v.repaint();
  }

  // RYB: 001 = blue,  010 = yellow, 100 = red,
  //      011 = green, 101 = violet, 110 = orange
  //      000 = white (not drawn)
  //                   white     blue      yellow    green     red       violet    orange    brown
  private int[] lightColor = {0xFFFFFF, 0x0066FF, 0xF0F000, 0x33FF33, 0xFF4D4D, 0xE600E6, 0xFFAD31, 0x663300};
  private int[] targetColor = {0xFFFFFF, 0x0000CC, 0xE0E000, 0x00EE00, 0xEE0000, 0xAA00AA, 0xFF9900, 0x663300};

  private class Vis extends JPanel implements MouseListener {
    private String filename;

    public void paint(Graphics g) {
      super.paint(g);
      Dimension dim = getVisDimension();
      BufferedImage bi = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      // background
      g2.setColor(new Color(0xDDDDDD));
      g2.fillRect(0, 0, dim.width, dim.height);
      // board
      g2.setColor(Color.WHITE);
      g2.fillRect(0, 0, W * SZ, H * SZ);
      g2.setBackground(Color.WHITE);

      // mark crystal cells with correct (green) and wrong (red) colors.
      if (mark) {
        for (int r = 0; r < H; ++r) {
          for (int c = 0; c < W; ++c) {
            if (targetBoard[r][c] != '.' && targetBoard[r][c] != 'X') {
              if (targetBoard[r][c] == resultBoard[r][c]) {
                g2.setColor(new Color(0xBBFFBB));
                g2.fillRect(c * SZ + 1, r * SZ + 1, SZ - 1, SZ - 1);
              } else if (targetBoard[r][c] != resultBoard[r][c] && resultBoard[r][c] != '0') {
                g2.setColor(new Color(0xFFBBBB));
                g2.fillRect(c * SZ + 1, r * SZ + 1, SZ - 1, SZ - 1);
              }
            }
          }
        }
      }
      // Highlight invalid lanterns (illuminated by other lantern or by itself)
      g2.setColor(Color.RED);
      for (Item item : addedItems) {
        if (item.isLantern() && !item.valid) {
          g2.drawRect(item.c * SZ + 1, item.r * SZ + 1, SZ - 2, SZ - 2);
          if (SZ > 15)
            g2.drawRect(item.c * SZ + 2, item.r * SZ + 2, SZ - 4, SZ - 4);
        }
      }

      // Rays from lanterns
      if (showRays) {
        if (SZ > 10) g2.setStroke(new BasicStroke(2f));
        for (Ray ray : rays) {
          g2.setColor(new Color(lightColor[ray.source.type - '0']));
          g2.drawLine(ray.c0 * SZ + SZ / 2, ray.r0 * SZ + SZ / 2, Math.min(W * SZ, ray.c1 * SZ + SZ / 2), Math.min(H * SZ, ray.r1 * SZ + SZ / 2));
        }
      }

      // actual colors of the crystals (shown as color of their interior)
      for (int r = 0; r < H; ++r)
        for (int c = 0; c < W; ++c) {
          if (targetBoard[r][c] != '.' && targetBoard[r][c] != 'X' && resultBoard[r][c] != '.') {
            int r0 = r * SZ + SZ / 2, c0 = c * SZ + SZ / 2;
            g2.setColor(new Color(lightColor[resultBoard[r][c] - '0']));
            g2.fillPolygon(new int[]{c0 - SZ / 3, c0, c0 + SZ / 3, c0 + SZ / 6, c0 - SZ / 6, c0 - SZ / 3},
                new int[]{r0, r0 + SZ / 3, r0, r0 - SZ / 6, r0 - SZ / 6, r0}, 5);
          }
        }

      if (SZ > 10)
        g2.setStroke(new BasicStroke(1.5f));
      for (int i = 0; i < addedItems.size(); ++i) {
        Item item = addedItems.get(i);
        if (item.isLantern()) {
          // lanterns as suns of the given color
          int r0 = item.r * SZ + SZ / 2, c0 = item.c * SZ + SZ / 2, d = (int) (SZ / 1.0 / 4);
          g2.setColor(new Color(targetColor[item.getColor()]));
          g2.drawLine(c0 - SZ / 3, r0, c0 + SZ / 3, r0);
          g2.drawLine(c0, r0 - SZ / 3, c0, r0 + SZ / 3);
          g2.drawLine(c0 - d, r0 - d, c0 + d, r0 + d);
          g2.drawLine(c0 - d, r0 + d, c0 + d, r0 - d);
          g2.drawOval(c0 - SZ / 6, r0 - SZ / 6, SZ / 3, SZ / 3);
          g2.setColor(new Color(lightColor[item.getColor()]));
          g2.fillOval(c0 - SZ / 6, r0 - SZ / 6, SZ / 3, SZ / 3);
          continue;
        }
        g2.setColor(Color.DARK_GRAY);
        if (item.type == 'X') {
          //Added obstacles, as a small square in the cell center
          g2.clearRect(item.c * SZ + SZ / 3, item.r * SZ + SZ / 3, SZ / 3, SZ / 3);
          g2.drawRect(item.c * SZ + SZ / 3, item.r * SZ + SZ / 3, SZ / 3, SZ / 3);
          g2.drawLine(item.c * SZ + SZ / 3, item.r * SZ + SZ / 3, item.c * SZ + 2 * SZ / 3, item.r * SZ + 2 * SZ / 3);
          g2.drawLine(item.c * SZ + 2 * SZ / 3, item.r * SZ + SZ / 3, item.c * SZ + SZ / 3, item.r * SZ + 2 * SZ / 3);
        } else if (item.type == '\\') {
          //Mirror, as a diagonal line
          g2.drawLine(item.c * SZ, item.r * SZ, (item.c + 1) * SZ, (item.r + 1) * SZ);
        } else if (item.type == '/') {
          //Mirror, as a diagonal line
          g2.drawLine(item.c * SZ, (item.r + 1) * SZ, (item.c + 1) * SZ, item.r * SZ);
        }
      }

      // finally, the required colors of the crystals (shown as color of their outline)
      for (int r = 0; r < H; ++r)
        for (int c = 0; c < W; ++c) {
          if (targetBoard[r][c] == '.')
            continue;
          if (targetBoard[r][c] == 'X') {
            g2.setColor(Color.DARK_GRAY);
            g2.fillRect(c * SZ + 1, r * SZ + 1, SZ - 1, SZ - 1);
          } else {
            int r0 = r * SZ + SZ / 2, c0 = c * SZ + SZ / 2;
            g2.setColor(new Color(targetColor[targetBoard[r][c] - '0']));
            g2.drawPolyline(new int[]{c0 - SZ / 3, c0, c0 + SZ / 3, c0 + SZ / 6, c0 - SZ / 6, c0 - SZ / 3},
                new int[]{r0, r0 + SZ / 3, r0, r0 - SZ / 6, r0 - SZ / 6, r0,}, 6);
            if (!plain) {
              g2.drawPolyline(new int[]{c0 - SZ / 3, c0 + SZ / 3}, new int[]{r0, r0}, 2);
              g2.drawPolyline(new int[]{c0 + SZ / 6, c0, c0 - SZ / 6, c0, c0 + SZ / 6},
                  new int[]{r0, r0 + SZ / 3, r0, r0 - SZ / 6, r0}, 5);
            }
          }
        }

      // lines between cells
      g2.setStroke(new BasicStroke(1f));
      g2.setColor(Color.BLACK);
      for (int i = 0; i <= H; i++)
        g2.drawLine(0, i * SZ, W * SZ, i * SZ);
      for (int i = 0; i <= W; i++)
        g2.drawLine(i * SZ, 0, i * SZ, H * SZ);

      g2.setFont(new Font("Arial", Font.BOLD, 13));

      // "buttons" to control visualization options
      int xText = SZ * W + 10;
      int wText = 100;
      int yText = 10;
      int hButton = 30;
      int hText = 20;
      int vGap = 10;

      if (manualReady) g2.clearRect(xText, yText, wText, hButton);
      drawString(g2, "READY", xText, yText, wText, hButton, 0);
      g2.drawRect(xText, yText, wText, hButton);
      yText += hButton + vGap;

      if (plain) g2.clearRect(xText, yText, wText, hButton);
      drawString(g2, "PLAIN", xText, yText, wText, hButton, 0);
      g2.drawRect(xText, yText, wText, hButton);
      yText += hButton + vGap;

      if (mark) g2.clearRect(xText, yText, wText, hButton);
      drawString(g2, "MARK", xText, yText, wText, hButton, 0);
      g2.drawRect(xText, yText, wText, hButton);
      yText += hButton + vGap;

      if (showRays) g2.clearRect(xText, yText, wText, hButton);
      drawString(g2, "RAYS", xText, yText, wText, hButton, 0);
      g2.drawRect(xText, yText, wText, hButton);
      yText += hButton + vGap;

      // current score
      drawString(g2, "SCORE", xText, yText, wText, hText, 0);
      yText += hText;
      drawString(g2, String.format("%d", (int) getScore(null)), xText, yText, wText, hText, 0);
      yText += hText * 2;

      // other information about the current test case / state.
      // Item costs
      drawString(g2, "COSTS", xText, yText, wText, hText, 0);
      yText += hText;
      drawString(g2, "Lantern:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(costLantern), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText;
      drawString(g2, "Mirror:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(costMirror), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText;
      drawString(g2, "Obstacle:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(costObstacle), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText * 2;

      // Number of items (Used / Limit)
      int numLantern = 0;
      int numMirror = 0;
      int numObstacle = 0;
      int numInvalidLanterns = 0;
      for (Item item : addedItems) {
        if (item.isLantern()) {
          numLantern++;
          if (!item.valid) numInvalidLanterns++;
        } else if (item.isMirror())
          numMirror++;
        else if (item.isObstacle())
          numObstacle++;
      }
      drawString(g2, "ADDED ITEMS", xText, yText, wText, hText, 0);
      yText += hText;
      drawString(g2, "Lanterns:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(numLantern), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText;
      drawString(g2, "Invalid:", xText, yText, wText * 2 / 3, hText, 1);
      if (numInvalidLanterns > 0) g2.setColor(Color.RED);
      drawString(g2, String.valueOf(numInvalidLanterns), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      g2.setColor(Color.BLACK);
      yText += hText;
      drawString(g2, "Mirrors:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, numMirror + "/" + maxMirrors, xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText;
      drawString(g2, "Obstacles:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, numObstacle + "/" + maxObstacles, xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText * 2;

      // Number of crystals
      drawString(g2, "CRYSTALS", xText, yText, wText, hText, 0);
      yText += hText;
      drawString(g2, "Total:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(numCrystals), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText;
      drawString(g2, "Prim.OK:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(numCorrectPrimary), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText;
      drawString(g2, "Sec.OK:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(numCorrectSecondary), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);
      yText += hText;
      drawString(g2, "Incorrect:", xText, yText, wText * 2 / 3, hText, 1);
      drawString(g2, String.valueOf(numIncorrect), xText + wText * 2 / 3 + 2, yText, wText / 3, hText, -1);

      if (filename != null) {
        try {
          ImageIO.write(bi, "png", new File(filename));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      g.drawImage(bi, 0, 0, null);
    }

    void drawString(Graphics2D g2, String text, int x, int y, int w, int h, int align) {
      FontMetrics metrics = g2.getFontMetrics();
      Rectangle2D rect = metrics.getStringBounds(text, g2);
      int th = (int) (rect.getHeight());
      int tw = (int) (rect.getWidth());
      if (align == 0) x = x + (w - tw) / 2;
      else if (align > 0) x = (x + w) - tw;
      y = y + (h - th) / 2 + metrics.getAscent();
      g2.drawString(text, x, y);
    }

    Vis(String filename) {
      this.filename = filename;
      addMouseListener(this);
      jf.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
          if (proc != null) proc.destroy();
          System.exit(0);
        }
      });
    }

    Dimension getVisDimension() {
      return new Dimension(W * SZ + 126, Math.max(H * SZ + 1, 550));
    }

    public void mousePressed(MouseEvent e) {
      // Treat "plain", "mark" and "rays" buttons
      int x = e.getX() - SZ * W - 10, y = e.getY() - 10;
      if (x >= 0 && x <= 100 && y >= 40 && y <= 70) {
        plain = !plain;
        repaint();
        return;
      }
      if (x >= 0 && x <= 100 && y >= 80 && y <= 110) {
        mark = !mark;
        repaint();
        return;
      }
      if (x >= 0 && x <= 100 && y >= 120 && y <= 150) {
        showRays = !showRays;
        repaint();
        return;
      }

      // for manual play
      if (!manual || manualReady) return;

      // "ready" button submits current state of the board
      if (x >= 0 && x <= 100 && y >= 0 && y <= 30) {
        manualReady = true;
        repaint();
        return;
      }

      // regular click places a new item, modifies an existing one or removes it
      int row = e.getY() / SZ, col = e.getX() / SZ;
      // convert to args only clicks with valid coordinates
      if (!isInside(row, col))
        return;
      // ignore clicks on obstacles and crystals (can't place an item there or remove it)
      if (targetBoard[row][col] != '.' && SwingUtilities.isLeftMouseButton(e)) {
        System.err.println("You can only place lanterns on empty cells of the board.");
        return;
      }

      String errmes;
      // decide whether we're removing an item or adding/modifying one
      if (SwingUtilities.isRightMouseButton(e)) {
        // right click removes an item
        errmes = removeOneItem(row, col);
        if (!errmes.equals("")) {
          System.err.println(errmes);
          return;
        }
      }
      if (SwingUtilities.isLeftMouseButton(e)) {
        // left click adds a new item or changes the existing one
        // changing is implemented as removal followed by addition
        char type = 0;
        for (int i = 0; i < addedItems.size(); ++i) {
          Item item = addedItems.get(i);
          if (item.r == row && item.c == col) {
            int idx = getTypeIdx(item.type);
            //Check if the next item type is available
            int usedObstacles = 0;
            int usedMirrors = 0;
            for (Item a : addedItems) {
              if (a.isObstacle()) usedObstacles++;
              else if (a.isMirror()) usedMirrors++;
            }
            while (true) {
              type = types[++idx % types.length];
              if (type == 'X' && usedObstacles >= maxObstacles) continue; //All obstacles used
              if ((type == '\\' || type == '/') && item.type != '\\' && item.type != '/' && usedMirrors >= maxMirrors)
                continue;  //All mirrors used
              break; //OK, accept this type
            }
          }
        }
        if (type != 0) {
          // remove current item
          errmes = removeOneItem(row, col);
          if (!errmes.equals("")) {
            System.err.println(errmes);
            return;
          }
        } else {
          //Initial type, in case of adding new item
          type = types[0];
        }
        placeOneItem(row, col, type);
      }
      calculateLighting();
      repaint();
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
  }

  private Tester(long seed) throws Exception {
    this.seed = seed;
    if (vis) {
      jf = new JFrame();
      jf.setTitle("Seed " + seed);
      v = new Vis(save ? seed + ".png" : null);
      JScrollPane sp = new JScrollPane(v);
      jf.getContentPane().add(sp);
    }
    Runtime rt = Runtime.getRuntime();
    proc = rt.exec("./tester");
    new ErrorReader(proc.getErrorStream()).start();
  }

  private static final int THREAD_COUNT = 1;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-vis")) vis = true;
      if (args[i].equals("-manual")) manual = true;
      if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
      if (args[i].equals("-H")) myH = Integer.parseInt(args[++i]);
      if (args[i].equals("-W")) myW = Integer.parseInt(args[++i]);
      if (args[i].equals("-plain")) plain = true;
      if (args[i].equals("-mark")) mark = true;
      if (args[i].equals("-rays")) showRays = true;
      if (args[i].equals("-save")) save = true;
    }
    if (manual) vis = true;

    if (begin != -1 && end != -1) {
      vis = false;
      BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
      for (long i = begin; i <= end; ++i) {
        q.add(i);
      }
      Result[] results = new Result[(int) (end - begin + 1)];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i] = new TestThread(q, results, begin);
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / (end - begin + 1)));
    } else {
      Tester tester = new Tester(seed);
      Result res = tester.runTest();
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    long seedMin;
    Result[] results;
    BlockingQueue<Long> q;

    TestThread(BlockingQueue<Long> q, Result[] results, long seedMin) {
      this.q = q;
      this.results = results;
      this.seedMin = seedMin;
    }

    public void run() {
      while (true) {
        Long seed = q.poll();
        if (seed == null) break;
        try {
          Tester f = new Tester(seed);
          Result res = f.runTest();
          results[(int) (seed - seedMin)] = res;
        } catch (Exception e) {
          e.printStackTrace();
          Result res = new Result();
          res.seed = seed;
          results[(int) (seed - seedMin)] = res;
        }
      }
    }
  }
}

class Result {
  long seed;
  int H, W, CL, CM, CO, MM, MO, PO, PC;
  int ok1, ok2, ng1, ng2, no, cl, cm, co;
  int score;
  long elapsed;

  public String toString() {
    String ret = String.format("seed:%4d\n", seed);
    ret += String.format("H:%3d W:%3d CL:%2d CM:%2d CO:%2d MM:%3d MO:%3d PO:%2d PC:%2d\n", H, W, CL, CM, CO, MM, MO, PO, PC);
    ret += String.format("ok1:%3d ok2:%3d ng1:%3d ng2:%3d no:%3d cl:%4d cm:%3d co:%3d\n", ok1, ok2, ng1, ng2, no, cl, cm, co);
    ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
    ret += String.format("score:%d", score);
    return ret;
  }
}

class ErrorReader extends Thread {
  private InputStream error;

  ErrorReader(InputStream is) {
    error = is;
  }

  public void run() {
    try {
      byte[] ch = new byte[50000];
      int read;
      while ((read = error.read(ch)) > 0) {
        String s = new String(ch, 0, read);
        System.err.print(s);
        System.err.flush();
      }
    } catch (Exception e) {
      //
    }
  }
}
