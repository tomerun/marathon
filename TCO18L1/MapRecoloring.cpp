#include <algorithm>
#include <utility>
#include <vector>
#include <unordered_set>
#include <bitset>
#include <string>
#include <sstream>
#include <iostream>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
#define DEBUG
#else
// #define DEBUG
#define NDEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;
using pi=pair<int, int>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 8000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9800;
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
	ull ret;
	__asm__ volatile ("rdtsc" : "=A" (ret));
	return ret;
#else
	ull high,low;
	__asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
	return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

const int INF = 1 << 29;
const int NOT_USED = -1;
int RND_TL = 500;
int R, C;
vvi adjacent;
vvi cost;
vi ans, assignment;
vvi adj_use_count;
vi adj_colors;
int used_color_max;
vi color_count;
int best_score, best_used_color;
vvi nodes_adj_colors(51);
vi nodes_adj_colors_idx;
int max_adj_colors;

struct Solution {
	vi assignment;
	int colors, repaint;

	bool operator<(const Solution& o) const {
		return repaint < o.repaint;
	}
};


void nodes_adj_colors_increment(int v) {
	int c = adj_colors[v];
	int sw = nodes_adj_colors[c].back();
	// debug("remove:%d %d %d\n", v, c, nodes_adj_colors_idx[v]);
	adj_colors[v]++;
	swap(nodes_adj_colors[c][nodes_adj_colors_idx[v]], nodes_adj_colors[c].back());
	nodes_adj_colors_idx[sw] = nodes_adj_colors_idx[v];
	nodes_adj_colors_idx[v] = nodes_adj_colors[c + 1].size();
	nodes_adj_colors[c].pop_back();
	nodes_adj_colors[c + 1].push_back(v);
	if (c == max_adj_colors) max_adj_colors++;
}

void nodes_adj_colors_remove(int v) {
	int c = adj_colors[v];
	int sw = nodes_adj_colors[c].back();
	// debug("remove:%d %d %d\n", v, c, nodes_adj_colors_idx[v]);
	swap(nodes_adj_colors[c][nodes_adj_colors_idx[v]], nodes_adj_colors[c].back());
	nodes_adj_colors_idx[sw] = nodes_adj_colors_idx[v];
	nodes_adj_colors[c].pop_back();
	if (c == max_adj_colors && nodes_adj_colors[c].empty()) {
		do {
			--max_adj_colors;
		} while (nodes_adj_colors[max_adj_colors].empty() && max_adj_colors > 0);
	}
}

pi solve_random_single(int start) {
	fill(assignment.begin(), assignment.end(), NOT_USED);
	for (int i = 0; i < R; ++i) {
		if (i == start) continue;
		nodes_adj_colors_idx[i] = nodes_adj_colors[0].size();
		nodes_adj_colors[0].push_back(i);
	}
	assignment[start] = 0;
	for (int a : adjacent[start]) {
		adj_use_count[a][0]++;
		nodes_adj_colors_increment(a);
	}
	color_count[0] = 1;
	used_color_max = 1;
	max_adj_colors = 1;
	int cost_sum = cost[start][0];
	for (int i = 1; i < R; ++i) {
		int v = nodes_adj_colors[max_adj_colors][rnd.nextUInt(nodes_adj_colors[max_adj_colors].size())];
		nodes_adj_colors_remove(v);
		for (int color = 0; color <= used_color_max; ++color) {
			if (adj_use_count[v][color] != 0) continue;
			assignment[v] = color;
			for (int a : adjacent[v]) {
				if (assignment[a] != NOT_USED) continue;
				if (adj_use_count[a][color] == 0) {
					nodes_adj_colors_increment(a);
				}
				adj_use_count[a][color]++;
			}
			if (color == used_color_max) {
				if (used_color_max >= best_used_color) return make_pair(0, 0);
				++used_color_max;
			}
			cost_sum += cost[v][color];
			color_count[color]++;
			break;
		}
		// if (max_adj_colors >= best_used_color) return;
	}
	return make_pair(used_color_max, cost_sum);
}

vector<Solution> solve_random(int ret_sol_count) {
	assignment.assign(R, NOT_USED);
	adj_use_count.assign(R, vi(51));
	adj_colors.resize(R);
	color_count.resize(51);
	int max_ord = 0;
	for (int i = 0; i < R; ++i) {
		max_ord = max(max_ord, (int)adjacent[i].size());
	}
	nodes_adj_colors.resize(max_ord + 1);
	nodes_adj_colors_idx.resize(R);
	best_used_color = 50;
	vector<vector<Solution>> solutions(11);
	int best_max_color_count = INF;
	int min_colors_count = 0;
	int turn = 1;
	for (; ; ++turn) {
		if (turn == 300) {
			debug("best_max_color_count:%d\n", best_max_color_count);
			if (best_max_color_count < 7) {
				RND_TL = TL / 4;
			}
		}
		if (turn & 0xFFF) {
			if (get_elapsed_msec() > RND_TL) {
				debug("rnd turn:%d min_colors_count:%d\n", turn, min_colors_count);
				break;
			}
		}
		pi result = solve_random_single(rnd.nextUInt(R));
		int colors = result.first;
		int repaint = result.second;
		if (result.first != 0 && colors < solutions.size()) {
			vector<Solution>& cur_sols = solutions[colors]; 
			if (colors < best_used_color) {
				best_used_color = colors;
				best_max_color_count = color_count[colors - 1];
				min_colors_count = 1;
			} else {
				best_max_color_count = min(best_max_color_count, color_count[colors - 1]);
				min_colors_count++;
			}
			bool same_score = false;
			int worst_repaint = 0;
			int worst_idx = 0;
			for (int i = 0; i < cur_sols.size(); ++i) {
				const Solution& sol = cur_sols[i];
				if (sol.repaint == repaint) {
					same_score = true;
					break;
				}
				if (sol.repaint > worst_repaint) {
					worst_repaint = sol.repaint;
					worst_idx = i;
				}
			}
			if (!same_score) {
				if (cur_sols.size() < ret_sol_count) {
					debug("sol:%d %d %d %d at %d\n", colors, repaint, colors * repaint, color_count[colors - 1], turn);
					Solution sol;
					sol.assignment = assignment;
					sol.colors = colors;
					sol.repaint = repaint;
					cur_sols.push_back(sol);
				} else if (worst_repaint > repaint) {
					debug("sol:%d %d %d %d at %d\n", colors, repaint, colors * repaint, color_count[colors - 1], turn);
					cur_sols[worst_idx].assignment = assignment;
					cur_sols[worst_idx].colors = colors;
					cur_sols[worst_idx].repaint = repaint;
				}
			}
		}
		fill(color_count.begin(), color_count.begin() + used_color_max, 0);
		for (auto& v : adj_use_count) {
			fill(v.begin(), v.begin() + used_color_max + 1, 0);
		}
		fill(adj_colors.begin(), adj_colors.end(), 0);
		for (auto& v : nodes_adj_colors) {
			v.clear();
		}
	}
	vector<Solution> ret;
	sort(solutions[best_used_color].begin(), solutions[best_used_color].end());
	sort(solutions[best_used_color+1].begin(), solutions[best_used_color+1].end());
	if (min_colors_count < turn / 200) {
		if (solutions[best_used_color+1].empty()) {
			ret = solutions[best_used_color];
		} else {
			ret = solutions[best_used_color+1];
			if (ret.size() == ret_sol_count) ret.pop_back();
			ret.push_back(solutions[best_used_color][0]);
		}
	} else if (min_colors_count < turn / 20) {
		ret = solutions[best_used_color];
		if (!solutions[best_used_color+1].empty()) {
			if (ret.size() == ret_sol_count) ret.pop_back();
			ret.push_back(solutions[best_used_color+1][0]);
		}
	} else {
		ret = solutions[best_used_color];
	}
	return ret;
}

double initial_sa_cooler;
double sa_cooler;
int sa_update_count;
int cur_repaint;
array<int, 4001> que;
int que_size;
array<int, 4001> buf_cand;
array<int, 4001> buf_orig;

bool accept(int prev_score, int cur_score) {
	if (cur_score <= prev_score) return true;
	return rnd.nextDouble() < exp((prev_score - cur_score) * sa_cooler);
}

int mutate(vi& cur, int start) {
	copy(cur.begin(), cur.end(), buf_orig.begin());
	int repaint = cur_repaint;
	int prev_score = used_color_max * cur_repaint;
	vector<bool> dirty(R);
	vector<bool> changed(R);
	dirty[start] = changed[start] = true;
	que[0] = start;
	que_size = 1;
	int giveup = max(20, R / 5);
	for (int turn = 0; que_size > 0; ++turn) {
		if (turn == giveup) {
			for (int i = 0; i < R; ++i) {
				if (!changed[i]) continue;
				for (int n : adjacent[i]) {
					adj_use_count[n][cur[i]]--;
					adj_use_count[n][buf_orig[i]]++;
				}
				cur[i] = buf_orig[i];
			}
			return INF;
		}
		int v = que[--que_size];
		dirty[v] = false;
		int min_used = R;
		int buf_size = 0;
		for (int i = 0; i < used_color_max; ++i) {
			if (i == cur[v]) continue;
			if (adj_use_count[v][i] < min_used) {
				buf_cand[0] = i;
				buf_size = 1;
				min_used = adj_use_count[v][i];
			} else if (adj_use_count[v][i] == min_used) {
				buf_cand[buf_size++] = i;
			}
		}
		int new_color = buf_cand[rnd.nextUInt(buf_size)];
		repaint += cost[v][new_color] - cost[v][cur[v]];
		for (int nv : adjacent[v]) {
			adj_use_count[nv][cur[v]]--;
			adj_use_count[nv][new_color]++;
			if (cur[nv] == new_color && !dirty[nv]) {
				dirty[nv] = true;
				que[que_size++] = nv;
				changed[nv] = true;
			}
		}
		cur[v] = new_color;
	}
	int cur_score = used_color_max * repaint;
	if (accept(prev_score, cur_score)) {
		sa_update_count++;
		if (sa_update_count == 20000) {
			sa_cooler *= 1.01;
			sa_update_count = 0;
			// debug("sa_cooler:%.4f score:%d\n", sa_cooler, cur_score);
		}
		cur_repaint = repaint;
		return used_color_max * repaint;
	} else {
		for (int i = 0; i < R; ++i) {
			if (!changed[i]) continue;
			for (int n : adjacent[i]) {
				adj_use_count[n][cur[i]]--;
				adj_use_count[n][buf_orig[i]]++;
			}
			cur[i] = buf_orig[i];
		}
		return INF;
	}
}

int swap_color(vi& cur) {
	int c1 = rnd.nextUInt(used_color_max);
	int c2 = rnd.nextUInt(used_color_max - 1);
	if (c2 >= c1) c2++;
	int diff = 0;
	for (int i = 0; i < R; ++i) {
		if (cur[i] == c1) {
			diff += cost[i][c2] - cost[i][c1];
		} else if (cur[i] == c2) {
			diff += cost[i][c1] - cost[i][c2];
		}
	}
	int prev_score = used_color_max * cur_repaint;
	int cur_score = used_color_max * (cur_repaint + diff);
	if (accept(prev_score, cur_score)) {
		for (int i = 0; i < R; ++i) {
			if (cur[i] == c1) {
				for (int n : adjacent[i]) {
					adj_use_count[n][c1]--;
					adj_use_count[n][c2]++;
				}
				cur[i] = c2;
			} else if (cur[i] == c2) {
				for (int n : adjacent[i]) {
					adj_use_count[n][c2]--;
					adj_use_count[n][c1]++;
				}
				cur[i] = c1;
			}
		}
		cur_repaint += diff;
		return used_color_max * cur_repaint;
	} else {
		return INF;
	}
}

Solution solve_sa_single(Solution& sol, ll time_limit) {
	vi cur = sol.assignment;
	best_score = INF;
	used_color_max = sol.colors;
	for (auto& v : adj_use_count) {
		fill(v.begin(), v.begin() + 10, 0);
	}
	vi best_ans = cur;
	cur_repaint = sol.repaint;
	for (int i = 0; i < R; ++i) {
		for (int a : adjacent[i]) {
			adj_use_count[a][cur[i]]++;
		}
	}
	for (int turn = 1; ; ++turn) {
		if ((turn & 0x3FF) == 0) {
			if (get_elapsed_msec() > time_limit) {
				debug("sa turn:%d score:%d %d\n", turn, best_score, cur_repaint * sol.colors);
				break;
			}
		}
		int score;
		if (rnd.nextUInt() & 0x1) {
			int start = turn % R;
			score = mutate(cur, start);
		} else {
			score = swap_color(cur);
		}
		ADD_COUNTER(score == INF ? 1 : 0);
		if (score < best_score) {
			// debug("score: %d %d at turn %d\n", used_color_max, cur_repaint, turn);
			best_score = score;
			best_ans = cur;
		}
	}
	Solution ret;
	ret.assignment = best_ans;
	ret.colors = sol.colors;
	ret.repaint = best_score / sol.colors;
	for (int i = 0; i < R; ++i) {
		fill(adj_use_count[i].begin(), adj_use_count[i].begin() + ret.colors, 0);
		for (int a : adjacent[i]) {
			adj_use_count[i][best_ans[a]]++;
		}
	}
	for (int i = 0; i < R; ++i) {
		int min_cost = INF;
		int new_color = 0;
		for (int j = 0; j < sol.colors; ++j) {
			if (adj_use_count[i][j] == 0 && cost[i][j] < min_cost) {
				min_cost = cost[i][j];
				new_color = j;
			}
		}
		if (new_color != best_ans[i] && min_cost < cost[i][best_ans[i]]) {
			debug("change:%d -> %d\n", cost[i][best_ans[i]], cost[i][new_color]);
			for (int n : adjacent[i]) {
				adj_use_count[n][ret.assignment[i]]--;
				adj_use_count[n][new_color]++;
			}
			ret.assignment[i] = new_color;
			ret.repaint += cost[i][new_color] - cost[i][best_ans[i]];
		}
	}
	return ret;
}

vi solve_sa(vector<Solution>& solutions) {
	initial_sa_cooler = R > 1000 ? 0.04 : 0.03;
	Solution best_sol;
	best_sol.colors = 99;
	best_sol.repaint = 99999;
	ll start_time = get_elapsed_msec();
	for (int i = 0; i < solutions.size(); ++i) {
		sa_cooler = initial_sa_cooler;
		ll time_limit = (TL - start_time) * (i + 1) / solutions.size() + start_time;
		Solution cur_sol = solve_sa_single(solutions[i], time_limit);
		if (cur_sol.colors * cur_sol.repaint < best_sol.colors * best_sol.repaint) {
			best_sol.assignment = cur_sol.assignment;
			best_sol.colors = cur_sol.colors;
			best_sol.repaint = cur_sol.repaint;
		}
	}
	return best_sol.assignment;
}

struct MapRecoloring {
	vi recolor(int h, const vi& regions, const vi& oldColors);
};

vi MapRecoloring::recolor(int h, const vi& regions, const vi& oldColors) {
	start_time = get_time();
	int H = h;
	int W = regions.size() / H;
	R = *max_element(regions.begin(), regions.end()) + 1;
	C = *max_element(oldColors.begin(), oldColors.end()) + 1;
	adjacent.resize(R);
	cost.assign(R, vi(20));
	vi region_size(R);
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			int r1 = regions[i * W + j];
			region_size[r1]++;
			cost[r1][oldColors[i * W + j]]++;
			if (i > 0) {
				int r2 = regions[(i - 1) * W + j];
				if (r1 != r2 && find(adjacent[r1].begin(), adjacent[r1].end(), r2) == adjacent[r1].end()) {
					adjacent[r1].push_back(r2);
					adjacent[r2].push_back(r1);
				}
			}
			if (j > 0) {
				int r2 = regions[i * W + j - 1];
				if (r1 != r2 && find(adjacent[r1].begin(), adjacent[r1].end(), r2) == adjacent[r1].end()) {
					adjacent[r1].push_back(r2);
					adjacent[r2].push_back(r1);
				}
			}
		}
	}
	for (int i = 0; i < R; ++i) {
		for (int j = 0; j < cost[i].size(); ++j) {
			cost[i][j] = region_size[i] - cost[i][j];
		}
	}
	int solution_count = max(1, (int)(120 / pow(R, 0.5)));
	auto solutions = solve_random(solution_count);
	debug("solution count:%d %d\n", solution_count, (int)solutions.size());
	vi ret = solve_sa(solutions);
	// for (int v : ret) {
	// 	debug("%d ", v);
	// }
	// debugln();
	PRINT_COUNTER();
	PRINT_TIMER();
	return ret;
}

#if defined(LOCAL)
int main() {
	MapRecoloring obj;
	int H, S, R;
	cin >> H >> S;
	vi regions(S);
	for (int i = 0; i < S; ++i) {
		cin >> regions[i];
	}
	cin >> R;
	vi oldColors(R);
	for (int i = 0; i < R; ++i) {
		cin >> oldColors[i];
	}

	vector<int> ret = obj.recolor(H, regions, oldColors);
	cout << ret.size() << endl;
	for (int i = 0; i < ret.size(); ++i) {
		cout << ret[i] << endl;
	}
}
#endif