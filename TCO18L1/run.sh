#!/bin/sh -x

for i in 0.07 0.08 0.09 0.11 0.12 0.14
do
	date
	g++48 -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL -DCOOLER="$i" -s -pipe -mmmx -msse -msse2 -msse3 -o tester MapRecoloring.cpp
	java Tester -b 1000 -e 1999 > log13_"$i".txt
done
