import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Tester {
  private static final int minS = 20, maxS = 200;
  private static final int minC = 2, maxC = 5;
  private static final int[] dr = {1, 0, -1, 0};
  private static final int[] dc = {0, 1, 0, -1};

  private static int myH, myW, myR, myC;
  private int H, W;               // size of the map
  private int R, C;               // number of regions
  private int[][] regions;        // which region this pixel of the map belongs to
  private int[][] oldColors;      // old (starting) colors of the regions of the map (one color per pixel)
  private int[] newColors;        // colors of the regions assigned by the solution (one color per region)

  private boolean isInside(int r, int c) {
    return (r >= 0 && r < H && c >= 0 && c < W);
  }

  private int getColor(boolean isNew, int r, int c) {
    return isNew ? newColors[regions[r][c]] : oldColors[r][c];
  }

  private void generate(long seed) throws Exception {
    SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
    rnd.setSeed(seed);
    H = rnd.nextInt(maxS - minS + 1) + minS;
    W = rnd.nextInt(maxS - minS + 1) + minS;
    C = rnd.nextInt(maxC - minC + 1) + minC;
    if (seed == 1) {
      W = H = minS;
      C = 2;
    } else if (seed == 2) {
      W = H = maxS;
      C = maxC;
    }
    if (myH != 0) H = myH;
    if (myW != 0) H = myW;
    if (myC != 0) H = myC;
    int szr = rnd.nextInt(41) + 10;
    R = W * H / szr;
    if (myR != 0) R = myR;
    regions = new int[H][W];
    for (int i = 0; i < H; ++i)
      Arrays.fill(regions[i], -1);

    for (int i = 0; i < R; ++i) {
      int parts = rnd.nextInt(4) + 1;
      for (int j = 0; j < parts; ++j) {
        int r, c;
        do {
          r = rnd.nextInt(H - 2) + 1;
          c = rnd.nextInt(W - 2) + 1;
        } while (regions[r][c] != -1);
        regions[r][c] = i;
      }
    }

    boolean[] used = new boolean[H * W];
    ArrayList<Integer> candidates = new ArrayList<>();
    for (int r = 0; r < H; ++r)
      for (int c = 0; c < W; ++c)
        if (regions[r][c] != -1) {
          for (int k = 0; k < 4; ++k) {
            int nr = r + dr[k], nc = c + dc[k];
            if (isInside(nr, nc) && regions[nr][nc] == -1 && !used[nr * W + nc]) {
              candidates.add(nr * W + nc);
              used[nr * W + nc] = true;
            }
          }
        }
    while (!candidates.isEmpty()) {
      int ind = rnd.nextInt(candidates.size());
      int r = candidates.get(ind) / W, c = candidates.get(ind) % W;
      ArrayList<Integer> adjRegions = new ArrayList<>();
      for (int k = 0; k < 4; ++k) {
        int nr = r + dr[k], nc = c + dc[k];
        if (isInside(nr, nc) && regions[nr][nc] != -1)
          adjRegions.add(k);
      }
      int adjK = adjRegions.get(rnd.nextInt(adjRegions.size()));
      regions[r][c] = regions[r + dr[adjK]][c + dc[adjK]];
      candidates.remove(ind);
      for (int k = 0; k < 4; ++k) {
        int nr = r + dr[k], nc = c + dc[k];
        if (isInside(nr, nc) && regions[nr][nc] == -1 && !used[nr * W + nc]) {
          candidates.add(nr * W + nc);
          used[nr * W + nc] = true;
        }
      }
    }

    while (true) {
      int[] regPrimaryColor = new int[R];
      int[][] regSecondaryColors = new int[R][];
      int[] solid = new int[R];
      for (int i = 0; i < R; ++i) {
        regPrimaryColor[i] = rnd.nextInt(C);
        int nSec = rnd.nextInt(C);
        regSecondaryColors[i] = new int[nSec];
        for (int j = 0; j < nSec; ++j) {
          do {
            regSecondaryColors[i][j] = rnd.nextInt(C);
          }
          while (regSecondaryColors[i][j] == regPrimaryColor[i]);
        }
        solid[i] = (nSec == 0 ? 100 : (rnd.nextInt(50) + 50));
      }

      oldColors = new int[H][W];
      for (int i = 0; i < H; ++i)
        for (int j = 0; j < W; ++j) {
          int reg = regions[i][j];
          if (rnd.nextInt(100) < solid[reg])
            oldColors[i][j] = regPrimaryColor[reg];
          else
            oldColors[i][j] = regSecondaryColors[reg][rnd.nextInt(regSecondaryColors[reg].length)];
        }

      boolean ok = false;
      for (int i = 0; i < H && !ok; ++i)
        for (int j = 0; j < W && !ok; ++j) {
          if (i > 0) {
            int r1 = regions[i][j];
            int r2 = regions[i - 1][j];
            if (r1 != r2 && getColor(false, i, j) == getColor(false, i - 1, j)) {
              ok = true;
            }
          }
          if (j > 0) {
            int r1 = regions[i][j];
            int r2 = regions[i][j - 1];
            if (r1 != r2 && getColor(false, i, j) == getColor(false, i, j - 1)) {
              ok = true;
            }
          }
        }
      if (ok) break;
    }

    if (vis) {
      palette = new ArrayList<>();
      int[] firstColors = {0x6633ff, 0xcc33ff, 0xffcc33, 0x33ccff, 0x66ff33};
      for (int i = 0; i < R && i < firstColors.length; ++i)
        palette.add(new Color(firstColors[i]));
      for (int i = firstColors.length; i < R; ++i)
        palette.add(new Color(rnd.nextInt(0x1000000)));
    }
  }

  private Result runTest(long seed) throws Exception {
    generate(seed);
    if (vis) {
      SZX = W * SZ + 1;
      SZY = H * SZ + 1;
      draw(false);
    }
    Result res = new Result();
    res.seed = seed;
    res.H = H;
    res.W = W;
    res.R = R;
    res.C = C;
    int[] regionsArg = new int[H * W];
    int[] oldColorsArg = new int[H * W];
    for (int i = 0; i < H; ++i)
      for (int j = 0; j < W; ++j) {
        regionsArg[i * W + j] = regions[i][j];
        oldColorsArg[i * W + j] = oldColors[i][j];
      }
    long startTime = System.currentTimeMillis();
    newColors = recolor(H, regionsArg, oldColorsArg);
    res.elapsed = System.currentTimeMillis() - startTime;
    if (newColors == null) {
      throw new RuntimeException("Your return contained invalid number of elements.");
    }
    if (newColors.length != R) {
      throw new RuntimeException("Your return contained " + newColors.length + " elements, and it should have contained " + R + ".");
    }
    for (int i = 0; i < R; ++i) {
      if (newColors[i] < 0 || newColors[i] >= R) {
        throw new RuntimeException("Each color in your return must be between 0 and " + (R - 1) + ", inclusive.");
      }
    }
    if (vis) {
      draw(true);
    }
    for (int i = 0; i < H; ++i)
      for (int j = 0; j < W; ++j) {
        if (i > 0) {
          int r1 = regions[i][j];
          int r2 = regions[i - 1][j];
          if (r1 != r2 && newColors[r1] == newColors[r2]) {
            throw new RuntimeException("Regions " + r1 + " and " + r2 + " adjacent in pixels (" + (i - 1) + "," + j +
                ") and (" + i + "," + j + ") are of the same color " + newColors[r1] + ".");
          }
        }
        if (j > 0) {
          int r1 = regions[i][j];
          int r2 = regions[i][j - 1];
          if (r1 != r2 && newColors[r1] == newColors[r2]) {
            throw new RuntimeException("Regions " + r1 + " and " + r2 + " adjacent in pixels (" + i + "," + (j - 1) +
                ") and (" + i + "," + j + ") are of the same color " + newColors[r1] + ".");
          }
        }
      }

    int recolor = 0;
    for (int i = 0; i < H; ++i)
      for (int j = 0; j < W; ++j)
        if (getColor(true, i, j) != getColor(false, i, j))
          recolor++;
    HashSet<Integer> distinct = new HashSet<>();
    for (int i = 0; i < R; ++i)
      distinct.add(newColors[i]);
    res.numRepaint = recolor;
    res.numColor = distinct.size();
    return res;
  }

  private static String fileName;
  private static boolean vis;
  private static int SZ = 20, SZX, SZY;

  private int[] recolor(int H, int[] regions, int[] oldColors) throws IOException {
    Process proc = Runtime.getRuntime().exec("./tester");
    new ErrorReader(proc.getErrorStream()).start();
    OutputStream os = proc.getOutputStream();
    InputStream is = proc.getInputStream();
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    try {
      StringBuilder sb = new StringBuilder();
      sb.append(H).append("\n");
      sb.append(regions.length).append("\n");
      for (int i = 0; i < regions.length; ++i) {
        sb.append(regions[i]).append("\n");
      }
      sb.append(oldColors.length).append("\n");
      for (int i = 0; i < oldColors.length; ++i) {
        sb.append(oldColors[i]).append("\n");
      }
      os.write(sb.toString().getBytes());
      os.flush();

      int N = Integer.parseInt(br.readLine());
      int[] ret = new int[N];
      for (int i = 0; i < N; i++)
        ret[i] = Integer.parseInt(br.readLine());
      return ret;
    } finally {
      os.close();
      br.close();
      proc.destroy();
    }
  }

  private ArrayList<Color> palette;

  private void draw(boolean newC) throws Exception {
    BufferedImage bi = new BufferedImage(SZX, SZY, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) bi.getGraphics();
    g2.setColor(Color.WHITE);
    g2.fillRect(0, 0, SZX, SZY);
    for (int i = 0; i < H; ++i)
      for (int j = 0; j < W; ++j) {
        g2.setColor(palette.get(getColor(newC, i, j)));
        g2.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
      }

    g2.setColor(Color.BLACK);
    g2.setFont(new Font("Arial", Font.PLAIN, 9));
    FontMetrics fm = g2.getFontMetrics();
    char[] ch;
    for (int i = 0; i < H; ++i)
      for (int j = 0; j < W; ++j) {
        ch = ("" + regions[i][j]).toCharArray();
        int h = i * SZ + SZ / 2 + fm.getHeight() / 2 - 2;
        g2.drawChars(ch, 0, ch.length, j * SZ + 2, h);
      }

    for (int i = 0; i <= H; i++)
      g2.drawLine(0, i * SZ, W * SZ, i * SZ);
    for (int i = 0; i <= W; i++)
      g2.drawLine(i * SZ, 0, i * SZ, H * SZ);

    g2.setStroke(new BasicStroke(3.0f));
    for (int i = 0; i < H; ++i)
      for (int j = 0; j < W; ++j) {
        if (i > 0 && regions[i][j] != regions[i - 1][j]) {
          int c0 = getColor(newC, i, j);
          int c1 = getColor(newC, i - 1, j);
          if (c0 == c1)
            g2.drawLine(j * SZ, i * SZ, (j + 1) * SZ, i * SZ);
        }
        if (j > 0 && regions[i][j] != regions[i][j - 1]) {
          int c0 = getColor(newC, i, j);
          int c1 = getColor(newC, i, j - 1);
          if (c0 == c1)
            g2.drawLine(j * SZ, i * SZ, j * SZ, (i + 1) * SZ);
        }
      }
    ImageIO.write(bi, "png", new File(fileName + (newC ? "-res" : "-in") + ".png"));
  }

  private static final int THREAD_COUNT = 2;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-H")) myH = Integer.parseInt(args[++i]);
      if (args[i].equals("-W")) myW = Integer.parseInt(args[++i]);
      if (args[i].equals("-R")) myR = Integer.parseInt(args[++i]);
      if (args[i].equals("-C")) myC = Integer.parseInt(args[++i]);
      if (args[i].equals("-vis")) vis = true;
      if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
    }
    if (vis) fileName = String.valueOf(seed);
    if (begin != -1 && end != -1) {
      vis = false;
      BlockingQueue<Long> q = new ArrayBlockingQueue<>((int) (end - begin + 1));
      for (long i = begin; i <= end; ++i) {
        q.add(i);
      }
      Result[] results = new Result[(int) (end - begin + 1)];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i] = new TestThread(q, results, begin);
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
      }
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    long seedMin;
    Result[] results;
    BlockingQueue<Long> q;

    TestThread(BlockingQueue<Long> q, Result[] results, long seedMin) {
      this.q = q;
      this.results = results;
      this.seedMin = seedMin;
    }

    public void run() {
      while (true) {
        Long seed = q.poll();
        if (seed == null) break;
        try {
          Tester f = new Tester();
          Result res = f.runTest(seed);
          results[(int) (seed - seedMin)] = res;
        } catch (Exception e) {
          e.printStackTrace();
          Result res = new Result();
          res.seed = seed;
          res.numRepaint = 999999;
          res.numColor = 9;
          results[(int) (seed - seedMin)] = res;
        }
      }
    }
  }
}

class Result {
  long seed;
  int R, C, H, W;
  int numRepaint, numColor;
  long elapsed;

  public String toString() {
    String ret = String.format("seed:%4d\n", seed);
    ret += String.format("H:%3d W:%3d R:%4d C:%1d\n", H, W, R, C);
    ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
    ret += String.format("score:%3d %6d", numColor, numRepaint);
    return ret;
  }
}

class ErrorReader extends Thread {
  private InputStream error;

  ErrorReader(InputStream is) {
    error = is;
  }

  public void run() {
    try {
      byte[] ch = new byte[50000];
      int read;
      while ((read = error.read(ch)) > 0) {
        String s = new String(ch, 0, read);
        System.err.print(s);
        System.err.flush();
      }
    } catch (Exception e) {
      // empty
    }
  }
}
