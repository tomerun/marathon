import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
	SAW model
    0 bottom arm
         ...
    1 top arm
         ...
    2 blanket 1
         0 polygon
    3 blanket 2
         0 polygon
    4 longerons
         0 cylinder
         1 cylinder
         2 cylinder
         3 cylinder
    5 bottom cap 
         0 cylinder
    6 top cap
         0 cylinder
 */
public class ISSModel {

	static final int NUM_STATES = 92;
	private static final int NUM_STRINGS = 41;
	private static final int MAX_TLEVEL = 30;
	static final int MAX_BSEARCH_LEVEL = 30;
	private Structure m;
	private ArrayList<Polygon> polygons = new ArrayList<Polygon>();
	private ArrayList<Cylinder> cylinders = new ArrayList<Cylinder>();
	double beta, yaw;
	private Ray sray = new Ray();
	private V srayB, srayC;
	private Triangle[] triangle_stack = new Triangle[MAX_TLEVEL];
	private PrimList[][] prim_stack = new PrimList[MAX_TLEVEL][3];
	boolean renient = true;

	ISSModel(double beta, double yaw) {
		this.m = new Reader().readModel();
		this.m.collect(polygons, cylinders);
		this.beta = beta;
		this.yaw = yaw;
		for (int i = 0; i < MAX_TLEVEL; ++i) {
			triangle_stack[i] = new Triangle();
			prim_stack[i] = new PrimList[3];
			prim_stack[i][0] = new PrimList();
			prim_stack[i][1] = new PrimList();
			prim_stack[i][2] = new PrimList();
		}
	}

	void calc(int ti, double[] angle, Result res) {
		setAlpha(ti * 360.0 / NUM_STATES);
		setAngle(angle);
		calculateOnePosition(res);
	}

	void calc(double alpha, double[] angle, Result res) {
		setAlpha(alpha);
		setAngle(angle);
		calculateOnePosition(res);
	}

	void setAlpha(double alpha) {
		M sunTransform = makeSunProjection(alpha);
		sunTransform.transform(new V(0, -1, 0), this.sray.d);
		srayB = new V(1, 0, 0);
		srayC = new V(0, 0, 1);
		sunTransform.transform(srayB, srayB);
		sunTransform.transform(srayC, srayC);
	}

	void setAngle(double[] angle) {
		rotate(angle);
		m.transform();
		m.precalc(sray.d, srayB, srayC);
	}

	M makeSunProjection(double alpha) {
		M proj = M.rotate_y_axis(-alpha);
		proj.rotate_x_axis_compose(90 - beta, proj);
		proj.rotate_y_axis_compose(alpha, proj);
		return proj;
	}

	static void rotateStructure(Structure struct, V axis, double angle, boolean x) {
		M trans = struct.transform;
		M.negative_translate(axis, trans);
		if (x) {
			trans.rotate_x_axis_compose(angle, trans);
		} else {
			trans.rotate_y_axis_compose(angle, trans);
		}
		trans.translate_compose(axis, trans);
	}

	static final V[] rotation_axis = new V[10];
	static final int[] rotDir = new int[] { 1, -1, 1, -1, -1, 1, -1, 1, 1, -1 };
	static final int[] rotOffset = new int[] { 0, 0, 270, 270, 90, 90, 90, 90, 270, 270 };
	static final V port_radiator_axis = new V(-250, -14600, -5.5);
	static final V starboard_radiator_axis = new V(-250, 14694, -23);

	static {
		rotation_axis[0] = new V(5, 0, 11); // ssarj
		rotation_axis[1] = new V(5, 0, 11); // psarj
		rotation_axis[2] = new V(0, 33350, -750); // 1A 
		rotation_axis[3] = new V(0, -33350, -750); // 2A 
		rotation_axis[4] = new V(0, 33350, 750); // 3A 
		rotation_axis[5] = new V(0, -33350, 750); // 4A 
		rotation_axis[6] = new V(0, 48422, 755); // 1B 
		rotation_axis[7] = new V(0, -48420, 683); // 2B
		rotation_axis[8] = new V(0, 48413, -750); // 3B
		rotation_axis[9] = new V(0, -48419, -817); // 4B 
	}

	void rotate(double[] angle) {
		m.transform = M.rotate_z_axis(-yaw);
		for (int i = 0; i < 2; ++i) {
			rotateStructure(m.child[i], rotation_axis[i], (angle[i] - rotOffset[i]) * rotDir[i], false);
		}
		for (int i = 2; i < 10; ++i) {
			rotateStructure(m.child[i % 2].child[(i - 2) / 2], rotation_axis[i], (angle[i] - rotOffset[i]) * rotDir[i], true);
		}
		rotateStructure(m.child[3], starboard_radiator_axis, (beta > 0) ? 25 : 60, true);
		rotateStructure(m.child[2], port_radiator_axis, 45, true);
	}

	void setRayOrigin(P p) {
		sray.o.copyFrom(p);
		sray.a = sray.d.dotp(p);
		sray.b = srayB.dotp(p);
		sray.c = srayC.dotp(p);
	}

	void setRayOrigin(double x, double y, double z) {
		sray.o.x = x;
		sray.o.y = y;
		sray.o.z = z;
		sray.a = sray.d.dot(x, y, z);
		sray.b = srayB.dot(x, y, z);
		sray.c = srayC.dot(x, y, z);
	}

	static int[] assumeZeroPlus = { -1, 0, -1, 1, 0, -1, 1, -1 };
	static int[] assumeZeroMinus = { 0, -1, 1, -1, -1, 0, -1, 1 };

	void calculateOnePosition(Result res) {
		int[] assumeZero = this.beta < 0 ? assumeZeroMinus : assumeZeroPlus;
		for (int i = 0; i < 8; ++i) {
			Structure saw = m.child[i % 2].child[i / 2];
			Polygon blanket = (Polygon) saw.solid[2].obj[0];
			double cosine = blanket.normal.dot(this.sray.d);
			if (cosine > 0) {
				double[] shadows1;
				if (!this.renient && assumeZero[i] == 0) {
					shadows1 = new double[NUM_STRINGS];
					Arrays.fill(shadows1, 1);
				} else {
					shadows1 = calculateOneBlanketT(blanket);
				}
				double[] shadows2;
				if (!this.renient && assumeZero[i] == 1) {
					shadows2 = new double[NUM_STRINGS];
					Arrays.fill(shadows2, 1);
				} else {
					shadows2 = calculateOneBlanketT((Polygon) saw.solid[3].obj[0]);
				}
				for (int j = 0; j < NUM_STRINGS; ++j) {
					res.power[i] += Math.max(0.0, 1.0 - 5.0 * shadows1[j]);
					res.power[i] += Math.max(0.0, 1.0 - 5.0 * shadows2[j]);
				}
				res.power[i] *= cosine * 137.13 * (400 * 0.08 * 0.08);
			}
			Tester.timer.start(3);
			int dangerCount = 0;
			for (int j = 0; j < 4; ++j) {
				Cylinder longeron = (Cylinder) saw.solid[4].obj[j];
				if (isLongeronShadowed(longeron)) ++dangerCount;
			}
			res.danger[i] = dangerCount % 2 != 0;
			Tester.timer.end(3);
		}
	}

	void calculatePower(Result res) {
		for (int i = 0; i < 8; ++i) {
			res.power[i] = 0;
			Structure saw = m.child[i % 2].child[i / 2];
			Polygon blanket = (Polygon) saw.solid[2].obj[0];
			double cosine = blanket.normal.dot(this.sray.d);
			if (cosine > 0) {
				double[] shadows1 = calculateOneBlanketT(blanket);
				double[] shadows2 = calculateOneBlanketT((Polygon) saw.solid[3].obj[0]);
				for (int j = 0; j < NUM_STRINGS; ++j) {
					res.power[i] += Math.max(0.0, 1.0 - 5.0 * shadows1[j]);
					res.power[i] += Math.max(0.0, 1.0 - 5.0 * shadows2[j]);
				}
				res.power[i] *= cosine * 137.13 * (400 * 0.08 * 0.08);
			}
		}
	}

	boolean[] isLongeronOK() {
		boolean[] ret = new boolean[8];
		for (int i = 0; i < 8; ++i) {
			Structure saw = m.child[i % 2].child[i / 2];
			int shadowCount = 0;
			for (int j = 0; j < 4; ++j) {
				Cylinder longeron = (Cylinder) saw.solid[4].obj[j];
				if (isLongeronShadowed(longeron)) {
					++shadowCount;
				}
			}
			ret[i] = shadowCount % 2 == 0;
		}
		return ret;
	}

	static void interp(P p1, P p2, double alpha, P result) { // faster in place
		result.x = p1.x * (1.0 - alpha) + p2.x * alpha;
		result.y = p1.y * (1.0 - alpha) + p2.y * alpha;
		result.z = p1.z * (1.0 - alpha) + p2.z * alpha;
	}

	double binary_search(P lowin, P highin, P lowout, P highout, Prim prim) {
		if (prim instanceof Polygon) {
			double[] tv = ((Polygon) prim).findCrossPoint(srayB.dotp(lowin), srayC.dotp(lowin), srayB.dotp(highin),
					srayC.dotp(highin));
			lowout.x = lowin.x + (highin.x - lowin.x) * tv[0];
			lowout.y = lowin.y + (highin.y - lowin.y) * tv[0];
			lowout.z = lowin.z + (highin.z - lowin.z) * tv[0];
			highout.x = lowin.x + (highin.x - lowin.x) * tv[1];
			highout.y = lowin.y + (highin.y - lowin.y) * tv[1];
			highout.z = lowin.z + (highin.z - lowin.z) * tv[1];
			return tv[0];
		}
		highout.x = highin.x;
		highout.y = highin.y;
		highout.z = highin.z;
		lowout.x = lowin.x;
		lowout.y = lowin.y;
		lowout.z = lowin.z;
		double v = 0.0;
		double s = 1.0;
		for (int i = 0; i < MAX_BSEARCH_LEVEL; ++i) {
			double mx = (highout.x + lowout.x) * 0.5;
			double my = (highout.y + lowout.y) * 0.5;
			double mz = (highout.z + lowout.z) * 0.5;
			sray.o.x = mx;
			sray.o.y = my;
			sray.o.z = mz; // not need to set b,c because prim is Cylinder
			s *= 0.5;
			if (prim.anyIntersection(sray)) {
				lowout.x = mx;
				lowout.y = my;
				lowout.z = mz;
				v += s;
			} else {
				highout.x = mx;
				highout.y = my;
				highout.z = mz;
			}
		}
		return v;
	}

	// among 3 Prims, if p1[i1] has the least order, return count of the same Prim that p1[i1]
	int primCount(int level, int p1, int i1, int p2, int i2, int p3, int i3) {
		PrimList[] p = prim_stack[level];
		boolean ok1 = i1 < p[p1].s && p[p1].valid[i1];
		boolean ok2 = i2 < p[p2].s && p[p2].valid[i2];
		boolean ok3 = i3 < p[p3].s && p[p3].valid[i3];
		int ret = 0;
		if (ok1) {
			if (!(ok2 && p[p1].prim[i1].order > p[p2].prim[i2].order)
					&& !(ok3 && p[p1].prim[i1].order > p[p3].prim[i3].order)) {
				ret |= 1;
			}
		}
		if (ok2) {
			if (!(ok1 && p[p2].prim[i2].order > p[p1].prim[i1].order)
					&& !(ok3 && p[p2].prim[i2].order > p[p3].prim[i3].order)) {
				ret |= 2;
			}
		}
		if (ok3) {
			if (!(ok2 && p[p3].prim[i3].order > p[p2].prim[i2].order)
					&& !(ok1 && p[p3].prim[i3].order > p[p1].prim[i1].order)) {
				ret |= 4;
			}
		}
		return ret;
		//		if (i1 >= p[p1].s || !p[p1].valid[i1]) return 0;
		//		int c = 1;
		//		if (i2 < p[p2].s && p[p2].valid[i2]) {
		//			if (p[p2].prim[i2].order < p[p1].prim[i1].order)
		//				return 0;
		//			else if (p[p2].prim[i2] == p[p1].prim[i1]) c = 2;
		//		}
		//		if (i3 < p[p3].s && p[p3].valid[i3]) {
		//			if (p[p3].prim[i3].order < p[p1].prim[i1].order)
		//				return 0;
		//			else if (p[p3].prim[i3] == p[p1].prim[i1]) c++;
		//		}
		//		return c;
	}

	double subdivide_triangle(int level, double area) {
		// at this point triangle_stack[level] and prim_stack[level] are
		// set to valid values for the current triangle to subdivide
		if (area < 1e-3) return 0;
		PrimList[] p = prim_stack[level];
		int zero = 0;
		if (p[0].s == 0) ++zero;
		if (p[1].s == 0) ++zero;
		if (p[2].s == 0) ++zero;
		if (zero == 3) return 0;

		if (p[0].s > 0 && p[1].s > 0 && p[2].s > 0) {
			HashSet<Prim> prims0 = new HashSet<Prim>();
			HashSet<Prim> prims1 = new HashSet<Prim>();
			for (int i = 0; i < p[0].s; ++i) {
				prims0.add(p[0].prim[i]);
			}
			for (int i = 0; i < p[1].s; ++i) {
				prims1.add(p[1].prim[i]);
			}
			for (int i = 0; i < p[2].s; ++i) {
				if (prims0.contains(p[2].prim[i]) && prims1.contains(p[2].prim[i])) {
					return area;
				}
			}
		}

		Triangle t = triangle_stack[level];
		int s0 = 0;
		int s1 = 0;
		int s2 = 0;
		double b = 0;
		if (zero <= 1) {
			while (p[1].s > s1 || p[2].s > s2 || p[0].s > s0) {
				int pc = primCount(level, 1, s1, 2, s2, 0, s0);
				if (pc == 3) {
					double a = subdivide_triangle2(level, t.v2, 1, t.v3, 2, t.v1, 0, p[1].prim[s1], area);
					if (level > 0 && a > 0) return a;
					b = Math.max(b, a);
					p[1].valid[s1] = false;
					p[2].valid[s2] = false;
				} else if (pc == 5) {
					double a = subdivide_triangle2(level, t.v1, 0, t.v2, 1, t.v3, 2, p[1].prim[s1], area);
					if (level > 0 && a > 0) return a;
					b = Math.max(b, a);
					if (a > 0) return a;
					p[1].valid[s1] = false;
					p[0].valid[s0] = false;
				} else if (pc == 6) {
					double a = subdivide_triangle2(level, t.v1, 0, t.v3, 2, t.v2, 1, p[0].prim[s0], area);
					if (level > 0 && a > 0) return a;
					b = Math.max(b, a);
					p[0].valid[s0] = false;
					p[2].valid[s2] = false;
				}
				if ((s1 < p[1].s && !p[1].valid[s1]) || (s2 < p[2].s && !p[2].valid[s2]) || (s0 < p[0].s && !p[0].valid[s0])) {
					if (s1 < p[1].s && !p[1].valid[s1]) ++s1;
					if (s2 < p[2].s && !p[2].valid[s2]) ++s2;
					if (s0 < p[0].s && !p[0].valid[s0]) ++s0;
				} else {
					if ((pc & 1) > 0)
						++s1;
					else if ((pc & 2) > 0)
						++s2;
					else
						++s0;
				}
			}
		}

		s0 = 0;
		s1 = 0;
		s2 = 0;
		while (s0 < p[0].s && !p[0].valid[s0])
			++s0;
		while (s1 < p[1].s && !p[1].valid[s1])
			++s1;
		while (s2 < p[2].s && !p[2].valid[s2])
			++s2;
		while (p[1].s > s1 || p[2].s > s2 || p[0].s > s0) {
			int pc = primCount(level, 1, s1, 2, s2, 0, s0);
			if ((pc & 1) > 0) {
				double a = subdivide_triangle1(level, t.v2, 1, t.v1, 0, t.v3, 2, p[1].prim[s1], area);
				if (level > 0 && a > 0) return a;
				b = Math.max(b, a);
				++s1;
				while (s1 < p[1].s && !p[1].valid[s1])
					++s1;
			} else if ((pc & 2) > 0) {
				double a = subdivide_triangle1(level, t.v3, 2, t.v2, 1, t.v1, 0, p[2].prim[s2], area);
				if (level > 0 && a > 0) return a;
				b = Math.max(b, a);
				++s2;
				while (s2 < p[2].s && !p[2].valid[s2])
					++s2;
			} else if ((pc & 4) > 0) {
				double a = subdivide_triangle1(level, t.v1, 0, t.v2, 1, t.v3, 2, p[0].prim[s0], area);
				if (level > 0 && a > 0) return a;
				b = Math.max(b, a);
				++s0;
				while (s0 < p[0].s && !p[0].valid[s0])
					++s0;
			}
		}
		return b;
	}

	private P mid = new P();

	double subdivide_triangle2(int level, P v1, int s1, P v2, int s2, P v3, int s3, Prim prim, double area) {
		// v1 and v2 are covered by prim, v3 is not
		// pointers to vertices are used instead of level so which vertices
		// are covered is expresssed. Similarly which prim does the covering
		// is directly passed in
		Triangle t1 = triangle_stack[level]; // only so we can use other
		// vertices as preallocated temporary variables
		double a23 = binary_search(v2, v3, t1.v23l, t1.v23h, prim);
		double a13 = binary_search(v1, v3, t1.v31l, t1.v31h, prim);
		// case 1
		// non-zero area trapeziod covered
		// recurse on one 2-vertex case triangle
		// case 2 
		// non-zero area triangle covered, one of a32 or a13 > 0
		// recurse on one 2-vertex case
		// case 3
		// non-zero area something covered with a32 and a13 == 0
		// and bisector > 0
		// recurse on two 2-vertex cases (this is the most problematic case)
		//
		// return interpolation results so vertices are IN the covering polyon 
		// (low variable in interpolation method) so recursion works.
		if (a13 > 1e-3 || a23 > 1e-3) { // case 1 and case 2
			double covered_area = (a13 + (1.0 - a13) * a23) * area;
			if (level < MAX_TLEVEL - 1) {
				Triangle t2 = triangle_stack[level + 1];
				PrimList[] p1 = prim_stack[level];
				PrimList[] p2 = prim_stack[level + 1];
				t2.v1.copyFrom(t1.v31h);
				setRayOrigin(t2.v1);
				p2[0].reset();
				m.allIntersections(sray, p2[0]);
				t2.v2.copyFrom(t1.v23h);
				setRayOrigin(t2.v2);
				p2[1].reset();
				m.allIntersections(sray, p2[1]);
				t2.v3.copyFrom(v3);
				p2[2].copy(p1[s3]);
				return covered_area + subdivide_triangle(level + 1, area - covered_area);
			} else {
				return covered_area;
			}
		}

		// possibly case 3
		mid.x = (v1.x + v2.x) * 0.5;
		mid.y = (v1.y + v2.y) * 0.5;
		mid.z = (v1.z + v2.z) * 0.5;
		double bisector = binary_search(mid, v3, t1.v12l, t1.v12h, prim);
		if (level == MAX_TLEVEL - 1) {
			return area * bisector;
		}
		Triangle t2 = triangle_stack[level + 1];
		PrimList[] p1 = prim_stack[level];
		PrimList[] p2 = prim_stack[level + 1];
		// zero length edges on v1-v3 and v2-v3
		// this is either zero points in the interior or one or more
		// points in the interior
		if (bisector < 1e-2) {
			t2.v1.copyFrom(t1.v31h);
			setRayOrigin(t2.v1);
			p2[0].reset();
			m.allIntersections(sray, p2[0]);
			t2.v2.copyFrom(t1.v23h);
			setRayOrigin(t2.v2);
			p2[1].reset();
			m.allIntersections(sray, p2[1]);
			t2.v3.copyFrom(v3);
			p2[2].copy(p1[s3]);
			return subdivide_triangle(level + 1, area);
		}
		double covered_area = bisector * area;
		// case 3
		// OK, now we have a point (t1.v12l) in the covering polygon and in
		// the interior of the triangle. Finding a vertex of the covering
		// polygon would allow the recursion to quickly give the "exact" answer
		// but for now we will just use this point and the convergence will be 
		// something like exponential wrt number of sub-triangles (which is 
		// much better than fixed ratio subdivision schemes).
		t2.v1.copyFrom(v1);
		p2[0].copy(p1[s1]);
		t2.v2.copyFrom(v3);
		p2[1].copy(p1[s3]);
		t2.v3.copyFrom(t1.v12l);
		setRayOrigin(t2.v3);
		p2[2].reset();
		m.allIntersections(sray, p2[2]);
		double c1 = subdivide_triangle(level + 1, (area - covered_area) * 0.5);
		t2.v1.copyFrom(v2);
		p2[0].copy(p1[s2]);
		double c2 = subdivide_triangle(level + 1, (area - covered_area) * 0.5);
		return covered_area + c1 + c2;
	}

	double subdivide_triangle1(int level, P v1, int p1, P v2, int p2, P v3, int p3, Prim prim, double area) {
		// There are two cases, 
		// case 1 when the covered part of both v1-v2 and v1-v3 
		// edges are are non-zero
		// this will recurse on a a trapezoid (a 2-vertex covered triangle
		// and a 1-vertex covered triangle)
		//
		// case 2 when only one of the edges is non-zero.
		// this will recurse on one 2-vertex covered triangle
		//
		// case 1 always has positive amount of coverage
		// case 2 only has a positive amount of coverage when the
		// bisector is non-zero
		//
		// It is hard to describe all these cases without diagrams
		Triangle t1 = triangle_stack[level];
		Triangle t2 = null;
		if (level < MAX_TLEVEL - 1) t2 = triangle_stack[level + 1];
		double a12 = binary_search(v1, v2, t1.v12l, t1.v12h, prim);
		double a31 = binary_search(v1, v3, t1.v31l, t1.v31h, prim);
		// empty case (no recursion)
		if (a12 < 1e-5 && a31 < 1e-5) {
			return 0;
		}
		// if only one is "zero" then the resulting two point
		// sub-triangle will be tested for bisection on the recursion
		// the above case is both zero...
		// full coverage case (no recursion)
		if (a12 > 0.99999 && a31 > 0.99999) {
			return area;
		}
		if (a12 > 1e-3 && a31 > 1e-3) {
			// case 1 (a triangle covered, recurse on trapezoid, two way)
			double covered_area = (a12 * a31) * area;
			if (level < MAX_TLEVEL - 1) {
				t2.v1.copyFrom(t1.v12l);
				setRayOrigin(t1.v12l);
				prim_stack[level + 1][0].reset();
				m.allIntersections(sray, prim_stack[level + 1][0]);
				t2.v2.copyFrom(t1.v31l);
				setRayOrigin(t1.v31l);
				prim_stack[level + 1][1].reset();
				m.allIntersections(sray, prim_stack[level + 1][1]);
				t2.v3.copyFrom(v2);
				prim_stack[level + 1][2].copy(prim_stack[level][p2]);
				double a1 = area * a31 - covered_area;
				double c1 = subdivide_triangle(level + 1, a1);
				t2.v1.copyFrom(v3);
				prim_stack[level + 1][0].copy(prim_stack[level][p3]);
				double a2 = area - covered_area - a1;
				double c2 = subdivide_triangle(level + 1, a2);
				return covered_area + c1 + c2;
			} else {
				return covered_area;
			}
		}
		// case 2 (v1-v2 edge with two recursions, let two point code handle
		// bisection recursion
		if (a12 > 1e-3) {
			mid.x = (v1.x + t1.v12l.x) * 0.5;
			mid.y = (v1.y + t1.v12l.y) * 0.5;
			mid.z = (v1.z + t1.v12l.z) * 0.5;
			double bisector = binary_search(mid, v3, t1.v23l, t1.v23h, prim);
			double covered_area = a12 * bisector;
			if (level == MAX_TLEVEL - 1 || (!renient && bisector == 0)) {
				return covered_area;
			} else {
				if (bisector < 1e-2) {
					t2.v1.copyFrom(t1.v12h);
				} else {
					t2.v1.copyFrom(t1.v12l);
				}
				setRayOrigin(t2.v1);
				prim_stack[level + 1][0].reset();
				m.allIntersections(sray, prim_stack[level + 1][0]);
				t2.v2.copyFrom(v2);
				prim_stack[level + 1][1].copy(prim_stack[level][p2]);
				t2.v3.copyFrom(v3);
				prim_stack[level + 1][2].copy(prim_stack[level][p3]);
				double a1 = (1.0 - a12) * area;
				double c1 = subdivide_triangle(level + 1, a1);
				if (bisector < 1e-2) {
					t2.v2.copyFrom(t1.v31h);
					setRayOrigin(t2.v2);
					prim_stack[level + 1][1].reset();
					m.allIntersections(sray, prim_stack[level + 1][1]);
				} else {
					t2.v2.copyFrom(v1);
					prim_stack[level + 1][1].copy(prim_stack[level][p1]);
				}
				double a2 = a12 * area;
				double c2 = subdivide_triangle(level + 1, a2);
				return c1 + c2;
			}
		}
		if (a31 > 1e-3) {
			mid.x = (v1.x + t1.v31l.x) * 0.5;
			mid.y = (v1.y + t1.v31l.y) * 0.5;
			mid.z = (v1.z + t1.v31l.z) * 0.5;
			double bisector = binary_search(mid, v2, t1.v23l, t1.v23h, prim);
			double covered_area = a31 * bisector;
			if (level == MAX_TLEVEL - 1 || (!renient && bisector == 0)) {
				return covered_area;
			} else {
				if (bisector < 1e-2) {
					t2.v1.copyFrom(t1.v31h);
				} else {
					t2.v1.copyFrom(t1.v31l);
				}
				setRayOrigin(t2.v1);
				prim_stack[level + 1][0].reset();
				m.allIntersections(sray, prim_stack[level + 1][0]);
				t2.v2.copyFrom(v2);
				prim_stack[level + 1][1].copy(prim_stack[level][p2]);
				t2.v3.copyFrom(v3);
				prim_stack[level + 1][2].copy(prim_stack[level][p3]);
				double a1 = (1.0 - a31) * area;
				double c1 = subdivide_triangle(level + 1, a1);
				if (bisector < 1e-2) {
					t2.v3.copyFrom(t1.v12h);
					setRayOrigin(t2.v3);
					prim_stack[level + 1][2].reset();
					m.allIntersections(sray, prim_stack[level + 1][2]);
				} else {
					t2.v3.copyFrom(v1);
					prim_stack[level + 1][2].copy(prim_stack[level][p1]);
				}
				double a2 = a31 * area;
				double c2 = subdivide_triangle(level + 1, a2);
				return c1 + c2;
			}
		}
		return 0;
	}

	private P str = new P(); // string top right
	private P stl = new P(); // string top left
	private P sbr = new P(); // string bottom right
	private P sbl = new P();
	private P sstr = new P(); // substring of blocks top right
	private P sstl = new P();
	private P ssbr = new P();
	private P ssbl = new P();
	private P bktr = new P(); // block of cells top right
	private P bktl = new P();
	private P bkbr = new P();
	private P bkbl = new P();

	double calcTriangle(double cur) {
		Triangle t = triangle_stack[0];
		PrimList[] p = prim_stack[0];

		Tester.timer.start(1);
		t.v1.copyFrom(bkbl);
		t.v2.copyFrom(bktl);
		t.v3.copyFrom(bkbr);
		t.v12h.copyFrom(bktl);
		t.v23h.copyFrom(bkbr);

		setRayOrigin(bkbl);
		p[0].reset();
		m.allIntersections(sray, p[0]);
		setRayOrigin(bktl);
		p[1].reset();
		m.allIntersections(sray, p[1]);
		setRayOrigin(bkbr);
		p[2].reset();
		m.allIntersections(sray, p[2]);
		Tester.timer.end(1);
		Tester.timer.start(2);
		double t1 = subdivide_triangle(0, 1.0);
		if (t1 > 1.0) t1 = 1.0;
		if (t1 < 0.0) t1 = 0.0;
		Tester.timer.end(2);
		if (cur + t1 >= 0.2 * 20) return t1; // pruning

		Tester.timer.start(1);
		t.v1.copyFrom(bktr);
		t.v2.copyFrom(bktl);
		t.v3.copyFrom(bkbr);
		t.v12h.copyFrom(bktl);
		t.v23h.copyFrom(bkbr);

		setRayOrigin(bktr);
		p[0].reset();
		m.allIntersections(sray, p[0]);
		for (int i = 0; i < p[1].s; ++i) {
			p[1].valid[i] = true;
		}
		for (int i = 0; i < p[2].s; ++i) {
			p[2].valid[i] = true;
		}
		Tester.timer.end(1);
		Tester.timer.start(2);
		double t2 = subdivide_triangle(0, 1.0);
		if (t2 > 1.0) t2 = 1.0;
		if (t2 < 0.0) t2 = 0.0;
		Tester.timer.end(2);

		return t1 + t2;
	}

	double[] calculateOneBlanketT(Polygon panel) {
		// The length of the area containing strings is 31 meters
		// 0.75617 meters per string
		// 0.378049 meters per half string
		// cell size = 0.32 meters for 4 x 8cm
		// gap width = 0.378049 - 0.32 = 0.058049 meters
		final double gapl = 58.049; // mm
		final double block_length = 320.0 + gapl;
		final double block_l_range = 1.0;
		final double block_l_step = block_l_range / 2.0;
		final double block_l_gap = gapl / (block_length * 2);
		final double half_l_gap = block_l_gap * 0.5;
		// width of string is 4.470 meters
		// width of block of cells is 10x8cm = .8 meters
		// width of all cells = 5 x .8meters = 4 meters
		// total gap width - 4.470 - 4 meters = 0.47m
		// gap = 6 (2 gaps outside and 4 gaps between)
		// gap width = 0.470meters / 6 = 0.078333meters
		final double gapw = 78.3333; // mm
		final double block_width = 4000.0 + gapw * 6;
		final double block_w_range = (block_width - gapw) / (block_width);
		final double block_w_step = block_w_range / 5.0;
		final double block_w_gap = gapw / (block_width);
		double[] shadow_fraction = new double[NUM_STRINGS];
		for (int string = 0; string < NUM_STRINGS; string++) {
			interp(panel.v[0], panel.v[3], (string) / 41.0, sbl);
			interp(panel.v[1], panel.v[2], (string) / 41.0, sbr);
			interp(panel.v[0], panel.v[3], (string + 1.0) / 41.0, stl);
			interp(panel.v[1], panel.v[2], (string + 1.0) / 41.0, str);
			panel.visible = false; // avoid self intersections
			checkNoIntersect(sbl, stl, str, sbr);
			OUT: for (int bw = 0; bw < 5; bw++) { // width, blocks per string
				interp(sbl, sbr, (bw * block_w_step + block_w_gap), ssbl);
				interp(stl, str, (bw * block_w_step + block_w_gap), sstl);
				interp(sbl, sbr, ((bw + 1) * block_w_step), ssbr);
				interp(stl, str, ((bw + 1) * block_w_step), sstr);
				for (int bl = 0; bl < 2; ++bl) {
					interp(ssbl, sstl, (bl * block_l_step + half_l_gap), bkbl);
					interp(ssbr, sstr, (bl * block_l_step + half_l_gap), bkbr);
					interp(ssbl, sstl, ((bl + 1) * block_l_step - half_l_gap), bktl);
					interp(ssbr, sstr, ((bl + 1) * block_l_step - half_l_gap), bktr);
					shadow_fraction[string] += calcTriangle(shadow_fraction[string]);
					if (shadow_fraction[string] >= 0.2 * 20) {
						break OUT;
					}
				}
			}
			shadow_fraction[string] *= 1.0 / 20.0; // 10 blocks/string x 2 tri/block
			revertNoIntersect();
		}
		return shadow_fraction;
	}

	static class RectD {
		double amin = 1e300;
		double amax = -1e300;
		double bmin = 1e300;
		double bmax = -1e300;
		double cmin = 1e300;
		double cmax = -1e300;

		void update(double a, double b, double c) {
			amin = Math.min(amin, a);
			amax = Math.max(amax, a);
			bmin = Math.min(bmin, b);
			bmax = Math.max(bmax, b);
			cmin = Math.min(cmin, c);
			cmax = Math.max(cmax, c);
		}

		void update(P p) {
			update(p.x, p.y, p.z);
		}

		boolean intersect(RectD o) {
			if (o.amax <= this.amin) return false;
			if (o.bmax <= this.bmin) return false;
			if (o.bmin >= this.bmax) return false;
			if (o.cmax <= this.cmin) return false;
			if (o.cmin >= this.cmax) return false;
			return true;
		}
	}

	P convertSunCoord(P p) {
		P ret = new P();
		ret.x = this.sray.d.dotp(p);
		ret.y = this.srayB.dotp(p);
		ret.z = this.srayC.dotp(p);
		return ret;
	}

	void checkNoIntersect(P p0, P p1, P p2, P p3) {
		RectD rect = new RectD();
		P[] convP = new P[4];
		convP[0] = convertSunCoord(p0);
		rect.update(convP[0]);
		convP[1] = convertSunCoord(p1);
		rect.update(convP[1]);
		convP[2] = convertSunCoord(p2);
		rect.update(convP[2]);
		convP[3] = convertSunCoord(p3);
		rect.update(convP[3]);

		for (int i = 0; i < polygons.size(); ++i) {
			Polygon poly = polygons.get(i);
			if (poly.maxA <= rect.amin || poly.maxB <= rect.bmin || poly.minB >= rect.bmax || poly.maxC <= rect.cmin
					|| poly.minC >= rect.cmax) {
				poly.visible = false;
			}
		}
		for (int i = 0; i < cylinders.size(); ++i) {
			Cylinder cyl = cylinders.get(i);
			if (cyl.isLongeron) continue;
			if (cyl.maxA <= rect.amin || cyl.maxB <= rect.bmin || cyl.minB >= rect.bmax || cyl.maxC <= rect.cmin
					|| cyl.minC >= rect.cmax) {
				cyl.visible = false;
			}
		}
		m.setVisibility();
	}

	void revertNoIntersect() {
		m.resetVisibility();
	}

	boolean isLongeronShadowed(Cylinder longeron) {
		P v1 = longeron.a1;
		P v2 = longeron.a2;
		int samples = (int) (v1.dist(v2) / 200);
		double delta = 1.0 / samples; // space initial samples 20cm
		boolean shadow = false;
		double prev_pos = 0;
		double shadow_fraction = 0;
		setRayOrigin(v1);
		boolean prev_shadow = m.anyIntersection(sray);
		for (double alpha = delta; alpha < 1.000001; alpha += delta) {
			setRayOrigin(v1.x * (1 - alpha) + v2.x * alpha, v1.y * (1 - alpha) + v2.y * alpha, v1.z * (1 - alpha) + v2.z
					* alpha);
			shadow = m.anyIntersection(sray);
			if (prev_shadow && shadow) {
				shadow_fraction += alpha - prev_pos;
				if (shadow_fraction >= 0.1) return true;
			} else if (prev_shadow != shadow) {
				double start = prev_pos;
				double finish = alpha;
				while (finish - start > 0.00001) {
					double beta = (finish + start) / 2;
					setRayOrigin(v1.x * (1 - beta) + v2.x * beta, v1.y * (1 - beta) + v2.y * beta, v1.z * (1 - beta) + v2.z
							* beta);
					if (m.anyIntersection(sray) == prev_shadow) {
						start = beta;
					} else {
						finish = beta;
					}
				}
				if (prev_shadow) {
					shadow_fraction += (finish + start) / 2 - prev_pos;
				} else {
					shadow_fraction += alpha - (finish + start) / 2;
				}
				if (shadow_fraction >= 0.1) return true;
			}
			prev_pos = alpha;
			prev_shadow = shadow;
		}
		return false;
	}

	static class Reader {
		private static FileInputStream in;

		private static String readString() throws Exception {
			int c = 0;
			String val = "";
			c = in.read();
			while (c <= 32 || c == ',') { // commma considered white space
				if (c == -1) break;
				c = in.read();
			}
			if (c == '{') { // comments
				c = in.read();
				while (c != '}')
					c = in.read();
				return readString();
			}
			while (c > 32 && c != ',') {
				val += (char) c;
				c = in.read();
			}
			return val;
		}

		private static double readDouble() {
			try {
				return Double.parseDouble(readString());
			} catch (Exception e) {
				System.err.println("number format exception");
			}
			return 0;
		}

		private void expect(String key) throws Exception {
			expect(readString(), key);
		}

		private void expect(String got, String key) throws Exception {
			if (!got.equals(key)) {
				System.err.println("parser expected " + key + " but got " + got);
			}
		}

		private Prim readCylinder() throws Exception {
			expect("radius");
			double r = readDouble();
			expect("center");
			double x = readDouble();
			double y = readDouble();
			double z = readDouble();
			P a1 = new P(x, y, z);
			expect("center");
			x = readDouble();
			y = readDouble();
			z = readDouble();
			P a2 = new P(x, y, z);
			expect("endcylinder");
			return new Cylinder(a1, a2, r);
		}

		private Prim readPolygon() throws Exception {
			P[] vtx = new P[100];
			int vc = 0;
			String vs = readString();
			while (vs.equals("vertex")) {
				double x = readDouble();
				double y = readDouble();
				double z = readDouble();
				P pt = new P(x, y, z);
				vtx[vc++] = pt;
				vs = readString();
			}
			expect(vs, "endpolygon");
			Polygon p = new Polygon(vtx, vc);
			return p;
		}

		private Prim readPrim(String what) throws Exception {
			Prim p = null;
			if (what.equals("polygon"))
				p = readPolygon();
			else if (what.equals("cylinder"))
				p = readCylinder();
			else {
				expect(what, "polygon, cylinder or sphere");
			}
			return p;
		}

		private Part readShape() throws Exception {
			Prim[] pt = new Prim[1000];
			Part sol = new Part();
			int pc = 0;
			String key;
			readString();
			key = readString();
			while (key.equals("cylinder") || key.equals("polygon")) {
				pt[pc++] = readPrim(key);
				key = readString();
			}
			sol.obj = new Prim[pc];
			for (int i = 0; i < pc; ++i)
				sol.obj[i] = pt[i];
			expect(key, "endshape");
			return sol;
		}

		private Structure readStructure() throws Exception {
			Structure mod = new Structure();
			Part[] sa = new Part[100];
			String key = readString();
			expect("parts");
			key = readString();
			int si = 0;
			while (key.equals("shape")) {
				sa[si++] = readShape();
				key = readString();
			}
			mod.solid = new Part[si];
			for (int i = 0; i < si; ++i) {
				mod.solid[i] = sa[i];
				mod.solid[i].makeBound();
			}
			expect(key, "endparts");
			expect("children");
			int childCount = 0;
			Structure[] tc = new Structure[100];
			key = readString();
			while (key.equals("structure")) {
				tc[childCount++] = readStructure();
				key = readString();
			}
			mod.child = new Structure[childCount];
			for (int i = 0; i < childCount; ++i)
				mod.child[i] = tc[i];
			expect(key, "endchildren");
			expect("endstructure");
			mod.makeBound();
			return mod;
		}

		Structure readModel() {
			Structure mod = null;
			try {
				in = new FileInputStream("ISS_simple.model");
				expect("model");
				readString();
				expect("structure");
				mod = readStructure();
				expect("endmodel");
			} catch (Exception e) {
				System.out.println("ERROR: reading input file");
				e.printStackTrace();
				System.exit(666);
			} finally {
				try {
					in.close();
				} catch (IOException e) {}
			}
			fix_longerons(mod);
			return mod;
		}

		private void fix_longeron(Structure m, int side, int saw, int[] offsets) {
			Cylinder c = (Cylinder) m.child[side].child[saw].solid[5].obj[0];
			P axis = c.a1;
			m.child[side].child[saw].solid[4].isLongeron = true; // set longeons invisible
			for (int i = 0; i < 4; i++) {
				Cylinder longeron = (Cylinder) m.child[side].child[saw].solid[4].obj[i];
				longeron.isLongeron = true;
				longeron.a1.y = longeron.a2.y = axis.y - offsets[2 * i];
				longeron.a1.z = longeron.a2.z = axis.z - offsets[2 * i + 1];
				longeron.original_a1.y = longeron.original_a2.y = longeron.a1.y;
				longeron.original_a1.z = longeron.original_a2.z = longeron.a1.z;
			}
		}

		private void fix_longerons(Structure m) {
			// 1A ( y,z, y,z, y,z, y,z ) 1B ( ... ) ... 3B ( ... ) 4B ( ... )
			int[][] offsets = new int[][] { { -181, -502, 580, -367, 444, 394, -316, 258 },
					{ 386, -637, -375, -503, -241, 258, 520, 124 }, { 183, -622, -578, -489, -446, 272, 316, 139 },
					{ -322, -542, 439, -407, 305, 354, -456, 219 }, { 296, -607, -464, -473, -330, 288, 431, 154 },
					{ -243, -558, 517, -422, 381, 339, -379, 202 }, { -248, -562, 513, -430, 380, 332, -381, 199 },
					{ 246, -566, -515, -434, -383, 327, 378, 195 } };
			for (int i = 0; i < 8; ++i) {
				fix_longeron(m, i % 2, i / 2, offsets[i]);
			}
		}

	}

}
