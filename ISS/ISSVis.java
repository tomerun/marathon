import java.util.ArrayList;
import java.util.Arrays;

class M {
	double[][] m; // the current transformation of this instance
	static double[][] tempMc = new double[4][4]; // compose temporary matrix
	static double[] tempA = new double[4];

	public String toString() {
		return Arrays.deepToString(this.m);
	}

	void transform(V in, V out) {
		for (int i = 0; i < 3; ++i) {
			tempA[i] = m[i][0] * in.x;
			tempA[i] += m[i][1] * in.y;
			tempA[i] += m[i][2] * in.z;
			tempA[i] += m[i][3];
		}
		out.x = tempA[0];
		out.y = tempA[1];
		out.z = tempA[2];
	}

	void transform(P in, P out) {
		for (int i = 0; i < 3; ++i) {
			tempA[i] = m[i][0] * in.x;
			tempA[i] += m[i][1] * in.y;
			tempA[i] += m[i][2] * in.z;
			tempA[i] += m[i][3];
		}
		out.x = tempA[0];
		out.y = tempA[1];
		out.z = tempA[2];
	}

	void compose_pre(M pre, M out) {
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j) {
				tempMc[i][j] = 0;
				for (int k = 0; k < 4; ++k)
					tempMc[i][j] += pre.m[i][k] * m[k][j];
			}
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j)
				out.m[i][j] = tempMc[i][j];
	}

	static void translate(double x, double y, double z, M out) {
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j)
				out.m[i][j] = 0;
			out.m[i][i] = 1;
		}
		out.m[0][3] = x;
		out.m[1][3] = y;
		out.m[2][3] = z;
	}

	static void negative_translate(V trans, M out) {
		M.translate(-trans.x, -trans.y, -trans.z, out);
	}

	void translate_compose(V trans, M out) {
		M tmp = new M();
		M.translate(trans.x, trans.y, trans.z, tmp);
		compose_pre(tmp, out);
	}

	void rotate_x_axis_compose(double angle, M out) {
		M rot = M.rotate_x_axis(angle);
		compose_pre(rot, out);
	}

	void rotate_y_axis_compose(double angle, M out) {
		M rot = M.rotate_y_axis(angle);
		compose_pre(rot, out);
	}

	void rotate_z_axis_compose(double angle, M out) {
		M rot = M.rotate_z_axis(angle);
		compose_pre(rot, out);
	}

	static M rotate_x_axis(double angle) {
		M out = new M();
		double ang = angle / 180 * Math.PI;
		out.m[1][1] = out.m[2][2] = Math.cos(ang);
		out.m[2][1] = Math.sin(ang);
		out.m[1][2] = -out.m[2][1];
		return out;
	}

	static M rotate_y_axis(double angle) {
		M out = new M();
		double ang = angle / 180 * Math.PI;
		out.m[0][0] = out.m[2][2] = Math.cos(ang);
		out.m[2][0] = Math.sin(ang);
		out.m[0][2] = -out.m[2][0];
		return out;
	}

	static M rotate_z_axis(double angle) {
		M out = new M();
		double ang = angle / 180 * Math.PI;
		out.m[0][0] = out.m[1][1] = Math.cos(ang);
		out.m[0][1] = Math.sin(ang);
		out.m[1][0] = -out.m[0][1];
		return out;
	}

	M() {
		m = new double[4][4];
		for (int i = 0; i < 4; ++i) {
			m[i][i] = 1;
		}
	}

	M(M orig) {
		m = new double[4][4];
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j)
				m[i][j] = orig.m[i][j];
		}
	}

} // end of class M 

class V {
	double x, y, z;

	double dotp(P v) {
		return x * v.x + y * v.y + z * v.z;
	}

	double dot(double x, double y, double z) {
		return this.x * x + this.y * y + this.z * z;
	}

	double dot(V v) {
		return x * v.x + y * v.y + z * v.z;
	}

	public String toString() {
		return x + "," + y + "," + z;
	}

	V(double x1, double x2, double x3) {
		x = x1;
		y = x2;
		z = x3;
	}

	V(V o) {
		x = o.x;
		y = o.y;
		z = o.z;
	}

	V() {
		x = 0;
		y = 0;
		z = 0;
	}
} // end class V

class P {
	double x, y, z;

	double dist2(P a) {
		return ((x - a.x) * (x - a.x) + (y - a.y) * (y - a.y) + (z - a.z) * (z - a.z));
	}

	double dist(P a) {
		return Math.sqrt((x - a.x) * (x - a.x) + (y - a.y) * (y - a.y) + (z - a.z) * (z - a.z));
	}

	void copyFrom(P o) {
		this.x = o.x;
		this.y = o.y;
		this.z = o.z;
	}

	public String toString() {
		return x + "," + y + "," + z;
	}

	P() {}

	P(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	P(P o) {
		x = o.x;
		y = o.y;
		z = o.z;
	}
} // end of class P

class BBox {
	double xmin, xmax, ymin, ymax, zmin, zmax;

	BBox() {
		xmin = 1e50;
		xmax = -1e50;
		ymin = 1e50;
		ymax = -1e50;
		zmin = 1e50;
		zmax = -1e50;
	}
} // end class BBox

class Part {
	Prim[] obj = new Prim[0];
	Sphere bound;
	boolean isLongeron = false;

	void collect(ArrayList<Polygon> polygons, ArrayList<Cylinder> cylinders) {
		for (int i = 0; i < obj.length; ++i) {
			obj[i].collect(polygons, cylinders);
		}
	}

	void precalc(V sray, V srayB, V srayC) {
		for (int i = 0; i < obj.length; ++i) {
			obj[i].precalc(sray, srayB, srayC);
		}
		this.bound.precalc(sray, srayB, srayC);
	}

	void setVisibility() {
		this.bound.visible = false;
		for (int i = 0; i < obj.length; ++i) {
			if (obj[i].visible) {
				this.bound.visible = true;
				return;
			}
		}
	}

	void resetVisibility() {
		this.bound.visible = true;
		for (int i = 0; i < obj.length; ++i) {
			obj[i].visible = true;
		}
	}

	void print(int depth) {
		for (int i = 0; i < depth; ++i) {
			System.out.print("  ");
		}
		System.out.println("part");
		for (int i = 0; i < obj.length; ++i)
			obj[i].print(depth + 1);
	}

	void makeBound() {
		BBox b = new BBox();
		bound(b);
		this.bound = new Sphere(new P((b.xmin + b.xmax) / 2, (b.ymin + b.ymax) / 2, (b.zmin + b.zmax) / 2),
				0.5 * Math.sqrt((b.xmin - b.xmax) * (b.xmin - b.xmax) + (b.ymin - b.ymax) * (b.ymin - b.ymax)
						+ (b.zmin - b.zmax) * (b.zmin - b.zmax)));
	}

	void bound(BBox bb) {
		for (int i = 0; i < obj.length; ++i)
			obj[i].bound(bb);
	}

	void transform(M m) {
		for (int i = 0; i < obj.length; ++i)
			obj[i].transform(m);
		if (bound != null) bound.transform(m);
	}

	boolean anyIntersection(Ray r) {
		if (!this.isLongeron && bound.anyIntersection(r)) {
			for (int i = 0; i < obj.length; ++i) {
				if (obj[i].anyIntersection(r)) return true;
			}
		}
		return false;
	}

	void allIntersections(Ray ray, PrimList pl) {
		if (!this.isLongeron && bound.anyIntersection(ray)) {
			for (int s = 0; s < obj.length; ++s) {
				if (obj[s].anyIntersection(ray)) pl.add(obj[s]);
			}
		}
	}

	boolean visible() {
		return !this.isLongeron && this.bound.visible;
	}

} // end class Part

class Structure {
	Part[] solid = new Part[0];
	Structure[] child = new Structure[0];
	Sphere bb = null;
	M transform = new M();

	void collect(ArrayList<Polygon> polygons, ArrayList<Cylinder> cylinders) {
		for (int i = 0; i < solid.length; ++i) {
			solid[i].collect(polygons, cylinders);
		}
		for (int i = 0; i < child.length; ++i) {
			child[i].collect(polygons, cylinders);
		}
	}

	void precalc(V sray, V srayB, V srayC) {
		for (int i = 0; i < solid.length; ++i) {
			solid[i].precalc(sray, srayB, srayC);
		}
		for (int i = 0; i < child.length; ++i) {
			child[i].precalc(sray, srayB, srayC);
		}
		this.bb.precalc(sray, srayB, srayC);
	}

	void setVisibility() {
		this.bb.visible = false;
		for (int i = 0; i < solid.length; ++i) {
			solid[i].setVisibility();
			this.bb.visible |= solid[i].visible();
		}
		for (int i = 0; i < child.length; ++i) {
			child[i].setVisibility();
		}
	}

	void resetVisibility() {
		for (int i = 0; i < solid.length; ++i) {
			solid[i].resetVisibility();
		}
		this.bb.visible = true;
		for (int i = 0; i < child.length; ++i) {
			child[i].resetVisibility();
		}
	}

	void print(int depth) {
		for (int i = 0; i < depth; ++i) {
			System.out.print("  ");
		}
		System.out.println("structure");
		for (int i = 0; i < solid.length; ++i)
			solid[i].print(depth + 1);
		for (int i = 0; i < child.length; ++i)
			child[i].print(depth + 1);
	}

	void makeBound() {
		BBox b = new BBox();
		bound(b);
		bb = new Sphere(new P((b.xmin + b.xmax) / 2, (b.ymin + b.ymax) / 2, (b.zmin + b.zmax) / 2),
				0.5 * Math.sqrt((b.xmin - b.xmax) * (b.xmin - b.xmax) + (b.ymin - b.ymax) * (b.ymin - b.ymax)
						+ (b.zmin - b.zmax) * (b.zmin - b.zmax)));
	}

	void bound(BBox b) {
		for (int i = 0; i < solid.length; ++i)
			solid[i].bound(b);
	}

	void transform(M m) {
		M new_m = new M();
		transform.compose_pre(m, new_m);
		for (int i = 0; i < solid.length; ++i)
			solid[i].transform(new_m);
		for (int i = 0; i < child.length; ++i)
			child[i].transform(new_m);
		if (bb != null) bb.transform(new_m);
	}

	void transform() {
		M new_m = new M();
		transform(new_m);
	}

	void allIntersections(Ray ray, PrimList pl) {
		if (bb.anyIntersection(ray)) {
			for (int s = 0; s < solid.length; ++s)
				solid[s].allIntersections(ray, pl);
		}
		for (int c = 0; c < child.length; c++)
			child[c].allIntersections(ray, pl);
	}

	boolean anyIntersection(Ray ray) {
		if (bb.anyIntersection(ray)) {
			for (int s = 0; s < solid.length; ++s) {
				if (solid[s].anyIntersection(ray)) return true;
			}
		}
		for (int c = 0; c < child.length; c++) {
			if (child[c].anyIntersection(ray)) return true;
		}
		return false;
	}
} // end of class Structure

class Ray {
	P o;
	V d;
	double a, b, c;

	Ray() {
		o = new P();
		d = new V();
	}
} // end of class Ray

class PrimList {
	Prim[] prim;
	boolean[] valid;
	int s;

	void copy(PrimList original) {
		s = original.s;
		for (int i = 0; i < s; ++i) {
			valid[i] = true;
			prim[i] = original.prim[i];
		}
	}

	void reset() {
		s = 0;
	}

	void add(Prim p) {
		valid[s] = true;
		prim[s++] = p;
	}

	PrimList() {
		prim = new Prim[64];
		valid = new boolean[64];
		s = 0;
	}
} // end of class PrimList

abstract class Prim {
	boolean visible = true;
	int order;
	static int next_order = 0;
	double maxA, minB, maxB, minC, maxC;

	abstract boolean anyIntersection(Ray r);

	abstract void transform(M old);

	abstract void bound(BBox b);

	abstract void print(int depth);

	abstract void precalc(V sray, V srayB, V srayC);

	void collect(ArrayList<Polygon> polygons, ArrayList<Cylinder> cylinders) {}

	Prim() {
		order = next_order++;
	}
} // end of class Prim

class Sphere extends Prim {
	final double rad, rad2;
	P cen, original_cen;
	double maxA, cenB, cenC;

	public String toString() {
		return cen + "," + original_cen + "," + rad;
	}

	void print(int depth) {
		for (int i = 0; i < depth; ++i) {
			System.out.print("  ");
		}
		System.out.println("sphere");
	}

	void bound(BBox b) {
		if (cen.x + rad > b.xmax) b.xmax = cen.x + rad;
		if (cen.y + rad > b.ymax) b.ymax = cen.y + rad;
		if (cen.z + rad > b.zmax) b.zmax = cen.z + rad;
		if (cen.x - rad < b.xmin) b.xmin = cen.x - rad;
		if (cen.y - rad < b.ymin) b.ymin = cen.y - rad;
		if (cen.z - rad < b.zmin) b.zmin = cen.z - rad;
	}

	void transform(M m) {
		m.transform(original_cen, cen);
	}

	boolean anyIntersection(Ray ray) {
		if (!this.visible) return false;
		if (ray.a >= maxA) return false;
		double dy = ray.b - cenB;
		double dz = ray.c - cenC;
		return dy * dy + dz * dz <= rad2;
	}

	void precalc(V sray, V srayB, V srayC) {
		maxA = sray.dotp(cen) + rad;
		cenB = srayB.dotp(cen);
		cenC = srayC.dotp(cen);
	}

	Sphere(P c, double radius) {
		cen = new P(c);
		original_cen = new P(c);
		rad = radius;
		rad2 = rad * rad;
	}
} // end of class sphere

class Cylinder extends Prim {
	double rad, rad2;
	P a1, original_a1, a2, original_a2;
	double length;
	V n; // unit vector along axis from a1 to a2
	double D1; // n and d are equation of plane of end cap 1
	double D2; // resp. end cap 2
	boolean isLongeron = false;
	double prod, d12, d23, d31, A;

	void collect(ArrayList<Polygon> polygons, ArrayList<Cylinder> cylinders) {
		cylinders.add(this);
	}

	void print(int depth) {
		for (int i = 0; i < depth; ++i) {
			System.out.print("  ");
		}
		System.out.println("cylinder");
	}

	void bound(BBox b) {
		if (a1.x + rad > b.xmax) b.xmax = a1.x + rad;
		if (a1.y + rad > b.ymax) b.ymax = a1.y + rad;
		if (a1.z + rad > b.zmax) b.zmax = a1.z + rad;
		if (a1.x - rad < b.xmin) b.xmin = a1.x - rad;
		if (a1.y - rad < b.ymin) b.ymin = a1.y - rad;
		if (a1.z - rad < b.zmin) b.zmin = a1.z - rad;
		if (a2.x + rad > b.xmax) b.xmax = a2.x + rad;
		if (a2.y + rad > b.ymax) b.ymax = a2.y + rad;
		if (a2.z + rad > b.zmax) b.zmax = a2.z + rad;
		if (a2.x - rad < b.xmin) b.xmin = a2.x - rad;
		if (a2.y - rad < b.ymin) b.ymin = a2.y - rad;
		if (a2.z - rad < b.zmin) b.zmin = a2.z - rad;
	}

	void transform(M m) {
		m.transform(original_a1, a1);
		m.transform(original_a2, a2);
		n.x = (a2.x - a1.x) / length;
		n.y = (a2.y - a1.y) / length;
		n.z = (a2.z - a1.z) / length;
		D1 = -(a1.x * n.x + a1.y * n.y + a1.z * n.z);
		D2 = -(a2.x * n.x + a2.y * n.y + a2.z * n.z);
	}

	Cylinder(P p1, P p2, double radius) {
		a1 = new P(p1);
		a2 = new P(p2);
		original_a1 = new P(p1);
		original_a2 = new P(p2);
		rad = radius;
		length = Math.sqrt((a1.x - a2.x) * (a1.x - a2.x) + (a1.y - a2.y) * (a1.y - a2.y) + (a1.z - a2.z) * (a1.z - a2.z));
		n = new V((a2.x - a1.x) / length, (a2.y - a1.y) / length, (a2.z - a1.z) / length);
		D1 = -(a1.x * n.x + a1.y * n.y + a1.z * n.z);
		D2 = -(a2.x * n.x + a2.y * n.y + a2.z * n.z);
	}

	void precalc(V sray, V srayB, V srayC) {
		this.prod = n.x * sray.x + n.y * sray.y + n.z * sray.z;
		this.d12 = n.y * sray.x - n.x * sray.y;
		this.d23 = n.z * sray.y - n.y * sray.z;
		this.d31 = n.x * sray.z - n.z * sray.x;
		this.A = d12 * d12 + d23 * d23 + d31 * d31;
		double v1 = sray.dotp(a1);
		double v2 = sray.dotp(a2);
		maxA = Math.max(v1, v2);
		v1 = srayB.dotp(a1);
		v2 = srayB.dotp(a2);
		minB = Math.min(v1, v2);
		maxB = Math.max(v1, v2);
		v1 = srayC.dotp(a1);
		v2 = srayC.dotp(a2);
		minC = Math.min(v1, v2);
		maxC = Math.max(v1, v2);
		minB -= rad;
		minC -= rad;
		maxA += rad;
		maxB += rad;
		maxC += rad;
	}

	boolean anyIntersection(Ray ray) {
		if (!visible) return false;
		double o1 = ray.o.x - a1.x;
		double o2 = ray.o.y - a1.y;
		double o3 = ray.o.z - a1.z; // ray origin shifted so a1 = (0,0,0)
		double c1 = n.x;
		double c2 = n.y;
		double c3 = n.z; // direction vector for cylinder
		double t, d;
		{
			// (t * ray.d + o) cross c = t(d12+d23+d31) + o12+o23+o31
			double o12 = c2 * o1 - c1 * o2;
			double o23 = c3 * o2 - c2 * o3;
			double o31 = c1 * o3 - c3 * o1; // cross product terms
			double B = d23 * o23 + d12 * o12 + d31 * o31;
			double C = o12 * o12 + o23 * o23 + o31 * o31 - rad * rad;
			double disc = B * B - A * C;
			if (disc < 0) return false;
			// hit infinite cylinder or cylinder parallel to ray, 
			// now check for finite cylinder wall
			double i1, i2, i3;
			double sqdisc = Math.sqrt(disc);
			if (-B - sqdisc >= 0) {
				t = (-B - sqdisc) / A;
				i1 = t * ray.d.x + ray.o.x;
				i2 = t * ray.d.y + ray.o.y;
				i3 = t * ray.d.z + ray.o.z; // (i1, i2, i3) is intersection point
				d = i1 * c1 + i2 * c2 + i3 * c3 + D1;
				if (d >= 0 && d <= length) return true; // farthest intersection is on finite cylinder wall
			}
			if (-B + sqdisc >= 0) {
				t = (-B + sqdisc) / A;
				i1 = t * ray.d.x + ray.o.x;
				i2 = t * ray.d.y + ray.o.y;
				i3 = t * ray.d.z + ray.o.z; // (i1, i2, i3) is intersection point
				d = i1 * c1 + i2 * c2 + i3 * c3 + D1;
				if (d >= 0 && d <= length) return true; // nearest intersection is on finite cylinder wall
			}
		}
		// we could possibly hit the end caps without hitting the finite cylinder wall so...
		{
			// only test end cap a1 is sufficient
			double prodp = c1 * ray.o.x + c2 * ray.o.y + c3 * ray.o.z;
			t = -(prodp + D1) / prod;
			if (t >= 0) {
				double i1 = t * ray.d.x + ray.o.x;
				double i2 = t * ray.d.y + ray.o.y;
				double i3 = t * ray.d.z + ray.o.z;
				d = ((i1 - a1.x) * (i1 - a1.x) + (i2 - a1.y) * (i2 - a1.y) + (i3 - a1.z) * (i3 - a1.z));
				if (d <= rad * rad) return true; // hit end cap 1
			}
		}
		return false;
	}
} // end of class cylinder

class Polygon extends Prim { // convex only
	P[] v;
	P[] original_v;
	V normal;
	double A, B, C, D;
	double prod;
	double[] srB, srC;

	void collect(ArrayList<Polygon> polygons, ArrayList<Cylinder> cylinders) {
		polygons.add(this);
	}

	void print(int depth) {
		for (int i = 0; i < depth; ++i) {
			System.out.print("  ");
		}
		System.out.println("polygon");
	}

	public String toString() {
		String ret = "";
		ret += Arrays.toString(v);
		ret += " ";
		ret += Arrays.toString(original_v);
		ret += " " + A + " " + B + " " + C + " " + D;
		return ret;
	}

	void bound(BBox b) {
		for (int i = 0; i < v.length; ++i) {
			if (v[i].x > b.xmax) b.xmax = v[i].x;
			if (v[i].y > b.ymax) b.ymax = v[i].y;
			if (v[i].z > b.zmax) b.zmax = v[i].z;
			if (v[i].x < b.xmin) b.xmin = v[i].x;
			if (v[i].y < b.ymin) b.ymin = v[i].y;
			if (v[i].z < b.zmin) b.zmin = v[i].z;
		}
	}

	void transform(M m) {
		for (int i = 0; i < v.length; ++i)
			m.transform(original_v[i], v[i]);
		setupEquation();
	}

	void precalc(V sray, V srayB, V srayC) {
		this.prod = A * sray.x + B * sray.y + C * sray.z;
		if (Math.abs(this.prod) <= 0.000001) this.prod = 0;
		if (this.prod < 0) {
			A *= -1;
			B *= -1;
			C *= -1;
			D *= -1;
		}
		this.srB = new double[this.v.length];
		this.srC = new double[this.v.length];
		maxA = maxB = maxC = -1e300;
		minB = minC = 1e300;
		for (int i = 0; i < v.length; ++i) {
			double pa = sray.dotp(v[i]);
			srB[i] = srayB.dotp(v[i]);
			srC[i] = srayC.dotp(v[i]);
			maxA = Math.max(maxA, pa);
			minB = Math.min(minB, srB[i]);
			maxB = Math.max(maxB, srB[i]);
			minC = Math.min(minC, srC[i]);
			maxC = Math.max(maxC, srC[i]);
		}
	}

	boolean anyIntersection(Ray ray) {
		if (!visible) return false;
		if (prod == 0) return false; // plane parallel to ray 
		if (A * ray.o.x + B * ray.o.y + C * ray.o.z + D >= 0) return false; // behind us
		if (ray.b < minB || ray.b > maxB || ray.c < minC || ray.c > maxC) return false;
		return inner(ray.b, ray.c);
	}

	// I already know this polygon intersects some part of string  
	boolean anyIntersectionPure(Ray ray) {
		return inner(ray.b, ray.c);
	}

	boolean inner(double b, double c) {
		double sign = (srB[v.length - 1] - b) * (srC[0] - c) - (srB[0] - b) * (srC[v.length - 1] - c);
		for (int i = 1; i < v.length; ++i) {
			if (sign * ((srB[i - 1] - b) * (srC[i] - c) - (srB[i] - b) * (srC[i - 1] - c)) <= 0) {
				return false;
			}
		}
		return true;
	}

	double[] findCrossPoint(double b3, double c3, double b4, double c4) {
		double[] v = new double[] { 0, 1 };
		for (int i = 0; i < ISSModel.MAX_BSEARCH_LEVEL; ++i) {
			double m = (v[0] + v[1]) * 0.5;
			double b = b3 + (b4 - b3) * m;
			double c = c3 + (c4 - c3) * m;
			if (inner(b, c)) {
				v[0] = m;
			} else {
				v[1] = m;
			}
		}
		return v;
	}

	Polygon(P[] p, int pc) {
		v = new P[pc];
		original_v = new P[pc];
		for (int i = 0; i < pc; i++) {
			v[i] = new P(p[i]);
			original_v[i] = new P(p[i]);
		}
		setupEquation();
	}

	void setupEquation() {
		A = (v[0].z - v[1].z) * (v[2].y - v[1].y) - (v[0].y - v[1].y) * (v[2].z - v[1].z);
		B = (v[0].x - v[1].x) * (v[2].z - v[1].z) - (v[0].z - v[1].z) * (v[2].x - v[1].x);
		C = (v[0].y - v[1].y) * (v[2].x - v[1].x) - (v[0].x - v[1].x) * (v[2].y - v[1].y);
		double n = Math.sqrt(A * A + B * B + C * C);
		A /= n;
		B /= n;
		C /= n;
		D = -(A * v[0].x + B * v[0].y + C * v[0].z);
		//		D = 0;
		//		for (int i = 0; i < v.length; ++i)
		//			D -= (A * v[0].x + B * v[0].y + C * v[0].z);
		//		D /= v.length;
		normal = new V(A, B, C);
	}
} // end class Polygon 

class Triangle {
	P v1, v2, v3, v12l, v23l, v31l, v12h, v23h, v31h;

	Triangle() {
		v1 = new P();
		v2 = new P();
		v3 = new P();
		v12l = new P();
		v23l = new P();
		v31l = new P();
		v12h = new P();
		v23h = new P();
		v31h = new P();
	}
} // end class Triangle 

final class Timer {
	static final boolean MEASURE_TIME = false;
	long[] sum = new long[20];
	long[] start = new long[sum.length];

	void start(int i) {
		if (MEASURE_TIME) {
			start[i] = System.currentTimeMillis();
		}
	}

	void end(int i) {
		if (MEASURE_TIME) {
			sum[i] += System.currentTimeMillis() - start[i];
		}
	}
}