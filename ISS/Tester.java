import java.util.Arrays;
import java.util.Random;

class Solver {

	private static final double EPS = 1e-9;
	ISSModel model;

	Solver(double beta, double yaw) {
		this.model = new ISSModel(beta, yaw);
	}

	Result calc(int ti, State os, State ns) {
		Result res = new Result();
		for (int i = 0; i < 2; ++i) {
			double move = transit(os.angle[i], os.velocity[i], ns.angle[i], ns.velocity[i], 0.15, 0.005);
			if (move < 0) {
				System.out.println("cannot transit at SARJ:" + i + " " + os.angle[i] + " " + os.velocity[i] + " " + ns.angle[i]
						+ " " + ns.velocity[i]);
				return null;
			}
		}
		if (!calcBGAMove(res, os, ns)) {
			return null;
		}
		model.calc(ti, ns.angle, res);
		return res;
	}

	Result calc(int ti, State init) {
		Result res = new Result();
		model.calc(ti, init.angle, res);
		return res;
	}

	Result calc(double alpha, State init) {
		Result res = new Result();
		model.calc(alpha, init.angle, res);
		return res;
	}

	Result[] calcAll(State[] states) {
		Result[] results = new Result[ISSModel.NUM_STATES];
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			results[i] = calc(i, states[i == 0 ? ISSModel.NUM_STATES - 1 : i - 1], states[i]);
			if (results[i] == null) {
				System.out.println("turn " + i + ":cannot transit");
				return null;
			}
		}
		return results;
	}

	boolean calcBGAMove(Result res, State os, State ns) {
		for (int i = 2; i < 10; ++i) {
			res.move[i - 2] = transit(os.angle[i], os.velocity[i], ns.angle[i], ns.velocity[i], 0.25, 0.01);
			if (res.move[i - 2] < 0) {
				//				System.out.println("cannot transit at BGA:" + i);
				return false;
			}
		}
		return true;
	}

	static boolean canTransit(State os, State ns) {
		for (int i = 0; i < 2; ++i) {
			if (transit(os.angle[i], os.velocity[i], ns.angle[i], ns.velocity[i], 0.15, 0.005) < 0) {
				//				System.out.println(i + " " + os.angle[i] + " " + ns.angle[i]);
				return false;
			}
		}
		for (int i = 2; i < 10; ++i) {
			if (transit(os.angle[i], os.velocity[i], ns.angle[i], ns.velocity[i], 0.25, 0.01) < 0) {
				//				System.out.println(i + " " + os.angle[i] + " " + ns.angle[i]);
				return false;
			}
		}
		return true;
	}

	// when transition is impossible, return negative value
	static double transit(double oa, double ov, double na, double nv, double maxSpeed, double maxAcc) {
		if (ov < 0) return transit(-oa, -ov, -na, -nv, maxSpeed, maxAcc);
		if (Math.abs(nv) > maxSpeed) return -1;
		double shift = na - oa;
		if (shift < -180.0) shift += 360.0;
		if (shift > 180.0) shift -= 360.0;
		if (!canTransit(shift, ov, nv, -maxSpeed, maxSpeed, maxAcc)) return -1;
		if (nv >= 0) {
			// can we always keep speed positive?
			if (canTransit(shift, ov, nv, 0, maxSpeed, maxAcc)) return Math.abs(shift);
			// minimize rotation at positive speeds
			double t1 = ov / maxAcc;
			double t2 = nv / maxAcc;
			assert shift < 0;
			return 2 * (path(t1, 0, maxAcc) + path(t2, 0, maxAcc)) - shift;
		} else {
			double t1 = ov / maxAcc;
			double t2 = -nv / maxAcc;
			double pos = path(t1, 0, maxAcc);
			double neg = path(t2, 0, maxAcc);
			double midShift = pos - neg;
			if (shift < midShift) {
				// minimize rotation at positive speeds
				neg = pos - shift;
			} else {
				// minimize rotation at negative speeds
				pos = neg + shift;
			}
			return pos + neg;
		}
	}

	static boolean canTransit(double shift, double ov, double nv, double minSpeed, double maxSpeed, double maxAcc) {
		if (Math.abs((nv - ov)) > maxAcc * 60.0 + EPS) return false;

		// find minimum possible angular shift
		double minShift = 0.0;
		double t1 = (ov - minSpeed) / maxAcc;
		double t2 = 60.0 - (nv - minSpeed) / maxAcc;
		if (t1 <= t2) {
			minShift += path(t1, ov, -maxAcc);
			minShift += path(t2 - t1, minSpeed, 0);
			minShift += path(60.0 - t2, minSpeed, maxAcc);
		} else {
			double t = (ov - nv + 60.0 * maxAcc) / 2.0 / maxAcc;
			minShift += path(t, ov, -maxAcc);
			minShift += path(60.0 - t, ov - maxAcc * t, maxAcc);
		}
		if (shift < minShift) return false;

		// find maximum possible angular shift
		double maxShift = 0.0;
		t1 = (maxSpeed - ov) / maxAcc;
		t2 = 60.0 - (maxSpeed - nv) / maxAcc;
		if (t1 <= t2) {
			maxShift += path(t1, ov, maxAcc);
			maxShift += path(t2 - t1, maxSpeed, 0);
			maxShift += path(60.0 - t2, maxSpeed, -maxAcc);
		} else {
			double t = (nv - ov + 60.0 * maxAcc) / 2.0 / maxAcc;
			maxShift += path(t, ov, maxAcc);
			maxShift += path(60.0 - t, ov + maxAcc * t, -maxAcc);
		}
		return shift <= maxShift + EPS;
	}

	static double path(double t, double v0, double a) {
		return v0 * t + a * t * t / 2.0;
	}

}

class Result {
	double[] move = new double[8]; // move distance of angle
	double[] power = new double[8]; // watt of new state
	boolean[] danger = new boolean[8];
}

class Scorer {
	Result[] results;
	double[] totalPower = new double[8];
	double[] totalMove = new double[8];
	int M = 0;
	int[] maxDanger = new int[8];

	Scorer(Result[] res) {
		this.results = res;
	}

	boolean checkLongeron() {
		Arrays.fill(maxDanger, 0);
		for (int i = 0; i < 8; ++i) {
			int counter = 0;
			for (int j = 0; j < 30; ++j) {
				for (int k = 0; k < ISSModel.NUM_STATES; ++k) {
					if (results[k].danger[i]) {
						++counter;
						maxDanger[i] = Math.max(maxDanger[i], counter);
						if (counter > 20) {
							System.out.println("Longeron " + i + " exceeds max danger counter.");
							return false;
						}
					} else {
						counter = Math.max(0, counter - 1);
					}
				}
			}
		}
		return true;
	}

	double score() {
		if (!checkLongeron()) {
			return 0;
		}
		Arrays.fill(totalPower, 0);
		Arrays.fill(totalMove, 0);
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			if (results[i] == null) return 0;
			for (int j = 0; j < 8; ++j) {
				totalPower[j] += results[i].power[j];
				totalMove[j] += results[i].move[j];
			}
		}
		M = 0;
		for (int i = 0; i < 8; ++i) {
			totalPower[i] /= ISSModel.NUM_STATES;
			if (totalPower[i] < Tester.MIN_POWER_REQ[i]) {
				++M;
			}
		}

		double maxMove = 0;
		double score = 0;
		for (int i = 0; i < 8; ++i) {
			maxMove = Math.max(maxMove, totalMove[i]);
			score += totalPower[i];
		}
		score *= Math.min(1.0, Math.pow(2.0, (80.0 - maxMove) / 300.0));
		score /= 1 << M;
		return score;
	}

	void log() {
		double sumPower = 0;
		System.out.print("power:[");
		for (int id = 0; id < 8; id++) {
			System.out.printf("%.3f, ", totalPower[id]);
			sumPower += totalPower[id];
		}
		System.out.println("]");
		System.out.println("totalPower:" + sumPower);
		System.out.println("underpowered:" + M);
		double maxMove = 0;
		System.out.print("rotation:[");
		for (int id = 0; id < 8; ++id) {
			System.out.printf("%.3f, ", totalMove[id]);
			maxMove = Math.max(maxMove, totalMove[id]);
		}
		System.out.println("]");
		System.out.println("moveFactor:" + Math.min(1.0, Math.pow(2.0, (80.0 - maxMove) / 300.0)));
		System.out.println("danger:" + Arrays.toString(maxDanger));
	}

}

class State {
	double[] angle = new double[10];
	double[] velocity = new double[10];
}

public class Tester {

	static final double[] MIN_POWER_REQ = new double[] { 6300.0, 6400.0, 6700.0, 8900.0, 5500.0, 8100.0, 4400.0, 4300.0 };
	static final boolean DEBUG = 0 == 0;
	static Timer timer = new Timer();
	static int[] counter = new int[30];
	State[] states = new State[ISSModel.NUM_STATES];
	int[] danger = new int[8];
	Random rnd = new Random(43);

	void optimizeFirst(double beta) {
		Solver solver = new Solver(beta, yaw);
		State state = new State();
		double[] base = new double[11];
		if (beta < 0) {
			base[2] = base[4] = 180;
			base[6] = base[8] = 180;
		} else {
			base[3] = base[5] = 180;
			base[7] = base[9] = 180;
		}
		double bestScore = 0;
		Random rand = new Random();
		double diff = 100;
		OUT: for (int turn = 1; turn <= 2000; ++turn) {
			if (turn % 20 == 0) {
				diff *= 0.95;
			}
			for (int i = 2; i < 10; ++i) {
				state.angle[i] = base[i] + rand.nextDouble() * diff * 2 - diff;
			}
			for (int i = 2; i < 10; ++i) {
				if (state.angle[i] < 0) state.angle[i] += 360;
				if (state.angle[i] >= 360) state.angle[i] -= 360;
			}
			solver.model.yaw = base[10] + rand.nextGaussian() * 2;
			if (solver.model.yaw < -7) solver.model.yaw = -7;
			if (solver.model.yaw > 7) solver.model.yaw = 7;

			Result result = solver.calc(0, state);
			if (result == null) {
				continue OUT;
			}
			double sum = 0;
			for (int i = 0; i < 8; ++i) {
				sum += result.power[i];
				if (result.danger[i]) {
					continue OUT;
				}
			}

			if (sum > bestScore) {
				bestScore = sum;
				for (int i = 0; i < 10; ++i) {
					base[i] = state.angle[i];
				}
				base[10] = solver.model.yaw;
				//				System.out.println(Arrays.toString(base));
				//				System.out.println(sum);
				//				System.out.println();
			}
		}
		System.out.println("beta:" + beta);
		System.out.println(Arrays.toString(base));
		System.out.println(bestScore);
	}

	boolean update(double cur, double prev) {
		if (cur >= prev) return true;
		if (cur < prev - 10) return false;
		return rnd.nextDouble() < Math.exp((cur - prev) * 3);
//		return false;
	}

	static double[] startp72 = new double[] { 0.0, 0.0, 17.722451371195284, 161.32036137372677, 342.3981465804844,
			198.07069055476904, 341.54318421296153, 197.53829786950854, 17.978598091558702, 161.35878134061466, };
	static double[] startp74 = new double[] { 0.0, 0.0, 359.60004177892364, 162.0127299219855, 321.4152707687345,
			199.6743667469768, 340.7397800141104, 218.01207751125207, 22.736766077580562, 181.90140080694636, };
	static double[] startm70 = new double[] { 0.0, 0.0, 159.3596006869924, 339.93070154487395, 192.6294982558921,
			334.6552068183941, 202.2940981517584, 338.8756130494141, 160.35197242826095, 22.498348014243412, };
	static double[] startm74 = new double[] { 0.0, 0.0, 161.57688553463439, 358.02308504090956, 203.00112385377764,
			0.8667031707005273, 179.4347993630836, 338.68692688135815, 180.79655288889555, 16.511980642861204, };

	void optimizeOne(double beta, double yaw, int time) {
		System.out.println("time:" + time);
		ISSModel model = new ISSModel(beta, yaw);
		model.setAlpha(time);
		State st = states[time] = new State();
		double alpha = time * 360.0 / 92;
		st.angle[0] = alpha;
		st.angle[1] = alpha == 0 ? 0 : 360.0 - alpha;
		if (time == 0) {
			//			if (beta < 0) {
			//				st.angle[2] = st.angle[4] = st.angle[6] = st.angle[8] = 180;
			//			} else {
			//				st.angle[3] = st.angle[5] = st.angle[7] = st.angle[9] = 180;
			//			}
			for (int i = 2; i < 10; ++i) {
				st.angle[i] = startp72[i];
			}
		} else {
			for (int i = 2; i < 10; ++i) {
				st.angle[i] = states[time - 1].angle[i];
			}
		}
		Result res = new Result();
		double[] bestAngle = new double[10];
		//		double[] diff = new double[10];
		double[] base = new double[10];
		for (int i = 0; i < 10; ++i) {
			base[i] = st.angle[i];
		}
		double[] power = new double[8];
		double bestScore = 0;
		double score = 0;
		boolean[] bestLongeronOK = null;
		// TODO: how to consider longeron gracefully?
		double delta = time == 0 ? 4 : 1;
		final double mul = 0.96;
		for (int turn = 1, sup = (time == 0 ? 1 << 12 : 1 << 8); turn <= sup; ++turn) {
			if (turn % (1 << 7) == 0) {
				delta *= mul;
			}
			double diffPena = 0;
			if (time != 0 || turn != 1) {
				for (int i = 2; i < 10; ++i) {
					while (true) {
						st.angle[i] = base[i] + rnd.nextGaussian() * delta;
						if (st.angle[i] < 0) st.angle[i] += 360;
						if (st.angle[i] >= 360) st.angle[i] -= 360;
						if (time != 0) {
							double diff = st.angle[i] - states[time - 1].angle[i];
							if (diff > 180) diff -= 360;
							if (diff <= -180) diff += 360;
							//						while (Math.abs(diff) > 120.0 / ISSModel.NUM_STATES) {
							//						st.angle[i] = base[i] + rnd.nextGaussian() * delta;
							//						if (st.angle[i] < 0) st.angle[i] += 360;
							//						if (st.angle[i] >= 360) st.angle[i] -= 360;
							//						diff = st.angle[i] - states[time - 1].angle[i];
							//						if (diff >= 180) diff -= 360;
							//						if (diff < -180) diff += 360;
							//					}
							while (Math.abs(diff) > 0.01 * 30 * 30) {
								st.angle[i] = base[i] + rnd.nextGaussian() * delta;
								if (st.angle[i] < 0) st.angle[i] += 360;
								if (st.angle[i] >= 360) st.angle[i] -= 360;
								diff = st.angle[i] - states[time - 1].angle[i];
								if (diff >= 180) diff -= 360;
								if (diff < -180) diff += 360;
							}
							double moveToEnd = st.angle[i] - states[0].angle[i];
							if (moveToEnd > 180) moveToEnd -= 360;
							if (moveToEnd <= -180) moveToEnd += 360;
							if (Math.abs(moveToEnd) > (ISSModel.NUM_STATES - time) * (0.01 * 30 * 30 / 12)) {
								continue;
							}
							diffPena += diff;
							if (diff > 80.0 / ISSModel.NUM_STATES) diffPena += diff;
						}
						break;
					}
					power[i - 2] = res.power[i - 2];
				}
			}

			//			if (time == ISSModel.NUM_STATES - 1 && !Solver.canTransit(st, states[0])) {
			//				--turn;
			//				continue;
			//			}
			//			if (time != 0 && !Solver.canTransit(states[time - 1], st)) {
			//				--turn;
			//				continue;
			//			}
			model.setAngle(st.angle);
			model.calculatePower(res);
			double sum = 0;
			for (int i = 0; i < 8; ++i) {
				sum += res.power[i];
			}
			boolean[] longeronOK = model.isLongeronOK();
			for (int i = 0; i < 8; ++i) {
				if (!longeronOK[i]) {
					sum -= (danger[i] + 2) * (danger[i] + 2) * 100;
				}
			}
			sum -= diffPena * 100;
			if (update(sum, score)) {
				if (sum > bestScore) {
					bestScore = sum;
					System.out.println(bestScore);
					for (int i = 2; i < 10; ++i) {
						bestAngle[i] = st.angle[i];
					}
					bestLongeronOK = longeronOK;
				}
				for (int i = 2; i < 10; ++i) {
					base[i] = st.angle[i];
				}
				score = sum;
			} else {
				for (int i = 0; i < 8; ++i) {
					res.power[i] = power[i];
				}
			}
		}
		for (int i = 2; i < 10; ++i) {
			st.angle[i] = bestAngle[i];
		}
		for (int i = 0; i < 8; ++i) {
			if (bestLongeronOK[i]) {
				if (danger[i] > 0) --danger[i];
			} else {
				++danger[i];
			}
			System.out.print(bestLongeronOK[i] ? 1 : 0);
		}
		System.out.println();
		System.out.println("best:" + bestScore);
	}

	double optimize(double beta) {
		double yaw = 0;
		Arrays.fill(danger, 10);
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			optimizeOne(beta, yaw, i);
			if (i == 0) {
				System.out.print("{");
				for (double a : states[i].angle) {
					System.out.print(a + ",");
				}
				System.out.println("},");
			}
		}
		Solver solver = new Solver(beta, yaw);
		Result[] results = new Result[ISSModel.NUM_STATES];
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			results[i] = solver.calc(i, states[i == 0 ? ISSModel.NUM_STATES - 1 : i - 1], states[i]);
			if (results[i] == null) {
				System.out.println("turn " + i + ":cannot transit");
				return -1;
			}
		}
		Scorer scorer = new Scorer(results);
		double score = scorer.score();
		System.out.println("beta:" + beta);
		System.out.println("score:" + score);
		scorer.log();
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			System.out.print("{");
			for (double a : states[i].angle) {
				System.out.print(a + ",");
			}
			System.out.println("},");
		}
		return score;
	}

	boolean moveSARJ(int time, int id) {
		return true;
	}

	static double[] sarj_p72 = new double[] { 0.0, 3.9130434782608696, 7.826086956521739, 11.73913043478261,
			15.652173913043478, 19.565217391304348, 23.47826086956522, 27.391304347826086, 31.304347826086957,
			35.21739130434783, 39.130434782608695, 43.04347826086956, 46.95652173913044, 50.869565217391305,
			54.78260869565217, 58.69565217391305, 62.608695652173914, 66.52173913043478, 70.43478260869566,
			74.34782608695652, 78.26086956521739, 82.17391304347827, 86.08695652173913, 90.0, 93.91304347826087,
			97.82608695652173, 101.73913043478261, 105.65217391304348, 109.56521739130434, 113.47826086956522,
			117.3913043478261, 121.30434782608695, 125.21739130434783, 129.1304347826087, 133.04347826086956,
			136.95652173913044, 140.8695652173913, 144.7826086956522, 148.69565217391303, 152.6086956521739,
			156.52173913043478, 160.43478260869566, 164.34782608695653, 168.2608695652174, 172.17391304347825,
			176.08695652173913, 180.0, 183.91304347826087, 187.82608695652175, 191.7391304347826, 195.65217391304347,
			199.56521739130434, 203.47826086956522, 207.3913043478261, 211.30434782608697, 215.2173913043478,
			219.1304347826087, 223.04347826086956, 226.95652173913044, 230.8695652173913, 234.7826086956522,
			238.69565217391303, 242.6086956521739, 246.52173913043478, 250.43478260869566, 254.34782608695653,
			258.2608695652174, 262.17391304347825, 266.0869565217391, 270.0, 273.9130434782609, 277.82608695652175,
			281.7391304347826, 285.6521739130435, 289.5652173913044, 293.4782608695652, 297.39130434782606,
			301.30434782608694, 305.2173913043478, 309.1304347826087, 313.04347826086956, 316.95652173913044,
			320.8695652173913, 324.7826086956522, 328.69565217391306, 332.60869565217394, 336.5217391304348,
			340.4347826086956, 344.3478260869565, 348.2608695652174, 352.17391304347825, 356.0869565217391, };
	static double[] sarj_p74 = new double[] { 0.0, 3.9130434782608696, 7.826086956521739, 11.73913043478261,
			15.652173913043478, 19.565217391304348, 23.47826086956522, 27.391304347826086, 31.304347826086957,
			35.21739130434783, 39.130434782608695, 43.04347826086956, 46.95652173913044, 50.869565217391305,
			54.78260869565217, 58.69565217391305, 62.608695652173914, 66.52173913043478, 70.43478260869566,
			74.34782608695652, 78.26086956521739, 82.17391304347827, 86.08695652173913, 90.0, 93.91304347826087,
			97.82608695652173, 101.73913043478261, 105.65217391304348, 109.56521739130434, 113.47826086956522,
			117.3913043478261, 121.30434782608695, 125.21739130434783, 129.1304347826087, 133.04347826086956,
			136.95652173913044, 140.8695652173913, 144.7826086956522, 148.69565217391303, 152.6086956521739,
			156.52173913043478, 160.43478260869566, 164.34782608695653, 168.2608695652174, 172.17391304347825,
			176.08695652173913, 180.0, 183.91304347826087, 187.82608695652175, 191.7391304347826, 195.65217391304347,
			199.56521739130434, 203.47826086956522, 207.3913043478261, 211.30434782608697, 215.2173913043478,
			219.1304347826087, 223.04347826086956, 226.95652173913044, 230.8695652173913, 234.7826086956522,
			238.69565217391303, 242.6086956521739, 246.52173913043478, 250.43478260869566, 254.34782608695653,
			258.2608695652174, 262.17391304347825, 266.0869565217391, 270.0, 273.9130434782609, 277.82608695652175,
			281.7391304347826, 285.6521739130435, 289.5652173913044, 293.4782608695652, 297.39130434782606,
			301.30434782608694, 305.2173913043478, 309.1304347826087, 313.04347826086956, 316.95652173913044,
			320.8695652173913, 324.7826086956522, 328.69565217391306, 332.60869565217394, 336.5217391304348,
			340.4347826086956, 344.3478260869565, 348.2608695652174, 352.17391304347825, 356.0869565217391, };
	static double[] sarj_m70 = new double[] { 0.0, 3.9130434782608696, 7.826086956521739, 11.73913043478261,
			15.652173913043478, 19.565217391304348, 23.47826086956522, 27.391304347826086, 31.304347826086957,
			35.21739130434783, 39.130434782608695, 43.04347826086956, 46.95652173913044, 50.869565217391305,
			54.78260869565217, 58.69565217391305, 62.608695652173914, 66.52173913043478, 70.43478260869566,
			74.34782608695652, 78.26086956521739, 82.17391304347827, 86.08695652173913, 90.0, 93.91304347826087,
			97.82608695652173, 101.73913043478261, 105.65217391304348, 109.56521739130434, 113.47826086956522,
			117.3913043478261, 121.30434782608695, 125.21739130434783, 129.1304347826087, 133.04347826086956,
			136.95652173913044, 140.8695652173913, 144.7826086956522, 148.69565217391303, 152.6086956521739,
			156.52173913043478, 160.43478260869566, 164.34782608695653, 168.2608695652174, 172.17391304347825,
			176.08695652173913, 180.0, 183.91304347826087, 187.82608695652175, 191.7391304347826, 195.65217391304347,
			199.56521739130434, 203.47826086956522, 207.3913043478261, 211.30434782608697, 215.2173913043478,
			219.1304347826087, 223.04347826086956, 226.95652173913044, 230.8695652173913, 234.7826086956522,
			238.69565217391303, 242.6086956521739, 246.52173913043478, 250.43478260869566, 254.34782608695653,
			258.2608695652174, 262.17391304347825, 266.0869565217391, 270.0, 273.9130434782609, 277.82608695652175,
			281.7391304347826, 285.6521739130435, 289.5652173913044, 293.4782608695652, 297.39130434782606,
			301.30434782608694, 305.2173913043478, 309.1304347826087, 313.04347826086956, 316.95652173913044,
			320.8695652173913, 324.7826086956522, 328.69565217391306, 332.60869565217394, 336.5217391304348,
			340.4347826086956, 344.3478260869565, 348.2608695652174, 352.17391304347825, 356.0869565217391, };
	static double[] sarj_m74 = new double[] { 0.0, 3.9130434782608696, 7.826086956521739, 11.73913043478261,
			15.652173913043478, 19.565217391304348, 23.47826086956522, 27.391304347826086, 31.304347826086957,
			35.21739130434783, 39.130434782608695, 43.04347826086956, 46.95652173913044, 50.869565217391305,
			54.78260869565217, 58.69565217391305, 62.608695652173914, 66.52173913043478, 70.43478260869566,
			74.34782608695652, 78.26086956521739, 82.17391304347827, 86.08695652173913, 90.0, 93.91304347826087,
			97.82608695652173, 101.73913043478261, 105.65217391304348, 109.56521739130434, 113.47826086956522,
			117.3913043478261, 121.30434782608695, 125.21739130434783, 129.1304347826087, 133.04347826086956,
			136.95652173913044, 140.8695652173913, 144.7826086956522, 148.69565217391303, 152.6086956521739,
			156.52173913043478, 160.43478260869566, 164.34782608695653, 168.2608695652174, 172.17391304347825,
			176.08695652173913, 180.0, 183.91304347826087, 187.82608695652175, 191.7391304347826, 195.65217391304347,
			199.56521739130434, 203.47826086956522, 207.3913043478261, 211.30434782608697, 215.2173913043478,
			219.1304347826087, 223.04347826086956, 226.95652173913044, 230.8695652173913, 234.7826086956522,
			238.69565217391303, 242.6086956521739, 246.52173913043478, 250.43478260869566, 254.34782608695653,
			258.2608695652174, 262.17391304347825, 266.0869565217391, 270.0, 273.9130434782609, 277.82608695652175,
			281.7391304347826, 285.6521739130435, 289.5652173913044, 293.4782608695652, 297.39130434782606,
			301.30434782608694, 305.2173913043478, 309.1304347826087, 313.04347826086956, 316.95652173913044,
			320.8695652173913, 324.7826086956522, 328.69565217391306, 332.60869565217394, 336.5217391304348,
			340.4347826086956, 344.3478260869565, 348.2608695652174, 352.17391304347825, 356.0869565217391, };

	static class Path {
		double[][] bga = new double[93][8];
		int[] ang = new int[93];
		int[] vel = new int[93];
		int[] danger = new int[8];
		double[] move = new double[8];
		double power;
		double score;

		public Path clone() {
			Path ret = new Path();
			for (int i = 0; i < 93; ++i) {
				ret.bga[i] = this.bga[i].clone();
				ret.ang[i] = this.ang[i];
				ret.vel[i] = this.vel[i];
			}
			ret.danger = this.danger.clone();
			ret.move = this.move.clone();
			ret.power = this.power;
			ret.score = this.score;
			return ret;
		}
	}

	Result improveAll_optimizeBGA(int time, Solver solver, double[][] baseA) {
		double[] best = new double[8];
		double bestScore = 0;
		Result res = solver.calc(time, states[time]);
		for (int i = 0; i < 8; ++i) {
			bestScore += res.power[i];
		}
		int ntime = time == ISSModel.NUM_STATES - 1 ? 0 : time + 1;
		int ptime = time == 0 ? ISSModel.NUM_STATES - 1 : time - 1;
		for (int turn = 0; turn < 1 << 4; ++turn) {
			for (int i = 2; i < 10; ++i) {
				while (true) {
					double next = baseA[time][i] + rnd.nextGaussian() * 0.05;
					if (next >= 360) next -= 360;
					if (next < 0) next += 360;
					states[time].angle[i] = next;
					double moveP = states[time].angle[i] - states[ptime].angle[i];
					while (moveP >= 180)
						moveP -= 360;
					while (moveP < -180)
						moveP += 360;
					if (Math.abs(moveP) > 0.01 * 25 * 25 + 0.25 * 10) continue;
					double moveN = states[time].angle[i] - states[ntime].angle[i];
					while (moveN >= 180)
						moveN -= 360;
					while (moveN < -180)
						moveN += 360;
					if (Math.abs(moveN) > 0.01 * 25 * 25 + 0.25 * 10) continue;
					break;
				}
			}
			double newScore = 0;
			Result newRes = solver.calc(time, states[time]);
			for (int i = 0; i < 8; ++i) {
				newScore += newRes.power[i];
			}
			if (newScore > bestScore) {
				bestScore = newScore;
				res = newRes;
				for (int i = 0; i < 8; ++i) {
					best[i] = states[time].angle[i + 2];
				}
			}
		}
		for (int i = 0; i < 8; ++i) {
			states[time].angle[i + 2] = best[i];
		}
		return res;
	}

	double[] determineVelocity(State prev, State cur, int moveSarj, boolean before) {
		double[] ret = new double[2];
		boolean minOK = Solver.transit(prev.angle[moveSarj], before ? prev.velocity[moveSarj] : 0, cur.angle[moveSarj],
				before ? 0 : cur.velocity[moveSarj], 0.15, 0.005) >= 0;
		boolean maxOK = Solver.transit(prev.angle[moveSarj], before ? prev.velocity[moveSarj] : 0.15, cur.angle[moveSarj],
				before ? 0.15 : cur.velocity[moveSarj], 0.15, 0.005) >= 0;
		if (minOK && maxOK) {
			ret[0] = 0;
			ret[1] = 0.15;
			return ret;
		}
		if (!minOK && !maxOK) return null;
		if (minOK) {
			double lo = 0;
			double hi = 0.15;
			for (int i = 0; i < 20; ++i) {
				double mid = (lo + hi) / 2;
				if (Solver.transit(prev.angle[moveSarj], before ? prev.velocity[moveSarj] : mid, cur.angle[moveSarj],
						before ? mid : cur.velocity[moveSarj], 0.15, 0.005) < 0) {
					hi = mid;
				} else {
					lo = mid;
				}
			}
			ret[1] = lo;
			return ret;
		}
		{
			double lo = 0;
			double hi = 0.15;
			for (int i = 0; i < 20; ++i) {
				double mid = (lo + hi) / 2;
				if (Solver.transit(prev.angle[moveSarj], before ? prev.velocity[moveSarj] : mid, cur.angle[moveSarj],
						before ? mid : cur.velocity[moveSarj], 0.15, 0.005) < 0) {
					lo = mid;
				} else {
					hi = mid;
				}
			}
			ret[0] = hi;
			ret[1] = 0.15;
			return ret;
		}
	}

	double determineVelocity(int time, int moveSarj) {
		State nst = states[time == ISSModel.NUM_STATES - 1 ? 0 : time + 1];
		State pst = states[time == 0 ? ISSModel.NUM_STATES - 1 : time - 1];
		State cst = states[time];
		//		System.out.println(pst.angle[moveSarj] + " " + pst.velocity[moveSarj] + " " + cst.angle[moveSarj] + " "
		//				+ cst.velocity[moveSarj] + " " + nst.angle[moveSarj] + nst.velocity[moveSarj]);
		double[] v1 = determineVelocity(pst, cst, moveSarj, true);
		double[] v2 = determineVelocity(cst, nst, moveSarj, false);
		//		System.out.println(Arrays.toString(v1) + " " + Arrays.toString(v2));
		if (v1 == null || v2 == null) return -1;
		if (v1[1] < v2[0] || v1[0] > v2[1]) return -1;
		double min = Math.max(v1[0], v2[0]);
		double max = Math.min(v1[1], v2[1]);
		return (min + max) / 2;
	}

	void improveAll(double beta) {
		double[][] angle = ISS.selectAngle(beta);
		double[][] vel = ISS.selectVelocity(beta);
		Solver solver = new Solver(beta, yaw);
		//		solver.model.renient = false;
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			for (int j = 0; j < 10; ++j) {
				states[i].angle[j] = angle[i][j];
			}
			states[i].velocity[0] = vel[0][i];
			states[i].velocity[1] = vel[1][i];
		}

//		double[] v = selectBGA(beta);
//		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
//			for (int j = 2; j < 10; ++j) {
//				states[i].angle[j] = v[j];
//			}
//			states[i].angle[0] = i * 360.0 / 92;
//			states[i].angle[1] = i == 0 ? 0 : 360 - i * 360.0 / 92;
//			states[i].velocity[0] = 360.0 / 92 / 60;
//			states[i].velocity[1] = -360.0 / 92 / 60;
//		}

		Result[] results = solver.calcAll(states);
		double[][] baseA = new double[ISSModel.NUM_STATES][10];
		double[][] baseV = new double[ISSModel.NUM_STATES][2];
		double[][] bestA = new double[ISSModel.NUM_STATES][10];
		double[][] bestV = new double[ISSModel.NUM_STATES][2];
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			for (int j = 0; j < 10; ++j) {
				baseA[i][j] = bestA[i][j] = angle[i][j];
			}
			for (int j = 0; j < 2; ++j) {
				baseV[i][j] = bestV[i][j] = vel[j][i];
			}
		}
		Scorer scorer = new Scorer(results);
		double score = scorer.score();
		for (int i = 0; i < 8; ++i) {
			int dangerPena = Math.max(0, scorer.maxDanger[i] - 19);
			score -= dangerPena * dangerPena;
		}
		double bestScore = score;
		double delta = 2;
//				int moveSarj = beta < 0 ? 1 : 0;
		double[] nextResMove = new double[8];
		System.out.println("improveAll init:" + score);
		for (int turn = 1, maxTurn = 1 << 13; turn <= maxTurn; ++turn) {
			if (turn % (1 << 8) == 0) {
				delta *= 0.93;
				System.out.println(turn + " " + delta);
				if (turn * 2 == maxTurn) {
					solver.model.renient = true;
				}
			}
			int moveSarj = rnd.nextInt(2);
			int time = turn % ISSModel.NUM_STATES;
			int ntime = time == ISSModel.NUM_STATES - 1 ? 0 : time + 1;
			int ptime = time == 0 ? ISSModel.NUM_STATES - 1 : time - 1;
			double minMove = baseA[ptime][moveSarj] - baseA[time][moveSarj];
			if (minMove > 0) minMove -= 360;
			double maxMove = baseA[ntime][moveSarj] - baseA[time][moveSarj];
			if (maxMove < 0) maxMove += 360;
			double move = rnd.nextGaussian() * delta;
			if (move < minMove) move = minMove;
			if (move > maxMove) move = maxMove;
			states[time].angle[moveSarj] = baseA[time][moveSarj] + move;
			double newVel = determineVelocity(time, moveSarj);
			if (newVel < 0) {
				states[time].angle[moveSarj] = baseA[time][moveSarj];
				//				continue;
			} else {
				states[time].velocity[moveSarj] = newVel;
			}

			//			if (!Solver.canTransit(states[ptime], states[time])) {
			//				System.out.println(1);
			//				System.out.println(states[ptime].angle[moveSarj] + " " + states[ptime].velocity[moveSarj] + " "
			//						+ states[time].angle[moveSarj] + " " + states[time].velocity[moveSarj]);
			//				System.exit(0);
			//			}
			//			if (!Solver.canTransit(states[time], states[ntime])) {
			//				System.out.println(2);
			//				System.out.println(states[time].angle[moveSarj] + " " + states[time].velocity[moveSarj] + " "
			//						+ states[ntime].angle[moveSarj] + " " + states[ntime].velocity[moveSarj]);
			//				System.out.println(move);
			//				System.exit(0);
			//			}
			Result curRes = results[time];
			results[time] = improveAll_optimizeBGA(time, solver, baseA);
			for (int i = 0; i < 8; ++i) {
				nextResMove[i] = results[ntime].move[i];
			}
			if (!solver.calcBGAMove(results[time], states[ptime], states[time])
					|| !solver.calcBGAMove(results[ntime], states[time], states[ntime])) {
				results[time] = curRes;
				for (int i = 0; i < 8; ++i) {
					results[ntime].move[i] = nextResMove[i];
				}
				for (int i = 0; i < 10; ++i) {
					states[time].angle[i] = baseA[time][i];
				}
				states[time].velocity[moveSarj] = baseV[time][moveSarj];
				continue;
			}

			double newScore = scorer.score();
			if (newScore > 0) {
				for (int i = 0; i < 8; ++i) {
					int dangerPena = Math.max(0, scorer.maxDanger[i] - 19);
					newScore -= dangerPena * dangerPena;
				}
			}
			if (update(newScore, score)) {
				if (newScore > bestScore) {
					System.out.println(newScore);
					bestScore = newScore;
					for (int i = 0; i < 10; ++i) {
						bestA[time][i] = states[time].angle[i];
					}
					bestV[time][moveSarj] = states[time].velocity[moveSarj];
				}
				score = newScore;
				for (int i = 0; i < 10; ++i) {
					baseA[time][i] = states[time].angle[i];
				}
				baseV[time][moveSarj] = states[time].velocity[moveSarj];
			} else {
				results[time] = curRes;
				for (int i = 0; i < 8; ++i) {
					results[ntime].move[i] = nextResMove[i];
				}
				for (int i = 0; i < 10; ++i) {
					states[time].angle[i] = baseA[time][i];
				}
				states[time].velocity[moveSarj] = baseV[time][moveSarj];
			}
		}

		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print("{");
			for (int k = 0; k < 10; ++k) {
				System.out.print(bestA[j][k] + ",");
			}
			System.out.println("},");
		}
		System.out.println("}");
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print(bestV[j][0] + ",");
		}
		System.out.println("},");
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print(bestV[j][1] + ",");
		}
		System.out.println("},");
		scorer.results = solver.calcAll(states);
		System.out.println("score:" + scorer.score());
		scorer.log();
	}

	void improveSARJ4(double beta) {
		double[] bgaA = selectBGA(beta);
		improveSARJ4(beta, bgaA);
	}

	void improveSARJ4(double beta, double[] initial) {
		Solver solver = new Solver(beta, 0);
		solver.model.renient = false;
		final int timespan = 30;
		final double oneAngle = 0.005 * timespan * timespan / 2 / 2;
		final int distUnits = (int) Math.ceil(360 / oneAngle);
		final int speedUnits = 3;
		final int initialSpeed = 1;
		Path[][][] dp = new Path[93][speedUnits][distUnits + 1];
		dp[0][initialSpeed][0] = new Path();
		for (int i = 2; i < 10; ++i) {
			dp[0][initialSpeed][0].bga[0][i - 2] = initial[i];
		}
		for (int i = 0; i < 92; ++i) {
			int minDistUnit = Math.max(0, distUnits - (92 - i) * (speedUnits - 1) * 4);
			minDistUnit = Math.max(minDistUnit, (int) (Math.ceil((i * 360.0 / 92 - 40) / oneAngle)));
			int maxDistUnit = Math.min(distUnits, (int) (Math.floor((i * 360.0 / 92 + 40) / oneAngle)));
			for (int j = 0; j < speedUnits; ++j) {
				for (int k = minDistUnit; k <= maxDistUnit; ++k) {
					if (dp[i][j][k] == null) continue;
					for (int l = 0; l < speedUnits; ++l) {
						for (int m = 0; m < 4 && k + j + l + m <= distUnits; ++m) {
							Path next = dp[i + 1][l][k + j + l + m];
							if (next == null || next.score < dp[i][j][k].score) {
								dp[i + 1][l][k + j + l + m] = dp[i][j][k];
							}
						}
					}
				}
			}

			// adjust bga angles
			for (int j = 0; j < speedUnits; ++j) {
				for (int k = minDistUnit; k <= distUnits; ++k) {
					Path path = dp[i + 1][j][k];
					if (path == null) continue;
					path = path.clone();
					dp[i + 1][j][k] = path;

					System.out.println((i + 1) + " " + j + " " + k);
					State st = new State();
					//					st.angle[0] = k * oneAngle;
					//					st.angle[1] = 360 - st.angle[0];
					if (beta < 0) {
						st.angle[0] = (i + 1) * 360.0 / 92;
						st.angle[1] = 360 - k * oneAngle;
					} else {
						st.angle[0] = k * oneAngle;
						st.angle[1] = 360 - (i + 1) * 360.0 / 92;
					}
					double[] best = new double[8];
					for (int l = 0; l < 8; ++l) {
						best[l] = st.angle[l + 2] = path.bga[i][l];
					}
					double delta = 0.4;
					double bestScore = 0;
					double bestPower = 0;
					boolean[] bestDanger = new boolean[8];
					for (int l = 1; l <= 1 << 4; ++l) {
						if (l % (1 << 5) == 0) {
							delta *= 0.8;
						}
						for (int m = 0; m < 8; ++m) {
							st.angle[m + 2] = best[m] + rnd.nextGaussian() * delta;
						}
						Result res = solver.calc(i + 1, st);
						double score = 0;
						double power = 0;
						for (int m = 0; m < 8; ++m) {
							power += res.power[m];
							score += res.power[m];
							if (res.danger[m]) {
								score -= (path.danger[m] + 1) * (path.danger[m] + 1) * 100;
							}
							double move = Math.abs(st.angle[m + 2] - path.bga[i][m]);
							score -= move * 20;
							if (move > (80 - path.move[m]) / (92 - i)) {
								score -= (move - (80 - path.move[m]) / (92 - i)) * 200;
							}
							if (move > 8) {
								score -= move * 1000;
							}
							if (i > 70) {
								double moveFirst = Math.abs(st.angle[m + 2] - path.bga[1][m]);
								if (moveFirst > 180) moveFirst = 360 - moveFirst;
								score -= moveFirst;
								if (moveFirst > (92 - i) * 1) {
									score -= (moveFirst - (92 - i) * 1) * 100;
								}
								if (moveFirst > (92 - i) * 2) {
									score -= (moveFirst - (92 - i) * 2) * 500;
								}
							}
						}
						if (score > bestScore) {
							bestPower = power;
							bestScore = score;
							for (int m = 0; m < 8; ++m) {
								best[m] = st.angle[m + 2];
								bestDanger[m] = res.danger[m];
							}
						}
					}
					System.out.println(bestScore + " " + bestPower);
					if (bestScore == 0) {
						dp[i + 1][j][k] = null;
					} else {
						path.ang[i + 1] = k;
						path.vel[i + 1] = j;
						path.score += bestScore;
						path.power += bestPower;
						for (int l = 0; l < 8; ++l) {
							path.bga[i + 1][l] = best[l];
							if (bestDanger[l]) path.danger[l]++;
							path.move[l] += Math.abs(best[l] - path.bga[i][l]);
						}
					}
				}
			}

			for (int j = 0; j <= distUnits; ++j) {
				double maxPower = 0;
				for (int k = 0; k < speedUnits; ++k) {
					if (dp[i + 1][k][j] != null) {
						maxPower = Math.max(maxPower, dp[i + 1][k][j].power);
					}
				}
				if (maxPower != 0) {
					System.out.println((i + 1) + " " + j + " " + maxPower);
				}
			}

			// pruning
			double maxScore = 0;
			int maxDist = 0;
			for (int j = 0; j <= distUnits; ++j) {
				for (int k = 0; k < speedUnits; ++k) {
					if (dp[i + 1][k][j] != null && dp[i + 1][k][j].score > maxScore) {
						maxScore = dp[i + 1][k][j].score;
						maxDist = j;
					}
				}
			}
			for (int j = maxDist + 8; j <= distUnits; ++j) {
				for (int k = 0; k < speedUnits; ++k) {
					if (dp[i + 1][k][j] != null && dp[i + 1][k][j].score < maxScore - (92 - (i + 1)) * 600) {
						dp[i + 1][k][j] = null;
					}
				}
			}
			for (int j = 0; j < Math.min(maxDist - 8, (int) (Math.floor(((i + 1) * 360.0 / 92) / oneAngle))); ++j) {
				for (int k = 0; k < speedUnits; ++k) {
					dp[i + 1][k][j] = null;
				}
			}
			for (int j = Math.max(maxDist + 10, (int) (Math.ceil(((i + 1) * 360.0 / 92) / oneAngle))) + 1; j <= distUnits; ++j) {
				for (int k = 0; k < speedUnits; ++k) {
					dp[i + 1][k][j] = null;
				}
			}
		}

		Path bestPath = dp[92][initialSpeed][distUnits];
		State[] states = new State[92];
		for (int j = 1; j <= 92; ++j) {
			State st = new State();
			//			st.angle[0] = bestPath.ang[j] * oneAngle;
			//			st.angle[1] = 360 - st.angle[0];
			if (beta < 0) {
				st.angle[0] = j == 92 ? 0 : j * 360.0 / 92;
				st.angle[1] = 360 - bestPath.ang[j] * oneAngle;
			} else {
				st.angle[0] = bestPath.ang[j] * oneAngle;
				st.angle[1] = j == 92 ? 0 : 360 - j * 360.0 / 92;
			}
			for (int k = 0; k < 8; ++k) {
				st.angle[k + 2] = bestPath.bga[j][k];
			}
			for (int k = 0; k < 10; ++k) {
				if (st.angle[k] >= 360) st.angle[k] -= 360;
				if (st.angle[k] < 0) st.angle[k] += 360;
			}
			//			st.velocity[0] = bestPath.vel[j] * 0.075;
			//			st.velocity[1] = -bestPath.vel[j] * 0.075;
			if (beta < 0) {
				st.velocity[0] = 360.0 / 92 / 60;
				st.velocity[1] = -bestPath.vel[j] * 0.075;
			} else {
				st.velocity[0] = bestPath.vel[j] * 0.075;
				st.velocity[1] = -360.0 / 92 / 60;
			}
			states[j % 92] = st;
		}
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print("{");
			for (int k = 0; k < 10; ++k) {
				System.out.print(states[j].angle[k] + ",");
			}
			System.out.println("},");
		}
		System.out.println("}");
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print(states[j].velocity[0] + ",");
		}
		System.out.println("},");
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print(states[j].velocity[1] + ",");
		}
		System.out.println("},");
		solver.model.renient = true;
		Result[] results = solver.calcAll(states);
		Scorer scorer = new Scorer(results);
		System.out.println("score:" + scorer.score());
		scorer.log();
	}

	void improveSARJ3(double beta) {
		double[] bgaA = selectBGA(beta);
		improveSARJ3(beta, bgaA);
	}

	static int[][] moveMin = { { 0, 2, 8 }, { 2, 4, 10 }, { 8, 10, 16 } };
	static int[][] moveMax = { { 16, 22, 24 }, { 22, 28, 30 }, { 24, 30, 32 } };

	void improveSARJ3(double beta, double[] initial) {
		Solver solver = new Solver(beta, yaw);
		solver.model.renient = false;
		final int timespan = 30;
		final double oneAngle = 0.005 * timespan * timespan / 2 / 8;
		final int distUnits = (int) Math.ceil(360 / oneAngle);
		final int speedUnits = 3;
		final int initialSpeed = 1;
		Path[][][] dp = new Path[93][speedUnits][distUnits + 1];
		dp[0][initialSpeed][0] = new Path();
		for (int i = 2; i < 10; ++i) {
			dp[0][initialSpeed][0].bga[0][i - 2] = initial[i];
		}
		for (int i = 0; i < 92; ++i) {
			int minDistUnit = Math.max(0, distUnits - (92 - i) * 32);
			minDistUnit = Math.max(minDistUnit, (int) (Math.ceil((i * 360.0 / 92 - 40) / oneAngle)));
			int maxDistUnit = Math.min(distUnits, (int) (Math.floor((i * 360.0 / 92 + 40) / oneAngle)));
			for (int j = 0; j < speedUnits; ++j) {
				for (int k = minDistUnit; k <= maxDistUnit; ++k) {
					if (dp[i][j][k] == null) continue;
					for (int l = 0; l < speedUnits; ++l) {
						for (int m = moveMin[j][l]; m < moveMax[j][l] && k + m <= distUnits; ++m) {
							Path next = dp[i + 1][l][k + m];
							if (next == null || next.score < dp[i][j][k].score) {
								dp[i + 1][l][k + m] = dp[i][j][k];
							}
						}
					}
				}
			}

			// adjust bga angles
			for (int j = 0; j < speedUnits; ++j) {
				for (int k = minDistUnit; k <= distUnits; ++k) {
					Path path = dp[i + 1][j][k];
					if (path == null) continue;
					path = path.clone();
					dp[i + 1][j][k] = path;

					System.out.println((i + 1) + " " + j + " " + k);
					State st = new State();
					//					st.angle[0] = k * oneAngle;
					//					st.angle[1] = 360 - st.angle[0];
					if (beta < 0) {
						st.angle[0] = (i + 1) * 360.0 / 92;
						st.angle[1] = 360 - k * oneAngle;
					} else {
						st.angle[0] = k * oneAngle;
						st.angle[1] = 360 - (i + 1) * 360.0 / 92;
					}
					double[] best = new double[8];
					for (int l = 0; l < 8; ++l) {
						best[l] = st.angle[l + 2] = path.bga[i][l];
					}
					double delta = 0.4;
					double bestScore = 0;
					double bestPower = 0;
					boolean[] bestDanger = new boolean[8];
					for (int l = 1; l <= 1 << 5; ++l) {
						if (l % (1 << 4) == 0) {
							delta *= 0.9;
						}
						for (int m = 0; m < 8; ++m) {
							st.angle[m + 2] = best[m] + rnd.nextGaussian() * delta;
						}
						Result res = solver.calc(i + 1, st);
						double score = 0;
						double power = 0;
						for (int m = 0; m < 8; ++m) {
							power += res.power[m];
							score += res.power[m];
							if (res.danger[m]) {
								score -= (path.danger[m] + 1) * (path.danger[m] + 1) * 100;
							}
							double move = Math.abs(st.angle[m + 2] - path.bga[i][m]);
							score -= move * 20;
							if (move > (80 - path.move[m]) / (92 - i)) {
								score -= (move - (80 - path.move[m]) / (92 - i)) * 200;
							}
							if (move > 8) {
								score -= move * 1000;
							}
							if (i > 70) {
								double moveFirst = Math.abs(st.angle[m + 2] - path.bga[1][m]);
								if (moveFirst > 180) moveFirst = 360 - moveFirst;
								score -= moveFirst;
								if (moveFirst > (92 - i) * 1) {
									score -= (moveFirst - (92 - i) * 1) * 100;
								}
								if (moveFirst > (92 - i) * 2) {
									score -= (moveFirst - (92 - i) * 2) * 500;
								}
							}
						}
						if (score > bestScore) {
							bestPower = power;
							bestScore = score;
							for (int m = 0; m < 8; ++m) {
								best[m] = st.angle[m + 2];
								bestDanger[m] = res.danger[m];
							}
						}
					}
					System.out.println(bestScore + " " + bestPower);
					if (bestScore == 0) {
						dp[i + 1][j][k] = null;
					} else {
						path.ang[i + 1] = k;
						path.vel[i + 1] = j;
						path.score += bestScore;
						path.power += bestPower;
						for (int l = 0; l < 8; ++l) {
							path.bga[i + 1][l] = best[l];
							if (bestDanger[l]) path.danger[l]++;
							path.move[l] += Math.abs(best[l] - path.bga[i][l]);
						}
					}
				}
			}

			for (int j = 0; j <= distUnits; ++j) {
				double maxPower = 0;
				for (int k = 0; k < speedUnits; ++k) {
					if (dp[i + 1][k][j] != null) {
						maxPower = Math.max(maxPower, dp[i + 1][k][j].power);
					}
				}
				if (maxPower != 0) {
					System.out.println((i + 1) + " " + j + " " + maxPower);
				}
			}

			// pruning
			if (i + 1 != 92) {
				double maxScore = 0;
				int maxDist = 0;
				for (int j = 0; j <= distUnits; ++j) {
					for (int k = 0; k < speedUnits; ++k) {
						if (dp[i + 1][k][j] != null && dp[i + 1][k][j].score > maxScore) {
							maxScore = dp[i + 1][k][j].score;
							maxDist = j;
						}
					}
				}
				for (int j = maxDist + 8; j <= distUnits; ++j) {
					for (int k = 0; k < speedUnits; ++k) {
						if (dp[i + 1][k][j] != null
								&& (dp[i + 1][k][j].score < maxScore - (92 - (i + 1)) * 600 || dp[i + 1][k][j].score < maxScore - 10000)) {
							dp[i + 1][k][j] = null;
						}
					}
				}
				for (int j = 0; j < Math.min(maxDist - 15, (int) (Math.floor(((i + 1) * 360.0 / 92) / oneAngle))); ++j) {
					for (int k = 0; k < speedUnits; ++k) {
						dp[i + 1][k][j] = null;
					}
				}
				for (int j = Math.max(maxDist + 30, (int) (Math.ceil(((i + 1) * 360.0 / 92) / oneAngle))) + 1; j <= distUnits; ++j) {
					for (int k = 0; k < speedUnits; ++k) {
						dp[i + 1][k][j] = null;
					}
				}
			}
		}

		Path bestPath = dp[92][initialSpeed][distUnits];
		State[] states = new State[92];
		for (int j = 1; j <= 92; ++j) {
			State st = new State();
			//			st.angle[0] = bestPath.ang[j] * oneAngle;
			//			st.angle[1] = 360 - st.angle[0];
			if (beta < 0) {
				st.angle[0] = j == 92 ? 0 : j * 360.0 / 92;
				st.angle[1] = 360 - bestPath.ang[j] * oneAngle;
			} else {
				st.angle[0] = bestPath.ang[j] * oneAngle;
				st.angle[1] = j == 92 ? 0 : 360 - j * 360.0 / 92;
			}
			for (int k = 0; k < 8; ++k) {
				st.angle[k + 2] = bestPath.bga[j][k];
			}
			for (int k = 0; k < 10; ++k) {
				if (st.angle[k] >= 360) st.angle[k] -= 360;
				if (st.angle[k] < 0) st.angle[k] += 360;
			}
			//			st.velocity[0] = bestPath.vel[j] * 0.075;
			//			st.velocity[1] = -bestPath.vel[j] * 0.075;
			if (beta < 0) {
				st.velocity[0] = 360.0 / 92 / 60;
				st.velocity[1] = -bestPath.vel[j] * 0.075;
			} else {
				st.velocity[0] = bestPath.vel[j] * 0.075;
				st.velocity[1] = -360.0 / 92 / 60;
			}
			states[j % 92] = st;
		}
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print("{");
			for (int k = 0; k < 10; ++k) {
				System.out.print(states[j].angle[k] + ",");
			}
			System.out.println("},");
		}
		System.out.println("}");
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print(states[j].velocity[0] + ",");
		}
		System.out.println("},");
		System.out.print("{");
		for (int j = 0; j < 92; ++j) {
			System.out.print(states[j].velocity[1] + ",");
		}
		System.out.println("},");
		solver.model.renient = true;
		Result[] results = solver.calcAll(states);
		Scorer scorer = new Scorer(results);
		System.out.println("score:" + scorer.score());
		scorer.log();
	}

	void improveSARJ2(double beta) {
		double yaw = 0;
		double[] bgaA = null;
		if (beta == 72) {
			bgaA = bga_p72;
		} else if (beta == 74) {
			bgaA = bga_p74;
		} else if (beta == -70) {
			bgaA = bga_m70;
		} else if (beta == -74) {
			bgaA = bga_m74;
		} else {
			System.exit(1);
		}
		final double sunY = -Math.sin(beta * Math.PI / 180);
		Solver solver = new Solver(beta, yaw);
		final int timespan = 10;
		double oneAngle = 0.005 * timespan * timespan / 2;
		int distUnits = (int) (360 / oneAngle);
		double[][][] dp = new double[92 * (60 / timespan) + 1][1 + (30 / timespan)][distUnits + 1];
		int[][][] prev = new int[92 * (60 / timespan) + 1][4][distUnits + 1];
		State st = new State();
		for (int i = 2; i < 10; ++i) {
			st.angle[i] = bgaA[i];
		}
		Result res = solver.calc(0, st);
		for (int i = 0; i < dp.length; ++i) {
			for (int j = 0; j < dp[0].length; ++j) {
				for (int k = 0; k < dp[0][0].length; ++k) {
					dp[i][j][k] = -1;
				}
			}
		}
		int startVelocity = 1;
		dp[0][startVelocity][0] = 0;
		double[] scs = new double[dp[0][0].length];
		for (int i = 0; i < dp.length - 1; ++i) {
			int maxD = Math.min((60 / timespan) * i, distUnits);
			int minD = Math.max(distUnits - (dp.length - i) * (60 / timespan), 0);
			for (int j = 0; j < dp[0].length; ++j) {
				for (int k = minD; k <= maxD; ++k) {
					if (dp[i][j][k] < 0) continue;
					if (j > 0 && k + j * 2 - 1 < distUnits && dp[i][j][k] > dp[i + 1][j - 1][k + j * 2 - 1]) {
						dp[i + 1][j - 1][k + j * 2 - 1] = dp[i][j][k];
						prev[i + 1][j - 1][k + j * 2 - 1] = j;
					}
					if (k + j * 2 < distUnits && dp[i][j][k] > dp[i + 1][j][k + j * 2]) {
						dp[i + 1][j][k + j * 2] = dp[i][j][k];
						prev[i + 1][j][k + j * 2] = j;
					}
					if (j < 30 / timespan && k + j * 2 + 1 < distUnits && dp[i][j][k] > dp[i + 1][j + 1][k + j * 2 + 1]) {
						dp[i + 1][j + 1][k + j * 2 + 1] = dp[i][j][k];
						prev[i + 1][j + 1][k + j * 2 + 1] = j;
					}
				}
			}
			if (i % (60 / timespan) == (60 / timespan) - 1) {
				System.out.println((i + 1) / (60 / timespan));
				double maxScore = 0;
				for (int j = 0; j < dp[0].length; ++j) {
					for (int k = minD; k <= maxD; ++k) {
						maxScore = Math.max(dp[i + 1][j][k], maxScore);
					}
				}
				System.out.println(maxScore);
				for (int j = minD; j <= maxD; ++j) {
					boolean any = false;
					for (int k = 0; k < 4; ++k) {
						if (dp[i + 1][k][j] >= 0) {
							any = true;
							break;
						}
					}
					if (!any) continue;
					st.angle[0] = j * oneAngle;
					st.angle[1] = 360 - st.angle[0];
					double diffAngle = (i + 1) / (60 / timespan) * 360.0 / ISSModel.NUM_STATES - j * oneAngle;
					double sunZ = -Math.cos(beta * Math.PI / 180) * Math.cos(diffAngle * Math.PI / 180);
					double bgaAngle = Math.atan(sunZ / sunY) * 180 / Math.PI;
					if (bgaAngle < 0) bgaAngle += 180;
					st.angle[2] = st.angle[8] = bgaAngle;
					st.angle[3] = st.angle[9] = 180 - bgaAngle;
					st.angle[4] = st.angle[6] = 360 - bgaAngle;
					st.angle[5] = st.angle[7] = bgaAngle + 180;
					res = solver.calc((i + 1) / (60 / timespan), st);
					scs[j] = 0;
					for (int k = 0; k < 8; ++k) {
						scs[j] += res.power[k];
					}
					for (int k = 0; k < 4; ++k) {
						if (dp[i + 1][k][j] >= 0) dp[i + 1][k][j] += scs[j];
					}
				}
				for (int j = minD; j <= maxD; ++j) {
					double max = 0;
					for (int k = 0; k < 4; ++k) {
						max = Math.max(dp[i + 1][k][j], max);
					}
					System.out.println(j + " " + max);
				}
				System.out.println();
			}
		}
		int[] v = new int[92];
		int[] a = new int[92];
		int vel = startVelocity;
		int ang = distUnits;
		for (int i = 92 * (60 / timespan); i > 0; --i) {
			int prevVel = prev[i][vel][ang];
			ang -= prevVel + vel;
			vel = prevVel;
			if (i % (60 / timespan) == 1) {
				v[i / (60 / timespan)] = vel;
				a[i / (60 / timespan)] = ang;
			}
		}
		System.out.println(dp[dp.length - 1][startVelocity][distUnits]);
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(v));
		State[] states = new State[ISSModel.NUM_STATES];
		for (int i = 0; i < states.length; ++i) {
			states[i] = new State();
			states[i].angle[0] = a[i] * oneAngle;
			states[i].angle[1] = 360 - states[i].angle[0];
			if (states[i].angle[1] == 360) states[i].angle[1] = 0;
			states[i].velocity[0] = states[i].velocity[1] = v[i] * 0.025;
			states[i].velocity[1] *= -1;
			for (int j = 2; j < 10; ++j) {
				st.angle[j] = bgaA[j];
			}
		}
		Result[] results = solver.calcAll(states);
		Scorer scorer = new Scorer(results);
		System.out.println(scorer.score());
		scorer.log();
	}

	void improveSARJ(double beta) {
		double yaw = 0;
		double[] bgaA = null;
		double[] sarjA = null;
		if (beta == 72) {
			bgaA = bga_p72;
			sarjA = sarj_p72;
		} else if (beta == 74) {
			bgaA = bga_p74;
			sarjA = sarj_p74;
		} else if (beta == -70) {
			bgaA = bga_m70;
			sarjA = sarj_m70;
		} else if (beta == -74) {
			bgaA = bga_m74;
			sarjA = sarj_m74;
		} else {
			System.exit(1);
		}
		Solver solver = new Solver(beta, yaw);
		Result[] results = new Result[ISSModel.NUM_STATES];
		double[][] base = new double[2][ISSModel.NUM_STATES];
		double[][] best = new double[2][ISSModel.NUM_STATES];
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			states[i].angle[0] = sarjA[i];
			states[i].angle[1] = i == 0 ? 0 : 360 - sarjA[i];
			for (int j = 2; j < 10; ++j) {
				states[i].angle[j] = bgaA[j];
			}
			results[i] = solver.calc(i, states[i]);
			best[0][i] = base[0][i] = states[i].angle[0];
			best[1][i] = base[1][i] = states[i].angle[1];
		}
		System.out.println();
		Scorer scorer = new Scorer(results);
		double score = scorer.score();
		double bestScore = score;
		double delta = 2;
		System.out.println("improveSARJ init:" + score);
		for (int turn = 1; turn <= 1 << 10; ++turn) {
			if (turn % (1 << 8) == 0) {
				delta *= 0.9;
			}
			int time = rnd.nextInt(ISSModel.NUM_STATES);
			double move = Math.abs(rnd.nextGaussian() * delta);
			int ntime = time == ISSModel.NUM_STATES - 1 ? 0 : time + 1;
			double diff = base[0][ntime] - base[0][time];
			if (diff < 0) diff += 360;
			if (diff + move > 9) move = 9 - diff;
			System.out.println(time + " " + move);
			int parity = rnd.nextInt(3) + 1;
			if ((parity & 1) != 0) states[time].angle[0] = base[0][time] - move / 2;
			if ((parity & 2) != 0) states[ntime].angle[0] = base[0][ntime] + move / 2;
			int pt = time;
			int ppt = pt == 0 ? ISSModel.NUM_STATES - 1 : pt - 1;
			int nt = ntime;
			int nnt = nt == ISSModel.NUM_STATES - 1 ? 0 : nt + 1;
			for (int i = 0; i < 45; ++i) {
				if ((parity & 1) != 0) {
					int pppt = ppt - 1;
					if (pppt < 0) pppt += 92;
					double diff1 = states[pt].angle[0] - base[0][ppt];
					if (diff1 < 0) diff1 += 360;
					if (diff1 >= 360) diff1 -= 360;
					if (diff1 > 180) {
						states[ppt].angle[0] = states[pt].angle[0];
					} else {
						double diff2 = base[0][ppt] - base[0][pppt];
						if (diff2 < 0) diff2 += 360;
						if (diff2 >= 360) diff2 -= 360;
						states[ppt].angle[0] = base[0][pppt] + (diff1 + diff2) / 2;
						if (states[ppt].angle[0] >= 360) states[ppt].angle[0] -= 360;
					}
					pt = ppt;
					ppt = pppt;
				}
				if ((parity & 2) != 0) {
					int nnnt = nnt + 1;
					if (nnnt == 92) nnnt = 0;
					double diff1 = base[0][nnt] - states[nt].angle[0];
					if (diff1 < 0) diff1 += 360;
					if (diff1 >= 360) diff1 -= 360;
					if (diff1 > 180) {
						states[nnt].angle[0] = states[nt].angle[0];
					} else {
						double diff2 = base[0][nnnt] - base[0][nnt];
						if (diff2 < 0) diff2 += 360;
						if (diff2 >= 360) diff2 -= 360;
						states[nnt].angle[0] = states[nt].angle[0] + (diff1 + diff2) / 2;
						if (states[nnt].angle[0] >= 360) states[nnt].angle[0] -= 360;
					}
					nt = nnt;
					nnt = nnnt;
				}
			}
			for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
				states[i].angle[1] = 360 - states[i].angle[0];
				if (states[i].angle[1] == 360) states[i].angle[1] = 0;
			}

			if (!moveSARJ(time, 0)) {
				continue;
			}
			scorer.results = solver.calcAll(states);
			if (scorer.results == null) continue;
			double newScore = scorer.score();
			System.out.println(newScore);
			if (update(newScore, score)) {
				if (newScore > bestScore) {
					System.out.println("update best");
					bestScore = newScore;
					for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
						best[0][i] = states[i].angle[0];
						best[1][i] = states[i].angle[1];
					}
				}
				score = newScore;
				for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
					base[0][i] = states[i].angle[0];
					base[1][i] = states[i].angle[1];
				}
			} else {
				// revert something?
			}
		}
		for (int i = 0; i < 2; ++i) {
			System.out.print("{");
			for (double val : best[i]) {
				System.out.print(val + ",");
			}
			System.out.println("},");
		}
	}

	static double[] bga_p71 = { 0.0, 0.0, 19.0, 161.0, 341.0, 199.0, 341.0, 199.0, 19.0, 161.0 };
	static double[] bga_p72 = { 0.0, 0.0, 18.279218941565123, 162.06614021179462, 341.60694234180124, 198.03432707819795,
			341.8030219884793, 198.04360679800646, 18.07040042187126, 162.01925957632687 };
	static double[] bga_p73 = { 0.0, 0.0, 10.166536910985759, 159.80188243699925, 350.0236486869488, 199.59784703443736,
			340.11485438222525, 189.37326147214694, 18.585423755067605, 170.44492702174153 };
	static double[] bga_p74 = { 0.0, 0.0, 357.8646767069267, 162.17897249203074, 2.0631111107457833, 198.7866024665054,
			338.78227431931646, 178.69586463913976, 21.605963390948087, 181.11039054887004, };
	static double[] bga_p75 = { 0.0, 0.0, 44.1043759626268, 162.58210322942384, 8.227296524235696, 195.8020352625579,
			339.2003771426919, 170.73154017106285, 18.127709501038222, 189.12400928309378 };
	//	static double[] bga_p74 = { 0.0, 0.0, 359.20760718480335, 163.57680326648057, 320.2712314434077,
	//			197.6395018321984, 348.45446879514697, 178.32827379488805, 18.079934020718277, 181.81202067164278 };
	static double[] bga_m70 = { 0.0, 0.0, 160.04743434282972, 20.151988300741856, 193.754213832393, 340.0650920753359,
			201.08673247491095, 339.8745155596758, 159.98972314623168, 19.918023111166683 };
	static double[] bga_m71 = { 0.0, 0.0, 161.62618926895064, 19.17489925943126, 198.516158953206, 340.6462809522969,
			198.98233375565576, 341.24440388229056, 160.83509725380625, 19.182946023147846 };
	static double[] bga_m73 = { 0.0, 0.0, 160.69746386621324, 9.696863000308486, 200.19674065553895, 350.74474918974147,
			189.97545530235206, 343.141701720627, 169.64412894779718, 18.906755560257295 };
	static double[] bga_m74 = { 0.0, 0.0, 162.218472753688, 358.95153078275223, 202.05667672818888, 0.8563124548856498,
			179.3599839701633, 339.23460900066004, 180.86611592837895, 18.109110355344793 };
	static double[] bga_m75 = { 0.0, 0.0, 167.16951742799714, 351.4300308475907, 198.39461121235252, 8.46560346234432,
			171.50173894401135, 340.7259536301363, 188.95656799249474, 17.406979241529932 };

	double[] selectBGA(double beta) {
		if (beta == 71) {
			return bga_p71;
		} else if (beta == 72) {
			return bga_p72;
		} else if (beta == 73) {
			return bga_p73;
		} else if (beta == 74) {
			return bga_p74;
		} else if (beta == 75) {
			return bga_p75;
		} else if (beta == -70) {
			return bga_m70;
		} else if (beta == -71) {
			return bga_m71;
		} else if (beta == -73) {
			return bga_m73;
		} else if (beta == -74) {
			return bga_m74;
		} else if (beta == -75) {
			return bga_m75;
		} else {
			return bga_p72;
		}
	}

	void improveBGAEach(double beta) {
		double[][] angle = ISS.selectAngle(beta);
		double[][] vel = ISS.selectVelocity(beta);
		Solver solver = new Solver(beta, yaw);
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			for (int j = 0; j < 10; ++j) {
				states[i].angle[j] = angle[i][j];
			}
			states[i].velocity[0] = vel[0][i];
			states[i].velocity[1] = vel[1][i];
		}
		//		solver.model.renient = false;
		Result[] res = solver.calcAll(states);
		double[][] base = new double[ISSModel.NUM_STATES][10];
		double[][] best = new double[ISSModel.NUM_STATES][10];
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			for (int j = 0; j < 10; ++j) {
				base[i][j] = best[i][j] = angle[i][j];
			}
		}
		Scorer scorer = new Scorer(res);
		double score = scorer.score();
		for (int i = 0; i < 8; ++i) {
			int dangerPena = Math.max(0, scorer.maxDanger[i] - 15);
			score -= dangerPena * dangerPena;
		}
		double bestScore = score;
		double delta = 0.3;
		double[] nextResMove = new double[8];
		System.out.println("improveBGAEach init:" + score);
		for (int turn = 1; turn <= 1 << 15; ++turn) {
			if (turn % (1 << 12) == 0) {
				delta *= 0.97;
				System.out.println(turn + " " + delta);
			}
			//			int time = rnd.nextInt(ISSModel.NUM_STATES);
			int time = turn % 92;
			int ntime = time == ISSModel.NUM_STATES - 1 ? 0 : time + 1;
			int ptime = time == 0 ? ISSModel.NUM_STATES - 1 : time - 1;
			for (int i = 2; i < 10; ++i) {
				while (true) {
					double next = base[time][i] + rnd.nextGaussian() * delta;
					if (next >= 360) next -= 360;
					if (next < 0) next += 360;
					states[time].angle[i] = next;
					double moveP = states[time].angle[i] - states[ptime].angle[i];
					while (moveP >= 180)
						moveP -= 360;
					while (moveP < -180)
						moveP += 360;
					if (Math.abs(moveP) > 0.01 * 25 * 25 + 0.25 * 10) continue;
					double moveN = states[time].angle[i] - states[ntime].angle[i];
					while (moveN >= 180)
						moveN -= 360;
					while (moveN < -180)
						moveN += 360;
					if (Math.abs(moveN) > 0.01 * 25 * 25 + 0.25 * 10) continue;
					break;
				}
			}
			Result curRes = res[time];
			for (int i = 0; i < 8; ++i) {
				nextResMove[i] = res[ntime].move[i];
			}
			res[time] = solver.calc(time, states[time]);
			if (!solver.calcBGAMove(res[time], states[ptime], states[time])
					|| !solver.calcBGAMove(res[ntime], states[time], states[ntime])) {
				res[time] = curRes;
				for (int i = 0; i < 8; ++i) {
					res[ntime].move[i] = nextResMove[i];
				}
				for (int i = 2; i < 10; ++i) {
					states[time].angle[i] = base[time][i];
				}
				continue;
			}
			double newScore = scorer.score();
			if (newScore > 0) {
				for (int i = 0; i < 8; ++i) {
					int dangerPena = Math.max(0, scorer.maxDanger[i] - 15);
					newScore -= dangerPena * dangerPena;
				}
			}
			if (update(newScore, score)) {
				if (newScore > bestScore) {
					System.out.println(newScore);
					bestScore = newScore;
					for (int i = 2; i < 10; ++i) {
						best[time][i] = states[time].angle[i];
					}
				}
				score = newScore;
				for (int i = 2; i < 10; ++i) {
					base[time][i] = states[time].angle[i];
				}
			} else {
				res[time] = curRes;
				for (int i = 0; i < 8; ++i) {
					res[ntime].move[i] = nextResMove[i];
				}
				for (int i = 2; i < 10; ++i) {
					states[time].angle[i] = base[time][i];
				}
			}
		}
		scorer.results = solver.calcAll(states);
		System.out.println("score:" + scorer.score());
		scorer.log();
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			System.out.print("{");
			for (double a : best[i]) {
				System.out.print(a + ",");
			}
			System.out.println("},");
		}
	}

	double[] improveBGAFirst(double beta) {
		double[] v = new double[] { 0, 0, 90 - beta, 90 + beta, 270 + beta, 270 - beta, 270 + beta, 270 - beta, 90 - beta,
				90 + beta, };
		v = selectBGA(beta);
		//				if (beta == 72) {
		//					v = bga_p72;
		//				} else if (beta == 74) {
		//					v = bga_p74;
		//				} else if (beta == -70) {
		//					v = bga_m70;
		//				} else if (beta == -74) {
		//					v = bga_m74;
		//				}
		Solver solver = new Solver(beta, yaw);
		solver.model.renient = false;
		Result res = new Result();
		State st = new State();
		for (int j = 0; j < 10; ++j) {
			st.angle[j] = v[j];
		}
		res = solver.calc(0, st);
		double[] base = v.clone();
		double[] best = v.clone();
		double score = 0;
		for (int i = 0; i < 8; ++i) {
			score += res.power[i];
			if (res.danger[i]) score -= 1000;
		}
		double bestScore = score;
		double delta = 5;
		System.out.println("improveBGAFirst init:" + score);
		for (int turn = 1; turn <= 1 << 13; ++turn) {
			if (turn % (1 << 9) == 0) {
				delta *= 0.9;
				System.out.println(delta);
			}
			for (int i = 2; i < 10; ++i) {
				st.angle[i] = base[i] + rnd.nextGaussian() * delta;
				if (st.angle[i] >= 360) st.angle[i] -= 360;
				if (st.angle[i] < 0) st.angle[i] += 360;
			}
			res = solver.calc(0, st);
			double newScore = 0;
			for (int i = 0; i < 8; ++i) {
				newScore += res.power[i];
				if (res.danger[i]) newScore -= 1000;
			}
			if (update(newScore, score)) {
				if (newScore > bestScore) {
					System.out.println(newScore);
					bestScore = newScore;
					for (int i = 2; i < 10; ++i) {
						best[i] = st.angle[i];
					}
				}
				score = newScore;
				for (int i = 2; i < 10; ++i) {
					base[i] = st.angle[i];
				}
			}
		}
		return best;
	}

	void improveBGA(double beta) {
		double[][] v = null;
		if (beta == 72) {
			v = P72.angle;
		} else if (beta == 74) {
			v = P74.angle;
		} else if (beta == -70) {
			v = M70.angle;
		} else if (beta == -74) {
			v = M74.angle;
		} else {
			System.exit(1);
		}
		improveBGA(beta, v[0]);
	}

	double[] improveBGA(double beta, double[] initial) {
		Solver solver = new Solver(beta, yaw);
		Result[] res = new Result[ISSModel.NUM_STATES];
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			states[i].angle[0] = i * 360.0 / 92;
			states[i].angle[1] = i == 0 ? 0 : 360 - states[i].angle[0];
			for (int j = 2; j < 10; ++j) {
				states[i].angle[j] = initial[j];
			}
			res[i] = solver.calc(i, states[i]);
		}
		double[] base = initial.clone();
		double[] best = initial.clone();
		Scorer scorer = new Scorer(res);
		double score = scorer.score();
		for (int i = 0; i < 8; ++i) {
			score -= scorer.maxDanger[i] * scorer.maxDanger[i];
		}
		solver.model.renient = false;
		double bestScore = score;
		double delta = 2;
		System.out.println("improveBGA init:" + score);
		for (int turn = 1; turn <= 1 << 8; ++turn) {
			if (turn % (1 << 4) == 0) {
				delta *= 0.9;
				System.out.println(delta);
			}
			for (int i = 2; i < 10; ++i) {
				double next = base[i] + rnd.nextGaussian() * delta;
				if (next >= 360) next -= 360;
				if (next < 0) next += 360;
				for (int j = 0; j < ISSModel.NUM_STATES; ++j) {
					states[j].angle[i] = next;
				}
			}
			for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
				res[i] = solver.calc(i, states[i]);
			}
			double newScore = scorer.score();
			if (newScore > 0) {
				for (int i = 0; i < 8; ++i) {
					newScore -= scorer.maxDanger[i] * scorer.maxDanger[i];
				}
			}
			if (update(newScore, score)) {
				if (newScore > bestScore) {
					System.out.println(newScore);
					bestScore = newScore;
					for (int i = 2; i < 10; ++i) {
						best[i] = states[0].angle[i];
					}
				}
				score = newScore;
				for (int i = 2; i < 10; ++i) {
					base[i] = states[0].angle[i];
				}
			}
		}
		return best;
	}

	double runTest(double beta) {
		ISS obj = new ISS();
		double yaw = obj.getInitialOrientation(beta);
		Solver solver = new Solver(beta, yaw);
		for (int i = 0; i < ISSModel.NUM_STATES; i++) {
			double[] ret = obj.getStateAtMinute(i);
			for (int j = 0; j < 10; ++j) {
				states[i].angle[j] = ret[2 * j];
				states[i].velocity[j] = ret[2 * j + 1];
			}
		}
		Result[] results = solver.calcAll(states);
		Scorer scorer = new Scorer(results);
		double score = scorer.score();

		System.out.println("beta:" + beta);
		if (DEBUG) {
			scorer.log();
		}
		System.out.println("Score:" + score);
		return score;
	}

	void test(double beta) {
		State st = new State();
		Solver solver = new Solver(beta, yaw);
		double[] maxA = bga_m70.clone();
		for (int alpha = 0; alpha <= 30; ++alpha) {
			st.angle[0] = alpha;
			st.angle[1] = 360 - alpha;
			double max = 0;
			double delta = 1;
			for (int i = 0; i < 1 << 10; ++i) {
				if (i % (1 << 8) == 0) {
					delta *= 0.6;
				}
				for (int j = 0; j < 4; ++j) {
					st.angle[j * 2 + 2] = maxA[j * 2 + 2] + rnd.nextGaussian() * delta;
				}
				Result res = solver.calc(0, st);
				double sum = 0;
				for (int j = 0; j < 4; ++j) {
					sum += res.power[j * 2];
				}
				if (sum > max) {
					max = sum;
					System.out.println(max);
					for (int j = 0; j < 4; ++j) {
						maxA[j * 2 + 2] = st.angle[j * 2 + 2];
					}
				}
			}
			System.out.println("rot:" + alpha);
			System.out.println(max);
			System.out.println(Arrays.toString(maxA));
		}
	}

	void all(double beta) {
		double[] bgaFirst = improveBGAFirst(beta);
		System.out.println("bgaFirst:" + Arrays.toString(bgaFirst));
		double[] bga = improveBGA(beta, bgaFirst);
		System.out.println("bga:" + Arrays.toString(bga));
		improveSARJ3(beta, bga);
	}

	Tester() {
		for (int i = 0; i < ISSModel.NUM_STATES; ++i) {
			states[i] = new State();
		}
	}

	double yaw = 0.;

	public static void main(String[] args) {
		double beta = 75;
		final int EXAMPLE = 0;
		final int PROVISIONAL = 1;
		final int SYSTEM = 2;
		int mode = 0;
		int type = -1;
		for (int a = 0; a < args.length; a++) {
			if ("-beta".equals(args[a])) {
				beta = Double.parseDouble(args[++a]);
			} else if ("-mode".equals(args[a])) {
				mode = Integer.parseInt(args[++a]);
			} else if ("-type".equals(args[a])) {
				type = Integer.parseInt(args[++a]);
			}
		}

		double[] betas;
		if (type == EXAMPLE) {
			betas = new double[] { 70, -72 };
		} else if (type == PROVISIONAL) {
			betas = new double[] { 72, 74, -70, -74 };
		} else if (type == SYSTEM) {
			betas = new double[] { 71, 73, 75, -71, -73, -75 };
		} else {
			betas = new double[] { beta };
		}
		double sum = 0;
		for (double b : betas) {
			long start = System.currentTimeMillis();
			Tester tester = new Tester();
			if (mode == 0) {
				sum += tester.runTest(b);
			} else if (mode == 1) {
				sum += tester.optimize(b);
			} else if (mode == 2) {
				tester.improveBGA(b);
			} else if (mode == 3) {
				tester.improveBGAEach(b);
			} else if (mode == 4) {
				tester.improveSARJ(b);
			} else if (mode == 5) {
				tester.improveSARJ2(b);
			} else if (mode == 6) {
				tester.improveSARJ3(b);
			} else if (mode == 7) {
				tester.improveBGAFirst(b);
			} else if (mode == 8) {
				tester.all(b);
			} else if (mode == 9) {
				tester.improveAll(b);
			} else if (mode < 0) {
				tester.test(b);
			}
			System.out.println("elapsed:" + (System.currentTimeMillis() - start));
			System.out.println();
		}
		if (betas.length > 1) System.out.println("ave:" + (sum / betas.length));
	}
}
