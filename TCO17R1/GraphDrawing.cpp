#include <algorithm>
#include <utility>
#include <vector>
#include <bitset>
#include <string>
#include <iostream>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifndef LOCAL
#define NDEBUG
// #define DEBUG
#else
// #define MEASURE_TIME
// #define DEBUG
#endif
#include <cassert>

using namespace std;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t i64;
typedef int64_t ll;
typedef uint64_t ull;
typedef vector<int> vi;
typedef vector<vi> vvi;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 6000;
#else
#ifdef TEST
const double CLOCK_PER_SEC = 2.0e9;
const ll TL = 9000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9900;
#endif
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;

//////// end of template ////////

struct Point {
	int y, x;

	bool operator==(const Point& p) const {
		return y == p.y && x == p.y;
	}
};

inline int dist2(int x1, int y1, int x2, int y2) {
	return sq(x2 - x1) + sq(y2 - y1);
}

inline int dist2(const Point& p1, const Point& p2) {
	return sq(p1.x - p2.x) + sq(p1.y - p2.y);
}

struct Edge {
	int s,e,d;
	double sq_inv_d;
	double score;
};

const int SZ = 701;
int V, E;
array<Point, 1000> vertexes;
array<Edge, 10000> edges;
vvi v2e;
vvi v2e_adj, v2e_d;
vector<vector<double>> v2e_d_inv;
array<bitset<SZ>, SZ> used;
alignas(16) array<float, 128> new_scores;
double sa_cooler;

inline bool transit(double old_score, double new_score) {
	if (new_score <= old_score) return true;
	return rnd.nextDouble() < exp((old_score - new_score) * sa_cooler);
}

void sa_initial() {
	const int BUCKET = 35;
	const int CELL_SZ = SZ / BUCKET;
	sa_cooler = 0.000018 * E / V;
	for (int i = 0; i < V; ++i) {
		vertexes[i].y = rnd.nextUInt(BUCKET);
		vertexes[i].x = rnd.nextUInt(BUCKET);
	}
	vector<double> v_scores(V);
	double score = 0;
	for (int i = 0; i < E; ++i) {
		Edge& e = edges[i];
		int dist = dist2(vertexes[e.s], vertexes[e.e]);
		double diff = sqrt(dist) * CELL_SZ - e.d;
		e.score = abs(diff);
		score += e.score;
		v_scores[e.s] += e.score;
		v_scores[e.e] += e.score;
	}

	const ll timelimit = TL * 0.39;
	const ll update_duration = (timelimit - get_elapsed_msec()) / 100;
	ll next_update_time = get_elapsed_msec() + update_duration;
	const __m128 cell_sz = _mm_set1_ps(CELL_SZ);
	double cutoff = -10 / sa_cooler;
	for (int turn = 0; ; ++turn) {
		START_TIMER(10);
		ADD_COUNTER(0);
		if ((turn & 0x7FFF) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed > timelimit) {
				debug("turn:%d score:%f\n", turn, score);
				break;
			}
			if (elapsed > next_update_time) {
				sa_cooler *= 1.08;
				if (elapsed > timelimit - update_duration) {
					sa_cooler *= 100;
				}
				cutoff = -10 / sa_cooler;
				next_update_time += update_duration;
				// debug("cooler:%f score:%.1f\n", sa_cooler, score);
			}
		}
		const int pi = rnd.nextUInt(V);
		const int ny = rnd.nextUInt(BUCKET);
		const int nx = rnd.nextUInt(BUCKET);
		double new_score_sum = 0;
		STOP_TIMER(10);
		START_TIMER(11);
		const __m128 base_y = _mm_set1_ps(ny);
		const __m128 base_x = _mm_set1_ps(nx);
		int i = 0;
		for (; i + 3 < v2e_adj[pi].size(); i += 4) {
			int api_1 = v2e_adj[pi][i];
			int api_2 = v2e_adj[pi][i+1];
			int api_3 = v2e_adj[pi][i+2];
			int api_4 = v2e_adj[pi][i+3];
			__m128 y = _mm_set_ps(vertexes[api_1].y, vertexes[api_2].y, vertexes[api_3].y, vertexes[api_4].y);
			__m128 x = _mm_set_ps(vertexes[api_1].x, vertexes[api_2].x, vertexes[api_3].x, vertexes[api_4].x);
			y = _mm_sub_ps(y, base_y);
			x = _mm_sub_ps(x, base_x);
			y = _mm_mul_ps(y, y);
			x = _mm_mul_ps(x, x);
			__m128 dist = _mm_add_ps(y, x);
			dist = _mm_sqrt_ps(dist);
			__m128 expect_dists = _mm_set_ps(v2e_d[pi][i], v2e_d[pi][i+1], v2e_d[pi][i+2], v2e_d[pi][i+3]);
			dist = _mm_mul_ps(dist, cell_sz);
			dist = _mm_sub_ps(dist, expect_dists);
			dist = _mm_andnot_ps(_mm_set1_ps(-0.0f), dist); // abs
			_mm_storer_ps(&new_scores[i], dist);
			new_score_sum += new_scores[i] + new_scores[i+1] + new_scores[i+2] + new_scores[i+3];
			if (v_scores[pi] - new_score_sum < cutoff) {
				STOP_TIMER(11);
				goto END;
			}
		}
		for (; i < v2e_adj[pi].size(); ++i) {
			int api = v2e_adj[pi][i];
			int new_dist2 = dist2(nx, ny, vertexes[api].x, vertexes[api].y);
			double diff = sqrt(new_dist2) * CELL_SZ - v2e_d[pi][i];
			double new_score = abs(diff);
			new_score_sum += new_score;
			if (v_scores[pi] - new_score_sum < cutoff) {
				STOP_TIMER(11);
				goto END;
			}
			new_scores[i] = (float)new_score;
		}
		STOP_TIMER(11);
		START_TIMER(12);
		if (transit(v_scores[pi], new_score_sum)) {
			ADD_COUNTER(1);
			score += new_score_sum - v_scores[pi];
			vertexes[pi].y = ny;
			vertexes[pi].x = nx;
			v_scores[pi] = new_score_sum;
			for (int i = 0; i < v2e[pi].size(); ++i) {
				Edge& e = edges[v2e[pi][i]];
				v_scores[e.s + e.e - pi] += new_scores[i] - e.score;
				e.score = new_scores[i];
			}
		}
		STOP_TIMER(12);
END:;
	}
	for (int i = 0; i < V; ++i) {
		while (true) {
			int y = vertexes[i].y * CELL_SZ + rnd.nextUInt(CELL_SZ);
			int x = vertexes[i].x * CELL_SZ + rnd.nextUInt(CELL_SZ);
			if (used[y][x]) continue;
			vertexes[i].y = y;
			vertexes[i].x = x;
			used[y][x] = true;
			break;
		}
	}
}

vector<Point> ans;
double ans_score;

void sa_main() {
	ans.resize(V);
	ans_score = 0;
	sa_cooler = 10.0 * E / V;
	int WINDOW = SZ * 1 / 10;
	double RATIO_TARGET_MAX = 1.0;
	double RATIO_TARGET_MIN = 1.0;
	double RATIO_TARGET_MID = 1.0;
	const ll timelimit = TL * 0.55;
	const ll update_duration = (timelimit - get_elapsed_msec()) / 150;
	vector<double> v_scores(V);

	auto update_score = [&RATIO_TARGET_MIN, &RATIO_TARGET_MAX, &RATIO_TARGET_MID, &v_scores](){
		v_scores.clear();
		double max_ratio = 0.0;
		double min_ratio = 99999.0;
		for (int i = 0; i < E; ++i) {
			double dist = dist2(vertexes[edges[i].s], vertexes[edges[i].e]);
			double ratio2 = dist * edges[i].sq_inv_d;
			max_ratio = max(max_ratio, ratio2);
			min_ratio = min(min_ratio, ratio2);
			edges[i].score = ratio2 >= 1 ? ratio2 : 1.0 / ratio2;
		}
		RATIO_TARGET_MAX = max(0.1, min(1.8, max_ratio - 0.1));
		RATIO_TARGET_MIN = max(0.1, min(1.8, 1.0 / min_ratio - 0.1));
		RATIO_TARGET_MIN = min(RATIO_TARGET_MAX, 1.0 / RATIO_TARGET_MIN);
		RATIO_TARGET_MID = sqrt(RATIO_TARGET_MAX * RATIO_TARGET_MIN);
		double sum = 0;
		for (int i = 0; i < E; ++i) {
			if (edges[i].score >= RATIO_TARGET_MID) {
				edges[i].score = edges[i].score > RATIO_TARGET_MAX ? sq(sq(edges[i].score - RATIO_TARGET_MAX + 0.4)) : 0;
			} else {
				edges[i].score = edges[i].score < RATIO_TARGET_MIN ? sq(sq(RATIO_TARGET_MIN - edges[i].score + 0.4)) : 0;
			}
			sum += edges[i].score;
			v_scores[edges[i].s] += edges[i].score;
			v_scores[edges[i].e] += edges[i].score;
		}
		double score = sqrt(min_ratio / max_ratio);
		if (score > ans_score) {
			ans_score = score;
			copy(vertexes.begin(), vertexes.begin() + V, ans.begin());
			// debug("ans score:%f\n", ans_score);
		}
		return sum;
	};
	update_score();

	ll next_update_time = get_elapsed_msec() + update_duration;
	vector<double> new_scores;
	double cutoff = -10 / sa_cooler;
	for (int turn = 0; ; ++turn) {
		START_TIMER(0);
		ADD_COUNTER(3);
		if ((turn & 0xFFFF) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed > timelimit) {
				debug("turn:%d score:%f\n", turn, ans_score);
				break;
			}
			if (elapsed > next_update_time) {
				update_score();
				RATIO_TARGET_MID = sqrt(RATIO_TARGET_MAX * RATIO_TARGET_MIN);
				WINDOW *= 0.97;
				if (WINDOW < 5) WINDOW = 5;
				sa_cooler *= 1.03;
				cutoff = -10 / sa_cooler;
				next_update_time += update_duration;
				// debug("WINDOW:%d cooler:%f RATIO_TARGET_MIN:%f RATIO_TARGET_MAX:%f\n", WINDOW, sa_cooler, RATIO_TARGET_MIN, RATIO_TARGET_MAX);
			}
		}
		int pi = rnd.nextUInt(V);
		int cy = vertexes[pi].y;
		int cx = vertexes[pi].x;
		int ny, nx;
		do {
			ny = cy + rnd.nextUInt(WINDOW * 2 + 1) - WINDOW;
			nx = cx + rnd.nextUInt(WINDOW * 2 + 1) - WINDOW;
			if (ny < 0) ny = 0;
			if (ny >= SZ) ny = SZ - 1;
			if (nx < 0) nx = 0;
			if (nx >= SZ) nx = SZ - 1;
		} while (used[ny][nx]);
		double new_score_sum = 0;
		new_scores.clear();
		STOP_TIMER(0);
		START_TIMER(1);
		for (int i = 0; i < v2e_adj[pi].size(); ++i) {
			int api = v2e_adj[pi][i];
			double new_dist = dist2(nx, ny, vertexes[api].x, vertexes[api].y);
			double new_ratio2 = new_dist * v2e_d_inv[pi][i];
			double new_score;
			if (new_ratio2 > RATIO_TARGET_MAX) {
				new_score = sq(sq(new_ratio2 - RATIO_TARGET_MAX + 0.4));
			} else if (new_ratio2 < RATIO_TARGET_MIN) {
				new_score = sq(sq(RATIO_TARGET_MIN - new_ratio2 + 0.4));
			} else {
				new_scores.push_back(0.0);
				continue;
			}
			new_score_sum += new_score;
			if (v_scores[pi] - new_score_sum < cutoff) {
				STOP_TIMER(1);
				goto END_MAIN;
			}
			new_scores.push_back(new_score);
		}
		STOP_TIMER(1);
		START_TIMER(2);
		if (transit(v_scores[pi], new_score_sum)) {
			ADD_COUNTER(4);
			used[cy][cx] = false;
			used[ny][nx] = true;
			vertexes[pi].y = ny;
			vertexes[pi].x = nx;
			v_scores[pi] = new_score_sum;
			for (int i = 0; i < new_scores.size(); ++i) {
				int ei = v2e[pi][i];
				Edge& e = edges[ei];
				v_scores[e.s + e.e - pi] += new_scores[i] - e.score;
				e.score = new_scores[i];
			}
		}
		STOP_TIMER(2);
END_MAIN:;
	}
	copy(ans.begin(), ans.end(), vertexes.begin());
	for (auto used_row : used) {
		used_row.reset();
	}
	for (int i = 0; i < V; ++i) {
		used[vertexes[i].y][vertexes[i].x] = true;
	}
}

vvi bucket_edges;
vi edges_bucket_pos;
const int BUCKET_COUNT = 20000;
const double BUCKET_WIDTH = 2.0 / BUCKET_COUNT;
const double BUCKET_WIDTH_INV = 1.0 / BUCKET_WIDTH;

inline int bucket_index(double ratio2) {
	int bi = (int)(ratio2 * BUCKET_WIDTH_INV + 1e-8);
	if (bi > BUCKET_COUNT) bi = BUCKET_COUNT;
	return bi;
}

void sa_strict() {
	ans.resize(V);
	sa_cooler = 100.;
	int WINDOW = SZ * 1 / 10;
	const ll timelimit = TL - 50;
	const ll update_duration = (timelimit - get_elapsed_msec()) / 150;
	bucket_edges.resize(BUCKET_COUNT + 1);
	edges_bucket_pos.resize(E);

	double max_ratio = 0.0;
	double min_ratio = 99999.0;
	int max_ratio_edge = 0;
	int min_ratio_edge = 0;
	for (int i = 0; i < E; ++i) {
		edges[i].score = dist2(vertexes[edges[i].s], vertexes[edges[i].e]) * edges[i].sq_inv_d;
		if (edges[i].score > max_ratio) {
			max_ratio = edges[i].score;
			max_ratio_edge = i;
		}
		if (edges[i].score < min_ratio) {
			min_ratio = edges[i].score;
			min_ratio_edge = i;
		}
		int bi = bucket_index(edges[i].score);
		edges_bucket_pos[i] = bucket_edges[bi].size();
		bucket_edges[bi].push_back(i);
	}
	double cur_score = sqrt(min_ratio / max_ratio);
	ans_score = cur_score;

	ll next_update_time = get_elapsed_msec() + update_duration;
	for (int turn = 0; ; ++turn) {
		ADD_COUNTER(7);
		if ((turn & 0xFFFF) == 0) {
			const ll elapsed = get_elapsed_msec();
			if (elapsed > timelimit) {
				debug("turn:%d score:%f\n", turn, ans_score);
				break;
			}
			if (elapsed > next_update_time) {
				WINDOW *= 0.98;
				if (WINDOW < 5) WINDOW = 5;
				sa_cooler *= 1.03;
				next_update_time += update_duration;
				// debug("cooler:%f min_ratio:%f max_ratio:%f\n", sa_cooler, min_ratio, max_ratio);
			}
		}
		START_TIMER(3);
		int ratio_ei = (turn & 1) ? max_ratio_edge : min_ratio_edge;
		int pi = (turn & 2) ? edges[ratio_ei].s : edges[ratio_ei].e;
		int ny, nx;
		do {
			ny = vertexes[pi].y + rnd.nextUInt(WINDOW * 2 + 1) - WINDOW;
			nx = vertexes[pi].x + rnd.nextUInt(WINDOW * 2 + 1) - WINDOW;
			if (ny < 0) ny = 0;
			if (ny >= SZ) ny = SZ - 1;
			if (nx < 0) nx = 0;
			if (nx >= SZ) nx = SZ - 1;
		} while (used[ny][nx]);
		double new_max_ratio = 0.0;
		double new_min_ratio = 99999.0;
		int new_max_edge = 0;
		int new_min_edge = 0;
		STOP_TIMER(3);
		START_TIMER(4);
		const __m128 base_y = _mm_set1_ps(ny);
		const __m128 base_x = _mm_set1_ps(nx);
		int i = 0;
		for (; i + 3 < v2e_adj[pi].size(); i += 4) {
			int api_1 = v2e_adj[pi][i];
			int api_2 = v2e_adj[pi][i+1];
			int api_3 = v2e_adj[pi][i+2];
			int api_4 = v2e_adj[pi][i+3];
			__m128 y = _mm_set_ps(vertexes[api_1].y, vertexes[api_2].y, vertexes[api_3].y, vertexes[api_4].y);
			__m128 x = _mm_set_ps(vertexes[api_1].x, vertexes[api_2].x, vertexes[api_3].x, vertexes[api_4].x);
			y = _mm_sub_ps(y, base_y);
			x = _mm_sub_ps(x, base_x);
			y = _mm_mul_ps(y, y);
			x = _mm_mul_ps(x, x);
			__m128 dist = _mm_add_ps(y, x);
			__m128 d_inv = _mm_set_ps(v2e_d_inv[pi][i], v2e_d_inv[pi][i+1], v2e_d_inv[pi][i+2], v2e_d_inv[pi][i+3]);
			dist = _mm_mul_ps(dist, d_inv);
			_mm_storer_ps(&new_scores[i], dist);
			for (int j = 0; j < 4; ++j) {
				if (new_scores[i + j] > new_max_ratio) {
					new_max_ratio = new_scores[i + j];
					new_max_edge = v2e[pi][i + j];
				}
				if (new_scores[i + j] < new_min_ratio) {
					new_min_ratio = new_scores[i + j];
					new_min_edge = v2e[pi][i + j];
				}
			}
		}
		for (; i < v2e_adj[pi].size(); ++i) {
			int api = v2e_adj[pi][i];
			double new_dist = dist2(nx, ny, vertexes[api].x, vertexes[api].y);
			double new_ratio2 = new_dist * v2e_d_inv[pi][i];
			if (new_ratio2 > new_max_ratio) {
				new_max_ratio = new_ratio2;
				new_max_edge = v2e[pi][i];
			}
			if (new_ratio2 < new_min_ratio) {
				new_min_ratio = new_ratio2;
				new_min_edge = v2e[pi][i];
			}
			new_scores[i] = new_ratio2;
		}
		STOP_TIMER(4);
		START_TIMER(5);
		if (new_max_ratio < max_ratio) {
			ADD_COUNTER(8);
			bool found = false;
			for (int bi = min(BUCKET_COUNT, bucket_index(max_ratio) + 1); !found; --bi) {
				for (int ei : bucket_edges[bi]) {
					const Edge& e = edges[ei];
					if (e.s == pi || e.e == pi) continue;
					found = true;
					if (e.score > new_max_ratio) {
						new_max_ratio = e.score;
						new_max_edge = ei;
					}
				}
			}
		}
		if (new_min_ratio > min_ratio) {
			ADD_COUNTER(9);
			bool found = false;
			for (int bi = max(0, bucket_index(min_ratio) - 1); !found; ++bi) {
				for (int ei : bucket_edges[bi]) {
					const Edge& e = edges[ei];
					if (e.s == pi || e.e == pi) continue;
					found = true;
					if (e.score < new_min_ratio) {
						new_min_ratio = e.score;
						new_min_edge = ei;
					}
				}
			}
		}
		STOP_TIMER(5);
		START_TIMER(6);
		double new_score = sqrt(new_min_ratio / new_max_ratio);
		if (transit(new_score, cur_score)) {
			ADD_COUNTER(10);
			used[vertexes[pi].y][vertexes[pi].x] = false;
			used[ny][nx] = true;
			vertexes[pi].y = ny;
			vertexes[pi].x = nx;
			for (int i = 0; i < v2e[pi].size(); ++i) {
				int ei = v2e[pi][i];
				Edge& e = edges[ei];
				int old_bi = bucket_index(e.score);
				int new_bi = bucket_index(new_scores[i]);
				if (old_bi != new_bi) {
					int old_pos = edges_bucket_pos[ei];
					swap(bucket_edges[old_bi][old_pos], bucket_edges[old_bi].back());
					bucket_edges[old_bi].pop_back();
					assert(edges_bucket_pos[bucket_edges[old_bi][old_pos]] == bucket_edges[old_bi].size());
					edges_bucket_pos[bucket_edges[old_bi][old_pos]] = old_pos;
					edges_bucket_pos[ei] = bucket_edges[new_bi].size();
					bucket_edges[new_bi].push_back(ei);
				}
				e.score = new_scores[i];
			}
			cur_score = new_score;
			max_ratio = new_max_ratio;
			min_ratio = new_min_ratio;
			max_ratio_edge = new_max_edge;
			min_ratio_edge = new_min_edge;
			if (cur_score > ans_score) {
				ans_score = cur_score;
				copy(vertexes.begin(), vertexes.begin() + V, ans.begin());
				// debug("ans score:%f\n", ans_score);
			}
		}
		STOP_TIMER(6);
	}
	copy(ans.begin(), ans.end(), vertexes.begin());
	for (auto used_row : used) {
		used_row.reset();
	}
	for (int i = 0; i < V; ++i) {
		used[vertexes[i].y][vertexes[i].x] = true;
	}
}

double max_ratio, min_ratio;

bool dfs(int base, int v, int expect_dist, int depth) {
	int dy = vertexes[v].y - vertexes[base].y;
	int dx = vertexes[v].x - vertexes[base].x;
	double d2 = dx * dx + dy * dy;
	double d2_inv = 1.0 / sqrt(d2);
	if (d2 >= max_ratio * expect_dist * expect_dist) {
		d2_inv *= -1;
	}
	double ey = dy * d2_inv;
	double ex = dx * d2_inv;
	int cy = (int)(vertexes[v].y + ey * 3);
	int cx = (int)(vertexes[v].x + ex * 3);
	int nx, ny;
	for (int i = 0; ; ++i) {
		ny = cy + rnd.nextUInt(9) - 4;
		nx = cx + rnd.nextUInt(9) - 4;
		if (ny < 0) ny = 0;
		if (ny >= SZ) ny = SZ - 1;
		if (nx < 0) nx = 0;
		if (nx >= SZ) nx = SZ - 1;
		if (!used[ny][nx]) break;
		if (i == 10) return false;
	}

	Edge* next = nullptr;
	for (int ei : v2e[v]) {
		Edge& e = edges[ei];
		int api = e.s + e.e - v;
		double new_dist = dist2(nx, ny, vertexes[api].x, vertexes[api].y);
		double new_ratio2 = new_dist * e.sq_inv_d;
		if (new_ratio2 >= max_ratio || new_ratio2 <= min_ratio) {
			if (next) return false;
			next = &e;
		}
	}
	int py = vertexes[v].y;
	int px = vertexes[v].x;
	used[py][px] = false;
	used[ny][nx] = true;
	vertexes[v].y = ny;
	vertexes[v].x = nx;
	if (!next) {
		debug("dfs success:%d\n", depth);
		return true;
	}
	int next_v = next->s + next->e - v;
	if (depth < 10 && dfs(v, next_v, next->d, depth + 1)) {
		return true;
	} else {
		used[ny][nx] = false;
		used[py][px] = true;
		vertexes[v].y = py;
		vertexes[v].x = px;
		return false;
	}
}

void climb() {
	int max_ei = 0;
	int min_ei = 0;
	max_ratio = 0;
	min_ratio = 99999;
	for (int i = 0; i < E; ++i) {
		double dist = dist2(vertexes[edges[i].s], vertexes[edges[i].e]);
		double ratio = dist * edges[i].sq_inv_d;
		if (ratio > max_ratio) {
			max_ratio = ratio;
			max_ei = i;
		} else if (ratio < min_ratio) {
			min_ratio = ratio;
			min_ei = i;
		}
	}
	for (int turn = 0; ; ++turn) {
		if ((turn & 0xFF) == 0) {
			if (get_elapsed_msec() > TL) {
				debug("turn:%d\n", turn);
				break;
			}
		}
		int ei = (turn & 1) ? max_ei : min_ei;
		int pi = (turn & 2) ? edges[ei].s : edges[ei].e;
		int api = edges[ei].s + edges[ei].e - pi;
		if (dfs(api, pi, edges[ei].d, 0)) {
			max_ei = 0;
			min_ei = 0;
			max_ratio = 0;
			min_ratio = 99999;
			for (int i = 0; i < E; ++i) {
				double dist = dist2(vertexes[edges[i].s], vertexes[edges[i].e]);
				double ratio = dist * edges[i].sq_inv_d;
				if (ratio > max_ratio) {
					max_ratio = ratio;
					max_ei = i;
				} else if (ratio < min_ratio) {
					min_ratio = ratio;
					min_ei = i;
				}
			}
			debug("score:%f\n", sqrt(min_ratio / max_ratio));
		}
	}
}

void solve() {
	sa_initial();
	sa_main();
	sa_strict();
	climb();
}

struct GraphDrawing {
  vi plot(int NV, const vi& edges);
};

vi GraphDrawing::plot(int NV, const vi& es) {
	start_time = get_time();
	V = NV;
	E = es.size() / 3;
	v2e.resize(V);
	for (int i = 0; i < E; ++i) {
		edges[i].s = es[3 * i];
		edges[i].e = es[3 * i + 1];
		edges[i].d = es[3 * i + 2];
		edges[i].sq_inv_d = 1.0 / sq(edges[i].d);
		v2e[edges[i].s].push_back(i);
		v2e[edges[i].e].push_back(i);
	}
	v2e_adj.resize(V);
	v2e_d.resize(V);
	v2e_d_inv.resize(V);
	for (int i = 0; i < V; ++i) {
		sort(v2e[i].begin(), v2e[i].end(), [](int e1, int e2){ return edges[e1].d < edges[e2].d; });
		for (int ei : v2e[i]) {
			const Edge& e = edges[ei];
			v2e_adj[i].push_back(e.s + e.e - i);
			v2e_d[i].push_back(e.d);
			v2e_d_inv[i].push_back(e.sq_inv_d);
		}
	}

	solve();
	vi ret;
	for (int i = 0; i < V; ++i) {
		ret.push_back(vertexes[i].y);
		ret.push_back(vertexes[i].x);
	}
#ifdef DEBUG
	for (int i = 0; i < E; ++i) {
		edges[i].score = sqrt(dist2(vertexes[edges[i].s], vertexes[edges[i].e])) / edges[i].d;
	}
	sort(edges.begin(), edges.begin() + E, [](const Edge& e1, const Edge& e2){return e1.score < e2.score;});
	for (int i = 0; i < 10; ++i) {
		int v1 = edges[i].s;
		int v2 = edges[i].e;
		debug("%f %3d (%3d,%3d)-(%3d,%3d) %f\n", edges[i].score, edges[i].d, vertexes[v1].y, vertexes[v1].x, vertexes[v2].y, vertexes[v2].x, sqrt(dist2(vertexes[v1], vertexes[v2])));
	}
	for (int i = 0; i < 10; ++i) {
		int v1 = edges[E - 1 - i].s;
		int v2 = edges[E - 1 - i].e;
		debug("%f %3d (%3d,%3d)-(%3d,%3d) %f\n", edges[E - 1 - i].score, edges[E - 1 - i].d, vertexes[v1].y, vertexes[v1].x, vertexes[v2].y, vertexes[v2].x, sqrt(dist2(vertexes[v1], vertexes[v2])));
	}
#endif
	PRINT_COUNTER();
	PRINT_TIMER();
	return ret;
}

#if defined(LOCAL) || defined(TEST)
int main() {
	int N, E;
	scanf("%d %d", &N, &E);
	vi edges(E);
	for (int i = 0; i < E; ++i) {
		scanf("%d", &edges[i]);
	}

	GraphDrawing obj;
	auto ret = obj.plot(N, edges);
	printf("%d\n", (int)ret.size());
	for (int i = 0; i < ret.size(); ++i) {
		printf("%d\n", ret[i]);
	}
	fflush(stdout);
}
#endif