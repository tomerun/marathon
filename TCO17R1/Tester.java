import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;

class Pnt {
  int x, y;

  Pnt(int x1, int y1) {
    x = x1;
    y = y1;
  }

  boolean equals(Pnt other) {
    return (x == other.x && y == other.y);
  }

  int dist2(Pnt other) {
    return (x - other.x) * (x - other.x) + (y - other.y) * (y - other.y);
  }

  public String toString() {
    return "(" + x + "," + y + ")";
  }
}

public class Tester {
  private static final int NV_MAX = 1000, NV_MIN = 10;
  private final int SZ = 700;             // field size
  private static int USER_V = -1, USER_E = -1, USER_P = -1;

  private int NV, NE, P;
  private int[] edgeBeg, edgeEnd;         // indices of edges start and ends
  private int[] edgeLen;                  // required length of the edges
  private Pnt[] p;                        // coordinates of points (returned by user)
  private double[] ratios;
  private Pnt[] pt;                       // coordinates of points used during generation and visualization
  private double min_ratio, max_ratio;

  private void generate(long seed) throws Exception {
    SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
    rnd.setSeed(seed);
    NV = rnd.nextInt(NV_MAX - NV_MIN + 1) + NV_MIN;
    if (seed == 1) {
      NV = NV_MIN / 2;
    } else if (seed == 2) {
      NV = NV_MIN * 2;
    } else if (seed == 3) {
      NV = NV_MAX;
    }
    if (1000 <= seed && seed < 2000) {
      NV = 10 + (int) (seed - 1000) / 10 * 10;
    }
    if (USER_V != -1) NV = USER_V;

    int maxNE = NV * Math.min(NV_MIN, (NV - 1) / 4), minNE = NV - 1;
    NE = rnd.nextInt(maxNE - minNE + 1) + minNE;
    if (USER_E != -1) NE = USER_E;

    // generate the required edges
    // each vertex must be used in at least one edge
    // no edge is given twice
    // graph doesn't have to be connected
    edgeBeg = new int[NE];
    edgeEnd = new int[NE];
    while (true) {
      boolean[] vertexUsed = new boolean[NV];
      int nvUsed = 0;
      HashSet<Integer> edgeUsed = new HashSet<>();
      for (int i = 0; i < NE; ++i) {
        while (true) {
          int beg;
          if (nvUsed < NV) {
            for (beg = 0; beg < NV; ++beg) {
              if (!vertexUsed[beg]) break;
            }
          } else {
            beg = rnd.nextInt(NV);
          }
          int end = rnd.nextInt(NV);
          if (end == beg) continue;
          if (beg > end) {
            int t = beg;
            beg = end;
            end = t;
          }
          Integer edge = beg * NV + end;
          if (edgeUsed.contains(edge)) continue;
          edgeUsed.add(edge);
          if (!vertexUsed[beg]) {
            nvUsed++;
            vertexUsed[beg] = true;
          }
          if (!vertexUsed[end]) {
            nvUsed++;
            vertexUsed[end] = true;
          }
          edgeBeg[i] = beg;
          edgeEnd[i] = end;
          break;
        }
      }
      if (nvUsed == NV) break;
    }

    int maxLen = (int) ((SZ + 1) * Math.sqrt(2.0));
    int minLen = 1;

    pt = new Pnt[NV];
    HashSet<Integer> pointUsed = new HashSet<>();
    for (int i = 0; i < NV; ++i) {
      while (true) {
        pt[i] = new Pnt(rnd.nextInt(SZ + 1), rnd.nextInt(SZ + 1));
        Integer point = pt[i].x * (SZ + 1) + pt[i].y;
        if (pointUsed.contains(point)) continue;
        pointUsed.add(point);
        break;
      }
    }

    // calculate lengths based on the real values and distort a certain percentage of them
    P = rnd.nextInt(100);
    if (1000 <= seed && seed < 2000) {
      P = (int) (seed % 10 * 10) + rnd.nextInt(10);
    }
    if (debug) System.out.println("Required edge lengths: ");
    edgeLen = new int[NE];
    for (int i = 0; i < NE; ++i) {
      double realLen = Math.sqrt(pt[edgeBeg[i]].dist2(pt[edgeEnd[i]]));
      if (rnd.nextDouble() * 100 < P) {
        // distorted value
        edgeLen[i] = (int) (realLen * Math.pow(1.1, rnd.nextGaussian()));
      } else {
        edgeLen[i] = (int) realLen;
      }
      if (edgeLen[i] < minLen) {
        edgeLen[i] = minLen;
      }
      if (edgeLen[i] > maxLen) {
        edgeLen[i] = maxLen;
      }

      if (debug) {
        String dist = String.format("%.6f", edgeLen[i] / realLen);
        System.out.println(edgeBeg[i] + "-" + edgeEnd[i] + " = " + edgeLen[i] + " (dist " + dist + ")");
      }
    }
  }

  private double calcScore(boolean deb) {
    // verify that all points have distinct coordinates
    for (int i = 0; i < NV; ++i)
      for (int j = 0; j < i; ++j)
        if (p[i].equals(p[j])) {
          throw new RuntimeException("Multiple vertices placed at " + p[i] + ".");
        }

    // for each edge, calculate the ratio "actual length / required length"
    min_ratio = -1;
    max_ratio = -1;
    for (int i = 0; i < NE; ++i) {
      double len = Math.sqrt(p[edgeBeg[i]].dist2(p[edgeEnd[i]]));
      double r = len / edgeLen[i];
      if (min_ratio < 0 || min_ratio > r) {
        min_ratio = r;
      }
      if (max_ratio < r) {
        max_ratio = r;
      }
      ratios[i] = r;
      if (deb) System.out.println(edgeLen[i] + " " + len + " " + r);
    }
    if (deb) {
      System.out.println("Min ratio = " + min_ratio);
      System.out.println("Max ratio = " + max_ratio);
    }

    return min_ratio / max_ratio;
  }

  private Result runTest(long seed) {
    Result res = new Result();
    res.seed = seed;
    try {
      generate(seed);
      res.V = USER_V == -1 ? NV : USER_V;
      res.E = USER_E == -1 ? NE : USER_E;
      res.P = USER_P == -1 ? P : USER_P;
      p = new Pnt[NV];
      ratios = new double[NE];

      if (!exec.isEmpty()) {
        Runtime rt = Runtime.getRuntime();
        proc = rt.exec(exec);
        new ErrorReader(proc.getErrorStream()).start();
      }
      if (proc != null) {
        int[] edgePar = new int[3 * NE];
        for (int i = 0; i < NE; ++i) {
          edgePar[3 * i] = edgeBeg[i];
          edgePar[3 * i + 1] = edgeEnd[i];
          edgePar[3 * i + 2] = edgeLen[i];
        }
        int[] ret;
        long startTime = System.currentTimeMillis();
        ret = plot(NV, edgePar);
        res.elapsed = System.currentTimeMillis() - startTime;
        if (ret.length != 2 * NV) {
          throw new RuntimeException("Your return must contain " + 2 * NV + " elements, and it contained " + ret.length + ".");
        }
        for (int i = 0; i < NV; ++i) {
          p[i] = new Pnt(ret[2 * i], ret[2 * i + 1]);
          if (p[i].x < 0 || p[i].x > SZ || p[i].y < 0 || p[i].y > SZ) {
            throw new RuntimeException("Vertex " + i + " placed at " + p[i] + " outside of [0, " + SZ + "] x [0, " + SZ + "] field.");
          }
        }
      } else {
        p = pt; // use the points allocation from which the input data was generated
      }

      if (vis) {
        jf.setSize(SZX + 17, SZY + 37);
        jf.setVisible(true);
        draw();
      }

      if (debug) {
        System.out.println("Final vertices coordinates: ");
        for (int i = 0; i < NV; ++i)
          System.out.println(i + " - " + p[i]);
      }
      res.score = calcScore(debug);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (proc != null) proc.destroy();
    }
    if (imageFileName != null) drawImage();
    return res;
  }

  private static String exec = "./tester";
  private static boolean vis, debug, labels;
  private static String imageFileName;
  private Process proc;
  private JFrame jf;
  private Vis v;
  private final int SZX = SZ + 2 + 100, SZY = SZ + 2;

  private int[] plot(int NV, int[] edges) throws IOException {
    OutputStream os = proc.getOutputStream();
    InputStream is = proc.getInputStream();
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();
    sb.append(NV).append('\n');
    sb.append(edges.length).append('\n');
    for (int edge : edges) {
      sb.append(edge).append('\n');
    }
    os.write(sb.toString().getBytes());
    os.flush();
    int nret = Integer.parseInt(br.readLine());
    int[] ret = new int[nret];
    for (int i = 0; i < nret; ++i)
      ret[i] = Integer.parseInt(br.readLine());
    return ret;
  }

  private void drawImage() {
    final int MARGIN = 10;
    final int MAG = 3;
    BufferedImage image = new BufferedImage(700 * MAG + MARGIN * 2, 700 * MAG + MARGIN * 2, BufferedImage.TYPE_INT_RGB);
    Graphics2D g = (Graphics2D) image.getGraphics();
    g.setColor(Color.WHITE);
    g.fillRect(0, 0, image.getWidth(), image.getHeight());
    g.translate(MARGIN, MARGIN);
    g.setColor(Color.LIGHT_GRAY);
    for (int i = 0; i <= 700; i += 100) {
      g.drawLine(i * MAG, 0, i * MAG, 700 * MAG);
      g.drawLine(0, i * MAG, 700 * MAG, i * MAG);
    }
    g.setColor(Color.BLACK);
    for (int i = 0; i < NV; i++) {
      g.fillOval(p[i].x * MAG - 2, p[i].y * MAG - 2, 4, 4);
    }
    g.setColor(Color.GRAY);
    for (int i = 0; i < NE; i++) {
      int x1 = p[edgeBeg[i]].x;
      int y1 = p[edgeBeg[i]].y;
      int x2 = p[edgeEnd[i]].x;
      int y2 = p[edgeEnd[i]].y;
      g.drawLine(x1 * MAG, y1 * MAG, x2 * MAG, y2 * MAG);
    }
    try {
      ImageIO.write(image, "png", new File(imageFileName));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void draw() {
    if (!vis) return;
    v.repaint();
  }

  private class Vis extends JPanel {
    public void paint(Graphics g) {
      try {
        char[] ch;
        BufferedImage bi = new BufferedImage(SZX + 10, SZY + 10, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) bi.getGraphics();
        // background
        g2.setColor(new Color(0xD3D3D3));
        g2.fillRect(0, 0, SZX + 10, SZY + 10);
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, SZ + 1, SZ + 1);
        // frame
        g2.setColor(Color.BLACK);
        g2.drawRect(0, 0, SZ + 1, SZ + 1);

        // get current score and ratios
        double score = calcScore(false);

        // edges that have given lengths (with ratios)
        for (int i = 0; i < NE; ++i) {
          // assign color based on ratio: longer than perfect are red, shorter are violet
          int c;
          if (ratios[i] >= 1.0) {
            if (max_ratio < 1.0 + 1E-5)
              c = 0;
            else
              c = 0x10000 * (int) ((ratios[i] - 1.0) / (max_ratio - 1.0) * 0xF0 + 0xF);
          } else {
            if (min_ratio > 1.0 - 1E-5)
              c = 0;
            else
              c = 0x10001 * (int) ((1.0 - ratios[i]) / (1.0 - min_ratio) * 0xF0 + 0xF);
          }
          g2.setColor(new Color(c));
          g2.drawLine(p[edgeBeg[i]].x, (SZ - 1 - p[edgeBeg[i]].y), p[edgeEnd[i]].x, (SZ - 1 - p[edgeEnd[i]].y));
          if (labels) {
            g2.setColor(Color.BLUE);
            ch = String.format("%.3f", ratios[i]).toCharArray();
            g2.drawChars(ch, 0, ch.length,
                (p[edgeBeg[i]].x + p[edgeEnd[i]].x) / 2 + 2, SZ - 1 - (p[edgeBeg[i]].y + p[edgeEnd[i]].y) / 2 - 2);
          }
        }

        // vertices (with numbers)
        g2.setColor(Color.BLACK);
        for (int i = 0; i < NV; i++) {
          g2.fillOval(p[i].x - 2, SZ - 1 - p[i].y - 2, 5, 5);
          if (labels) {
            ch = (i + "").toCharArray();
            g2.drawChars(ch, 0, ch.length, p[i].x + 2, SZ - 1 - p[i].y - 2);
          }
        }

        ch = String.format("%.6f", score).toCharArray();
        g2.setFont(new Font("Arial", Font.BOLD, 14));
        g2.drawChars(ch, 0, ch.length, SZ + 10, 200);

        g.drawImage(bi, 0, 0, SZX + 10, SZY + 10, null);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    private Vis() {
      jf.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
          if (proc != null) {
            try {
              proc.destroy();
            } catch (Exception ex) {
              ex.printStackTrace();
            }
          }
          System.exit(0);
        }
      });
    }
  }

  private Tester() throws Exception {
    if (vis) {
      jf = new JFrame();
      v = new Vis();
      jf.getContentPane().add(v);
    }
  }

  private static final int THREAD_COUNT = 2;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-exec")) exec = args[++i];
      if (args[i].equals("-vis")) vis = true;
      if (args[i].equals("-debug")) debug = true;
      if (args[i].equals("-labels")) labels = true;
      if (args[i].equals("-image")) imageFileName = args[++i];
      if (args[i].equals("-V")) USER_V = Integer.parseInt(args[++i]);
      if (args[i].equals("-E")) USER_E = Integer.parseInt(args[++i]);
      if (args[i].equals("-P")) USER_P = Integer.parseInt(args[++i]);
    }
    if (begin != -1 && end != -1) {
      vis = false;
      ArrayList<Long> seeds = new ArrayList<>();
      for (long i = begin; i <= end; ++i) {
        seeds.add(i);
      }
      int len = seeds.size();
      Result[] results = new Result[len];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      int prev = 0;
      for (int i = 0; i < THREAD_COUNT; ++i) {
        int next = len * (i + 1) / THREAD_COUNT;
        threads[i] = new TestThread(prev, next - 1, seeds, results);
        prev = next;
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / results.length));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    int begin, end;
    ArrayList<Long> seeds;
    Result[] results;

    TestThread(int begin, int end, ArrayList<Long> seeds, Result[] results) {
      this.begin = begin;
      this.end = end;
      this.seeds = seeds;
      this.results = results;
    }

    public void run() {
      for (int i = begin; i <= end; ++i) {
        try {
          Tester f = new Tester();
          Result res = f.runTest(seeds.get(i));
          results[i] = res;
        } catch (Exception e) {
          e.printStackTrace();
          results[i] = new Result();
          results[i].seed = seeds.get(i);
        }
      }
    }
  }

  private static class Result {
    long seed;
    int V, E, P;
    double score;
    long elapsed;

    public String toString() {
      String ret = String.format("seed:%4d\n", seed);
      ret += String.format("V:%3d E:%4d P:%2d\n", V, E, P);
      ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
      ret += String.format("score:%.7f", score);
      return ret;
    }
  }
}

class ErrorReader extends Thread {
  private InputStream error;

  ErrorReader(InputStream is) {
    error = is;
  }

  public void run() {
    try {
      byte[] ch = new byte[50000];
      int read;
      while ((read = error.read(ch)) > 0) {
        String s = new String(ch, 0, read);
        System.err.print(s);
        System.err.flush();
      }
    } catch (Exception e) {
      // do nothing
    }
  }
}
