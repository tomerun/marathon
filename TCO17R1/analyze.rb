#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:V, :E, :P, :elapsed, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /V: *(\d+) E: *(\d+) P: *(\d+)/
      testcase.V = $1.to_i
      testcase.E = $2.to_i
      testcase.P = $3.to_i
    elsif line =~ /elapsed:(.+)/
      testcase.elapsed = $1.to_f
    elsif line =~ /score:(.+)/
      testcase.score = $1.to_f
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, filter)
  sum_score_diff = 0
  sum_score_diff_relative = 0
  sum_score = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    sum_score += s2
    sum_score_diff += s2 - s1
    count += 1
    if s1 < s2
      win += 1
    elsif s1 > s2
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff / count)
  puts sprintf("  ave :%.6f", sum_score / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  1.step(1000, 100) do |v|
    puts "V:#{v}-#{v + 100 - 1}"
    calc(from, to, proc { |tc| tc[0].V.between?(v, v + 100 - 1) })
  end
  puts "---------------------"
  1.step(9, 1) do |e|
    puts "E/V:#{e}-#{e + 1}"
    calc(from, to, proc { |tc| (tc[0].E + 1).between?(tc[0].V * e, tc[0].V * (e + 1) - 1) })
  end
  puts "---------------------"
  0.step(99, 10) do |p|
    puts "P:#{p}-#{p + 10 - 1}"
    calc(from, to, proc { |tc| tc[0].P.between?(p, p + 10 - 1) })
  end
  puts "---------------------"
  puts "total"
  calc(from, to, proc { |tc| true })
end

main
