
window.onload = () => {
	'use strict';

	const lineToInts = (line) => line.split(' ').map(e => parseInt(e, 10));

	class Pair {
		constructor(r1, c1, r2, c2) {
			this.r1 = r1;
			this.c1 = c1;
			this.r2 = r2;
			this.c2 = c2;
		}
	}

	class State {
		constructor(h, w, c, board, pairs) {
			this.H = h;
			this.W = w;
			this.C = c;
			this.board = board;
			this.origBoard = Array.from(board, row => Array.from(row));
			this.pairs = pairs;
			this.position = 0;
		}
	}

	const EMPTY = -1;
	let state;

	// initialize

	async function getText(elem) {
		if (elem.files.length === 0) {
			console.log('file is not specified.');
			return null;
		}
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.onload = (event) => {
				resolve(reader.result);
			};
			reader.onerror = (event) => {
				reject("fail to read from file");
			};
			reader.readAsText(elem.files[0]);
		});
	}

	function parseInput(inputData) {
		const lines = inputData.trim().split(/\r?\n/);
		const l1 = lineToInts(lines[0]);
		const H = l1[0];
		const W = l1[1];
		const C = l1[2];
		const board = lines.slice(1, H + 1).map(line => Array.from(line, cell => cell.charCodeAt(0) - 0x30));
		const pi = parseInt(lines[H + 1], 10)
		const pairs = lines.slice(H + 2, H + pi + 2).map(line => new Pair(...lineToInts(line)));
		return new State(H, W, C, board, pairs);
	}

	const colors = ["#21618C", "#922b21", "#9a7d0a", "#85c1e9", "#e6b0aa", "#f7dc6f"];

	function paint(state) {
		const canvas = document.getElementById('canvas');
		const ctx = canvas.getContext('2d');
		const cellSize = Math.min(canvas.height / state.H, canvas.width / state.W, 30);
		const pair = state.pairs[state.position];
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		for (let r = 0; r < state.H; ++r) {
			for (let c = 0; c < state.W; ++c) {
				if (state.board[r][c] == EMPTY) continue;
				ctx.fillStyle = colors[state.origBoard[r][c]];
				ctx.fillRect(c * cellSize, r * cellSize, cellSize, cellSize);
			}
		}
		ctx.strokeStyle = 'gray';
		ctx.beginPath();
		for (let r = 0; r <= state.H; ++r) {
			ctx.moveTo(0, r * cellSize);
			ctx.lineTo(state.W * cellSize, r * cellSize);
		}
		for (let c = 0; c <= state.W; ++c) {
			ctx.moveTo(c * cellSize, 0);
			ctx.lineTo(c * cellSize, state.H * cellSize);
		}
		ctx.stroke();
		ctx.strokeStyle = 'black';
		ctx.beginPath();
		for (let r = 0; r <= state.H; r += 5) {
			ctx.moveTo(0, r * cellSize);
			ctx.lineTo(state.W * cellSize, r * cellSize);
		}
		for (let c = 0; c <= state.W; c += 5) {
			ctx.moveTo(c * cellSize, 0);
			ctx.lineTo(c * cellSize, state.H * cellSize);
		}
		ctx.stroke();
		ctx.strokeStyle = 'red';
		ctx.lineWidth = 8.0;
		ctx.strokeRect(pair.c1 * cellSize + 1, pair.r1 * cellSize + 1, cellSize - 1, cellSize - 1);
		ctx.strokeRect(pair.c2 * cellSize + 1, pair.r2 * cellSize + 1, cellSize - 1, cellSize - 1);
		ctx.lineWidth = 1.0;
	}

	// event handler

	document.getElementsByName('run')[0].onclick = () => {
		(async function() {
			const logData = getText(document.getElementsByName('input')[0]);
			return await logData;
		})().then((v) => {
			state = parseInput(v);
			state.position = -1;
			const spinner = document.getElementsByName('frame_pos')[0];
			spinner.max = state.pairs.length - 1;
			setFrame(0);
		})
	};

	const setFrame = (position) => {
		if (position !== state.position) {
			for (let i = 0; i < state.H; ++i) {
				state.board[i] = Array.from(state.origBoard[i]);
			}
			for (let i = 0; i < position; ++i) {
				const p = state.pairs[i];
				state.board[p.r1][p.c1] = EMPTY;
				state.board[p.r2][p.c2] = EMPTY;
			}
			const spinner = document.getElementsByName('frame_pos')[0];
			spinner.value = position;
			state.position = position;
			paint(state);
		}
	};

	const moveFrameFactory = (step) => {
		return () => {
			stopAnimation();
			let next = state.position + step;
			next = Math.max(0, next);
			next = Math.min(state.pairs.length - 1, next);
			setFrame(next);
		};
	};

	document.getElementsByName('show_first')[0].onclick = moveFrameFactory(-9999);
	document.getElementsByName('show_prev')[0].onclick = moveFrameFactory(-1);
	document.getElementsByName('show_next')[0].onclick = moveFrameFactory(1);
	document.getElementsByName('show_last')[0].onclick = moveFrameFactory(9999);

	const getDelay = () => {
		const speed = document.getElementsByName('speed')[0].value;
		return 1000 / speed;
	}

	let timerId = -1;

	function stopAnimation() {
		clearTimeout(timerId);
		timerId = -1;
		document.getElementsByName('play')[0].innerText = '▶️';
	}

	const moveForwardAnimation = () => {
		if (state.position == state.pairs.length - 1) {
			stopAnimation();
			return;
		}
		setFrame(state.position + 1);
		timerId = setTimeout(moveForwardAnimation, getDelay());
	}

	document.getElementsByName('play')[0].onclick = () => {
		if (timerId == -1) {
			timerId = setTimeout(moveForwardAnimation, getDelay());
			document.getElementsByName('play')[0].innerText = '■';
		} else {
			stopAnimation();
		}
	};

	document.getElementsByName('frame_pos')[0].onchange = (ev) => {
		try {
			const pos = parseInt(ev.target.value, 10);
			setFrame(pos);
		} catch {
			console.log(`invalid position:${ev.target.value}`);
		}
	}

};
