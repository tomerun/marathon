import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

class Pair {
  int r1, c1, r2, c2;
  char col;

  Pair(int R1, int C1, int R2, int C2, char Col) {
    r1 = R1;
    r2 = R2;
    c1 = C1;
    c2 = C2;
    col = Col;
  }
}

public class Tester {
  private static final int minS = 10, maxS = 100;
  private static final int minC = 2, maxC = 6;
  private int H, W, C;
  private static int myH, myW, myC;
  private char[][] board;
  private ArrayList<Pair> removed;
  private static FileWriter logFile;

  private boolean isInside(int r, int c) {
    return (r >= 0 && r < H && c >= 0 && c < W);
  }

  private void generate(long seed) throws Exception {
    SecureRandom r1 = SecureRandom.getInstance("SHA1PRNG");
    r1.setSeed(seed);
    H = r1.nextInt(maxS - minS + 1) + minS;
    W = r1.nextInt(maxS - minS + 1) + minS;
    if ((W * H) % 2 == 1) {
      if (r1.nextInt(2) == 0)
        W++;
      else
        H++;
    }
    if (seed == 1)
      W = H = 10;
    else if (seed == 2)
      W = H = 30;
    else if (seed == 3)
      W = H = 100;
    if (myH != 0) H = myH;
    if (myW != 0) W = myW;
    C = r1.nextInt(maxC - minC + 1) + minC;
    if (seed <= 3) {
      C = (int) seed * 2;
    }
    if (myC != 0) C = myC;
    if (1000 <= seed && seed < 2000) {
      C = (int) (seed - 1000) / 200 + 2;
      ArrayList<Integer> l = new ArrayList<>();
      for (int i = minS; i <= maxS; i++) {
        for (int j = minS; j <= maxS; j++) {
          if (i * j % 2 != 0) continue;
          l.add(((i * j) << 16) | (i << 8) | j);
        }
      }
      Collections.sort(l);
      for (int i = 0; i < C - 2; i++) {
        l.remove(0);
      }
      int idx = (int) (seed % 200) * (l.size() - 1) / 200;
      H = (l.get(idx) >> 8) & 0xFF;
      W = l.get(idx) & 0xFF;
    }
    if (vis) SZ = Math.min(860 / H, 1200 / W);

    board = new char[H][W];
    while (true) {
      int[] ncols = new int[C];
      for (int i = 0; i < H; i++)
        for (int j = 0; j < W; j++) {
          int c = r1.nextInt(C);
          board[i][j] = (char) ((int) '0' + c);
          ncols[c]++;
        }
      for (int i = 0; i < C; ++i)
        if (ncols[i] % 2 == 1) {
          for (int j = i + 1; j < C; ++j)
            if (ncols[j] % 2 == 1) {
              for (int r = 0; r < H && ncols[j] % 2 == 1; ++r)
                for (int c = 0; c < W; ++c)
                  if (board[r][c] == (char) ((int) '0' + j)) {
                    ncols[j]--;
                    ncols[i]++;
                    board[r][c] = (char) ((int) '0' + i);
                    break;
                  }
            }
        }

      boolean allCols = true;
      for (int i = 0; i < C; ++i)
        allCols &= (ncols[i] > 0);
      if (allCols) break;
    }
    if (logFile != null) {
      logFile.write(H + " " + W + " " + C + "\n");
      for (int i = 0; i < H; i++) {
        logFile.write(String.valueOf(board[i]) + "\n");
      }
    }
  }

  private double getScore() {
    return removed.size() * 2.0 / (H * W);
  }

  private void removePair(int r1, int c1, int r2, int c2) {
    if (board[r1][c1] == '.' || board[r2][c2] == '.')
      throw new RuntimeException("Both tiles you're trying to remove must still be on the board.");
    if (board[r1][c1] != board[r2][c2])
      throw new RuntimeException("Both tiles you're trying to remove must have the same color.");
    if (r1 == r2 && c1 == c2)
      throw new RuntimeException("The tiles you're trying to remove must be distinct.");
    char c = board[r1][c1];
    for (int i = Math.min(r1, r2); i <= Math.max(r1, r2); ++i)
      for (int j = Math.min(c1, c2); j <= Math.max(c1, c2); ++j)
        if (board[i][j] != c && board[i][j] != '.')
          throw new RuntimeException("The rectangle must not contain tiles of any other color.");
    removed.add(new Pair(r1, c1, r2, c2, board[r1][c1]));
    board[r1][c1] = board[r2][c2] = '.';
  }

  private Result runTest(long seed) throws Exception {
    generate(seed);
    Result res = new Result();
    res.seed = seed;
    res.H = H;
    res.W = W;
    res.C = C;
    removed = new ArrayList<>();
    if (vis) {
      jf.setSize((W + 3) * SZ + 50, H * SZ + 40);
      jf.setVisible(true);
      draw();
    }

    String[] boardArg = new String[H];
    for (int i = 0; i < H; ++i) {
      boardArg[i] = new String(board[i]);
    }
    long startTime = System.currentTimeMillis();
    String[] moves = removePairs(boardArg);
    res.elapsed = System.currentTimeMillis() - startTime;
    if (moves == null) {
      throw new RuntimeException("Your return contained invalid number of elements.");
    }
    if (moves.length > W * H / 2) {
      throw new RuntimeException("Your return contained more than " + (W * H / 2) + " elements.");
    }

    for (int i = 0; i < moves.length; ++i) {
      String[] s = moves[i].split(" ");
      if (s.length != 4) {
        throw new RuntimeException("Move " + i + ": Each element of your return must be formatted as \"R1 C1 R2 C2\"");
      }
      int R1 = Integer.parseInt(s[0]);
      int C1 = Integer.parseInt(s[1]);
      int R2 = Integer.parseInt(s[2]);
      int C2 = Integer.parseInt(s[3]);
      if (!isInside(R1, C1)) {
        throw new RuntimeException("Move " + i + ": R1 and C1 in each element of your return must specify a cell within the board.");
      }
      if (!isInside(R2, C2)) {
        throw new RuntimeException("Move " + i + ": R2 and C2 in each element of your return must specify a cell within the board.");
      }
      removePair(R1, C1, R2, C2);
      if (vis) draw();
    }
    res.score = H * W - removed.size() * 2;
    if (logFile != null) {
      logFile.write(removed.size() + "\n");
      for (Pair p : removed) {
        logFile.write(p.r1 + " " + p.c1 + " " + p.r2 + " " + p.c2 + "\n");
      }
    }
    return res;
  }

  private JFrame jf;
  private Vis v;
  private static boolean vis = false;
  private Process proc;
  private static int delay = 20;
  private static int SZ = 20;

  private String[] removePairs(String[] board) throws IOException {
    proc = Runtime.getRuntime().exec("./tester");
    new ErrorReader(proc.getErrorStream()).start();
    OutputStream os = proc.getOutputStream();
    InputStream is = proc.getInputStream();
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    try {
      StringBuilder sb = new StringBuilder();
      sb.append(H).append("\n");
      for (int i = 0; i < H; ++i) {
        sb.append(board[i]).append("\n");
      }
      os.write(sb.toString().getBytes());
      os.flush();
      int N = Integer.parseInt(br.readLine());
      String[] ret = new String[N];
      for (int i = 0; i < N; i++)
        ret[i] = br.readLine();
      return ret;
    } finally {
      os.close();
      is.close();
      br.close();
      proc.destroy();
      proc = null;
    }
  }

  private void draw() throws Exception {
    v.repaint();
    Thread.sleep(delay);
  }

  private final static int[] colors = {0x21618C, 0x922b21, 0x9a7d0a, 0x85c1e9, 0xe6b0aa, 0xf7dc6f};
  private BufferedImage cacheBoard;
  private char[][] cache;

  private void drawStartingBoard() {
    cacheBoard = new BufferedImage(W * SZ + 120, H * SZ + 40, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) cacheBoard.getGraphics();
    g2.setColor(new Color(0xDDDDDD));
    g2.fillRect(0, 0, W * SZ + 120, H * SZ + 40);
    g2.setColor(Color.WHITE);
    g2.fillRect(0, 0, W * SZ, H * SZ);

    cache = new char[H][W];
    for (int i = 0; i < H; ++i)
      for (int j = 0; j < W; ++j) {
        cache[i][j] = board[i][j];
        if (board[i][j] != '.') {
          g2.setColor(new Color(colors[board[i][j] - '0']));
          g2.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
        }
      }

    g2.setColor(Color.BLACK);
    for (int i = 0; i <= H; i++)
      g2.drawLine(0, i * SZ, W * SZ, i * SZ);
    for (int i = 0; i <= W; i++)
      g2.drawLine(i * SZ, 0, i * SZ, H * SZ);

    g2.setFont(new Font("Arial", Font.BOLD, 14));
    g2.drawString("READY", SZ * W + 25, 30);
    g2.drawRect(SZ * W + 12, 10, 70, 30);
    g2.drawString("UNDO", SZ * W + 25, 70);
    g2.drawRect(SZ * W + 12, 50, 70, 30);
  }

  private static BufferedImage deepCopy(BufferedImage source) {
    BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
    Graphics g = b.getGraphics();
    g.drawImage(source, 0, 0, null);
    g.dispose();
    return b;
  }

  private class Vis extends JPanel {
    public void paint(Graphics g) {
      if (cacheBoard == null) {
        drawStartingBoard();
      }
      Graphics2D g2 = (Graphics2D) cacheBoard.getGraphics();
      for (int i = 0; i < H; ++i)
        for (int j = 0; j < W; ++j)
          if (cache[i][j] != board[i][j]) {
            cache[i][j] = board[i][j];
            g2.setColor(cache[i][j] == '.' ? Color.WHITE : new Color(colors[cache[i][j] - '0']));
            g2.fillRect(j * SZ + 1, i * SZ + 1, SZ - 1, SZ - 1);
          }
      g2.dispose();

      BufferedImage bi = deepCopy(cacheBoard);
      g2 = (Graphics2D) bi.getGraphics();
      g2.setColor(Color.BLACK);
      g2.setFont(new Font("Arial", Font.BOLD, 14));
      FontMetrics fm = g2.getFontMetrics();
      g2.drawString(String.format("%.4f", getScore()), W * SZ + 25, 110 + fm.getHeight());
      g.drawImage(bi, 0, 0, W * SZ + 120, H * SZ + 40, null);
    }

    Vis() {
      addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
        }
      });
      jf.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
          if (proc != null) proc.destroy();
          System.exit(0);
        }
      });
    }
  }

  private Tester() {
    if (vis) {
      jf = new JFrame();
      v = new Vis();
      jf.getContentPane().add(v);
    }
  }

  private static final int THREAD_COUNT = 2;

  public static void main(String[] args) throws Exception {
    long seed = 1, begin = -1, end = -1;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-seed")) seed = Long.parseLong(args[++i]);
      if (args[i].equals("-b")) begin = Long.parseLong(args[++i]);
      if (args[i].equals("-e")) end = Long.parseLong(args[++i]);
      if (args[i].equals("-H")) myH = Integer.parseInt(args[++i]);
      if (args[i].equals("-W")) myW = Integer.parseInt(args[++i]);
      if (args[i].equals("-C")) myC = Integer.parseInt(args[++i]);
      if (args[i].equals("-delay")) delay = Integer.parseInt(args[++i]);
      if (args[i].equals("-vis")) vis = true;
      if (args[i].equals("-size")) SZ = Integer.parseInt(args[++i]);
      if (args[i].equals("-log")) {
        if (args.length < i + 1 && !args[i + 1].startsWith("-")) {
          logFile = new FileWriter(new File(args[++i]));
        } else {
          logFile = new FileWriter(new File(seed + ".log"));
        }
      }
    }
    if (begin != -1 && end != -1) {
      vis = false;
      BlockingQueue<Long> q = new ArrayBlockingQueue<>((int)(end - begin + 1));
      for (long i = begin; i <= end; ++i) {
        q.add(i);
      }
      Result[] results = new Result[(int) (end - begin + 1)];
      TestThread[] threads = new TestThread[THREAD_COUNT];
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i] = new TestThread(q, results, begin);
        threads[i].start();
      }
      for (int i = 0; i < THREAD_COUNT; ++i) {
        threads[i].join();
      }
      double sum = 0;
      for (Result result : results) {
        System.out.println(result);
        System.out.println();
        sum += result.score;
      }
      System.out.println("ave:" + (sum / results.length));
    } else {
      Tester tester = new Tester();
      Result res = tester.runTest(seed);
      if (logFile != null) {
        logFile.close();
      }
      System.out.println(res);
    }
  }

  private static class TestThread extends Thread {
    long seedMin;
    Result[] results;
    BlockingQueue<Long> q;

    TestThread(BlockingQueue<Long> q, Result[] results, long seedMin) {
      this.q = q;
      this.results = results;
      this.seedMin = seedMin;
    }

    public void run() {
      while (true) {
        Long seed = q.poll();
        if (seed == null) break;
        try {
          Tester f = new Tester();
          Result res = f.runTest(seed);
          results[(int)(seed - seedMin)] = res;
        } catch (Exception e) {
          e.printStackTrace();
          Result res = new Result();
          res.seed = seed;
          res.score = 9999;
          results[(int)(seed - seedMin)] = res;
        }
      }
    }
  }
}

class Result {
  long seed;
  int C, H, W;
  int score;
  long elapsed;

  public String toString() {
    String ret = String.format("seed:%4d\n", seed);
    ret += String.format("H:%3d W:%3d C:%1d\n", H, W, C);
    ret += String.format("elapsed:%.4f\n", elapsed / 1000.0);
    ret += String.format("score:%4d", score);
    return ret;
  }
}

class ErrorReader extends Thread {
  private InputStream error;

  ErrorReader(InputStream is) {
    error = is;
  }

  public void run() {
    try {
      byte[] ch = new byte[50000];
      int read;
      while ((read = error.read(ch)) > 0) {
        String s = new String(ch, 0, read);
        System.err.print(s);
        System.err.flush();
      }
    } catch (Exception e) {
      // do nothing
    }
  }
}
