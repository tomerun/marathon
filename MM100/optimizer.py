from bayes_opt import BayesianOptimization
import json
import re
import subprocess


RE_SCORE = re.compile(r'score:\s*(\d+)')


def execute(**kwargs):
    opt = ' '.join(f'-D{k.upper()}={v}' for k, v in kwargs.items())
    cmd = f'g++48 -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL {opt} '\
          '-pipe -mmmx -msse -msse2 -msse3 -o tester SameColorPairs.cpp'
    res = subprocess.run(cmd.split(' '))
    if res.returncode != 0:
        raise RuntimeError(str(res))
    with open('result.txt', 'w') as f:
        subprocess.run('java Tester -b 10000 -e 19999'.split(' '), stdout=f)
    with open('result.txt', 'r') as f:
        log = f.read()
        return len([match
                   for match
                   in re.finditer(RE_SCORE, log)
                   if match[1] == '0'])


param_bounds = {'min_beam_size': (10, 300),
                'beam_size': (50000, 2000000),
                'rep_single': (2, 10),
                'add_bonus': (0, 2000),
                'start_min_ratio': (0.0, 0.9),
                'start_max_ratio': (0.1, 0.9),
                'end_min_ratio': (0.0, 0.9),
                'end_max_ratio': (0.1, 0.9)}
optimizer = BayesianOptimization(f=execute, pbounds=param_bounds)
optimizer.explore({'min_beam_size': [100],
                   'beam_size': [200000],
                   'rep_single': [3],
                   'add_bonus': [300],
                   'start_min_ratio': [0],
                   'start_max_ratio': [0.75],
                   'end_min_ratio': [0],
                   'end_max_ratio': [0.75]})
optimizer.maximize(init_points=5, n_iter=5, acq='ei')

print(json.dumps(optimizer.res['max'], indent=2))
print(json.dumps(optimizer.res['all'], indent=2))
