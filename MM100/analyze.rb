#!/usr/bin/ruby

if ARGV.length < 2
  p "need 2 arguments"
  exit
end

TestCase = Struct.new(:H, :W, :C, :elapsed, :score)

def get_scores(filename)
  scores = []
  seed = 0;
  testcase = nil
  IO.foreach(filename) do |line|
    if line =~ /seed: *(\d+)/
      testcase = TestCase.new
      seed = $1.to_i
    elsif line =~ /H: *(\d+) W: *(\d+) C: *(\d+)/
      testcase.H = $1.to_i
      testcase.W = $2.to_i
      testcase.C = $3.to_i
    elsif line =~ /elapsed:(.+)/
      testcase.elapsed = $1.to_f
    elsif line =~ /score:(.+)/
      testcase.score = $1.to_i
      scores[seed] = testcase
      testcase = nil
    end
  end
  return scores.compact
end

def calc(from, to, &filter)
  sum_score_diff = 0.0
  sum_score_diff_relative = 0
  count = 0
  win = 0
  lose = 0
  list = from.zip(to).select(&filter)
  return if list.empty?
  list.each do |pair|
    next unless pair[0] && pair[1]
    s1 = pair[0].score
    s2 = pair[1].score
    sum_score_diff += s2 - s1
    count += 1
    if s1 > s2
      win += 1
    elsif s1 < s2
      lose += 1
    else
      #
    end
  end
  puts sprintf("  win:%d lose:%d tie:%d", win, lose, count - win - lose)
  puts sprintf("  diff:%.6f", sum_score_diff / count)
end

def main
  from = get_scores(ARGV[0])
  to = get_scores(ARGV[1])

  1.step(10000, 1000) do |param|
    puts "S:#{param}-#{param + 999}"
    calc(from, to) { |from, to| (from.H * from.W).between?(param, param + 999) }
  end
  puts "---------------------"
  2.upto(6) do |param|
    puts "C:#{param}"
    calc(from, to) { |from, to| from.C == param }
  end
  puts "---------------------"
  puts "total"
  calc(from, to) {|from, to| true }
end

main
