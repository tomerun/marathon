#include <algorithm>
#include <utility>
#include <vector>
#include <unordered_set>
#include <bitset>
#include <string>
#include <sstream>
#include <iostream>
#include <array>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
// #define DEBUG
#define NDEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 3000;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 9800;
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

template<class T>
inline T sq(T v) { return v * v; }

XorShift rnd;
Timer timer;
Counter counter;
const int DR[4] = {1, 0, -1, 0};
const int DC[4] = {0, 1, 0, -1};
const int8_t EMPTY = -1;

//////// end of template ////////

struct Removal {
	int32_t r1, c1, r2, c2;
};

int H, W;
int8_t field[100][100];
vi rnd_pos;
int left_pos[10000];
int unused_count[100][100];
vector<Removal> rem_buf;
uint64_t cell_hash[100][100];
vector<unordered_set<uint64_t>> exist_hash;
int adj_vert[100][100];
int top_prev[100];


inline void set_exist(int r, int c) {
	adj_vert[r][c] = top_prev[c];
	if (top_prev[c] != 0xFF) {
		adj_vert[top_prev[c]][c] |= r << 8;
	}
	top_prev[c] = r;
}

void remove_exist(int r, int c) {
	int top = adj_vert[r][c] & 0xFF;
	int bottom = adj_vert[r][c] >> 8;
	if (top != 0xFF) {
		adj_vert[top][c] = (adj_vert[top][c] & 0xFF) | (bottom << 8);
	}
	if (bottom != 0) {
		adj_vert[bottom][c] = (adj_vert[bottom][c] & 0xFF00) | top;
	}
}

void restore_exist(int r, int c) {
	int top = adj_vert[r][c] & 0xFF;
	int bottom = adj_vert[r][c] >> 8;
	if (top != 0xFF) {
		adj_vert[top][c] = (adj_vert[top][c] & 0xFF) | (r << 8);
	}
	if (bottom != 0) {
		adj_vert[bottom][c] = (adj_vert[bottom][c] & 0xFF00) | r;
	}
}

template<int width>
struct UsedManager {
};

template<>
struct UsedManager<0> {
	vector<uint64_t> used;

	UsedManager() : used(H) {}

	bool is_used(int r, int c) const {
		return used[r] & (1ull << c);
	}

	void set_used(int r, int c) {
		used[r] |= (1ull << c);
	}

	void clear_used(int r, int c) {
		used[r] &= ~(1ull << c);
	}

	void clear_used() {
		for (int i = 0; i < H; ++i) {
			used[i] = 0;
		}
	}

	int next_right(int r, int c) const {
		uint64_t v = ~(used[r] | ((1ull << c) - 1));
		return __builtin_ctzll(v);
	}

	void set_left_pos() const {
		int idx = 0;
		for (int i = 0; i < H; ++i) {
			uint64_t v = ~used[i];
			while (true) {
				int c = __builtin_ctzll(v);
				if (c >= W) break;
				left_pos[idx++] = (i << 8) | c;
				set_exist(i, c);
				v &= v - 1;
			}
		}
	}
};

template<>
struct UsedManager<1> {
	vector<uint64_t> used;

	UsedManager() : used(H) {}

	bool is_used(int r, int c) const {
		return used[r] & (1ull << c);
	}

	void set_used(int r, int c) {
		used[r] |= (1ull << c);
	}

	void clear_used(int r, int c) {
		used[r] &= ~(1ull << c);
	}

	void clear_used() {
		for (int i = 0; i < H; ++i) {
			used[i] = 0;
		}
	}

	int next_right(int r, int c) const {
		uint64_t v = ~(used[r] | ((1ull << c) - 1));
		return v == 0 ? 64 : __builtin_ctzll(v);
	}

	void set_left_pos() const {
		int idx = 0;
		for (int i = 0; i < H; ++i) {
			uint64_t v = ~used[i];
			while (v != 0) {
				int c = __builtin_ctzll(v);
				if (c >= W) break;
				left_pos[idx++] = (i << 8) | c;
				set_exist(i, c);
				v &= v - 1;
			}
		}
	}
};

template<>
struct UsedManager<2> {
	vector<array<uint64_t, 2>> used;

	UsedManager() : used(H) {}

	bool is_used(int r, int c) const {
		return used[r][c >> 6] & (1ull << (c & 63));
	}

	void set_used(int r, int c) {
		used[r][c >> 6] |= (1ull << (c & 63));
	}

	void clear_used(int r, int c) {
		used[r][c >> 6] &= ~(1ull << (c & 63));
	}

	void clear_used() {
		for (int i = 0; i < H; ++i) {
			used[i][0] = used[i][1] = 0;
		}
	}

	int next_right(int r, int c) const {
		if (c <= 63) {
			uint64_t v = ~(used[r][0] | ((1ull << c) - 1));
			if (v != 0) {
				return __builtin_ctzll(v);
			}
			return __builtin_ctzll(~used[r][1]) + 64;
		}
		uint64_t v = ~(used[r][1] | ((1ull << (c - 64)) - 1));
		return __builtin_ctzll(v) + 64;
	}

	void set_left_pos() const {
		int idx = 0;
		for (int i = 0; i < H; ++i) {
			uint64_t v = ~used[i][0];
			while (v != 0) {
				int c = __builtin_ctzll(v);
				left_pos[idx++] = (i << 8) | c;
				set_exist(i, c);
				v &= v - 1;
			}
			v = ~used[i][1];
			while (v != 0) {
				int c = __builtin_ctzll(v) + 64;
				if (c >= W) break;
				left_pos[idx++] = (i << 8) | c;
				set_exist(i, c);
				v &= v - 1;
			}
		}
	}
};

template<int width>
struct Solver {
UsedManager<width> used_manager;

int solve_single() {
	used_manager.clear_used();
	fill(&adj_vert[0][0], &adj_vert[0][W], (1 << 8) | 0xFF);
	for (int i = 1; i < H - 1; ++i) {
		fill(&adj_vert[i][0], &adj_vert[i][W], ((i + 1) << 8) | (i - 1));
	}
	fill(&adj_vert[H - 1][0], &adj_vert[H - 1][W], H - 2);
	int rem_count = 0;
	int rnd_pos_count = H * W;
	while (true) {
		bool update = false;
		for (int i = 0; i < rnd_pos_count; ++i) {
			int r = rnd_pos[i] >> 8;
			int c = rnd_pos[i] & 0xFF;
			bool found = false;
			if (used_manager.is_used(r, c)) {
				--rnd_pos_count;
				swap(rnd_pos[i], rnd_pos[rnd_pos_count]);
				--i;
				continue;
			}
			int top = adj_vert[r][c] & 0xFF;
			if (top != 0xFF && field[top][c] == field[r][c]) {
				rem_buf[rem_count].r2 = top;
				rem_buf[rem_count].c2 = c;
				found = true;
			}
			if (!found) {
				int bottom = adj_vert[r][c] >> 8;
				if (bottom != 0 && field[bottom][c] == field[r][c]) {
					rem_buf[rem_count].r2 = bottom;
					rem_buf[rem_count].c2 = c;
					found = true;
				}
			}
			if (!found && c < W - 1) {
				if (!used_manager.is_used(r, c + 1)) {
					if (field[r][c+1] == field[r][c]) {
						rem_buf[rem_count].r2 = r;
						rem_buf[rem_count].c2 = c + 1;
						found = true;
					}
				} else {
					int right = used_manager.next_right(r, c + 1);
					if (right < W && field[r][right] == field[r][c]) {
						rem_buf[rem_count].r2 = r;
						rem_buf[rem_count].c2 = right;
						found = true;
					}
					if (!found) {
						int prev = right - 1;
						for (int y = r + 1; y < H; ++y) {
							int x = used_manager.next_right(y, c);
							if (x > prev) continue;
							if (field[r][c] == field[y][x]) {
								rem_buf[rem_count].r2 = y;
								rem_buf[rem_count].c2 = x;
								found = true;
								break;
							} else if (x == c) {
								break;
							} else {
								prev = x - 1;
							}
						}
					}
					if (!found) {
						int prev = right - 1;
						for (int y = r - 1; y >= 0; --y) {
							int x = used_manager.next_right(y, c);
							if (x > prev) continue;
							if (field[r][c] == field[y][x]) {
								rem_buf[rem_count].r2 = y;
								rem_buf[rem_count].c2 = x;
								found = true;
								break;
							} else if (x == c) {
								break;
							} else {
								prev = x - 1;
							}
						}
					}
				}
			}
			if (found) {
				rem_buf[rem_count].r1 = r;
				rem_buf[rem_count].c1 = c;
				used_manager.set_used(r, c);
				used_manager.set_used(rem_buf[rem_count].r2, rem_buf[rem_count].c2);
				remove_exist(r, c);
				remove_exist(rem_buf[rem_count].r2, rem_buf[rem_count].c2);
				++rem_count;
				update = true;
				--rnd_pos_count;
				swap(rnd_pos[i], rnd_pos[rnd_pos_count]);
				--i;
			}
		}
		if (!update) break;
	}
	return rem_count;
}

vector<Removal> solve_random() {
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			rnd_pos.push_back((i << 8) | j);
		}
	}
	rem_buf.resize(H * W / 2);
	vector<Removal> best;
	vi best_rnd_pos = rnd_pos;
	array<int, 8> change_pos;
	const int INITIAL_COUNT = H * W < 1000 ? 1000 : 500;
	int mask = 1;
	while (mask < 40000 / (H * W)) {
		mask *= 2;
	}
	mask--;
	for (int turn = 1; ; ++turn) {
		if ((turn & mask) == 0 && get_elapsed_msec() > TL / 16) {
			debug("turn:%d\n", turn);
			break;
		}
		if (turn >= INITIAL_COUNT) {
			for (int i = 0; i < change_pos.size(); i += 2) {
				change_pos[i] = rnd.nextUInt(best_rnd_pos.size());
				change_pos[i + 1] = rnd.nextUInt(best_rnd_pos.size());
				swap(best_rnd_pos[change_pos[i]], best_rnd_pos[change_pos[i + 1]]);
			}
			rnd_pos = best_rnd_pos;
		} else {
			for (int i = 0; i < rnd_pos.size(); ++i) {
				int p = rnd.nextUInt(H * W - i) + i;
				swap(rnd_pos[i], rnd_pos[p]);
			}
		}
		int score = solve_single();
		if (turn < INITIAL_COUNT) {
			for (int i = 0; i < H * W - score * 2; ++i) {
				int r = rnd_pos[i] >> 8;
				int c = rnd_pos[i] & 0xFF;
				unused_count[r][c]++;
			}
		}
		if (score > best.size()) {
			swap(best_rnd_pos, rnd_pos);
			best.assign(rem_buf.begin(), rem_buf.begin() + score);
			debug("turn:%d score:%lu\n", turn, H * W - best.size() * 2);
			if (best.size() * 2 == H * W) break;
		} else {
			if (turn >= INITIAL_COUNT) {
				for (int i = change_pos.size() - 2; i >= 0; i -= 2) {
					swap(best_rnd_pos[change_pos[i]], best_rnd_pos[change_pos[i + 1]]);
				}
			}
		}
	}
	// for (int i = 0; i < H; ++i) {
	// 	for (int j = 0; j < W; ++j) {
	// 		debug("%5d ", unused_count[i][j]);
	// 	}
	// 	debugln();
	// }
	return best;
}

struct State {
	vector<Removal> pairs;
	UsedManager<width> used_manager;
	uint64_t hash;
	int score;
};

struct Sequence {
	vector<Removal> pairs;
	uint64_t hash;
	int score;
	State* parent_st;
};

Sequence create_sequence(State& st) {
	ADD_COUNTER(7);
	Sequence seq;
	seq.parent_st = &st;
	seq.hash = st.hash;
	seq.score = st.score;
	int rem_count = 0;
	START_TIMER(2);
	int rep_count = H * W - st.pairs.size() * 2;
	int window = 1;
	while (window * 2 <= rep_count) {
		window *= 2;
	}
	--window;
	for (int i = 0; i < rep_count / 2; ++i) {
		int pos = rep_count - 1 - (rnd.nextUInt() & window);
		swap(left_pos[i], left_pos[pos]);
		ADD_COUNTER(18);
	}
	// for (int i = rep_count / 2; i < rep_count; ++i) {
	// 	int pos = rnd.nextUInt() & (window - 1);
	// 	swap(left_pos[i], left_pos[pos]);
	// 	ADD_COUNTER(18);
	// }
	STOP_TIMER(2);
	START_TIMER(3);
	for (int i = 0; i < rep_count; ++i) {
		int r = left_pos[i] >> 8;
		int c = left_pos[i] & 0xFF;
		if (st.used_manager.is_used(r, c)) {
			ADD_COUNTER(16);
			continue;
		}
		bool found = false;
		int top = adj_vert[r][c] & 0xFF;
		if (top != 0xFF && field[top][c] == field[r][c]) {
			rem_buf[rem_count].r2 = top;
			rem_buf[rem_count].c2 = c;
			found = true;
		} else {
			int bottom = adj_vert[r][c] >> 8;
			if (bottom != 0 && field[bottom][c] == field[r][c]) {
				rem_buf[rem_count].r2 = bottom;
				rem_buf[rem_count].c2 = c;
				found = true;
			}
		}
		if (!found && c < W - 1) {
			if (!st.used_manager.is_used(r, c + 1)) {
				ADD_COUNTER(13);
				if (field[r][c+1] == field[r][c]) {
					ADD_COUNTER(0);
					rem_buf[rem_count].r2 = r;
					rem_buf[rem_count].c2 = c + 1;
					found = true;
				}
			} else {
				ADD_COUNTER(14);
				int right = st.used_manager.next_right(r, c + 1);
				if (right < W && field[r][right] == field[r][c]) {
					ADD_COUNTER(3);
					rem_buf[rem_count].r2 = r;
					rem_buf[rem_count].c2 = right;
					found = true;
				}
				if (!found) {
					int prev = right - 1;
					for (int y = r + 1; y < H; ++y) {
						ADD_COUNTER(4);
						int x = st.used_manager.next_right(y, c);
						if (x > prev) continue;
						if (field[r][c] == field[y][x]) {
							rem_buf[rem_count].r2 = y;
							rem_buf[rem_count].c2 = x;
							ADD_COUNTER(c == x ? 8 : 9);
							found = true;
							break;
						} else if (x <= c + 1) {
							break;
						} else {
							prev = x - 1;
						}
					}
				}
				if (!found) {
					int prev = right - 1;
					for (int y = r - 1; y >= 0; --y) {
						ADD_COUNTER(5);
						int x = st.used_manager.next_right(y, c);
						if (x > prev) continue;
						if (field[r][c] == field[y][x]) {
							rem_buf[rem_count].r2 = y;
							rem_buf[rem_count].c2 = x;
							ADD_COUNTER(c == x ? 8 : 9);
							found = true;
							break;
						} else if (x <= c + 1) {
							break;
						} else {
							prev = x - 1;
						}
					}
				}
			}
		}
		ADD_COUNTER(found ? 10 : 11);
		if (found) {
			rem_buf[rem_count].r1 = r;
			rem_buf[rem_count].c1 = c;
			const Removal& rem = rem_buf[rem_count];
			st.used_manager.set_used(r, c);
			st.used_manager.set_used(rem.r2, rem.c2);
			remove_exist(r, c);
			remove_exist(rem.r2, rem.c2);
			seq.hash ^= cell_hash[r][c];
			seq.hash ^= cell_hash[rem.r2][rem.c2];
			seq.score += unused_count[r][c];
			seq.score += unused_count[rem.r2][rem.c2];
			++rem_count;
		}
	}
	STOP_TIMER(3);
	START_TIMER(4);
	for (int i = rem_count - 1; i >= 0; --i) {
		st.used_manager.clear_used(rem_buf[i].r1, rem_buf[i].c1);
		st.used_manager.clear_used(rem_buf[i].r2, rem_buf[i].c2);
		restore_exist(rem_buf[i].r2, rem_buf[i].c2);
		restore_exist(rem_buf[i].r1, rem_buf[i].c1);
	}
	seq.pairs.assign(rem_buf.begin(), rem_buf.begin() + rem_count);
	STOP_TIMER(4);
	return seq;
}

vector<Removal> solve_beam(const vector<Removal>& best_ans) {
	vector<Removal> ans;
	State elite_state;
	elite_state.used_manager.clear_used();
	elite_state.score = 0;
	elite_state.hash = 0;
	double elapsed_ratio = 1.0 * get_elapsed_msec() / TL;
	double min_count_ratio = START_MIN_RATIO + (END_MIN_RATIO - START_MIN_RATIO) * elapsed_ratio;
	double max_count_ratio = START_MIN_RATIO + (END_MAX_RATIO - START_MAX_RATIO) * elapsed_ratio;
	int initial_count = (int)(best_ans.size() * rnd.nextDouble() * (max_count_ratio - min_count_ratio) + min_count_ratio);
	for (int i = 0; i < initial_count; ++i) {
		const Removal& rem = best_ans[i];
		elite_state.pairs.push_back(rem);
		elite_state.score += unused_count[rem.r1][rem.c1] + unused_count[rem.r2][rem.c2];
		elite_state.hash ^= cell_hash[rem.r1][rem.c1] ^ cell_hash[rem.r2][rem.c2];
		elite_state.used_manager.set_used(rem.r1, rem.c1);
		elite_state.used_manager.set_used(rem.r2, rem.c2);
	}
	vector<State> cur = {elite_state};
	int best_len = best_ans.size();
	int beam_size = min((int)MIN_BEAM_SIZE, (int)BEAM_SIZE / (H * W));
#ifdef DEBUG
	int cur_best_len = 0;
#endif
	for (int turn = 0; ; ++turn) {
		auto elapsed = get_elapsed_msec();
		if (elapsed > TL) break;
		if (elapsed > TL - 400) {
			beam_size = 10;
		} else {
			beam_size += beam_size / 8;
		}
		vector<Sequence> seqs;
		vector<pair<int, int>> sort_seq;
		// debug("cur size:%d turn:%d\n", (int)cur.size(), turn);
		START_TIMER(10);
		for (State& st : cur) {
			START_TIMER(11);
			fill(top_prev, top_prev + W, 0xFF);
			st.used_manager.set_left_pos();
			STOP_TIMER(11);
			for (int i = 0; i < (int)REP_SINGLE; ++i) {
				START_TIMER(0);
				Sequence seq = create_sequence(st);
				STOP_TIMER(0);
				if (seq.pairs.empty()) break;
				int size = st.pairs.size() + seq.pairs.size();
				if (exist_hash[size].count(seq.hash) != 0) {
					continue;
				}
#ifdef DEBUG
				if (size > cur_best_len) {
					cur_best_len = size;
				}
#endif
				if (size > best_len) {
					debug("beam score update:%d\n", H * W - size * 2);
					best_len = size;
					ans = st.pairs;
					ans.insert(ans.end(), seq.pairs.begin(), seq.pairs.end());
					if (best_len == H * W / 2) return ans;
				}
				exist_hash[size].insert(seq.hash);
				seqs.push_back(seq);
				sort_seq.emplace_back(seq.score, seqs.size() - 1);
			}
		}
		STOP_TIMER(10);
		START_TIMER(1);
		if (seqs.size() > beam_size) {
			nth_element(sort_seq.begin(), sort_seq.begin() + sort_seq.size() - beam_size, sort_seq.end());
			sort_seq.erase(sort_seq.begin(), sort_seq.begin() + sort_seq.size() - beam_size);
		}
		START_TIMER(21);
		vector<State> next;
		next.reserve(sort_seq.size() + 1);
		int max_len = 0;
		for (auto& ss : sort_seq) {
			Sequence& seq = seqs[ss.second];
			State st;
			st.pairs = seq.parent_st->pairs;
			st.pairs.insert(st.pairs.end(), seq.pairs.begin(), seq.pairs.end());
			st.score = seq.score;
			st.hash = seq.hash;
			st.used_manager.used = seq.parent_st->used_manager.used;
			for (Removal& rem : seq.pairs) {
				st.used_manager.set_used(rem.r1, rem.c1);
				st.used_manager.set_used(rem.r2, rem.c2);
			}
			next.push_back(st);
			max_len = max(max_len, (int)st.pairs.size());
		}
		STOP_TIMER(21);
		if (next.empty()) break;
		for (int i = elite_state.pairs.size(); i < min(max_len, (int)best_ans.size()); ++i) {
			const Removal& rem = best_ans[i];
			elite_state.pairs.push_back(rem);
			elite_state.score += unused_count[rem.r1][rem.c1] + unused_count[rem.r2][rem.c2];
			elite_state.hash ^= cell_hash[rem.r1][rem.c1] ^ cell_hash[rem.r2][rem.c2];
			elite_state.used_manager.set_used(rem.r1, rem.c1);
			elite_state.used_manager.set_used(rem.r2, rem.c2);
		}
		next.push_back(elite_state);
		swap(cur, next);
		STOP_TIMER(1);
	}
#ifdef DEBUG
	// debug("beam score:%d\n", H * W - 2 * cur_best_len);
#endif
	return ans;
}

};

template<int width>
vector<Removal> solve() {
	Solver<width> solver;
	vector<Removal> ans = solver.solve_random();
	for (int t = 0; ans.size() * 2 < H * W; ++t) {
		if (reach_time_limit()) {
			debug("beam turn:%d\n", t);
			break;
		}
		// if ((t & 0x3F) == 0) {
		// 	for (auto& hs : exist_hash) {
		// 		hs.clear();
		// 	}
		// }
		auto ans2 = solver.solve_beam(ans);
		int add = (int)ADD_BONUS;
		if (ans2.size() > ans.size()) {
			swap(ans, ans2);
			for (int i = 0; i < H; ++i) {
				for (int j = 0; j < W; ++j) {
					unused_count[i][j] += add;
				}
			}
			for (const Removal& r : ans) {
				unused_count[r.r1][r.c1] -= add;
				unused_count[r.r2][r.c2] -= add;
			}
		}
	}
	return ans;
}

struct SameColorPairs {
  vector<string> removePairs(const vector<string>& board);
};

vector<string> SameColorPairs::removePairs(const vector<string>& board) {
	start_time = get_time();
	H = board.size();
	W = board[0].size();
	bool rot = (H <= 64 && W <= 64 && H > W) || (W > 64 && 20 <= H && H <= 64);
	if (rot) {
		swap(H, W);
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				field[i][j] = board[j][i] - '0';
			}
		}
	} else {
		for (int i = 0; i < H; ++i) {
			for (int j = 0; j < W; ++j) {
				field[i][j] = board[i][j] - '0';
			}
		}
	}
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			// unused_count[i][j] = 1;
			unused_count[i][j] = (H / 2 - min(i, H - 1 - i)) + (W / 2 - min(j, W - 1 - j));
			cell_hash[i][j] = ((uint64_t)rnd.nextUInt() << 32) | rnd.nextUInt();
		}
	}
	exist_hash.resize(H * W / 2 + 1);
	vector<Removal> ans;
	if (W < 64) {
		ans = solve<0>();
	} else if (W == 64) {
		ans = solve<1>();
	} else {
		ans = solve<2>();
	}
	vector<string> ret;
	for (auto& r : ans) {
		stringstream ss;
		if (rot) {
			ss << r.c1 << " " << r.r1 << " " << r.c2 << " " << r.r2;
		} else {
			ss << r.r1 << " " << r.c1 << " " << r.r2 << " " << r.c2;
		}
		ret.push_back(ss.str());
	}
	PRINT_COUNTER();
	PRINT_TIMER();
	return ret;
}

#if defined(LOCAL)
int main() {
  SameColorPairs scp;
  int H;
  cin >> H;
  vector<string> board(H);
  for (int i = 0; i < H; ++i) {
  	cin >> board[i];
  }

  vector<string> ret = scp.removePairs(board);
  printf("%lu\n", ret.size());
  for (int i = 0; i < (int)ret.size(); ++i) {
	  printf("%s\n", ret[i].c_str());
  }
	fflush(stdout);
}
#endif