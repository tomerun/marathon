#!/bin/sh -x

for i in 100000 200000 400000
do
	date
	g++-7 -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL -DREP="$i" -s -pipe -mmmx -msse -msse2 -msse3 -o tester SameColorPairs.cpp
	java Tester -b 2000 -e 4999 > log/log12_"$i".txt
done
