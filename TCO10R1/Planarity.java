import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Planarity {

	private static final long TIMELIMIT = 3000;
	private static final boolean DEBUG = 0 == 0;
	private static final boolean MEASURE_TIME = 0 == 1;

	final long startTime = System.currentTimeMillis();
	XorShift rand = new XorShift();
	int V, E;
	int[] evs;
	int[][] with, without;
	boolean[][] used = new boolean[700][700];
	MyP[] bestPs;
	ArrayList<Integer> hist = new ArrayList<Integer>();
	int cutoff = 0;

	int[] untangle(int V, int[] edges) {
		this.V = V;
		this.E = edges.length / 2;
		this.evs = edges;
		int[] ans = new int[V * 2];
		ArrayList<ArrayList<Integer>> wa = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < V; ++i) {
			wa.add(new ArrayList<Integer>());
		}
		for (int i = 0; i < E; ++i) {
			wa.get(edges[2 * i]).add(i);
			wa.get(edges[2 * i + 1]).add(i);
		}
		with = new int[V][];
		without = new int[V][];
		for (int i = 0; i < V; ++i) {
			with[i] = new int[wa.get(i).size()];
			without[i] = new int[E - with[i].length];
			for (int j = 0; j < with[i].length; ++j) {
				with[i][j] = wa.get(i).get(j);
			}
			int ai = 0;
			int bi = 0;
			for (int j = 0; j < E; ++j) {
				if (ai < with[i].length && with[i][ai] == j) {
					++ai;
				} else {
					without[i][bi++] = j;
				}
			}
		}

		for (int i = 0;; ++i) {
			if (Math.exp(-i * 100.0 / E) < 1e-4) {
				cutoff = i;
				break;
			}
		}
		solve();
		debug(hist.size(), hist);
		for (int i = 0; i < V; ++i) {
			ans[2 * i] = bestPs[i].x;
			ans[2 * i + 1] = bestPs[i].y;
		}
		return ans;
	}

	void solve() {
		// ignore unused vertex 
		int[] vis = new int[V];
		int nonzeroV = 0;
		for (int i = 0; i < V; ++i) {
			if (with[i].length != 0) {
				vis[nonzeroV++] = i;
			}
		}

		MyP[] ps = new MyP[V];
		for (int j = 0; j < V; ++j) {
			ps[j] = new MyP(rand.nextInt(700), rand.nextInt(700));
			while (used[ps[j].y][ps[j].x]) {
				ps[j] = new MyP(rand.nextInt(700), rand.nextInt(700));
			}
			used[ps[j].y][ps[j].x] = true;
		}
		MyEdge[] es = new MyEdge[E];
		boolean[][] cross = new boolean[E][E];
		int cc = 1;
		for (int i = 0; i < E; ++i) {
			es[i] = new MyEdge(ps[evs[2 * i]], ps[evs[2 * i + 1]]);
			for (int j = 0; j < i; ++j) {
				if (es[i].intersect(es[j])) {
					++cc;
					cross[i][j] = cross[j][i] = true;
				}
			}
		}
		debug("init:", cc);

		int bestCount = cc;
		bestPs = ps.clone();
		boolean[] crossCache = new boolean[E * E];
		MyEdge[] edgeCache = new MyEdge[nonzeroV];
		boolean smallMove = false;
		double[] crossRatio = new double[V];
		double[] crossOneRatio = new double[V];
		for (int i = 0; i < nonzeroV; ++i) {
			crossOneRatio[vis[i]] = 1.0 / with[vis[i]].length;
		}
		double crossRatioSum = 0;
		int lastTransit = 0;
		final int tickCount = E > 200 ? 127 : (E > 100 ? 511 : 1023);

		for (int turn = 1;; ++turn) {
			if ((turn & tickCount) == 0) {
				long elapsed = System.currentTimeMillis() - startTime;
				if (elapsed > TIMELIMIT) {
					debug("turn:", turn);
					break;
				}
				if (!smallMove && elapsed > TIMELIMIT * 0.5) {
					smallMove = true;
					debug("switch to smallmove");
					for (int i = 0; i < E; ++i) {
						for (int j = 0; j < i; ++j) {
							if (cross[i][j]) {
								crossRatio[evs[2 * i]] += crossOneRatio[evs[2 * i]];
								crossRatio[evs[2 * i + 1]] += crossOneRatio[evs[2 * i + 1]];
								crossRatio[evs[2 * j]] += crossOneRatio[evs[2 * j]];
								crossRatio[evs[2 * j + 1]] += crossOneRatio[evs[2 * j + 1]];
							}
						}
					}
				}
			}
			int vi = Integer.MIN_VALUE;
			int nx, ny;
			if (smallMove) {
				if (crossRatioSum == 0) {
					for (int i = 0; i < V; ++i) {
						crossRatioSum += crossRatio[i];
					}
				}
				double which = rand.nextDouble() * crossRatioSum;
				for (int j = 0; j < V; ++j) {
					which -= crossRatio[j];
					if (which < 0) {
						vi = j;
						break;
					}
				}
				final int MOVE = 100;
				nx = ps[vi].x + rand.nextInt(MOVE * 2 + 1) - MOVE;
				ny = ps[vi].y + rand.nextInt(MOVE * 2 + 1) - MOVE;
				while (nx < 0 || 700 <= nx || ny < 0 || 700 <= ny || used[ny][nx]) {
					nx = ps[vi].x + rand.nextInt(MOVE * 2 + 1) - MOVE;
					ny = ps[vi].y + rand.nextInt(MOVE * 2 + 1) - MOVE;
				}
			} else {
				vi = vis[rand.nextInt(nonzeroV)];
				nx = rand.nextInt(700);
				ny = rand.nextInt(700);
				while (used[ny][nx]) {
					nx = rand.nextInt(700);
					ny = rand.nextInt(700);
				}
			}
			MyP op = ps[vi];
			ps[vi] = new MyP(nx, ny);
			int diff = 0;
			int cacheEdgeI = 0;
			int cacheCrossI = 0;
			OUT:for (int i : with[vi]) {
				edgeCache[cacheEdgeI++] = new MyEdge(ps[evs[2 * i]], ps[evs[2 * i + 1]]);
				for (int j : without[vi]) {
					boolean sect = edgeCache[cacheEdgeI - 1].intersect(es[j]);
					crossCache[cacheCrossI++] = sect;
					if (cross[i][j] != sect) {
						diff += sect ? 1 : -1;
						if (diff==cutoff) break OUT;
					}
				}
			}
			for (int i = 0; i < with[vi].length&&diff < cutoff; ++i) {
				int ei1 = with[vi][i];
				for (int j = 0; j < i; ++j) {
					int ei2 = with[vi][j];
					boolean sect = edgeCache[i].intersect(edgeCache[j]);
					crossCache[cacheCrossI++] = sect;
					if (cross[ei1][ei2] != sect) {
						diff += sect ? 1 : -1;
						if (diff==cutoff) break;
					}
				}
			}
			if (diff >= 0) {
				while (hist.size() <= diff) {
					hist.add(0);
				}
				hist.set(diff, hist.get(diff) + 1);
			}
			if (turn == lastTransit + 30000 || transit(diff)) {
				lastTransit = turn;
				cc += diff;
				if (cc < bestCount) {
					bestCount = cc;
					bestPs = ps.clone();
					debug("best:", bestCount, turn);
				}

				cacheEdgeI = 0;
				cacheCrossI = 0;
				for (int i : with[vi]) {
					es[i] = edgeCache[cacheEdgeI++];
					for (int j : without[vi]) {
						boolean newCross = crossCache[cacheCrossI++];
						if (cross[i][j] != newCross) {
							if (smallMove) {
								int sign = cross[i][j] ? -1 : 1;
								crossRatio[evs[2 * i]] += crossOneRatio[evs[2 * i]] * sign;
								crossRatio[evs[2 * i + 1]] += crossOneRatio[evs[2 * i + 1]] * sign;
								crossRatio[evs[2 * j]] += crossOneRatio[evs[2 * j]] * sign;
								crossRatio[evs[2 * j + 1]] += crossOneRatio[evs[2 * j + 1]] * sign;
							}
							cross[i][j] = cross[j][i] = newCross;
						}
					}
				}
				for (int i = 0; i < with[vi].length; ++i) {
					int ei1 = with[vi][i];
					for (int j = 0; j < i; ++j) {
						int ei2 = with[vi][j];
						boolean newCross = crossCache[cacheCrossI++];
						if (cross[ei1][ei2] != newCross) {
							if (smallMove) {
								int sign = cross[ei1][ei2] ? -1 : 1;
								crossRatio[evs[2 * ei1]] += crossOneRatio[evs[2 * ei1]] * sign;
								crossRatio[evs[2 * ei1 + 1]] += crossOneRatio[evs[2 * ei1 + 1]] * sign;
								crossRatio[evs[2 * ei2]] += crossOneRatio[evs[2 * ei2]] * sign;
								crossRatio[evs[2 * ei2 + 1]] += crossOneRatio[evs[2 * ei2 + 1]] * sign;
							}
							cross[ei1][ei2] = cross[ei2][ei1] = newCross;
						}
					}
				}
				crossRatioSum = 0;
				used[ny][nx] = true;
				used[op.y][op.x] = false;
			} else {
				ps[vi] = op;
			}
		}
	}

	boolean transit(int diff) {
		if (diff <= 0) return true;
		if (diff >= cutoff) return false;
		return rand.nextDouble() < Math.exp(-diff * 100.0 / E);
	}

	static void debug(Object... obj) {
		if (DEBUG) {
			System.out.println(Arrays.deepToString(obj));
		}
	}

	static final class Timer {
		long[] sum = new long[9];
		long[] start = new long[sum.length];

		void start(int i) {
			if (MEASURE_TIME) start[i] = System.currentTimeMillis();
		}

		void end(int i) {
			if (MEASURE_TIME) sum[i] += System.currentTimeMillis() - start[i];
		}

		void print() {
			if (MEASURE_TIME) System.out.println(Arrays.toString(sum));
		}

	}

	static final class XorShift {
		int x = 123456789;
		int y = 362436069;
		int z = 521288629;
		int w = 88675123;

		int nextInt(int n) {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			final int r = w % n;
			return r < 0 ? r + n : r;
		}

		int nextInt() {
			final int t = x ^ (x << 11);
			x = y;
			y = z;
			z = w;
			w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
			return w == Integer.MIN_VALUE ? 0 : Math.abs(w);
		}

		double nextDouble() {
			return 1. * nextInt() / Integer.MAX_VALUE;
		}
	}

	static class MyP {
		int x, y;

		public MyP(int x1, int y1) {
			x = x1;
			y = y1;
		}

		public static MyP substr(MyP p1, MyP p2) {
			return new MyP(p1.x - p2.x, p1.y - p2.y);
		}
	}

	static class MyEdge {
		public MyP p1, p2, vect; //vector p1 -> p2

		public MyEdge(MyP p1n, MyP p2n) {
			if (p1n.x < p2n.x || p1n.x == p2n.x && p1n.y < p2n.y) { // direct to right
				p1 = p1n;
				p2 = p2n;
			} else {
				p1 = p2n;
				p2 = p1n;
			}
			vect = MyP.substr(p2, p1);
		}

		public boolean intersect(MyEdge other) {
			if (p1.x > other.p2.x) return false;
			if (p2.x < other.p1.x) return false;
			if (Math.min(p1.y, p2.y) > Math.max(other.p1.y, other.p2.y)) return false;
			if (Math.max(p1.y, p2.y) < Math.min(other.p1.y, other.p2.y)) return false;

			int den = other.vect.y * vect.x - other.vect.x * vect.y;

			if (den == 0) {
				int S = (other.p1.y - p1.y) * vect.x - other.vect.x * (other.p1.x - p1.x);
				if (S != 0) return false;
				if (p1 == other.p2) return false;
				if (p2 == other.p1) return false;
				return true;
			}

			if (this.p1 == other.p1 || this.p1 == other.p2 || this.p2 == other.p1 || this.p2 == other.p2) return false;

			int num1 = other.vect.x * (p1.y - other.p1.y) - other.vect.y * (p1.x - other.p1.x);
			double u1 = num1 * 1. / den;
			if (u1 < 0 || u1 > 1) return false;
			int num2 = vect.x * (p1.y - other.p1.y) - vect.y * (p1.x - other.p1.x);
			double u2 = num2 * 1. / den;
			if (u2 < 0 || u2 > 1) return false;
			return true;
		}

	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int V = sc.nextInt();
		int N = sc.nextInt();
		int[] edges = new int[N];
		for (int i = 0; i < N; ++i) {
			edges[i] = sc.nextInt();
		}
		int[] coords = new Planarity().untangle(V, edges);
		for (int i = 0; i < coords.length; ++i) {
			System.out.println(coords[i]);
		}
		System.out.flush();
	}
}
